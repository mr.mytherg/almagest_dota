package com.erg.almagestdota.complexadapter.viewHolders.stats

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.erg.almagestdota.base.external.image.loadImageByUrl
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.R
import com.erg.almagestdota.complexadapter.databinding.HeroItemBinding
import com.erg.almagestdota.complexadapter.databinding.ItemPlayerLanesBinding
import com.erg.almagestdota.complexadapter.databinding.ItemStatsDraftStageBinding

class PlayersLanesViewHolder(
    private val binding: ItemPlayerLanesBinding,
) : ComplexViewHolder<ComplexItem.PlayerLanesItem>(binding.root) {

    companion object {
        fun create(parent: ViewGroup): PlayersLanesViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemPlayerLanesBinding.inflate(inflater, parent, false)
            return PlayersLanesViewHolder(binding)
        }
    }

    override fun bind(item: ComplexItem.PlayerLanesItem) = with(binding) {
        tvName.text = item.playerName
        tvOverall.text = item.overall.toString()
        tvMatchCount.text = item.matchCount.toString()

    }
}