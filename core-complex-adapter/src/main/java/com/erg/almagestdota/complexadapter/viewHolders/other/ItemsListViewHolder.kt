package com.erg.almagestdota.complexadapter.viewHolders.other

import android.view.LayoutInflater
import android.view.ViewGroup
import com.erg.almagestdota.complexadapter.*
import com.erg.almagestdota.complexadapter.databinding.ItemsListBinding

class ItemsListViewHolder(
    private val binding: ItemsListBinding,
    private val complexListener: ComplexListener?,
) : ComplexViewHolder<ComplexItem.ItemsListItem>(binding.root) {

    companion object {
        fun create(parent: ViewGroup, complexListener: ComplexListener?): ItemsListViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemsListBinding.inflate(inflater, parent, false)
            return ItemsListViewHolder(binding, complexListener)
        }
    }

    private val complexAdapter =
        ComplexAdapter(complexListener = complexListener)

    override fun bind(item: ComplexItem.ItemsListItem) = with(binding) {
        tvTitle.text = item.title
        if (rvItems.adapter == null) {
            rvItems.adapter = complexAdapter
        }
        complexAdapter.submitList(item.items)
    }
}