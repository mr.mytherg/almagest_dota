package com.erg.almagestdota.complexadapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.erg.almagestdota.base.external.rx.clicksWithDebounce

class ComplexAdapter(
    private val complexListener: ComplexListener? = null,
    private val holderFactory: (ViewGroup, Int) -> ComplexViewHolder<ComplexItem> = ComplexHolderFactory(
        complexListener,
    ),
) : ListAdapter<ComplexItem, ComplexViewHolder<ComplexItem>>(diffCallback) {

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<ComplexItem>() {
            override fun areItemsTheSame(oldItem: ComplexItem, newItem: ComplexItem): Boolean {
                return oldItem.key == newItem.key
            }

            override fun areContentsTheSame(oldItem: ComplexItem, newItem: ComplexItem): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ComplexViewHolder<ComplexItem> {
        return holderFactory(parent, viewType).apply {
            complexListener?.let { listener ->
                itemView.clicksWithDebounce {
                    if (absoluteAdapterPosition != RecyclerView.NO_POSITION) {
                        val item = getItem(absoluteAdapterPosition)
                        if (!item.subItemClick) {
                            listener.onClick(item)
                        }
                    }
                }
                itemView.setOnLongClickListener {
                    if (absoluteAdapterPosition != RecyclerView.NO_POSITION) {
                        val item = getItem(absoluteAdapterPosition)
                        if (!item.subItemClick) {
                            listener.onLongClick(item)
                        }
                    }
                    return@setOnLongClickListener true
                }
            }
        }
    }

    override fun onBindViewHolder(holder: ComplexViewHolder<ComplexItem>, position: Int) {
        holder.bind(currentList[position])
    }

    override fun getItemViewType(position: Int): Int {
        return currentList[position].viewType
    }
}