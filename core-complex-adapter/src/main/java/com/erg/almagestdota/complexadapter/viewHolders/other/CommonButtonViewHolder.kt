package com.erg.almagestdota.complexadapter.viewHolders.other

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.R
import com.erg.almagestdota.complexadapter.databinding.ItemCommonButtonBinding
import com.erg.almagestdota.complexadapter.databinding.ItemFilterBinding

class CommonButtonViewHolder(
    private val binding: ItemCommonButtonBinding,
) : ComplexViewHolder<ComplexItem.CommonButtonItem>(binding.root) {

    companion object {
        fun create(parent: ViewGroup): CommonButtonViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemCommonButtonBinding.inflate(inflater, parent, false)
            return CommonButtonViewHolder(binding)
        }
    }

    override fun bind(item: ComplexItem.CommonButtonItem) = with(binding) {
        tvTitle.text = item.title
    }
}