package com.erg.almagestdota.complexadapter.viewHolders.heroes

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.erg.almagestdota.base.external.image.loadImageByUrl
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.R
import com.erg.almagestdota.complexadapter.databinding.HeroItemBinding

class HeroSynergyViewHolder(
    private val binding: HeroItemBinding,
) : ComplexViewHolder<ComplexItem.HeroSynergyItem>(binding.root) {

    companion object {
        fun create(parent: ViewGroup): HeroSynergyViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = HeroItemBinding.inflate(inflater, parent, false)
            return HeroSynergyViewHolder(binding)
        }
    }

    override fun bind(item: ComplexItem.HeroSynergyItem) = with(binding) {
        tvName.text = item.title
        tvName.isVisible = true
        ivHero.loadImageByUrl(item.url, R.drawable.ic_placeholder)
        ivHero.alpha = if (item.hasBlackout) 0.4f else 1.0f

        tvSynergy.isVisible = true
        tvSynergy.text = item.synergy.toString()

        tvSteal.isVisible = false
        tvCounter.isVisible = false
        llPro.isVisible = false
        llPub.isVisible = false
        tvRole1.isVisible = false
        tvRole2.isVisible = false
        tvRole3.isVisible = false
        tvRole4.isVisible = false
        tvRole5.isVisible = false
    }
}