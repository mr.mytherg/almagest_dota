package com.erg.almagestdota.complexadapter.viewHolders.heroes

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.erg.almagestdota.base.external.image.loadImageByUrl
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.R
import com.erg.almagestdota.complexadapter.databinding.HeroItemBinding

class HeroStatsViewHolder(
    private val binding: HeroItemBinding,
) : ComplexViewHolder<ComplexItem.HeroStatsItem>(binding.root) {

    companion object {
        fun create(parent: ViewGroup): HeroStatsViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = HeroItemBinding.inflate(inflater, parent, false)
            return HeroStatsViewHolder(binding)
        }
    }

    override fun bind(item: ComplexItem.HeroStatsItem) = with(binding) {
        tvName.isVisible = true
        tvName.text = item.name
        ivHero.loadImageByUrl(item.url, R.drawable.ic_placeholder)
        ivHero.alpha = 1.0f

        llPro.isVisible = true
        llPub.isVisible = true

        tvWon.text = item.wonCount.toString()
        tvLost.text = item.lostCount.toString()

        tvWonPub.text = item.wonCountPub.toString()
        tvLostPub.text = item.lostCountPub.toString()

        tvWon.text = item.wonCount.toString()
        tvLost.text = item.lostCount.toString()

        tvSteal.isVisible = false
        tvSynergy.isVisible = false
        tvCounter.isVisible = false
        tvRole1.isVisible = false
        tvRole2.isVisible = false
        tvRole3.isVisible = false
        tvRole4.isVisible = false
        tvRole5.isVisible = false
    }
}