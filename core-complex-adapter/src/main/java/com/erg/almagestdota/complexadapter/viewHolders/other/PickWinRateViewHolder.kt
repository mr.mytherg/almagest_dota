package com.erg.almagestdota.complexadapter.viewHolders.other

import android.view.LayoutInflater
import android.view.ViewGroup
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.R
import com.erg.almagestdota.complexadapter.databinding.ItemRoleStageBinding

class PickWinRateViewHolder(
    private val binding: ItemRoleStageBinding,
) : ComplexViewHolder<ComplexItem.PickWinRateItem>(binding.root) {

    companion object {
        fun create(parent: ViewGroup): PickWinRateViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemRoleStageBinding.inflate(inflater, parent, false)
            return PickWinRateViewHolder(binding)
        }
    }

    override fun bind(item: ComplexItem.PickWinRateItem) = with(binding) {
        tvTitle.text = item.title
        tvPickRate.text = root.context.getString(R.string.hero_info_item_pick_rate, item.pickRate.toString())
        tvWinRate.text = root.context.getString(R.string.hero_info_item_win_rate, item.pickRate.toString())
    }
}