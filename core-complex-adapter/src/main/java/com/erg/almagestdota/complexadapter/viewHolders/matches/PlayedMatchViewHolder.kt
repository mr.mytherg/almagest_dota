package com.erg.almagestdota.complexadapter.viewHolders.matches

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.complexadapter.*
import com.erg.almagestdota.complexadapter.databinding.ItemMatchBinding

class PlayedMatchViewHolder(
    private val binding: ItemMatchBinding,
    private val complexListener: ComplexListener?,
) : ComplexViewHolder<ComplexItem.PlayedMatchItem>(binding.root) {

    companion object {
        fun create(
            parent: ViewGroup,
            recycledViewPool: RecyclerView.RecycledViewPool,
            complexListener: ComplexListener? = null
        ): PlayedMatchViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemMatchBinding.inflate(inflater, parent, false).apply {
                rvDire.setRecycledViewPool(recycledViewPool)
                rvRadiant.setRecycledViewPool(recycledViewPool)
            }

            return PlayedMatchViewHolder(binding, complexListener)
        }
    }

    private val radiantAdapter = ComplexAdapter()
    private val direAdapter = ComplexAdapter()
    private val context = binding.root.context

    override fun bind(item: ComplexItem.PlayedMatchItem) = with(binding) {
        root.clicksWithDebounce {
            complexListener?.let { listener ->
                listener.onClick(item)
            }
        }

        tvDireResult.isVisible = false
        tvRadiantResult.isVisible = false
        tvRadiantScore.isVisible = true
        tvDireScore.isVisible = true
        tvRadiantSeries.isVisible = false
        tvDireSeries.isVisible = false
        tvDuration.isVisible = true
        tvSeriesType.isVisible = true
        tvSeriesType.isVisible = item.wasPredictionRight != null
        tvSeriesType.text = when (item.wasPredictionRight) {
            null -> {
                tvSeriesType.setTextColor(
                    ContextCompat.getColor(
                        root.context,
                        R.color.textColorSecondary
                    )
                )
                context.getString(R.string.prediction_no)
            }
            true -> {
                tvSeriesType.setTextColor(ContextCompat.getColor(root.context, R.color.green))
                context.getString(R.string.prediction_right)
            }
            false -> {
                tvSeriesType.setTextColor(ContextCompat.getColor(root.context, R.color.red))
                context.getString(R.string.prediction_wrong)
            }
        }

        if (item.hasRadiantWon) {
            tvRadiantResult.isVisible = true
        } else {
            tvDireResult.isVisible = true
        }

        tvRadiantScore.text = item.radiantScore.toString()
        tvDireScore.text = item.direScore.toString()

        tvRadiantName.text = item.radiantName.toString()
        tvDireName.text = item.direName.toString()

        tvDuration.text = item.durationReadable
        if (item.time.isNotEmpty()) {
            tvTime.isVisible = true
            tvTime.text = item.time
        } else {
            tvTime.isVisible = false
        }

        tvTitle.text = item.title

        if (rvRadiant.adapter == null) {
            rvRadiant.setHasFixedSize(true)
            rvRadiant.adapter = radiantAdapter
        }

        if (rvDire.adapter == null) {
            rvDire.setHasFixedSize(true)
            rvDire.adapter = direAdapter
        }
        radiantAdapter.submitList(item.radiantHeroes)
        direAdapter.submitList(item.direHeroes)
    }
}