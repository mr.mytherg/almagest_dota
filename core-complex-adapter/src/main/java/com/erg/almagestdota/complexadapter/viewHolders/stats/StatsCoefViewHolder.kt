package com.erg.almagestdota.complexadapter.viewHolders.stats

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.erg.almagestdota.base.external.image.loadImageByUrl
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.R
import com.erg.almagestdota.complexadapter.databinding.HeroItemBinding
import com.erg.almagestdota.complexadapter.databinding.ItemStatsCoefBinding
import com.erg.almagestdota.complexadapter.databinding.ItemStatsDraftStageBinding

class StatsCoefViewHolder(
    private val binding: ItemStatsCoefBinding,
) : ComplexViewHolder<ComplexItem.StatsCoefItem>(binding.root) {

    companion object {
        fun create(parent: ViewGroup): StatsCoefViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemStatsCoefBinding.inflate(inflater, parent, false)
            return StatsCoefViewHolder(binding)
        }
    }

    override fun bind(item: ComplexItem.StatsCoefItem) = with(binding) {
        ivHero.loadImageByUrl(item.url, R.drawable.ic_placeholder)
        tvName.text = item.heroName
        if (item.value.toDouble() < 0) {
            tvValue.setTextColor(ContextCompat.getColor(root.context, R.color.red))
        } else if (item.value.toDouble() > 0) {
            tvValue.setTextColor(ContextCompat.getColor(root.context, R.color.green))
        } else {
            tvValue.setTextColor(ContextCompat.getColor(root.context, R.color.textColor))
        }
        tvValue.text = item.value
    }
}