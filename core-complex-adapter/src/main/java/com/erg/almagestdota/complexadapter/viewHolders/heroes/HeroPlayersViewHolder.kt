package com.erg.almagestdota.complexadapter.viewHolders.heroes

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import com.erg.almagestdota.base.external.image.loadImageByUrl
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.R
import com.erg.almagestdota.complexadapter.databinding.HeroItemBinding

class HeroPlayersViewHolder(
    private val binding: HeroItemBinding,
) : ComplexViewHolder<ComplexItem.HeroPlayersItem>(binding.root) {

    companion object {
        fun create(parent: ViewGroup): HeroPlayersViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = HeroItemBinding.inflate(inflater, parent, false)
            return HeroPlayersViewHolder(binding)
        }
    }

    override fun bind(item: ComplexItem.HeroPlayersItem) = with(binding) {
        tvName.isVisible = true
        tvName.text = item.name
        ivHero.loadImageByUrl(item.url, R.drawable.ic_placeholder)
        ivHero.alpha = if (item.hasBlackout) 0.4f else 1.0f

        llPro.isVisible = true
        llPub.isVisible = true

        tvWon.text = item.wonCount.toString()
        tvLost.text = item.lostCount.toString()

        tvWonPub.text = item.wonCountPub.toString()
        tvLostPub.text = item.lostCountPub.toString()

        tvSteal.isVisible = false
        tvSynergy.isVisible = false
        tvCounter.isVisible = false
        tvRole1.isInvisible = true
        tvRole2.isInvisible = true
        tvRole3.isInvisible = true
        tvRole4.isInvisible = true
        tvRole5.isInvisible = true

        if (item.players.isNotEmpty()) {
            for (index in item.players.indices) {
                val player = item.players[index]
                when (index) {
                    0 -> {
                        tvRole1.isInvisible = false
                        tvRole1.text = getPlayerName(player)
                    }
                    1 -> {
                        tvRole2.isInvisible = false
                        tvRole2.text = getPlayerName(player)
                    }
                    2 -> {
                        tvRole3.isInvisible = false
                        tvRole3.text = getPlayerName(player)
                    }
                    3 -> {
                        tvRole4.isInvisible = false
                        tvRole4.text = getPlayerName(player)
                    }
                    4 -> {
                        tvRole5.isInvisible = false
                        tvRole5.text = getPlayerName(player)
                    }
                }
            }
        }
    }

    private fun getPlayerName(player: Pair<Long, String>): String {
        return player.second.ifEmpty { player.first.toString() }
    }
}