package com.erg.almagestdota.complexadapter.viewHolders.stats

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.erg.almagestdota.base.external.image.loadImageByUrl
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.R
import com.erg.almagestdota.complexadapter.databinding.HeroItemBinding
import com.erg.almagestdota.complexadapter.databinding.ItemStatsDraftStageBinding

class StatsDraftStageViewHolder(
    private val binding: ItemStatsDraftStageBinding,
) : ComplexViewHolder<ComplexItem.DraftStageItem>(binding.root) {

    companion object {
        fun create(parent: ViewGroup): StatsDraftStageViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemStatsDraftStageBinding.inflate(inflater, parent, false)
            return StatsDraftStageViewHolder(binding)
        }
    }

    private val context = binding.root.context

    override fun bind(item: ComplexItem.DraftStageItem) = with(binding) {
        ivHero.isVisible = !item.hideImage
        ivHero.loadImageByUrl(item.url, R.drawable.ic_placeholder)
        tvName.text = item.heroName
        if (item.winRate < 50) {
            tvWinRate.setTextColor(ContextCompat.getColor(root.context, R.color.red))
        } else if (item.winRate > 50) {
            tvWinRate.setTextColor(ContextCompat.getColor(root.context, R.color.green))
        } else {
            tvWinRate.setTextColor(ContextCompat.getColor(root.context, R.color.textColor))
        }
        tvWinRate.text = context.getString(R.string.percentage, item.winRate.toString())
        tvPickRate.text = context.getString(R.string.percentage, item.pickRate.toString())
        tvMatchCount.isVisible = item.showMatchCount
        tvMatchCount.text = item.matchCount.toString()

        root.alpha = if (item.hasBlackout) 0.4f else 1.0f

    }
}