package com.erg.almagestdota.complexadapter.viewHolders.stats

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.erg.almagestdota.base.external.image.loadImageByUrl
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.R
import com.erg.almagestdota.complexadapter.databinding.*

class Bands3ViewHolder(
    private val binding: ItemBand3Binding,
) : ComplexViewHolder<ComplexItem.Band3Item>(binding.root) {

    companion object {
        fun create(parent: ViewGroup): Bands3ViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemBand3Binding.inflate(inflater, parent, false)
            return Bands3ViewHolder(binding)
        }
    }
    private val context = binding.root.context

    override fun bind(item: ComplexItem.Band3Item) = with(binding) {
        root.alpha = if (item.hasBlackout) 0.4f else 1.0f
        ivHero1.loadImageByUrl(item.url1, R.drawable.ic_placeholder)
        ivHero2.loadImageByUrl(item.url2, R.drawable.ic_placeholder)
        ivHero3.loadImageByUrl(item.url3, R.drawable.ic_placeholder)
        tvStat1.text = item.matchCount.toString()
        if (item.winRate < 50) {
            tvStat2.setTextColor(ContextCompat.getColor(root.context, R.color.red))
        } else if (item.winRate > 50) {
            tvStat2.setTextColor(ContextCompat.getColor(root.context, R.color.green))
        } else {
            tvStat2.setTextColor(ContextCompat.getColor(root.context, R.color.textColor))
        }
        tvStat2.text = context.getString(R.string.percentage, item.winRate.toString())
        tvStat3.text = context.getString(R.string.percentage, item.pickRate.toString())

        tvName1.text = item.heroName1
        tvName2.text = item.heroName2
        tvName3.text = item.heroName3
    }
}