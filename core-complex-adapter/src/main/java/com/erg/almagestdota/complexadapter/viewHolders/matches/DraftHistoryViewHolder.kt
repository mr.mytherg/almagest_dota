package com.erg.almagestdota.complexadapter.viewHolders.matches

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.complexadapter.ComplexAdapter
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.databinding.ItemMatchBinding

class DraftHistoryViewHolder(
    private val binding: ItemMatchBinding,
    private val complexListener: ComplexListener?,
) : ComplexViewHolder<ComplexItem.DraftHistoryItem>(binding.root) {

    companion object {
        fun create(
            parent: ViewGroup,
            recycledViewPool: RecyclerView.RecycledViewPool,
            complexListener: ComplexListener? = null
        ): DraftHistoryViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemMatchBinding.inflate(inflater, parent, false).apply {
                rvDire.setRecycledViewPool(recycledViewPool)
                rvRadiant.setRecycledViewPool(recycledViewPool)
            }
            return DraftHistoryViewHolder(binding, complexListener)
        }
    }

    private val radiantAdapter = ComplexAdapter()
    private val direAdapter = ComplexAdapter()

    override fun bind(item: ComplexItem.DraftHistoryItem) = with(binding) {
        root.clicksWithDebounce {
            complexListener?.let { listener ->
                listener.onClick(item)
            }
        }
        tvDireResult.isVisible = false
        tvRadiantResult.isVisible = false
        tvRadiantScore.isVisible = false
        tvDireScore.isVisible = false
        tvRadiantSeries.isVisible = false
        tvDireSeries.isVisible = false
        tvDuration.isVisible = false
        tvSeriesType.isVisible = false

        tvRadiantName.text = item.radiantName.toString()
        tvDireName.text = item.direName.toString()

        tvTitle.text = item.title

        if (rvRadiant.adapter == null) {
            rvRadiant.setHasFixedSize(true)
            rvRadiant.adapter = radiantAdapter
        }

        if (rvDire.adapter == null) {
            rvDire.setHasFixedSize(true)
            rvDire.adapter = direAdapter
        }
        radiantAdapter.submitList(item.radiantHeroes)
        direAdapter.submitList(item.direHeroes)
    }
}