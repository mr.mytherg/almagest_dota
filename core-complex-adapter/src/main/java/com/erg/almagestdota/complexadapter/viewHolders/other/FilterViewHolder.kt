package com.erg.almagestdota.complexadapter.viewHolders.other

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.R
import com.erg.almagestdota.complexadapter.databinding.ItemFilterBinding

class FilterViewHolder(
    private val binding: ItemFilterBinding,
) : ComplexViewHolder<ComplexItem.FilterItem>(binding.root) {

    companion object {
        fun create(parent: ViewGroup): FilterViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemFilterBinding.inflate(inflater, parent, false)
            return FilterViewHolder(binding)
        }
    }

    override fun bind(item: ComplexItem.FilterItem) = with(binding) {
        tvFilter.text = item.title
        if (item.isSelected) {
            tvFilter.setTextColor(ContextCompat.getColor(root.context, R.color.white))
            tvFilter.setBackgroundResource(R.drawable.bg_rect_filled_rounded)
        } else {
            tvFilter.setTextColor(ContextCompat.getColor(root.context, R.color.colorPrimary))
            tvFilter.setBackgroundResource(R.drawable.bg_rect_filled_with_border_rounded)
        }
    }
}