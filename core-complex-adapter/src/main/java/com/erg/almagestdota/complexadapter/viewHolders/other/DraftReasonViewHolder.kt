package com.erg.almagestdota.complexadapter.viewHolders.other

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.erg.almagestdota.base.external.image.loadImageByUrl
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.R
import com.erg.almagestdota.complexadapter.databinding.HeroItemBinding
import com.erg.almagestdota.complexadapter.databinding.ItemDraftReasonBinding
import com.erg.almagestdota.complexadapter.databinding.ItemStatsDraftStageBinding
import com.erg.almagestdota.complexadapter.databinding.ItemStatsTournamentsBinding

class DraftReasonViewHolder(
    private val binding: ItemDraftReasonBinding,
) : ComplexViewHolder<ComplexItem.DraftReasonItem>(binding.root) {

    companion object {
        fun create(parent: ViewGroup): DraftReasonViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemDraftReasonBinding.inflate(inflater, parent, false)
            return DraftReasonViewHolder(binding)
        }
    }

    private val context = binding.root.context

    override fun bind(item: ComplexItem.DraftReasonItem) = with(binding) {
        tvName.text = item.name
    }
}