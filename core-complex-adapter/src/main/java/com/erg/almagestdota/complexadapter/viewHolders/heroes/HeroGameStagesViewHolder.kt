package com.erg.almagestdota.complexadapter.viewHolders.heroes

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import com.erg.almagestdota.base.external.image.loadImageByUrl
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.R
import com.erg.almagestdota.complexadapter.databinding.HeroItemBinding

class HeroGameStagesViewHolder(
    private val binding: HeroItemBinding,
) : ComplexViewHolder<ComplexItem.HeroGameStagesItem>(binding.root) {

    companion object {
        fun create(parent: ViewGroup): HeroGameStagesViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = HeroItemBinding.inflate(inflater, parent, false)
            return HeroGameStagesViewHolder(binding)
        }
    }

    override fun bind(item: ComplexItem.HeroGameStagesItem) = with(binding) {
        tvName.isVisible = true
        tvName.text = item.title
        ivHero.loadImageByUrl(item.url, R.drawable.ic_placeholder)
        ivHero.alpha = 1.0f


        llPro.isVisible = false
        llPub.isVisible = false

        tvSteal.isVisible = false
        tvSynergy.isVisible = false
        tvCounter.isVisible = false

        tvRole1.isInvisible = true
        tvRole2.isInvisible = true
        tvRole3.isInvisible = true
        tvRole4.isVisible = false
        tvRole5.isVisible = false

        if (item.isEarlyGame) {
            tvRole1.isInvisible = false
            tvRole1.text = root.context.getString(R.string.draft_type_game_stage_early)
        }
        if (item.isMidGame) {
            tvRole2.isInvisible = false
            tvRole2.text = root.context.getString(R.string.draft_type_game_stage_mid)
        }
        if (item.isLateGame) {
            tvRole3.isInvisible = false
            tvRole3.text = root.context.getString(R.string.draft_type_game_stage_late)
        }
    }
}