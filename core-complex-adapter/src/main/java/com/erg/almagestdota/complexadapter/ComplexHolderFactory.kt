package com.erg.almagestdota.complexadapter

import android.view.ViewGroup
import com.erg.almagestdota.complexadapter.viewHolders.*
import com.erg.almagestdota.complexadapter.viewHolders.heroes.*
import com.erg.almagestdota.complexadapter.viewHolders.other.*
import com.erg.almagestdota.complexadapter.viewHolders.stats.*

class ComplexHolderFactory(
    private val complexListener: ComplexListener?,
) : (ViewGroup, Int) -> ComplexViewHolder<ComplexItem> {

    @Suppress("UNCHECKED_CAST")
    override fun invoke(parent: ViewGroup, viewType: Int): ComplexViewHolder<ComplexItem> {
        return when (viewType) {
            ComplexItem.ItemDefault.TYPE -> ItemDefaultViewHolder.create(parent)

            ComplexItem.HeroBanItem.TYPE -> HeroBanViewHolder.create(parent)
            ComplexItem.HeroStatsItem.TYPE -> HeroStatsViewHolder.create(parent)
            ComplexItem.HeroRolesItem.TYPE -> HeroRolesViewHolder.create(parent)
            ComplexItem.HeroPlayersItem.TYPE -> HeroPlayersViewHolder.create(parent)
            ComplexItem.HeroScoreItem.TYPE -> HeroScoreViewHolder.create(parent)
            ComplexItem.HeroCounterItem.TYPE -> HeroCounterViewHolder.create(parent)
            ComplexItem.HeroSynergyItem.TYPE -> HeroSynergyViewHolder.create(parent)
            ComplexItem.HeroResultItem.TYPE -> HeroResultViewHolder.create(parent)
            ComplexItem.HeroDraftItem.TYPE -> HeroDraftViewHolder.create(parent)
            ComplexItem.HeroGameStagesItem.TYPE -> HeroGameStagesViewHolder.create(parent)
            ComplexItem.LeagueHistoryItem.TYPE -> LeagueHistoryViewHolder.create(parent)
            ComplexItem.HeroSimplePlayerItem.TYPE -> HeroSimplePlayerViewHolder.create(parent)
            ComplexItem.DraftReasonItem.TYPE -> DraftReasonViewHolder.create(parent)
            ComplexItem.DraftAnalysisReasonItem.TYPE -> DraftAnalysisReasonViewHolder.create(parent)
            ComplexItem.GameDataItem.TYPE -> GameDataViewHolder.create(parent)
            ComplexItem.CommonButtonItem.TYPE -> CommonButtonViewHolder.create(parent)

            ComplexItem.Band2Item.TYPE -> Bands2ViewHolder.create(parent)
            ComplexItem.Band3Item.TYPE -> Bands3ViewHolder.create(parent)
            ComplexItem.Band4Item.TYPE -> Bands4ViewHolder.create(parent)
            ComplexItem.DraftStageItem.TYPE -> StatsDraftStageViewHolder.create(parent)
            ComplexItem.StatsCoefItem.TYPE -> StatsCoefViewHolder.create(parent)
            ComplexItem.Stats1Item.TYPE -> Stats1ViewHolder.create(parent)
            ComplexItem.Stats2Item.TYPE -> Stats2ViewHolder.create(parent)
            ComplexItem.Stats3Item.TYPE -> Stats3ViewHolder.create(parent)
            ComplexItem.StatsTournamentsItem.TYPE -> StatsTournamentsViewHolder.create(parent)
            ComplexItem.PlayerLanesItem.TYPE -> PlayersLanesViewHolder.create(parent)


            ComplexItem.PickWinRateItem.TYPE -> PickWinRateViewHolder.create(parent)
            ComplexItem.SequenceItem.TYPE -> SequenceViewHolder.create(parent)
            ComplexItem.FilterItem.TYPE -> FilterViewHolder.create(parent)
            ComplexItem.MatchUpItem.TYPE -> MatchUpViewHolder.create(parent)
            ComplexItem.ItemsListItem.TYPE -> ItemsListViewHolder.create(parent, complexListener)
            else -> throw IllegalArgumentException("Unknown view type")
        } as ComplexViewHolder<ComplexItem>
    }
}