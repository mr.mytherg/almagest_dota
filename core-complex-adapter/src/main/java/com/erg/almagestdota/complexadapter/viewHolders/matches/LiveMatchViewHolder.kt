package com.erg.almagestdota.complexadapter.viewHolders.matches

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.complexadapter.ComplexAdapter
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.databinding.ItemMatchBinding

class LiveMatchViewHolder(
    private val binding: ItemMatchBinding,
    private val complexListener: ComplexListener?,
) : ComplexViewHolder<ComplexItem.LiveMatchItem>(binding.root) {

    companion object {
        fun create(
            parent: ViewGroup,
            recycledViewPool: RecyclerView.RecycledViewPool,
            complexListener: ComplexListener? = null
        ): LiveMatchViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemMatchBinding.inflate(inflater, parent, false).apply {
                rvDire.setRecycledViewPool(recycledViewPool)
                rvRadiant.setRecycledViewPool(recycledViewPool)
            }
            return LiveMatchViewHolder(binding, complexListener)
        }
    }

    private val radiantAdapter =
        ComplexAdapter()
    private val direAdapter =
        ComplexAdapter()

    override fun bind(item: ComplexItem.LiveMatchItem) = with(binding) {
        root.clicksWithDebounce {
            complexListener?.let { listener ->
                listener.onClick(item)
            }
        }
        tvDireResult.isVisible = false
        tvRadiantResult.isVisible = false
        tvRadiantScore.isVisible = true
        tvDireScore.isVisible = true
        tvRadiantSeries.isVisible = true
        tvDireSeries.isVisible = true
        tvDuration.isVisible = true
        tvSeriesType.isVisible = true

        tvRadiantScore.text = item.radiantScore.toString()
        tvDireScore.text = item.direScore.toString()

        tvRadiantSeries.text = item.radiantSeriesScore.toString()
        tvDireSeries.text = item.direSeriesScore.toString()

        tvRadiantName.text = item.radiantName.toString()
        tvDireName.text = item.direName.toString()

        tvDuration.text = item.durationReadable
        tvTitle.text = item.title
        tvSeriesType.text = item.seriesType

        if (rvRadiant.adapter == null) {
            rvRadiant.adapter = radiantAdapter
        }

        if (rvDire.adapter == null) {
            rvDire.adapter = direAdapter
        }
        radiantAdapter.submitList(item.radiantHeroes)
        direAdapter.submitList(item.direHeroes)
    }
}