package com.erg.almagestdota.complexadapter.viewHolders.stats

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.erg.almagestdota.base.external.image.loadImageByUrl
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.R
import com.erg.almagestdota.complexadapter.databinding.HeroItemBinding
import com.erg.almagestdota.complexadapter.databinding.ItemStats2Binding
import com.erg.almagestdota.complexadapter.databinding.ItemStatsCoefBinding
import com.erg.almagestdota.complexadapter.databinding.ItemStatsDraftStageBinding

class Stats2ViewHolder(
    private val binding: ItemStats2Binding,
) : ComplexViewHolder<ComplexItem.Stats2Item>(binding.root) {

    companion object {
        fun create(parent: ViewGroup): Stats2ViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemStats2Binding.inflate(inflater, parent, false)
            return Stats2ViewHolder(binding)
        }
    }

    override fun bind(item: ComplexItem.Stats2Item) = with(binding) {
        root.alpha = if (item.hasBlackout) 0.4f else 1.0f
        if (item.hasImage) {
            ivHero.loadImageByUrl(item.url, R.drawable.ic_placeholder)
            ivHero.isVisible = true
        } else
            ivHero.isVisible = false
        tvStat1.text = item.value1
        tvStat2.text = item.value2
        tvName.text = item.heroName
    }
}