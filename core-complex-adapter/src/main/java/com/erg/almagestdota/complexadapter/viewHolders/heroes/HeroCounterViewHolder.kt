package com.erg.almagestdota.complexadapter.viewHolders.heroes

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.erg.almagestdota.base.external.image.loadImageByUrl
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.R
import com.erg.almagestdota.complexadapter.databinding.HeroItemBinding

class HeroCounterViewHolder(
    private val binding: HeroItemBinding,
) : ComplexViewHolder<ComplexItem.HeroCounterItem>(binding.root) {

    companion object {
        fun create(parent: ViewGroup): HeroCounterViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = HeroItemBinding.inflate(inflater, parent, false)
            return HeroCounterViewHolder(binding)
        }
    }

    override fun bind(item: ComplexItem.HeroCounterItem) = with(binding) {
        tvName.text = item.title
        tvName.isVisible = true
        ivHero.loadImageByUrl(item.url, R.drawable.ic_placeholder)
        ivHero.alpha = if (item.hasBlackout) 0.4f else 1.0f

        tvCounter.isVisible = true
        tvCounter.text = item.counter.toString()

        tvSteal.isVisible = false
        tvSynergy.isVisible = false
        llPro.isVisible = false
        llPub.isVisible = false
        tvRole1.isVisible = false
        tvRole2.isVisible = false
        tvRole3.isVisible = false
        tvRole4.isVisible = false
        tvRole5.isVisible = false
    }
}