package com.erg.almagestdota.complexadapter.viewHolders

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.erg.almagestdota.base.external.image.loadImageByUrl
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.R
import com.erg.almagestdota.complexadapter.databinding.HeroItemBinding

class ItemDefaultViewHolder(
    private val binding: HeroItemBinding,
) : ComplexViewHolder<ComplexItem.ItemDefault>(binding.root) {

    companion object {
        fun create(parent: ViewGroup): ItemDefaultViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = HeroItemBinding.inflate(inflater, parent, false)
            return ItemDefaultViewHolder(binding)
        }
    }

    override fun bind(item: ComplexItem.ItemDefault) = with(binding) {
        tvName.text = item.title
        tvName.isVisible = item.title.isNotEmpty()
        if (item.hasPlaceholder) {
            ivHero.loadImageByUrl(item.url, R.drawable.ic_placeholder)
        } else {
            ivHero.loadImageByUrl(item.url)
        }

        ivHero.alpha = if (item.hasBlackout) 0.4f else 1.0f

        tvSteal.isVisible = false
        tvSynergy.isVisible = false
        tvCounter.isVisible = false
        llPro.isVisible = false
        llPub.isVisible = false
        tvRole1.isVisible = false
        tvRole2.isVisible = false
        tvRole3.isVisible = false
        tvRole4.isVisible = false
        tvRole5.isVisible = false
    }
}