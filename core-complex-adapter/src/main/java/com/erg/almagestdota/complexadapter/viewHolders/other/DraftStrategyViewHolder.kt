package com.erg.almagestdota.complexadapter.viewHolders.other

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.complexadapter.ComplexAdapter
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.databinding.ItemDraftStrategyBinding
import com.erg.almagestdota.complexadapter.databinding.ItemMatchBinding

class DraftStrategyViewHolder(
    private val binding: ItemDraftStrategyBinding,
    private val complexListener: ComplexListener?,
) : ComplexViewHolder<ComplexItem.DraftStrategyItem>(binding.root) {

    companion object {
        fun create(
            parent: ViewGroup,
            complexListener: ComplexListener? = null
        ): DraftStrategyViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemDraftStrategyBinding.inflate(inflater, parent, false)
            return DraftStrategyViewHolder(binding, complexListener)
        }
    }

    private val picksAdapter = ComplexAdapter(complexListener = complexListener)
    private val bansAdapter = ComplexAdapter(complexListener = complexListener)

    override fun bind(item: ComplexItem.DraftStrategyItem) = with(binding) {
        if (rvPicks.adapter == null) {
            rvPicks.adapter = picksAdapter
        }

        if (rvBans.adapter == null) {
            rvBans.adapter = bansAdapter
        }
        picksAdapter.submitList(item.picks)
        bansAdapter.submitList(item.bans)
    }
}