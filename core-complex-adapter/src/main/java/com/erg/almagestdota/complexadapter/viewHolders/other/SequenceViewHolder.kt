package com.erg.almagestdota.complexadapter.viewHolders.other

import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.R
import com.erg.almagestdota.complexadapter.databinding.ItemSequenceBinding

class SequenceViewHolder(
    private val binding: ItemSequenceBinding,
) : ComplexViewHolder<ComplexItem.SequenceItem>(binding.root) {

    companion object {
        fun create(parent: ViewGroup): SequenceViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemSequenceBinding.inflate(inflater, parent, false)
            return SequenceViewHolder(binding)
        }
    }

    override fun bind(item: ComplexItem.SequenceItem) = with(binding) {
        tvSequence.text = if (item.isPick) {
            root.context.getString(R.string.pick)
        } else {
            root.context.getString(R.string.ban)
        }

        if (item.isRadiant) {
            ivSequence.setImageDrawable(ContextCompat.getDrawable(ivSequence.context, R.drawable.ic_sequence_top))
        } else {
            ivSequence.setImageDrawable(ContextCompat.getDrawable(ivSequence.context, R.drawable.ic_sequence_bottom))
        }

        configureImageView(this, item.isCurrent)
    }

    private fun configureImageView(binding: ItemSequenceBinding, isCurrent: Boolean) {
        if (isCurrent) {
            binding.ivSequence.colorFilter =
                PorterDuffColorFilter(
                    ContextCompat.getColor(binding.ivSequence.context, R.color.colorPrimary),
                    PorterDuff.Mode.SRC_IN
                )
            binding.tvSequence.setTextAppearance(R.style.AlmagestTheme_TextActive)
        } else {
            binding.ivSequence.colorFilter =
                PorterDuffColorFilter(
                    ContextCompat.getColor(binding.ivSequence.context, R.color.textColorSecondary),
                    PorterDuff.Mode.SRC_IN
                )
            binding.tvSequence.setTextAppearance(R.style.AlmagestTheme_TextPassive)
        }
    }
}