package com.erg.almagestdota.complexadapter.viewHolders.other

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.erg.almagestdota.base.external.image.loadImageByUrl
import com.erg.almagestdota.complexadapter.*
import com.erg.almagestdota.complexadapter.databinding.HeroItemBinding
import com.erg.almagestdota.complexadapter.databinding.ItemRolesSelectionBinding

class RolesSelectionViewHolder(
    private val binding: ItemRolesSelectionBinding,
    private val complexListener: ComplexListener,
) : ComplexViewHolder<ComplexItem.RoleSelectionItem>(binding.root) {

    companion object {
        fun create(
            parent: ViewGroup,
            complexListener: ComplexListener
        ): RolesSelectionViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemRolesSelectionBinding.inflate(inflater, parent, false)
            return RolesSelectionViewHolder(binding, complexListener)
        }
    }

    private val adapter = ComplexAdapter(complexListener)

    override fun bind(item: ComplexItem.RoleSelectionItem) = with(binding) {
        tvName.text = item.name
        ivHero.loadImageByUrl(item.url, R.drawable.ic_placeholder)
        ivHero.alpha = if (item.roles.isEmpty()) 0.4f else 1.0f
        if (rvRoles.adapter == null) {
            rvRoles.adapter = adapter
            rvRoles.setHasFixedSize(true)
        }
        adapter.submitList(item.roles)
    }
}