package com.erg.almagestdota.complexadapter.viewHolders.stats

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.erg.almagestdota.base.external.image.loadImageByUrl
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.R
import com.erg.almagestdota.complexadapter.databinding.*

class Stats1ViewHolder(
    private val binding: ItemStats1Binding,
) : ComplexViewHolder<ComplexItem.Stats1Item>(binding.root) {

    companion object {
        fun create(parent: ViewGroup): Stats1ViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemStats1Binding.inflate(inflater, parent, false)
            return Stats1ViewHolder(binding)
        }
    }

    override fun bind(item: ComplexItem.Stats1Item) = with(binding) {
        root.alpha = if (item.hasBlackout) 0.4f else 1.0f
        if (item.hasImage) {
            ivHero.loadImageByUrl(item.url, R.drawable.ic_placeholder)
            ivHero.isVisible = true
        } else
            ivHero.isVisible = false

        tvStat1.text = item.value1
        tvName.text = item.heroName
    }
}