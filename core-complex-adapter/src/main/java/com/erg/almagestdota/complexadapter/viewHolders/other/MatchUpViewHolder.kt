package com.erg.almagestdota.complexadapter.viewHolders.other

import android.view.LayoutInflater
import android.view.ViewGroup
import com.erg.almagestdota.complexadapter.ComplexAdapter
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.databinding.ItemMatchUpBinding

class MatchUpViewHolder(
    private val binding: ItemMatchUpBinding,
) : ComplexViewHolder<ComplexItem.MatchUpItem>(binding.root) {

    companion object {
        fun create(parent: ViewGroup): MatchUpViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemMatchUpBinding.inflate(inflater, parent, false)
            return MatchUpViewHolder(binding)
        }
    }

    private val adapter = ComplexAdapter()

    override fun bind(item: ComplexItem.MatchUpItem) = with(binding) {
        if (rvMatchUp.adapter == null) {
            rvMatchUp.adapter = adapter
            rvMatchUp.setHasFixedSize(true)
        }
        adapter.submitList(item.heroes)
    }
}