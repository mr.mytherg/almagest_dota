package com.erg.almagestdota.complexadapter

interface ComplexListener {
    fun onClick(item: ComplexItem) = Unit
    fun onLongClick(item: ComplexItem): Boolean = false
}