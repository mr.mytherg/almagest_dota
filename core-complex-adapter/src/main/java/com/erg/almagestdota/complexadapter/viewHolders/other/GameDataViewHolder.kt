package com.erg.almagestdota.complexadapter.viewHolders.other

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.erg.almagestdota.base.external.image.loadImageByUrl
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.R
import com.erg.almagestdota.complexadapter.databinding.HeroItemBinding
import com.erg.almagestdota.complexadapter.databinding.ItemDraftAnalysisReasonBinding
import com.erg.almagestdota.complexadapter.databinding.ItemDraftReasonBinding
import com.erg.almagestdota.complexadapter.databinding.ItemStatsDraftStageBinding
import com.erg.almagestdota.complexadapter.databinding.ItemStatsTournamentsBinding

class GameDataViewHolder(
    private val binding: ItemDraftAnalysisReasonBinding,
) : ComplexViewHolder<ComplexItem.GameDataItem>(binding.root) {

    companion object {
        fun create(parent: ViewGroup): GameDataViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemDraftAnalysisReasonBinding.inflate(inflater, parent, false)
            return GameDataViewHolder(binding)
        }
    }

    private val context = binding.root.context

    override fun bind(item: ComplexItem.GameDataItem) = with(binding) {
        root.alpha = if (item.hasBlackout) 0.4f else 1.0f
        tvName.text = item.name
        if (item.radiantCount > item.direCount) {
            tvRadiantNumber.setTextColor(ContextCompat.getColor(root.context, R.color.green))
            tvDireNumber.setTextColor(ContextCompat.getColor(root.context, R.color.red))
        } else if (item.radiantCount < item.direCount) {
            tvRadiantNumber.setTextColor(ContextCompat.getColor(root.context, R.color.red))
            tvDireNumber.setTextColor(ContextCompat.getColor(root.context, R.color.green))
        } else {
            tvRadiantNumber.setTextColor(ContextCompat.getColor(root.context, R.color.textColor))
            tvDireNumber.setTextColor(ContextCompat.getColor(root.context, R.color.textColor))
        }
        tvRadiantNumber.text = item.radiantCount.toString()
        tvDireNumber.text = item.direCount.toString()
    }
}
