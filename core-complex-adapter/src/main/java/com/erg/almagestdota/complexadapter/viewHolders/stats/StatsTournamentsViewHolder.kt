package com.erg.almagestdota.complexadapter.viewHolders.stats

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.erg.almagestdota.base.external.image.loadImageByUrl
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.R
import com.erg.almagestdota.complexadapter.databinding.HeroItemBinding
import com.erg.almagestdota.complexadapter.databinding.ItemStatsDraftStageBinding
import com.erg.almagestdota.complexadapter.databinding.ItemStatsTournamentsBinding

class StatsTournamentsViewHolder(
    private val binding: ItemStatsTournamentsBinding,
) : ComplexViewHolder<ComplexItem.StatsTournamentsItem>(binding.root) {

    companion object {
        fun create(parent: ViewGroup): StatsTournamentsViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemStatsTournamentsBinding.inflate(inflater, parent, false)
            return StatsTournamentsViewHolder(binding)
        }
    }

    private val context = binding.root.context

    override fun bind(item: ComplexItem.StatsTournamentsItem) = with(binding) {
        tvName.text = item.name
        tvMatchCount.text = item.matchCount.toString()

        ivSelected.isVisible = item.isSelected
    }
}