package com.erg.almagestdota.complexadapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class ComplexViewHolder<in T : ComplexItem>(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun bind(item: T)
}