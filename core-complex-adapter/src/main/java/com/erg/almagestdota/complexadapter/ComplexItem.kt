package com.erg.almagestdota.complexadapter

sealed class ComplexItem {

    abstract val key: String
    abstract val viewType: Int
    abstract val subItemClick: Boolean

    abstract override fun equals(other: Any?): Boolean
    abstract override fun hashCode(): Int

    data class ItemDefault(
        override var key: String,
        override val subItemClick: Boolean = false,
        var url: String? = null,
        var title: String = "",
        var hasBlackout: Boolean = false,
        var hasPlaceholder: Boolean = true,
    ) : ComplexItem() {

        override val viewType: Int = TYPE

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is ItemDefault) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (url != other.url) return false
            if (title != other.title) return false
            if (hasBlackout != other.hasBlackout) return false
            if (hasPlaceholder != other.hasPlaceholder) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + url.hashCode()
            result = 31 * result + title.hashCode()
            result = 31 * result + hasBlackout.hashCode()
            result = 31 * result + hasPlaceholder.hashCode()
            return result
        }

        companion object {
            const val TYPE = 0
        }
    }

    data class HeroStatsItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var name: String = "",
        var url: String? = null,
        var wonCount: Int = 0,
        var wonCountPub: Int = 0,
        var lostCount: Int = 0,
        var lostCountPub: Int = 0,
    ) : ComplexItem() {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is HeroStatsItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (url != other.url) return false
            if (name != other.name) return false
            if (wonCount != other.wonCount) return false
            if (lostCount != other.lostCount) return false
            if (wonCountPub != other.wonCountPub) return false
            if (lostCountPub != other.lostCountPub) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + url.hashCode()
            result = 31 * result + name.hashCode()
            result = 31 * result + wonCount.hashCode()
            result = 31 * result + wonCountPub.hashCode()
            result = 31 * result + lostCount.hashCode()
            result = 31 * result + lostCountPub.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 1
        }
    }

    data class HeroBanItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var url: String? = null,
        var hasBlackout: Boolean = true,
    ) : ComplexItem() {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is HeroBanItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (url != other.url) return false
            if (hasBlackout != other.hasBlackout) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + url.hashCode()
            result = 31 * result + hasBlackout.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 2
        }
    }

    data class HeroPlayersItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var name: String? = null,
        var url: String? = null,
        var wonCount: Int = 0,
        var wonCountPub: Int = 0,
        var lostCount: Int = 0,
        var lostCountPub: Int = 0,
        var hasBlackout: Boolean = false,
        var players: List<Pair<Long, String>> = listOf(),
    ) : ComplexItem() {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is HeroPlayersItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (name != other.name) return false
            if (url != other.url) return false
            if (wonCount != other.wonCount) return false
            if (lostCount != other.lostCount) return false
            if (players != other.players) return false
            if (wonCountPub != other.wonCountPub) return false
            if (lostCountPub != other.lostCountPub) return false
            if (hasBlackout != other.hasBlackout) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + name.hashCode()
            result = 31 * result + url.hashCode()
            result = 31 * result + wonCount.hashCode()
            result = 31 * result + lostCount.hashCode()
            result = 31 * result + players.hashCode()
            result = 31 * result + wonCountPub.hashCode()
            result = 31 * result + lostCountPub.hashCode()
            result = 31 * result + hasBlackout.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 3
        }
    }

    data class HeroRolesItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var url: String? = null,
        var title: String = "",
        var hasRole1: Boolean = false,
        var hasRole2: Boolean = false,
        var hasRole3: Boolean = false,
        var hasRole4: Boolean = false,
        var hasRole5: Boolean = false,
        var hasNoRoles: Boolean = false,
        var hasBlackout: Boolean = false,
    ) : ComplexItem() {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is HeroRolesItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (url != other.url) return false
            if (title != other.title) return false
            if (hasRole1 != other.hasRole1) return false
            if (hasRole2 != other.hasRole2) return false
            if (hasRole3 != other.hasRole3) return false
            if (hasRole4 != other.hasRole4) return false
            if (hasRole5 != other.hasRole5) return false
            if (hasNoRoles != other.hasNoRoles) return false
            if (hasBlackout != other.hasBlackout) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + url.hashCode()
            result = 31 * result + title.hashCode()
            result = 31 * result + hasRole1.hashCode()
            result = 31 * result + hasRole2.hashCode()
            result = 31 * result + hasRole3.hashCode()
            result = 31 * result + hasRole4.hashCode()
            result = 31 * result + hasRole5.hashCode()
            result = 31 * result + hasNoRoles.hashCode()
            result = 31 * result + hasBlackout.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 4
        }
    }

    data class HeroScoreItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var url: String? = null,
        var title: String = "",
        var kills: Int = 0,
        var deaths: Int = 0,
        var assists: Int = 0,
    ) : ComplexItem() {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is HeroScoreItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (url != other.url) return false
            if (title != other.title) return false
            if (kills != other.kills) return false
            if (assists != other.assists) return false
            if (deaths != other.deaths) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + url.hashCode()
            result = 31 * result + title.hashCode()
            result = 31 * result + kills.hashCode()
            result = 31 * result + assists.hashCode()
            result = 31 * result + deaths.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 5
        }
    }

    data class HeroCounterItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var url: String? = null,
        var title: String = "",
        var counter: Double = 0.0,
        var hasBlackout: Boolean = false
    ) : ComplexItem() {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is HeroCounterItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (url != other.url) return false
            if (title != other.title) return false
            if (counter != other.counter) return false
            if (hasBlackout != other.hasBlackout) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + url.hashCode()
            result = 31 * result + title.hashCode()
            result = 31 * result + counter.hashCode()
            result = 31 * result + hasBlackout.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 6
        }
    }

    data class HeroSynergyItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var url: String? = null,
        var title: String = "",
        var synergy: Double = 0.0,
        var hasBlackout: Boolean = false

    ) : ComplexItem() {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is HeroSynergyItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (url != other.url) return false
            if (title != other.title) return false
            if (synergy != other.synergy) return false
            if (hasBlackout != other.hasBlackout) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + url.hashCode()
            result = 31 * result + title.hashCode()
            result = 31 * result + synergy.hashCode()
            result = 31 * result + hasBlackout.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 7
        }
    }

    data class HeroResultItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var url: String? = null,
        var title: String = "",
        var synergy: Double = 0.0,
        var counter: Double = 0.0,
        var hasBlackout: Boolean = false
    ) : ComplexItem() {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is HeroResultItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (url != other.url) return false
            if (title != other.title) return false
            if (counter != other.counter) return false
            if (synergy != other.synergy) return false
            if (hasBlackout != other.hasBlackout) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + url.hashCode()
            result = 31 * result + title.hashCode()
            result = 31 * result + synergy.hashCode()
            result = 31 * result + counter.hashCode()
            result = 31 * result + hasBlackout.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 8
        }
    }

    data class HeroDraftItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var url: String? = null,
        var title: String = "",
        var synergy: Double = 0.0,
        var counter: Double = 0.0,
        var steal: Double = 0.0,
        var hasBlackout: Boolean = false

    ) : ComplexItem() {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is HeroDraftItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (url != other.url) return false
            if (title != other.title) return false
            if (counter != other.counter) return false
            if (synergy != other.synergy) return false
            if (steal != other.steal) return false
            if (hasBlackout != other.hasBlackout) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + url.hashCode()
            result = 31 * result + title.hashCode()
            result = 31 * result + synergy.hashCode()
            result = 31 * result + counter.hashCode()
            result = 31 * result + steal.hashCode()
            result = 31 * result + hasBlackout.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 9
        }
    }

    data class PickWinRateItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var title: String = "",
        var pickRate: Double = 0.0,
        var winRate: Double = 0.0,
        var hasBlackout: Boolean = false
    ) : ComplexItem() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is PickWinRateItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (pickRate != other.pickRate) return false
            if (title != other.title) return false
            if (winRate != other.winRate) return false
            if (hasBlackout != other.hasBlackout) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + title.hashCode()
            result = 31 * result + pickRate.hashCode()
            result = 31 * result + winRate.hashCode()
            result = 31 * result + hasBlackout.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 10
        }
    }

    data class SequenceItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var isCurrent: Boolean = false,
        var isPick: Boolean = false,
        var isRadiant: Boolean = false
    ) : ComplexItem() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is SequenceItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (isCurrent != other.isCurrent) return false
            if (isPick != other.isPick) return false
            if (isRadiant != other.isRadiant) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + isCurrent.hashCode()
            result = 31 * result + isPick.hashCode()
            result = 31 * result + isRadiant.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 11
        }
    }

    data class FilterItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var isSelected: Boolean = false,
        var title: String = "",
    ) : ComplexItem() {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is FilterItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (isSelected != other.isSelected) return false
            if (title != other.title) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + title.hashCode()
            result = 31 * result + isSelected.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 12
        }
    }

    data class LiveMatchItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var title: String = "",
        var radiantSeriesScore: Int = 0,
        var direSeriesScore: Int = 0,
        var radiantScore: Int = 0,
        var direScore: Int = 0,
        var durationReadable: String = "",
        var seriesType: String = "",
        var radiantName: String = "",
        var direName: String = "",
        var radiantHeroes: List<ComplexItem> = listOf(),
        var direHeroes: List<ComplexItem> = listOf(),
    ) : ComplexItem() {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is LiveMatchItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (radiantSeriesScore != other.radiantSeriesScore) return false
            if (direSeriesScore != other.direSeriesScore) return false
            if (radiantScore != other.radiantScore) return false
            if (direScore != other.direScore) return false
            if (durationReadable != other.durationReadable) return false
            if (seriesType != other.seriesType) return false
            if (radiantName != other.radiantName) return false
            if (direName != other.direName) return false
            if (radiantHeroes != other.radiantHeroes) return false
            if (direHeroes != other.direHeroes) return false
            if (title != other.title) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + title.hashCode()
            result = 31 * result + radiantSeriesScore.hashCode()
            result = 31 * result + direSeriesScore.hashCode()
            result = 31 * result + radiantScore.hashCode()
            result = 31 * result + direScore.hashCode()
            result = 31 * result + durationReadable.hashCode()
            result = 31 * result + seriesType.hashCode()
            result = 31 * result + radiantName.hashCode()
            result = 31 * result + direName.hashCode()
            result = 31 * result + radiantHeroes.hashCode()
            result = 31 * result + direHeroes.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 13
        }
    }

    data class PlayedMatchItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var time: String = "",
        var title: String = "",
        var radiantScore: Int = 0,
        var direScore: Int = 0,
        var durationReadable: String = "",
        var hasRadiantWon: Boolean = false,
        var wasPredictionRight: Boolean? = null,
        var radiantName: String = "",
        var direName: String = "",
        var radiantHeroes: List<ComplexItem> = listOf(),
        var direHeroes: List<ComplexItem> = listOf(),
    ) : ComplexItem() {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is PlayedMatchItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (radiantName != other.radiantName) return false
            if (direName != other.direName) return false
            if (radiantScore != other.radiantScore) return false
            if (hasRadiantWon != other.hasRadiantWon) return false
            if (direScore != other.direScore) return false
            if (durationReadable != other.durationReadable) return false
            if (wasPredictionRight != other.wasPredictionRight) return false
            if (radiantHeroes != other.radiantHeroes) return false
            if (direHeroes != other.direHeroes) return false
            if (title != other.title) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + title.hashCode()
            result = 31 * result + hasRadiantWon.hashCode()
            result = 31 * result + wasPredictionRight.hashCode()
            result = 31 * result + radiantScore.hashCode()
            result = 31 * result + direScore.hashCode()
            result = 31 * result + durationReadable.hashCode()
            result = 31 * result + radiantName.hashCode()
            result = 31 * result + direName.hashCode()
            result = 31 * result + radiantHeroes.hashCode()
            result = 31 * result + direHeroes.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 14
        }
    }

    data class MatchUpItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var heroes: List<ComplexItem> = listOf(),
    ) : ComplexItem() {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is MatchUpItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (heroes != other.heroes) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + heroes.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 16
        }
    }

    data class DraftHistoryItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var title: String = "",
        var radiantName: String = "",
        var direName: String = "",
        var radiantHeroes: List<ComplexItem> = listOf(),
        var direHeroes: List<ComplexItem> = listOf(),
    ) : ComplexItem() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is DraftHistoryItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (title != other.title) return false
            if (radiantName != other.radiantName) return false
            if (direName != other.direName) return false
            if (radiantHeroes != other.radiantHeroes) return false
            if (direHeroes != other.direHeroes) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + title.hashCode()
            result = 31 * result + radiantName.hashCode()
            result = 31 * result + direName.hashCode()
            result = 31 * result + radiantHeroes.hashCode()
            result = 31 * result + direHeroes.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 17
        }
    }

    data class ItemsListItem(
        override var key: String,
        override val subItemClick: Boolean = true,
        var title: String = "",
        var items: List<ComplexItem> = listOf(),
    ) : ComplexItem() {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is ItemsListItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (title != other.title) return false
            if (items != other.items) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + title.hashCode()
            result = 31 * result + items.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 18
        }
    }

    data class HeroGameStagesItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var url: String? = null,
        var title: String = "",
        var isEarlyGame: Boolean = false,
        var isMidGame: Boolean = false,
        var isLateGame: Boolean = false,
    ) : ComplexItem() {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is HeroGameStagesItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (title != other.title) return false
            if (url != other.url) return false
            if (isEarlyGame != other.isEarlyGame) return false
            if (isMidGame != other.isMidGame) return false
            if (isLateGame != other.isLateGame) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + title.hashCode()
            result = 31 * result + url.hashCode()
            result = 31 * result + isEarlyGame.hashCode()
            result = 31 * result + isMidGame.hashCode()
            result = 31 * result + isLateGame.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 19
        }
    }

    data class LeagueHistoryItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var name: String = "",
        var successCount: Int = 0,
        var failCount: Int = 0,
        var unpredictableCount: Int = 0
    ) : ComplexItem() {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is LeagueHistoryItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (name != other.name) return false
            if (successCount != other.successCount) return false
            if (failCount != other.failCount) return false
            if (unpredictableCount != other.unpredictableCount) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + name.hashCode()
            result = 31 * result + successCount.hashCode()
            result = 31 * result + failCount.hashCode()
            result = 31 * result + unpredictableCount.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 20
        }
    }

    data class HeroSimplePlayerItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var hasBlackout: Boolean = false,
        var url: String? = null,
        var name: String? = "",
    ) : ComplexItem() {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is HeroSimplePlayerItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (url != other.url) return false
            if (name != other.name) return false
            if (hasBlackout != other.hasBlackout) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + url.hashCode()
            result = 31 * result + name.hashCode()
            result = 31 * result + hasBlackout.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 21
        }
    }

    data class DraftStrategyItem(
        override var key: String,
        override val subItemClick: Boolean = true,
        var picks: List<ComplexItem> = listOf(),
        var bans: List<ComplexItem> = listOf(),
    ) : ComplexItem() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is DraftStrategyItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (picks != other.picks) return false
            if (bans != other.bans) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + picks.hashCode()
            result = 31 * result + bans.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 22
        }
    }

    data class DraftStageItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var heroName: String = "",
        var url: String = "",
        var hideImage: Boolean = false,
        var winRate: Double = 0.0,
        var pickRate: Double = 0.0,
        var matchCount: Int = 0,
        var hasBlackout: Boolean = false,
        var showMatchCount: Boolean = true
    ) : ComplexItem() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is DraftStageItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (heroName != other.heroName) return false
            if (url != other.url) return false
            if (hideImage != other.hideImage) return false
            if (winRate != other.winRate) return false
            if (pickRate != other.pickRate) return false
            if (matchCount != other.matchCount) return false
            if (hasBlackout != other.hasBlackout) return false
            if (showMatchCount != other.showMatchCount) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + heroName.hashCode()
            result = 31 * result + url.hashCode()
            result = 31 * result + winRate.hashCode()
            result = 31 * result + hideImage.hashCode()
            result = 31 * result + pickRate.hashCode()
            result = 31 * result + matchCount.hashCode()
            result = 31 * result + hasBlackout.hashCode()
            result = 31 * result + showMatchCount.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 23
        }
    }

    data class StatsCoefItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var heroName: String = "",
        var url: String = "",
        var value: String = "",
    ) : ComplexItem() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is StatsCoefItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (heroName != other.heroName) return false
            if (url != other.url) return false
            if (value != other.value) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + heroName.hashCode()
            result = 31 * result + url.hashCode()
            result = 31 * result + value.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 24
        }
    }

    data class StatsTournamentsItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var name: String = "",
        var matchCount: Int = 0,
        var isSelected: Boolean = true,
    ) : ComplexItem() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is StatsTournamentsItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (name != other.name) return false
            if (matchCount != other.matchCount) return false
            if (isSelected != other.isSelected) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + name.hashCode()
            result = 31 * result + matchCount.hashCode()
            result = 31 * result + isSelected.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 25
        }
    }

    data class DraftReasonItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var name: String = "",
    ) : ComplexItem() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is DraftReasonItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (name != other.name) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + name.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 26
        }
    }

    data class DraftAnalysisReasonItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var name: String = "",
        var radiantCount: Int = 0,
        var direCount: Int = 0,
        var hasBlackout: Boolean = false,
    ) : ComplexItem() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is DraftAnalysisReasonItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (name != other.name) return false
            if (radiantCount != other.radiantCount) return false
            if (direCount != other.direCount) return false
            if (hasBlackout != other.hasBlackout) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + name.hashCode()
            result = 31 * result + radiantCount.hashCode()
            result = 31 * result + direCount.hashCode()
            result = 31 * result + hasBlackout.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 27
        }
    }

    data class Stats2Item(
        override var key: String,
        override val subItemClick: Boolean = false,
        var heroName: String = "",
        var url: String = "",
        var value1: String = "",
        var value2: String = "",
        var hasBlackout: Boolean = false,
        var hasImage: Boolean = true,
    ) : ComplexItem() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is Stats2Item) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (heroName != other.heroName) return false
            if (url != other.url) return false
            if (value1 != other.value1) return false
            if (value2 != other.value2) return false
            if (hasBlackout != other.hasBlackout) return false
            if (hasImage != other.hasImage) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + heroName.hashCode()
            result = 31 * result + url.hashCode()
            result = 31 * result + value1.hashCode()
            result = 31 * result + value2.hashCode()
            result = 31 * result + hasBlackout.hashCode()
            result = 31 * result + hasImage.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 28
        }
    }

    data class Stats3Item(
        override var key: String,
        override val subItemClick: Boolean = false,
        var heroName: String = "",
        var url: String = "",
        var value1: String = "",
        var value2: String = "",
        var value3: String = "",
        var hasBlackout: Boolean = false,
        var hasImage: Boolean = true,
    ) : ComplexItem() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is Stats3Item) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (heroName != other.heroName) return false
            if (url != other.url) return false
            if (value1 != other.value1) return false
            if (value2 != other.value2) return false
            if (value3 != other.value3) return false
            if (hasBlackout != other.hasBlackout) return false
            if (hasImage != other.hasImage) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + heroName.hashCode()
            result = 31 * result + url.hashCode()
            result = 31 * result + value1.hashCode()
            result = 31 * result + value2.hashCode()
            result = 31 * result + value3.hashCode()
            result = 31 * result + hasBlackout.hashCode()
            result = 31 * result + hasImage.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 29
        }
    }

    data class Stats1Item(
        override var key: String,
        override val subItemClick: Boolean = false,
        var heroName: String = "",
        var url: String = "",
        var value1: String = "",
        var hasBlackout: Boolean = false,
        var hasImage: Boolean = true,
    ) : ComplexItem() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is Stats1Item) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (heroName != other.heroName) return false
            if (url != other.url) return false
            if (value1 != other.value1) return false
            if (hasBlackout != other.hasBlackout) return false
            if (hasImage != other.hasImage) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + heroName.hashCode()
            result = 31 * result + url.hashCode()
            result = 31 * result + value1.hashCode()
            result = 31 * result + hasBlackout.hashCode()
            result = 31 * result + hasImage.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 30
        }
    }

    data class GameDataItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var name: String = "",
        var radiantCount: Double = 0.0,
        var direCount: Double = 0.0,
        var hasBlackout: Boolean = false,
    ) : ComplexItem() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is GameDataItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (name != other.name) return false
            if (radiantCount != other.radiantCount) return false
            if (direCount != other.direCount) return false
            if (hasBlackout != other.hasBlackout) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + name.hashCode()
            result = 31 * result + radiantCount.hashCode()
            result = 31 * result + direCount.hashCode()
            result = 31 * result + hasBlackout.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 31
        }
    }

    data class RoleSelectionItem(
        override var key: String,
        override val subItemClick: Boolean = true,
        var name: String = "",
        var url: String = "",
        var roles: List<ComplexItem> = listOf()
    ) : ComplexItem() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is RoleSelectionItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (name != other.name) return false
            if (url != other.url) return false
            if (roles != other.roles) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + name.hashCode()
            result = 31 * result + url.hashCode()
            result = 31 * result + roles.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 32
        }
    }


    data class RoleItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var isSelected: Boolean = false,
        var title: String = "",
    ) : ComplexItem() {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is RoleItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (isSelected != other.isSelected) return false
            if (title != other.title) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + title.hashCode()
            result = 31 * result + isSelected.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 33
        }
    }


    data class Band2Item(
        override var key: String,
        override val subItemClick: Boolean = false,
        var heroName1: String = "",
        var url1: String = "",
        var heroName2: String = "",
        var url2: String = "",
        var winRate: Double = 0.0,
        var pickRate: Double = 0.0,
        var matchCount: Int = 0,
        var hasBlackout: Boolean = false,
    ) : ComplexItem() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is Band2Item) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (heroName1 != other.heroName1) return false
            if (url1 != other.url1) return false
            if (heroName2 != other.heroName2) return false
            if (url2 != other.url2) return false
            if (winRate != other.winRate) return false
            if (pickRate != other.pickRate) return false
            if (matchCount != other.matchCount) return false
            if (hasBlackout != other.hasBlackout) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + heroName1.hashCode()
            result = 31 * result + url1.hashCode()
            result = 31 * result + heroName2.hashCode()
            result = 31 * result + url2.hashCode()
            result = 31 * result + winRate.hashCode()
            result = 31 * result + pickRate.hashCode()
            result = 31 * result + matchCount.hashCode()
            result = 31 * result + hasBlackout.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 34
        }
    }


    data class Band3Item(
        override var key: String,
        override val subItemClick: Boolean = false,
        var heroName1: String = "",
        var url1: String = "",
        var heroName2: String = "",
        var url2: String = "",
        var heroName3: String = "",
        var url3: String = "",
        var winRate: Double = 0.0,
        var pickRate: Double = 0.0,
        var matchCount: Int = 0,
        var hasBlackout: Boolean = false,
    ) : ComplexItem() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is Band3Item) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (heroName1 != other.heroName1) return false
            if (url1 != other.url1) return false
            if (heroName2 != other.heroName2) return false
            if (url2 != other.url2) return false
            if (heroName3 != other.heroName3) return false
            if (url3 != other.url3) return false
            if (winRate != other.winRate) return false
            if (pickRate != other.pickRate) return false
            if (matchCount != other.matchCount) return false
            if (hasBlackout != other.hasBlackout) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + heroName1.hashCode()
            result = 31 * result + url1.hashCode()
            result = 31 * result + heroName2.hashCode()
            result = 31 * result + url2.hashCode()
            result = 31 * result + heroName3.hashCode()
            result = 31 * result + url3.hashCode()
            result = 31 * result + winRate.hashCode()
            result = 31 * result + pickRate.hashCode()
            result = 31 * result + matchCount.hashCode()
            result = 31 * result + hasBlackout.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 35
        }
    }

    data class Band4Item(
        override var key: String,
        override val subItemClick: Boolean = false,
        var heroName1: String = "",
        var url1: String = "",
        var heroName2: String = "",
        var url2: String = "",
        var heroName3: String = "",
        var url3: String = "",
        var heroName4: String = "",
        var url4: String = "",
        var winRate: Double = 0.0,
        var pickRate: Double = 0.0,
        var matchCount: Int = 0,
        var hasBlackout: Boolean = false,
    ) : ComplexItem() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is Band4Item) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (heroName1 != other.heroName1) return false
            if (url1 != other.url1) return false
            if (heroName2 != other.heroName3) return false
            if (url2 != other.url2) return false
            if (heroName3 != other.heroName3) return false
            if (url3 != other.url2) return false
            if (heroName4 != other.heroName4) return false
            if (url4 != other.url4) return false
            if (winRate != other.winRate) return false
            if (pickRate != other.pickRate) return false
            if (matchCount != other.matchCount) return false
            if (hasBlackout != other.hasBlackout) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + heroName1.hashCode()
            result = 31 * result + url1.hashCode()
            result = 31 * result + heroName2.hashCode()
            result = 31 * result + url2.hashCode()
            result = 31 * result + heroName3.hashCode()
            result = 31 * result + url3.hashCode()
            result = 31 * result + heroName4.hashCode()
            result = 31 * result + url4.hashCode()
            result = 31 * result + winRate.hashCode()
            result = 31 * result + pickRate.hashCode()
            result = 31 * result + matchCount.hashCode()
            result = 31 * result + hasBlackout.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 36
        }
    }

    data class CommonButtonItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var title: String = "",
    ) : ComplexItem() {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is CommonButtonItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (title != other.title) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + title.hashCode()
            return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 37
        }
    }

    data class PlayerLanesItem(
        override var key: String,
        override val subItemClick: Boolean = false,
        var playerName: String = "",
        var overall: Double = 0.0,
        var matchCount: Int = 0,
    ) : ComplexItem() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is PlayerLanesItem) return false
            if (key != other.key) return false
            if (subItemClick != other.subItemClick) return false
            if (playerName != other.playerName) return false
            if (overall != other.overall) return false
            if (matchCount != other.matchCount) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + subItemClick.hashCode()
            result = 31 * result + playerName.hashCode()
            result = 31 * result + overall.hashCode()
            result = 31 * result + matchCount.hashCode()
              return result
        }

        override val viewType: Int = TYPE

        companion object {
            const val TYPE = 38
        }
    }
}