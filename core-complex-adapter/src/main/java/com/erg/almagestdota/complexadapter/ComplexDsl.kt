package com.erg.almagestdota.complexadapter

/**
 * Пример использования:
 * val data: List<ComplexItem> = complexList {
 *     itemDefault {
 *         url = "https://......"
 *         title = "Legion Commander"
 *     }
 *     heroes.forEach { hero ->
 *         itemDefault {
 *              url = hero.url
 *              title = hero.name
 *         }
 *     }
 * }
 * recyclerViewAdapter.submitList(data)
 *
 * Каждому элементу можно указать ключ в аргументах функции либо внутри лямбды:
 * itemDefault(key = hero.id.toString()) {
 *     ...
 * }
 * itemDefault {
 *     key = item.id.toString()
 * }
 *
 * Это может быть полезно, если нужно использовать идентификатор элемента
 * для каких-либо операций (например для открытия нового экрана или выбора героя).
 *
 * По-умолчанию в качестве ключа будет использоваться порядковый номер элемента в списке.
 */
inline fun complexList(block: ComplexDsl.() -> Unit): List<ComplexItem> {
    val complexDsl = ComplexDsl()
    block(complexDsl)
    return complexDsl.build()
}

inline fun ComplexDsl.itemDefault(
    key: String = "ItemDefault-" + size(),
    block: ComplexItem.ItemDefault.() -> Unit = {},
) = add(ComplexItem.ItemDefault(key).apply(block))

inline fun ComplexDsl.commonButtonItem(
    key: String = "CommonButtonItem-" + size(),
    block: ComplexItem.CommonButtonItem.() -> Unit = {},
) = add(ComplexItem.CommonButtonItem(key).apply(block))

inline fun ComplexDsl.heroBanItem(
    key: String = "HeroBanItem-" + size(),
    block: ComplexItem.HeroBanItem.() -> Unit = {},
) = add(ComplexItem.HeroBanItem(key).apply(block))

inline fun ComplexDsl.band2Item(
    key: String = "Band2Item-" + size(),
    block: ComplexItem.Band2Item.() -> Unit = {},
) = add(ComplexItem.Band2Item(key).apply(block))

inline fun ComplexDsl.band3Item(
    key: String = "Band3Item-" + size(),
    block: ComplexItem.Band3Item.() -> Unit = {},
) = add(ComplexItem.Band3Item(key).apply(block))

inline fun ComplexDsl.band4Item(
    key: String = "Band4Item-" + size(),
    block: ComplexItem.Band4Item.() -> Unit = {},
) = add(ComplexItem.Band4Item(key).apply(block))

inline fun ComplexDsl.heroStatsItem(
    key: String = "HeroStatsItem-" + size(),
    block: ComplexItem.HeroStatsItem.() -> Unit = {},
) = add(ComplexItem.HeroStatsItem(key).apply(block))

inline fun ComplexDsl.draftReasonItem(
    key: String = "DraftReasonItem-" + size(),
    block: ComplexItem.DraftReasonItem.() -> Unit = {},
) = add(ComplexItem.DraftReasonItem(key).apply(block))

inline fun ComplexDsl.heroRolesItem(
    key: String = "HeroRolesItem-" + size(),
    block: ComplexItem.HeroRolesItem.() -> Unit = {},
) = add(ComplexItem.HeroRolesItem(key).apply(block))

inline fun ComplexDsl.stats1Item(
    key: String = "Stats1Item-" + size(),
    block: ComplexItem.Stats1Item.() -> Unit = {},
) = add(ComplexItem.Stats1Item(key).apply(block))

inline fun ComplexDsl.stats2Item(
    key: String = "Stats2Item-" + size(),
    block: ComplexItem.Stats2Item.() -> Unit = {},
) = add(ComplexItem.Stats2Item(key).apply(block))

inline fun ComplexDsl.roleSelectionItem(
    key: String = "RoleSelectionItem-" + size(),
    block: ComplexItem.RoleSelectionItem.() -> Unit = {},
) = add(ComplexItem.RoleSelectionItem(key).apply(block))

inline fun ComplexDsl.stats3Item(
    key: String = "Stats3Item-" + size(),
    block: ComplexItem.Stats3Item.() -> Unit = {},
) = add(ComplexItem.Stats3Item(key).apply(block))

inline fun ComplexDsl.heroPlayersItem(
    key: String = "HeroPlayersItem-" + size(),
    block: ComplexItem.HeroPlayersItem.() -> Unit = {},
) = add(ComplexItem.HeroPlayersItem(key).apply(block))

inline fun ComplexDsl.heroSimplePlayerItem(
    key: String = "HeroSimplePlayerItem-" + size(),
    block: ComplexItem.HeroSimplePlayerItem.() -> Unit = {},
) = add(ComplexItem.HeroSimplePlayerItem(key).apply(block))

inline fun ComplexDsl.draftStrategyItem(
    key: String = "DraftStrategyItem-" + size(),
    block: ComplexItem.DraftStrategyItem.() -> Unit = {},
) = add(ComplexItem.DraftStrategyItem(key).apply(block))

inline fun ComplexDsl.heroScoreItem(
    key: String = "HeroScoreItem-" + size(),
    block: ComplexItem.HeroScoreItem.() -> Unit = {},
) = add(ComplexItem.HeroScoreItem(key).apply(block))

inline fun ComplexDsl.heroCounterItem(
    key: String = "HeroCounterItem-" + size(),
    block: ComplexItem.HeroCounterItem.() -> Unit = {},
) = add(ComplexItem.HeroCounterItem(key).apply(block))

inline fun ComplexDsl.heroSynergyItem(
    key: String = "HeroSynergyItem-" + size(),
    block: ComplexItem.HeroSynergyItem.() -> Unit = {},
) = add(ComplexItem.HeroSynergyItem(key).apply(block))

inline fun ComplexDsl.heroResultItem(
    key: String = "HeroResultItem-" + size(),
    block: ComplexItem.HeroResultItem.() -> Unit = {},
) = add(ComplexItem.HeroResultItem(key).apply(block))

inline fun ComplexDsl.heroGameStagesItem(
    key: String = "HeroGameStagesItem-" + size(),
    block: ComplexItem.HeroGameStagesItem.() -> Unit = {},
) = add(ComplexItem.HeroGameStagesItem(key).apply(block))

inline fun ComplexDsl.heroDraftItem(
    key: String = "HeroDraftItem-" + size(),
    block: ComplexItem.HeroDraftItem.() -> Unit = {},
) = add(ComplexItem.HeroDraftItem(key).apply(block))

inline fun ComplexDsl.pickWinRateItem(
    key: String = "PickWinRateItem-" + size(),
    block: ComplexItem.PickWinRateItem.() -> Unit = {},
) = add(ComplexItem.PickWinRateItem(key).apply(block))

inline fun ComplexDsl.sequenceItem(
    key: String = "SequenceItem-" + size(),
    block: ComplexItem.SequenceItem.() -> Unit = {},
) = add(ComplexItem.SequenceItem(key).apply(block))

inline fun ComplexDsl.filterItem(
    key: String = "FilterItem-" + size(),
    block: ComplexItem.FilterItem.() -> Unit = {},
) = add(ComplexItem.FilterItem(key).apply(block))

inline fun ComplexDsl.draftAnalysisReasonItem(
    key: String = "DraftAnalysisReasonItem-" + size(),
    block: ComplexItem.DraftAnalysisReasonItem.() -> Unit = {},
) = add(ComplexItem.DraftAnalysisReasonItem(key).apply(block))

inline fun ComplexDsl.gameDataItem(
    key: String = "GameDataItem-" + size(),
    block: ComplexItem.GameDataItem.() -> Unit = {},
) = add(ComplexItem.GameDataItem(key).apply(block))

inline fun ComplexDsl.draftStageItem(
    key: String = "DraftStageItem-" + size(),
    block: ComplexItem.DraftStageItem.() -> Unit = {},
) = add(ComplexItem.DraftStageItem(key).apply(block))

inline fun ComplexDsl.playerLanesItem(
    key: String = "PlayerLanesItem-" + size(),
    block: ComplexItem.PlayerLanesItem.() -> Unit = {},
) = add(ComplexItem.PlayerLanesItem(key).apply(block))

inline fun ComplexDsl.statsTournamentsItem(
    key: String = "StatsTournamentsItem-" + size(),
    block: ComplexItem.StatsTournamentsItem.() -> Unit = {},
) = add(ComplexItem.StatsTournamentsItem(key).apply(block))

inline fun ComplexDsl.statsCoefItem(
    key: String = "StatsCoefItem-" + size(),
    block: ComplexItem.StatsCoefItem.() -> Unit = {},
) = add(ComplexItem.StatsCoefItem(key).apply(block))

inline fun ComplexDsl.liveMatchItem(
    key: String = "LiveMatchItem-" + size(),
    block: ComplexItem.LiveMatchItem.() -> Unit = {},
) = add(ComplexItem.LiveMatchItem(key).apply(block))

inline fun ComplexDsl.playedMatchItem(
    key: String = "PlayedMatchItem-" + size(),
    block: ComplexItem.PlayedMatchItem.() -> Unit = {},
) = add(ComplexItem.PlayedMatchItem(key).apply(block))

inline fun ComplexDsl.leagueHistoryItem(
    key: String = "LeagueHistoryItem-" + size(),
    block: ComplexItem.LeagueHistoryItem.() -> Unit = {},
) = add(ComplexItem.LeagueHistoryItem(key).apply(block))

inline fun ComplexDsl.matchUpItem(
    key: String = "MatchUpItem-" + size(),
    block: ComplexItem.MatchUpItem.() -> Unit = {},
) = add(ComplexItem.MatchUpItem(key).apply(block))

inline fun ComplexDsl.draftHistoryItem(
    key: String = "DraftHistoryItem-" + size(),
    block: ComplexItem.DraftHistoryItem.() -> Unit = {},
) = add(ComplexItem.DraftHistoryItem(key).apply(block))

inline fun ComplexDsl.itemsListItem(
    key: String = "ItemsListItem-" + size(),
    block: ComplexItem.ItemsListItem.() -> Unit = {},
) = add(ComplexItem.ItemsListItem(key).apply(block))

class ComplexDsl {

    private val complexItems = mutableListOf<ComplexItem>()

    fun add(complexItem: ComplexItem): Boolean {
        return complexItems.add(complexItem)
    }

    fun size(): Int {
        return complexItems.size
    }

    fun build(): List<ComplexItem> {
        return complexItems
    }
}