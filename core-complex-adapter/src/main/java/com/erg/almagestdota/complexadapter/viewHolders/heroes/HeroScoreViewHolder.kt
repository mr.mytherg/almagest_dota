package com.erg.almagestdota.complexadapter.viewHolders.heroes

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.erg.almagestdota.base.external.image.loadImageByUrl
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.R
import com.erg.almagestdota.complexadapter.databinding.HeroItemBinding

class HeroScoreViewHolder(
    private val binding: HeroItemBinding,
) : ComplexViewHolder<ComplexItem.HeroScoreItem>(binding.root) {

    companion object {
        fun create(parent: ViewGroup): HeroScoreViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = HeroItemBinding.inflate(inflater, parent, false)
            return HeroScoreViewHolder(binding)
        }
    }

    override fun bind(item: ComplexItem.HeroScoreItem) = with(binding) {
        tvName.text = item.title
        tvName.isVisible = true
        ivHero.loadImageByUrl(item.url, R.drawable.ic_placeholder)
        ivHero.alpha = 1.0f

        tvRole1.isVisible = true
        tvRole1.text = root.context.getString(R.string.hero_score, item.kills, item.deaths, item.assists)

        tvSteal.isVisible = false
        tvSynergy.isVisible = false
        tvCounter.isVisible = false

        llPro.isVisible = false
        llPub.isVisible = false
        tvRole2.isVisible = false
        tvRole3.isVisible = false
        tvRole4.isVisible = false
        tvRole5.isVisible = false
    }
}