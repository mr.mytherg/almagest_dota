package com.erg.almagestdota.complexadapter.viewHolders.heroes

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import com.erg.almagestdota.base.external.image.loadImageByUrl
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.R
import com.erg.almagestdota.complexadapter.databinding.HeroItemBinding

class HeroRolesViewHolder(
    private val binding: HeroItemBinding,
) : ComplexViewHolder<ComplexItem.HeroRolesItem>(binding.root) {

    companion object {
        fun create(parent: ViewGroup): HeroRolesViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = HeroItemBinding.inflate(inflater, parent, false)
            return HeroRolesViewHolder(binding)
        }
    }

    override fun bind(item: ComplexItem.HeroRolesItem) = with(binding) {
        tvName.isVisible = true
        tvName.text = item.title
        ivHero.loadImageByUrl(item.url, R.drawable.ic_placeholder)
        ivHero.alpha = if (item.hasBlackout) 0.4f else 1.0f


        llPro.isVisible = false
        llPub.isVisible = false

        tvSteal.isVisible = false
        tvSynergy.isVisible = false
        tvCounter.isVisible = false

        if (item.hasNoRoles) {
            tvRole1.isInvisible = false
            tvRole1.text = root.context.getString(R.string.hero_role_unknown)

            tvRole2.isInvisible = true
            tvRole3.isInvisible = true
            tvRole4.isInvisible = true
            tvRole5.isInvisible = true
        } else {
            tvRole1.isInvisible = !item.hasRole1
            tvRole2.isInvisible = !item.hasRole2
            tvRole3.isInvisible = !item.hasRole3
            tvRole4.isInvisible = !item.hasRole4
            tvRole5.isInvisible = !item.hasRole5

            tvRole1.text = root.context.getString(R.string.role_1)
            tvRole2.text = root.context.getString(R.string.role_2)
            tvRole3.text = root.context.getString(R.string.role_3)
            tvRole4.text = root.context.getString(R.string.role_4)
            tvRole5.text = root.context.getString(R.string.role_5)
        }
    }
}