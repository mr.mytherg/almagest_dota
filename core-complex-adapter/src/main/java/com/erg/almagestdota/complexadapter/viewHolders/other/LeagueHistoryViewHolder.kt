package com.erg.almagestdota.complexadapter.viewHolders.other

import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexViewHolder
import com.erg.almagestdota.complexadapter.R
import com.erg.almagestdota.complexadapter.databinding.ItemLeagueHistoryBinding
import com.erg.almagestdota.complexadapter.databinding.ItemSequenceBinding

class LeagueHistoryViewHolder(
    private val binding: ItemLeagueHistoryBinding,
) : ComplexViewHolder<ComplexItem.LeagueHistoryItem>(binding.root) {

    companion object {
        fun create(parent: ViewGroup): LeagueHistoryViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemLeagueHistoryBinding.inflate(inflater, parent, false)
            return LeagueHistoryViewHolder(binding)
        }
    }

    private val context = binding.root.context

    override fun bind(item: ComplexItem.LeagueHistoryItem) = with(binding) {
        tvUnpredictable.text = context.getString(R.string.predictions_league_unpredictable, item.unpredictableCount)
        tvSuccess.text = context.getString(R.string.predictions_league_success, item.successCount)
        tvFail.text = context.getString(R.string.predictions_league_fail, item.failCount)
        tvTitle.text = item.name
    }
}