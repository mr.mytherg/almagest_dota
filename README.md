# Almagest Dota 2 Picker

## Описание

Проект создан с большой любовью к доте и программированию. Это будет третья итерация с попыткой сделать этот проект лучше.

## Поддержка
Если имеются вопросы или хотите помочь mr.mytherg@mail.ru

## Дорожная карта
Раздел по статистике professional и dpc матчей. Статистика отдельной команды, турнира и героя. Возможно каждого игрока.

Раздел про просмотру матчей в реальном времени по агрегатору стима

Раздел по драфту для All Pick, Captain's Mode и кастомных режимов (с собственной очередью пика и количеством героев/банов).
Раздел будет включать в себя расширенную статистику героев, советы по драфту, возможность соревноваться в искусстве драфта с ботом или товарищем на одном устройстве (может быть нескольких).

Раздел по статистике героев. А-ля легкий дотабафф.

Раздел с мини-играми.

## License

Copyright 2022, Aynur Fatkullin
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
