package com.erg.almagestdota.local_storage.external.models.main_settings

import android.os.Parcelable
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.local_storage.internal.data.models.main_settings.UserHeroesPoolRealmModel
import com.erg.almagestdota.local_storage.internal.helpers.realm.essentialMapRealmModel
import kotlinx.android.parcel.Parcelize
import javax.inject.Inject

@Parcelize
data class UserHeroesPool(
    val filters: List<UserHeroesPoolFilter>,
) : Parcelable {
    internal class MapperToUserHeroesPoolRealmModel @Inject constructor(
        private val mapperToUserHeroesPoolFilterRealmModel: UserHeroesPoolFilter.MapperToUserHeroesPoolFilterRealmModel,
    ) : EssentialMapper<UserHeroesPool, UserHeroesPoolRealmModel>() {
        override fun transform(raw: UserHeroesPool): UserHeroesPoolRealmModel {
            return UserHeroesPoolRealmModel().apply {
                filters = raw.filters.essentialMapRealmModel(mapperToUserHeroesPoolFilterRealmModel)
            }
        }
    }
}