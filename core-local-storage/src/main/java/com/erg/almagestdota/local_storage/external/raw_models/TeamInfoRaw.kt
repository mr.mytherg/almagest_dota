package com.erg.almagestdota.local_storage.external.raw_models

import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamInfo

class TeamInfoRaw(
    var id: Long = 0L,
    var name: String = "",
    var rating: Int = 1400,
    val is322: Boolean = false,
    val imageLink: String = "",
    var matchCount: Int = 0,
    var members: List<Long> = listOf(),
) {
    companion object {
        fun toModel(model: TeamInfoRaw?): TeamInfo {
            return TeamInfo(
                id = model?.id.orDefault(),
                name = model?.name.orDefault(),
                rating = model?.rating.orDefault(),
                is322 = model?.is322.orDefault(),
                imageLink = model?.imageLink.orDefault(),
                matchCount = model?.matchCount.orDefault(),
                members = model?.members.orEmpty(),
            )
        }
    }
}