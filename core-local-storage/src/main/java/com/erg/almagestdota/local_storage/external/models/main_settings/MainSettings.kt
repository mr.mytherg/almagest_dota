package com.erg.almagestdota.local_storage.external.models.main_settings

import android.os.Parcelable
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.local_storage.external.models.draft.DotaItem
import com.erg.almagestdota.local_storage.external.models.draft.HeroItems
import com.erg.almagestdota.local_storage.external.models.draft.HeroStatsData
import com.erg.almagestdota.local_storage.external.models.draft.HeroValues
import com.erg.almagestdota.local_storage.external.models.draft.HeroesItems
import com.erg.almagestdota.local_storage.external.models.draft.HeroesValues
import com.erg.almagestdota.local_storage.external.raw_models.MainSettingsRaw
import com.erg.almagestdota.local_storage.internal.data.models.main_settings.MainSettingsRealmModel
import com.erg.almagestdota.local_storage.internal.helpers.realm.essentialMapRealmModel
import com.google.gson.Gson
import kotlinx.android.parcel.Parcelize
import javax.inject.Inject

/**
 * @param sequenceCM - draft sequence: 0 - user hero, 1 - enemy hero, 2 - user ban, 3 - enemy ban
 */

@Parcelize
data class MainSettings(
    val heroes: List<HeroStatsData>,
    val heroesItems: MutableMap<Int, HeroItems>,
    var items: List<DotaItem>,
    val sequenceCM: String,
    val sequenceAP: String,
    val patch: String,
    val isActualVersion: Boolean,
) : Parcelable {

    fun getPickSizeCM(): Int = sequenceCM.count { it == '0' }
    fun getBanSizeCM(): Int = sequenceCM.count { it == '2' }

    internal class MapperToMainSettingsRealmModel @Inject constructor(
        private val gson: Gson,
    ) : EssentialMapper<MainSettings, MainSettingsRealmModel>() {
        override fun transform(raw: MainSettings): MainSettingsRealmModel {
            return MainSettingsRealmModel().apply {
                json = gson.toJson(raw)
            }
        }
    }

    companion object {
        fun toModel(raw: MainSettings): MainSettingsRaw {
            val mutableHeroItemsMap = mutableMapOf<String, String>()
            for ((heroId, items) in raw.heroesItems)
                mutableHeroItemsMap[heroId.toString()] = encodeItems(items)
              return MainSettingsRaw(
                heroes = raw.heroes.map { HeroStatsData.toModel(it) },
                items = raw.items.map { DotaItem.toModel(it) },
                sequence_cm = raw.sequenceCM,
                sequence_ap = raw.sequenceAP,
                patch = raw.patch,
                actual_version = raw.isActualVersion,
                hero_items = mutableHeroItemsMap,
            )
        }
    }
}

private fun encodeItems(items: HeroItems): String {
    var counterItems = ""
    items.againstHero.forEachIndexed { index, item ->
        if (counterItems.isNotEmpty()) {
            counterItems += ","
        }
        counterItems += item.id
    }
    var heroItems = ""
    items.main.plus(items.situational).forEachIndexed { index, item ->
        if (heroItems.isNotEmpty()) {
            heroItems += ","
        }
        heroItems += item.id
    }
    var earlyItems = ""
    items.early.forEachIndexed { index, item ->
        if (earlyItems.isNotEmpty()) {
            earlyItems += ","
        }
        earlyItems += item.id
    }
    var neutrals = ""
    items.neutralTier1.plus(items.neutralTier2).plus(items.neutralTier3).plus(items.neutralTier4).plus(items.neutralTier5).forEachIndexed { index, item ->
        if (neutrals.isNotEmpty()) {
            neutrals += ","
        }
        neutrals += item.id
    }
    return "$counterItems#$heroItems#$earlyItems#$neutrals"
}