package com.erg.almagestdota.local_storage.external.models

import android.os.Parcelable
import com.erg.almagestdota.local_storage.external.models.draft.HeroDraftData
import com.erg.almagestdota.local_storage.external.models.draft.main.DraftModel
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroPlayerData
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DraftAnalysis(
    val stage: Int,
    val selectedHeroId: Int,
    val heroesReasons: List<HeroDraftData>,
    val heroesPlayers: List<HeroPlayerData>,
    val draftModel: DraftModel
) : Parcelable