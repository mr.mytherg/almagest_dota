package com.erg.almagestdota.local_storage.external.models.draft

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 *
 * every list descending by frequency
 * @param main - 4 main items
 * @param early - 4 early items
 * @param team - team items among main, situational and early
 * @param situational - 16 items
 * @param againstHero - items that counters hero
 * @param neutralTier1 - 8 top neutral TIER 1 items
 * @param neutralTier2 - 8 top neutral TIER 2 items
 * @param neutralTier3 - 8 top neutral TIER 3 items
 * @param neutralTier4 - 8 top neutral TIER 4 items
 * @param neutralTier5 - 8 top neutral TIER 5 items
 */

@Parcelize
data class HeroItems(
    var main: List<DotaItem>,
    var situational: List<DotaItem>,
    var team: List<DotaItem> = listOf(),
    var early: List<DotaItem>,
    var againstHero: List<DotaItem>,
    var neutralTier1: List<DotaItem>,
    var neutralTier2: List<DotaItem>,
    var neutralTier3: List<DotaItem>,
    var neutralTier4: List<DotaItem>,
    var neutralTier5: List<DotaItem>,
) : Parcelable