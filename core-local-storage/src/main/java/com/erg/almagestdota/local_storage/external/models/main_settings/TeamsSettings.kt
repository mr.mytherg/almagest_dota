package com.erg.almagestdota.local_storage.external.models.main_settings

import android.os.Parcelable
import com.erg.almagestdota.local_storage.external.models.draft.HeroValues
import com.erg.almagestdota.local_storage.external.models.draft.main.MemberInfo
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamInfo
import com.erg.almagestdota.local_storage.external.raw_models.ImportantSettingsRaw
import com.erg.almagestdota.local_storage.external.raw_models.PlayersSettingsRaw
import com.erg.almagestdota.local_storage.external.raw_models.TeamInfoRaw
import com.erg.almagestdota.local_storage.external.raw_models.TeamSettingsRaw
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TeamsSettings(
    var teams: List<TeamInfo> = listOf(),
) : Parcelable {

    companion object {
        fun toModel(raw: TeamsSettings): TeamSettingsRaw {
            return TeamSettingsRaw(
                teams = raw.teams.map { TeamInfo.toModel(it) },
            )
        }
    }
}