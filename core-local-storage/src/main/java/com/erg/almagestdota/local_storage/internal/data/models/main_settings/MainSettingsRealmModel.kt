package com.erg.almagestdota.local_storage.internal.data.models.main_settings

import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.local_storage.external.models.draft.HeroesItems
import com.erg.almagestdota.local_storage.external.models.draft.HeroesValues
import com.erg.almagestdota.local_storage.external.models.main_settings.MainSettings
import com.erg.almagestdota.local_storage.internal.data.models.draft.DotaItemRealmModel
import com.erg.almagestdota.local_storage.internal.data.models.draft.HeroStatsDataRealmModel
import com.google.gson.Gson
import io.realm.FieldAttribute
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.RealmSchema
import io.realm.annotations.PrimaryKey
import java.util.*
import javax.inject.Inject

internal open class MainSettingsRealmModel : RealmObject() {

    @PrimaryKey
    var primaryKey: String = UUID.randomUUID().toString()
    var json: String = ""

    internal class Mapper @Inject constructor(
        private val gson: Gson,
    ) : EssentialMapper<MainSettingsRealmModel, MainSettings>() {
        override fun transform(raw: MainSettingsRealmModel): MainSettings {
            return gson.fromJson(raw.json, MainSettings::class.java)
        }
    }

    companion object {
        const val MODEL_NAME = "MainSettingsRealmModel"
        fun createModel(schema: RealmSchema) {
            if (schema[MODEL_NAME] != null) {
                return
            }
            val model = schema.create(MODEL_NAME)
            model?.apply {
                addField("primaryKey", String::class.java, FieldAttribute.PRIMARY_KEY)
                addField("json", String::class.java)
            }
        }
    }
}