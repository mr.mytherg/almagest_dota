package com.erg.almagestdota.local_storage.external.models.draft.main

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class LaneStatusType(val type: String, val stratzType: String) : Parcelable {
    RADIANT_STOMP("R_S", "RADIANT_STOMP"),
    RADIANT_WIN("R_W", "RADIANT_VICTORY"),
    DIRE_STOMP("D_S", "DIRE_STOMP"),
    DIRE_WIN("D_W", "DIRE_VICTORY"),
    UNDEFINED("U", "UNDEFINED"),
    TIE("T", "TIE");

    companion object {
        fun byLaneStatusType(value: String?): LaneStatusType {
            return values().firstOrNull() { it.type == value } ?: UNDEFINED
        }
        fun byLaneStatusTypeStratz(value: String?): LaneStatusType {
            return values().firstOrNull() { it.stratzType == value } ?: UNDEFINED
        }
    }
}