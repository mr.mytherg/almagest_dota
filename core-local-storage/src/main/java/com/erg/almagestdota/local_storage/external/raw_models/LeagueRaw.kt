package com.erg.almagestdota.local_storage.external.raw_models

import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.local_storage.external.models.leagues.League
import com.erg.almagestdota.local_storage.external.models.leagues.LeagueTierType
import javax.inject.Inject

class LeagueRaw(
    val id: Long = 0,
    val tier: String = "",
    val name: String = ""
) {
    class MapperToLeague @Inject constructor() : EssentialMapper<LeagueRaw, League>() {
        override fun transform(raw: LeagueRaw): League {
            return League(
                id = raw.id,
                tier = LeagueTierType.byName(raw.tier),
                name = raw.name,
            )
        }
    }
}