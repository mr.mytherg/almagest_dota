package com.erg.almagestdota.local_storage.external.raw_models

import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.local_storage.external.models.draft.HeroStage
import com.erg.almagestdota.local_storage.external.models.draft.StageType
import javax.inject.Inject

class HeroStageRaw(
    val stage: String = "",
    val pickRate: Double = 0.0,
    val winRate: Double = 0.0,
    val matchCount: Int = 0,
) {
    companion object {
        fun toModel(raw: HeroStageRaw?): HeroStage {
            return HeroStage(
                stage = StageType.byName(raw?.stage.orDefault()),
                pickRate = raw?.pickRate.orDefault(),
                winRate = raw?.winRate.orDefault(),
                matchCount = raw?.matchCount.orDefault(),
            )
        }
    }
}