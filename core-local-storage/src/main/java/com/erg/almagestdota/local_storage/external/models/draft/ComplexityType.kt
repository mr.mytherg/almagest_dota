package com.erg.almagestdota.local_storage.external.models.draft

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class ComplexityType(val complexity: String) : Parcelable {
    SIMPLE("1"),
    MEDIUM("2"),
    HARD("3");

    companion object {
        fun byComplexity(value: String): ComplexityType {
            return values().firstOrNull() { it.complexity == value } ?: SIMPLE
        }
    }
}