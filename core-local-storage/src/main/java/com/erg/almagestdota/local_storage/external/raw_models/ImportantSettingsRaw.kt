package com.erg.almagestdota.local_storage.external.raw_models

import com.erg.almagestdota.local_storage.external.models.main_settings.ImportantSettings

class ImportantSettingsRaw(
    val lastDocument: Int? = 1000,
    val firstDocument: Int? = 1000,
    val heroesValues: Map<String, String> = mapOf(),
    val heroesProValues: Map<String, String> = mapOf(),
) {
    companion object {
        fun toModel(raw: ImportantSettingsRaw): ImportantSettings {
            return ImportantSettings(
                lastDocument = raw.lastDocument ?: 1000,
                firstDocument = raw.firstDocument ?: 1000,
                heroesValues = raw.heroesValues.getHeroesValues(),
                heroesProValues = raw.heroesProValues.getHeroesValues(),
            )
        }
    }
}