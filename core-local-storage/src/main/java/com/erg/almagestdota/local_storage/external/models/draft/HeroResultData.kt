package com.erg.almagestdota.local_storage.external.models.draft

import android.os.Parcelable
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * @param synergyValue - how much hero is good with the allies
 * @param counterValue - how much hero is good against the enemies
 */

@Parcelize
open class HeroResultData(
    @SerializedName("HeroResultData_id") override val id: Int,
    @SerializedName("HeroResultData_name") override val name: String,
    @SerializedName("HeroResultData_imageLink") override val imageLink: String,
    @SerializedName("HeroResultData_synergyValue") open val synergyValue: Double = 0.0,
    @SerializedName("HeroResultData_counterValue") open val counterValue: Double = 0.0,
) : HeroCoreData(id, name, imageLink), Parcelable {

    companion object {
        fun createEmpty(hero: HeroCoreData): HeroResultData {
            return HeroResultData(hero.id, hero.name, hero.imageLink, 0.0, 0.0)
        }
    }

    open fun getSum(): Double {
        return synergyValue + counterValue
    }
}