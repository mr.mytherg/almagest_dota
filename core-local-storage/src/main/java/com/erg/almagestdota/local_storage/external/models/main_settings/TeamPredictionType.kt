package com.erg.almagestdota.local_storage.external.models.main_settings

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class TeamPredictionType() : Parcelable {
    P50_100,
    P100_150,
    P150_200,
    P200_250,
    P250_300,
    P300;
}