package com.erg.almagestdota.local_storage.external.models.draft

import android.os.Parcelable
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData
import com.erg.almagestdota.local_storage.external.raw_models.HeroStatsDataRaw
import com.erg.almagestdota.local_storage.internal.data.models.draft.HeroStatsDataRealmModel
import com.erg.almagestdota.local_storage.internal.helpers.realm.essentialMapRealmModel
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import javax.inject.Inject

/**
 * @param team - radiant = true / dire = false
 */

@Parcelize
data class HeroStatsData(
    @SerializedName("HeroStatsData_id") override val id: Int,
    @SerializedName("HeroStatsData_name") override val name: String,
    @SerializedName("HeroStatsData_imageLink") override val imageLink: String,
    var magicalDamagePercentage: Double,
    var pureDamagePercentage: Double,
    var physicalDamagePercentage: Double,
    var towerDamage: Double,
    var healing: Double,

    var laning: List<GameStageData>,
    var early: List<GameStageData>,
    var mid: List<GameStageData>,
    var late: List<GameStageData>,

    val attackType: AttackType,
    val primaryAttribute: AttributeType,
    val complexity: ComplexityType,
    val team: Boolean,
    val hasBlink: Boolean,
    val hasBkb: Boolean,
    var roles: List<HeroRole>,
    var factions: List<FactionData>,
    var stages: List<HeroStage>,
) : HeroCoreData(id, name, imageLink), Parcelable {

    companion object {
        fun toModel(model: HeroStatsData): HeroStatsDataRaw {
            return HeroStatsDataRaw(
                id = model.id,
                image = model.imageLink,
                name = model.name,
                magicalDamagePercentage = model.magicalDamagePercentage,
                pureDamagePercentage = model.pureDamagePercentage,
                towerDamage = model.towerDamage,
                healing = model.healing,

                laning = model.laning.map { GameStageData.toModel(it) },
                early = model.early.map { GameStageData.toModel(it) },
                mid = model.mid.map { GameStageData.toModel(it) },
                late = model.late.map { GameStageData.toModel(it) },

                hasBlink = model.hasBlink,
                hasBkb = model.hasBkb,
                physicalDamagePercentage = model.physicalDamagePercentage,
                attackType = model.attackType.type,
                primaryAttribute = model.primaryAttribute.attr,
                complexity = model.complexity.complexity,
                team = model.team,
                roles = model.roles.map { HeroRole.toModel(it) },
                factions = model.factions.map { FactionData.toModel(it) },
                stages = model.stages.map { HeroStage.toModel(it) },
            )
        }
    }
}