package com.erg.almagestdota.local_storage.internal.helpers.shared_preferences

internal interface IConverter<T> {

    fun deserialize(serialized: String): T

    fun serialize(value: T): String
}