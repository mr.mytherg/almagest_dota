package com.erg.almagestdota.local_storage.internal.data

import com.erg.almagestdota.local_storage.external.ISharedPreferencesContract
import javax.inject.Inject

internal class SharedPreferencesRepository @Inject constructor(
    private val sharedPreferencesApi: SharedPreferencesApi
) : ISharedPreferencesContract {

    override fun getDataExpirationTimestamp(): Long {
        return sharedPreferencesApi.dataExpirationTimestamp.value
    }

    override fun getLastReviewTimestamp(): Long {
        return sharedPreferencesApi.lastReviewTimestamp.value
    }

    override fun setLastReviewTimestamp(timestamp: Long) {
        sharedPreferencesApi.lastReviewTimestamp.value = timestamp
    }

    override fun setDataExpirationTimestamp(timestamp: Long) {
        sharedPreferencesApi.dataExpirationTimestamp.value = timestamp
    }
}