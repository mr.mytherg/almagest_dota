package com.erg.almagestdota.local_storage.external.models.draft.main

import android.os.Parcelable
import com.erg.almagestdota.local_storage.external.models.draft.GameModeType
import com.erg.almagestdota.local_storage.external.models.draft.MonthType
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DraftModel(
    var monthType: MonthType = MonthType.MONTH3,
    var needToLoadPubMatches: Boolean = false,
    var isDireBot: Boolean = false,
    var isAllPick: Boolean = false,
    var gameMode: GameModeType = GameModeType.TEAM,
    var sequence: String = "",
    val radiantTeam: TeamDraft = TeamDraft(),
    val direTeam: TeamDraft = TeamDraft(),
) : Parcelable