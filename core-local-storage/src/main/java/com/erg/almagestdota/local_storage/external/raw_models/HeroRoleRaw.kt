package com.erg.almagestdota.local_storage.external.raw_models

import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.local_storage.external.models.draft.*
import javax.inject.Inject

class HeroRoleRaw(
    val role: String = "",
    val frequency_percentage: Double = 0.0,
    val win_rate_percentage: Double = 0.0,
    val matchCount: Int = 0,
) {
    companion object {

        fun toModel(raw: HeroRoleRaw?): HeroRole {
            return HeroRole(
                roleType = RoleType.byName(raw?.role.orDefault()),
                pickRate = raw?.frequency_percentage.orDefault(),
                winRatePercentage = raw?.win_rate_percentage.orDefault(),
                matchCount = raw?.matchCount.orDefault(),
            )
        }
    }

}