package com.erg.almagestdota.local_storage.external.models.draft.main

import android.os.Parcelable
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TeamDraft(
    val picks: MutableList<PlayerInfo> = mutableListOf(),
    val bans: MutableList<Int> = mutableListOf(),
    var teamInfo: TeamInfo = TeamInfo(),
) : Parcelable {
    var heroPool: MutableMap<Int, Map<Long, PlayerStats>> = mutableMapOf()
    var heroPoolPub: MutableMap<Long, MutableMap<Int, Pair<Int, Int>>> = mutableMapOf()

    private fun getPlayerMatches(selectedPlayerId: Long, metaMatches: List<DotaMatch>): List<DotaMatch> {
        if (selectedPlayerId == 0L) return listOf()
        return metaMatches.filter {
            it.radiantHeroes.find { it.playerId == selectedPlayerId } != null ||
                it.direHeroes.find { it.playerId == selectedPlayerId } != null
        }
    }

    fun setHeroPool(metaMatches: List<DotaMatch>) {
        val poolMap = mutableMapOf<Int, MutableMap<Long, PlayerStats>>()

        for (member in teamInfo.members) {
            val playerMatches = getPlayerMatches(member, metaMatches)
            for (match in playerMatches) {
                val isPlayerRadiant = match.radiantHeroes.find { it.playerId == member } != null
                if (isPlayerRadiant) {
                    addInfo(match.radiantHeroes.find { it.playerId == member }, poolMap, match.hasRadiantWon)
                } else {
                    addInfo(match.direHeroes.find { it.playerId == member }, poolMap, !match.hasRadiantWon)
                }
            }
            val heroPool = heroPoolPub[member] ?: continue
            for ((heroId, info) in heroPool) {
                val heroMap = poolMap[heroId] ?: mutableMapOf()
                val player = heroMap[member] ?: PlayerStats.createEmptyPlayer(member, teamInfo.members)

                heroMap[member] = player.apply {
                    wonCountPub = info.first
                    lostCountPub = info.second
                }
                poolMap[heroId] = heroMap
            }
        }

        for ((heroId, players) in poolMap) {
            val map: MutableMap<Long, PlayerStats> = mutableMapOf()
            for ((playerId, stats) in players) {
                if (stats.wonCount + stats.lostCount > 0 || stats.wonCountPub + stats.lostCountPub >= 10)
                    map[playerId] = stats
            }
            heroPool[heroId] = map
        }
//        heroPool = poolMap
    }

    private fun addInfo(
        playerInMatch: PlayerInfo?,
        poolMap: MutableMap<Int, MutableMap<Long, PlayerStats>>,
        hasTeamWon: Boolean
    ) {
        if (playerInMatch == null) {
            return
        }
        val heroId = playerInMatch.heroId
        val heroMap = poolMap[heroId] ?: mutableMapOf()
        val player = heroMap[playerInMatch.playerId] ?: PlayerStats.createEmptyPlayer(playerInMatch.playerId, teamInfo.members)
        if (hasTeamWon) {
            player.wonCount++
        } else {
            player.lostCount++
        }
        heroMap[playerInMatch.playerId] = player
        poolMap[heroId] = heroMap
    }

    fun hasHeroInPool(heroId: Int): Boolean {
        return heroPool[heroId] != null
    }
}