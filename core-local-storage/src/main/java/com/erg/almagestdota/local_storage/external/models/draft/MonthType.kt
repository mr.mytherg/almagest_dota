package com.erg.almagestdota.local_storage.external.models.draft

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class MonthType(val monthCount: Int) : Parcelable {
    MONTH1(1),
    MONTH3(3),
    MONTH6(6),
    MONTH12(12);
}