package com.erg.almagestdota.local_storage.external

interface ISharedPreferencesContract {
    fun getDataExpirationTimestamp(): Long
    fun getLastReviewTimestamp(): Long
    fun setLastReviewTimestamp(timestamp: Long)

    fun setDataExpirationTimestamp(timestamp: Long)
}