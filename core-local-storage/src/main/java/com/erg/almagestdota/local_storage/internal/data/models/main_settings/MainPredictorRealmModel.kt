package com.erg.almagestdota.local_storage.internal.data.models.main_settings

import io.realm.FieldAttribute
import io.realm.RealmObject
import io.realm.RealmSchema
import io.realm.annotations.PrimaryKey
import java.util.*

internal open class MainPredictorRealmModel : RealmObject() {

    @PrimaryKey
    var primaryKey: String = UUID.randomUUID().toString()
    var heroesProValuesEarly: String = ""
    var heroesProValuesMid: String = ""
    var heroesProValuesLate: String = ""
    var probabilities: String = ""
    var teamProbabilities: String = ""
    var teamRatings: String = ""

    companion object {
        const val MODEL_NAME = "MainPredictorRealmModel"
        fun createModel(schema: RealmSchema) {
            if (schema[MODEL_NAME] != null) {
                return
            }
            val model = schema.create(MODEL_NAME)
            model?.apply {
                addField("primaryKey", String::class.java, FieldAttribute.PRIMARY_KEY)
                addField("heroesProValuesEarly", String::class.java)
                addField("heroesProValuesMid", String::class.java)
                addField("heroesProValuesLate", String::class.java)
                addField("probabilities", String::class.java)
                addField("teamProbabilities", String::class.java)
                addField("teamRatings", String::class.java)
            }
        }
    }
}