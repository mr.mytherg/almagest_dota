package com.erg.almagestdota.local_storage.external.models.draft

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class AttackType(val type: String) : Parcelable {
    MELEE("Melee"),
    RANGED("Ranged");

    companion object {
        fun byAttackType(value: String): AttackType {
            return values().firstOrNull() { it.type == value } ?: MELEE
        }
    }
}