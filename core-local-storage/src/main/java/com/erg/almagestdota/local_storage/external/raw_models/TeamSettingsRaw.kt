package com.erg.almagestdota.local_storage.external.raw_models

import com.erg.almagestdota.local_storage.external.models.main_settings.ImportantSettings
import com.erg.almagestdota.local_storage.external.models.main_settings.PlayersSettings
import com.erg.almagestdota.local_storage.external.models.main_settings.TeamsSettings

class TeamSettingsRaw(
    var teams: List<TeamInfoRaw> = listOf(),
) {
    companion object {
        fun toModel(raw: TeamSettingsRaw): TeamsSettings {
            return TeamsSettings(
                teams = raw.teams.map { TeamInfoRaw.toModel(it) },
            )
        }
    }
}