package com.erg.almagestdota.local_storage.external.raw_models

import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.local_storage.external.models.draft.DotaItem
import com.erg.almagestdota.local_storage.external.models.draft.HeroItems
import com.erg.almagestdota.local_storage.external.models.draft.HeroValues
import com.erg.almagestdota.local_storage.external.models.main_settings.MainSettings
import javax.inject.Inject

data class MainSettingsRaw(
    val heroes: List<HeroStatsDataRaw> = listOf(),
    val items: List<DotaItemRaw> = listOf(),
    val sequence_cm: String = "",
    val sequence_ap: String = "",
    val patch: String = "",
    val actual_version: Boolean = true,
    val hero_items: Map<String, String> = mapOf()
) {
    companion object {
        fun toModel(raw: MainSettingsRaw?): MainSettings {
            val items = raw?.items.orEmpty().map { DotaItemRaw.toModel(it) }
            val heroes = raw?.heroes.orEmpty().map { HeroStatsDataRaw.toModel(it) }
            val heroesItems = getHeroesItems(raw?.hero_items.orEmpty(), items)
            return MainSettings(
                heroes = heroes,
                sequenceCM = raw?.sequence_cm.orDefault(),
                sequenceAP = raw?.sequence_ap.orDefault(),
                patch = raw?.patch.orDefault(),
                isActualVersion = raw?.actual_version.orDefault(),
                heroesItems = heroesItems.toMutableMap(),
                items = items,
            )
        }
    }
}

private fun getHeroesItems(heroesItems: Map<String, String>, items: List<DotaItem>): Map<Int, HeroItems> {
    val mutableMap = mutableMapOf<Int, HeroItems>()
    val neutralWithAnalogs = items.filter { it.neutralTier != 0 && it.analogs.isNotEmpty() }
    for ((heroId, itemsEncoded) in heroesItems)
        mutableMap[heroId.toInt()] = getHeroItems(itemsEncoded, items, neutralWithAnalogs)
    return mutableMap
}

private fun getHeroItems(itemsEncoded: String, items: List<DotaItem>, neutralWithAnalogs: List<DotaItem>): HeroItems {
    var indexDelimiter = itemsEncoded.indexOf("#")
    val againstHeroEncodedItems = itemsEncoded.substring(0, indexDelimiter)
    var lastEncodedItems = itemsEncoded.substring(indexDelimiter + 1)
    indexDelimiter = lastEncodedItems.indexOf("#")
    val onHeroEncodedItems = lastEncodedItems.substring(0, indexDelimiter)
    lastEncodedItems = lastEncodedItems.substring(indexDelimiter + 1)
    indexDelimiter = lastEncodedItems.indexOf("#")
    val earlyEncodedItems = lastEncodedItems.substring(0, indexDelimiter)
    val neutralItemsEncoded = lastEncodedItems.substring(indexDelimiter + 1)

    val heroItems = parseHeroItems(onHeroEncodedItems, mutableListOf(), items)
    val earlyItems = parseHeroItems(earlyEncodedItems, mutableListOf(), items)
    val neutralItems = parseHeroItems(neutralItemsEncoded, mutableListOf(), items)
    val teamItems = mutableListOf<DotaItem>()
    teamItems.addAll(heroItems.filter { it.team })
    teamItems.addAll(earlyItems.filter { it.team })

    val againstHeroItems = parseHeroItems(againstHeroEncodedItems, mutableListOf(), items)
    return HeroItems(
        againstHero = againstHeroItems,
        main = heroItems.take(4),
        situational = heroItems.takeLast(16),
        early = earlyItems,
        neutralTier1 = neutralItems.filter { it.neutralTier == 1 },
        neutralTier2 = neutralItems.filter { it.neutralTier == 2 },
        neutralTier3 = neutralItems.filter { it.neutralTier == 3 },
        neutralTier4 = neutralItems.filter { it.neutralTier == 4 },
        neutralTier5 = neutralItems.filter { it.neutralTier == 5 },
        team = teamItems
    )
}

private fun parseHeroItems(encodedItems: String, decodedItems: MutableList<DotaItem>, items: List<DotaItem>): List<DotaItem> {
    val delimiterIndex = encodedItems.indexOf(",")
    if (delimiterIndex == -1) {
        val itemId = encodedItems.toInt()
        val item = items.find { it.id == itemId }
        if (item != null) {
            decodedItems.add(item)
        }
        return decodedItems
    }
    val itemId = encodedItems.substring(0, delimiterIndex).toInt()
    val item = items.find { it.id == itemId }
    if (item != null) {
        decodedItems.add(item)
    }
    return parseHeroItems(encodedItems.substring(delimiterIndex + 1), decodedItems, items)
}

fun Map<String, String>.getHeroesValues(): MutableMap<Int, HeroValues> {
    val mutableMap = mutableMapOf<Int, HeroValues>()
    for ((heroId, valuesEncoded) in this)
        mutableMap[heroId.toInt()] = getHeroValues(valuesEncoded)
    return mutableMap
}

private fun getHeroValues(valuesEncoded: String): HeroValues {
    val synergiesEncoded = valuesEncoded.substring(0, valuesEncoded.indexOf("#"))
    val countersEncoded = valuesEncoded.substring(valuesEncoded.indexOf("#") + 1)

    return HeroValues(
        synergies = parseHeroValues(synergiesEncoded, mutableMapOf()),
        counters = parseHeroValues(countersEncoded, mutableMapOf()),
    )
}

private fun parseHeroValues(valuesEncoded: String, decodedValues: MutableMap<Int, Double>): MutableMap<Int, Double> {
    val delimiterIndex = valuesEncoded.indexOf(",")
    if (delimiterIndex == -1) {
        val heroIdValueDelimiter = valuesEncoded.indexOf("=")
        val heroId = valuesEncoded.substring(0, heroIdValueDelimiter).toInt()
        val heroValue = valuesEncoded.substring(heroIdValueDelimiter + 1).toDouble().round2()
        decodedValues[heroId] = heroValue
        return decodedValues
    }
    val heroIdValue = valuesEncoded.substring(0, delimiterIndex)
    val heroIdValueDelimiter = heroIdValue.indexOf("=")
    val heroId = heroIdValue.substring(0, heroIdValueDelimiter).toInt()
    val heroValue = heroIdValue.substring(heroIdValueDelimiter + 1).toDouble().round2()
    decodedValues[heroId] = heroValue
    return parseHeroValues(valuesEncoded.substring(delimiterIndex + 1), decodedValues)
}