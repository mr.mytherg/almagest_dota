package com.erg.almagestdota.local_storage.external.models.leagues

import android.os.Parcelable
import androidx.annotation.StringRes
import com.erg.almagestdota.local_storage.R
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class LeagueTierType(val leagueName: String) : Parcelable {
    PREMIUM("premium"),
    PROFESSIONAL("professional"),
    AMATEUR("amateur"),
    UNKNOWN("unknown");

    companion object {
        fun byName(value: String): LeagueTierType {
            return values().firstOrNull { it.leagueName == value } ?: AMATEUR
        }

        @StringRes
        fun getStringId(type: LeagueTierType): Int {
            return when (type) {
                PREMIUM -> R.string.match_tier_1
                PROFESSIONAL -> R.string.match_tier_2
                AMATEUR -> R.string.match_tier_3
                else -> R.string.match_tier_unknown
            }
        }
    }
}