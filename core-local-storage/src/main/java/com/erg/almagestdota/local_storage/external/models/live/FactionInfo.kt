package com.erg.almagestdota.local_storage.external.models.live

import android.os.Parcelable
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FactionInfo(
    val score: Int = 0,
    val towerState: Long = 0L,
    val barracksState: Long = 0L,
    var picks: List<HeroCoreData> = listOf(),
    val bans: List<HeroCoreData> = listOf(),
    val players: List<SteamPlayerInfo> = listOf(),
) : Parcelable