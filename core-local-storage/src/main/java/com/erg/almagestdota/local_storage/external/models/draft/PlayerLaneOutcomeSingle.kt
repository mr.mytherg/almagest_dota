package com.erg.almagestdota.local_storage.external.models.draft

import android.os.Parcelable
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.local_storage.external.models.draft.main.LaneOutcomeType
import com.erg.almagestdota.local_storage.external.models.draft.main.LaneStatusType
import com.erg.almagestdota.local_storage.external.raw_models.HeroRoleRaw
import com.erg.almagestdota.local_storage.internal.data.models.draft.HeroRoleRealmModel
import kotlinx.android.parcel.Parcelize
import javax.inject.Inject

@Parcelize
data class PlayerLaneOutcomeSingle(
    val id: Long,
    val statusType: LaneOutcomeType,
    var count: Int = 0,
    var matchPercentage: Double = 0.0,
) : Parcelable
