package com.erg.almagestdota.local_storage.external

import com.erg.almagestdota.local_storage.external.models.DraftAnalysis
import com.erg.almagestdota.local_storage.external.models.draft.DraftHistoryItem
import com.erg.almagestdota.local_storage.external.models.draft.DraftReasonType
import com.erg.almagestdota.local_storage.external.models.draft.main.DraftModel
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamInfo
import com.erg.almagestdota.local_storage.external.models.leagues.League
import com.erg.almagestdota.local_storage.external.models.main_settings.MainPredictor
import com.erg.almagestdota.local_storage.external.models.main_settings.MainSettings
import com.erg.almagestdota.local_storage.external.models.main_settings.UserHeroesPool
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.local_storage.internal.data.models.draft.GameStagesTeamData
import kotlinx.coroutines.flow.StateFlow

interface ILocalStorageContract {
    var draftModel: DraftModel?
    var selectedTeam: TeamInfo
    var analysis: List<DraftAnalysis>
    var reasons: List<DraftReasonType>
    var gameStagesData: GameStagesTeamData
    var metaMatches: List<DotaMatch>
    var selectedMatches: List<DotaMatch>
    var matchesToShow: List<DotaMatch>

    fun getDraftHistoryStateFlow(): StateFlow<List<DraftHistoryItem>?>

    fun getLeagues(): List<League>

    fun setLeagues(leagues: List<League>)

    suspend fun initGlobalMainSettings(action: suspend () -> Unit)

    fun getMainSettings(): MainSettings?

    fun setMainSettings(mainSettings: MainSettings)
    fun setMainPredictor(mainPredictor: MainPredictor)

     fun loadDraftHistory()

    fun addItemToDraftHistory(draftItem: DraftHistoryItem)

    @Deprecated("Use filters repository")
    fun getUserHeroesPool(): UserHeroesPool

    @Deprecated("Use filters repository")
    fun setUserHeroesPool(userHeroesPool: UserHeroesPool)
}