package com.erg.almagestdota.local_storage.internal.helpers.realm

import com.erg.almagestdota.local_storage.internal.data.models.draft.*
import com.erg.almagestdota.local_storage.internal.data.models.draft.DotaItemRealmModel
import com.erg.almagestdota.local_storage.internal.data.models.draft.HeroRoleRealmModel
import com.erg.almagestdota.local_storage.internal.data.models.draft.HeroStageRealmModel
import com.erg.almagestdota.local_storage.internal.data.models.leagues.LeagueRealmModel
import com.erg.almagestdota.local_storage.internal.data.models.main_settings.*
import com.erg.almagestdota.local_storage.internal.data.models.main_settings.HeroCoreDataRealmModel
import com.erg.almagestdota.local_storage.internal.data.models.main_settings.MainSettingsRealmModel
import com.erg.almagestdota.local_storage.internal.data.models.main_settings.UserHeroesPoolFilterRealmModel
import com.erg.almagestdota.local_storage.internal.data.models.main_settings.UserHeroesPoolRealmModel
import io.realm.DynamicRealm
import io.realm.RealmMigration

internal class RealmMigration : RealmMigration {
    override fun migrate(realm: DynamicRealm, _oldVersion: Long, newVersion: Long) {
        var oldVersion = _oldVersion
        val schema = realm.schema

        if (oldVersion == 0L) {
            HeroCoreDataRealmModel.createModel(schema)
            LeagueRealmModel.createModel(schema)
            MainSettingsRealmModel.createModel(schema)
            UserHeroesPoolRealmModel.createModel(schema)
            UserHeroesPoolFilterRealmModel.createModel(schema)
            HeroRoleRealmModel.createModel(schema)
            HeroStageRealmModel.createModel(schema)
            DotaItemRealmModel.createModel(schema)
            oldVersion++
        }
        if (oldVersion == 1L) {
            oldVersion++
        }
        if (oldVersion == 2L) {
            oldVersion++
        }
        if (oldVersion == 3L) {
            oldVersion++
        }
        if (oldVersion == 4L) {
            MainPredictorRealmModelV4.createModel(schema)
            oldVersion++
        }
        if (oldVersion == 5L) {
            DraftRealmModelV3.createModel(schema)
            oldVersion++
        }
    }
}