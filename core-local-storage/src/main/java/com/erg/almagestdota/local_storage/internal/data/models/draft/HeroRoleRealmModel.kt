package com.erg.almagestdota.local_storage.internal.data.models.draft

import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.local_storage.external.models.draft.HeroRole
import com.erg.almagestdota.local_storage.external.models.draft.RoleType
import com.google.gson.Gson
import io.realm.FieldAttribute
import io.realm.RealmObject
import io.realm.RealmSchema
import io.realm.annotations.PrimaryKey
import java.util.*
import javax.inject.Inject

internal open class HeroRoleRealmModel : RealmObject() {
    @PrimaryKey
    var primaryKey: String = UUID.randomUUID().toString()
    var json: String = ""

    class Mapper @Inject constructor(
        private val gson: Gson
    ) : EssentialMapper<HeroRoleRealmModel, HeroRole>() {
        override fun transform(raw: HeroRoleRealmModel): HeroRole {
            return gson.fromJson(raw.json, HeroRole::class.java)
        }
    }

    companion object {
        const val MODEL_NAME = "HeroRoleRealmModel"
        fun createModel(schema: RealmSchema) {
            if (schema[MODEL_NAME] != null) {
                return
            }
            val model = schema.create(MODEL_NAME)
            model?.apply {
                addField("primaryKey", String::class.java, FieldAttribute.PRIMARY_KEY)
                addField("json", String::class.java)
            }
        }
    }
}