package com.erg.almagestdota.local_storage.external.models.draft

import android.os.Parcelable
import com.erg.almagestdota.base.external.extensions.round2
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * @param synergyValue - how much hero is good with the allies
 * @param counterValue - how much hero is good against the enemies
 * @param stealValue - how much hero fits for both of teams. You should steal such heroes.
 * @param avgCounter - how much enemy team can counter this hero in the next pick. Considering other picked heroes.
 */

@Parcelize
data class HeroDraftData(
    @SerializedName("HeroDraftData_id") override val id: Int,
    @SerializedName("HeroDraftData_name") override val name: String,
    @SerializedName("HeroDraftData_imageLink") override val imageLink: String,
    @SerializedName("HeroDraftData_synergyValue") override val synergyValue: Double = 0.0,
    @SerializedName("HeroDraftData_counterValue") override val counterValue: Double = 0.0,
    @SerializedName("HeroDraftData_stealValue") val stealValue: Double = 0.0,
    @SerializedName("HeroDraftData_avgCounter") val avgCounter: Double = 0.0,
    @SerializedName("HeroDraftData_reasons") val reasons: MutableList<DraftReasonType> = mutableListOf(),
) : HeroResultData(id, name, imageLink, synergyValue, counterValue), Parcelable {

    var isCurrent = false

    companion object {
        fun createNullHero(): HeroDraftData {
            return HeroDraftData(
                id = -1,
                name = "",
                imageLink = "",
            )
        }

        fun isNullHero(hero: HeroDraftData): Boolean {
            return hero.id == -1
        }
    }

    override fun getSum(): Double {
        return (synergyValue + counterValue + stealValue + avgCounter).round2()
    }
}