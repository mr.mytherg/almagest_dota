package com.erg.almagestdota.local_storage.external.models.draft

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class HeroValues(
    var synergies: MutableMap<Int, Double>,
    var counters: MutableMap<Int, Double>
) : Parcelable {

    fun encodeValues(): String {
        var synergies = ""
        this.synergies.forEach {
            if (synergies.isNotEmpty()) {
                synergies += ","
            }
            synergies += "${it.key}=${it.value}"
        }
        var counters = ""
        this.counters.forEach {
            if (counters.isNotEmpty()) {
                counters += ","
            }
            counters += "${it.key}=${it.value}"
        }
        return "$synergies#$counters"
    }

    companion object {
        fun copy(heroesValues: MutableMap<Int, HeroValues>): MutableMap<Int, HeroValues> {
            val mutableMap = mutableMapOf<Int, HeroValues>()
            for (heroValues in heroesValues) {
                val countersMap = mutableMapOf<Int, Double>()
                val synergiesMap = mutableMapOf<Int, Double>()
                for (counter in heroValues.value.counters)
                    countersMap[counter.key] = counter.value
                for (synergy in heroValues.value.synergies)
                    synergiesMap[synergy.key] = synergy.value

                mutableMap[heroValues.key] = HeroValues(
                    synergies = synergiesMap,
                    counters = countersMap,
                )
            }
            return mutableMap
        }
    }
}