package com.erg.almagestdota.local_storage.external.models.leagues

fun List<League>.findLeagueByBinaryMethod(id: Long, startIndex: Int = 0, endIndex: Int = this.lastIndex): League {
    val size = endIndex + startIndex
    val currentIndex = size / 2
    if (this.isEmpty()) {
        return League(tier = LeagueTierType.UNKNOWN, name = "", id = 0L)
    }
    val item = this[currentIndex]
    if (id == item.id) {
        return item
    } else if (startIndex >= endIndex) {
        return League(tier = LeagueTierType.UNKNOWN, name = "", id = 0L)
    } else if (id > item.id) {
        return findLeagueByBinaryMethod(id, currentIndex + 1, endIndex)
    } else if (id < item.id) {
        return findLeagueByBinaryMethod(id, startIndex, currentIndex - 1)
    } else {
        return League(tier = LeagueTierType.UNKNOWN, name = "", id = 0L)
    }
}