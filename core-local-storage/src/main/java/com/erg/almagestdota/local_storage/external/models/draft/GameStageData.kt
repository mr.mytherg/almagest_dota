package com.erg.almagestdota.local_storage.external.models.draft

import android.os.Parcelable
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData
import com.erg.almagestdota.local_storage.external.raw_models.HeroStatsDataRaw
import com.erg.almagestdota.local_storage.internal.data.models.draft.HeroStatsDataRealmModel
import com.erg.almagestdota.local_storage.internal.helpers.realm.essentialMapRealmModel
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import javax.inject.Inject

@Parcelize
data class GameStageData(
    var kills: Double,
    var deaths: Double,
    var assists: Double,
    var magicalDamage: Double,
    var position: RoleType,
    var cs: Double,
    var dn: Double,
    var neutrals: Double,
    var heroDamage: Double,
    var towerDamage: Double,
    var damageReceived: Double,
    var healingAllies: Double,
    var disableCount: Double,
    var disableDuration: Double,
    var stunCount: Double,
    var stunDuration: Double,
) : Parcelable {

    companion object {
        fun createEmpty(): GameStageData {
            return GameStageData(
                kills = 0.0,
                deaths = 0.0,
                assists = 0.0,
                position = RoleType.UNKNOWN,
                cs = 0.0,
                dn = 0.0,
                neutrals = 0.0,
                magicalDamage = 0.0,
                heroDamage = 0.0,
                towerDamage = 0.0,
                damageReceived = 0.0,
                healingAllies = 0.0,
                disableCount = 0.0,
                disableDuration = 0.0,
                stunCount = 0.0,
                stunDuration = 0.0,
            )
        }

        fun toModel(model: GameStageData): GameStageDataRaw {
            return GameStageDataRaw(
                kills = model.kills,
                deaths = model.deaths,
                assists = model.assists,
                position = model.position.name,
                cs = model.cs,
                dn = model.dn,
                neutrals = model.neutrals,
                magicalDamage = model.magicalDamage,
                heroDamage = model.heroDamage,
                towerDamage = model.towerDamage,
                damageReceived = model.damageReceived,
                healingAllies = model.healingAllies,
                disableCount = model.disableCount,
                disableDuration = model.disableDuration,
                stunCount = model.stunCount,
                stunDuration = model.stunDuration,
            )
        }
    }
}