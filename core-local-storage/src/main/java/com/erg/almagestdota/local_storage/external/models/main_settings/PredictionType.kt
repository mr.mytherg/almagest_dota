package com.erg.almagestdota.local_storage.external.models.main_settings

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class PredictionType() : Parcelable {
    P5_10,
    P10_15,
    P15_20,
    P20_25,
    P25_30,
    P30;
}