package com.erg.almagestdota.local_storage.external.models.main_settings

import android.os.Parcelable
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamInfo
import com.erg.almagestdota.local_storage.external.raw_models.MainPredictorRaw
import com.erg.almagestdota.local_storage.external.raw_models.TeamInfoRaw
import com.erg.almagestdota.local_storage.internal.data.models.main_settings.MainPredictorRealmModelV4
import com.google.gson.Gson
import kotlinx.android.parcel.Parcelize
import javax.inject.Inject

@Parcelize
data class MainPredictor(
    val disabled: Boolean,
    var teamProbabilities: MutableMap<String, Double>,
    var draftProbabilities: MutableMap<String, Double>,
) : Parcelable {

    internal class MapperToMainPredictorRealmModel @Inject constructor(
        private val gson: Gson,
    ) : EssentialMapper<MainPredictor, MainPredictorRealmModelV4>() {
        override fun transform(raw: MainPredictor): MainPredictorRealmModelV4 {
            return MainPredictorRealmModelV4().apply {
                json = gson.toJson(raw)
            }
        }
    }

    companion object {
        fun toModel(model: MainPredictor): MainPredictorRaw {
            return MainPredictorRaw(
                disabled = model.disabled,
                teamProbabilities = model.teamProbabilities,
                draftProbabilities = model.draftProbabilities,
            )
        }
    }
}