package com.erg.almagestdota.local_storage.external.models.main_settings

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
open class HeroPlayerData(
    @SerializedName("HeroPlayerData_id") override val id: Int,
    @SerializedName("HeroPlayerData_name") override val name: String,
    @SerializedName("HeroPlayerData_imageLink") override val imageLink: String,
    @SerializedName("HeroPlayerData_heroPlayers") open var heroPlayers: Set<Pair<Long, String>>? = null,
    @SerializedName("HeroPlayerData_wonCount") open var wonCount: Int,
    @SerializedName("HeroPlayerData_lostCount") open var lostCount: Int,
    @SerializedName("HeroPlayerData_wonCount") open var wonCountPub: Int,
    @SerializedName("HeroPlayerData_lostCount") open var lostCountPub: Int,

) : HeroCoreData(id, name, imageLink), Parcelable {
    companion object {
        fun createNullHero(): HeroPlayerData {
            return HeroPlayerData(
                id = -1,
                "",
                "",
                null,
                0,
                0,
                0,
                0
            )
        }
    }
}