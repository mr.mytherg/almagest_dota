package com.erg.almagestdota.local_storage.external.models.main_settings

import com.erg.almagestdota.local_storage.external.models.draft.HeroStatsData
import com.erg.almagestdota.local_storage.external.models.draft.main.MemberInfo
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamInfo
import com.erg.almagestdota.local_storage.external.models.leagues.League

object GlobalVars {
    const val CALIBRATION_MATCH_COUNT = 20
    lateinit var mainSettings: MainSettings
    lateinit var mainPredictor: MainPredictor
    lateinit var importantSettings: ImportantSettings
    lateinit var leagues: List<League>
    lateinit var players: MutableMap<Long, MemberInfo>
    lateinit var teams: MutableMap<Long, TeamInfo>
    var heroesAsMap: Map<Int, HeroStatsData> = mapOf()
    var groups: List<DraftFilterGroup> = listOf()

    const val RADIANT_PICK = '0'
    const val DIRE_PICK = '1'
    const val RADIANT_BAN = '2'
    const val DIRE_BAN = '3'

    fun initPlayers(players: List<MemberInfo>) {
        val mutableMap = mutableMapOf<Long, MemberInfo>()
        for (player in players) {
            mutableMap[player.playerId] = player
        }
        this.players = mutableMap
    }

    fun initTeams(teams: List<TeamInfo>) {
        val mutableMap = mutableMapOf<Long, TeamInfo>()
        for (team in teams) {
            mutableMap[team.id] = team
        }
        this.teams = mutableMap
    }

    fun getAllIds(): String {
        var ids = ""
        for (hero in mainSettings.heroes) {
            if (ids.isNotEmpty()) {
                ids += ","
            }
            ids += hero.id
        }
        return ids
    }
}