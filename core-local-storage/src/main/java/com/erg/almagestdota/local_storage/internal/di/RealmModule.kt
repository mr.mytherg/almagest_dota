package com.erg.almagestdota.local_storage.internal.di

import android.content.Context
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.DraftHistoryItem
import com.erg.almagestdota.local_storage.external.models.leagues.League
import com.erg.almagestdota.local_storage.external.models.main_settings.MainPredictor
import com.erg.almagestdota.local_storage.external.models.main_settings.MainSettings
import com.erg.almagestdota.local_storage.external.models.main_settings.UserHeroesPool
import com.erg.almagestdota.local_storage.internal.data.LocalStorageRepository
import com.erg.almagestdota.local_storage.internal.data.models.draft.DraftRealmModelV3
import com.erg.almagestdota.local_storage.internal.data.models.leagues.LeagueRealmModel
import com.erg.almagestdota.local_storage.internal.data.models.main_settings.*
import com.erg.almagestdota.local_storage.internal.data.models.main_settings.MainPredictorRealmModelV4
import com.erg.almagestdota.local_storage.internal.data.models.main_settings.MainSettingsRealmModel
import com.erg.almagestdota.local_storage.internal.data.models.main_settings.UserHeroesPoolRealmModel
import com.erg.almagestdota.local_storage.internal.helpers.realm.RealmMigration
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import io.realm.Realm
import io.realm.RealmConfiguration
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
internal object RealmModule {

    @Singleton
    @Provides
    fun provideDatabaseInstance(@ApplicationContext appContext: Context): Realm {
        if (Realm.getDefaultConfiguration() == null) {
            Realm.init(appContext)
            val config = RealmConfiguration.Builder()
                .name("com.erg.almagestdota-realm")
                .schemaVersion(6L)
                 .deleteRealmIfMigrationNeeded() //
                .migration(RealmMigration())
                .build()
            Realm.setDefaultConfiguration(config)
            return Realm.getDefaultInstance()
        } else {
            return Realm.getDefaultInstance()
        }
    }

    @Singleton
    @Provides
    fun provideLocalStorageContract(
        realm: Realm,
        mapperToDraftRealmModel: DraftRealmModelV3.Mapper,
        mapperToRealmModelToDraftHistoryItem: DraftHistoryItem.MapperToRealmModel,
        mapperToLeague: LeagueRealmModel.Mapper,
        mapperToLeagueRealmModel: League.MapperToRealmModel,
        mapperToMainSettingsRealmModel: MainSettings.MapperToMainSettingsRealmModel,
        mapperToMainPredictorRealmModel: MainPredictor.MapperToMainPredictorRealmModel,
        mainPredictorRealmModel: MainPredictorRealmModelV4.Mapper,
        mapperToMainSettings: MainSettingsRealmModel.Mapper,
        mapperToUserHeroesPool: UserHeroesPoolRealmModel.Mapper,
        mapperToUserHeroesPoolRealmModel: UserHeroesPool.MapperToUserHeroesPoolRealmModel,
    ): ILocalStorageContract {
        return LocalStorageRepository(
            realm = realm,
            mapperToDraftRealmModel = mapperToDraftRealmModel,
            mapperToRealmModelToDraftHistoryItem = mapperToRealmModelToDraftHistoryItem,
            mapperToLeague = mapperToLeague,
            mapperToMainPredictorRealmModel = mapperToMainPredictorRealmModel,
            mapperToLeagueRealmModel = mapperToLeagueRealmModel,
            mapperToMainSettingsRealmModel = mapperToMainSettingsRealmModel,
            mainPredictorRealmModel = mainPredictorRealmModel,
            mapperToMainSettings = mapperToMainSettings,
            mapperToUserHeroesPool = mapperToUserHeroesPool,
            mapperToUserHeroesPoolRealmModel = mapperToUserHeroesPoolRealmModel,
        )
    }
}