package com.erg.almagestdota.local_storage.internal.data.models.draft

import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.local_storage.external.models.draft.AttackType
import com.erg.almagestdota.local_storage.external.models.draft.AttributeType
import com.erg.almagestdota.local_storage.external.models.draft.ComplexityType
import com.erg.almagestdota.local_storage.external.models.draft.HeroStatsData
import com.google.gson.Gson
import io.realm.FieldAttribute
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.RealmSchema
import io.realm.annotations.PrimaryKey
import javax.inject.Inject

internal open class HeroStatsDataRealmModel : RealmObject() {
    @PrimaryKey
    var id: Int = 0
    var json: String = ""

    class Mapper @Inject constructor(
        private val gson: Gson
    ) : EssentialMapper<HeroStatsDataRealmModel, HeroStatsData>() {
        override fun transform(raw: HeroStatsDataRealmModel): HeroStatsData {
            return gson.fromJson(raw.json, HeroStatsData::class.java)
        }
    }

    companion object {
        const val MODEL_NAME = "HeroStatsDataRealmModel"
        fun createModel(schema: RealmSchema) {
            if (schema[MODEL_NAME] != null) {
                return
            }
            val model = schema.create(MODEL_NAME)
            model?.apply {
                addField("id", Int::class.java, FieldAttribute.PRIMARY_KEY)
                addField("json", String::class.java)
            }
        }
    }
}