package com.erg.almagestdota.local_storage.external.models.draft

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class HeroesItems(
    val heroesItems: Map<Int, HeroItems>,
) : Parcelable