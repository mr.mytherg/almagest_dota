package com.erg.almagestdota.local_storage.external.raw_models

import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.local_storage.external.models.draft.RoleType
import com.erg.almagestdota.local_storage.external.models.draft.main.MemberInfo

class MemberInfoRaw(
    val playerId: Long? = 0L,
    var name: String? = "",
    var role: String? = "",
    var rating: Int? = 1400,
    var matchCount: Int? = 0,
) {
    companion object {
        fun toModel(raw: MemberInfoRaw): MemberInfo {
            return MemberInfo(
                playerId = raw.playerId.orDefault(),
                name = raw.name.orDefault(),
                role = RoleType.byName(raw.role.orDefault()),
                rating = raw.rating.orDefault(),
                matchCount = raw.matchCount.orDefault()
            )
        }
    }
}