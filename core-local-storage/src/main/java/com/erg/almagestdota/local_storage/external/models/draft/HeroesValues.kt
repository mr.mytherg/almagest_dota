package com.erg.almagestdota.local_storage.external.models.draft

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class HeroesValues(
    val heroesValues: Map<Int, HeroValues>,
) : Parcelable