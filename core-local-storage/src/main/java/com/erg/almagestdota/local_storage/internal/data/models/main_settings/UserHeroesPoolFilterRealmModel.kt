package com.erg.almagestdota.local_storage.internal.data.models.main_settings

import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.local_storage.external.models.main_settings.UserHeroesPoolFilter
import io.realm.FieldAttribute
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.RealmSchema
import io.realm.annotations.PrimaryKey
import java.util.*
import javax.inject.Inject

internal open class UserHeroesPoolFilterRealmModel : RealmObject() {
    @PrimaryKey
    var primaryKey: String = UUID.randomUUID().toString()
    var name: String = ""
    var heroes: RealmList<HeroCoreDataRealmModel> = RealmList()

    internal class Mapper @Inject constructor(
        private val mapper: HeroCoreDataRealmModel.Mapper
    ) : EssentialMapper<UserHeroesPoolFilterRealmModel, UserHeroesPoolFilter>() {
        override fun transform(raw: UserHeroesPoolFilterRealmModel): UserHeroesPoolFilter {
            return UserHeroesPoolFilter(
                heroes = raw.heroes.essentialMap(mapper),
                name = raw.name
            )
        }
    }

    companion object {
        const val MODEL_NAME = "UserHeroesPoolFilterRealmModel"
        fun createModel(schema: RealmSchema) {
            if (schema[MODEL_NAME] != null) {
                return
            }
            val model = schema.create(MODEL_NAME)
            model?.apply {
                addField("primaryKey", String::class.java, FieldAttribute.PRIMARY_KEY)
                addField("name", String::class.java)
                addRealmListField("heroes", schema[HeroCoreDataRealmModel.MODEL_NAME]!!)
            }
        }
    }
}