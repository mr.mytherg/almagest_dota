package com.erg.almagestdota.local_storage.external.models.leagues

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class OpenDotaSeriesType(val id: Long) : Parcelable {
    BO1(0),
    BO3(1),
    UNKNOWN(3),
    BO5(2);

    companion object {
        fun byId(value: Long? = 3): OpenDotaSeriesType {
            return values().find { it.id == value } ?: UNKNOWN
        }
    }
}