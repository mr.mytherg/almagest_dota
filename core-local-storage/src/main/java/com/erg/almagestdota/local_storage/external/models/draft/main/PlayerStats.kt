package com.erg.almagestdota.local_storage.external.models.draft.main

import android.os.Parcelable
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PlayerStats(
    val playerId: Long,
    val playerName: String,
    val isStandIn: Boolean,
    var wonCount: Int,
    var lostCount: Int,
    var wonCountPub: Int,
    var lostCountPub: Int,
) : Parcelable {
    companion object {
        fun createEmptyPlayer(playerId: Long, members: List<Long>): PlayerStats {
            val member = GlobalVars.players[members.find { it == playerId }]
            return PlayerStats(
                playerId = playerId,
                playerName = member?.name.orDefault(),
                isStandIn = member == null,
                wonCount = 0,
                lostCount = 0,
                wonCountPub = 0,
                lostCountPub = 0,
            )
        }
    }
}