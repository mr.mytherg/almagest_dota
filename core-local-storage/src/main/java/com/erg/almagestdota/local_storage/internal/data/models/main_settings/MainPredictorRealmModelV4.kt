package com.erg.almagestdota.local_storage.internal.data.models.main_settings

import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.local_storage.external.models.main_settings.MainPredictor
import com.google.gson.Gson
import io.realm.FieldAttribute
import io.realm.RealmObject
import io.realm.RealmSchema
import io.realm.annotations.PrimaryKey
import java.util.*
import javax.inject.Inject

internal open class MainPredictorRealmModelV4 : RealmObject() {

    @PrimaryKey
    var primaryKey2: String = UUID.randomUUID().toString()
    var json: String = ""

    internal class Mapper @Inject constructor(
        private val gson: Gson,
    ) : EssentialMapper<MainPredictorRealmModelV4, MainPredictor>() {
        override fun transform(raw: MainPredictorRealmModelV4): MainPredictor {
            return gson.fromJson(raw.json, MainPredictor::class.java)
        }
    }

    companion object {
        const val MODEL_NAME = "MainPredictorRealmModelV4"
        fun createModel(schema: RealmSchema) {
            if (schema[MODEL_NAME] != null) {
                return
            }
            val model = schema.create(MODEL_NAME)
            model?.apply {
                addField("primaryKey2", String::class.java, FieldAttribute.PRIMARY_KEY, FieldAttribute.REQUIRED)
                addField("json", String::class.java, FieldAttribute.REQUIRED)
            }
        }
    }
}