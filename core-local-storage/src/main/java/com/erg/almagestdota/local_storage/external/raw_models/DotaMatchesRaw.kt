package com.erg.almagestdota.local_storage.external.raw_models

import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch

class DotaMatchesRaw(
    var matches: List<DotaMatchRaw> = listOf(),
) {
    companion object {
        fun toModel(matches: List<DotaMatchRaw>): List<DotaMatch> {
            return matches.map { DotaMatchRaw.toModel(it) }
        }
    }
}