package com.erg.almagestdota.local_storage.external.models.main_settings

import android.os.Parcelable
import com.erg.almagestdota.local_storage.external.models.draft.HeroValues
import com.erg.almagestdota.local_storage.external.models.draft.main.MemberInfo
import com.erg.almagestdota.local_storage.external.raw_models.ImportantSettingsRaw
import com.erg.almagestdota.local_storage.external.raw_models.PlayersSettingsRaw
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PlayersSettings(
    var players: List<MemberInfo> = listOf(),
) : Parcelable {

    companion object {
        fun toModel(raw: PlayersSettings): PlayersSettingsRaw {
            return PlayersSettingsRaw(
                players = raw.players.map { MemberInfo.toModel(it) },
            )
        }
    }
}