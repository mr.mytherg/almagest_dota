package com.erg.almagestdota.local_storage.external.models.draft

import android.os.Parcelable
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData
import com.erg.almagestdota.local_storage.external.raw_models.HeroStatsDataRaw
import com.erg.almagestdota.local_storage.internal.data.models.draft.HeroStatsDataRealmModel
import com.erg.almagestdota.local_storage.internal.helpers.realm.essentialMapRealmModel
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import javax.inject.Inject

class GameStageDataRaw(
    var kills: Double = 0.0,
    var deaths: Double? = 0.0,
    var assists: Double = 0.0,
    var position: String = "",
    var cs: Double = 0.0,
    var dn: Double = 0.0,
    var neutrals: Double = 0.0,
    var magicalDamage: Double = 0.0,
    var heroDamage: Double = 0.0,
    var towerDamage: Double = 0.0,
    var damageReceived: Double = 0.0,
    var healingAllies: Double = 0.0,
    var disableCount: Double = 0.0,
    var disableDuration: Double = 0.0,
    var stunCount: Double = 0.0,
    var stunDuration: Double = 0.0,
) {

    companion object {
        fun toModel(_model: GameStageDataRaw?): GameStageData {
            val model = _model ?: GameStageDataRaw()
            return GameStageData(
                kills = model.kills,
                deaths = model.deaths.orDefault(),
                assists = model.assists,
                position = RoleType.byName(model.position),
                cs = model.cs,
                dn = model.dn,
                neutrals = model.neutrals,
                magicalDamage = model.magicalDamage,
                heroDamage = model.heroDamage,
                towerDamage = model.towerDamage,
                damageReceived = model.damageReceived,
                healingAllies = model.healingAllies,
                disableCount = model.disableCount,
                disableDuration = model.disableDuration,
                stunCount = model.stunCount,
                stunDuration = model.stunDuration,
            )
        }
    }
}