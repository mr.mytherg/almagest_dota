package com.erg.almagestdota.local_storage.internal.data.models.draft

import android.os.Parcelable
import androidx.annotation.StringRes
import com.erg.almagestdota.local_storage.external.models.draft.GameStageData
import com.erg.almagestdota.local_storage.external.models.draft.GameStageType
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GameStageTeamData(
    val stageType: GameStageType,
    val stageData: GameStageData
) : Parcelable