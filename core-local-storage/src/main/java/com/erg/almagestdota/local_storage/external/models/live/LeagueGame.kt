package com.erg.almagestdota.local_storage.external.models.live

import android.os.Parcelable
import com.erg.almagestdota.local_storage.external.models.leagues.LeagueTierType
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import kotlinx.android.parcel.Parcelize

@Parcelize // todo league
data class LeagueGame(
    val players: List<PlayerAccountInfo>,
    val matchId: Long,
    val lobbyId: Long,
    var radiantSeriesWins: Int,
    var direSeriesWins: Int,
    val leagueId: Long,
    val leagueName: String,
    val leagueTierType: LeagueTierType,
    val leagueNodeId: Long,
    val scoreboard: GameScoreboard = GameScoreboard(),
    val streamDelayS: Int,
    val spectators: Int,
    var seriesType: LeagueSeriesType,
    val radiantTeam: LiveTeamInfo?,
    val direTeam: LiveTeamInfo?,
) : Parcelable {
    fun hasMatchStarted(): Boolean {
        return scoreboard.radiant.players.all { it.id > 0 } &&
            scoreboard.radiant.players.size == GlobalVars.mainSettings.getPickSizeCM() &&
            scoreboard.dire.players.all { it.id > 0 } &&
                scoreboard.dire.players.size == GlobalVars.mainSettings.getPickSizeCM()
    }
}