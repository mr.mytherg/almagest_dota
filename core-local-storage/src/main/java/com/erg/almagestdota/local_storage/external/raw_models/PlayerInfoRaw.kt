package com.erg.almagestdota.local_storage.external.raw_models

import android.os.Parcelable
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.local_storage.external.models.draft.main.LaneStatusType
import com.erg.almagestdota.local_storage.external.models.draft.main.LaneType
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerInfo
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PlayerInfoRaw(
    val heroId: Int? = 0,
    val playerId: Long? = 0L,
    var lane: String? = "",
    var laneStatus: String? = "",
) : Parcelable {
    companion object {
        fun toModel(model: PlayerInfoRaw?): PlayerInfo {
            return PlayerInfo(
                heroId = model?.heroId.orDefault(),
                playerId = model?.playerId.orDefault(),
                lane = LaneType.byLaneType(model?.lane.orDefault()),
                laneStatus = LaneStatusType.byLaneStatusType(model?.laneStatus.orDefault()),
            )
        }
    }
}