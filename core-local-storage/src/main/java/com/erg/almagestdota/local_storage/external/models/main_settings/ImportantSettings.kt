package com.erg.almagestdota.local_storage.external.models.main_settings

import android.os.Parcelable
import com.erg.almagestdota.local_storage.external.models.draft.HeroValues
import com.erg.almagestdota.local_storage.external.models.draft.main.MemberInfo
import com.erg.almagestdota.local_storage.external.raw_models.ImportantSettingsRaw
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ImportantSettings(
    var lastDocument: Int = 1000,
    var firstDocument: Int = 1000,
    var heroesValues: MutableMap<Int, HeroValues>,
    var heroesProValues: MutableMap<Int, HeroValues>,
) : Parcelable {

    companion object {
        fun toModel(raw: ImportantSettings): ImportantSettingsRaw {
            val mutableHeroValuesMap = mutableMapOf<String, String>()
            for ((heroId, values) in raw.heroesValues)
                mutableHeroValuesMap[heroId.toString()] = values.encodeValues()

            val mutableProHeroValuesMap = mutableMapOf<String, String>()
            for ((heroId, values) in raw.heroesProValues)
                mutableProHeroValuesMap[heroId.toString()] = values.encodeValues()
            return ImportantSettingsRaw(
                lastDocument = raw.lastDocument,
                firstDocument = raw.firstDocument,
                heroesValues = mutableHeroValuesMap,
                heroesProValues = mutableProHeroValuesMap
            )
        }
    }
}