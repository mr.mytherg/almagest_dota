package com.erg.almagestdota.local_storage.external.models.main_settings

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DraftFilterGroup(
    var name: String,
    val type: DraftFilterGroupType,
    var filters: List<DraftFilter>
) : Parcelable

@Parcelize
enum class DraftFilterGroupType : Parcelable {
    RECOMMENDATIONS,
    MY_POOL,
    ROLES,
    FLEXIBILITY,
    DRAFT_STAGES,
    DAMAGE_AND_HEAL,
    GAME_STAGES,
    ATTACK_TYPE,
    ATTRIBUTES,
    COMPLEXITY,
    TEAM_PLAYERS,
    TEAM_STRATEGIES,
    TEAM_DIRE,
    FACTION,
    IN_GAME_ROLES;
}