package com.erg.almagestdota.local_storage.external.models.live

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PlayerAccountInfo(
    val accountId: Long,
    val heroId: Int,
    val team: Int,
    val nickname: String,
) : Parcelable