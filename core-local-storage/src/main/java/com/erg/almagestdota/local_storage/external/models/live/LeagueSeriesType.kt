package com.erg.almagestdota.local_storage.external.models.live

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class LeagueSeriesType(val seriesType: Int) : Parcelable {
    BO1(0),
    BO3(1),
    BO5(2),
    BO2(3),
    UNKNOWN(4);

    companion object {
        fun bySteamSeriesType(value: Int?): LeagueSeriesType {
            return values().firstOrNull { it.seriesType == value } ?: UNKNOWN
        }
        fun byHawkSeriesType(value: Int): LeagueSeriesType {
            return when (value) {
                1 -> BO1
                2 -> BO2
                3 -> BO3
                5 -> BO5
                else -> UNKNOWN
            }
        }
    }
}