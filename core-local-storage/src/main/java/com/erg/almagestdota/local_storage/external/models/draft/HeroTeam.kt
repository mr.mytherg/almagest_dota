package com.erg.almagestdota.local_storage.external.models.draft

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class HeroTeam(
    var wonCount: Int,
    var lostCount: Int
) : Parcelable