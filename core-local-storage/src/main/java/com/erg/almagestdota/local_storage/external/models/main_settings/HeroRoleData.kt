package com.erg.almagestdota.local_storage.external.models.main_settings

import android.os.Parcelable
import com.erg.almagestdota.local_storage.external.models.draft.RoleType
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
open class HeroRoleData(
    @SerializedName("HeroRoleData_id") override val id: Int,
    @SerializedName("HeroRoleData_name") override val name: String,
    @SerializedName("HeroRoleData_imageLink") override val imageLink: String,
    @SerializedName("HeroRoleData_heroRoles") open var heroRoles: Set<RoleType>? = null
) : HeroCoreData(id, name, imageLink), Parcelable {
    companion object {
        fun createNullHero(): HeroRoleData {
            return HeroRoleData(
                id = -1,
                "",
                "",
                null
            )
        }
    }
}