package com.erg.almagestdota.local_storage.external.models.live

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GameScoreboard(
    val duration: Double = 0.0,
    val durationReadable: String = "",
    val roshanRespawnTimer: Int = 0,
    val radiant: FactionInfo = FactionInfo(),
    val dire: FactionInfo = FactionInfo(),
) : Parcelable