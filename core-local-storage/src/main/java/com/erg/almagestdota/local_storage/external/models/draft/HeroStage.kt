package com.erg.almagestdota.local_storage.external.models.draft

import android.os.Parcelable
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.local_storage.external.raw_models.HeroRoleRaw
import com.erg.almagestdota.local_storage.external.raw_models.HeroStageRaw
import com.erg.almagestdota.local_storage.internal.data.models.draft.HeroStageRealmModel
import kotlinx.android.parcel.Parcelize
import javax.inject.Inject

@Parcelize
data class HeroStage(
    val stage: StageType,
    val pickRate: Double,
    val winRate: Double,
    val matchCount: Int,
) : Parcelable {

    companion object {

        fun toModel(model: HeroStage): HeroStageRaw {
            return  HeroStageRaw(
                stage = model.stage.name,
                pickRate = model.pickRate,
                winRate = model.winRate,
                matchCount = model.matchCount,
            )
        }
    }

    var isPossibleStage = false


}