package com.erg.almagestdota.local_storage.external.models.draft

import androidx.annotation.StringRes
import com.erg.almagestdota.local_storage.R

enum class GameStageType(val duration: Int) {
    LANING(0),
    EARLY_GAME(8),
    MID_GAME(20),
    LATE_GAME(36);

    companion object {
        @StringRes
        fun getId(stage: GameStageType): Int {
            return when (stage) {
                LANING -> R.string.filter_name_game_stage_laning
                EARLY_GAME -> R.string.filter_name_game_stage_early
                MID_GAME -> R.string.filter_name_game_stage_mid
                LATE_GAME -> R.string.filter_name_game_stage_late
            }
        }
    }
}