package com.erg.almagestdota.local_storage.external.models.main_settings

import android.os.Parcelable
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.local_storage.internal.data.models.main_settings.UserHeroesPoolFilterRealmModel
import com.erg.almagestdota.local_storage.internal.helpers.realm.essentialMapRealmModel
import kotlinx.android.parcel.Parcelize
import javax.inject.Inject

@Parcelize
data class UserHeroesPoolFilter(
    val heroes: List<HeroCoreData>,
    val name: String,
) : Parcelable {
    internal class MapperToUserHeroesPoolFilterRealmModel @Inject constructor(
        private val mapperToHeroCoreDataRealmModel: HeroCoreData.MapperToHeroCoreDataRealmModel,
    ) : EssentialMapper<UserHeroesPoolFilter, UserHeroesPoolFilterRealmModel>() {
        override fun transform(raw: UserHeroesPoolFilter): UserHeroesPoolFilterRealmModel {
            return UserHeroesPoolFilterRealmModel().apply {
                heroes = raw.heroes.essentialMapRealmModel(mapperToHeroCoreDataRealmModel)
                name = raw.name
            }
        }
    }
}