package com.erg.almagestdota.local_storage.external.models.draft

import android.os.Parcelable
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.local_storage.external.models.draft.main.DraftModel
import com.erg.almagestdota.local_storage.internal.data.models.draft.DraftRealmModelV3
import com.google.gson.Gson
import kotlinx.android.parcel.Parcelize
import java.util.*
import javax.inject.Inject

@Parcelize
data class DraftHistoryItem(
    val draftModel: DraftModel,
    val dateTimestamp: Long = System.currentTimeMillis(),
    val primaryKey: String = UUID.randomUUID().toString(),
) : Parcelable {

    internal class MapperToRealmModel @Inject constructor(
        private val gson: Gson
    ) : EssentialMapper<DraftHistoryItem, DraftRealmModelV3>() {
        override fun transform(raw: DraftHistoryItem): DraftRealmModelV3 {
            return DraftRealmModelV3().apply {
                primaryKey = raw.primaryKey
                dateTimestamp = raw.dateTimestamp
                draftModel = gson.toJson(raw.draftModel)
            }
        }
    }
}