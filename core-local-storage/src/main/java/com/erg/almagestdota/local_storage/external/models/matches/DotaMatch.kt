package com.erg.almagestdota.local_storage.external.models.matches

import android.os.Parcelable
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerInfo
import com.erg.almagestdota.local_storage.external.models.leagues.LeagueTierType
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData
import com.erg.almagestdota.local_storage.external.raw_models.DotaMatchRaw
import kotlinx.android.parcel.Parcelize

fun Int.getHeroById(): HeroCoreData {
    return GlobalVars.heroesAsMap[this] ?: HeroCoreData.createNullHero()
}

@Parcelize
data class DotaMatch(
    val matchId: Long = 0L,
    val radiantTeamId: Long = 0L,
    val direTeamId: Long = 0L,
    val hasRadiantWon: Boolean = false,
    var willRadiantWin: Boolean? = null,
    var isInvalid: Boolean = false,
    val hasRadiantFirstPick: Boolean = false,
    val duration: Long = 0L,
    val leagueId: Long = 0L,
    val startTime: Long = 0L,
    val seriesId: Long = 0L,
    val radiantScore: Int = 0,
    val direScore: Int = 0,
    val leagueTierType: LeagueTierType = LeagueTierType.UNKNOWN,
    val radiantHeroes: List<PlayerInfo> = listOf(),
    val radiantBans: List<Int> = listOf(),
    val direHeroes: List<PlayerInfo> = listOf(),
    val direBans: List<Int> = listOf()
) : Parcelable {
    companion object {

        fun toModel(raw: DotaMatch): DotaMatchRaw {
            return DotaMatchRaw(
                leagueTier = raw.leagueTierType.name,
                match_id = raw.matchId,
                has_radiant_won = raw.hasRadiantWon,
                has_radiant_first_pick = raw.hasRadiantFirstPick,
                duration = raw.duration,
                startTime = raw.startTime,
                radiantTeamId = raw.radiantTeamId,
                seriesId = raw.seriesId,
                leagueId = raw.leagueId,
                isInvalid = raw.isInvalid,
                willRadiantWin = raw.willRadiantWin,
                direTeamId = raw.direTeamId,
                radiant_heroes = raw.radiantHeroes.map { PlayerInfo.toModel(it) },
                dire_heroes = raw.direHeroes.map { PlayerInfo.toModel(it) },
                radiant_bans = raw.radiantBans,
                radiantScore = raw.radiantScore,
                direScore = raw.direScore,
                dire_bans = raw.direBans,
            )
        }
    }
}