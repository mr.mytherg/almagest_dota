package com.erg.almagestdota.local_storage.internal.data.models.main_settings

import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.local_storage.external.models.main_settings.UserHeroesPool
import io.realm.FieldAttribute
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.RealmSchema
import io.realm.annotations.PrimaryKey
import java.util.*
import javax.inject.Inject

internal open class UserHeroesPoolRealmModel : RealmObject() {
    @PrimaryKey
    var primaryKey: String = UUID.randomUUID().toString()
    var filters: RealmList<UserHeroesPoolFilterRealmModel> = RealmList()

    internal class Mapper @Inject constructor(
        private val mapper: UserHeroesPoolFilterRealmModel.Mapper
    ) : EssentialMapper<UserHeroesPoolRealmModel, UserHeroesPool>() {
        override fun transform(raw: UserHeroesPoolRealmModel): UserHeroesPool {
            return UserHeroesPool(
                filters = raw.filters.essentialMap(mapper),
            )
        }
    }

    companion object {
        const val MODEL_NAME = "UserHeroesPoolRealmModel"
        fun createModel(schema: RealmSchema) {
            if (schema[MODEL_NAME] != null) {
                return
            }
            val model = schema.create(MODEL_NAME)
            model?.apply {
                addField("primaryKey", String::class.java, FieldAttribute.PRIMARY_KEY)
                addRealmListField("heroes", schema[HeroCoreDataRealmModel.MODEL_NAME]!!)
            }
        }
    }
}