package com.erg.almagestdota.local_storage.external.models.draft

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class AttributeType(val attr: String) : Parcelable {
    INTELLIGENCE("int"),
    AGILITY("agi"),
    STRENGTH("str");

    companion object {
        fun byAttr(value: String): AttributeType {
            return values().firstOrNull() { it.attr == value } ?: AGILITY
        }
    }
}