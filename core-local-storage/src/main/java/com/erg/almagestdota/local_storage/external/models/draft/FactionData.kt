package com.erg.almagestdota.local_storage.external.models.draft

import android.os.Parcelable
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData
import com.erg.almagestdota.local_storage.external.models.main_settings.MainSettings
import com.erg.almagestdota.local_storage.external.raw_models.FactionDataRaw
import com.erg.almagestdota.local_storage.external.raw_models.MainSettingsRaw
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FactionData(
    var factionType: FactionType,
    var pickRate: Double,
    var winRate: Double,
    var matchCount: Int,
): Parcelable {
    var pickCount = 0
    var winCount = 0


    companion object {
        fun toModel(raw: FactionData): FactionDataRaw {
            return FactionDataRaw(
                faction = raw.factionType.name,
                pickRate = raw.pickRate,
                winRate = raw.winRate,
                matchCount = raw.matchCount,
            )
        }
    }
}
