package com.erg.almagestdota.local_storage.external.models.draft.main

import android.os.Parcelable
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.local_storage.external.models.draft.RoleType
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.local_storage.external.raw_models.MemberInfoRaw
import com.google.gson.annotations.Expose
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MemberInfo(
    val playerId: Long = 0L,
    var name: String = "",
    var rating: Int = 1400,
    var role: RoleType = RoleType.UNKNOWN,
    var matchCount: Int = 0,
) : Parcelable {

    companion object {
        fun toModel(raw: MemberInfo): MemberInfoRaw {
            return MemberInfoRaw(
                playerId = raw.playerId.orDefault(),
                name = raw.name.orDefault(),
                role = raw.role.name,
                rating = raw.rating.orDefault(),
                matchCount = raw.matchCount.orDefault()
            )
        }
    }
}