package com.erg.almagestdota.local_storage.external.raw_models

import com.erg.almagestdota.local_storage.external.models.main_settings.ImportantSettings
import com.erg.almagestdota.local_storage.external.models.main_settings.PlayersSettings

class PlayersSettingsRaw(
    var players: List<MemberInfoRaw> = listOf(),
) {
    companion object {
        fun toModel(raw: PlayersSettingsRaw): PlayersSettings {
            return PlayersSettings(
                players = raw.players.map { MemberInfoRaw.toModel(it) },
            )
        }
    }
}