package com.erg.almagestdota.local_storage.internal.data.models.draft

import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.local_storage.external.models.draft.DotaItem
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.realm.FieldAttribute
import io.realm.RealmObject
import io.realm.RealmSchema
import io.realm.annotations.PrimaryKey
import java.lang.reflect.Type
import javax.inject.Inject

internal open class DotaItemRealmModel : RealmObject() {
    @PrimaryKey
    var id: Int = 0
    var name: String = ""
    var imageLink: String = ""
    var analogs: String = ""
    var neutralTier: Int = 0
    var early: Boolean = false
    var team: Boolean = false

    class Mapper @Inject constructor(
        private val gson: Gson
    ) : EssentialMapper<DotaItemRealmModel, DotaItem>() {
        override fun transform(raw: DotaItemRealmModel): DotaItem {
            val listType: Type = object : TypeToken<List<Int>>() {}.type
            val analogs: List<Int> = gson.fromJson(raw.analogs, listType)
            return DotaItem(
                id = raw.id,
                name = raw.name,
                imageLink = raw.imageLink,
                neutralTier = raw.neutralTier,
                early = raw.early,
                team = raw.team,
                analogs = analogs
            )
        }
    }

    companion object {
        const val MODEL_NAME = "DotaItemRealmModel"
        fun createModel(schema: RealmSchema) {
            if (schema[MODEL_NAME] != null) {
                return
            }
            val model = schema.create(MODEL_NAME)
            model?.apply {
                addField("id", Int::class.java, FieldAttribute.PRIMARY_KEY)
                addField("name", String::class.java)
                addField("imageLink", String::class.java)
                addField("analogs", String::class.java)
                addField("neutralTier", Int::class.java)
                addField("early", Boolean::class.java)
                addField("team", Boolean::class.java)
            }
        }
    }
}