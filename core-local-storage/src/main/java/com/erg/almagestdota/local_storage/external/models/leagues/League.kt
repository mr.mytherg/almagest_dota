package com.erg.almagestdota.local_storage.external.models.leagues

import android.os.Parcelable
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.local_storage.external.raw_models.LeagueRaw
import com.erg.almagestdota.local_storage.internal.data.models.leagues.LeagueRealmModel
import kotlinx.android.parcel.Parcelize
import javax.inject.Inject

@Parcelize
data class League(
    val tier: LeagueTierType,
    val name: String,
    val id: Long,
) : Parcelable {
    class MapperToLeagueRaw @Inject constructor() : EssentialMapper<League, LeagueRaw>() {
        override fun transform(raw: League): LeagueRaw {
            return LeagueRaw(
                id = raw.id,
                tier = raw.tier.leagueName,
                name = raw.name,
            )
        }
    }

    internal class MapperToRealmModel @Inject constructor() : EssentialMapper<League, LeagueRealmModel>() {
        override fun transform(raw: League): LeagueRealmModel {
            return LeagueRealmModel().apply {
                tier = raw.tier.leagueName
                name = raw.name
                id = raw.id
            }
        }
    }
}