package com.erg.almagestdota.local_storage.internal.data

import android.content.Context
import com.erg.almagestdota.local_storage.internal.helpers.shared_preferences.BaseRxSharedPreferences
import com.erg.almagestdota.local_storage.internal.helpers.shared_preferences.IPreference
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
internal class SharedPreferencesApi @Inject constructor(
    @ApplicationContext private val context: Context
) {

    companion object {
        private const val SP_USER_FILE_NAME = "SP_USER_FILE_NAME"

        private const val SP_DATA_EXPIRATION_TIMESTAMP = "SP_DATA_EXPIRATION_TIMESTAMP"
        private const val SP_LAST_REVIEW_TIMESTAMP = "SP_LAST_REVIEW_TIMESTAMP"
    }

    private val base = object : BaseRxSharedPreferences(context, SP_USER_FILE_NAME) {
        override fun clear() {
            dataExpirationTimestamp.delete()
            lastReviewTimestamp.delete()
        }
    }

    val dataExpirationTimestamp: IPreference<Long> = base.factory.getLong(SP_DATA_EXPIRATION_TIMESTAMP, 0L)
    val lastReviewTimestamp: IPreference<Long> = base.factory.getLong(SP_LAST_REVIEW_TIMESTAMP, 0L)
}