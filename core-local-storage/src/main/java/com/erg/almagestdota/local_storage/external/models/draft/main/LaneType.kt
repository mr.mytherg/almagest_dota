package com.erg.almagestdota.local_storage.external.models.draft.main

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class LaneType(val type: String) : Parcelable {
    JUNGLE("J"),
    MID("M"),
    TOP("T"),
    BOTTOM("B"),
    UNDEFINED("U"),
    ROAMING("R");

    companion object {
        fun byLaneType(value: String): LaneType {
            return values().firstOrNull() { it.type == value } ?: UNDEFINED
        }
    }
}