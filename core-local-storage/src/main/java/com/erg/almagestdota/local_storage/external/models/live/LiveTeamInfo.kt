package com.erg.almagestdota.local_storage.external.models.live

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * @param isTeamComplete Whether the players for this team are all team members.
 */

@Parcelize
data class LiveTeamInfo(
    val teamName: String,
    val teamId: Long,
    val isTeamComplete: Boolean,
) : Parcelable