package com.erg.almagestdota.local_storage.external.models.draft

import androidx.annotation.StringRes
import com.erg.almagestdota.local_storage.R

enum class StageType(val number: Int) {
    FIRST_PICK(1),
    DOUBLE_PICK(2),
    FIRST_STAGE_ENDING(3),
    SECOND_STAGE_OPENING(4),
    SECOND_STAGE_DOUBLE_PICK(5),
    SECOND_STAGE_ENDING(6),
    THIRD_STAGE_OPENING(7),
    LAST_PICK(8);

    companion object {
        fun byName(value: String): StageType {
            return values().find { it.name == value } ?: FIRST_PICK
        }
        fun byNumber(number: Int): StageType {
            return values().find { it.number == number }!!
        }

        @StringRes
        fun getId(stage: StageType): Int {
            return when (stage) {
                FIRST_PICK -> R.string.filter_stage_first_pick
                DOUBLE_PICK -> R.string.filter_stage_double_pick
                FIRST_STAGE_ENDING -> R.string.filter_stage_1_stage_ending
                SECOND_STAGE_OPENING -> R.string.filter_stage_2_stage_opening
                SECOND_STAGE_DOUBLE_PICK -> R.string.filter_stage_2_stage_double_pick
                SECOND_STAGE_ENDING -> R.string.filter_stage_2_stage_ending
                THIRD_STAGE_OPENING -> R.string.filter_stage_3_stage_opening
                LAST_PICK -> R.string.filter_stage_last_pick
            }
        }
    }
}