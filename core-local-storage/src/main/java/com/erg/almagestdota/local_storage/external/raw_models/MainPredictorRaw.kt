package com.erg.almagestdota.local_storage.external.raw_models

import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.MainPredictor
import javax.inject.Inject

data class MainPredictorRaw(
    val disabled: Boolean = false,
    val teamProbabilities: Map<String, Double> = mapOf(),
    val draftProbabilities: Map<String, Double> = mapOf(),
) {
    companion object {
        fun toModel(raw: MainPredictorRaw): MainPredictor {
            return MainPredictor(
                disabled = raw.disabled,
                teamProbabilities = raw.teamProbabilities.toMutableMap(),
                draftProbabilities = raw.draftProbabilities.toMutableMap(),
            )
        }
    }
}