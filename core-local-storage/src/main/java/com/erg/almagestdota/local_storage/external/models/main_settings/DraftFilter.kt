package com.erg.almagestdota.local_storage.external.models.main_settings

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DraftFilter(
    val id: Int,
    var name: String,
    val description: String,
    var heroes: List<Int>
) : Parcelable