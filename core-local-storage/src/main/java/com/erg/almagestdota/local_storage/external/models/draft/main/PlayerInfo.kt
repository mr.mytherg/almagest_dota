package com.erg.almagestdota.local_storage.external.models.draft.main

import android.os.Parcelable
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.local_storage.external.raw_models.PlayerInfoRaw
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PlayerInfo(
    val heroId: Int = 0,
    val playerId: Long = 0L,
    var lane: LaneType = LaneType.UNDEFINED,
    var laneStatus: LaneStatusType = LaneStatusType.UNDEFINED,
) : Parcelable {
    companion object {
        fun toModel(model: PlayerInfo): PlayerInfoRaw {
            return PlayerInfoRaw(
                heroId = model.heroId.orDefault(),
                playerId = model.playerId.orDefault(),
                lane = model.lane.type,
                laneStatus = model.laneStatus.type,
            )
        }
    }
}