package com.erg.almagestdota.local_storage.external.models.draft

import android.os.Parcelable
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.local_storage.external.raw_models.HeroRoleRaw
import com.erg.almagestdota.local_storage.internal.data.models.draft.HeroRoleRealmModel
import kotlinx.android.parcel.Parcelize
import javax.inject.Inject

@Parcelize
data class PlayerLaneOutcome(
    val id: Long,
    val roleType: RoleType,
    var teamId: Long = 0L,
    var winPlus: Int = 0,
    var win: Int = 0,
    var tie: Int = 0,
    var lose: Int = 0,
    var loseMinus: Int = 0,
    var undefined: Int = 0,
    var matchCount: Int = 0,
) : Parcelable
