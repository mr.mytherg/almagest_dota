package com.erg.almagestdota.local_storage.external.models.draft

import androidx.annotation.StringRes
import com.erg.almagestdota.local_storage.R

enum class RoleType(val position: String, val number: Int) {
    ROLE_1("POSITION_1", 1),
    ROLE_2("POSITION_2", 2),
    ROLE_3("POSITION_3", 3),
    ROLE_4("POSITION_4", 4),
    UNKNOWN("UNKNOWN", 6),
    ROLE_5("POSITION_5", 5);

    companion object {
        fun byName(value: String): RoleType {
            return values().firstOrNull() { it.name == value } ?: UNKNOWN
        }

        fun byPosition(value: String): RoleType {
            return values().firstOrNull() { it.position == value } ?: UNKNOWN
        }

        fun byNumber(number: Int): RoleType {
            return values().firstOrNull() { it.number == number }!!
        }

        @StringRes
        fun getId(role: RoleType): Int {
            return when (role) {
                ROLE_5 -> R.string.role_5
                ROLE_4 -> R.string.role_4
                ROLE_3 -> R.string.role_3
                ROLE_2 -> R.string.role_2
                ROLE_1 -> R.string.role_1
                else -> 0
            }
        }
    }
}