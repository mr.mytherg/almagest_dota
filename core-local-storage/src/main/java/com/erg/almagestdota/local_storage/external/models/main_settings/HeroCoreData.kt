package com.erg.almagestdota.local_storage.external.models.main_settings

import android.os.Parcelable
import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.local_storage.external.models.draft.HeroValues
import com.erg.almagestdota.local_storage.internal.data.models.main_settings.HeroCoreDataRealmModel
import kotlinx.android.parcel.Parcelize
import java.lang.Exception
import javax.inject.Inject

@Parcelize
open class HeroCoreData(
    open val id: Int,
    open val name: String,
    open val imageLink: String,
) : Parcelable {

    companion object {
        fun createSkippedHero() = HeroCoreData(id = 0, name = ",", imageLink = "")
        fun createNullHero() = HeroCoreData(id = -1, name = ",", imageLink = "")
    }

    fun isSkippedHero(): Boolean = id == 0
    fun isNullHero(): Boolean = id == -1

    override fun equals(other: Any?): Boolean {
        if (other === this) return true
        if (other !is HeroCoreData) return false
        val hero: HeroCoreData = other
        return hero.id == id &&
            hero.name == name &&
            hero.imageLink == imageLink
    }

    // Idea from effective Java : Item 9
    override fun hashCode(): Int {
        var result = 17
        result = try {
            31 * result + id.hashCode()
        } catch (e: Exception) {
            0
        }
        result = try {
            31 * result + name.hashCode()
        } catch (e: Exception) {
            0
        }
        result = try {
            31 * result + imageLink.hashCode()
        } catch (e: Exception) {
            0
        }
        return result
    }

    internal class MapperToHeroCoreDataRealmModel @Inject constructor() : EssentialMapper<HeroCoreData, HeroCoreDataRealmModel>() {
        override fun transform(raw: HeroCoreData): HeroCoreDataRealmModel {
            return HeroCoreDataRealmModel().apply {
                id = raw.id
                imageLink = raw.imageLink
                name = raw.name
            }
        }
    }
}

fun List<Int>.findHeroByBinaryMethod(id: Int, startIndex: Int = 0, endIndex: Int = this.lastIndex): Int {
    val size = endIndex + startIndex
    val currentIndex = size / 2
    if (this.isEmpty()) {
        return -1
    }
    val item = this[currentIndex]
    return if (id == item) {
        item
    } else if (startIndex > endIndex) {
        return -1
    } else if (id > item) {
        findHeroByBinaryMethod(id, currentIndex + 1, endIndex)
    } else {
        findHeroByBinaryMethod(id, startIndex, currentIndex - 1)
    }
}

fun Int.getCounter(enemies: List<Int>, stats: Map<Int, HeroValues> = GlobalVars.importantSettings.heroesValues): Double {
    if (enemies.isEmpty()) {
        return 0.0
    }
    var counter = 0.0
    for (enemy in enemies)
        counter += (stats[this]!!.counters[enemy]!! - stats[enemy]!!.counters[this]!!) / 2
    return (counter).round2()
}

fun Int.getSynergy(allies: List<Int>, stats: Map<Int, HeroValues> = GlobalVars.importantSettings.heroesValues): Double {
    if (allies.isEmpty()) {
        return 0.0
    }
    var synergy = 0.0
    for (allie in allies) {
        if (allie == this) continue
        synergy += stats[this]!!.synergies[allie]!!
    }
    return (synergy).round2()
}

fun Int.isAllCounterPositive(enemies: List<Int>, stats: Map<Int, HeroValues> = GlobalVars.importantSettings.heroesValues): Boolean {
    for (enemy in enemies) {
        val counter = (stats[this]!!.counters[enemy]!! - stats[enemy]!!.counters[this]!!) / 2
        if (counter < 0.0)
            return false
    }
    return true
}