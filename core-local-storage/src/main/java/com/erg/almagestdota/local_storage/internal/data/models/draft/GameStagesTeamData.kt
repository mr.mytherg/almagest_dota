package com.erg.almagestdota.local_storage.internal.data.models.draft

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GameStagesTeamData(
    val radiantTeam: List<GameStageTeamData>,
    val direTeam: List<GameStageTeamData>
) : Parcelable