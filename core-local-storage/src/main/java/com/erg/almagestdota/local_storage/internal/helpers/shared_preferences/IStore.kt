package com.erg.almagestdota.local_storage.internal.helpers.shared_preferences

internal interface IStore {
    fun clear()
}