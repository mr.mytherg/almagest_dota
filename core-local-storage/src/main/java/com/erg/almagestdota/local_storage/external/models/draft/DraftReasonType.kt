package com.erg.almagestdota.local_storage.external.models.draft

import androidx.annotation.StringRes
import com.erg.almagestdota.local_storage.R

enum class DraftReasonType(val isBad: Boolean) {
    NON_META(true),
    LOW_COEF(true),
    NOT_IN_POOL(true),
    HAS_HERO_FOR_THIS_ROLE(true),
    INVALID_STAGE(true),
    PLAYER_ALREADY_HAS_HERO(true);

    companion object {
        @StringRes
        fun getStringId(reason: DraftReasonType): Int {
            return when (reason) {
                PLAYER_ALREADY_HAS_HERO -> R.string.draft_reason_player_already_has_hero
                LOW_COEF -> R.string.draft_reason_low_coef
                NON_META -> R.string.draft_reason_non_meta
                NOT_IN_POOL -> R.string.draft_reason_not_in_pool
                HAS_HERO_FOR_THIS_ROLE -> R.string.draft_reason_has_hero_for_this_role
                INVALID_STAGE -> R.string.draft_reason_invalid_stage
            }
        }
    }
}