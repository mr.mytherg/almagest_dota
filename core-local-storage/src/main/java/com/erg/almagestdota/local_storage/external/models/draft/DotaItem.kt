package com.erg.almagestdota.local_storage.external.models.draft

import android.os.Parcelable
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.local_storage.external.raw_models.DotaItemRaw
import com.erg.almagestdota.local_storage.internal.data.models.draft.DotaItemRealmModel
import com.google.gson.Gson
import kotlinx.android.parcel.Parcelize
import javax.inject.Inject

@Parcelize
open class DotaItem(
    open val id: Int,
    open val name: String,
    open val imageLink: String,
    open val neutralTier: Int,
    open val early: Boolean,
    open val team: Boolean,
    open var analogs: List<Int>,
) : Parcelable {
    companion object {
        fun toModel(model: DotaItem):DotaItemRaw {
            return DotaItemRaw(
                id = model.id,
                image = model.imageLink,
                name = model.name,
                neutral_tier = model.neutralTier,
                early = model.early,
                team = model.team,
                analogs = model.analogs.ifEmpty { null }
            )
        }
    }

    internal class MapperToDotaItemRealmModel @Inject constructor(
        private val gson: Gson
    ) : EssentialMapper<DotaItem, DotaItemRealmModel>() {
        override fun transform(raw: DotaItem): DotaItemRealmModel {
            return DotaItemRealmModel().apply {
                id = raw.id
                imageLink = raw.imageLink
                name = raw.name
                analogs = gson.toJson(raw.analogs)
                neutralTier = raw.neutralTier
                early = raw.early
                team = raw.team
            }
        }
    }
}