package com.erg.almagestdota.local_storage.external.models.main_settings

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
open class HeroSimplePlayerData(
    @SerializedName("HeroPlayerData_id") override val id: Int,
    @SerializedName("HeroPlayerData_name") override val name: String,
    @SerializedName("HeroPlayerData_imageLink") override val imageLink: String,
    @SerializedName("HeroPlayerData_heroPlayers") open var heroPlayer: Pair<Long, String>,
) : HeroCoreData(id, name, imageLink), Parcelable