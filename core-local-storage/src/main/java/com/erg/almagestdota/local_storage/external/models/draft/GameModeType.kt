package com.erg.almagestdota.local_storage.external.models.draft

import android.os.Parcelable
import androidx.annotation.StringRes
import com.erg.almagestdota.local_storage.R
import kotlinx.android.parcel.Parcelize

fun GameModeType.isUser() = this == GameModeType.USER
fun GameModeType.isTeam() = this == GameModeType.TEAM
fun GameModeType.isBot() = this == GameModeType.BOT
fun GameModeType.isBattleCup() = this == GameModeType.BATTLE_CUP
fun GameModeType.isLiveDraft() = this == GameModeType.LIVE_IN_DRAFT
fun GameModeType.isLiveGame() = this == GameModeType.LIVE_IN_GAME
fun GameModeType.isLiveFinished() = this == GameModeType.LIVE_FINISHED
fun GameModeType.isLive() = isLiveDraft() || isLiveGame()
fun GameModeType.hasTeams() = isTeam() || isLive() || isLiveFinished() || isBattleCup()

@Parcelize
enum class GameModeType : Parcelable {
    BATTLE_CUP,
    TEAM,
    BOT,
    LIVE_IN_DRAFT,
    LIVE_IN_GAME,
    LIVE_FINISHED,
    USER;

    @StringRes
    fun getResource(): Int {
        return when (this) {
            BATTLE_CUP -> R.string.game_mode_battle_cup
            TEAM -> R.string.game_mode_against_team
            USER -> R.string.game_mode_lobby
            BOT -> R.string.game_mode_against_bot_deprecated
            LIVE_IN_DRAFT -> R.string.game_mode_live_draft
            LIVE_IN_GAME -> R.string.game_mode_live_game
            LIVE_FINISHED -> R.string.game_mode_live_ended
        }
    }

    companion object {
        fun byName(value: String): GameModeType {
            return values().firstOrNull { it.name == value } ?: USER
        }
    }
}