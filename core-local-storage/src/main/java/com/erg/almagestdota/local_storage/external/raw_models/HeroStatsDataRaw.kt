package com.erg.almagestdota.local_storage.external.raw_models

import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.local_storage.external.models.draft.*
import javax.inject.Inject

class HeroStatsDataRaw(
    val id: Int = 0,
    val image: String = "",
    val name: String = "",
    val magicalDamagePercentage: Double = 0.0,
    val pureDamagePercentage: Double = 0.0,
    val towerDamage: Double = 0.0,
    val healing: Double = 0.0,

    var laning: List<GameStageDataRaw>,
    var early: List<GameStageDataRaw>,
    var mid: List<GameStageDataRaw>,
    var late: List<GameStageDataRaw>,
    var hasBlink: Boolean? = null,
    var hasBkb: Boolean? = null,

    val physicalDamagePercentage: Double = 0.0,
    val attackType: String = "",
    val primaryAttribute: String = "",
    val complexity: String = "",
    val team: Boolean = true,
    val roles: List<HeroRoleRaw> = listOf(),
    val factions: List<FactionDataRaw>? = listOf(),
    val stages: List<HeroStageRaw> = listOf(),
) {
    companion object {

        fun toModel(raw: HeroStatsDataRaw?): HeroStatsData {
            return HeroStatsData(
                laning = raw?.laning.orEmpty().map { GameStageDataRaw.toModel(it) },
                early = raw?.early.orEmpty().map { GameStageDataRaw.toModel(it) },
                mid = raw?.mid.orEmpty().map { GameStageDataRaw.toModel(it) },
                late = raw?.late.orEmpty().map { GameStageDataRaw.toModel(it) },
                id = raw?.id.orDefault(),
                imageLink = raw?.image.orDefault(),
                name = raw?.name.orDefault(),
                hasBlink = raw?.hasBlink.orDefault(),
                hasBkb = raw?.hasBkb.orDefault(),
                magicalDamagePercentage = raw?.magicalDamagePercentage.orDefault(),
                pureDamagePercentage = raw?.pureDamagePercentage.orDefault(),
                towerDamage = raw?.towerDamage.orDefault(),
                healing = raw?.healing.orDefault(),
                physicalDamagePercentage = raw?.physicalDamagePercentage.orDefault(),
                attackType = AttackType.byAttackType(raw?.attackType.orDefault()),
                primaryAttribute = AttributeType.byAttr(raw?.primaryAttribute.orDefault()),
                complexity = ComplexityType.byComplexity(raw?.complexity.orDefault()),
                team = raw?.team.orDefault(),
                roles = raw?.roles.orEmpty().map { HeroRoleRaw.toModel(it) },
                factions = raw?.factions.orEmpty().map { FactionDataRaw.toModel(it) },
                stages = raw?.stages.orEmpty().map { HeroStageRaw.toModel(it) },
            )
        }
    }
}