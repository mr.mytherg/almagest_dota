package com.erg.almagestdota.local_storage.external.models.live

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
 data class AbilityInfo(
     val abilityId: Int,
     val abilityLevel: Int,
 ) : Parcelable