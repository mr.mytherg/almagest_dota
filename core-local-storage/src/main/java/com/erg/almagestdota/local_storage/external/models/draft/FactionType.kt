package com.erg.almagestdota.local_storage.external.models.draft

import android.os.Parcelable
import androidx.annotation.StringRes
import com.erg.almagestdota.local_storage.R
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class FactionType(val number: Int): Parcelable {
    RADIANT(1),
    DIRE(2),
    OVERALL(0);
    companion object {
        @StringRes
        fun getId(role: FactionType): Int {
            return when (role) {
                RADIANT -> R.string.radiant
                DIRE -> R.string.dire
                else -> R.string.overall
            }
        }

        fun byName(value: String): FactionType {
            return FactionType.values().find { it.name == value } ?: FactionType.OVERALL
        }

        fun byNumber(number: Int): FactionType {
            return FactionType.values().firstOrNull() { it.number == number }!!
        }
    }
}