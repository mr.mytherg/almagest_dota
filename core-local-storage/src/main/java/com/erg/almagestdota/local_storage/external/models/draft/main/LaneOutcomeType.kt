package com.erg.almagestdota.local_storage.external.models.draft.main

import android.os.Parcelable
import androidx.annotation.StringRes
import com.erg.almagestdota.local_storage.R
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class LaneOutcomeType(val number: Int) : Parcelable {
    WIN_PLUS(0),
    WIN(1),
    LOSE_MINUS(4),
    LOSE(3),
    UNDEFINED(5),
    TIE(2);

    companion object {
        fun byNumber(value: Int): LaneOutcomeType {
            return values().firstOrNull() { it.number == value } ?: UNDEFINED
        }

        @StringRes
        fun getId(status:LaneOutcomeType): Int {
            return when (status) {
                WIN_PLUS -> R.string.lane_outcome_status_win_plus
                    WIN -> R.string.lane_outcome_status_win
                    TIE -> R.string.lane_outcome_status_tie
                    LOSE -> R.string.lane_outcome_status_lose
                    LOSE_MINUS -> R.string.lane_outcome_status_lose_minus
                    UNDEFINED -> R.string.lane_outcome_status_undefined
            }
        }

    }
}