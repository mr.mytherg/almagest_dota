package com.erg.almagestdota.local_storage.external.models.live

import android.os.Parcelable
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SteamPlayerInfo(
    val playerSlot: Int,
    val accountId: Long,
    val kills: Int,
    val death: Int,
    val assists: Int,
    val lastHits: Int,
    val denies: Int,
    val gold: Int,
    val level: Int,
    val goldPerMin: Int,
    val xpPerMin: Int,
    val ultimateState: Int,
    val ultimateCooldown: Int,
    val item0: Int,
    val item1: Int,
    val item2: Int,
    val item3: Int,
    val item4: Int,
    val item5: Int,
    val respawnTimer: Int,
    val positionX: Double,
    val positionY: Double,
    val netWorth: Int,
    override val id: Int,
    override val name: String,
    override val imageLink: String,
) : Parcelable, HeroCoreData(
    id = id,
    name = name,
    imageLink = imageLink
)