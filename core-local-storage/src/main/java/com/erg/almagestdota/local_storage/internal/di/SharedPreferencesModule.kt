package com.erg.almagestdota.local_storage.internal.di

import com.erg.almagestdota.local_storage.external.ISharedPreferencesContract
import com.erg.almagestdota.local_storage.internal.data.SharedPreferencesRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
internal abstract class SharedPreferencesModule {

    @Binds
    @Singleton
    abstract fun bindSharedPreferencesContract(impl: SharedPreferencesRepository): ISharedPreferencesContract
}