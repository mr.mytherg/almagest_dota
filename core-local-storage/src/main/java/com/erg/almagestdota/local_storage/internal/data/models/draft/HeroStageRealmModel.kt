package com.erg.almagestdota.local_storage.internal.data.models.draft

import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.local_storage.external.models.draft.HeroStage
import com.erg.almagestdota.local_storage.external.models.draft.StageType
import io.realm.FieldAttribute
import io.realm.RealmObject
import io.realm.RealmSchema
import io.realm.annotations.PrimaryKey
import java.util.*
import javax.inject.Inject

internal open class HeroStageRealmModel : RealmObject() {
    @PrimaryKey
    var primaryKey: String = UUID.randomUUID().toString()
    var stage: String = ""
    var pickRate: Double = 0.0
    var winRate: Double = 0.0
    var matchCount: Int = 0

    class Mapper @Inject constructor() : EssentialMapper<HeroStageRealmModel, HeroStage>() {
        override fun transform(raw: HeroStageRealmModel): HeroStage {
            return HeroStage(
                stage = StageType.byName(raw.stage),
                pickRate = raw.pickRate,
                winRate = raw.winRate,
                matchCount = raw.matchCount,
            )
        }
    }

    companion object {
        const val MODEL_NAME = "HeroStageRealmModel"
        fun createModel(schema: RealmSchema) {
            if (schema[MODEL_NAME] != null) {
                return
            }
            val model = schema.create(MODEL_NAME)
            model?.apply {
                addField("primaryKey", String::class.java, FieldAttribute.PRIMARY_KEY)
                addField("stage", String::class.java)
                addField("pickRate", Double::class.java)
                addField("winRate", Double::class.java)
                addField("matchCount", Int::class.java)
            }
        }
    }
}