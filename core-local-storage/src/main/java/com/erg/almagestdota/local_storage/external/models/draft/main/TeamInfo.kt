package com.erg.almagestdota.local_storage.external.models.draft.main

import android.os.Parcelable
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.local_storage.external.raw_models.MemberInfoRaw
import com.erg.almagestdota.local_storage.external.raw_models.TeamInfoRaw
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TeamInfo(
    var id: Long = 0L,
    var name: String = "",
    var rating: Int = 1400,
    val imageLink: String = "",
    var matchCount: Int = 0,
    val is322: Boolean = false,
    var members: List<Long> = listOf(),
) : Parcelable {
    companion object {
        fun toModel(model: TeamInfo): TeamInfoRaw {
            return TeamInfoRaw(
                id = model.id.orDefault(),
                name = model.name.orDefault(),
                rating = model.rating.orDefault(),
                is322 = model.is322.orDefault(),
                imageLink = model.imageLink.orDefault(),
                matchCount = model.matchCount.orDefault(),
                members = model.members,
            )
        }
    }
}