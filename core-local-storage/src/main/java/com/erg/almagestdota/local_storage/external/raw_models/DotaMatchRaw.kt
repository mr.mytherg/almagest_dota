package com.erg.almagestdota.local_storage.external.raw_models

import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerInfo
import com.erg.almagestdota.local_storage.external.models.leagues.LeagueTierType
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch

class DotaMatchRaw(
    val match_id: Long? = null,
    val has_radiant_won: Boolean? = null,
    val has_radiant_first_pick: Boolean? = null,
    val duration: Long? = null,
    val startTime: Long? = null,
    val leagueTier: String? = null,
    val radiantName: String? = null,
    val radiantTeamId: Long? = null,
    val willRadiantWin: Boolean? = null,
    val isInvalid: Boolean? = null,
    val seriesId: Long? = null,
    val leagueId: Long? = null,
    val direTeamId: Long? = null,
    val radiantScore: Int? = null,
    val direScore: Int? = null,
    val radiant_heroes: List<PlayerInfoRaw>? = null,
    val radiant_bans: List<Int>? = null,
    val dire_heroes: List<PlayerInfoRaw>? = null,
    val dire_bans: List<Int>? = null
) {
    companion object {
        fun toModel(raw: DotaMatchRaw): DotaMatch {
            return DotaMatch(
                duration = raw.duration.orDefault(),
                matchId = raw.match_id.orDefault(),
                startTime = raw.startTime.orDefault(),
                radiantTeamId = raw.radiantTeamId.orDefault(),
                direTeamId = raw.direTeamId.orDefault(),
                radiantScore = raw.radiantScore.orDefault(),
                direScore = raw.direScore.orDefault(),
                willRadiantWin = raw.willRadiantWin,
                isInvalid = raw.isInvalid.orDefault(),
                seriesId = raw.seriesId.orDefault(),
                leagueId = raw.leagueId.orDefault(),
                hasRadiantFirstPick = raw.has_radiant_first_pick.orDefault(),
                leagueTierType = LeagueTierType.valueOf(raw.leagueTier.orDefault()),
                hasRadiantWon = raw.has_radiant_won.orDefault(),
                radiantHeroes = raw.radiant_heroes.orEmpty().map { PlayerInfoRaw.toModel(it) },
                radiantBans = raw.radiant_bans.orEmpty().map { it },
                direHeroes = raw.dire_heroes.orEmpty().map { PlayerInfoRaw.toModel(it) },
                direBans = raw.dire_bans.orEmpty().map { it },
            )
        }
    }
}