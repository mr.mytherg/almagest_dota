package com.erg.almagestdota.local_storage.internal.data.models.main_settings

import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData
import io.realm.FieldAttribute
import io.realm.RealmObject
import io.realm.RealmSchema
import io.realm.annotations.PrimaryKey
import java.util.*
import javax.inject.Inject

internal open class HeroCoreDataRealmModel : RealmObject() {
    @PrimaryKey
    var id: Int = 0
    var name: String = ""
    var imageLink: String = ""

    class Mapper @Inject constructor() : EssentialMapper<HeroCoreDataRealmModel, HeroCoreData>() {
        override fun transform(raw: HeroCoreDataRealmModel): HeroCoreData {
            return HeroCoreData(
                id = raw.id,
                name = raw.name,
                imageLink = raw.imageLink,
            )
        }
    }

    companion object {
        const val MODEL_NAME = "HeroCoreDataRealmModel"
        fun createModel(schema: RealmSchema) {
            if (schema[MODEL_NAME] != null) {
                return
            }
            val model = schema.create(MODEL_NAME)
            model?.apply {
                addField("id", Int::class.java, FieldAttribute.PRIMARY_KEY)
                addField("name", String::class.java)
                addField("imageLink", String::class.java)
            }
        }
    }
}