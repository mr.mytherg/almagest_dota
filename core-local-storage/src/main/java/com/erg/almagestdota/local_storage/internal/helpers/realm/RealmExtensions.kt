@file:Suppress("NOTHING_TO_INLINE")

package com.erg.almagestdota.local_storage.internal.helpers.realm

import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.Function
import io.realm.Realm
import io.realm.RealmList
import io.realm.RealmModel
import io.realm.RealmObject

internal fun Realm.insertOrUpdateWithTransaction(realmObject: RealmModel) {
    if (!isInTransaction) {
        beginTransaction()
    }
    insertOrUpdate(realmObject)
    commitTransaction()
}

internal inline fun <reified T : RealmObject> Realm.deleteWithTransaction(realmObject: Class<T>) {
    if (!isInTransaction) {
        beginTransaction()
    }
    delete(realmObject)
    commitTransaction()
}

internal fun <T : RealmModel> Realm.insertOrUpdateWithTransaction(realmCollection: Collection<T>) {
    if (!isInTransaction) {
        beginTransaction()
    }
    insertOrUpdate(realmCollection)
    commitTransaction()
}

internal fun <T : Any, R : Any> List<T?>?.essentialMapRealmModel(mapper: EssentialMapper<T, R>): RealmList<R> {
    return deepAssert().mapping(mapper)
}

internal inline fun <T> List<T?>?.deepAssert(): List<T> {
    return this!!.map { it!! }
}

internal inline fun <T, R : Any> List<T>.mapping(transform: Function<T, R>): RealmList<R> {
    val realmList = RealmList<R>()
    forEach { realmList.add(transform.apply(it)) }
    return realmList
}