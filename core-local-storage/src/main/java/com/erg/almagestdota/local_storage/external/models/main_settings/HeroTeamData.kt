package com.erg.almagestdota.local_storage.external.models.main_settings

import android.os.Parcelable
import com.erg.almagestdota.local_storage.external.models.draft.RoleType
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class HeroTeamData(
    @SerializedName("HeroTeamData_id") override val id: Int,
    @SerializedName("HeroTeamData_name") override val name: String,
    @SerializedName("HeroTeamData_imageLink") override val imageLink: String,
    @SerializedName("HeroTeamData_heroRoles") override var heroRoles: Set<RoleType>? = null,
    @SerializedName("HeroTeamData_wonCount") var wonCount: Int,
    @SerializedName("HeroTeamData_lostCount") var lostCount: Int,
) : HeroRoleData(id, name, imageLink), Parcelable {
    companion object {
        fun createNullHero(): HeroTeamData {
            return HeroTeamData(
                id = -1,
                "",
                "",
                null,
                0,
                0
            )
        }
    }
}