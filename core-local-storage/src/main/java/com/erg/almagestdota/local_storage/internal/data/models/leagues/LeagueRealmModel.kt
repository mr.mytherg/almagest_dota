package com.erg.almagestdota.local_storage.internal.data.models.leagues

import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.local_storage.external.models.leagues.League
import com.erg.almagestdota.local_storage.external.models.leagues.LeagueTierType
import io.realm.FieldAttribute
import io.realm.RealmObject
import io.realm.RealmSchema
import io.realm.annotations.PrimaryKey
import javax.inject.Inject

internal open class LeagueRealmModel : RealmObject() {

    @PrimaryKey
    var id: Long = 0L
    var tier: String = ""
    var name: String = ""

    class Mapper @Inject constructor() : EssentialMapper<LeagueRealmModel, League>() {
        override fun transform(raw: LeagueRealmModel): League {
            return League(
                tier = LeagueTierType.byName(raw.tier),
                name = raw.name,
                id = raw.id,
            )
        }
    }

    companion object {
        const val MODEL_NAME = "LeagueRealmModel"
        fun createModel(schema: RealmSchema) {
            if (schema[MODEL_NAME] != null) {
                return
            }
            val model = schema.create(MODEL_NAME)
            model?.apply {
                addField("id", Long::class.java, FieldAttribute.PRIMARY_KEY)
                addField("tier", Long::class.java)
                addField("name", String::class.java)
            }
        }
    }
}