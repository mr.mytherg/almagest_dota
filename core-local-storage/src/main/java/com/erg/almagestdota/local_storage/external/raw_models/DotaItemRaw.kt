package com.erg.almagestdota.local_storage.external.raw_models

import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.NotRequired
import com.erg.almagestdota.local_storage.external.models.draft.DotaItem
import javax.inject.Inject

data class DotaItemRaw(
    val id: Int,
    val name: String,
    val image: String,
    val neutral_tier: Int,
    val early: Boolean,
    val team: Boolean,
    @NotRequired val analogs: List<Int>? = listOf(),
) {
    companion object {
        fun toModel(raw: DotaItemRaw?): DotaItem {
            return DotaItem(
                id = raw?.id.orDefault(),
                name = raw?.name.orDefault(),
                imageLink = raw?.image.orDefault(),
                neutralTier = raw?.neutral_tier.orDefault(),
                early = raw?.early.orDefault(),
                team = raw?.team.orDefault(),
                analogs = raw?.analogs.orEmpty(),
            )
        }
    }
}