package com.erg.almagestdota.local_storage.internal.data

import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.DraftAnalysis
import com.erg.almagestdota.local_storage.external.models.draft.DraftHistoryItem
import com.erg.almagestdota.local_storage.external.models.draft.DraftReasonType
import com.erg.almagestdota.local_storage.external.models.draft.main.DraftModel
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerInfo
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamDraft
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamInfo
import com.erg.almagestdota.local_storage.external.models.leagues.League
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.main_settings.MainPredictor
import com.erg.almagestdota.local_storage.external.models.main_settings.MainSettings
import com.erg.almagestdota.local_storage.external.models.main_settings.UserHeroesPool
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.local_storage.internal.data.models.draft.DraftRealmModelV3
import com.erg.almagestdota.local_storage.internal.data.models.draft.GameStagesTeamData
import com.erg.almagestdota.local_storage.internal.data.models.leagues.LeagueRealmModel
import com.erg.almagestdota.local_storage.internal.data.models.main_settings.*
import com.erg.almagestdota.local_storage.internal.data.models.main_settings.MainPredictorRealmModelV4
import com.erg.almagestdota.local_storage.internal.data.models.main_settings.MainSettingsRealmModel
import com.erg.almagestdota.local_storage.internal.data.models.main_settings.UserHeroesPoolRealmModel
import com.erg.almagestdota.local_storage.internal.helpers.realm.deleteWithTransaction
import com.erg.almagestdota.local_storage.internal.helpers.realm.insertOrUpdateWithTransaction
import io.realm.Realm
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
internal class LocalStorageRepository @Inject constructor(
    private val realm: Realm,
    private val mapperToDraftRealmModel: DraftRealmModelV3.Mapper,
    private val mapperToRealmModelToDraftHistoryItem: DraftHistoryItem.MapperToRealmModel,
    private val mapperToLeague: LeagueRealmModel.Mapper,
    private val mapperToLeagueRealmModel: League.MapperToRealmModel,
    private val mapperToMainSettingsRealmModel: MainSettings.MapperToMainSettingsRealmModel,
    private val mapperToMainPredictorRealmModel: MainPredictor.MapperToMainPredictorRealmModel,
    private val mainPredictorRealmModel: MainPredictorRealmModelV4.Mapper,
    private val mapperToMainSettings: MainSettingsRealmModel.Mapper,
    private val mapperToUserHeroesPool: UserHeroesPoolRealmModel.Mapper,
    private val mapperToUserHeroesPoolRealmModel: UserHeroesPool.MapperToUserHeroesPoolRealmModel,

    ) : ILocalStorageContract {

    override var selectedTeam: TeamInfo = TeamInfo()
    override var analysis: List<DraftAnalysis> = listOf()
    override var reasons: List<DraftReasonType> = listOf()
    override var gameStagesData = GameStagesTeamData(listOf(), listOf())
    override var metaMatches: List<DotaMatch> = listOf()
    override var selectedMatches: List<DotaMatch> = listOf()
    override var matchesToShow: List<DotaMatch> = listOf()

    override var draftModel: DraftModel? = null
        set(value) {
            val draftModel = DraftModel(
                monthType = value!!.monthType,
                isDireBot = value.isDireBot,
                isAllPick = value.isAllPick,
                needToLoadPubMatches = value.needToLoadPubMatches,
                gameMode = value.gameMode,
                sequence = value.sequence,
                radiantTeam = TeamDraft(
                    picks = value.radiantTeam.picks.map { PlayerInfo(it.heroId, it.playerId) }.toMutableList(),
                    bans = value.radiantTeam.bans.map { it }.toMutableList(),
                    teamInfo = value.radiantTeam.teamInfo
                ).apply {
                    heroPool = value.radiantTeam.heroPool
                },
                direTeam = TeamDraft(
                    picks = value.direTeam.picks.map { PlayerInfo(it.heroId, it.playerId) }.toMutableList(),
                    bans = value.direTeam.bans.map { it }.toMutableList(),
                    teamInfo = value.direTeam.teamInfo
                ).apply {
                    heroPool = value.direTeam.heroPool
                },
            )
            field = draftModel
        }
    private val draftHistory = MutableStateFlow<List<DraftHistoryItem>?>(null)

    override fun getDraftHistoryStateFlow() = draftHistory.asStateFlow()

    override fun getLeagues(): List<League> {
        val instance = Realm.getDefaultInstance()
        val leagues = instance
            .where(LeagueRealmModel::class.java)
            .findAll()?.essentialMap(mapperToLeague)?.sortedBy { it.id } ?: listOf()
        instance.close()
        return leagues
    }

    override fun setLeagues(leagues: List<League>) {
        val instance = Realm.getDefaultInstance()
        for (item in leagues) {
            if (instance.where(LeagueRealmModel::class.java).equalTo("id", item.id).findFirst() == null) {
                instance.insertOrUpdateWithTransaction(item.essentialMap(mapperToLeagueRealmModel))
            }
        }
        GlobalVars.leagues = leagues
        instance.close()
    }

    override suspend fun initGlobalMainSettings(action: suspend () -> Unit) {
        GlobalVars.mainSettings = getMainSettings()!!
        GlobalVars.heroesAsMap = GlobalVars.mainSettings.heroes.associateBy { it.id }
        action.invoke()
        GlobalVars.mainPredictor = getPredictor()!!
        GlobalVars.leagues = getLeagues()
    }

    private fun getPredictor(): MainPredictor? {
        val instance = Realm.getDefaultInstance()
        val mainPredictor: MainPredictor? = instance
            .where(MainPredictorRealmModelV4::class.java)
            .findFirst()?.essentialMap(mainPredictorRealmModel)
        instance.close()
        return mainPredictor
    }

    override fun getMainSettings(): MainSettings? {
        val instance = Realm.getDefaultInstance()
        val mainSettings: MainSettings? = instance
            .where(MainSettingsRealmModel::class.java)
            .findFirst()?.essentialMap(mapperToMainSettings)
        instance.close()
        return mainSettings
    }

    override fun setMainSettings(mainSettings: MainSettings) {
        val instance = Realm.getDefaultInstance()
        instance.deleteWithTransaction(MainSettingsRealmModel::class.java)
        instance.insertOrUpdateWithTransaction(mainSettings.essentialMap(mapperToMainSettingsRealmModel))
        GlobalVars.mainSettings = mainSettings
        GlobalVars.heroesAsMap = GlobalVars.mainSettings.heroes.associateBy { it.id }
        instance.close()
    }

    override fun setMainPredictor(mainPredictor: MainPredictor) {
        val instance = Realm.getDefaultInstance()
        instance.deleteWithTransaction(MainPredictorRealmModelV4::class.java)
        instance.insertOrUpdateWithTransaction(mainPredictor.essentialMap(mapperToMainPredictorRealmModel))
        GlobalVars.mainPredictor = mainPredictor
        instance.close()
    }

    override fun loadDraftHistory() {
        val instance = Realm.getDefaultInstance()
        val list = instance
            .where(DraftRealmModelV3::class.java)
            .findAll() ?: listOf()
        draftHistory.value = list.essentialMap(mapperToDraftRealmModel).toMutableList()
        instance.close()
    }

    override fun addItemToDraftHistory(draftItem: DraftHistoryItem) {
        val instance = Realm.getDefaultInstance()
        instance.insertOrUpdateWithTransaction(draftItem.essentialMap(mapperToRealmModelToDraftHistoryItem))
        instance.close()
        val list = draftHistory.value.orEmpty().toMutableList()
        list.add(0, draftItem)
        draftHistory.update { list }
    }

    override fun getUserHeroesPool(): UserHeroesPool {
        val instance = Realm.getDefaultInstance()
        val pool = instance
            .where(UserHeroesPoolRealmModel::class.java)
            .findFirst() ?: UserHeroesPoolRealmModel()
        instance.close()
        return pool.essentialMap(mapperToUserHeroesPool)
    }

    override fun setUserHeroesPool(userHeroesPool: UserHeroesPool) {
        val instance = Realm.getDefaultInstance()
        instance.insertOrUpdateWithTransaction(userHeroesPool.essentialMap(mapperToUserHeroesPoolRealmModel))
        instance.close()
    }
}