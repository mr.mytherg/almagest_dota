package com.erg.almagestdota.local_storage.external.models.draft

import android.os.Parcelable
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.local_storage.external.raw_models.HeroRoleRaw
import com.erg.almagestdota.local_storage.internal.data.models.draft.HeroRoleRealmModel
import kotlinx.android.parcel.Parcelize
import javax.inject.Inject

@Parcelize
data class HeroRole(
    val roleType: RoleType,
    val pickRate: Double,
    val winRatePercentage: Double,
    var matchCount: Int,
) : Parcelable {
    companion object {
        const val ROLE_MINIMUM_PERCENTAGE = 20.0

        fun toModel(model: HeroRole): HeroRoleRaw {
            return HeroRoleRaw(
                matchCount = model.matchCount,
                role = model.roleType.name,
                frequency_percentage = model.pickRate,
                win_rate_percentage = model.winRatePercentage,
            )
        }
    }

    var isPossibleRole = false
    var winCount = 0
}

fun List<HeroRole>.isHeroFitsForRole(roleType: RoleType): Boolean {
    val role = this.find { roleType == it.roleType }!!
    return if (role.roleType.number <= 2) {
        role.pickRate >= 15
    } else if (role.roleType.number == 3) {
        role.pickRate >= 7
    } else {
        role.pickRate >= 10
    }

//    val mostCommonRole: HeroRole = defineMostCommonRole(this)
//    return isRoleBetterThanMostCommonRole(role, mostCommonRole)
}

private fun defineMostCommonRole(roles: List<HeroRole>): HeroRole {
    var mostCommonRole: HeroRole = roles.first()
    for (item in roles)
        if (item.pickRate > mostCommonRole.pickRate) {
            mostCommonRole = item
        }
    return mostCommonRole
}

fun List<HeroRole>.countNumberOfPossibleRoles(): Int {
    var count = 0
    for (role in this) {
        if (this.isHeroFitsForRole(role.roleType))
            count++
    }
//    val mostCommonRole: HeroRole = defineMostCommonRole(this)
//    for (role in this) {
//        if (isRoleBetterThanMostCommonRole(role, mostCommonRole)) {
//            count++
//        }
//    }
    return count
}

private fun isRoleBetterThanMostCommonRole(role: HeroRole, mostCommonRole: HeroRole): Boolean {
    return role.pickRate >= 8 &&
        (role.winRatePercentage >= mostCommonRole.winRatePercentage || role.winRatePercentage >= 50) ||
        role.pickRate >= HeroRole.ROLE_MINIMUM_PERCENTAGE
}