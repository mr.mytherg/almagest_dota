package com.erg.almagestdota.local_storage.internal.data.models.draft

import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.local_storage.external.models.draft.DraftHistoryItem
import com.erg.almagestdota.local_storage.external.models.draft.main.DraftModel
import com.google.gson.Gson
import io.realm.FieldAttribute
import io.realm.RealmObject
import io.realm.RealmSchema
import io.realm.annotations.PrimaryKey
import java.util.*
import javax.inject.Inject

internal open class DraftRealmModelV3 : RealmObject() {

    @PrimaryKey
    var primaryKey: String = ""
    var dateTimestamp: Long = 0L
    var draftModel: String = ""

    class Mapper @Inject constructor() : EssentialMapper<DraftRealmModelV3, DraftHistoryItem>() {
        override fun transform(raw: DraftRealmModelV3): DraftHistoryItem {
            val draftModel = Gson().fromJson(raw.draftModel, DraftModel::class.java)
            return DraftHistoryItem(
                primaryKey = raw.primaryKey,
                dateTimestamp = raw.dateTimestamp,
                draftModel = draftModel
            )
        }
    }

    companion object {
        const val MODEL_NAME = "DraftRealmModelV3"
        fun createModel(schema: RealmSchema) {
            if (schema[MODEL_NAME] != null) {
                return
            }
            val model = schema.create(MODEL_NAME)
            model?.apply {
                addField("primaryKey", String::class.java, FieldAttribute.PRIMARY_KEY, FieldAttribute.REQUIRED)
                addField("dateTimestamp", Long::class.java, FieldAttribute.REQUIRED)
                addField("draftModel", String::class.java, FieldAttribute.REQUIRED)
            }
        }
    }
}