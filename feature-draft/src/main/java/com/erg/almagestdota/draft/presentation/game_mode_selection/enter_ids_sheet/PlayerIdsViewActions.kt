package com.erg.almagestdota.draft.presentation.game_mode_selection.enter_ids_sheet

internal sealed class PlayerIdsViewActions {
    class Submit(
        val playerId1: String,
        val playerId2: String,
        val playerId3: String,
        val playerId4: String,
        val playerId5: String,
    ) : PlayerIdsViewActions()

    object OnBackClick : PlayerIdsViewActions()
}