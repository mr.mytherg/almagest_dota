package com.erg.almagestdota.draft.presentation.game_mode_selection

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.draft.presentation.draft_history.DraftHistoryFragmentDirections

internal object GameModeSelectionScreen : Screen<NavDirections>(
    route = DraftHistoryFragmentDirections.toGameModeSelectionFragment(),
)