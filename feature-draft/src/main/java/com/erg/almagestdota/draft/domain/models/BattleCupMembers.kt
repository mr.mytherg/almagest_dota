package com.erg.almagestdota.draft.domain.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BattleCupMembers(
    val playerIds: List<Long>
): Parcelable