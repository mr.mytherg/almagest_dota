package com.erg.almagestdota.draft.presentation.game_mode_selection.enter_ids_sheet

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.ActivityManager
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.heroselector.databinding.HeroSelectorSheetBinding
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.draft.R
import com.erg.almagestdota.draft.databinding.PlayerIdsSheetBinding
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdpta.core_navigation.external.helpers.BottomSheetFragment
import com.erg.almagestdpta.core_navigation.external.helpers.navHostFragmentManager
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import com.google.android.material.bottomsheet.BottomSheetBehavior
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
internal class PlayerIdsSheet : BottomSheetFragment(R.layout.player_ids_sheet),
    BackPressListener {

    private val args by navArgs<PlayerIdsSheetArgs>()

    private val binding by viewBinding { PlayerIdsSheetBinding.bind(requireContentView()) }
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    @Inject
    lateinit var viewModelFactory: PlayerIdsViewModel.Factory
    private val viewModel by viewModels<PlayerIdsViewModel> {
        PlayerIdsViewModel.provideFactory(
            isRadiant = args.isRadiant,
            assistedFactory = viewModelFactory
        )
    }

    override fun onBackPressed() {
        viewModel.obtainAction(PlayerIdsViewActions.OnBackClick)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
        viewModel.viewEvent.setResultListener(this)
    }

    override fun onBottomSheetStateChanged(bottomSheet: View, newState: Int) {
        super.onBottomSheetStateChanged(bottomSheet, newState)
        if (newState == BottomSheetBehavior.STATE_HIDDEN) {
            onClose()
        }
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun onClose(result: Bundle = PlayerIdsScreen.Result.createResultCanceled()) {
        val fm = navHostFragmentManager()
        navController.popBackStack()
        fm.setFragmentResult(PlayerIdsScreen.TAG, result)
    }

    private inner class ViewsConfigurator {

        fun initStartState() = with(binding) {
            tvSubmit.clicksWithDebounce {
                viewModel.obtainAction(
                    PlayerIdsViewActions.Submit(
                        playerId1 = etId1.text.toString(),
                        playerId2 = etId2.text.toString(),
                        playerId3 = etId3.text.toString(),
                        playerId4 = etId4.text.toString(),
                        playerId5 = etId5.text.toString(),
                    )
                )
            }
        }

        fun renderState(state: PlayerIdsViewState) {
            when (state) {
                is PlayerIdsViewState.DefaultState -> renderDefaultState(state)
            }
        }

        private fun renderDefaultState(state: PlayerIdsViewState.DefaultState) = with(binding) {

        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
                else -> handleCustomEvents(event)
            }
        }

        private fun handleCustomEvents(event: ViewEvent) {
            when (event) {
                is PlayerIdsViewEvents.BackClickEvent -> {
                    onClose()
                }
                is PlayerIdsViewEvents.ResultEvent -> {
                    onClose(PlayerIdsScreen.Result.createResult(args.isRadiant, event.battleCupMembers))
                }
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            viewModel.viewEvent.setResultListener(this@PlayerIdsSheet)
            navController.navigateViaScreenRoute(screen)
        }
    }
}