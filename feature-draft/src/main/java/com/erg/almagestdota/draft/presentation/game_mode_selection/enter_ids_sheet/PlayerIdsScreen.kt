package com.erg.almagestdota.draft.presentation.game_mode_selection.enter_ids_sheet

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.draft.domain.models.BattleCupMembers
import com.erg.almagestdota.draft.presentation.game_mode_selection.GameModeSelectionFragmentDirections

class PlayerIdsScreen(
    isRadiant: Boolean
) : Screen<NavDirections>(
    route = GameModeSelectionFragmentDirections.toPlayerIdsSheet(isRadiant),
    requestKey = TAG
) {
    companion object {
        const val TAG = "PlayerIdsSheet"
        private const val RESULT_KEY = "RESULT_KEY"
        private const val SIDE_KEY = "SIDE_KEY"
        private const val DATA_KEY = "DATA_KEY"

        fun getResult(data: Bundle): Result = Result.enumValueOf(data.getString(RESULT_KEY))
        fun isRadiant(data: Bundle): Boolean = data.getBoolean(SIDE_KEY)
        fun getData(data: Bundle): BattleCupMembers = data.getParcelable(DATA_KEY)!!
    }

    enum class Result {
        SELECTED,
        CANCELED;

        companion object {
            fun enumValueOf(value: String?): Result {
                return values().firstOrNull { it.name == value } ?: CANCELED
            }

            internal fun createResult(isRadiant: Boolean = false, data: BattleCupMembers): Bundle {
               return bundleOf(RESULT_KEY to SELECTED.name, SIDE_KEY to isRadiant, DATA_KEY to data)
            }

            internal fun createResultCanceled(): Bundle {
               return bundleOf(RESULT_KEY to CANCELED.name)
            }
        }
    }
}