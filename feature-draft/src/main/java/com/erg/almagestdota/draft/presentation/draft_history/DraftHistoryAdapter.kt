package com.erg.almagestdota.draft.presentation.draft_history

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.complexadapter.viewHolders.matches.DraftHistoryViewHolder

class DraftHistoryAdapter(
    private val complexListener: ComplexListener? = null,
) : ListAdapter<ComplexItem.DraftHistoryItem, DraftHistoryViewHolder>(diffCallback) {

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<ComplexItem.DraftHistoryItem>() {
            override fun areItemsTheSame(
                oldItem: ComplexItem.DraftHistoryItem,
                newItem: ComplexItem.DraftHistoryItem
            ): Boolean {
                return oldItem.key == newItem.key
            }

            override fun areContentsTheSame(
                oldItem: ComplexItem.DraftHistoryItem,
                newItem: ComplexItem.DraftHistoryItem
            ): Boolean {
                return oldItem == newItem
            }
        }
    }

    private val pool = RecyclerView.RecycledViewPool()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DraftHistoryViewHolder {
        return DraftHistoryViewHolder.create(parent, pool, complexListener)
    }

    override fun onBindViewHolder(holder: DraftHistoryViewHolder, position: Int) {
        holder.bind(currentList[position])
    }
}