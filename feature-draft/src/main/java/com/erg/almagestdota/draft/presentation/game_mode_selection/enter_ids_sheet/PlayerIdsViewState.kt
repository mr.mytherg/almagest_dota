package com.erg.almagestdota.draft.presentation.game_mode_selection.enter_ids_sheet

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem

internal sealed class PlayerIdsViewState : ViewState() {

    object DefaultState: PlayerIdsViewState()
}