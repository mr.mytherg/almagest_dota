package com.erg.almagestdota.draft.presentation.draft_history

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.complexList
import com.erg.almagestdota.complexadapter.draftHistoryItem
import com.erg.almagestdota.complexadapter.itemDefault
import com.erg.almagestdota.draft.domain.Interactor
import com.erg.almagestdota.draft.presentation.game_mode_selection.GameModeSelectionScreen
import com.erg.almagestdota.drafting.presentation.DraftScreen
import com.erg.almagestdota.local_storage.external.models.draft.DraftHistoryItem
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerInfo
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
internal class DraftHistoryViewModel @Inject constructor(
    private val stringProvider: StringProvider,
    private val interactor: Interactor,
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data -> }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow<DraftHistoryViewState?>(null)
    val viewState = viewStateMutable.asStateFlow()

    private val loadingStateMutable = MutableStateFlow<Loading>(Loading.Disabled)
    val loadingState = loadingStateMutable.asStateFlow()

    init {
        updateDraftHistory()
        interactor.getDraftHistoryStateFlow().filterNotNull().onEach {
            stateConfigurator.draftHistory = it
            viewStateMutable.value = stateConfigurator.defineFragmentState()
        }.launchIn(viewModelScope)
    }

    private fun updateDraftHistory() {
        interactor.loadDraftHistory()
    }

    fun obtainAction(action: DraftHistoryViewActions) {
        when (action) {
            is DraftHistoryViewActions.UpdateList -> updateDraftHistory()
            is DraftHistoryViewActions.OpenDraftResult -> {
                val draft = stateConfigurator.draftHistory.find { it.dateTimestamp.toString() == action.key } ?: return
                interactor.setDraftModel(draft.draftModel)
                val screen = DraftScreen(DraftHistoryFragmentDirections.toDraftFragment())
                emit(viewEventMutable, ViewEvent.Navigation(screen))
            }
            is DraftHistoryViewActions.OpenGameModeSelection -> emit(
                viewEventMutable,
                ViewEvent.Navigation(GameModeSelectionScreen)
            )
        }
    }

    private inner class StateConfigurator {
        var draftHistory = listOf<DraftHistoryItem>()

        fun defineFragmentState(): DraftHistoryViewState {
            return if (draftHistory.isEmpty()) {
                DraftHistoryViewState.EmptyList
            } else {
                DraftHistoryViewState.FilledList(getComplexList())
            }
        }

        private fun getComplexList(): List<ComplexItem> {
            return complexList {
                for (draftItem in draftHistory) {
                    val draft = draftItem.draftModel
                    draftHistoryItem(key = draftItem.dateTimestamp.toString()) {
                        title = stringProvider.getString(draft.gameMode.getResource())
                        radiantName = draft.radiantTeam.teamInfo.name
                        direName = draft.direTeam.teamInfo.name
                        radiantHeroes = getHeroesList(draft.radiantTeam.picks)
                        direHeroes = getHeroesList(draft.direTeam.picks)
                    }
                }
            }
        }

        private fun getHeroesList(heroes: List<PlayerInfo>): List<ComplexItem> {
            return complexList {
                heroes.map {
                    val hero = it.heroId.getHeroById()
                    itemDefault(key = hero.id.toString()) {
                        title = hero.name
                        url = hero.imageLink
                        hasPlaceholder = false
                    }
                }
            }
        }
    }
}