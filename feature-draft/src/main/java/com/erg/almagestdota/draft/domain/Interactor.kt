package com.erg.almagestdota.draft.domain

import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.main.DraftModel
import javax.inject.Inject

internal class Interactor @Inject constructor(
    private val localStorage: ILocalStorageContract,
) {
    fun getDraftHistoryStateFlow() = localStorage.getDraftHistoryStateFlow()

    fun setDraftModel(draftModel: DraftModel) {
        localStorage.draftModel = draftModel
    }

    fun loadDraftHistory() {
        localStorage.loadDraftHistory()
    }
}