package com.erg.almagestdota.draft.presentation.game_mode_selection

import com.erg.almagestdota.base.external.viewEvent.ViewEvent

internal sealed class GameModeSelectionViewEvents : ViewEvent() {
    object BackClickEvent : GameModeSelectionViewEvents()
}