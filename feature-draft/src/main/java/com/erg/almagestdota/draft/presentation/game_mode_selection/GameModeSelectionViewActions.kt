package com.erg.almagestdota.draft.presentation.game_mode_selection

internal sealed class GameModeSelectionViewActions {

    object StartDraft : GameModeSelectionViewActions()
    object PubMatchesCheckBox : GameModeSelectionViewActions()
    object AllPickCheckBox : GameModeSelectionViewActions()
    object FirstPickCheckBox : GameModeSelectionViewActions()
    object BotCheckBox : GameModeSelectionViewActions()
    object OpenSelectRadiantTeam : GameModeSelectionViewActions()
    object OpenSelectDireTeam : GameModeSelectionViewActions()
    object UserMode : GameModeSelectionViewActions()
    object BotMode : GameModeSelectionViewActions()
    object TeamMode : GameModeSelectionViewActions()
    object BattleCupMode : GameModeSelectionViewActions()
    object OnBackClick : GameModeSelectionViewActions()
    class SelectMonthItem(val key: String) : GameModeSelectionViewActions()
}