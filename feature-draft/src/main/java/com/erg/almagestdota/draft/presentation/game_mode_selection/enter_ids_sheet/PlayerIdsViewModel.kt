package com.erg.almagestdota.draft.presentation.game_mode_selection.enter_ids_sheet

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.NotificationStatus
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.heroselector.R
import com.erg.almagestdota.complexadapter.complexList
import com.erg.almagestdota.complexadapter.itemDefault
import com.erg.almagestdota.draft.domain.models.BattleCupMembers
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

internal class PlayerIdsViewModel @AssistedInject constructor(
    @Assisted private val isRadiant: Boolean,
    private val stringProvider: StringProvider
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data -> }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    fun obtainAction(action: PlayerIdsViewActions) {
        when (action) {
            is PlayerIdsViewActions.Submit -> {
                val battleCupMembers = BattleCupMembers(
                    listOf(
                        getId(action.playerId1),
                        getId(action.playerId2),
                        getId(action.playerId3),
                        getId(action.playerId4),
                        getId(action.playerId5),
                    )
                )
                emit(viewEventMutable, PlayerIdsViewEvents.ResultEvent( battleCupMembers))
            }
            is PlayerIdsViewActions.OnBackClick -> {
                emit(viewEventMutable, PlayerIdsViewEvents.BackClickEvent)
            }
        }
    }

    private fun getId(playerId: String): Long {
        return try {
            playerId.toLong()
        } catch (e: Exception) {
            val text = stringProvider.getString(com.erg.almagestdota.draft.R.string.snack_error_invalid_id, playerId)
            emit(viewEventMutable, ViewEvent.AlmagestSnackbar(text, NotificationStatus.ERROR))
            0L
        }
    }

    private inner class StateConfigurator {
        fun defineFragmentState(): PlayerIdsViewState {
            return PlayerIdsViewState.DefaultState
        }
    }

    companion object {
        fun provideFactory(
            isRadiant: Boolean,
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create(isRadiant) as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(isRadiant: Boolean): PlayerIdsViewModel
    }
}