package com.erg.almagestdota.draft.presentation.draft_history

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem

internal sealed class DraftHistoryViewState : ViewState() {

    object EmptyList : DraftHistoryViewState()
    class FilledList(val games: List<ComplexItem>) : DraftHistoryViewState()
}