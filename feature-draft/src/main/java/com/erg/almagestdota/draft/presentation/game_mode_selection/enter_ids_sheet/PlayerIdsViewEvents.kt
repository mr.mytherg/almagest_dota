package com.erg.almagestdota.draft.presentation.game_mode_selection.enter_ids_sheet

import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.draft.domain.models.BattleCupMembers

internal sealed class PlayerIdsViewEvents : ViewEvent() {
    object BackClickEvent : PlayerIdsViewEvents()
    class ResultEvent(val battleCupMembers: BattleCupMembers) : PlayerIdsViewEvents()
}