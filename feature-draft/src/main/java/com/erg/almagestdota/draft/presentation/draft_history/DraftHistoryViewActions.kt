package com.erg.almagestdota.draft.presentation.draft_history

internal sealed class DraftHistoryViewActions {

    class OpenDraftResult(val key: String) : DraftHistoryViewActions()
    object OpenGameModeSelection : DraftHistoryViewActions()

    object UpdateList : DraftHistoryViewActions()
}