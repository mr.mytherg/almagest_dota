package com.erg.almagestdota.draft.presentation.draft_history

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.draft.R
import com.erg.almagestdota.draft.databinding.DraftHistoryFragmentBinding
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
internal class DraftHistoryFragment : Fragment(R.layout.draft_history_fragment) {

    companion object {
        const val TAG = "DraftHistoryFragment"
    }

    private val binding by viewBinding(DraftHistoryFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewModel by hiltNavGraphViewModels<DraftHistoryViewModel>(R.id.graphDraft)
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
        viewModel.viewEvent.setResultListener(this)
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).filterNotNull().onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.loadingState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach {
            binding.loadingView.isVisible = it is Loading.Enabled
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private inner class ViewsConfigurator {
        private val draftHistoryAdapter = DraftHistoryAdapter(
            complexListener = object : ComplexListener {
                override fun onClick(item: ComplexItem) {
                    viewModel.obtainAction(DraftHistoryViewActions.OpenDraftResult(item.key))
                }
            }
        )

        fun initStartState() = with(binding) {
            tvOpenGameMode.clicksWithDebounce {
                viewModel.obtainAction(DraftHistoryViewActions.OpenGameModeSelection)
            }
        }

        fun renderState(state: DraftHistoryViewState) {
            when (state) {
                is DraftHistoryViewState.FilledList -> renderFilledListState(state)
                is DraftHistoryViewState.EmptyList -> renderEmptyListState()
            }
        }

        private fun renderEmptyListState() = with(binding) {
            rvHistory.isVisible = false
            tvDraftsEmpty.isVisible = true
        }

        private fun renderFilledListState(state: DraftHistoryViewState.FilledList) = with(binding) {
            rvHistory.isVisible = true
            tvDraftsEmpty.isVisible = false

            if (rvHistory.adapter == null) {
                rvHistory.adapter = draftHistoryAdapter
            }
            draftHistoryAdapter.submitList(state.games as List<ComplexItem.DraftHistoryItem>)
        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            viewModel.viewEvent.setResultListener(this@DraftHistoryFragment)
            navController.navigateViaScreenRoute(screen)
        }
    }
}