package com.erg.almagestdota.draft.presentation.game_mode_selection

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.local_storage.external.models.draft.GameModeType
import com.erg.almagestdota.local_storage.external.models.draft.MonthType

internal sealed class GameModeSelectionViewState : ViewState() {

    class DefaultState(
        val needToLoadPubMatches: Boolean,
        val gameMode: GameModeType,
        val isBot: Boolean,
        val isAllPick: Boolean,
        val isRadiantFirstPick: Boolean,
        val monthList: List<ComplexItem>,
        val monthType: MonthType,
        val radiantName: String,
        val direName: String
    ) : GameModeSelectionViewState()
}