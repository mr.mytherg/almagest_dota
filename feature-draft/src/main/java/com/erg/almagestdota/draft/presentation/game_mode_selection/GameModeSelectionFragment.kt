package com.erg.almagestdota.draft.presentation.game_mode_selection

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.complexadapter.ComplexAdapter
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.draft.R
import com.erg.almagestdota.draft.databinding.GameModeSelectionFragmentBinding
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdota.local_storage.external.models.draft.*
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.game_mode_selection_fragment.*
import kotlinx.android.synthetic.main.game_mode_selection_fragment.view.*
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
internal class GameModeSelectionFragment : Fragment(R.layout.game_mode_selection_fragment), BackPressListener {

    companion object {
        const val TAG = "GameModeSelectionFragment"
    }

    private val binding by viewBinding(GameModeSelectionFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewModel by viewModels<GameModeSelectionViewModel>()
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
        viewModel.viewEvent.setResultListener(this)
    }

    override fun onBackPressed() {
        viewModel.obtainAction(GameModeSelectionViewActions.OnBackClick)
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).filterNotNull().onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.loadingState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach {
            binding.loadingView.isVisible = it is Loading.Enabled
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private inner class ViewsConfigurator {
        private val complexAdapter = ComplexAdapter(
            complexListener = object : ComplexListener {
                override fun onClick(item: ComplexItem) {
                    viewModel.obtainAction(GameModeSelectionViewActions.SelectMonthItem(item.key))
                }
            }
        )

        fun initStartState() = with(binding) {
            ivBack.clicksWithDebounce {
                viewModel.obtainAction(GameModeSelectionViewActions.OnBackClick)
            }
            tvUser.clicksWithDebounce {
                viewModel.obtainAction(GameModeSelectionViewActions.UserMode)
            }
            tvBot.clicksWithDebounce {
                viewModel.obtainAction(GameModeSelectionViewActions.BotMode)
            }
            cbBot.clicksWithDebounce {
                viewModel.obtainAction(GameModeSelectionViewActions.BotCheckBox)
            }
            tvBattleCup.clicksWithDebounce {
                viewModel.obtainAction(GameModeSelectionViewActions.BattleCupMode)
            }
            tvTeam.clicksWithDebounce {
                viewModel.obtainAction(GameModeSelectionViewActions.TeamMode)
            }
            cbAllPick.clicksWithDebounce {
                viewModel.obtainAction(GameModeSelectionViewActions.AllPickCheckBox)
            }
            cbPubMatches.clicksWithDebounce {
                viewModel.obtainAction(GameModeSelectionViewActions.PubMatchesCheckBox)
            }
            cbFirstPick.clicksWithDebounce {
                viewModel.obtainAction(GameModeSelectionViewActions.FirstPickCheckBox)
            }
            tvStartDraft.clicksWithDebounce {
                viewModel.obtainAction(GameModeSelectionViewActions.StartDraft)
            }
            tvRadiant.clicksWithDebounce {
                viewModel.obtainAction(GameModeSelectionViewActions.OpenSelectRadiantTeam)
            }
            tvDire.clicksWithDebounce {
                viewModel.obtainAction(GameModeSelectionViewActions.OpenSelectDireTeam)
            }
            tvTitle.text = getString(R.string.game_mode_toolbar_title)
        }

        fun renderState(state: GameModeSelectionViewState) {
            when (state) {
                is GameModeSelectionViewState.DefaultState -> renderDefaultState(state)
            }
        }

        private fun renderDefaultState(state: GameModeSelectionViewState.DefaultState) = with(binding) {
            cbFirstPick.isVisible = true
            cbBot.isVisible = state.gameMode.hasTeams()
            cbBot.isChecked = state.isBot
            tvRadiant.isVisible = state.gameMode.hasTeams()
            tvDire.isVisible = state.gameMode.hasTeams()
            cbPubMatches.isVisible = state.gameMode.hasTeams()
            rvMonthType.adapter = complexAdapter.apply {
                submitList(state.monthList)
            }

            cbPubMatches.isEnabled = !state.gameMode.isBattleCup()
            if (state.gameMode.isBattleCup()) {
                cbPubMatches.isChecked = true
                cbPubMatches.alpha = 0.4f
            } else
                cbPubMatches.alpha = 1.0f
            cbPubMatches.isChecked = state.needToLoadPubMatches
            rvMonthType.isVisible = state.gameMode.hasTeams() && cbPubMatches.isChecked

            cbFirstPick.isChecked = state.isRadiantFirstPick
            cbAllPick.isChecked = state.isAllPick

            if (state.gameMode.isTeam()) {
                tvRadiant.text = if (state.radiantName.isEmpty()) {
                    getString(R.string.team_game_mode_name, getString(R.string.radiant), getString(R.string.team_name_placeholder))
                } else {
                    getString(R.string.team_game_mode_name, getString(R.string.radiant), state.radiantName)
                }
                tvDire.text = if (state.direName.isEmpty()) {
                    getString(R.string.team_game_mode_name, getString(R.string.dire), getString(R.string.team_name_placeholder))
                } else {
                    getString(R.string.team_game_mode_name, getString(R.string.dire), state.direName)
                }
            } else if (state.gameMode.isBattleCup()) {
                tvRadiant.text = if (state.radiantName.isEmpty()) {
                    getString(R.string.team_game_mode_name, getString(R.string.radiant), getString(R.string.battle_cup_team_name_placeholder))
                } else {
                    getString(R.string.team_game_mode_name, getString(R.string.radiant), getString(R.string.battle_cup_team_name_done))
                }
                tvDire.text = if (state.direName.isEmpty()) {
                    getString(R.string.team_game_mode_name, getString(R.string.dire), getString(R.string.battle_cup_team_name_placeholder))
                } else {
                    getString(R.string.team_game_mode_name, getString(R.string.dire), getString(R.string.battle_cup_team_name_done))
                }
            }


            tvGameModeDescription.text = when (state.gameMode) {
                GameModeType.TEAM -> getString(R.string.game_mode_description_team)
                GameModeType.USER -> getString(R.string.game_mode_description_user)
                GameModeType.BOT -> getString(R.string.game_mode_description_bot)
                GameModeType.BATTLE_CUP -> getString(R.string.game_mode_description_battle_cup)
                else -> ""
            }

            when (state.gameMode) {
                GameModeType.USER -> {
                    setViewSelected(tvUser)
                    setViewUnselected(tvBattleCup)
                    setViewUnselected(tvTeam)
                    setViewUnselected(tvBot)
                }
                GameModeType.BOT -> {
                    setViewSelected(tvBot)
                    setViewUnselected(tvBattleCup)
                    setViewUnselected(tvUser)
                    setViewUnselected(tvTeam)
                }
                GameModeType.TEAM -> {
                    setViewSelected(tvTeam)
                    setViewUnselected(tvBattleCup)
                    setViewUnselected(tvBot)
                    setViewUnselected(tvUser)
                }
                GameModeType.BATTLE_CUP -> {
                    setViewSelected(tvBattleCup)
                    setViewUnselected(tvTeam)
                    setViewUnselected(tvBot)
                    setViewUnselected(tvUser)
                }
                else -> Unit
            }
        }

        private fun setViewSelected(selectableTextView: TextView) {
            selectableTextView.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
            selectableTextView.setBackgroundResource(R.drawable.bg_rect_filled_rounded)
        }

        private fun setViewUnselected(selectableTextView: TextView) {
            selectableTextView.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorPrimary))
            selectableTextView.setBackgroundResource(R.drawable.bg_rect_filled_with_border_rounded)
        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
                else -> handleCustomEvents(event)
            }
        }

        private fun handleCustomEvents(event: ViewEvent) {
            when (event) {
                is GameModeSelectionViewEvents.BackClickEvent -> {
                    navController.popBackStack()
                }
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            viewModel.viewEvent.setResultListener(this@GameModeSelectionFragment)
            navController.navigateViaScreenRoute(screen)
        }
    }
}