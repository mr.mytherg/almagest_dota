package com.erg.almagestdota.draft.presentation.game_mode_selection

import androidx.lifecycle.ViewModel
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.NotificationStatus
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.complexadapter.complexList
import com.erg.almagestdota.complexadapter.filterItem
import com.erg.almagestdota.draft.R
import com.erg.almagestdota.draft.domain.Interactor
import com.erg.almagestdota.draft.domain.models.BattleCupMembers
import com.erg.almagestdota.draft.presentation.game_mode_selection.enter_ids_sheet.PlayerIdsScreen
import com.erg.almagestdota.drafting.presentation.DraftScreen
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.*
import com.erg.almagestdota.local_storage.external.models.draft.main.DraftModel
import com.erg.almagestdota.local_storage.external.models.draft.main.MemberInfo
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamDraft
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.main_settings.MainSettings
import com.erg.almagestdota.stats.presentation.team_selector.TeamSelectionScreen
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import com.google.gson.Gson
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

@HiltViewModel
internal class GameModeSelectionViewModel @Inject constructor(
    private val stringProvider: StringProvider,
    private val interactor: Interactor,
    private val localStorage: ILocalStorageContract,
    private val gson: Gson,
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data ->
        when (resultKey) {
            TeamSelectionScreen.TAG -> {
                if (TeamSelectionScreen.getResult(data) == TeamSelectionScreen.Result.SELECTED) {
                    if (TeamSelectionScreen.isRadiant(data)) {
                        stateConfigurator.radiantTeam = getTeamDraft(TeamSelectionScreen.getSelectedTeam(data))
                    } else {
                        stateConfigurator.direTeam = getTeamDraft(TeamSelectionScreen.getSelectedTeam(data))
                    }
                    viewStateMutable.value = stateConfigurator.defineFragmentState()
                }
            }
            PlayerIdsScreen.TAG -> {
                if (PlayerIdsScreen.getResult(data) == PlayerIdsScreen.Result.SELECTED) {
                    val isRadiant = PlayerIdsScreen.isRadiant(data)
                    if (isRadiant) {
                        stateConfigurator.radiantTeam = createTeam(PlayerIdsScreen.getData(data), "Radiant")
                    } else {
                        stateConfigurator.direTeam = createTeam(PlayerIdsScreen.getData(data), "Dire")
                    }
                    viewStateMutable.value = stateConfigurator.defineFragmentState()
                }
            }
        }
    }

    val viewEvent: ResultFlow = viewEventMutable

    private val loadingStateMutable = MutableStateFlow<Loading>(Loading.Disabled)
    val loadingState = loadingStateMutable.asStateFlow()

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    fun obtainAction(action: GameModeSelectionViewActions) {
        when (action) {
            is GameModeSelectionViewActions.OnBackClick -> {
                emit(viewEventMutable, GameModeSelectionViewEvents.BackClickEvent)
            }
            is GameModeSelectionViewActions.SelectMonthItem -> {
                val monthType = MonthType.valueOf(action.key)
                if (stateConfigurator.monthType == monthType) return
                stateConfigurator.monthType = monthType
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is GameModeSelectionViewActions.OpenSelectRadiantTeam -> {
                if (stateConfigurator.gameMode.isBattleCup()) {
                    val screen = PlayerIdsScreen(true)
                    emit(viewEventMutable, ViewEvent.Navigation(screen))
                } else {
                    if (localStorage.metaMatches.isEmpty())
                        showSelectTournamentsError()
                    else
                        emit(viewEventMutable, ViewEvent.Navigation(TeamSelectionScreen(true)))
                }
            }
            is GameModeSelectionViewActions.OpenSelectDireTeam -> {
                if (stateConfigurator.gameMode.isBattleCup()) {
                    val screen = PlayerIdsScreen(false)
                    emit(viewEventMutable, ViewEvent.Navigation(screen))
                } else {
                    if (localStorage.metaMatches.isEmpty())
                        showSelectTournamentsError()
                    else
                        emit(viewEventMutable, ViewEvent.Navigation(TeamSelectionScreen(false)))
                }
            }
            is GameModeSelectionViewActions.StartDraft -> {
                if (stateConfigurator.gameMode.hasTeams()) {
                    if (localStorage.metaMatches.isEmpty())
                        showSelectTournamentsError()
                    else if (stateConfigurator.radiantTeam.teamInfo.name.isEmpty()) {
                        val text = stringProvider.getString(R.string.snack_error_select_team_for_radiant)
                        emit(viewEventMutable, ViewEvent.AlmagestSnackbar(text, NotificationStatus.ERROR))
                    } else if (stateConfigurator.direTeam.teamInfo.name.isEmpty()) {
                        val text = stringProvider.getString(R.string.snack_error_select_team_for_dire)
                        emit(viewEventMutable, ViewEvent.AlmagestSnackbar(text, NotificationStatus.ERROR))
                    } else {
                        openDraft()
                    }
                } else {
                    stateConfigurator.radiantTeam = TeamDraft()
                    stateConfigurator.direTeam = TeamDraft()
                    openDraft()
                }
            }
            is GameModeSelectionViewActions.AllPickCheckBox -> {
                stateConfigurator.isAllPick = !stateConfigurator.isAllPick
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is GameModeSelectionViewActions.PubMatchesCheckBox -> {
                stateConfigurator.needToLoadPubMatches = !stateConfigurator.needToLoadPubMatches
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is GameModeSelectionViewActions.BotCheckBox -> {
                stateConfigurator.isBot = !stateConfigurator.isBot
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is GameModeSelectionViewActions.UserMode -> {
                if (stateConfigurator.gameMode == GameModeType.USER) return
                stateConfigurator.gameMode = GameModeType.USER
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is GameModeSelectionViewActions.BotMode -> {
                if (stateConfigurator.gameMode == GameModeType.BOT) return
                stateConfigurator.gameMode = GameModeType.BOT
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is GameModeSelectionViewActions.BattleCupMode -> {
                if (stateConfigurator.gameMode == GameModeType.BATTLE_CUP) return
                stateConfigurator.radiantTeam = TeamDraft()
                stateConfigurator.direTeam = TeamDraft()
                stateConfigurator.gameMode = GameModeType.BATTLE_CUP
                stateConfigurator.needToLoadPubMatches = true
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is GameModeSelectionViewActions.TeamMode -> {
                if (stateConfigurator.gameMode == GameModeType.TEAM) return
                stateConfigurator.radiantTeam = TeamDraft()
                stateConfigurator.direTeam = TeamDraft()
                stateConfigurator.gameMode = GameModeType.TEAM
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is GameModeSelectionViewActions.FirstPickCheckBox -> {
                stateConfigurator.isRadiantFirstPick = !stateConfigurator.isRadiantFirstPick
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
        }
    }

    private fun showSelectTournamentsError() {
        val text = stringProvider.getString(R.string.stats_snack_error_select_tournaments)
        emit(viewEventMutable, ViewEvent.AlmagestSnackbar(text, NotificationStatus.ERROR))
    }

    private fun createTeam(data: BattleCupMembers, name: String): TeamDraft {
        return TeamDraft(
            teamInfo = TeamInfo(
                name = name,
                members = listOf(
                    data.playerIds[0],
                    data.playerIds[1],
                    data.playerIds[2],
                    data.playerIds[3],
                    data.playerIds[4],
                )
            )
        )
    }

    private fun getTeamDraft(id: Long): TeamDraft {
        return TeamDraft(teamInfo = GlobalVars.teams[id]!!)
    }

    private fun openDraft() {
        val draftModel = DraftModel(
            monthType = stateConfigurator.monthType,
            isDireBot = stateConfigurator.gameMode.hasTeams() && stateConfigurator.isBot,
            needToLoadPubMatches = stateConfigurator.gameMode.isBattleCup() || stateConfigurator.needToLoadPubMatches && stateConfigurator.gameMode.hasTeams(),
            isAllPick = stateConfigurator.isAllPick,
            gameMode = stateConfigurator.gameMode,
            sequence = defineSequence(
                stateConfigurator.isAllPick,
                stateConfigurator.gameMode,
                stateConfigurator.isRadiantFirstPick,
                GlobalVars.mainSettings
            ),
            radiantTeam = stateConfigurator.radiantTeam,
            direTeam = stateConfigurator.direTeam,
        )
        interactor.setDraftModel(draftModel)
        val screen = DraftScreen(GameModeSelectionFragmentDirections.toDraftFragment())
        emit(viewEventMutable, ViewEvent.Navigation(screen))
    }

    private fun defineSequence(
        isAllPick: Boolean,
        gameMode: GameModeType,
        isRadiantFirstPick: Boolean,
        mainSettings: MainSettings
    ): String {
        val sequence = if (isAllPick) mainSettings.sequenceAP else mainSettings.sequenceCM
        return if (isRadiantFirstPick || gameMode.isUser()) {
            sequence
        } else {
            reverseSequence(sequence)
        }
    }

    private fun reverseSequence(_sequence: String): String {
        var sequence = ""
        for (ch in _sequence) {
            sequence += when (ch) {
                '0' -> '1'
                '1' -> '0'
                '2' -> '3'
                else -> '2'
            }
        }
        return sequence
    }

    private inner class StateConfigurator {
        var needToLoadPubMatches: Boolean = false
        var gameMode: GameModeType = GameModeType.USER
        var monthType: MonthType = MonthType.MONTH3
        var isBot: Boolean = false
        var isAllPick: Boolean = true
        var isRadiantFirstPick = true
        var radiantTeam: TeamDraft = TeamDraft()
        var direTeam: TeamDraft = TeamDraft()
        val monthList = listOf(MonthType.MONTH1, MonthType.MONTH3, MonthType.MONTH6, MonthType.MONTH12)

        fun defineFragmentState(): GameModeSelectionViewState {
            val monthListComplex = complexList {
                for (monthItem in monthList) {
                    filterItem(key = monthItem.name) {
                        title = when (monthItem) {
                            MonthType.MONTH1 -> stringProvider.getString(R.string.month_type_1)
                            MonthType.MONTH3 -> stringProvider.getString(R.string.month_type_3)
                            MonthType.MONTH6 -> stringProvider.getString(R.string.month_type_6)
                            MonthType.MONTH12 -> stringProvider.getString(R.string.month_type_12)
                        }
                        isSelected = monthType == monthItem
                    }
                }
            }
            return GameModeSelectionViewState.DefaultState(
                needToLoadPubMatches,
                gameMode,
                isBot,
                isAllPick,
                isRadiantFirstPick,
                monthListComplex,
                monthType,
                radiantTeam.teamInfo.name,
                direTeam.teamInfo.name
            )
        }
    }
}