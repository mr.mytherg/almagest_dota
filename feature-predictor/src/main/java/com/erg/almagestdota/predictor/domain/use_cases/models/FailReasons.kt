package com.erg.almagestdota.predictor.domain.use_cases.models

enum class FailReasons {
    CONTROVERSIAL_INFO,
    CONTROVERSIAL_INFO_DRAFTS,
    PLAYERS_INVALID,
    PLAYERS_NOT_CALIBRATED,
    TOO_LOW_COEFFICIENTS;
}