package com.erg.almagestdota.predictor.domain.use_cases

import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.local_storage.external.models.draft.HeroValues
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import kotlinx.coroutines.*
import javax.inject.Inject

class HeroValuesCalibratorUseCase @Inject constructor() {
    companion object {
        private const val COEF_SYNERGY_BIG = 0.16
        private const val COEF_SYNERGY_SMALL = 0.08
        private const val COEF_COUNTER_BIG = 0.24
        private const val COEF_COUNTER_SMALL = 0.12
    }

    private var heroesValues: MutableMap<Int, HeroValues> = mutableMapOf()

    suspend fun calibrateProMatches(
        matches: List<DotaMatch>,
        viewModelScope: CoroutineScope,
        heroesValues: MutableMap<Int, HeroValues> = HeroValues.copy(GlobalVars.importantSettings.heroesValues)
    ): MutableMap<Int, HeroValues> {
        this.heroesValues = heroesValues
        calibrateV2(matches, viewModelScope)
        return this.heroesValues
    }

    private suspend fun calibrateV2(dotaMatches: List<DotaMatch>, viewModelScope: CoroutineScope) {
        if (dotaMatches.isEmpty()) {
            return
        }
        for (match in dotaMatches) {
            calibrateMatchV2(match)
        }
    }

    private fun calibrateMatchV2(match: DotaMatch) {
        if (match.hasRadiantWon) {
            changeCoefficients(match.radiantHeroes.map { it.heroId }, match.direHeroes.map { it.heroId }, match.radiantScore, match.direScore)
        } else {
            changeCoefficients(match.direHeroes.map { it.heroId }, match.radiantHeroes.map { it.heroId }, match.direScore, match.radiantScore)
        }
    }

    private fun changeCoefficients(winner: List<Int>, loser: List<Int>, winnerScore: Int, loserScore: Int) {
        val multiplier = 1.0 // if (winnerScore == 0 || loserScore == 0) 1.0 else loserScore * 1.0 / winnerScore
        for (winHero in winner)
            for (loserHero in loser) {
                val coef1 = if (heroesValues[winHero]!!.counters[loserHero]!! >= 0) COEF_COUNTER_SMALL else COEF_COUNTER_BIG
                heroesValues[winHero]!!.counters[loserHero] = (heroesValues[winHero]!!.counters[loserHero]!! + coef1 * multiplier).round2()
                val coef2 = if (heroesValues[loserHero]!!.counters[winHero]!! >= 0) COEF_COUNTER_SMALL else COEF_COUNTER_BIG
                heroesValues[loserHero]!!.counters[winHero] = (heroesValues[loserHero]!!.counters[winHero]!! - coef2 * multiplier).round2()
            }
        for (hero1 in 0..winner.lastIndex)
            for (hero2 in (hero1 + 1)..winner.lastIndex) {
                val heroId1 = winner[hero1]
                val heroId2 = winner[hero2]
                val coef = if (heroesValues[heroId1]!!.synergies[heroId2]!! >= 0) COEF_SYNERGY_SMALL else COEF_SYNERGY_BIG
                val newValue = heroesValues[heroId1]!!.synergies[heroId2]!! + coef * multiplier
                heroesValues[heroId1]!!.synergies[heroId2] = newValue.round2()
                heroesValues[heroId2]!!.synergies[heroId1] = newValue.round2()
            }
        for (hero1 in 0..loser.lastIndex)
            for (hero2 in (hero1 + 1)..loser.lastIndex) {
                val heroId1 = loser[hero1]
                val heroId2 = loser[hero2]
                val coef = if (heroesValues[heroId1]!!.synergies[heroId2]!! >= 0) COEF_SYNERGY_BIG else COEF_SYNERGY_SMALL
                val newValue = heroesValues[heroId1]!!.synergies[heroId2]!! - coef * multiplier
                heroesValues[heroId1]!!.synergies[heroId2] = newValue.round2()
                heroesValues[heroId2]!!.synergies[heroId1] = newValue.round2()
            }
    }
}