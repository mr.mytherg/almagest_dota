package com.erg.almagestdota.predictor.domain.use_cases

import com.erg.almagestdota.predictor.domain.use_cases.models.HawkMatch
import com.erg.almagestdota.predictor.domain.use_cases.models.HawkSeries
import com.erg.almagestdota.predictor.domain.use_cases.models.HawkSeriesList
import com.erg.almagestdota.predictor.domain.use_cases.models.HawkTeam
import com.google.gson.Gson
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import javax.inject.Inject

class LoadHawkDraftUseCase @Inject constructor(
    private val gson: Gson
) {
    suspend operator fun invoke(): List<HawkSeries> {
        var doc: Document
        try {
            doc = Jsoup.connect("https://hawk.live/").get()
        } catch (e: Exception) {
            return listOf()
        }
        val str = doc.html().toString().replace("&quot;", "\"")
        val text = str.substring(str.indexOf("\"status\":\"live\",\"series\":[") + 16)
        val json = "{${text.substring(0, text.indexOf("]}]}]") + 5)}}"
        return gson.fromJson(json, HawkSeriesList::class.java).series.map {
            var team1Wins = 0
            var team2Wins = 0
            for (match in it.matches) {
                if (match.hasRadiantWon == null) continue
                if ((match.isTeam1Radiant && match.hasRadiantWon) || (!match.isTeam1Radiant && !match.hasRadiantWon))
                    team1Wins++
                else
                    team2Wins++
            }

            it.team1Wins = team1Wins
            it.team2Wins = team2Wins
            it.matches = it.matches.filter { it.hasRadiantWon == null }
            it
        }
    }
}