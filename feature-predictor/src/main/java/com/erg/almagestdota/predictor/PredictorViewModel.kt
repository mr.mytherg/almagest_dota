package com.erg.almagestdota.predictor

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.extensions.launchOnIO
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.NotificationStatus
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.predictor.domain.use_cases.PredictionUseCase
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

internal class PredictorViewModel @AssistedInject constructor(
    private val stringProvider: StringProvider,
    private val predictionUseCase: PredictionUseCase,
    private val localStorageContract: ILocalStorageContract
) : ViewModel() {

    private val draftModel = localStorageContract.draftModel!!

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data -> }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow<PredictorViewState?>(null)
    val viewState = viewStateMutable.asStateFlow()

    private val loadingStateMutable = MutableStateFlow<Loading>(Loading.Enabled)
    val loadingState = loadingStateMutable.asStateFlow()

    init {
        getPrediction()
    }

    private fun getPrediction() {
        launchOnIO(
            loadingStateMutable,
            action = { predictionUseCase.invoke(draftModel, true) },
            onSuccess = { prediction ->
                var text = if (prediction.reason != null) {
                    if (BuildConfig.DEBUG) {
                        prediction.reason.name
                    } else {
                        stringProvider.getString(
                            R.string.predictor_cannot,
                        )
                    }
                } else {
                    val percentage = (1.0 / prediction.coefficient * 100).round2()
                    stateConfigurator.percentage = percentage
                    if (prediction.willRadiantWin!!) {
                        stringProvider.getString(
                            R.string.predictor_wins,
                            draftModel.radiantTeam.teamInfo.name,
                            prediction.coefficient.toString(),
                            percentage.toString()
                        )
                    } else {
                        stringProvider.getString(
                            R.string.predictor_wins,
                            draftModel.direTeam.teamInfo.name,
                            prediction.coefficient.toString(),
                            percentage.toString()
                        )
                    }
                }
                if (BuildConfig.DEBUG) {
                    text += "\nDraftValue ${prediction.draftValue}\n"
                    text += "DraftValuePub ${prediction.draftValuePub}\n"
                    text += "Team Diff ${prediction.teamDifference}\n"
                }

                stateConfigurator.draftPrediction = text
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            },
            onError = {
                emit(
                    viewEventMutable,
                    ViewEvent.AlmagestSnackbar(
                        message = it.message.orDefault(),
                        status = NotificationStatus.ERROR
                    )
                )
            }
        )
    }

    private inner class StateConfigurator {
        var draftPrediction = ""
        var percentage = 0.0

        fun defineFragmentState(): PredictorViewState {
            return PredictorViewState.DefaultState(
                draftPrediction = draftPrediction,
            )
        }
    }

    companion object {

        fun provideFactory(
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create() as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(): PredictorViewModel
    }
}