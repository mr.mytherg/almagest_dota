package com.erg.almagestdota.predictor.domain.use_cases.models

enum class PredictType {
    DraftPredict,
    TeamPredict;
}