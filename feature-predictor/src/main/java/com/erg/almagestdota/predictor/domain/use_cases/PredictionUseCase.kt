package com.erg.almagestdota.predictor.domain.use_cases

import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.local_storage.external.models.draft.HeroValues
import com.erg.almagestdota.local_storage.external.models.draft.main.DraftModel
import com.erg.almagestdota.local_storage.external.models.draft.main.MemberInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.network.external.firebase.FirebaseRepository
import com.erg.almagestdota.predictor.domain.use_cases.models.DotaPrediction
import com.erg.almagestdota.predictor.domain.use_cases.models.FailReasons
import com.erg.almagestdota.predictor.domain.use_cases.models.PredictType
import kotlinx.coroutines.*
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.math.absoluteValue

@Singleton
class PredictionUseCase @Inject constructor(
    private val firebaseRepository: FirebaseRepository
) {
    companion object {
        const val NORMAL_MEMBERS_MINIMUM = 6
        private const val MINIMUM_WIN_RATE = 64.0
        private const val MULTIPLIER_DRAFT = 10.0
        private const val MULTIPLIER_TEAM = 50
    }

    suspend fun invoke(draftModel: DraftModel, updatePlayers: Boolean): DotaPrediction {
        if (updatePlayers) {
            val settings = firebaseRepository.getImportantSettings()
            GlobalVars.importantSettings = settings
        }
        if (!isPlayersNormal(draftModel)) {
            return DotaPrediction(null, 0.0, null, FailReasons.PLAYERS_INVALID, 0.0,0.0,0)
        }
        if (!isPlayersCalibrated(draftModel)) {
            return DotaPrediction(null, 0.0, null, FailReasons.PLAYERS_NOT_CALIBRATED,0.0,0.0,0)
        }
        val draftValue = getDraftValue(draftModel, GlobalVars.importantSettings.heroesProValues)
        val draftValuePub = getDraftValue(draftModel, GlobalVars.importantSettings.heroesValues)
        val radiantRating = getTeamRating(draftModel.radiantTeam.teamInfo.members)
        val direRating = getTeamRating(draftModel.direTeam.teamInfo.members)
        val draftWinRate = getDraftWinRate(draftValue.absoluteValue)
        val teamWinRate = getTeamWinRate(radiantRating, direRating)
        if (!isPredictionNormal(radiantRating, direRating, draftValue)) {
            return DotaPrediction(
                null,
                0.0,
                if (draftWinRate > teamWinRate) PredictType.DraftPredict else PredictType.TeamPredict,
                FailReasons.CONTROVERSIAL_INFO,
                draftValue = draftValue,
                draftValuePub = draftValuePub,
                teamDifference = radiantRating - direRating
            )
        }
        if (draftWinRate < MINIMUM_WIN_RATE && teamWinRate < MINIMUM_WIN_RATE) {
            return DotaPrediction(null, 0.0, null, FailReasons.TOO_LOW_COEFFICIENTS,
                draftValue = draftValue,
                draftValuePub = draftValuePub,
                teamDifference = radiantRating - direRating
            )
        }
        val coefficient = getCoefficient(draftWinRate, teamWinRate)

        val dotaPrediction = DotaPrediction(
            willRadiantWin = willRadiantWin(radiantRating, direRating, draftValue),
            coefficient = coefficient,
            predictType = if (draftWinRate > teamWinRate) PredictType.DraftPredict else PredictType.TeamPredict,
            draftValue = draftValue,
            draftValuePub = draftValuePub,
            teamDifference = radiantRating - direRating
        )
        return dotaPrediction
    }

    private fun getTeamWinRate(radiantRating: Int, direRating: Int): Double {
        val eloDiff = radiantRating - direRating
        val winProbability = when {
            eloDiff.absoluteValue < MULTIPLIER_TEAM -> 50.0
            eloDiff.absoluteValue in MULTIPLIER_TEAM * 1 until MULTIPLIER_TEAM * 2 -> 61.75//GlobalVars.mainPredictor.teamProbabilities[TeamPredictionType.P50_100.name]!!
            eloDiff.absoluteValue in MULTIPLIER_TEAM * 2 until MULTIPLIER_TEAM * 3 -> 63.34//GlobalVars.mainPredictor.teamProbabilities[TeamPredictionType.P100_150.name]!!
            eloDiff.absoluteValue in MULTIPLIER_TEAM * 3 until MULTIPLIER_TEAM * 4 -> 68.66//GlobalVars.mainPredictor.teamProbabilities[TeamPredictionType.P150_200.name]!!
            eloDiff.absoluteValue in MULTIPLIER_TEAM * 4 until MULTIPLIER_TEAM * 5 -> 69.29//GlobalVars.mainPredictor.teamProbabilities[TeamPredictionType.P200_250.name]!!
            eloDiff.absoluteValue in MULTIPLIER_TEAM * 5 until MULTIPLIER_TEAM * 6 -> 74.23//GlobalVars.mainPredictor.teamProbabilities[TeamPredictionType.P250_300.name]!!
            else -> 72.91//GlobalVars.mainPredictor.teamProbabilities[TeamPredictionType.P300.name]!!
        }
        return winProbability
    }

    private fun getDraftWinRate(absoluteValue: Double): Double {
        return when  {
            absoluteValue < MULTIPLIER_DRAFT -> 50.0
            absoluteValue in MULTIPLIER_DRAFT * 1..MULTIPLIER_DRAFT * 2 -> 57.08//GlobalVars.mainPredictor.draftProbabilities[PredictionType.P5_10.name]!!
            absoluteValue in MULTIPLIER_DRAFT * 2..MULTIPLIER_DRAFT * 3 -> 61.43//GlobalVars.mainPredictor.draftProbabilities[PredictionType.P10_15.name]!!
            absoluteValue in MULTIPLIER_DRAFT * 3..MULTIPLIER_DRAFT * 4 -> 64.87//GlobalVars.mainPredictor.draftProbabilities[PredictionType.P15_20.name]!!
            absoluteValue in MULTIPLIER_DRAFT * 4..MULTIPLIER_DRAFT * 5 -> 68.72//GlobalVars.mainPredictor.draftProbabilities[PredictionType.P20_25.name]!!
            absoluteValue in MULTIPLIER_DRAFT * 5..MULTIPLIER_DRAFT * 6 -> 73.49//GlobalVars.mainPredictor.draftProbabilities[PredictionType.P25_30.name]!!
            else -> 74.84//GlobalVars.mainPredictor.draftProbabilities[PredictionType.P30.name]!!
        }
    }

    private fun getCoefficient(draftWinRate: Double, teamWinRate: Double): Double {
        val winRate = if (draftWinRate > teamWinRate) {
            draftWinRate
        } else {
            teamWinRate
        }
        return (1.0 / (winRate / 100)).round2()
    }

    private fun isPredictionNormal(
        radiantRating: Int,
        direRating: Int,
        draftValue: Double
    ): Boolean {
        return willRadiantWin(radiantRating, direRating, draftValue) ||
            willDireWin(radiantRating, direRating, draftValue)
    }

    private fun willRadiantWin(radiantRating: Int, direRating: Int, draftValue: Double): Boolean {
        return radiantRating - direRating >= -MULTIPLIER_TEAM && draftValue >= -MULTIPLIER_DRAFT || draftValue >= MULTIPLIER_DRAFT * 5
    }

    private fun willDireWin(radiantRating: Int, direRating: Int, draftValue: Double): Boolean {
        return radiantRating - direRating <= MULTIPLIER_TEAM && draftValue <= MULTIPLIER_DRAFT || draftValue <= -MULTIPLIER_DRAFT * 5
    }

    private fun getTeamRating(members: List<Long>): Int {
        return members.fold(0) { acc, next -> acc + (GlobalVars.players[next]?.rating ?: 1400) } / 5
    }

    private fun getDraftValue(draftModel: DraftModel, values: MutableMap<Int, HeroValues>): Double {
        return if (GlobalVars.mainPredictor.disabled) {
            0.0
        } else {
            val radiantValue = getTeamValue(
                draftModel.radiantTeam.picks.map { it.heroId },
                draftModel.direTeam.picks.map { it.heroId },
                values,
            )

            val direValue = getTeamValue(
                draftModel.direTeam.picks.map { it.heroId },
                draftModel.radiantTeam.picks.map { it.heroId },
                values,
            )
            return (radiantValue - direValue).round2()
        }
    }

    private fun isPlayersNormal(draftModel: DraftModel): Boolean {
        val radiant = draftModel.radiantTeam.teamInfo.members
        val dire = draftModel.direTeam.teamInfo.members
        return radiant.plus(dire).count { GlobalVars.players[it]?.name.orDefault().isNotEmpty() } >= NORMAL_MEMBERS_MINIMUM
    }

    private fun isPlayersCalibrated(draftModel: DraftModel): Boolean {
        val radiant = draftModel.radiantTeam.teamInfo.members
        val dire = draftModel.direTeam.teamInfo.members
        return radiant.plus(dire)
            .count {  GlobalVars.players[it]?.matchCount.orDefault() >= GlobalVars.CALIBRATION_MATCH_COUNT } >= NORMAL_MEMBERS_MINIMUM
    }

    private fun getTeamValue(
        allies: List<Int>,
        enemies: List<Int>,
        heroes: Map<Int, HeroValues>,
    ): Double {
        return allies.fold(0.0) { acc, next ->
            acc + next.getCounter(enemies, heroes) + next.getSynergy(
                allies,
                heroes
            )
        }
    }
}