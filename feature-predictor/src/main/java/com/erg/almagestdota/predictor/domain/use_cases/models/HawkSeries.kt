package com.erg.almagestdota.predictor.domain.use_cases.models

import com.google.gson.annotations.SerializedName

data class HawkSeriesList(
    val series: List<HawkSeries>
)

data class HawkSeries(
    val id: Int,
    val team1: HawkTeam,
    var team1Wins: Int? = 0,
    var team2Wins: Int? = 0,
    val team2: HawkTeam,
    @SerializedName("championship_name")
    val leagueName: String,
    @SerializedName("best_of")
    val seriesType: Int,
    var matches: List<HawkMatch>
)

data class HawkTeam(
    val name: String,
    @SerializedName("logo_url")
    val logoUrl: String?,
)

data class HawkMatch(
    val id: Int,
    val number: Int,
    @SerializedName("is_team1_radiant")
    val isTeam1Radiant: Boolean,
    @SerializedName("is_radiant_won")
    val hasRadiantWon: Boolean?,
    val heroes: List<HawkHero>
)

data class HawkHero(
    val name: String,
    @SerializedName("code_name")
    val codeName: String,
    @SerializedName("is_radiant")
    val isRadiant: Boolean
)