package com.erg.almagestdota.predictor.domain.use_cases.models

data class DotaPrediction(
    val willRadiantWin: Boolean?,
    val coefficient: Double,
    val predictType: PredictType? = null,
    val reason: FailReasons? = null,
    val draftValue: Double,
    val draftValuePub: Double,
    val teamDifference: Int,
)