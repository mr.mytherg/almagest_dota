package com.erg.almagestdota.predictor.domain.use_cases

import android.util.Log
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import kotlinx.coroutines.*
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import javax.inject.Inject

class LoadPlayerHeroPoolPublicUseCase @Inject constructor() {
    suspend operator fun invoke(
        accounts: Set<Long>,
        monthCount: Int = 3
    ): MutableMap<Int, Pair<Int, Int>> {
        val heroes: MutableMap<Int, Pair<Int, Int>> = mutableMapOf()
        for (account in accounts) {
            handleDotabuff(account, heroes, monthCount)
        }
        return heroes
    }

    private suspend fun handleDotabuff(
        playerId: Long,
        heroes: MutableMap<Int, Pair<Int, Int>>,
        monthCount: Int
    ) {
        val accHeroes = loadHeroPoolDotabuff(
            playerId,
            monthCount
        )
        for ((heroId, pair) in accHeroes) {
            heroes[heroId] = Pair(
                first = (heroes[heroId]?.first ?: 0) + pair.first,
                second = (heroes[heroId]?.second ?: 0) + pair.second
            )
        }
    }

    private suspend fun loadHeroPoolDotabuff(playerId: Long, monthCount: Int): MutableMap<Int, Pair<Int, Int>> {
        val date = when (monthCount) {
            1 -> "month"
            3 -> "3month"
            6 -> "6month"
            12 -> "year"
            else -> IllegalStateException("Неверный период")
        }
        val url = "https://www.dotabuff.com/players/$playerId/heroes?date=$date&lobby_type=ranked_matchmaking&metric=played"
        val heroes = mutableMapOf<Int, Pair<Int, Int>>()
        var doc: Document
        var delayCount = 0
        while (true) {
            try {
                doc = Jsoup.connect(url)
                    .header(
                        "Cookie",
                        "_tz=Asia%2FSaigon; _ga=GA1.2.1122684581.1669626336; __qca=P0-773539065-1669626336277; _hjSessionUser_2490228=eyJpZCI6Ijc0MGQ3Y2U2LWQwZjItNTgwZi1iNzkwLTMxZjA4OTJjNjhjNSIsImNyZWF0ZWQiOjE2Njk2MjYzMzYzMjgsImV4aXN0aW5nIjp0cnVlfQ==; _gid=GA1.2.2120575359.1671292247; _hjSession_2490228=eyJpZCI6IjRjMDczYWUwLWE2YjMtNDYyZC1iNjU2LTU3ZWY2ZWMxZWZmOCIsImNyZWF0ZWQiOjE2NzEzMzg0MTg3MjQsImluU2FtcGxlIjpmYWxzZX0=; _hjAbsoluteSessionInProgress=0; _hjIncludedInSessionSample=0; _hi=1671343654459"
                    )
                    .header(
                        "User-Agent",
                        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.174 YaBrowser/22.1.3.848 Yowser/2.5 Safari/537.36"
                    )
                    .header("Connection", "keep-alive")
                    .header("sec-ch-ua", "\"Not?A_Brand\";v=\"8\", \"Chromium\";v=\"108\", \"Microsoft Edge\";v=\"108\"")
                    .get()
                break
            } catch (e: Exception) {
                if (delayCount >= 10) {
                    Log.e("HEROPOOL", "FAIL - more than 10 delays")
                    return mutableMapOf()
                } else if (e.stackTraceToString().contains("Status=429")) {
                    Log.e("HEROPOOL", "delay")
                    delay(300)
                    delayCount++
                } else {
                    Log.e("HEROPOOL", "FAIL - ${e.stackTraceToString()}")
                    return mutableMapOf()
                }
            }
        }
        Log.e("HEROPOOL", "name SUCCESS - ${playerId} ")
        val elements = doc.getElementsByAttributeValue("class", "sortable").select("tbody").select("tr")
        for (element in elements) {
            try {
                val heroRow = element.select("tr")
                val heroName = heroRow.select("td")[1].select("a")[0].text().toString()
                val matchCount = heroRow.select("td")[2].text().toInt()
                val winRateStart = heroRow.select("td")[3].text()
                val winRate = winRateStart.substring(0, winRateStart.indexOf("%")).toDouble()

                val wonCount: Int = (matchCount * winRate / 100).toInt()
                val lostCount: Int = matchCount - wonCount
                heroes[GlobalVars.mainSettings.heroes.find { it.name == heroName }!!.id] =
                    Pair(wonCount, lostCount)
            } catch (e: Exception) {
                Log.e("HEROPOOL", "fetch error ${e.stackTraceToString()}")
            }
        }
        Log.e("HEROPOOL", "isSuccess player pool ${heroes.isNotEmpty()}")
        return heroes
    }
}