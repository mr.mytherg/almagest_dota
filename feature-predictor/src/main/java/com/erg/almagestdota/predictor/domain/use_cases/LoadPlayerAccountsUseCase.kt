package com.erg.almagestdota.predictor.domain.use_cases

import android.util.Log
import com.erg.almagestdota.base.external.extensions.addIn
import com.erg.almagestdota.local_storage.external.models.draft.main.MemberInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import kotlinx.coroutines.*
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import javax.inject.Inject
import kotlin.coroutines.coroutineContext

class LoadPlayerAccountsUseCase @Inject constructor() {
    suspend operator fun invoke(playerId: Long): Set<Long> {
        val player = GlobalVars.players[playerId] ?: MemberInfo(playerId)
        return if (player.name.isNotEmpty())
            getAllAccounts(player.name).plus(playerId)
        else
            setOf(playerId)
    }

    private suspend fun getAllAccounts(playerName: String): Set<Long> {
        val listDeferred = mutableListOf<Deferred<Long>>()
        val playerNames = listOf(
            playerName,
            "$playerName (smurf)",
            "$playerName (smurf 2)",
            "$playerName (smurf 3)",
            "$playerName (smurf 4)",
            "$playerName (smurf 5)",
        )
        for (name in playerNames) {
            if (name.isEmpty()) continue
            val deferred = CoroutineScope(coroutineContext).async(Dispatchers.IO) {
                loadSmurfId(name)
            }
            listDeferred.add(deferred)
        }
        return listDeferred.awaitAll().filter { it != 0L }.toSet()
    }

    private suspend fun loadSmurfId(name: String): Long {
        val url = "https://www.dota2protracker.com/player/$name"

        val doc: Document
        try {
            doc = Jsoup.connect(url)
                .header(
                    "Cookie",
                    "_ga=GA1.1.1260870144.1670606247; __cf_bm=kxYbo.85y7Ix0.96fxEt4BA7QBl0agOuVkg6oVTDQGw-1671343273-0-ARjfOHHDL0U9jYW5WMtc+TUPFl52gal/0jDi6oMPzLeqy4jazGqivr+iJHH4tY/h0jA0OuQ645n0/Y92fDpGAYndLFUByUnM0IWIf4YuurTddsphdSiKYsG6Wd2syLjAnJ3waZPQNXW4GtFlrk9fh7Y=; _ga_BXTGY4VVYT=GS1.1.1671343053.6.1.1671343275.0.0.0"
                )
                .header(
                    "User-Agent",
                    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.174 YaBrowser/22.1.3.848 Yowser/2.5 Safari/537.36"
                )
                .header("Connection", "keep-alive")
                .header("sec-ch-ua", "\"Not?A_Brand\";v=\"8\", \"Chromium\";v=\"108\", \"Microsoft Edge\";v=\"108\"")
                .get()
        } catch (e: Exception) {
            Log.e("HEROPOOL", "FAIL - ${name}")
            return 0L
        }
        Log.e("HEROPOOL", "name SUCCESS - ${name} ")
        var playerId: Long = 0L
        try {
            val element = doc.getElementsByAttributeValue("class", "hero-header-stratz-inner").select("a")[0].toString()
            val key = "https://stratz.com/player/"
            val idStart = element.substring(element.indexOf(key) + key.length)
            playerId = idStart.substring(0, idStart.indexOf("\"")).toLong()
        } catch (e: Exception) {
            Log.e("HEROPOOL", "matchId fetch error ${e.stackTraceToString()}")
        }
        return playerId
    }
}