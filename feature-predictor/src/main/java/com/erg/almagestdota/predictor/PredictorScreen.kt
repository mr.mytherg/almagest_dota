package com.erg.almagestdota.predictor

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen

class PredictorScreen(
    route: NavDirections
) : Screen<NavDirections>(
    route = route,
    requestKey = TAG
) {
    companion object {
        const val TAG = "PredictorSheet"
    }
}