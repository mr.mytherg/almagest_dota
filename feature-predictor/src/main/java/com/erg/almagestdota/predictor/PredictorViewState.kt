package com.erg.almagestdota.predictor

import androidx.constraintlayout.motion.utils.ViewState

internal sealed class PredictorViewState : ViewState() {

    class DefaultState(
        val draftPrediction: String,
    ) : PredictorViewState()
}