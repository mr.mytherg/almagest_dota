package com.erg.almagestdota.predictor.domain.use_cases

import com.erg.almagestdota.local_storage.external.models.draft.RoleType
import com.erg.almagestdota.local_storage.external.models.draft.isHeroFitsForRole
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroRoleData
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import javax.inject.Inject

class FilterHeroesByRolesUseCase @Inject constructor() {

    fun getMatchUpList(teamHeroes: List<HeroCoreData>): Set<List<HeroRoleData>> {
        val matchUpList = getMatchUps(sortHeroesByRoleCount(teamHeroes.map { it.id }))
        return matchUpList.map {
            it.map { role ->
                if (role.hero == null) {
                    HeroRoleData.createNullHero()
                } else {
                    val hero = role.hero!!.getHeroById()
                    HeroRoleData(
                        id = hero.id,
                        name = hero.name,
                        imageLink = hero.imageLink,
                        heroRoles = setOf(role.roleType)
                    )
                }
            }
        }.toSet()
    }

    fun getHeroesRoles(teamHeroes: List<Int>): List<HeroRoleData> {
        val matchUpList = getMatchUps(sortHeroesByRoleCount(teamHeroes))
        val heroesRoles = isDraftRolesValid(matchUpList)
        val heroRoleDataList = mutableListOf<HeroRoleData>()
        for (heroId in teamHeroes) {
            val hero = heroId.getHeroById()
            heroRoleDataList.add(
                HeroRoleData(
                    id = hero.id,
                    name = hero.name,
                    imageLink = hero.imageLink,
                    heroRoles = heroesRoles?.find { it.hero == hero.id }?.possibleRoles
                )
            )
        }
        return heroRoleDataList
    }

    operator fun <T : HeroCoreData> invoke(
        heroes: List<T>,
        teamHeroes: List<Int>
    ): List<T> {
        val matchUpList = getMatchUps(sortHeroesByRoleCount(teamHeroes))
        if (matchUpList.isEmpty()) return heroes
        val filtered = heroes.filter {
            val heroPossibleRoles = sortHeroesByRoleCount(teamHeroes.plus(it.id))
            getMatchUps(heroPossibleRoles).isNotEmpty()
        }
        return filtered
    }

    private fun isDraftRolesValid(matchUpList: Set<List<Role>>): List<HeroPossibleRoles>? {
        return if (matchUpList.isNotEmpty()) {
            val heroPossibleRolesChanged = mutableListOf<HeroPossibleRoles>()
            for (matchUp in matchUpList) {
                for (role in matchUp) {
                    if (role.hero == null) continue
                    var hero = heroPossibleRolesChanged.find { it.hero == role.hero!! }
                    if (hero == null) {
                        hero = HeroPossibleRoles(
                            hero = role.hero!!,
                            possibleRoles = setOf(role.roleType)
                        )
                        heroPossibleRolesChanged.add(hero)
                    } else {
                        hero.possibleRoles = hero.possibleRoles.plus(role.roleType)
                    }
                }
            }
            heroPossibleRolesChanged
        } else {
            null
        }
    }

    private fun sortHeroesByRoleCount(teamHeroes: List<Int>): List<HeroPossibleRoles> {
        val mutableList = mutableListOf<HeroPossibleRoles>()
        teamHeroes.forEachIndexed { index, hero ->
            val roles = GlobalVars.heroesAsMap[hero]!!.roles
            val roleTypes = mutableSetOf<RoleType>()
            for (role in roles) {
                val isFits = roles.isHeroFitsForRole(role.roleType)
                if (!isFits) continue
                roleTypes.add(role.roleType)
            }
            mutableList.add(HeroPossibleRoles(hero, roleTypes))
        }
        return mutableList.sortedBy { it.possibleRoles.size }
    }

    private fun getMatchUps(leastHeroes: List<HeroPossibleRoles>): Set<List<Role>> {
        val matchUp = listOf(
            Role(RoleType.ROLE_1, null),
            Role(RoleType.ROLE_2, null),
            Role(RoleType.ROLE_3, null),
            Role(RoleType.ROLE_4, null),
            Role(RoleType.ROLE_5, null),
        )
        val matchUpList = mutableSetOf<List<Role>>()
        addAllMatchUpsRecursively(leastHeroes, matchUp, matchUpList)
        return matchUpList
    }

    private fun addAllMatchUpsRecursively(
        leastHeroes: List<HeroPossibleRoles>,
        _matchUp: List<Role>,
        matchUpList: MutableSet<List<Role>>
    ) {
        for (hero in leastHeroes) {
            for (roleType in hero.possibleRoles) {
                val matchUp: List<Role> = listOf(
                    Role(roleType = _matchUp[0].roleType, hero = _matchUp[0].hero),
                    Role(roleType = _matchUp[1].roleType, hero = _matchUp[1].hero),
                    Role(roleType = _matchUp[2].roleType, hero = _matchUp[2].hero),
                    Role(roleType = _matchUp[3].roleType, hero = _matchUp[3].hero),
                    Role(roleType = _matchUp[4].roleType, hero = _matchUp[4].hero)
                )
                val isSuccess = tryToFillTeamRoles(matchUp, Role(roleType, hero.hero))
                if (isSuccess) {
                    addAllMatchUpsRecursively(leastHeroes.minus(hero), matchUp, matchUpList)
                }
            }
        }
        if (leastHeroes.isEmpty()) {
            matchUpList.add(_matchUp)
        }
    }

    private fun tryToFillTeamRoles(
        teamRoles: List<Role>,
        role: Role
    ): Boolean {
        when (role.roleType) {
            RoleType.ROLE_1 -> {
                if (teamRoles[0].hero != null) {
                    return false
                } else {
                    teamRoles[0].hero = role.hero
                }
            }
            RoleType.ROLE_2 -> {
                if (teamRoles[1].hero != null) {
                    return false
                } else {
                    teamRoles[1].hero = role.hero
                }
            }
            RoleType.ROLE_3 -> {
                if (teamRoles[2].hero != null) {
                    return false
                } else {
                    teamRoles[2].hero = role.hero
                }
            }
            RoleType.ROLE_4 -> {
                if (teamRoles[3].hero != null) {
                    return false
                } else {
                    teamRoles[3].hero = role.hero
                }
            }
            RoleType.ROLE_5 -> {
                if (teamRoles[4].hero != null) {
                    return false
                } else {
                    teamRoles[4].hero = role.hero
                }
            }
        }
        return true
    }
}

data class Role(
    val roleType: RoleType,
    var hero: Int? = null,
)

data class HeroPossibleRoles(
    val hero: Int,
    var possibleRoles: Set<RoleType>
)