package com.erg.almagestdota.predictor

import junit.framework.Assert.assertEquals
import kotlinx.coroutines.*
import org.junit.Test
import java.text.DecimalFormat

data class HeroValues(
    val synergies: MutableMap<Int, Double>,
    val counters: MutableMap<Int, Double>,
)

data class DotaMatch(
    val radiantHeroes: List<Int>,
    val direHeroes: List<Int>,
    val hasRadiantWon: Boolean
)

fun Double.round2(): Double {
    var formattedDouble = DecimalFormat("#0.00").format(this)
    formattedDouble = formattedDouble.replace(",", ".")
    return formattedDouble.toDouble()
}

class ExampleUnitTest {
    val map = mutableMapOf<Int, HeroValues>()
    val SMALL = 0.2
    val BIG = 0.2

    @Test
    fun addition_isCorrect() {
        fillInitial(map)
        processMatches(map)
        checkResult(map)
    }

    private fun checkResult(map: MutableMap<Int, HeroValues>) {
        for ((hero, heroValues) in map) {
            for ((hero1, value) in heroValues.counters)
                assertEquals(1.0, value)
            for ((hero1, value) in heroValues.synergies)
                assertEquals(1.0, value)
        }
    }

    private fun processMatches(map: MutableMap<Int, HeroValues>) {
        val matches = mutableListOf<DotaMatch>()
        fillMatches(matches)
        runBlocking {
            for (match in matches) {
                if (match.hasRadiantWon) {
                    changeCoefficients(match.radiantHeroes, match.direHeroes)
                } else {
                    changeCoefficients(match.direHeroes, match.radiantHeroes)
                }
            }
        }
    }

    private fun changeCoefficients(winner: List<Int>, loser: List<Int>) {
        for (winHero in winner)
            for (loserHero in loser) {
                val coef1 = if (map[winHero]!!.counters[loserHero]!! >= 0) SMALL else BIG
                map[winHero]!!.counters[loserHero] = (map[winHero]!!.counters[loserHero]!! + coef1).round2()
                val coef2 = if (map[loserHero]!!.counters[winHero]!! >= 0) SMALL else BIG
                map[loserHero]!!.counters[winHero] = (map[loserHero]!!.counters[winHero]!! - coef2).round2()
            }
        for (hero1 in 0..winner.lastIndex)
            for (hero2 in (hero1 + 1)..winner.lastIndex) {
                val heroId1 = winner[hero1]
                val heroId2 = winner[hero2]
                val coef = if (map[heroId1]!!.synergies[heroId2]!! > 0) SMALL else BIG
                val newValue = map[heroId1]!!.synergies[heroId2]!! + coef
                map[heroId1]!!.synergies[heroId2] = newValue.round2()
                map[heroId2]!!.synergies[heroId1] = newValue.round2()
            }
        for (hero1 in 0..loser.lastIndex)
            for (hero2 in (hero1 + 1)..loser.lastIndex) {
                val heroId1 = loser[hero1]
                val heroId2 = loser[hero2]
                val coef = if (map[heroId1]!!.synergies[heroId2]!! > 0) BIG else SMALL
                val newValue = map[heroId1]!!.synergies[heroId2]!! - coef
                map[heroId1]!!.synergies[heroId2] = newValue.round2()
                map[heroId2]!!.synergies[heroId1] = newValue.round2()
            }
    }

    private fun fillMatches(matches: MutableList<DotaMatch>) {
        for (index in 1..20000) {
            matches.add(
                DotaMatch(
                    radiantHeroes = listOf(1, 2, 3, 4, 5),
                    direHeroes = listOf(6, 7, 8, 9, 10),
                    hasRadiantWon = index % 2 == 0,
                )
            )
        }
    }

    private fun fillInitial(map: MutableMap<Int, HeroValues>) {
        for (index in 1..10) {
            val synergies = mutableMapOf<Int, Double>()
            for (index1 in 1..10)
                synergies[index1] = 1.0
            val counters = mutableMapOf<Int, Double>()
            for (index1 in 1..10)
                counters[index1] = 1.0
            map[index] = HeroValues(synergies, counters)
        }
    }
}