package com.erg.almagestdota.drafting.presentation.draft_analysis

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem

internal sealed class DraftAnalysisViewState : ViewState() {

    class DefaultState(
        val radiantBans: List<ComplexItem>,
        val radiantPicks: List<ComplexItem>,
        val direBans: List<ComplexItem>,
        val direPicks: List<ComplexItem>,
        val heroesToSelect: List<ComplexItem>,
        val sequence: List<ComplexItem>,
        val isPrevAvailable: Boolean,
        val isNextAvailable: Boolean,
        val stage: Int,
        val radiantName: String,
        val direName: String,
        val draftAnalysisReasons: List<ComplexItem>
    ) : DraftAnalysisViewState()
}