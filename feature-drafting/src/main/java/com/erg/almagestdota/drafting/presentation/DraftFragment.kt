package com.erg.almagestdota.drafting.presentation

import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.ActivityManager
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.complexadapter.ComplexAdapter
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.drafting.BuildConfig
import com.erg.almagestdota.drafting.R
import com.erg.almagestdota.drafting.databinding.DraftFragmentBinding
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdota.local_storage.external.models.draft.hasTeams
import com.erg.almagestdota.local_storage.external.models.draft.isLive
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
internal class DraftFragment : Fragment(R.layout.draft_fragment), BackPressListener {

    companion object {
        const val TAG = "DraftFragment"
        const val MILLISECONDS_PER_INCH = 500f
    }

    private val binding by viewBinding(DraftFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    @Inject
    lateinit var factory: DraftViewModel.Factory
    private val viewModel by viewModels<DraftViewModel> { factory }

    override fun onBackPressed() {
        viewModel.obtainAction(DraftViewActions.OnBackClick)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
        viewModel.viewEvent.setResultListener(this)
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).filterNotNull().onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.loadingState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { state ->
            binding.loadingView.isVisible = state is Loading.Enabled
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private inner class ViewsConfigurator {
        private val smoothScrollerFilters: RecyclerView.SmoothScroller by lazy { createSmoothScrollListener() }
        private val smoothScrollerGroups: RecyclerView.SmoothScroller by lazy { createSmoothScrollListener() }
        private val smoothScrollerSequence: RecyclerView.SmoothScroller by lazy { createSmoothScrollListener() }
        private val smoothScrollerTypes: RecyclerView.SmoothScroller by lazy { createSmoothScrollListener() }

        private val filtersAdapter by lazy {
            ComplexAdapter(
                complexListener = object : ComplexListener {
                    override fun onClick(item: ComplexItem) {
                        viewModel.obtainAction(DraftViewActions.SelectFilter(item.key))
                    }
                }
            )
        }
        private val typesAdapter by lazy {
            ComplexAdapter(
                complexListener = object : ComplexListener {
                    override fun onClick(item: ComplexItem) {
                        viewModel.obtainAction(DraftViewActions.SelectType(item.key))
                    }
                }
            )
        }
        private val groupsAdapter by lazy {
            ComplexAdapter(
                complexListener = object : ComplexListener {
                    override fun onClick(item: ComplexItem) {
                        viewModel.obtainAction(DraftViewActions.SelectGroup(item.key))
                    }
                }
            )
        }
        private val heroesToSelectAdapter by lazy {
            ComplexAdapter(
                complexListener = object : ComplexListener {
                    override fun onClick(item: ComplexItem) {
                        viewModel.obtainAction(DraftViewActions.SelectHero(item.key))
                    }
                }
            )
        }
        private val draftStrategyAdapter by lazy {
            DraftStrategyAdapter(
                complexListener = object : ComplexListener {
                    override fun onClick(item: ComplexItem) {
                        viewModel.obtainAction(DraftViewActions.SelectHero(item.key))
                    }
                }
            )
        }

        private val radiantHeroesAdapter by lazy { ComplexAdapter() }
        private val radiantBansAdapter by lazy { ComplexAdapter() }
        private val direHeroesAdapter by lazy { ComplexAdapter() }
        private val direBansAdapter by lazy { ComplexAdapter() }
        private val sequenceAdapter by lazy { ComplexAdapter() }

        private val heroesResultAdapter by lazy {
            ComplexAdapter(
                complexListener = object : ComplexListener {
                    override fun onClick(item: ComplexItem) {
                        viewModel.obtainAction(DraftViewActions.ShowHeroItems(item.key))
                    }
                }
            )
        }

        private val draftItemTypeAdapter by lazy {
            ComplexAdapter(
                complexListener = object : ComplexListener {
                    override fun onClick(item: ComplexItem) {
                        if (item is ComplexItem.ItemDefault && !item.hasBlackout) {
                            viewModel.obtainAction(DraftViewActions.OpenResultItemInfoSheet(item.key))
                        }
                    }
                }
            )
        }

        private fun createSmoothScrollListener(): RecyclerView.SmoothScroller {
            return object : LinearSmoothScroller(requireContext()) {
                override fun getHorizontalSnapPreference(): Int = SNAP_TO_START
                override fun calculateSpeedPerPixel(displayMetrics: DisplayMetrics): Float =
                    MILLISECONDS_PER_INCH / displayMetrics.densityDpi
            }
        }

        fun initStartState() = with(binding) {
            ivBack.clicksWithDebounce {
                viewModel.obtainAction(DraftViewActions.OnBackClick)
            }
            ivCancelLastChoice.clicksWithDebounce {
                viewModel.obtainAction(DraftViewActions.CancelLastChoice)
            }
            ivSearch.clicksWithDebounce {
                viewModel.obtainAction(DraftViewActions.OpenHeroSelectorSheet)
            }
            ivRevertSequence.clicksWithDebounce {
                viewModel.obtainAction(DraftViewActions.RevertSequence)
            }
            ivInfo.clicksWithDebounce {
                viewModel.obtainAction(DraftViewActions.OpenInfo)
            }
            ivRadiantHistory.clicksWithDebounce {
                viewModel.obtainAction(DraftViewActions.OpenMatchesSheet(true))
            }
            ivRadiantHistoryResult.clicksWithDebounce {
                viewModel.obtainAction(DraftViewActions.OpenMatchesSheet(true))
            }
            ivDireHistory.clicksWithDebounce {
                viewModel.obtainAction(DraftViewActions.OpenMatchesSheet(false))
            }
            ivDireHistoryResult.clicksWithDebounce {
                viewModel.obtainAction(DraftViewActions.OpenMatchesSheet(false))
            }
            tvPrediction.clicksWithDebounce {
                viewModel.obtainAction(DraftViewActions.OpenPredictor)
            }
            tvSkipBans.clicksWithDebounce {
                viewModel.obtainAction(DraftViewActions.SkipBans)
            }
            ivAnalysis.clicksWithDebounce {
                viewModel.obtainAction(DraftViewActions.ShowAnalysis)
            }
            ivData.clicksWithDebounce {
                viewModel.obtainAction(DraftViewActions.OpenRolesSelection)
            }

            if (rvHeroesResult.adapter == null) {
                rvHeroesResult.adapter = heroesResultAdapter
                rvHeroesResult.layoutManager =
                    GridLayoutManager(requireContext(), GlobalVars.mainSettings.getPickSizeCM())
                rvHeroesResult.setHasFixedSize(true)
            }
            if (rvItemTypes.adapter == null) {
                rvItemTypes.adapter = draftItemTypeAdapter
            }
            if (rvDireHeroes.adapter == null) {
                rvDireHeroes.adapter = direHeroesAdapter
                rvDireHeroes.layoutManager =
                    GridLayoutManager(requireContext(), GlobalVars.mainSettings.getPickSizeCM())
                rvDireHeroes.setHasFixedSize(true)
            }
            if (rvFilters.adapter == null) {
                rvFilters.adapter = filtersAdapter
            }
            if (rvTypes.adapter == null) {
                rvTypes.adapter = typesAdapter
                rvTypes.setHasFixedSize(true)
            }
            if (rvRadiantBans.adapter == null) {
                rvRadiantBans.adapter = radiantBansAdapter
                rvRadiantBans.layoutManager = GridLayoutManager(requireContext(), GlobalVars.mainSettings.getBanSizeCM())
                rvRadiantBans.setHasFixedSize(true)
            }
            if (rvRadiantHeroes.adapter == null) {
                rvRadiantHeroes.adapter = radiantHeroesAdapter
                rvRadiantHeroes.layoutManager = GridLayoutManager(requireContext(), GlobalVars.mainSettings.getPickSizeCM())
                rvRadiantHeroes.setHasFixedSize(true)
            }
            if (rvFilterGroups.adapter == null) {
                rvFilterGroups.adapter = groupsAdapter
            }
            if (rvDireBans.adapter == null) {
                rvDireBans.adapter = direBansAdapter
                rvDireBans.layoutManager = GridLayoutManager(requireContext(), GlobalVars.mainSettings.getBanSizeCM())
                rvDireBans.setHasFixedSize(true)
            }

            if (rvSequence.adapter == null) {
                rvSequence.adapter = sequenceAdapter
                rvSequence.setHasFixedSize(true)
            }
        }

        fun renderState(state: DraftViewState) {
            when (state) {
                is DraftViewState.DefaultState -> renderDefaultState(state)
                is DraftViewState.ResultState -> renderResultState(state)
                is DraftViewState.LiveDraftState -> renderLiveDraftState(state)
                is DraftViewState.LiveGameState -> renderLiveGameState(state)
            }
        }

        private fun renderLiveGameState(state: DraftViewState.LiveGameState) = with(binding) {
            ivAnalysis.isVisible = false
            ivData.isVisible = true
            svDraft.isVisible = false
            flResult.isVisible = true
            tvTitle.text = getString(state.gameMode.getResource())
            ivSearch.isVisible = false
            tvPrediction.isVisible = true
            tvRadiantNameResult.text = state.radiantName
            tvDireNameResult.text = state.direName
            ivRadiantHistoryResult.isVisible = true
            ivDireHistoryResult.isVisible = true
            rvTypes.isVisible = true
            ivCancelLastChoice.isVisible = false
            heroesResultAdapter.submitList(state.heroes)
            tvSelectedHero.text = getString(R.string.draft_items_title, state.selectedResultHero.name)
            draftItemTypeAdapter.submitList(state.items)
            typesAdapter.submitList(state.types)
            scrollRvToPosition(state.typeId, smoothScrollerTypes, rvTypes.layoutManager!!)
        }

        private fun renderLiveDraftState(state: DraftViewState.LiveDraftState) = with(binding) {
            ivData.isVisible = false
            ivAnalysis.isVisible = false
            svDraft.isVisible = true
            flResult.isVisible = false
            tvSkipBans.isVisible = !state.isAllPick
            tvPrediction.isVisible = false
            tvRadiantName.text = state.radiantName
            tvDireName.text = state.direName
            ivRadiantHistory.isVisible = true
            ivDireHistory.isVisible = true
            rvDireBans.isVisible = !state.isAllPick
            rvRadiantBans.isVisible = !state.isAllPick
            ivRevertSequence.isVisible = state.isRevertSequenceAvailable
            tvTitle.text = getString(state.gameMode.getResource())
            ivSearch.isVisible = true
            rvTypes.isVisible = true
            ivCancelLastChoice.isVisible = state.isCancelChoiceAvailable

            groupsAdapter.submitList(state.groups)
            filtersAdapter.submitList(state.filters)
            typesAdapter.submitList(state.types)
            tvFilterDescription.text = state.filterDescription
            tvFilterDescription.isVisible = state.filterDescription.isNotEmpty()
            direHeroesAdapter.submitList(state.direHeroes)
            direBansAdapter.submitList(state.direBans)
            radiantHeroesAdapter.submitList(state.radiantHeroes)
            radiantBansAdapter.submitList(state.radiantBans)
            sequenceAdapter.submitList(state.sequence)

            handleHeroesToSelect(state.heroesToSelect)

            scrollRvToPosition(state.draftStage, smoothScrollerSequence, rvSequence.layoutManager!!)
            scrollRvToPosition(state.groupId, smoothScrollerGroups, rvFilterGroups.layoutManager!!)
            scrollRvToPosition(state.filterId, smoothScrollerFilters, rvFilters.layoutManager!!)
            scrollRvToPosition(state.typeId, smoothScrollerTypes, rvTypes.layoutManager!!)
        }

        private fun handleHeroesToSelect(heroesToSelect: List<ComplexItem>) = with(binding) {
            if (heroesToSelect.isEmpty()) {
                rvHeroes.isVisible = false
                rvStrategies.isVisible = false
                tvPlaceholderHeroes.isVisible = true
            } else {
                tvPlaceholderHeroes.isVisible = false
                if (heroesToSelect.first() is ComplexItem.DraftStrategyItem) {
                    rvHeroes.isVisible = false
                    rvStrategies.isVisible = true
                    if (rvStrategies.adapter == null)
                        rvStrategies.adapter = draftStrategyAdapter
                    draftStrategyAdapter.submitList(heroesToSelect as List<ComplexItem.DraftStrategyItem>)
                } else {
                    rvHeroes.isVisible = true
                    rvStrategies.isVisible = false
                    if (rvHeroes.adapter == null)
                        rvHeroes.adapter = heroesToSelectAdapter
                    heroesToSelectAdapter.submitList(heroesToSelect)
                }
            }
        }

        private fun renderResultState(state: DraftViewState.ResultState) = with(binding) {
            ivData.isVisible = true
            (requireActivity() as? ActivityManager)?.showAppReview()
            if (state.isAllPick) {
                rvDireBans.isVisible = false
                rvRadiantBans.isVisible = false
            } else {
                rvDireBans.isVisible = true
                rvRadiantBans.isVisible = true
            }
            ivAnalysis.isVisible = state.hasAnalysis
            ivRadiantHistoryResult.isVisible = state.gameMode.hasTeams()
            ivDireHistoryResult.isVisible = state.gameMode.hasTeams()
            tvRadiantNameResult.text = state.radiantName
            tvDireNameResult.text = state.direName
            tvTitle.text = getString(state.gameMode.getResource())
            tvPrediction.isVisible = if (BuildConfig.DEBUG) state.gameMode.hasTeams() else state.gameMode.isLive()
            ivSearch.isVisible = false
            rvTypes.isVisible = true
            svDraft.isVisible = false
            flResult.isVisible = true
            ivCancelLastChoice.isVisible = state.isCancelChoiceAvailable

            heroesResultAdapter.submitList(state.heroes)
            tvSelectedHero.text = getString(R.string.draft_items_title, state.selectedResultHero.name)
            draftItemTypeAdapter.submitList(state.items)
            typesAdapter.submitList(state.types)
            scrollRvToPosition(state.typeId, smoothScrollerTypes, rvTypes.layoutManager!!)
        }

        private fun renderDefaultState(state: DraftViewState.DefaultState) = with(binding) {
            ivData.isVisible = false
            ivAnalysis.isVisible = false
            if (state.isAllPick) {
                rvDireBans.isVisible = false
                rvRadiantBans.isVisible = false
            } else {
                rvDireBans.isVisible = true
                rvRadiantBans.isVisible = true
            }
            tvSkipBans.isVisible = !state.isAllPick
            ivRadiantHistory.isVisible = state.gameMode.hasTeams()
            ivDireHistory.isVisible = state.gameMode.hasTeams()
            tvRadiantName.text = state.radiantName
            tvDireName.text = state.direName
            ivRevertSequence.isVisible = false
            tvTitle.text = getString(state.gameMode.getResource())
            tvPrediction.isVisible = false
            ivSearch.isVisible = true
            svDraft.isVisible = true
            flResult.isVisible = false
            rvTypes.isVisible = true
            ivCancelLastChoice.isVisible = state.isCancelChoiceAvailable

            rvRadiantBans.isVisible = !state.isAllPick
            rvDireBans.isVisible = !state.isAllPick
            groupsAdapter.submitList(state.groups)
            filtersAdapter.submitList(state.filters)
            typesAdapter.submitList(state.types)
            tvFilterDescription.text = state.filterDescription
            tvFilterDescription.isVisible = state.filterDescription.isNotEmpty()
            direHeroesAdapter.submitList(state.direHeroes)
            direBansAdapter.submitList(state.direBans)
            radiantHeroesAdapter.submitList(state.radiantHeroes)
            radiantBansAdapter.submitList(state.radiantBans)
            sequenceAdapter.submitList(state.sequence)

            handleHeroesToSelect(state.heroesToSelect)

            scrollRvToPosition(state.groupId, smoothScrollerGroups, rvFilterGroups.layoutManager!!)
            scrollRvToPosition(state.filterId, smoothScrollerFilters, rvFilters.layoutManager!!)
            scrollRvToPosition(state.draftStage, smoothScrollerSequence, rvSequence.layoutManager!!)
            scrollRvToPosition(state.typeId, smoothScrollerTypes, rvTypes.layoutManager!!)
        }

        private fun scrollRvToPosition(
            position: Int,
            smoothScroller: RecyclerView.SmoothScroller,
            layoutManager: RecyclerView.LayoutManager
        ) {
//            smoothScroller.targetPosition = position
//            layoutManager.startSmoothScroll(smoothScroller)
        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
                is ViewEvent.PopBackStack<*> -> navController.popBackStack()
            }
        }



        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            viewModel.viewEvent.setResultListener(this@DraftFragment)
            navController.navigateViaScreenRoute(screen)
        }
    }
}