package com.erg.almagestdota.drafting.data.raw

import com.erg.almagestdota.local_storage.external.models.draft.RoleType

class HeroKillsResponseRaw(
    val data: HeroKillsDataRaw
)

class HeroKillsDataRaw(
    val heroStats: KillsForFiltersRaw
)

class KillsForFiltersRaw(
    val laning: List<KillsEventRaw> = listOf(),
    val early: List<KillsEventRaw> = listOf(),
    val mid: List<KillsEventRaw> = listOf(),
    val superLate: List<KillsEventRaw> = listOf(),
    val late: List<KillsEventRaw> = listOf(),
)

class KillsEventRaw(
    val heroId: Int,
    val kills: Double,
    val deaths: Double,
    val assists: Double,
    val position: String,
    val cs: Double,
    val dn: Double,
    val neutrals: Double,
    val heroDamage: Double,
    val magicalDamage: Double,
    val towerDamage: Double,
    val damageReceived: Double,
    val healingAllies: Double,
    val disableCount: Double,
    val disableDuration: Double,
    val stunCount: Double,
    val stunDuration: Double,
)
