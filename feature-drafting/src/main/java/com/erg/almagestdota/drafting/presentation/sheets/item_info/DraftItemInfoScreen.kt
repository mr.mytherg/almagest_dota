package com.erg.almagestdota.drafting.presentation.sheets.item_info

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.drafting.domain.models.DotaItemResult
import com.erg.almagestdota.drafting.presentation.DraftFragmentDirections
import com.google.gson.Gson

class DraftItemInfoScreen(
    val item: DotaItemResult
) : Screen<NavDirections>(
    route = DraftFragmentDirections.toDraftItemInfoSheet(
        item = Gson().toJson(item),
    ),
    requestKey = DraftItemInfoSheet.TAG
)