package com.erg.almagestdota.drafting.presentation.sheets.item_info

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.erg.almagestdota.base.external.image.loadImageByUrl
import com.erg.almagestdota.heroselector.databinding.DraftHeroItemBinding
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData

class ItemHeroesAdapter :
    ListAdapter<HeroCoreData, ItemHeroesAdapter.HeroInDraftListViewHolder>(diffCallback) {

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<HeroCoreData>() {
            override fun areItemsTheSame(oldItem: HeroCoreData, newItem: HeroCoreData): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: HeroCoreData, newItem: HeroCoreData): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeroInDraftListViewHolder {
        val binding = DraftHeroItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return HeroInDraftListViewHolder(binding)
    }

    override fun onBindViewHolder(holder: HeroInDraftListViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    class HeroInDraftListViewHolder(
        private val binding: DraftHeroItemBinding,
    ) : RecyclerView.ViewHolder(binding.root) {
        private val context = binding.root.context

        fun bind(hero: HeroCoreData) = with(binding) {
            tvName.isVisible = true
            tvName.text = hero.name
            ivHero.loadImageByUrl(hero.imageLink)
        }
    }
}