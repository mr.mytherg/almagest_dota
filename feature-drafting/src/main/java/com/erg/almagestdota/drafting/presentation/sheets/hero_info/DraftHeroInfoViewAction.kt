package com.erg.almagestdota.drafting.presentation.sheets.hero_info

internal sealed class DraftHeroInfoViewAction {

    object ConfirmHero : DraftHeroInfoViewAction()
}