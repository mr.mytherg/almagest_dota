package com.erg.almagestdota.drafting.domain.use_cases

import android.content.Context
import com.erg.almagestdota.base.external.extensions.round1
import com.erg.almagestdota.local_storage.external.models.draft.*
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerInfo
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamDraft
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.predictor.domain.use_cases.FilterHeroesByRolesUseCase
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

internal class GetRecommendationsUseCase @Inject constructor(
    private val filterHeroesByRolesUseCase: FilterHeroesByRolesUseCase,
    private val filterPlayersByHeroesUseCase: FilterPlayersByHeroesUseCase,
    @ApplicationContext private val context: Context,
) {
    companion object {
        const val MIN_COEF = 0
        const val COUNTER_MIN_COEF = 0
    }

    operator fun invoke(
        gameMode: GameModeType,
        sortedHeroes: List<HeroDraftData>,
        teamDraft: TeamDraft,
        enemyTeam: TeamDraft,
        heroesPlayers: List<HeroPlayerData>,
        hasRadiantFirstPick: Boolean,
        isBanStage: Boolean,
        draftStage: Int,
        isHeroesForRadiant: Boolean,
        heroesValues: MutableMap<Int, HeroValues>
    ): List<HeroDraftData> {
        val teamHeroes = teamDraft.picks

        // фильтруем героев, отсекая тех, которые слишком плохи, учитывая текущие драфты
        val filteredByMeta = filterByMeta(sortedHeroes)
        val filteredByPlayersOrRoles = filterByPlayersOrRoles(gameMode, teamDraft, heroesPlayers, filteredByMeta, teamHeroes)
        val heroesFilteredByStages = filterHeroesByStages(teamHeroes, filteredByPlayersOrRoles, hasRadiantFirstPick, isHeroesForRadiant, isBanStage, draftStage)
        val heroesFilteredByValues = filterByValues(heroesFilteredByStages, enemyTeam, heroesValues)
        return heroesFilteredByValues
        // todo тут можно будет фильтровать героев и по другим параметрам (жадность, урон по строениям и прочее, чтобы сбалансировать пик)
    }

    private fun filterByValues(
        heroesFilteredByStages: List<HeroDraftData>,
        enemyTeam: TeamDraft,
        heroesValues: MutableMap<Int, HeroValues>
    ): List<HeroDraftData> {
        return heroesFilteredByStages.filter {
            if (it.counterValue + it.synergyValue >= MIN_COEF && it.counterValue >= COUNTER_MIN_COEF) {
                true
            } else {
                it.reasons.add(DraftReasonType.LOW_COEF)
                true
            }
        }
    }

    private fun filterByPlayersOrRoles(
        gameMode: GameModeType,
        teamDraft: TeamDraft,
        heroesPlayers: List<HeroPlayerData>,
        filteredByMeta: List<HeroDraftData>,
        teamHeroes: MutableList<PlayerInfo>
    ): List<HeroDraftData> {
        return if (gameMode.hasTeams() && teamDraft.heroPool.isNotEmpty()) {
            val filteredByPlayers = mutableListOf<HeroDraftData>()
            for (hero in filteredByMeta) {
                if (teamDraft.heroPool[hero.id].orEmpty().isEmpty()) {
                    filteredByPlayers.add(hero.apply { reasons.add(DraftReasonType.NOT_IN_POOL) })
                    continue
                }
                val isHeroCompatible = filterPlayersByHeroesUseCase.invoke(listOf(hero), teamDraft).filter { heroPlayer ->
                    heroesPlayers.find { it.id == heroPlayer.id } != null
                }.isNotEmpty()
                if (isHeroCompatible)
                    filteredByPlayers.add(hero)
                else {
                    filteredByPlayers.add(hero.apply { reasons.add(DraftReasonType.PLAYER_ALREADY_HAS_HERO) })
                }
            }
            val filteredByRoles = mutableListOf<HeroDraftData>()
            for (hero in filteredByPlayers) {
                val isHeroCompatible = filterHeroesByRolesUseCase.invoke(listOf(hero), teamHeroes.map { it.heroId }).isNotEmpty()
                if (isHeroCompatible)
                    filteredByRoles.add(hero)
                else {
                    filteredByRoles.add(hero.apply { reasons.add(DraftReasonType.HAS_HERO_FOR_THIS_ROLE) })
                }
            }
            filteredByRoles
        } else {
            val filteredByRoles = mutableListOf<HeroDraftData>()
            for (hero in filteredByMeta) {
                val isHeroCompatible = filterHeroesByRolesUseCase.invoke(listOf(hero), teamHeroes.map { it.heroId }).isNotEmpty()
                if (isHeroCompatible)
                    filteredByRoles.add(hero)
                else {
                    filteredByRoles.add(hero.apply { reasons.add(DraftReasonType.HAS_HERO_FOR_THIS_ROLE) })
                }
            }
            filteredByRoles
        }
    }

    private fun filterByMeta(sortedHeroes: List<HeroDraftData>): List<HeroDraftData> {
        return sortedHeroes.filter {
            if (GlobalVars.heroesAsMap[it.id]!!.factions[0].pickRate.round1() >= 2) {
                true
            } else {
                it.reasons.add(DraftReasonType.NON_META)
                true
            }
        }
    }

    private fun filterHeroesByStages(
        teamHeroes: List<PlayerInfo>,
        heroes: List<HeroDraftData>,
        hasRadiantFirstPick: Boolean,
        isHeroesForRadiant: Boolean,
        isBanStage: Boolean,
        draftStage: Int
    ): List<HeroDraftData> {
        if (isBanStage && draftStage > 6)
            return heroes
        val heroesFilteredByStages = mutableListOf<HeroDraftData>()
        val stagesGroup = GlobalVars.groups.find { it.type == DraftFilterGroupType.DRAFT_STAGES }!!
        if (hasRadiantFirstPick && isHeroesForRadiant || !hasRadiantFirstPick && !isHeroesForRadiant) {
            for (hero in heroes) {
                when (teamHeroes.size) {
                    0 -> if (stagesGroup.filters[0].heroes.findHeroByBinaryMethod(hero.id) == -1) {
                        heroesFilteredByStages.add(hero.apply { reasons.add(DraftReasonType.INVALID_STAGE) })
                    } else
                        heroesFilteredByStages.add(hero)
                    1 -> if (stagesGroup.filters[2].heroes.findHeroByBinaryMethod(hero.id) == -1) {
                        heroesFilteredByStages.add(hero.apply { reasons.add(DraftReasonType.INVALID_STAGE) })
                    } else
                        heroesFilteredByStages.add(hero)
                    2, 3 -> if (stagesGroup.filters[4].heroes.findHeroByBinaryMethod(hero.id) == -1) {
                        heroesFilteredByStages.add(hero.apply { reasons.add(DraftReasonType.INVALID_STAGE) })
                    } else
                        heroesFilteredByStages.add(hero)
                    4 -> heroesFilteredByStages.add(hero)
                    //для ластпика не должна учитываться стадия выбора
                }
            }
        } else {
            for (hero in heroes) {
                when (teamHeroes.size) {
                    0, 1 -> if (stagesGroup.filters[1].heroes.findHeroByBinaryMethod(hero.id) == -1) {
                        heroesFilteredByStages.add(hero.apply { reasons.add(DraftReasonType.INVALID_STAGE) })
                    } else
                        heroesFilteredByStages.add(hero)
                    2 -> if (stagesGroup.filters[3].heroes.findHeroByBinaryMethod(hero.id) == -1) {
                        heroesFilteredByStages.add(hero.apply { reasons.add(DraftReasonType.INVALID_STAGE) })
                    } else
                        heroesFilteredByStages.add(hero)
                    3 -> if (stagesGroup.filters[5].heroes.findHeroByBinaryMethod(hero.id) == -1) {
                        heroesFilteredByStages.add(hero.apply { reasons.add(DraftReasonType.INVALID_STAGE) })
                    } else
                        heroesFilteredByStages.add(hero)
                    4 -> heroesFilteredByStages.add(hero)
                }

            }
        }

        return heroesFilteredByStages
    }
}