package com.erg.almagestdota.drafting.presentation.sheets.match_ups

import androidx.constraintlayout.motion.utils.ViewState

internal sealed class DraftMatchUpsViewState : ViewState() {

    class DefaultState(
        val matchUps: List<DraftMatchUp>,
    ) : DraftMatchUpsViewState()
}