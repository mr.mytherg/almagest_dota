package com.erg.almagestdota.drafting.data.raw

class StratzPlayersDataRaw(
     val data: StratzPlayersRaw?
)

class StratzPlayersRaw(
     val players: List<StratzSteamRaw>?
)

class StratzSteamRaw(
     val steamAccount: StratzSteamAccountRaw?
)

class StratzSteamAccountRaw(
    val proSteamAccount:  StratzProSteamAccountRaw?
)

class StratzProSteamAccountRaw(
    val name: String?,
    val id: Long
)

