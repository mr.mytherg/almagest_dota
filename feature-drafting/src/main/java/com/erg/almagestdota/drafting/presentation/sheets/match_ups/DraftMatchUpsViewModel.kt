package com.erg.almagestdota.drafting.presentation.sheets.match_ups

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroRoleData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import java.lang.reflect.Type

internal class DraftMatchUpsViewModel @AssistedInject constructor(
    @Assisted private val matchUps: Set<List<HeroRoleData>>,
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    private inner class StateConfigurator {

        fun defineFragmentState(): DraftMatchUpsViewState {
            val list = mutableListOf<DraftMatchUp>()
            matchUps.forEachIndexed { index, item ->
                list.add(DraftMatchUp(index.toLong(), item))
            }

            return DraftMatchUpsViewState.DefaultState(list)
        }
    }

    companion object {
        fun provideFactory(
            matchUps: String,
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {
                val listType: Type = object : TypeToken<Set<List<HeroRoleData>>>() {}.type

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create(Gson().fromJson(matchUps, listType)) as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(
            matchUps: Set<List<HeroRoleData>>,
        ): DraftMatchUpsViewModel
    }
}

data class DraftMatchUp(
    val id: Long,
    val heroes: List<HeroRoleData>
)