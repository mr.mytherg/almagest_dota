package com.erg.almagestdota.drafting.domain

import android.content.Context
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.drafting.R
import com.erg.almagestdota.drafting.domain.exceptions.AlreadySelectedException
import com.erg.almagestdota.drafting.domain.models.*
import com.erg.almagestdota.drafting.domain.use_cases.*
import com.erg.almagestdota.predictor.domain.use_cases.FilterHeroesByRolesUseCase
import com.erg.almagestdota.drafting.domain.use_cases.FilterPlayersByHeroesUseCase
import com.erg.almagestdota.drafting.domain.use_cases.GetHeroForBotsTurnUseCase
import com.erg.almagestdota.drafting.domain.use_cases.GetRecommendationsUseCase
import com.erg.almagestdota.drafting.domain.use_cases.SortHeroesByPickerAlgorithmUseCase
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.DraftAnalysis
import com.erg.almagestdota.local_storage.external.models.draft.*
import com.erg.almagestdota.local_storage.external.models.draft.main.*
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import com.erg.almagestdota.predictor.domain.use_cases.LoadPlayerAccountsUseCase
import com.erg.almagestdota.predictor.domain.use_cases.LoadPlayerHeroPoolPublicUseCase
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.FragmentScoped
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import javax.inject.Inject
import kotlin.coroutines.coroutineContext

@FragmentScoped
internal class DraftInteractor @Inject constructor(
    private val getHeroForBotsTurnUseCase: GetHeroForBotsTurnUseCase,
    private val getRecommendationsUseCase: GetRecommendationsUseCase,
    private val sortHeroesByPickerAlgorithmUseCase: SortHeroesByPickerAlgorithmUseCase,
    private val filterHeroesByRolesUseCase: FilterHeroesByRolesUseCase,
    private val filterPlayersByHeroesUseCase: FilterPlayersByHeroesUseCase,
    private val loadTeamInfoUseCase: LoadTeamInfoUseCase,
    private val loadPlayerInfoUseCase: LoadPlayerInfoUseCase,
    private val localStorage: ILocalStorageContract,
    @ApplicationContext private val context: Context,
    private val getTeamStrategiesUseCase: GetTeamStrategiesUseCase,
    private val stringProvider: StringProvider,
    private val accountsUseCase: LoadPlayerAccountsUseCase,
    private val heroPoolPublicUseCase: LoadPlayerHeroPoolPublicUseCase,
) : IDraftInteractor {

    private var heroes: MutableList<Int> = mutableListOf()

    private var sortedHeroes: List<HeroDraftData> = listOf()

    private var heroesRoles: List<HeroRoleData> = listOf()
    private var heroesPlayers: List<HeroPlayerData> = listOf()

    private var heroesDraftReasons: List<HeroDraftData> = listOf()
    private var recommendations: List<HeroDraftData> = listOf()

    private var draftModel: DraftModel = DraftModel()
    private var draftStage: Int = 0

    private var dotabuffLoadingFailed = false
    private var groupId: Int = 0
    private var typeId: Int = 0
    private var filterId: Int = 0
    private var selectedResultHero: HeroCoreData = HeroCoreData.createNullHero()
    private var teamInfoWasLoaded = false
    private var groups = GlobalVars.groups
    private var groupRadiant: DraftFilterGroup? = null
    private var groupRadiantStrategies: List<DraftStrategy> = listOf()
    private var groupDire: DraftFilterGroup? = null
    private var groupDireStrategies: List<DraftStrategy> = listOf()

    private var wasRevertedAutomatically: Boolean = false

    private val draftAnalysisList = mutableListOf<DraftAnalysis>()

    override fun skipBans() {
        draftModel.isAllPick = true
        draftStage = draftModel.radiantTeam.picks.size + draftModel.direTeam.picks.size
        draftModel.sequence = draftModel.sequence.replace(GlobalVars.RADIANT_BAN.toString(), "")
        draftModel.sequence = draftModel.sequence.replace(GlobalVars.DIRE_BAN.toString(), "")
    }

    override fun hasAnalysis(): Boolean {
        return hasDraftDone() && !draftModel.isAllPick && draftModel.radiantTeam.bans.isNotEmpty() && draftModel.direTeam.bans.isNotEmpty()
    }

    override suspend fun getAnalysis(): List<DraftAnalysis> {
        if (draftAnalysisList.size != draftModel.sequence.length) {
            val heroesSequence = getHeroesSequence()
            draftStage = 0
            heroes = GlobalVars.mainSettings.heroes.map { it.id }.toMutableList()
            draftAnalysisList.clear()
            draftModel.radiantTeam.picks.clear()
            draftModel.radiantTeam.bans.clear()
            draftModel.direTeam.picks.clear()
            draftModel.direTeam.bans.clear()
            sortHeroes()
            for (hero in heroesSequence)
                selectHero(hero, false)
        }
        return draftAnalysisList
    }

    private fun getHeroesSequence(): List<Int> {
        val mutableList = mutableListOf<Int>()
        var radiantPick = 0
        var radiantBan = 0
        var direPick = 0
        var direBan = 0

        for (ch in draftModel.sequence) {
            when (ch) {
                GlobalVars.RADIANT_PICK -> {
                    mutableList.add(draftModel.radiantTeam.picks[radiantPick].heroId)
                    radiantPick++
                }
                GlobalVars.RADIANT_BAN -> {
                    mutableList.add(draftModel.radiantTeam.bans[radiantBan])
                    radiantBan++
                }
                GlobalVars.DIRE_PICK -> {
                    mutableList.add(draftModel.direTeam.picks[direPick].heroId)
                    direPick++
                }
                GlobalVars.DIRE_BAN -> {
                    mutableList.add(draftModel.direTeam.bans[direBan])
                    direBan++
                }
            }
        }

        return mutableList
    }

    override fun isRevertSequenceAvailable(): Boolean {
        return !isLastPick() && !wasRevertedAutomatically
    }

    override fun needToShowSearchSheet(): Boolean {
        return draftModel.radiantTeam.picks.size + draftModel.direTeam.picks.size < GlobalVars.mainSettings.getPickSizeCM() * 2 - 1
    }

    override fun isPubMatchesLoadingFailed(): Boolean {
        return dotabuffLoadingFailed
    }

    override fun getHeroReasons(heroId: Int): List<DraftReasonType> {
        val hero = heroesDraftReasons.find { it.id == heroId } ?: return listOf()
        return hero.reasons
    }

    override fun isValid(heroId: Int): Boolean {
        val hero = heroesDraftReasons.find { it.id == heroId } ?: return false
        return hero.reasons.find { it.isBad } == null
    }

    override fun setDraftModel(draftModel: DraftModel) {
        localStorage.draftModel = draftModel
    }

    override suspend fun revertSequence() {
        var newSequence = ""
        for (ch in draftModel.sequence) {
            newSequence += when (ch) {
                GlobalVars.RADIANT_PICK -> GlobalVars.DIRE_PICK
                GlobalVars.DIRE_PICK -> GlobalVars.RADIANT_PICK
                GlobalVars.DIRE_BAN -> GlobalVars.RADIANT_BAN
                else -> GlobalVars.DIRE_BAN
            }
        }
        draftModel.sequence = newSequence
        sortHeroes()
    }

    override suspend fun configureDraftModel() {
        groups = GlobalVars.groups
        heroes = GlobalVars.mainSettings.heroes.map { it.id }.toMutableList()
        draftModel = localStorage.draftModel!!
        draftStage = draftModel.direTeam.bans.size + draftModel.direTeam.picks.size +
            draftModel.radiantTeam.bans.size + draftModel.radiantTeam.picks.size
        checkDraftStage()
        removeSelectedHeroes(draftModel.radiantTeam.picks.map { it.heroId })
        removeSelectedHeroes(draftModel.direTeam.picks.map { it.heroId })
        removeSelectedHeroes(draftModel.radiantTeam.bans)
        removeSelectedHeroes(draftModel.direTeam.bans)
        if (!teamInfoWasLoaded && draftModel.gameMode.hasTeams()) {
            loadTeamInfo(draftModel.radiantTeam)
            groupRadiant = addGroup(true, draftModel.radiantTeam)
            loadTeamInfo(draftModel.direTeam)
            groupDire = addGroup(false, draftModel.direTeam)
            teamInfoWasLoaded = true
        }
        checkToShowWarning()
        handleDraftStage(true)
        checkSequence()
    }

    private fun checkToShowWarning() {
        if (draftModel.needToLoadPubMatches && draftModel.radiantTeam.heroPoolPub.isEmpty() && draftModel.direTeam.heroPoolPub.isEmpty()) {
            dotabuffLoadingFailed = true
        }
    }

    private fun checkDraftStage() {
        if (hasDraftDone() && !draftModel.isAllPick && draftModel.sequence.length != draftStage) {
            skipBans()
        }
    }

    private suspend fun checkSequence() {
        if (draftStage % 2 == 1) {
            val needToRevert = when (draftModel.sequence[draftStage - 1]) {
                GlobalVars.RADIANT_BAN -> !isSequenceValid(draftModel.radiantTeam.bans.size, GlobalVars.RADIANT_BAN)
                GlobalVars.RADIANT_PICK -> !isSequenceValid(draftModel.radiantTeam.picks.size, GlobalVars.RADIANT_PICK)
                GlobalVars.DIRE_BAN -> !isSequenceValid(draftModel.direTeam.bans.size, GlobalVars.DIRE_BAN)
                GlobalVars.DIRE_PICK -> !isSequenceValid(draftModel.direTeam.picks.size, GlobalVars.DIRE_PICK)
                else -> false
            }
            wasRevertedAutomatically = true
            if (needToRevert) {
                revertSequence()
            }
        }
    }

    private fun isSequenceValid(size: Int, ch: Char): Boolean {
        return draftModel.sequence.substring(0, draftStage).count { it == ch } == size
    }

    private fun addGroup(isRadiant: Boolean, team: TeamDraft): DraftFilterGroup {
        val players = mutableListOf<DraftFilter>()
        team.teamInfo.members.forEachIndexed { index, memberId ->
            val member = GlobalVars.players[memberId]
            val draftFilter = DraftFilter(
                id = index,
                name = "(${member?.rating ?: 1400}) ${member?.name?.ifEmpty { memberId.toString() }}",
                description = "",
                heroes = team.heroPool.filter {
                    it.value.values.find { it.playerId == memberId } != null
                }.map { it.key }.sorted()
            )
            players.add(draftFilter)
        }
        return DraftFilterGroup(
            name = team.teamInfo.name.ifEmpty {
                if (isRadiant) {
                    context.getString(R.string.filter_radiant)
                } else {
                    context.getString(R.string.filter_dire)
                }
            },
            type = DraftFilterGroupType.TEAM_PLAYERS,
            filters = players
        )
    }

    private fun removeSelectedHeroes(selectedHeroes: List<Int>) {
        for (heroId in selectedHeroes)
            heroes.remove(heroId)
    }

    private suspend fun loadTeamInfo(team: TeamDraft) {
        val currentGamePlayers = team.teamInfo.members
        val teamLoaded = loadTeamInfoUseCase.invoke(team.teamInfo.id).apply {
            if (team.teamInfo.name.isNotEmpty())
                name = team.teamInfo.name
        }
        team.teamInfo = teamLoaded
        if (currentGamePlayers.isNotEmpty()) {
            team.teamInfo.members = currentGamePlayers
        }

        loadPlayerInfo(team)
    }

    private suspend fun loadPlayerInfo(team: TeamDraft) {
        val members = mutableListOf<Deferred<Unit>>()
        for (playerId in team.teamInfo.members) {
            val deferred = CoroutineScope(coroutineContext).async {
                if (draftModel.needToLoadPubMatches) {
                    team.heroPoolPub[playerId] = heroPoolPublicUseCase.invoke(accountsUseCase.invoke(playerId), draftModel.monthType.monthCount)
                }
            }
            members.add(deferred)
        }
        members.awaitAll()
        team.setHeroPool(localStorage.metaMatches)
    }

    private suspend fun handleDraftStage(needToLookForStrategies: Boolean) {
        if (hasDraftDone()) {
            selectResultHero(draftModel.radiantTeam.picks.first().heroId)
            return
        }
        if (needToLookForStrategies)
            updateStrategies()
        if (draftModel.isDireBot || draftModel.gameMode.isBot()) {
            if (isDireStage()) {
                sortHeroes()
                val heroId: Int = getHeroForBotsTurnUseCase.invoke(recommendations, isFirstPick(), heroesPlayers)
                selectHero(heroId)
            } else {
                sortHeroes()
            }
        } else {
            sortHeroes()
        }
    }

    private suspend fun updateStrategies() {
        if (!draftModel.gameMode.hasTeams()) return
        if (isRadiantStage(draftStage))
            groupRadiantStrategies = getTeamStrategiesUseCase.invoke(draftModel, true, isRadiantStage(0))
        else
            groupDireStrategies = getTeamStrategiesUseCase.invoke(draftModel, false, isRadiantStage(0))
    }

    override fun getDraftModel(): DraftModel {
        return draftModel
    }

    override suspend fun selectHero(heroId: Int, needToSaveDraft: Boolean) {
        if (heroes.find { it == heroId } == null) throw AlreadySelectedException()
        when (draftModel.sequence[draftStage]) {
            GlobalVars.RADIANT_BAN -> draftModel.radiantTeam.bans.add(heroId)
            GlobalVars.RADIANT_PICK -> draftModel.radiantTeam.picks.add(PlayerInfo(heroId, 0L))
            GlobalVars.DIRE_BAN -> draftModel.direTeam.bans.add(heroId)
            GlobalVars.DIRE_PICK -> draftModel.direTeam.picks.add(PlayerInfo(heroId, 0L))
            else -> Unit
        }
        heroes.remove(heroId)
        draftAnalysisList.add(
            DraftAnalysis(
                draftStage,
                heroId,
                heroesDraftReasons.toList(),
                heroesPlayers.toList(),
                draftModel.copy(
                    radiantTeam = draftModel.radiantTeam.copy(
                        picks = draftModel.radiantTeam.picks.toMutableList(),
                        bans = draftModel.radiantTeam.bans.toMutableList(),
                        teamInfo = draftModel.radiantTeam.teamInfo
                    ),
                    direTeam = draftModel.direTeam.copy(
                        picks = draftModel.direTeam.picks.toMutableList(),
                        bans = draftModel.direTeam.bans.toMutableList(),
                        teamInfo = draftModel.direTeam.teamInfo
                    ),
                )
            )
        )
        draftStage++
        if (hasDraftDone() && needToSaveDraft) {
            saveDraft()
        }
        handleDraftStage(needToSaveDraft)
    }

    private fun saveDraft() {
        if (draftModel.gameMode.isLive()) return
        val radiantInfo = draftModel.radiantTeam.teamInfo.copy(members = draftModel.radiantTeam.teamInfo.members)
        val direInfo = draftModel.direTeam.teamInfo.copy(members = draftModel.direTeam.teamInfo.members)
        val radiantTeam = draftModel.radiantTeam.copy(teamInfo = radiantInfo)
        val direTeam = draftModel.direTeam.copy(teamInfo = direInfo)
        val draft = draftModel.copy(radiantTeam = radiantTeam, direTeam = direTeam, needToLoadPubMatches = draftModel.needToLoadPubMatches)
        localStorage.addItemToDraftHistory(DraftHistoryItem(draft))
    }

    override fun hasDraftDone(): Boolean {
        return draftModel.radiantTeam.picks.size == GlobalVars.mainSettings.getPickSizeCM() &&
            draftModel.direTeam.picks.size == GlobalVars.mainSettings.getPickSizeCM()
    }

    override fun hasDraftStarted(): Boolean {
        return draftStage != 0
    }

    override fun getHeroesToSelect(): List<HeroCoreData> {
        val list = when {
            groupId == 0 && filterId == 0 -> {
                recommendations
            }
            groupId == 0 && filterId == 1 -> {
                sortedHeroes.sortedByDescending { it.stealValue }
            }
            groupId == 0 && filterId == 2 -> {
                sortedHeroes.sortedByDescending { it.counterValue }
            }
            groupId == 0 && filterId == 3 -> {
                sortedHeroes.sortedByDescending { it.synergyValue }
            }
            groupId == 0 && filterId == 4 -> {
                sortedHeroes
            }
            groupId == 2 && draftModel.gameMode.hasTeams() -> {
                val strategies = if (isRadiantStage(draftStage)) groupRadiantStrategies else groupDireStrategies
                return getHeroesListFromStrategies(strategies)
            }
            else -> {
                val currentFilter = getGroups()[groupId].filters[filterId].heroes
                sortedHeroes.filter { currentFilter.findHeroByBinaryMethod(it.id) != -1 }
            }
        }
        return when (typeId) {
            0 -> list
            1 -> heroesRoles.filter { heroRole -> list.find { it.id == heroRole.id } != null }
            2 -> heroesPlayers.filter { heroPlayers -> list.find { it.id == heroPlayers.id } != null }
            else -> listOf()
        }
    }

    private fun getHeroesListFromStrategies(strategies: List<DraftStrategy>): List<HeroCoreData> {
        val mutableList = mutableListOf<HeroCoreData>()
        for (strategy in strategies) {
            for (player in strategy.picks) {
                val hero = GlobalVars.heroesAsMap[player.heroId]!!
                mutableList.add(
                    HeroSimplePlayerData(
                        id = hero.id,
                        name = hero.name,
                        imageLink = hero.imageLink,
                        heroPlayer = Pair(player.playerId, GlobalVars.players[player.playerId]?.name.orDefault())
                    )
                )
            }
            for (heroId in strategy.bans) {
                val hero = GlobalVars.heroesAsMap[heroId]!!
                mutableList.add(
                    HeroCoreData(
                        id = hero.id,
                        name = hero.name,
                        imageLink = hero.imageLink,
                    )
                )
            }
        }
        return mutableList
    }

    override suspend fun cancelLastSelection() {
        if (draftStage <= 0) return
        if (draftModel.isDireBot) {
            do {
                draftAnalysisList.removeLast()
                draftStage--
                removeLastChosenHero()
            } while (isDireStage())
        } else {
            draftAnalysisList.removeLast()
            draftStage--
            removeLastChosenHero()
        }
        updateStrategies()
        sortHeroes()
    }

    override suspend fun selectFilter(filterId: Int) {
        if (this.filterId == filterId) return
        this.filterId = filterId
        sortHeroes()
    }

    override suspend fun selectType(typeId: Int) {
        if (this.typeId == typeId) return
        this.typeId = typeId
        sortHeroes()
    }

    override suspend fun selectGroup(groupType: DraftFilterGroupType) {
        val groups = getGroups()
        var groupId = -1
        for (index in groups.indices) {
            val group = groups[index]
            if (group.type == groupType) {
                groupId = index
                break
            }
        }
        if (this.groupId == groupId) return
        this.groupId = groupId
        this.filterId = 0
        sortHeroes()
    }

    override fun getGroups(): List<DraftFilterGroup> {
        return if (draftModel.gameMode.hasTeams() && !hasDraftDone()) {
            groups.toMutableList().apply {
                add(1, if (isForDireStage()) groupDire!! else groupRadiant!!)
                add(
                    2,
                    DraftFilterGroup(
                        name = stringProvider.getString(R.string.group_strategies),
                        type = DraftFilterGroupType.TEAM_STRATEGIES,
                        filters = listOf(
                            DraftFilter(
                                id = 0,
                                name = stringProvider.getString(R.string.group_strategies_filter_all),
                                description = stringProvider.getString(R.string.group_strategies_description),
                                heroes = listOf()
                            )
                        )
                    )
                )
            }
        } else {
            groups
        }
    }

    override fun getFilters(): List<DraftFilter> {
        return getGroups()[groupId].filters
    }

    override fun getTypes(): List<HeroDraftType> {
        return if (draftModel.gameMode.hasTeams()) {
            listOf(
                HeroDraftType.Coefficients(),
                HeroDraftType.Roles(),
                HeroDraftType.Players(),
            )
        } else {
            listOf(
                HeroDraftType.Coefficients(),
                HeroDraftType.Roles(),
            )
        }
    }

    override fun getFilterId() = filterId
    override fun getDraftStage() = draftStage

    override fun getGroupId() = groupId
    override fun getTypeId() = typeId

    override fun isCurrentStage(stage: Int) = draftStage == stage

    override fun isPickStage(stage: Int): Boolean {
        return draftModel.sequence[stage] == GlobalVars.RADIANT_PICK || draftModel.sequence[stage] == GlobalVars.DIRE_PICK
    }

    override fun isBanStage(): Boolean =
        draftModel.sequence[draftStage] == GlobalVars.RADIANT_BAN || draftModel.sequence[draftStage] == GlobalVars.DIRE_BAN

    override fun isRadiantStage(stage: Int): Boolean {
        return draftModel.sequence[stage] == GlobalVars.RADIANT_PICK || draftModel.sequence[stage] == GlobalVars.RADIANT_BAN
    }

    override fun getSelectedResultHero() = selectedResultHero

    override fun isCancelChoiceAvailable(): Boolean {
        return when {
            draftModel.gameMode.isLiveGame() || draftModel.gameMode.isLiveFinished() -> false
            draftModel.isDireBot -> {
                draftModel.radiantTeam.picks.isNotEmpty() || draftModel.radiantTeam.bans.isNotEmpty()
            }
            else -> {
                hasDraftStarted()
            }
        }
    }

    override fun getHeroesDraftReasons(): List<HeroDraftData> {
        return heroesDraftReasons
    }

    override fun getFilterDescription(): String {
        return getFilters()[filterId].description
    }

    override fun selectResultHero(heroId: Int) {
        selectedResultHero = heroId.getHeroById()
    }

    override fun getTeamHeroes(isRadiant: Boolean): List<HeroCoreData> {
        val team = if (isRadiant) draftModel.radiantTeam else draftModel.direTeam
        val teamHeroes = if (isRadiant) draftModel.radiantTeam.picks else draftModel.direTeam.picks
        val enemyHeroes =
            if (!isRadiant) draftModel.radiantTeam.picks else draftModel.direTeam.picks
        return when (typeId) {
            0 -> {
                teamHeroes.map {
                    val hero = it.heroId.getHeroById()
                    HeroResultData(
                        id = hero.id,
                        name = hero.name,
                        imageLink = hero.imageLink,
                        counterValue = hero.id.getCounter(
                            enemyHeroes.map { it.heroId },
                            getRelevantValues()
                        ),
                        synergyValue = hero.id.getSynergy(
                            teamHeroes.map { it.heroId },
                            getRelevantValues()
                        ),
                    )
                }
            }
            1 -> filterHeroesByRolesUseCase.getHeroesRoles(teamHeroes.map { it.heroId })
            2 -> {
                if (draftModel.gameMode.isLiveGame() || draftModel.gameMode.isLiveFinished()) {
                    getPlayersStats(team, teamHeroes)
                } else {
                    filterPlayersByHeroesUseCase.getHeroesPlayers(team)

                }
            }
            else -> listOf()
        }
    }

    private fun getRelevantValues() = if (draftModel.gameMode.hasTeams() || draftModel.gameMode.isBot()) {
        GlobalVars.importantSettings.heroesProValues
    } else {
        GlobalVars.importantSettings.heroesValues
    }

    // main logic
    private suspend fun sortHeroes() {
        if (hasDraftDone()) return

        val team = if (isForDireStage()) draftModel.direTeam else draftModel.radiantTeam
        val enemyTeam = if (!isForDireStage()) draftModel.direTeam else draftModel.radiantTeam

        sortedHeroes = sortHeroesByPickerAlgorithmUseCase.invoke(
            draftModel = draftModel,
            heroes = heroes,
            allies = getAllies(),
            allieTeam = team,
            enemies = getEnemies(),
            enemyTeam = enemyTeam,
            heroesValues = getRelevantValues()
        )
        heroesRoles = sortedHeroes.map { draftData ->
            filterHeroesByRolesUseCase.getHeroesRoles(
                team.picks.map { it.heroId }
                    .plus(draftData.id)
            ).last()
        }

        if (draftModel.gameMode.hasTeams()) {
            heroesPlayers = sortedHeroes.map { hero ->
                val heroPlayers = mutableSetOf<Pair<Long, String>>()
                val playerStats = team.heroPool[hero.id] ?: mapOf()
                var wonCount = 0
                var lostCount = 0
                var wonCountPub = 0
                var lostCountPub = 0
                for ((id, player) in playerStats) {
                    heroPlayers.add(player.playerId to player.playerName)
                    wonCount += player.wonCount
                    wonCountPub += player.wonCountPub
                    lostCount += player.lostCount
                    lostCountPub += player.lostCountPub
                }
                HeroPlayerData(
                    id = hero.id,
                    imageLink = hero.imageLink,
                    name = hero.name,
                    heroPlayers = heroPlayers,
                    wonCount = wonCount,
                    lostCount = lostCount,
                    wonCountPub = wonCountPub,
                    lostCountPub = lostCountPub
                )
            }
        }

        heroesDraftReasons = getRecommendationsUseCase.invoke(
            gameMode = draftModel.gameMode,
            sortedHeroes = sortedHeroes,
            teamDraft = team,
            enemyTeam = enemyTeam,
            heroesPlayers = heroesPlayers,
            hasRadiantFirstPick = isRadiantStage(0),
            isBanStage = isBanStage(),
            draftStage = draftStage,
            heroesValues = getRelevantValues(),
            isHeroesForRadiant = isRadiantStage(draftStage)
        )
        recommendations = heroesDraftReasons.filter { it.reasons.count { it.isBad } == 0 }.ifEmpty {
            heroesDraftReasons.filter {
                it.reasons.find {
                    it == DraftReasonType.HAS_HERO_FOR_THIS_ROLE
                        || it == DraftReasonType.PLAYER_ALREADY_HAS_HERO
                } == null
            }.sortedBy { it.reasons.count { it.isBad } }.take(5)
        }
    }


    private fun getPlayersStats(team: TeamDraft, heroes: List<PlayerInfo>): List<HeroPlayerData> {
        return heroes.map { heroPlayer ->
            val heroPlayers = mutableSetOf<Pair<Long, String>>()
            val playerStats = team.heroPool[heroPlayer.heroId] ?: mapOf()
            val player = playerStats[heroPlayer.playerId] ?: PlayerStats.createEmptyPlayer(playerId = heroPlayer.playerId, team.teamInfo.members)
            heroPlayers.add(player.playerId to player.playerName)
            val hero = heroPlayer.heroId.getHeroById()
            HeroPlayerData(
                id = hero.id,
                imageLink = hero.imageLink,
                name = hero.name,
                heroPlayers = heroPlayers,
                wonCount = player.wonCount,
                lostCount = player.lostCount,
                wonCountPub = player.wonCountPub,
                lostCountPub = player.lostCountPub,
            )
        }
    }

    private fun removeLastChosenHero() {
        val heroId = when (draftModel.sequence[draftStage]) {
            GlobalVars.RADIANT_BAN -> draftModel.radiantTeam.bans.removeLast()
            GlobalVars.RADIANT_PICK -> draftModel.radiantTeam.picks.removeLast().heroId
            GlobalVars.DIRE_BAN -> draftModel.direTeam.bans.removeLast()
            GlobalVars.DIRE_PICK -> draftModel.direTeam.picks.removeLast().heroId
            else -> 0
        }
        heroes.add(heroId)
    }

    private fun getAllies(): List<Int> {
        return if (isForDireStage()) {
            draftModel.direTeam.picks.map { it.heroId }
        } else {
            draftModel.radiantTeam.picks.map { it.heroId }
        }
    }

    private fun getEnemies(): List<Int> {
        return if (isForDireStage()) {
            draftModel.radiantTeam.picks.map { it.heroId }
        } else {
            draftModel.direTeam.picks.map { it.heroId }
        }
    }

    private fun isDireStage(): Boolean {
        return draftModel.sequence[draftStage] == GlobalVars.DIRE_PICK || draftModel.sequence[draftStage] == GlobalVars.DIRE_BAN
    }

    private fun isForDireStage(): Boolean {
        return draftModel.sequence[draftStage] == GlobalVars.DIRE_PICK || draftModel.sequence[draftStage] == GlobalVars.RADIANT_BAN
    }

    private fun isFirstPick(): Boolean {
        return draftStage == 0
    }

    override fun getResultItems(): List<DotaItemList> {
        val enemies =
            if (draftModel.radiantTeam.picks.find { it.heroId == selectedResultHero.id } == null) {
                draftModel.radiantTeam.picks
            } else {
                draftModel.direTeam.picks
            }
        return listOf(
            DotaItemList(
                R.string.draft_early_items,
                getCounteredHeroes(
                    GlobalVars.mainSettings.heroesItems[selectedResultHero.id]?.early.orEmpty(),
                    enemies
                )
            ),
            DotaItemList(
                R.string.draft_main_items,
                getCounteredHeroes(
                    GlobalVars.mainSettings.heroesItems[selectedResultHero.id]?.main.orEmpty(),
                    enemies
                )
            ),
            DotaItemList(
                R.string.draft_situational_items,
                getCounteredHeroes(
                    GlobalVars.mainSettings.heroesItems[selectedResultHero.id]?.situational.orEmpty(),
                    enemies
                )
            ),
            DotaItemList(
                R.string.draft_neutral_1_items,
                getCounteredHeroes(
                    GlobalVars.mainSettings.heroesItems[selectedResultHero.id]?.neutralTier1.orEmpty(),
                    enemies
                )
            ),
            DotaItemList(
                R.string.draft_neutral_2_items,
                getCounteredHeroes(
                    GlobalVars.mainSettings.heroesItems[selectedResultHero.id]?.neutralTier2.orEmpty(),
                    enemies
                )
            ),
            DotaItemList(
                R.string.draft_neutral_3_items,
                getCounteredHeroes(
                    GlobalVars.mainSettings.heroesItems[selectedResultHero.id]?.neutralTier3.orEmpty(),
                    enemies
                )
            ),
            DotaItemList(
                R.string.draft_neutral_4_items,
                getCounteredHeroes(
                    GlobalVars.mainSettings.heroesItems[selectedResultHero.id]?.neutralTier4.orEmpty(),
                    enemies
                )
            ),
            DotaItemList(
                R.string.draft_neutral_5_items,
                getCounteredHeroes(
                    GlobalVars.mainSettings.heroesItems[selectedResultHero.id]?.neutralTier5.orEmpty(),
                    enemies
                )
            ),
            DotaItemList(
                R.string.draft_against_hero_items,
                getAgainstHeroItems(
                    GlobalVars.mainSettings.heroesItems[selectedResultHero.id]?.againstHero.orEmpty(),
                    enemies
                )
            ),
        )
    }

    private fun getAgainstHeroItems(
        items: List<DotaItem>,
        enemies: List<PlayerInfo>
    ): List<DotaItemResult> {
        val mutableList = mutableListOf<DotaItemResult>()
        for (item in items) {
            val countersHeroes = mutableListOf<Int>()
            for (enemy in enemies) {
                val enemyCanCounterCurrentHeroWithItems =
                    GlobalVars.mainSettings.heroesItems[enemy.heroId]!!.main
                        .plus(GlobalVars.mainSettings.heroesItems[enemy.heroId]!!.early)
                        .plus(GlobalVars.mainSettings.heroesItems[enemy.heroId]!!.situational)
                if (enemyCanCounterCurrentHeroWithItems.find { it.id == item.id } != null) {
                    countersHeroes.add(enemy.heroId)
                }
            }
            mutableList.add(
                DotaItemResult(
                    heroId = selectedResultHero.id,
                    id = item.id,
                    name = item.name,
                    imageLink = item.imageLink,
                    neutralTier = item.neutralTier,
                    early = item.early,
                    team = item.team,
                    analogs = item.analogs,
                    countersHeroes = countersHeroes
                )
            )
        }
        return mutableList
    }

    private fun getCounteredHeroes(
        items: List<DotaItem>,
        enemies: List<PlayerInfo>
    ): List<DotaItemResult> {
        val mutableList = mutableListOf<DotaItemResult>()
        for (item in items) {
            val countersHeroes = mutableListOf<Int>()
            for (enemy in enemies) {
                val enemyCanBeCounteredByItems =
                    GlobalVars.mainSettings.heroesItems[enemy.heroId]!!.againstHero
                if (enemyCanBeCounteredByItems.find { heroItem -> heroItem.id == item.id || item.analogs.find { it == heroItem.id } != null } != null) {
                    countersHeroes.add(enemy.heroId)
                }
            }
            mutableList.add(
                DotaItemResult(
                    heroId = selectedResultHero.id,
                    id = item.id,
                    name = item.name,
                    imageLink = item.imageLink,
                    neutralTier = item.neutralTier,
                    early = item.early,
                    team = item.team,
                    analogs = item.analogs,
                    countersHeroes = countersHeroes
                )
            )
        }
        return mutableList
    }

    private fun isLastPick(): Boolean {
        return draftModel.sequence.length - 1 == draftStage
    }
}