package com.erg.almagestdota.drafting.data

import com.erg.almagestdota.drafting.data.raw.*
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface IStratzGraphqlApi {
    companion object {
        private const val TOKEN = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiJodHRwczovL3N0ZWFtY29tbXVuaXR5LmNvbS9vcGVuaWQvaWQvNzY1NjExOTgwOTczNTgxNDAiLCJ1bmlxdWVfbmFtZSI6ItC_0L7Qt9Cw0LLRh9C10YDQsCIsIlN1YmplY3QiOiI0NDViMWNiNi05NTZkLTRmYmItOGZkYi02ZjE1OGMwMTcyOTciLCJTdGVhbUlkIjoiMTM3MDkyNDEyIiwibmJmIjoxNjYwMDM4MTU3LCJleHAiOjE2OTE1NzQxNTcsImlhdCI6MTY2MDAzODE1NywiaXNzIjoiaHR0cHM6Ly9hcGkuc3RyYXR6LmNvbSJ9.5uhj-fcWgDE2CwnKW6JmS0bYBK8-2kPr4hg0Ugg6clI"
    }

    @Headers("authorization: $TOKEN")
    @GET("graphql")
    suspend fun getTeamsInfo(
        @Query("query") query: String
    ): TeamsInfoResponseRaw

    @Headers("authorization: $TOKEN")
    @GET("graphql")
    suspend fun getMatchesLanes(
        @Query("query") query: String
    ): MatchInfoResponseRaw

    @Headers("authorization: $TOKEN")
    @GET("graphql")
    suspend fun getHeroDamage(
        @Query("query") query: String = "{heroStats{stats(heroIds:[${GlobalVars.getAllIds()}],bracketBasicIds:[DIVINE_IMMORTAL]){heroId,magicalDamage,physicalDamage,pureDamage,towerDamage,healingAllies,healingItemAllies}}}"
    ): HeroStatsResponseRaw

    @Headers("authorization: $TOKEN")
    @GET("graphql")
    suspend fun getHeroKillsSuperLate(
        @Query("query") query: String = "{heroStats{" +
            "superLate:stats(heroIds:[${GlobalVars.getAllIds()}],bracketBasicIds:[DIVINE_IMMORTAL],groupByPosition:true,minTime:51,maxTime:70){heroId,kills,assists,position,deaths,cs,dn,neutrals,heroDamage,towerDamage,damageReceived,healingAllies,disableCount,disableDuration,stunCount,stunDuration,magicalDamage}" +
            "}}"
    ): HeroKillsResponseRaw

    @Headers("authorization: $TOKEN")
    @GET("graphql")
    suspend fun getHeroKillsLate(
        @Query("query") query: String = "{heroStats{" +
            "late:stats(heroIds:[${GlobalVars.getAllIds()}],bracketBasicIds:[DIVINE_IMMORTAL],groupByPosition:true,minTime:37,maxTime:70){heroId,kills,assists,position,deaths,cs,dn,neutrals,heroDamage,towerDamage,damageReceived,healingAllies,disableCount,disableDuration,stunCount,stunDuration,magicalDamage}" +
            "}}"
    ): HeroKillsResponseRaw

    @Headers("authorization: $TOKEN")
    @GET("graphql")
    suspend fun getHeroKillsMid(
        @Query("query") query: String = "{heroStats{" +
            "mid:stats(heroIds:[${GlobalVars.getAllIds()}],bracketBasicIds:[DIVINE_IMMORTAL],groupByPosition:true,minTime:21,maxTime:36){heroId,kills,assists,position,deaths,cs,dn,neutrals,heroDamage,towerDamage,damageReceived,healingAllies,disableCount,disableDuration,stunCount,stunDuration,magicalDamage}" +
            "}}"
    ): HeroKillsResponseRaw

    @Headers("authorization: $TOKEN")
    @GET("graphql")
    suspend fun getHeroKillsEarly(
        @Query("query") query: String = "{heroStats{" +
            "early:stats(heroIds:[${GlobalVars.getAllIds()}],bracketBasicIds:[DIVINE_IMMORTAL],groupByPosition:true,minTime:9,maxTime:20){heroId,kills,assists,position,deaths,cs,dn,neutrals,heroDamage,towerDamage,damageReceived,healingAllies,disableCount,disableDuration,stunCount,stunDuration,magicalDamage}" +
            "}}"
    ): HeroKillsResponseRaw

    @Headers("authorization: $TOKEN")
    @GET("graphql")
    suspend fun getHeroKillsLaning(
        @Query("query") query: String = "{heroStats{" +
            "laning:stats(heroIds:[${GlobalVars.getAllIds()}],bracketBasicIds:[DIVINE_IMMORTAL],groupByPosition:true,minTime:0,maxTime:8){heroId,kills,assists,position,deaths,cs,dn,neutrals,heroDamage,towerDamage,damageReceived,healingAllies,disableCount,disableDuration,stunCount,stunDuration,magicalDamage}" +
            "}}"
    ): HeroKillsResponseRaw

    @Headers("authorization: $TOKEN")
    @GET("graphql")
    suspend fun getHeroRoles(
        @Query("query") query: String
    ): HeroRolesResponseRaw

    @Headers("authorization: $TOKEN")
    @GET("graphql")
    suspend fun loadCoefficientsSynergy(
        @Query("query") query: String
    ): HeroCoefficientSynergyResponseRaw

    @Headers("authorization: $TOKEN")
    @GET("graphql")
    suspend fun getPlayers(
        @Query("query") query: String
    ): StratzPlayersDataRaw
}