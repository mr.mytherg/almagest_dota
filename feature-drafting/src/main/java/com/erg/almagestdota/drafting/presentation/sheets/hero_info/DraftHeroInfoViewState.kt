package com.erg.almagestdota.drafting.presentation.sheets.hero_info

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.local_storage.external.models.draft.HeroDraftData
import com.erg.almagestdota.local_storage.external.models.draft.HeroRole
import com.erg.almagestdota.local_storage.external.models.draft.HeroStage
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData

internal sealed class DraftHeroInfoViewState : ViewState() {

    class DefaultState(
        val hero: HeroCoreData,
        val list: List<ComplexItem>
    ) : DraftHeroInfoViewState()
}