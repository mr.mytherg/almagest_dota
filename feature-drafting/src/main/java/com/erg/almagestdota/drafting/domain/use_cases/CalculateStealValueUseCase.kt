package com.erg.almagestdota.drafting.domain.use_cases

import android.content.Context
import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.drafting.R
import com.erg.almagestdota.drafting.domain.use_cases.GetRecommendationsUseCase.Companion.COUNTER_MIN_COEF
import com.erg.almagestdota.drafting.domain.use_cases.GetRecommendationsUseCase.Companion.MIN_COEF
import com.erg.almagestdota.local_storage.external.models.draft.HeroValues
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import com.erg.almagestdota.predictor.domain.use_cases.FilterHeroesByRolesUseCase
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

internal class CalculateStealValueUseCase @Inject constructor(
    private val filterHeroesByRolesUseCase: FilterHeroesByRolesUseCase,
    @ApplicationContext private val context: Context,
) {
    operator fun invoke(
        hero: Int,
        alliesHeroes: List<Int>,
        enemiesHeroes: List<Int>,
        listOfReasons: MutableList<String>? = null,
        alliesValues: Map<Int, HeroValues>,
        enemiesValues: Map<Int, HeroValues>
    ): Double {
        val STEAL_MULT = 0.15
        var stealValue = 0.0
        if (isFirstPick(alliesHeroes, enemiesHeroes) || isLastPick(alliesHeroes, enemiesHeroes)) {
            stealValue = 0.0
        } else if (isHeroRoleFitsForTeam(hero, alliesHeroes) && isHeroRoleFitsForTeam(hero, enemiesHeroes)) {
            val synergy = hero.getSynergy(alliesHeroes, alliesValues)
            val counter = hero.getCounter(enemiesHeroes, alliesValues)
            val synergyEnemy = hero.getSynergy(enemiesHeroes, enemiesValues)
            val counterEnemy = hero.getCounter(alliesHeroes, enemiesValues)
            if (synergy + counter >= MIN_COEF && counter >= COUNTER_MIN_COEF &&
                synergyEnemy + counterEnemy >= MIN_COEF && counterEnemy >= COUNTER_MIN_COEF
            ) {
                stealValue = synergy + counter + synergyEnemy + counterEnemy
            }
        }

        return (STEAL_MULT * stealValue).round2()
    }

    private fun isHeroRoleFitsForTeam(hero: Int, heroes: List<Int>): Boolean {
        return filterHeroesByRolesUseCase.invoke(listOf(hero.getHeroById()), heroes).size == 1
    }

    private fun isLastPick(userHeroes: List<Int>, enemyHeroes: List<Int>): Boolean {
        return userHeroes.size + enemyHeroes.size == GlobalVars.mainSettings.getPickSizeCM() * 2 - 1
    }

    private fun isFirstPick(
        userHeroes: List<Int>,
        enemyHeroes: List<Int>
    ): Boolean {
        return userHeroes.size + enemyHeroes.size == 0
    }
}