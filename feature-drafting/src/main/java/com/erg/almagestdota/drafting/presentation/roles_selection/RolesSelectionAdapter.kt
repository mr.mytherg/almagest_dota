package com.erg.almagestdota.drafting.presentation.roles_selection

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.complexadapter.viewHolders.matches.PlayedMatchViewHolder
import com.erg.almagestdota.complexadapter.viewHolders.other.RolesSelectionViewHolder

class RolesSelectionAdapter(
    private val complexListener: ComplexListener,
) : ListAdapter<ComplexItem.RoleSelectionItem, RolesSelectionViewHolder>(diffCallback) {

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<ComplexItem.RoleSelectionItem>() {
            override fun areItemsTheSame(
                oldItem: ComplexItem.RoleSelectionItem,
                newItem: ComplexItem.RoleSelectionItem
            ): Boolean {
                return oldItem.key == newItem.key
            }

            override fun areContentsTheSame(
                oldItem: ComplexItem.RoleSelectionItem,
                newItem: ComplexItem.RoleSelectionItem
            ): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RolesSelectionViewHolder {
        return RolesSelectionViewHolder.create(parent, complexListener)
    }

    override fun onBindViewHolder(holder: RolesSelectionViewHolder, position: Int) {
        holder.bind(currentList[position])
    }
}