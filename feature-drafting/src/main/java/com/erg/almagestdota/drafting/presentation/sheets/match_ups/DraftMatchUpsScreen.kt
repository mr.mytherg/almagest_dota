package com.erg.almagestdota.drafting.presentation.sheets.match_ups

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.drafting.presentation.DraftFragmentDirections
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroRoleData
import com.google.gson.Gson

class DraftMatchUpsScreen(
    matchUps: Set<List<HeroRoleData>>,
) : Screen<NavDirections>(
    route = DraftFragmentDirections.toDraftMatchUpsSheet(
        matchUps = Gson().toJson(matchUps)
    ),
    requestKey = DraftMatchUpsSheet.TAG
)