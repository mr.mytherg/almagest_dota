package com.erg.almagestdota.drafting.presentation.players_matches

import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.complexadapter.ComplexAdapter
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.drafting.R
import com.erg.almagestdota.drafting.databinding.PlayerMatchesFragmentBinding
import com.erg.almagestdota.drafting.presentation.DraftFragment
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
internal class PlayerMatchesFragment : Fragment(R.layout.player_matches_fragment), BackPressListener {

    companion object {
        const val TAG = "PlayerMatchesFragment"
    }

    private val binding by viewBinding(PlayerMatchesFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    @Inject
    lateinit var factory: PlayerMatchesViewModel.Factory
    private val viewModel by viewModels<PlayerMatchesViewModel> { factory }

    override fun onBackPressed() {
        viewModel.obtainAction(PlayerMatchesViewActions.OnBackClick)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
        viewModel.viewEvent.setResultListener(this)
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).filterNotNull().onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.loadingState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { state ->
            binding.loadingView.isVisible = state is Loading.Enabled
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private inner class ViewsConfigurator {
        private val matchesAdapter by lazy {
            PlayerMatchesAdapter(
                complexListener = object : ComplexListener {
                    override fun onClick(item: ComplexItem) {
                        viewModel.obtainAction(PlayerMatchesViewActions.OpenDraft(item.key))
                    }
                }
            )
        }

        private val smoothScrollerPlayers: RecyclerView.SmoothScroller by lazy { createSmoothScrollListener() }
        private val playersAdapter by lazy {
            ComplexAdapter(
                complexListener = object : ComplexListener {
                    override fun onClick(item: ComplexItem) {
                        viewModel.obtainAction(PlayerMatchesViewActions.SelectPlayer(item.key))
                    }
                }
            )
        }


        private fun createSmoothScrollListener(): RecyclerView.SmoothScroller {
            return object : LinearSmoothScroller(requireContext()) {
                override fun getHorizontalSnapPreference(): Int = SNAP_TO_START
                override fun calculateSpeedPerPixel(displayMetrics: DisplayMetrics): Float =
                    DraftFragment.MILLISECONDS_PER_INCH / displayMetrics.densityDpi
            }
        }

        fun initStartState() = with(binding) {
            ivBack.clicksWithDebounce {
                viewModel.obtainAction(PlayerMatchesViewActions.OnBackClick)
            }
            if (rvPlayers.adapter == null) {
                rvPlayers.adapter = playersAdapter
                rvPlayers.setHasFixedSize(true)
            }

            if (rvMatches.adapter == null) {
                rvMatches.adapter = matchesAdapter
                rvMatches.setHasFixedSize(true)
            }
        }

        fun renderState(state: PlayerMatchesViewState) {
            when (state) {
                is PlayerMatchesViewState.DefaultState -> renderDefaultState(state)
            }
        }

        private fun renderDefaultState(state: PlayerMatchesViewState.DefaultState) = with(binding) {
            tvTitle.text = state.teamName
            playersAdapter.submitList(state.players)
            matchesAdapter.submitList(state.matches as List<ComplexItem.PlayedMatchItem>)
            scrollRvToPosition(state.selectedIndex, smoothScrollerPlayers, rvPlayers.layoutManager!!)
        }

        private fun scrollRvToPosition(
            position: Int,
            smoothScroller: RecyclerView.SmoothScroller,
            layoutManager: RecyclerView.LayoutManager
        ) {
//            smoothScroller.targetPosition = position
//            layoutManager.startSmoothScroll(smoothScroller)
        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
                else -> handleCustomEvents(event)
            }
        }

        private fun handleCustomEvents(event: ViewEvent) {
            when (event) {
                is ViewEvent.PopBackStack<*> -> {
                    navController.popBackStack()
                }
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            viewModel.viewEvent.setResultListener(this@PlayerMatchesFragment)
            navController.navigateViaScreenRoute(screen)
        }
    }
}