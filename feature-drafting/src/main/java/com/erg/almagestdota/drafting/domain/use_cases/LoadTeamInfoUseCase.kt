package com.erg.almagestdota.drafting.domain.use_cases

import android.util.Log
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class LoadTeamInfoUseCase @Inject constructor(
    private val api: FirebaseFirestore,
    private val loadTeamInfoStratzUseCase: LoadTeamInfoStratzUseCase
) {
    suspend operator fun invoke(teamId: Long): TeamInfo {
        val team = try {
            GlobalVars.teams[teamId]!!
        } catch (e: Exception) {
            Log.e("ALMAGESTERROR", "$teamId")
            val teamInfo = loadTeamInfoStratzUseCase.invoke(teamId)
            GlobalVars.teams[teamId] = teamInfo
            teamInfo
        }
        return team
    }
}