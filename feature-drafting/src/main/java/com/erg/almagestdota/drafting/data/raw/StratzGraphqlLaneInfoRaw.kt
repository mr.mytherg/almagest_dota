package com.erg.almagestdota.drafting.data.raw

class MatchInfoResponseRaw(
    val data: MatchInfoDataRaw? = null
)

class MatchInfoDataRaw(
    val matches: List<MatchInfoRaw>? = null
)

class MatchInfoRaw(
    val id: Long? = 0L,
    val topLaneOutcome: String? = "",
    val bottomLaneOutcome: String? = "",
    val midLaneOutcome: String? = "",
)
