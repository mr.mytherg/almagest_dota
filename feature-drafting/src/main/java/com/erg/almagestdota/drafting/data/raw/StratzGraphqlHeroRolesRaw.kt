package com.erg.almagestdota.drafting.data.raw

class HeroRolesResponseRaw(
    val data: HeroRolesDataRaw
)

class HeroRolesDataRaw(
    val heroStats: RolesRaw
)

class RolesRaw(
    val winMonth: List<RoleRaw>
)

class RoleRaw(
    val heroId: Int,
    var winCount: Int,
    var matchCount: Int,
)