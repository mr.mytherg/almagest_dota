package com.erg.almagestdota.drafting.presentation.roles_selection

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem

internal sealed class RolesSelectionViewState : ViewState() {

    class DefaultState(
        val radiantHeroes: List<ComplexItem>,
        val direHeroes: List<ComplexItem>,
        val hasAllHeroesSettled: Boolean
    ) : RolesSelectionViewState()
}