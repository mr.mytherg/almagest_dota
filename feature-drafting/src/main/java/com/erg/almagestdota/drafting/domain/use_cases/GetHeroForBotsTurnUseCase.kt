package com.erg.almagestdota.drafting.domain.use_cases

import com.erg.almagestdota.local_storage.external.models.draft.HeroDraftData
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroPlayerData
import java.util.*
import javax.inject.Inject

internal class GetHeroForBotsTurnUseCase @Inject constructor() {

    companion object {
        private const val THE_BEST_HEROES_COUNT_LIMIT = 5
    }

    private var generator: Random = Random()

    operator fun invoke(
        sortedAndFilteredTheBestHeroes: List<HeroDraftData>,
        isFirstPick: Boolean,
        heroesPlayers: List<HeroPlayerData>
    ): Int {
        if (sortedAndFilteredTheBestHeroes.isEmpty())
            throw IllegalStateException("Изъян в алгоритме")
        return if (isFirstPick) {
            getRandomHero(heroesPlayers)
        } else {
            getSortedRandomHero(sortedAndFilteredTheBestHeroes)
        }
    }

    private fun getRandomHero(heroes: List<HeroPlayerData>): Int {
        val theBestHeroes = heroes
            .sortedByDescending { it.wonCount * 2 + it.wonCountPub }
            .take(10)
            .sortedByDescending {
                (it.wonCount * 1.0 / (it.wonCount + it.lostCount) +
                it.wonCountPub * 1.0 / (it.wonCountPub + it.lostCountPub)) / 2
            }.take(THE_BEST_HEROES_COUNT_LIMIT)
        return theBestHeroes[generator.nextInt(theBestHeroes.size)].id
    }

    private fun getSortedRandomHero(heroes: List<HeroDraftData>): Int {
        return if (heroes.size < THE_BEST_HEROES_COUNT_LIMIT) {
            heroes[generator.nextInt(heroes.size)].id
        } else {
            heroes[generator.nextInt(THE_BEST_HEROES_COUNT_LIMIT)].id
        }
    }
}