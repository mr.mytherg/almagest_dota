package com.erg.almagestdota.drafting.presentation.roles_selection

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.drafting.presentation.DraftFragmentDirections

object RolesSelectionScreen : Screen<NavDirections>(
    route = DraftFragmentDirections.toRolesSelectionFragment(),
    requestKey = RolesSelectionFragment.TAG
)