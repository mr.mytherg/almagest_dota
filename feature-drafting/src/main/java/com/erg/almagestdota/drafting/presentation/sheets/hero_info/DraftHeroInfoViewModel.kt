package com.erg.almagestdota.drafting.presentation.sheets.hero_info

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.complexList
import com.erg.almagestdota.complexadapter.draftReasonItem
import com.erg.almagestdota.drafting.R
import com.erg.almagestdota.drafting.domain.Interactor
import com.erg.almagestdota.drafting.domain.models.HeroInfoModel
import com.erg.almagestdota.drafting.domain.use_cases.GetRecommendationsUseCase.Companion.COUNTER_MIN_COEF
import com.erg.almagestdota.drafting.domain.use_cases.GetRecommendationsUseCase.Companion.MIN_COEF
import com.erg.almagestdota.drafting.presentation.sheets.info.DraftInfoScreen
import com.erg.almagestdota.local_storage.external.models.draft.*
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.DraftFilterGroupType
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.main_settings.getCounter
import com.erg.almagestdota.local_storage.external.models.main_settings.getSynergy
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

internal class DraftHeroInfoViewModel @AssistedInject constructor(
    @Assisted private val heroId: Int,
    private val interactor: Interactor,
    private val stringProvider: StringProvider,
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data -> }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    fun obtainAction(action: DraftHeroInfoViewAction) {
        when (action) {
            is DraftHeroInfoViewAction.ConfirmHero -> {
                emit(viewEventMutable, ViewEvent.PopBackStack.Data(heroId))
            }
        }
    }

    private inner class StateConfigurator {

        fun defineFragmentState(): DraftHeroInfoViewState {
            val reasons = interactor.getReasons()

            return DraftHeroInfoViewState.DefaultState(
                hero = heroId.getHeroById(),
                list = complexList {
                    for (reason in reasons) {
                        draftReasonItem {
                            name = stringProvider.getString(DraftReasonType.getStringId(reason))
                        }
                    }
                }
            )
        }
    }

    companion object {
        fun provideFactory(
            heroId: Int,
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create(heroId) as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(
            heroId: Int,
        ): DraftHeroInfoViewModel
    }
}