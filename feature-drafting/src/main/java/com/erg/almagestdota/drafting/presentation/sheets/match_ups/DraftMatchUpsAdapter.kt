package com.erg.almagestdota.drafting.presentation.sheets.match_ups

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.erg.almagestdota.heroselector.presentation.heroSelector.adapter.SelectedHeroesAdapter
import com.erg.almagestdota.drafting.databinding.ItemMatchUpBinding

internal class DraftMatchUpsAdapter() : ListAdapter<DraftMatchUp,
    DraftMatchUpsAdapter.DraftMatchUpViewHolder>(diffCallback) {

    companion object {

        private val diffCallback = object : DiffUtil.ItemCallback<DraftMatchUp>() {
            override fun areItemsTheSame(oldItem: DraftMatchUp, newItem: DraftMatchUp): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: DraftMatchUp, newItem: DraftMatchUp): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DraftMatchUpViewHolder {
        val binding = ItemMatchUpBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return DraftMatchUpViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DraftMatchUpViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class DraftMatchUpViewHolder(
        private val binding: ItemMatchUpBinding,
    ) : RecyclerView.ViewHolder(binding.root) {
        private val context = binding.root.context
        private val adapter = SelectedHeroesAdapter(false)

        fun bind(matchUp: DraftMatchUp) = with(binding) {
            if (rvMatchUp.adapter == null) {
                rvMatchUp.adapter = adapter
                rvMatchUp.setHasFixedSize(true)
                rvMatchUp.itemAnimator = null
            }

            adapter.submitList(matchUp.heroes)
        }
    }
}