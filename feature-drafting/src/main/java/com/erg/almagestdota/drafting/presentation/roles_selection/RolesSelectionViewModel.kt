package com.erg.almagestdota.drafting.presentation.roles_selection

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.*
import com.erg.almagestdota.drafting.domain.Interactor
import com.erg.almagestdota.drafting.domain.use_cases.FilterPlayersByHeroesUseCase
import com.erg.almagestdota.drafting.domain.use_cases.GameDataUseCase
import com.erg.almagestdota.drafting.presentation.game_data.GameDataScreen
import com.erg.almagestdota.local_storage.external.models.draft.*
import com.erg.almagestdota.local_storage.external.models.draft.main.*
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import com.erg.almagestdota.predictor.domain.use_cases.FilterHeroesByRolesUseCase
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

internal class RolesSelectionViewModel @Inject constructor(
    private val interactor: Interactor,
    private val stringProvider: StringProvider,
    private val filterPlayersByHeroesUseCase: FilterPlayersByHeroesUseCase,
    private val filterHeroesByRolesUseCase: FilterHeroesByRolesUseCase,
    private val gameDataUseCase: GameDataUseCase,
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data -> }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    fun obtainAction(action: RolesSelectionViewActions) {
        when (action) {
            is RolesSelectionViewActions.OnBackClick -> sendBackClickEvent()
            is RolesSelectionViewActions.ShowData -> {
                gameDataUseCase.invoke(
                    radiant = stateConfigurator.radiantHeroesRoles.map {
                        val hero = it.key.getHeroById()
                        HeroRoleData(
                            hero.id,
                            hero.name,
                            hero.imageLink,
                            setOf(it.value)
                        )
                    },
                    dire = stateConfigurator.direHeroesRoles.map {
                        val hero = it.key.getHeroById()
                        HeroRoleData(
                            hero.id,
                            hero.name,
                            hero.imageLink,
                            setOf(it.value)
                        )
                    }
                )
                emit(viewEventMutable, ViewEvent.Navigation(GameDataScreen))
            }
            is RolesSelectionViewActions.SelectHeroRoleRadiant -> {
                prepareRole(action.key, stateConfigurator.radiantHeroesRoles)
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is RolesSelectionViewActions.SelectHeroRoleDire -> {
                prepareRole(action.key, stateConfigurator.direHeroesRoles)
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
        }
    }

    private fun prepareRole(key: String, heroesRoles: MutableMap<Int, RoleType>) {
        val heroId = key.substring(0, key.indexOf(" ")).toInt()
        val role = RoleType.byName(key.substring(key.indexOf(" ") + 1))
        setRoles(heroesRoles, heroId, role)
    }

    private fun setRoles(heroesRoles: MutableMap<Int, RoleType>, heroId: Int, role: RoleType) {
        heroesRoles.forEach { (heroId, roleType) ->
            if (roleType == role)
                heroesRoles[heroId] = RoleType.UNKNOWN
        }
        heroesRoles[heroId] = role
    }

    private fun sendBackClickEvent() {
        emit(viewEventMutable, ViewEvent.PopBackStack.Empty)
    }

    private inner class StateConfigurator {
        private val draftModel = interactor.getDraftModel()
        private val rolesList = listOf<RoleType>(
            RoleType.ROLE_1,
            RoleType.ROLE_2,
            RoleType.ROLE_3,
            RoleType.ROLE_4,
            RoleType.ROLE_5,
        )
        val radiantHeroesRoles = mutableMapOf<Int, RoleType>()
        val direHeroesRoles = mutableMapOf<Int, RoleType>()

        fun defineFragmentState(): RolesSelectionViewState {
            if (radiantHeroesRoles.isEmpty())
                defineHeroesRoles(draftModel.radiantTeam, radiantHeroesRoles)
            if (direHeroesRoles.isEmpty())
                defineHeroesRoles(draftModel.direTeam, direHeroesRoles)

            val radiantComplex = getHeroesComplex(radiantHeroesRoles)
            val direComplex = getHeroesComplex(direHeroesRoles)

            return RolesSelectionViewState.DefaultState(
                radiantComplex,
                direComplex,
                radiantHeroesRoles.all { it.value != RoleType.UNKNOWN } && direHeroesRoles.all { it.value != RoleType.UNKNOWN }
            )
        }

        private fun getHeroesComplex(radiantHeroesRoles: MutableMap<Int, RoleType>): List<ComplexItem> {
            return complexList {
                for ((heroId, roleType) in radiantHeroesRoles) {
                    val hero = heroId.getHeroById()
                    roleSelectionItem(key = hero.id.toString()) {
                        name = hero.name
                        url = hero.imageLink
                        roles = complexList {
                            for (role in rolesList) {
                                filterItem(key = "${hero.id} ${role.name}") {
                                    title = stringProvider.getString(RoleType.getId(role))
                                    isSelected = radiantHeroesRoles[hero.id] == role
                                }
                            }
                        }
                    }
                }
            }
        }

        private fun defineHeroesRoles(teamDraft: TeamDraft, heroesRolesMap: MutableMap<Int, RoleType>) {
            if (!draftModel.gameMode.hasTeams()) {
                val heroesRoles = filterHeroesByRolesUseCase.getHeroesRoles(teamDraft.picks.map { it.heroId })
                for (heroRole in heroesRoles) {
                    val list = heroRole.heroRoles.orEmpty()
                    if (list.size == 1) {
                        setRoles(heroesRolesMap, heroRole.id, list.first())
                    } else {
                        heroesRolesMap[heroRole.id] = RoleType.UNKNOWN
                    }
                }
            } else if (teamDraft.picks.all { it.playerId != 0L }) {
                for (heroPlayer in teamDraft.picks) {
                    val playerInfo = GlobalVars.players[heroPlayer.playerId]
                    if (playerInfo != null) {
                        setRoles(heroesRolesMap, heroPlayer.heroId, playerInfo.role)
                    } else {
                        heroesRolesMap[heroPlayer.heroId] = RoleType.UNKNOWN
                    }
                }
            } else {
                val heroesPlayers = filterPlayersByHeroesUseCase.getHeroesPlayers(teamDraft)
                for (heroPlayer in heroesPlayers) {
                    val list = heroPlayer.heroPlayers.orEmpty()
                    if (list.size == 1) {
                        val playerInfo = GlobalVars.players[list.first().first]
                        if (playerInfo != null) {
                            setRoles(heroesRolesMap, heroPlayer.id, playerInfo.role)
                        } else {
                            heroesRolesMap[heroPlayer.id] = RoleType.UNKNOWN
                        }
                    } else {
                        heroesRolesMap[heroPlayer.id] = RoleType.UNKNOWN
                    }
                }
            }
        }
    }

    class Factory @Inject constructor(
        private val interactor: Interactor,
        private val stringProvider: StringProvider,
        private val filterPlayersByHeroesUseCase: FilterPlayersByHeroesUseCase,
        private val filterHeroesByRolesUseCase: FilterHeroesByRolesUseCase,
        private val gameDataUseCase: GameDataUseCase,
    ) : ViewModelProvider.Factory {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>): T =
            RolesSelectionViewModel(interactor, stringProvider, filterPlayersByHeroesUseCase, filterHeroesByRolesUseCase, gameDataUseCase) as T
    }
}