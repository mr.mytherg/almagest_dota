package com.erg.almagestdota.drafting.presentation.dialogs

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.drafting.presentation.DraftFragmentDirections

internal object ExitConfirmScreen : Screen<NavDirections>(
    route = DraftFragmentDirections.toExitConfirmDialog(),
    requestKey = ExitConfirmDialog.TAG
)