package com.erg.almagestdota.drafting.presentation.game_data

internal sealed class GameDataViewActions {

    object OnBackClick : GameDataViewActions()
    class SelectGameStage(val key: String) : GameDataViewActions()
}