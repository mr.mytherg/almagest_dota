package com.erg.almagestdota.drafting.presentation.roles_selection

internal sealed class RolesSelectionViewActions {

    object OnBackClick : RolesSelectionViewActions()
    object ShowData : RolesSelectionViewActions()
    class SelectHeroRoleRadiant(val key: String) : RolesSelectionViewActions()
    class SelectHeroRoleDire(val key: String) : RolesSelectionViewActions()
}