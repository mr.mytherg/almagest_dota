package com.erg.almagestdota.drafting.domain.use_cases

import android.provider.Settings.Global
import android.util.Log
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.drafting.data.IStratzGraphqlApi
import com.erg.almagestdota.drafting.data.OpenDotaApi
import com.erg.almagestdota.local_storage.external.models.draft.main.MemberInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class LoadPlayerInfoUseCase @Inject constructor(
    private val api: FirebaseFirestore,
    private val openDotaApi: OpenDotaApi,
    private val stratzApi: IStratzGraphqlApi,
) {
    suspend operator fun invoke(playerId: Long): MemberInfo {
        val member = GlobalVars.players[playerId]
        if (member != null)
            return member
        else {
            val player = openDotaApi.loadPlayer(playerId)
            val memberInfo = MemberInfo(
                playerId,
                name = player.profile?.name.orDefault()
            )
            if (memberInfo.name.isEmpty()) {
                val query =
                    "{players(steamAccountIds:[$playerId]){steamAccount{proSteamAccount{name}}}}"
                val playerInfo = stratzApi.getPlayers(query).data?.players.orEmpty().firstOrNull()
                memberInfo.name = playerInfo?.steamAccount?.proSteamAccount?.name.orDefault()
            }
            return memberInfo
        }
    }
}