package com.erg.almagestdota.drafting.data.raw

class HeroCoefficientSynergyResponseRaw(
    val data: HeroCoefficientSynergyDataRaw
)

class HeroCoefficientSynergyDataRaw(
    val heroStats: HeroCoefficientSynergyHeroStatsRaw
)

class HeroCoefficientSynergyHeroStatsRaw(
    val matchUp: List<HeroCoefficientSynergyMatchUpRaw>
)

class HeroCoefficientSynergyMatchUpRaw(
    val with: List<HeroCoefficientSynergyRaw>
)

class HeroCoefficientSynergyRaw(
    val heroId2: Int,
    val synergy: Double
)