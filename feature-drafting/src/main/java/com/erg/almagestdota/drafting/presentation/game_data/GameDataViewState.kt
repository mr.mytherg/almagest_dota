package com.erg.almagestdota.drafting.presentation.game_data

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem

internal sealed class GameDataViewState : ViewState() {

    class DefaultState(
        val gameStagesFilters: List<ComplexItem>,
        val gameStagesData: List<ComplexItem>,
    ) : GameDataViewState()
}