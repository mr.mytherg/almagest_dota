package com.erg.almagestdota.drafting.domain.use_cases

import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.local_storage.external.models.draft.main.MemberInfo
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerInfo
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerStats
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamDraft
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroPlayerData
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import javax.inject.Inject

internal class FilterPlayersByHeroesUseCase @Inject constructor() {

    fun getMatchUpList(teamDraft: TeamDraft): Set<List<HeroPlayerData>> {
        val matchUpList = getMatchUps(sortHeroesByRoleCount(teamDraft), teamDraft)
        return matchUpList.map {
            it.map { role ->
                if (role.hero == null) {
                    HeroPlayerData.createNullHero()
                } else {
                    val hero = role.hero!!.getHeroById()
                    HeroPlayerData(
                        id = hero.id,
                        name = hero.name,
                        imageLink = hero.imageLink,
                        heroPlayers = setOf(getPlayersPairs(role.playerId)),
                        wonCount = teamDraft.heroPool[hero.id]?.get(role.playerId)?.wonCount.orDefault(),
                        lostCount = teamDraft.heroPool[hero.id]?.get(role.playerId)?.lostCount.orDefault(),
                        wonCountPub = teamDraft.heroPool[hero.id]?.get(role.playerId)?.wonCountPub.orDefault(),
                        lostCountPub = teamDraft.heroPool[hero.id]?.get(role.playerId)?.lostCountPub.orDefault(),
                    )
                }
            }
        }.toSet()
    }

    private fun getPlayersPairs(playerId: Long): Pair<Long, String> {
        return playerId to (GlobalVars.players[playerId] ?: MemberInfo(playerId)).name
    }

    private fun mapHeroesPlayers(team: TeamDraft, heroes: List<PlayerInfo>): List<HeroPlayerData> {
        return heroes.map { heroPlayer ->
            val heroPlayers = mutableSetOf<Pair<Long, String>>()
            val playerStats = team.heroPool[heroPlayer.heroId] ?: mapOf()
            var wonCount = 0
            var lostCount = 0
            var wonCountPub = 0
            var lostCountPub = 0
            for ((id, player) in playerStats) {
                heroPlayers.add(player.playerId to player.playerName)
                wonCount += player.wonCount
                wonCountPub += player.wonCountPub
                lostCount += player.lostCount
                lostCountPub += player.lostCountPub
            }
            val hero = heroPlayer.heroId.getHeroById()
            HeroPlayerData(
                id = hero.id,
                imageLink = hero.imageLink,
                name = hero.name,
                heroPlayers = heroPlayers,
                wonCount = wonCount,
                lostCount = lostCount,
                wonCountPub = wonCountPub,
                lostCountPub = lostCountPub,
            )
        }
    }

    fun getHeroesPlayers(teamDraft: TeamDraft): List<HeroPlayerData> {

        val matchUpList = getMatchUps(sortHeroesByRoleCount(teamDraft), teamDraft)
        val heroesPlayers = isDraftRolesValid(matchUpList)
        val heroRoleDataList = mutableListOf<HeroPlayerData>()
        for (player in teamDraft.picks) {
            val hero = player.heroId.getHeroById()
            val possiblePlayers = heroesPlayers?.find { it.hero == hero.id }?.possiblePlayers
            heroRoleDataList.add(
                HeroPlayerData(
                    id = hero.id,
                    name = hero.name,
                    imageLink = hero.imageLink,
                    heroPlayers = possiblePlayers.orEmpty().map { getPlayersPairs(it) }
                        .toSet(),
                    wonCount = teamDraft.heroPool[hero.id]?.values?.fold(0) { acc, next ->
                        acc + if (possiblePlayers?.find { next.playerId == it } != null) next.wonCount else 0
                    }.orDefault(),
                    lostCount = teamDraft.heroPool[hero.id]?.values?.fold(0) { acc, next ->
                        acc + if (possiblePlayers?.find { next.playerId == it } != null) next.lostCount else 0
                    }.orDefault(),
                    wonCountPub = teamDraft.heroPool[hero.id]?.values?.fold(0) { acc, next ->
                        acc + if (possiblePlayers?.find { next.playerId == it } != null) next.wonCountPub else 0
                    }.orDefault(),
                    lostCountPub = teamDraft.heroPool[hero.id]?.values?.fold(0) { acc, next ->
                        acc + if (possiblePlayers?.find { next.playerId == it } != null) next.lostCountPub else 0
                    }.orDefault(),
                )
            )
        }

        val heroes = if (heroRoleDataList.all { it.heroPlayers.isNullOrEmpty() }) {
            mapHeroesPlayers(teamDraft, teamDraft.picks)
        } else {
            heroRoleDataList
        }
        return heroes
    }

    operator fun <T : HeroCoreData> invoke(heroes: List<T>, teamDraft: TeamDraft): List<T> {
        val matchUpList = getMatchUps(sortHeroesByRoleCount(teamDraft), teamDraft)
        if (matchUpList.isEmpty()) return heroes
        val filtered = heroes.filter {
            val newTeamDraft = teamDraft.copy(
                picks = teamDraft.picks.plus(PlayerInfo(it.id, 0L)).toMutableList()
            ).apply {
                heroPool = teamDraft.heroPool
            }
            val heroPossibleRoles = sortHeroesByRoleCount(newTeamDraft)
            getMatchUps(heroPossibleRoles, teamDraft).isNotEmpty()
        }
        return filtered
    }

    private fun isDraftRolesValid(matchUpList: Set<List<HeroPlayer>>): List<HeroPossiblePlayers>? {
        return if (matchUpList.isNotEmpty()) {
            val heroPossibleRolesChanged = mutableListOf<HeroPossiblePlayers>()
            for (matchUp in matchUpList) {
                for (player in matchUp) {
                    if (player.hero == null) continue
                    var hero = heroPossibleRolesChanged.find { it.hero == player.hero }
                    if (hero == null) {
                        hero = HeroPossiblePlayers(hero = player.hero!!, possiblePlayers = setOf(player.playerId))
                        heroPossibleRolesChanged.add(hero)
                    } else {
                        hero.possiblePlayers = hero.possiblePlayers.plus(player.playerId)
                    }
                }
            }
            heroPossibleRolesChanged
        } else {
            null
        }
    }

    private fun sortHeroesByRoleCount(teamDraft: TeamDraft): List<HeroPossiblePlayers> {
        val mutableList = mutableListOf<HeroPossiblePlayers>()
        teamDraft.picks.forEachIndexed { index, player ->
            var heroPlayers: Map<Long, PlayerStats> = teamDraft.heroPool[player.heroId] ?: mapOf()
            if (heroPlayers.isEmpty()) {
                val allPlayersMap = mutableMapOf<Long, PlayerStats>()
                for (pick in teamDraft.picks)
                    allPlayersMap[pick.playerId] = PlayerStats.createEmptyPlayer(pick.playerId, teamDraft.teamInfo.members)
                heroPlayers = allPlayersMap
            }
            val players = mutableSetOf<Long>()
            for ((playerId, playerStats) in heroPlayers) {
                val currentPlayer = heroPlayers[playerId]
                if (currentPlayer != null && (currentPlayer.wonCount > 0 || currentPlayer.wonCountPub + currentPlayer.lostCountPub >= 10)) {
                    players.add(playerStats.playerId)
                } else {
                    continue
                }
            }
            mutableList.add(HeroPossiblePlayers(player.heroId, players))
        }
        return mutableList.sortedBy { it.possiblePlayers.size }
    }

    private fun getMatchUps(leastHeroes: List<HeroPossiblePlayers>, teamDraft: TeamDraft): Set<List<HeroPlayer>> {
        val matchUp = listOf(
            HeroPlayer(null, teamDraft.teamInfo.members[0]),
            HeroPlayer(null, teamDraft.teamInfo.members[1]),
            HeroPlayer(null, teamDraft.teamInfo.members[2]),
            HeroPlayer(null, teamDraft.teamInfo.members[3]),
            HeroPlayer(null, teamDraft.teamInfo.members[4]),
        )
        val matchUpList = mutableSetOf<List<HeroPlayer>>()
        addAllMatchUpsRecursively(leastHeroes, matchUp, matchUpList)
        return matchUpList
    }

    private fun addAllMatchUpsRecursively(
        leastHeroes: List<HeroPossiblePlayers>,
        _matchUp: List<HeroPlayer>,
        matchUpList: MutableSet<List<HeroPlayer>>
    ) {
        for (hero in leastHeroes) {
            for (playerName in hero.possiblePlayers) {
                val matchUp: List<HeroPlayer> = listOf(
                    HeroPlayer(playerId = _matchUp[0].playerId, hero = _matchUp[0].hero),
                    HeroPlayer(playerId = _matchUp[1].playerId, hero = _matchUp[1].hero),
                    HeroPlayer(playerId = _matchUp[2].playerId, hero = _matchUp[2].hero),
                    HeroPlayer(playerId = _matchUp[3].playerId, hero = _matchUp[3].hero),
                    HeroPlayer(playerId = _matchUp[4].playerId, hero = _matchUp[4].hero),
                )
                val isSuccess = tryToFillTeamRoles(matchUp, HeroPlayer(hero.hero, playerName))
                if (isSuccess) {
                    addAllMatchUpsRecursively(leastHeroes.minus(hero), matchUp, matchUpList)
                }
            }
        }
        if (leastHeroes.isEmpty()) {
            matchUpList.add(_matchUp)
        }
    }

    private fun tryToFillTeamRoles(
        teamRoles: List<HeroPlayer>,
        role: HeroPlayer
    ): Boolean {
        teamRoles.find { it.playerId == role.playerId }?.apply {
            if (this.hero != null) {
                return false
            }
            this.hero = role.hero
        }
        return true
    }
}

data class HeroPossiblePlayers(
    val hero: Int,
    var possiblePlayers: Set<Long>
)

data class HeroPlayer(
    var hero: Int? = null,
    var playerId: Long
)