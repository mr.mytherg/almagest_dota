package com.erg.almagestdota.drafting.presentation.draft_analysis

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.drafting.presentation.DraftFragmentDirections

class DraftAnalysisScreen() : Screen<NavDirections>(
    route = DraftFragmentDirections.toDraftAnalysisFragment(),
    requestKey = DraftAnalysisFragment.TAG
)