package com.erg.almagestdota.drafting.presentation.sheets.info

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.drafting.presentation.DraftFragmentDirections

class DraftInfoScreen() : Screen<NavDirections>(
    route = DraftFragmentDirections.toDraftInfoSheet(),
    requestKey = DraftInfoSheet.TAG
)