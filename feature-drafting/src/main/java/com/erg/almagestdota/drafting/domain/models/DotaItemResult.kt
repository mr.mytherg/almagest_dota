package com.erg.almagestdota.drafting.domain.models

import android.os.Parcelable
import com.erg.almagestdota.local_storage.external.models.draft.DotaItem
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DotaItemResult(
    @SerializedName("DotaItemResult_heroId") val heroId: Int,
    @SerializedName("DotaItemResult_id") override var id: Int,
    @SerializedName("DotaItemResult_name") override val name: String,
    @SerializedName("DotaItemResult_imageLink") override val imageLink: String,
    @SerializedName("DotaItemResult_neutralTier") override val neutralTier: Int,
    @SerializedName("DotaItemResult_early") override val early: Boolean,
    @SerializedName("DotaItemResult_team") override val team: Boolean,
    @SerializedName("DotaItemResult_analogs") override var analogs: List<Int>,
    @SerializedName("DotaItemResult_countersHeroes") val countersHeroes: List<Int>
) : DotaItem(id, name, imageLink, neutralTier, early, team, analogs), Parcelable