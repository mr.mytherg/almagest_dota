package com.erg.almagestdota.drafting.presentation

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.complexadapter.viewHolders.matches.DraftHistoryViewHolder
import com.erg.almagestdota.complexadapter.viewHolders.other.DraftStrategyViewHolder

class DraftStrategyAdapter(
    private val complexListener: ComplexListener? = null,
) : ListAdapter<ComplexItem.DraftStrategyItem, DraftStrategyViewHolder>(diffCallback) {

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<ComplexItem.DraftStrategyItem>() {
            override fun areItemsTheSame(
                oldItem: ComplexItem.DraftStrategyItem,
                newItem: ComplexItem.DraftStrategyItem
            ): Boolean {
                return oldItem.key == newItem.key
            }

            override fun areContentsTheSame(
                oldItem: ComplexItem.DraftStrategyItem,
                newItem: ComplexItem.DraftStrategyItem
            ): Boolean {
                return oldItem == newItem
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DraftStrategyViewHolder {
        return DraftStrategyViewHolder.create(parent, complexListener)
    }

    override fun onBindViewHolder(holder: DraftStrategyViewHolder, position: Int) {
        holder.bind(currentList[position])
    }
}