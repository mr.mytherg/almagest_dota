package com.erg.almagestdota.drafting.data.raw

class HeroStatsResponseRaw(
    val data: HeroStatsDataRaw
)

class HeroStatsDataRaw(
    val heroStats: StatsForFiltersRaw
)

class StatsForFiltersRaw(
    val stats: List<StatsEventRaw>
)

class StatsEventRaw(
    val heroId: Int,
    val physicalDamage: Double,
    val pureDamage: Double,
    val magicalDamage: Double,
    val towerDamage: Double,
    val healingAllies: Double,
    val healingItemAllies: Double,
)