package com.erg.almagestdota.drafting.domain.models

sealed class ItemListType(open val item: DotaItemResult) {
    class ItemAgainstEnemy(override val item: DotaItemResult) : ItemListType(item)
    class ItemForEnemy(override val item: DotaItemResult) : ItemListType(item)
}