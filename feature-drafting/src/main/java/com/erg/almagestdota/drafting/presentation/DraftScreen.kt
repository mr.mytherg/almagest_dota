package com.erg.almagestdota.drafting.presentation

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen

class DraftScreen(
    route: NavDirections
) : Screen<NavDirections>(
    route = route,
    requestKey = DraftFragment.TAG
)