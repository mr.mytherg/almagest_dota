package com.erg.almagestdota.drafting.domain.models

import com.erg.almagestdota.local_storage.external.models.draft.HeroValues
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamDraft
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData

class HeroInfoModel(
    val heroesToSelect: List<HeroCoreData>,
    var hero: HeroCoreData,
    val isUserHero: Boolean,
    val radiantTeam: TeamDraft,
    val direTeam: TeamDraft,
    val radiantValues: Map<Int, HeroValues>,
    val direValues: Map<Int, HeroValues>,
)