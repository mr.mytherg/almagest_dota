package com.erg.almagestdota.drafting.presentation.sheets.hero_info

import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import com.erg.almagestdota.base.external.image.loadImageByUrl
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.ComplexAdapter
import com.erg.almagestdota.heroselector.presentation.heroSelector.adapter.HeroesToSelectWrapAdapter
import com.erg.almagestdota.drafting.R
import com.erg.almagestdota.drafting.databinding.DraftHeroInfoSheetBinding
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdpta.core_navigation.external.helpers.BottomSheetFragment
import com.erg.almagestdpta.core_navigation.external.helpers.navHostFragmentManager
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
internal class DraftHeroInfoSheet : BottomSheetFragment(R.layout.draft_hero_info_sheet), BackPressListener {

    private val args by navArgs<DraftHeroInfoSheetArgs>()

    companion object {
        const val TAG = "DraftHeroInfoSheet"
    }

    private val binding by viewBinding { DraftHeroInfoSheetBinding.bind(requireContentView()) }
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    @Inject
    lateinit var viewModelFactory: DraftHeroInfoViewModel.Factory
    private val viewModel by viewModels<DraftHeroInfoViewModel> {
        DraftHeroInfoViewModel.provideFactory(
            heroId = args.heroId,
            assistedFactory = viewModelFactory
        )
    }

    override fun calculateScreenHeight() = ViewGroup.LayoutParams.WRAP_CONTENT
    override fun calculateScreenOffset() = 1.0f


    override fun onBackPressed() {
        onClose()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
        viewModel.viewEvent.setResultListener(this)
    }

    override fun onBottomSheetStateChanged(bottomSheet: View, newState: Int) {
        super.onBottomSheetStateChanged(bottomSheet, newState)
        if (newState == BottomSheetBehavior.STATE_HIDDEN) {
            onClose()
        }
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun onClose(heroId: Int? = null) {
        val fm = navHostFragmentManager()
        navController.popBackStack()
        if (heroId == null) {
            fm.setFragmentResult(TAG, bundleOf())
        } else {
            fm.setFragmentResult(TAG, DraftHeroInfoScreen.createBundleForResult(heroId))
        }
    }

    private inner class ViewsConfigurator {
        private val adapter = ComplexAdapter()
        fun initStartState() = with(binding) {
            btnConfirm.clicksWithDebounce {
                viewModel.obtainAction(DraftHeroInfoViewAction.ConfirmHero)
            }

        }

        fun renderState(state: DraftHeroInfoViewState) {
            when (state) {
                is DraftHeroInfoViewState.DefaultState -> renderDefaultState(state)
            }
        }

        private fun renderDefaultState(state: DraftHeroInfoViewState.DefaultState) = with(binding) {
            ivItem.loadImageByUrl(state.hero.imageLink)
            rvReasons.adapter = adapter
            adapter.submitList(state.list)
        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
                is ViewEvent.PopBackStack.Data<*> -> onClose(event.data as? Int)
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            viewModel.viewEvent.setResultListener(this@DraftHeroInfoSheet)
            navController.navigateViaScreenRoute(screen)
        }
    }
}