package com.erg.almagestdota.drafting.domain.use_cases

import com.erg.almagestdota.local_storage.external.models.draft.HeroDraftData
import com.erg.almagestdota.local_storage.external.models.draft.HeroValues
import com.erg.almagestdota.local_storage.external.models.draft.hasTeams
import com.erg.almagestdota.local_storage.external.models.draft.main.DraftModel
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamDraft
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroPlayerData
import com.erg.almagestdota.local_storage.external.models.main_settings.getCounter
import com.erg.almagestdota.local_storage.external.models.main_settings.getSynergy
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import com.erg.almagestdota.predictor.domain.use_cases.FilterHeroesByRolesUseCase
import javax.inject.Inject

internal class SortHeroesByPickerAlgorithmUseCase @Inject constructor(
    private val filterHeroesByRolesUseCase: FilterHeroesByRolesUseCase,
    private val calculateStealValueUseCase: CalculateStealValueUseCase,
) {

    suspend operator fun invoke(
        draftModel: DraftModel,
        heroes: List<Int>,
        allies: List<Int>,
        enemies: List<Int>,
        heroesValues: Map<Int, HeroValues>,
        allieTeam: TeamDraft,
        enemyTeam: TeamDraft,
    ): List<HeroDraftData> {
        val heroDraftData = mutableListOf<HeroDraftData>()

        if (allies.isEmpty() && enemies.isEmpty() ) {
            if (draftModel.gameMode.hasTeams()) {
                return heroes.map { heroId ->
                    val hero = heroId.getHeroById()
                    val heroPlayers = mutableSetOf<Pair<Long, String>>()
                    val playerStats = allieTeam.heroPool[hero.id] ?: mapOf()
                    var wonCount = 0
                    var lostCount = 0
                    var wonCountPub = 0
                    var lostCountPub = 0
                    for ((id, player) in playerStats) {
                        heroPlayers.add(player.playerId to player.playerName)
                        wonCount += player.wonCount
                        wonCountPub += player.wonCountPub
                        lostCount += player.lostCount
                        lostCountPub += player.lostCountPub
                    }
                    HeroPlayerData(
                        id = hero.id,
                        imageLink = hero.imageLink,
                        name = hero.name,
                        heroPlayers = heroPlayers,
                        wonCount = wonCount,
                        lostCount = lostCount,
                        wonCountPub = wonCountPub,
                        lostCountPub = lostCountPub
                    )
                }.sortedByDescending { it.wonCount + it.wonCountPub + it.lostCount + it.lostCountPub }.map {
                    HeroDraftData(
                        id = it.id,
                        name = it.name,
                        imageLink = it.imageLink,
                        synergyValue = 0.0,
                        counterValue = 0.0,
                        stealValue = 0.0,
                        avgCounter = 0.0
                    )
                }
            } else {
                return heroes.sortedByDescending { GlobalVars.heroesAsMap[it]!!.factions[0].pickRate }.map {
                    val hero = it.getHeroById()
                    HeroDraftData(
                        id = hero.id,
                        name = hero.name,
                        imageLink = hero.imageLink,
                    )
                }
            }
        }


        for (heroId in heroes) {
            val synergyValue = heroId.getSynergy(allies, heroesValues)
            val counterValue = heroId.getCounter(enemies, heroesValues)

            val hero = heroId.getHeroById()
            heroDraftData.add(
                HeroDraftData(
                    id = hero.id,
                    name = hero.name,
                    imageLink = hero.imageLink,
                    synergyValue = synergyValue,
                    counterValue = counterValue,
                    stealValue = calculateStealValueUseCase.invoke(
                        hero.id,
                        allies,
                        enemies,
                        alliesValues = heroesValues,
                        enemiesValues = heroesValues,
                    ),
                    avgCounter = 0.0 // avgCounter.round2()
                )
            )
        }
        return heroDraftData.sortedWith(
            compareByDescending<HeroDraftData> { it.getSum() }.thenBy {
                it.name
            }
        )
    }


}