package com.erg.almagestdota.drafting.domain

import com.erg.almagestdota.drafting.domain.models.DotaItemList
import com.erg.almagestdota.local_storage.external.models.DraftAnalysis
import com.erg.almagestdota.drafting.domain.models.HeroDraftType
import com.erg.almagestdota.local_storage.external.models.draft.DraftReasonType
import com.erg.almagestdota.local_storage.external.models.draft.HeroDraftData
import com.erg.almagestdota.local_storage.external.models.draft.main.DraftModel
import com.erg.almagestdota.local_storage.external.models.main_settings.*

interface IDraftInteractor {
    fun skipBans()

    suspend fun selectHero(heroId: Int, needToSaveDraft: Boolean = true)
    fun getDraftModel(): DraftModel
    suspend fun configureDraftModel()
    suspend fun cancelLastSelection()
    suspend fun selectFilter(filterId: Int)
    suspend fun selectType(typeId: Int)
    suspend fun selectGroup(groupType: DraftFilterGroupType)
    fun setDraftModel(draftModel: DraftModel)
    suspend fun getAnalysis(): List<DraftAnalysis>
    fun hasAnalysis(): Boolean

    fun getGroups(): List<DraftFilterGroup>
    fun getFilters(): List<DraftFilter>
    fun getTypes(): List<HeroDraftType>
    fun getGroupId(): Int
    fun getTypeId(): Int
    fun getFilterId(): Int
    fun getDraftStage(): Int
    fun isCurrentStage(stage: Int): Boolean
    fun isPickStage(stage: Int): Boolean
    fun isBanStage(): Boolean
    fun isRadiantStage(stage: Int): Boolean
    fun hasDraftDone(): Boolean
    fun hasDraftStarted(): Boolean

    fun getResultItems(): List<DotaItemList>
    fun selectResultHero(heroId: Int)
    fun getSelectedResultHero(): HeroCoreData
    fun isCancelChoiceAvailable(): Boolean
    fun getFilterDescription(): String

    fun getHeroesDraftReasons(): List<HeroDraftData>
    fun getHeroesToSelect(): List<HeroCoreData>
    fun getTeamHeroes(isRadiant: Boolean): List<HeroCoreData>
    suspend fun revertSequence()
    fun isRevertSequenceAvailable(): Boolean
    fun needToShowSearchSheet(): Boolean
    fun isPubMatchesLoadingFailed(): Boolean
    fun isValid(heroId: Int): Boolean
    fun getHeroReasons(heroId: Int): List<DraftReasonType>
}