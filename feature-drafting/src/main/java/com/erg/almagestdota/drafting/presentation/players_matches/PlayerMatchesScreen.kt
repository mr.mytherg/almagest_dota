package com.erg.almagestdota.drafting.presentation.players_matches

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen

class PlayerMatchesScreen(
    route: NavDirections
) : Screen<NavDirections>(
    route = route,
    requestKey = PlayerMatchesFragment.TAG
)