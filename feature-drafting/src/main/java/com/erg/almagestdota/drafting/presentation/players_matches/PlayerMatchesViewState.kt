package com.erg.almagestdota.drafting.presentation.players_matches

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem

internal sealed class PlayerMatchesViewState : ViewState() {

    class DefaultState(
        val matches: List<ComplexItem>,
        val players: List<ComplexItem>,
        val selectedIndex: Int,
        val teamName: String
    ) : PlayerMatchesViewState()
}