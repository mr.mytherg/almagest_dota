package com.erg.almagestdota.drafting.presentation.draft_analysis

import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.complexadapter.ComplexAdapter
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.drafting.R
import com.erg.almagestdota.drafting.databinding.DraftAnalysisFragmentBinding
import com.erg.almagestdota.drafting.databinding.PlayerMatchesFragmentBinding
import com.erg.almagestdota.drafting.presentation.DraftFragment
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
internal class DraftAnalysisFragment : Fragment(R.layout.draft_analysis_fragment), BackPressListener {

    companion object {
        const val TAG = "DraftAnalysisFragment"
    }

    private val binding by viewBinding(DraftAnalysisFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    @Inject
    lateinit var factory: DraftAnalysisViewModel.Factory
    private val viewModel by viewModels<DraftAnalysisViewModel> { factory }

    override fun onBackPressed() {
        viewModel.obtainAction(DraftAnalysisViewActions.OnBackClick)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
        viewModel.viewEvent.setResultListener(this)
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).filterNotNull().onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

    }

    private inner class ViewsConfigurator {
        private val draftAnalysisReasons by lazy { ComplexAdapter() }
        private val radiantHeroesAdapter by lazy { ComplexAdapter() }
        private val radiantBansAdapter by lazy { ComplexAdapter() }
        private val direHeroesAdapter by lazy { ComplexAdapter() }
        private val direBansAdapter by lazy { ComplexAdapter() }
        private val sequenceAdapter by lazy { ComplexAdapter(
            complexListener = object : ComplexListener {
                override fun onClick(item: ComplexItem) {
                    viewModel.obtainAction(DraftAnalysisViewActions.SelectStage(item.key))
                }
            }
        ) }
        private val heroesToSelectAdapter by lazy { ComplexAdapter() }
        private val smoothScrollerSequence: RecyclerView.SmoothScroller by lazy { createSmoothScrollListener() }

        private fun createSmoothScrollListener(): RecyclerView.SmoothScroller {
            return object : LinearSmoothScroller(requireContext()) {
                override fun getHorizontalSnapPreference(): Int = SNAP_TO_START
                override fun calculateSpeedPerPixel(displayMetrics: DisplayMetrics): Float =
                    DraftFragment.MILLISECONDS_PER_INCH / displayMetrics.densityDpi
            }
        }

        fun initStartState() = with(binding) {
            ivBack.clicksWithDebounce {
                viewModel.obtainAction(DraftAnalysisViewActions.OnBackClick)
            }
            ivNext.clicksWithDebounce {
                viewModel.obtainAction(DraftAnalysisViewActions.NextStage)
            }
            ivPrev.clicksWithDebounce {
                viewModel.obtainAction(DraftAnalysisViewActions.PrevStage)
            }
            if (rvRadiantHeroes.adapter == null) {
                rvRadiantHeroes.layoutManager = GridLayoutManager(requireContext(), GlobalVars.mainSettings.getPickSizeCM())
                rvRadiantHeroes.adapter = radiantHeroesAdapter
            }
            if (rvRadiantBans.adapter == null) {
                rvRadiantBans.layoutManager = GridLayoutManager(requireContext(), GlobalVars.mainSettings.getBanSizeCM())
                rvRadiantBans.adapter = radiantBansAdapter
            }
            if (rvDireHeroes.adapter == null) {
                rvDireHeroes.layoutManager = GridLayoutManager(requireContext(), GlobalVars.mainSettings.getPickSizeCM())
                rvDireHeroes.adapter = direHeroesAdapter
            }
            if (rvDireBans.adapter == null) {
                rvDireBans.layoutManager = GridLayoutManager(requireContext(), GlobalVars.mainSettings.getBanSizeCM())
                rvDireBans.adapter = direBansAdapter
            }
            if (rvSequence.adapter == null)
                rvSequence.adapter = sequenceAdapter
            if (rvHeroes.adapter == null) {
                rvHeroes.adapter = heroesToSelectAdapter
            }
        }

        fun renderState(state: DraftAnalysisViewState) {
            when (state) {
                is DraftAnalysisViewState.DefaultState -> renderDefaultState(state)
            }
        }

        private fun renderDefaultState(state: DraftAnalysisViewState.DefaultState) = with(binding) {
            tvRadiantName.text = state.radiantName
            tvDireName.text = state.direName
            radiantHeroesAdapter.submitList(state.radiantPicks)
            radiantBansAdapter.submitList(state.radiantBans)
            direHeroesAdapter.submitList(state.direPicks)
            direBansAdapter.submitList(state.direBans)
            sequenceAdapter.submitList(state.sequence)
            heroesToSelectAdapter.submitList(state.heroesToSelect)
            rvAnalysisReasons.adapter = ComplexAdapter().apply {
                submitList(state.draftAnalysisReasons)
            }

            ivNext.isVisible = state.isNextAvailable
            ivPrev.isVisible = state.isPrevAvailable

            if (state.heroesToSelect.isEmpty()) {
                rvHeroes.isVisible = false
                tvPlaceholderHeroes.isVisible = true
            } else {
                rvHeroes.isVisible = true
                tvPlaceholderHeroes.isVisible = false
            }

            scrollRvToPosition(state.stage, smoothScrollerSequence, rvSequence.layoutManager!!)
        }

        private fun scrollRvToPosition(
            position: Int,
            smoothScroller: RecyclerView.SmoothScroller,
            layoutManager: RecyclerView.LayoutManager
        ) {
//            smoothScroller.targetPosition = position
//            layoutManager.startSmoothScroll(smoothScroller)
        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
                else -> handleCustomEvents(event)
            }
        }

        private fun handleCustomEvents(event: ViewEvent) {
            when (event) {
                is ViewEvent.PopBackStack<*> -> {
                    navController.popBackStack()
                }
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            viewModel.viewEvent.setResultListener(this@DraftAnalysisFragment)
            navController.navigateViaScreenRoute(screen)
        }
    }
}