package com.erg.almagestdota.drafting.presentation.sheets.item_info

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.erg.almagestdota.base.external.image.loadImageByUrl
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.ComplexAdapter
import com.erg.almagestdota.drafting.R
import com.erg.almagestdota.drafting.databinding.DraftItemInfoSheetBinding
import com.erg.almagestdota.drafting.domain.models.DotaItemResult
import com.erg.almagestdota.drafting.presentation.sheets.info.DraftInfoViewState
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdpta.core_navigation.external.helpers.BottomSheetFragment
import com.erg.almagestdpta.core_navigation.external.helpers.navHostFragmentManager
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
internal class DraftItemInfoSheet : BottomSheetFragment(R.layout.draft_item_info_sheet), BackPressListener {

    private val args by navArgs<DraftItemInfoSheetArgs>()

    companion object {
        const val TAG = "DraftItemInfoSheet"
    }

    private val binding by viewBinding { DraftItemInfoSheetBinding.bind(requireContentView()) }
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    @Inject
    lateinit var gson: Gson

    @Inject
    lateinit var viewModelFactory: DraftItemInfoViewModel.Factory
    private val viewModel by viewModels<DraftItemInfoViewModel> {
        DraftItemInfoViewModel.provideFactory(
            item = gson.fromJson(args.item, DotaItemResult::class.java),
            assistedFactory = viewModelFactory
        )
    }

    override fun onBackPressed() {
        onClose()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
        viewModel.viewEvent.setResultListener(this)
    }

    override fun calculateScreenHeight() = ViewGroup.LayoutParams.WRAP_CONTENT
    override fun calculateScreenOffset() = 1.0f

    override fun onBottomSheetStateChanged(bottomSheet: View, newState: Int) {
        super.onBottomSheetStateChanged(bottomSheet, newState)
        if (newState == BottomSheetBehavior.STATE_HIDDEN) {
            onClose()
        }
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun onClose() {
        val fm = navHostFragmentManager()
        navController.popBackStack()
        fm.setFragmentResult(TAG, bundleOf())
    }

    private inner class ViewsConfigurator {
        private val heroSelectorAdapter by lazy { ComplexAdapter() }

        fun initStartState() = with(binding) {
            if (rvHeroes.adapter == null) {
                rvHeroes.adapter = heroSelectorAdapter
                rvHeroes.setHasFixedSize(true)
            }
        }

        fun renderState(state: DraftInfoViewState) {
            when (state) {
                is DraftInfoViewState.DefaultState -> renderDefaultState(state)
            }
        }

        private fun renderDefaultState(state: DraftInfoViewState.DefaultState) = with(binding) {
            heroSelectorAdapter.submitList(state.heroes)
            ivItem.loadImageByUrl(state.itemUrl)
        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            viewModel.viewEvent.setResultListener(this@DraftItemInfoSheet)
            navController.navigateViaScreenRoute(screen)
        }
    }
}