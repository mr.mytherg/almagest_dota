package com.erg.almagestdota.drafting.data

import retrofit2.http.GET
import retrofit2.http.Path

interface OpenDotaApi {

    @GET("players/{player_id}")
    suspend fun loadPlayer(
        @Path("player_id") playerId: Long,
    ): OpenDotaPlayerResponse
}