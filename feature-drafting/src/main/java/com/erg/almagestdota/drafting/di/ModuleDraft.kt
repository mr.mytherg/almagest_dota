package com.erg.almagestdota.drafting.di

import android.content.Context
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.drafting.data.IStratzGraphqlApi
import com.erg.almagestdota.drafting.data.OpenDotaApi
import com.erg.almagestdota.drafting.domain.DraftInteractor
import com.erg.almagestdota.drafting.domain.IDraftInteractor
import com.erg.almagestdota.drafting.domain.use_cases.*
import com.erg.almagestdota.predictor.domain.use_cases.FilterHeroesByRolesUseCase
import com.erg.almagestdota.drafting.domain.use_cases.GetHeroForBotsTurnUseCase
import com.erg.almagestdota.drafting.domain.use_cases.GetRecommendationsUseCase
import com.erg.almagestdota.drafting.domain.use_cases.LoadPlayerInfoUseCase
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.network.external.annotations.RetrofitClientOpendota
import com.erg.almagestdota.predictor.domain.use_cases.LoadPlayerAccountsUseCase
import com.erg.almagestdota.predictor.domain.use_cases.LoadPlayerHeroPoolPublicUseCase
import com.google.firebase.firestore.FirebaseFirestore
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.FragmentScoped
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Singleton

@Module
@InstallIn(FragmentComponent::class)
internal object ModuleDraft {

    @Provides
    @FragmentScoped
    fun provideRepository(
        getHeroForBotsTurnUseCase: GetHeroForBotsTurnUseCase,
        getRecommendationsUseCase: GetRecommendationsUseCase,
        sortHeroesByPickerAlgorithmUseCase: SortHeroesByPickerAlgorithmUseCase,
        filterHeroesByRolesUseCase: FilterHeroesByRolesUseCase,
        filterPlayersByHeroesUseCase: FilterPlayersByHeroesUseCase,
        loadTeamInfoUseCase: LoadTeamInfoUseCase,
        loadPlayerInfoUseCase: LoadPlayerInfoUseCase,
        localStorage: ILocalStorageContract,
        @ApplicationContext context: Context,
        loadPlayerHeroPoolPublicUseCase: LoadPlayerHeroPoolPublicUseCase,
        getTeamStrategiesUseCase: GetTeamStrategiesUseCase,
        stringProvider: StringProvider,

        accountsUseCase: LoadPlayerAccountsUseCase,
        heroPoolPublicUseCase: LoadPlayerHeroPoolPublicUseCase,

        ): IDraftInteractor {
        return DraftInteractor(
            getHeroForBotsTurnUseCase,
            getRecommendationsUseCase,
            sortHeroesByPickerAlgorithmUseCase,
            filterHeroesByRolesUseCase,
            filterPlayersByHeroesUseCase,
            loadTeamInfoUseCase,
            loadPlayerInfoUseCase,
            localStorage,
            context,
            getTeamStrategiesUseCase,
            stringProvider,
            accountsUseCase,
            heroPoolPublicUseCase

        )
    }

    @Provides
    @FragmentScoped
    fun provideGetTeamStrategiesUseCase(
        filterPlayersByHeroesUseCase: FilterPlayersByHeroesUseCase,
        stringProvider: StringProvider
    ): GetTeamStrategiesUseCase {
        return GetTeamStrategiesUseCase(
            filterPlayersByHeroesUseCase,
            stringProvider
        )
    }

    @Provides
    @FragmentScoped
    fun provideLoadPlayerInfoUseCase(
        api: FirebaseFirestore,
        openDotaApi: OpenDotaApi,
        stratzGraphqlApi: IStratzGraphqlApi
    ): LoadPlayerInfoUseCase {
        return LoadPlayerInfoUseCase(
            api,
            openDotaApi,
            stratzGraphqlApi
        )
    }

    @Provides
    @FragmentScoped
    fun provideLoadTeamInfoUseCase(
        api: FirebaseFirestore,
        loadTeamInfoStratzUseCase: LoadTeamInfoStratzUseCase
    ): LoadTeamInfoUseCase {
        return LoadTeamInfoUseCase(
            api,
            loadTeamInfoStratzUseCase
        )
    }

    @Provides
    @FragmentScoped
    fun provideGameDataUseCase(
       localStorage: ILocalStorageContract
    ): GameDataUseCase {
        return GameDataUseCase(
            localStorage,
        )
    }
}

@Module
@InstallIn(SingletonComponent::class)
internal object ModuleDraftSingleton {

    @Provides
    @Singleton
    fun provideOpenDotaApi(@RetrofitClientOpendota retrofit: Retrofit): OpenDotaApi {
        return retrofit.create(OpenDotaApi::class.java)
    }
}