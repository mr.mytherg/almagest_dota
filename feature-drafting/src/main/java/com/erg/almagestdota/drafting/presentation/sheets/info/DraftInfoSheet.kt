package com.erg.almagestdota.drafting.presentation.sheets.info

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.drafting.R
import com.erg.almagestdota.drafting.databinding.DraftInfoSheetBinding
import com.erg.almagestdpta.core_navigation.external.helpers.BottomSheetFragment
import com.erg.almagestdpta.core_navigation.external.helpers.navHostFragmentManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
internal class DraftInfoSheet : BottomSheetFragment(R.layout.draft_info_sheet), BackPressListener {

    companion object {
        const val TAG = "DraftInfoSheet"
    }

    private val binding by viewBinding { DraftInfoSheetBinding.bind(requireContentView()) }
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()

    override fun onBackPressed() {
        onClose()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewsConfigurator.initStartState()
    }

    override fun calculateScreenHeight() = ViewGroup.LayoutParams.WRAP_CONTENT
    override fun calculateScreenOffset() = 1.0f

    override fun onBottomSheetStateChanged(bottomSheet: View, newState: Int) {
        super.onBottomSheetStateChanged(bottomSheet, newState)
        if (newState == BottomSheetBehavior.STATE_HIDDEN) {
            onClose()
        }
    }

    private fun onClose() {
        val fm = navHostFragmentManager()
        navController.popBackStack()
        fm.setFragmentResult(TAG, bundleOf())
    }

    private inner class ViewsConfigurator {
        fun initStartState() = with(binding) {
        }
    }
}