package com.erg.almagestdota.drafting.domain.exceptions

class AlreadySelectedException : Exception()