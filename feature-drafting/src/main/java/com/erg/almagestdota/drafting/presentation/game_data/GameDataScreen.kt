package com.erg.almagestdota.drafting.presentation.game_data

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.drafting.presentation.DraftFragmentDirections
import com.erg.almagestdota.drafting.presentation.roles_selection.RolesSelectionFragmentDirections

object GameDataScreen : Screen<NavDirections>(
    route = RolesSelectionFragmentDirections.toGameDataFragment(),
    requestKey = GameDataFragment.TAG
)