package com.erg.almagestdota.drafting.presentation.players_matches

import com.erg.almagestdota.drafting.R
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.ui.millisToTextAgo
import com.erg.almagestdota.base.external.ui.secondsToMatchTimerFormat
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.complexadapter.*
import com.erg.almagestdota.drafting.domain.IDraftInteractor
import com.erg.almagestdota.drafting.domain.Interactor
import com.erg.almagestdota.drafting.presentation.DraftScreen
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.*
import com.erg.almagestdota.local_storage.external.models.draft.main.*
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

internal class PlayerMatchesViewModel @Inject constructor(
    private val draftInteractor: IDraftInteractor,
    private val stringProvider: StringProvider,
    private val localStorage: ILocalStorageContract,
    private val interactor: Interactor,
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data -> }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow<PlayerMatchesViewState?>(null)
    val viewState = viewStateMutable.asStateFlow()

    private val loadingStateMutable = MutableStateFlow<Loading>(Loading.Disabled)
    val loadingState = loadingStateMutable.asStateFlow()

    init {
        stateConfigurator.teamInfo = interactor.getTeam()
        viewStateMutable.value = stateConfigurator.defineFragmentState()
    }

    fun obtainAction(action: PlayerMatchesViewActions) {
        when (action) {
            is PlayerMatchesViewActions.SelectPlayer -> {
                if (stateConfigurator.selectedPlayerId == action.key.toLong()) return
                stateConfigurator.selectedPlayerId = action.key.toLong()
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is PlayerMatchesViewActions.OpenDraft -> {
                val dotaMatch =
                    stateConfigurator.getDotaMatches().find { it.matchId == action.key.toLong() }
                        ?: return
                val draftModel = DraftModel(
                    monthType = interactor.getDraftModel().monthType,
                    needToLoadPubMatches = interactor.getDraftModel().needToLoadPubMatches,
                    isAllPick = false,
                    gameMode = GameModeType.LIVE_FINISHED,
                    sequence = GlobalVars.mainSettings.sequenceCM,
                    radiantTeam = TeamDraft(
                        picks = dotaMatch.radiantHeroes.toMutableList(),
                        bans = dotaMatch.radiantBans.toMutableList(),
                        teamInfo = GlobalVars.teams[dotaMatch.radiantTeamId] ?: TeamInfo(dotaMatch.radiantTeamId)
                    ),
                    direTeam = TeamDraft(
                        picks = dotaMatch.direHeroes.toMutableList(),
                        bans = dotaMatch.direBans.toMutableList(),
                        teamInfo = GlobalVars.teams[dotaMatch.direTeamId] ?: TeamInfo(dotaMatch.direTeamId)
                    ),
                )
                interactor.setDraftModel(draftModel)
                val screen = DraftScreen(PlayerMatchesFragmentDirections.toDraftFragment())
                emit(viewEventMutable, ViewEvent.Navigation(screen))
            }
            is PlayerMatchesViewActions.OnBackClick -> sendBackClickEvent()
        }
    }

    private fun sendBackClickEvent() {
        emit(viewEventMutable, ViewEvent.PopBackStack.Empty)
    }

    private inner class StateConfigurator {
        var teamInfo = TeamInfo()
        var selectedPlayerId = 0L

        fun defineFragmentState(): PlayerMatchesViewState {
            val dotaMatches = getDotaMatches()
            var selectedIndex = 0
            val players = complexList {
                filterItem(key = "0") {
                    title = stringProvider.getString(R.string.team_roster_filter)
                    isSelected = 0L == selectedPlayerId
                    if (isSelected)
                        selectedIndex = 0
                }
                for (index in teamInfo.members.indices) {
                    val playerId = teamInfo.members[index]
                    val player = GlobalVars.players[playerId] ?: MemberInfo(playerId)
                    filterItem(key = playerId.toString()) {
                        title = player.name.ifEmpty { player.playerId.toString() }
                        isSelected = player.playerId == selectedPlayerId
                        if (isSelected)
                            selectedIndex = index + 1
                    }
                }
            }
            return PlayerMatchesViewState.DefaultState(
                matches = complexList {
                    for (match in dotaMatches) {
                        val tournament = GlobalVars.leagues.find { it.id == match.leagueId }
                        playedMatchItem(key = match.matchId.toString()) {
                            val pair = (match.startTime + match.duration).millisToTextAgo()
                            time = stringProvider.getQuantityString(
                                pair.first,
                                pair.second.toInt(),
                                pair.second.toInt()
                            )
                            title = tournament?.name.orDefault()
                                .ifEmpty { tournament?.tier?.leagueName.orDefault() }
                            radiantScore = match.radiantScore
                            direScore = match.direScore
                            durationReadable = match.duration.secondsToMatchTimerFormat()
                            hasRadiantWon = match.hasRadiantWon
                            radiantName =
                                GlobalVars.teams[match.radiantTeamId]?.name.orDefault()
                                    .ifEmpty {
                                        match.radiantTeamId.toString()
                                    }
                            direName =
                                GlobalVars.teams[match.direTeamId]?.name.orDefault()
                                    .ifEmpty {
                                        match.direTeamId.toString()
                                    }
                            radiantHeroes = getComplexPicks(match.radiantHeroes)
                            direHeroes = getComplexPicks(match.direHeroes)
                            wasPredictionRight = null
                        }
                    }
                }.reversed(),
                players = players,
                selectedIndex = selectedIndex,
                teamName = teamInfo.name.ifEmpty { teamInfo.id.toString() }
            )
        }


        fun getDotaMatches(): List<DotaMatch> {
            return when (selectedPlayerId) {
                0L -> getConsistentMatches(localStorage.metaMatches.filter {
                    it.radiantTeamId == teamInfo.id || it.direTeamId == teamInfo.id
                })
                else -> getPlayerMatches(selectedPlayerId)
            }
        }

        private fun getPlayerMatches(selectedPlayerId: Long): List<DotaMatch> {
            if (selectedPlayerId == 0L) return listOf()
            return localStorage.metaMatches.filter {
                it.radiantHeroes.find { it.playerId == selectedPlayerId } != null ||
                    it.direHeroes.find { it.playerId == selectedPlayerId } != null
            }
        }

        private fun getConsistentMatches(matches: List<DotaMatch>): List<DotaMatch> {
            return matches.filter {
                val isRadiant = it.radiantHeroes.find { it.playerId == teamInfo.members.first() } != null
                val isDire = it.direHeroes.find { it.playerId == teamInfo.members.first() } != null
                when {
                    isRadiant -> isRosterSame(it.radiantHeroes, teamInfo.members)
                    isDire -> isRosterSame(it.direHeroes, teamInfo.members)
                    else -> false
                }
            }
        }

        private fun isRosterSame(
            radiantHeroes: List<PlayerInfo>,
            members: List<Long>
        ): Boolean {
            for (player in radiantHeroes) {
                if (members.find { it == player.playerId } == null)
                    return false
            }
            return true
        }

        private fun getComplexPicks(players: List<PlayerInfo>): List<ComplexItem> {
            return complexList {
                for (player in players) {
                    val hero = player.heroId.getHeroById()
                    itemDefault {
                        url = hero.imageLink
                        title = hero.name
                        hasBlackout = if (selectedPlayerId == 0L) {
                            false
                        } else {
                            selectedPlayerId != player.playerId
                        }
                        hasPlaceholder = true
                    }
                }
            }
        }
    }

    class Factory @Inject constructor(
        private val draftInteractor: IDraftInteractor,
        private val stringProvider: StringProvider,
        private val localStorage: ILocalStorageContract,
        private val interactor: Interactor,
    ) : ViewModelProvider.Factory {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>): T =
            PlayerMatchesViewModel(draftInteractor, stringProvider, localStorage, interactor) as T
    }
}