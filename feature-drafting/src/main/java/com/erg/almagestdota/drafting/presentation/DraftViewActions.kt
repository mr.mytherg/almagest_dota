package com.erg.almagestdota.drafting.presentation

internal sealed class DraftViewActions {

    object OpenHeroSelectorSheet : DraftViewActions()
    class OpenMatchesSheet(val isRadiant: Boolean) : DraftViewActions()
    object OnBackClick : DraftViewActions()
    object OpenPredictor : DraftViewActions()
    object RevertSequence : DraftViewActions()
    object OpenInfo : DraftViewActions()
    object OpenRolesSelection : DraftViewActions()

    object CancelLastChoice : DraftViewActions()
    object SkipBans : DraftViewActions()
    object ShowAnalysis : DraftViewActions()
    class SelectHero(val key: String) : DraftViewActions()
    class OpenResultItemInfoSheet(val key: String) : DraftViewActions()
    class ShowHeroItems(val key: String) : DraftViewActions()
    class SelectGroup(val key: String) : DraftViewActions()
    class SelectFilter(val key: String) : DraftViewActions()
    class SelectType(val key: String) : DraftViewActions()
}