package com.erg.almagestdota.drafting.presentation.game_data

import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.ComplexAdapter
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.drafting.R
import com.erg.almagestdota.drafting.databinding.DraftAnalysisFragmentBinding
import com.erg.almagestdota.drafting.databinding.GameStagesDataFragmentBinding
import com.erg.almagestdota.drafting.presentation.DraftFragment
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
internal class GameDataFragment : Fragment(R.layout.game_stages_data_fragment), BackPressListener {

    companion object {
        const val TAG = "GameDataFragment"
    }

    private val binding by viewBinding(GameStagesDataFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    @Inject
    lateinit var factory: GameDataViewModel.Factory
    private val viewModel by viewModels<GameDataViewModel> { factory }

    override fun onBackPressed() {
        viewModel.obtainAction(GameDataViewActions.OnBackClick)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
        viewModel.viewEvent.setResultListener(this)
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).filterNotNull().onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

    }

    private inner class ViewsConfigurator {
        private val gameStages = ComplexAdapter(
            complexListener = object : ComplexListener {
                override fun onClick(item: ComplexItem) {
                    viewModel.obtainAction(GameDataViewActions.SelectGameStage(item.key))
                }
            }
        )

        fun initStartState() = with(binding) {
            ivBack.clicksWithDebounce {
                viewModel.obtainAction(GameDataViewActions.OnBackClick)
            }

        }

        fun renderState(state: GameDataViewState) {
            when (state) {
                is GameDataViewState.DefaultState -> renderDefaultState(state)
            }
        }

        private fun renderDefaultState(state: GameDataViewState.DefaultState) = with(binding) {
            rvGameStagesData.adapter = ComplexAdapter().apply {
                submitList(state.gameStagesData)
            }
            if (rvGameStages.adapter == null) {
                rvGameStages.adapter = gameStages
                rvGameStages.setHasFixedSize(true)
            }
            gameStages.submitList(state.gameStagesFilters)
        }
    }

    private inner class EventsConfigurator {

        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
                else -> handleCustomEvents(event)
            }
        }

        private fun handleCustomEvents(event: ViewEvent) {
            when (event) {
                is ViewEvent.PopBackStack<*> -> {
                    navController.popBackStack()
                }
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            viewModel.viewEvent.setResultListener(this@GameDataFragment)
            navController.navigateViaScreenRoute(screen)
        }
    }
}