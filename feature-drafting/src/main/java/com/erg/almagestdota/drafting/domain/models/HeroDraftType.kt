package com.erg.almagestdota.drafting.domain.models

import androidx.annotation.StringRes
import com.erg.almagestdota.drafting.R

sealed class HeroDraftType(@StringRes val name: Int, val id: Int) {
    class Coefficients() : HeroDraftType(R.string.draft_type_coefficients, 0)
    class Roles() : HeroDraftType(R.string.draft_type_roles, 1)
    class Players() : HeroDraftType(R.string.draft_type_players, 2)
}