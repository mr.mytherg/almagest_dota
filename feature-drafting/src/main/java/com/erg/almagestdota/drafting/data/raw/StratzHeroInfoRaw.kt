package com.erg.almagestdota.drafting.data.raw

import com.erg.almagestdota.base.external.mapper.NotRequired

/**
 * @param heroes key - hero id
 */

class StratzHeroRaw(
    @NotRequired private val heroes: Map<String, HeroInfoRaw> = mapOf()
)

class HeroInfoRaw(
    @NotRequired private val id: Long = 0L,
    @NotRequired private val displayName: String = "",
    @NotRequired private val stat: HeroStatsRaw? = null,

    )

class HeroStatsRaw(
    @NotRequired private val attackType: String = "",
    @NotRequired private val primaryAttribute: String = "",
    @NotRequired private val complexity: String = "",
    @NotRequired private val team: Boolean? = null,
)