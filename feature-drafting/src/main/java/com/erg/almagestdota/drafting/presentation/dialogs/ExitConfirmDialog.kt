package com.erg.almagestdota.drafting.presentation.dialogs

import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.WhichButton
import com.afollestad.materialdialogs.actions.getActionButton
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.drafting.R
import com.erg.almagestdpta.core_navigation.external.helpers.navHostFragmentManager
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
internal class ExitConfirmDialog : DialogFragment() {

    companion object {
        const val TAG = "ExitConfirmDialog"
        private const val RESULT_KEY = "RESULT_KEY"

        fun isConfirmed(bundle: Bundle): Boolean = bundle.getBoolean(RESULT_KEY).orDefault()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return MaterialDialog(requireContext()).show {
            message(R.string.draft_abandon_dialog_title)
            title(R.string.draft_abandon)
            negativeButton(R.string.action_cancel) { onClose(false) }
            positiveButton(R.string.action_confirm) { onClose(true) }
            getActionButton(WhichButton.POSITIVE).updateTextColor(Color.BLACK)
            cancelOnTouchOutside(false)
            cornerRadius(8f)
        }
    }

    private fun onClose(result: Boolean) {
        val bundle = bundleOf(RESULT_KEY to result)
        val fm = navHostFragmentManager()
        findNavController().popBackStack()
        fm.setFragmentResult(TAG, bundle)
    }
}