package com.erg.almagestdota.drafting.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.extensions.launchOnDefault
import com.erg.almagestdota.base.external.extensions.launchOnIO
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.NotificationStatus
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.heroselector.presentation.heroSelector.HeroSelectorScreen
import com.erg.almagestdota.complexadapter.*
import com.erg.almagestdota.drafting.R
import com.erg.almagestdota.drafting.domain.IDraftInteractor
import com.erg.almagestdota.drafting.domain.Interactor
import com.erg.almagestdota.drafting.domain.exceptions.AlreadySelectedException
import com.erg.almagestdota.drafting.domain.models.DotaItemList
import com.erg.almagestdota.drafting.domain.models.HeroDraftType
import com.erg.almagestdota.drafting.presentation.dialogs.ExitConfirmDialog
import com.erg.almagestdota.drafting.presentation.dialogs.ExitConfirmScreen
import com.erg.almagestdota.drafting.presentation.draft_analysis.DraftAnalysisScreen
import com.erg.almagestdota.drafting.presentation.game_data.GameDataScreen
import com.erg.almagestdota.drafting.presentation.players_matches.PlayerMatchesScreen
import com.erg.almagestdota.drafting.presentation.roles_selection.RolesSelectionScreen
import com.erg.almagestdota.drafting.presentation.sheets.hero_info.DraftHeroInfoScreen
import com.erg.almagestdota.drafting.presentation.sheets.hero_info.DraftHeroInfoSheet
import com.erg.almagestdota.drafting.presentation.sheets.info.DraftInfoScreen
import com.erg.almagestdota.drafting.presentation.sheets.item_info.DraftItemInfoScreen
import com.erg.almagestdota.local_storage.external.models.draft.*
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamDraft
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import com.erg.almagestdota.predictor.PredictorScreen
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import java.lang.Exception
import javax.inject.Inject

internal class DraftViewModel @Inject constructor(
    private val draftInteractor: IDraftInteractor,
    private val stringProvider: StringProvider,
    private val interactor: Interactor
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data ->
        when (resultKey) {
            ExitConfirmDialog.TAG -> {
                if (ExitConfirmDialog.isConfirmed(data)) {
                    sendBackClickEvent()
                }
            }
            HeroSelectorScreen.TAG -> {
                if (HeroSelectorScreen.getResult(data) == HeroSelectorScreen.Result.SELECTED) {
                    val heroId = HeroSelectorScreen.getSelectedHero(data)
                    checkHero(heroId)
                }
            }
            DraftHeroInfoSheet.TAG -> {
                val heroId = DraftHeroInfoScreen.getHeroId(data)
                if (heroId != 0) {
                    selectHero(heroId)
                }
            }
        }
    }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow<DraftViewState?>(null)
    val viewState = viewStateMutable.asStateFlow()

    private val loadingStateMutable = MutableStateFlow<Loading>(Loading.Enabled)
    val loadingState = loadingStateMutable.asStateFlow()

    init {
        launchOnIO(
            loader = loadingStateMutable,
            action = { draftInteractor.configureDraftModel() },
            onSuccess = { viewStateMutable.value = stateConfigurator.defineFragmentState() },
            onError = ::handleError
        )
    }

    fun obtainAction(action: DraftViewActions) {
        when (action) {
            is DraftViewActions.OpenRolesSelection -> {
                interactor.setDraftModel(draftInteractor.getDraftModel())
                emit(viewEventMutable, ViewEvent.Navigation(RolesSelectionScreen))
            }
            is DraftViewActions.SkipBans -> {
                draftInteractor.skipBans()
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is DraftViewActions.OpenMatchesSheet -> {
                val teamInfo = if (action.isRadiant) {
                    draftInteractor.getDraftModel().radiantTeam.teamInfo
                } else {
                    draftInteractor.getDraftModel().direTeam.teamInfo
                }
                interactor.setTeam(teamInfo)
                val screen = PlayerMatchesScreen(
                    DraftFragmentDirections.toPlayerMatchesFragment()
                )
                emit(viewEventMutable, ViewEvent.Navigation(screen))
            }
            is DraftViewActions.OpenPredictor -> {
                draftInteractor.setDraftModel(draftInteractor.getDraftModel())
                val screen = PredictorScreen(DraftFragmentDirections.toPredictorSheet())
                emit(viewEventMutable, ViewEvent.Navigation(screen))
            }
            is DraftViewActions.RevertSequence -> {
                launchOnDefault(
                    loader = loadingStateMutable,
                    action = { draftInteractor.revertSequence() },
                    onSuccess = {
                        viewStateMutable.value = stateConfigurator.defineFragmentState()
                    },
                    onError = ::handleError
                )
            }
            is DraftViewActions.OpenInfo -> {
                emit(viewEventMutable, ViewEvent.Navigation(DraftInfoScreen()))
            }
            is DraftViewActions.OpenHeroSelectorSheet -> {
                if (draftInteractor.hasDraftDone()) return
                val screen = HeroSelectorScreen()
                emit(viewEventMutable, ViewEvent.Navigation(screen))
            }
            is DraftViewActions.ShowAnalysis -> {
                getAnalysis()
            }
            is DraftViewActions.OpenResultItemInfoSheet -> {
                val listIndex = action.key.substring(0, action.key.indexOf(" ")).toInt()
                val itemIndex = action.key.substring(action.key.indexOf(" ") + 1).toInt()
                val item = stateConfigurator.items[listIndex].items[itemIndex]
                val screen = DraftItemInfoScreen(item)
                emit(viewEventMutable, ViewEvent.Navigation(screen))
            }
            is DraftViewActions.ShowHeroItems -> {
                launchOnDefault(
                    loader = loadingStateMutable,
                    action = { draftInteractor.selectResultHero(action.key.toInt()) },
                    onSuccess = {
                        viewStateMutable.value = stateConfigurator.defineFragmentState()
                    },
                    onError = ::handleError
                )
            }
            is DraftViewActions.OnBackClick -> {
                if (draftInteractor.hasDraftDone()) {
                    sendBackClickEvent()
                } else {
                    emit(viewEventMutable, ViewEvent.Navigation(ExitConfirmScreen))
                }
            }
            is DraftViewActions.SelectFilter -> {
                launchOnDefault(
                    loader = loadingStateMutable,
                    action = { draftInteractor.selectFilter(action.key.toInt()) },
                    onSuccess = {
                        viewStateMutable.value = stateConfigurator.defineFragmentState()
                    },
                    onError = ::handleError
                )
            }
            is DraftViewActions.SelectType -> {
                launchOnDefault(
                    loader = loadingStateMutable,
                    action = { draftInteractor.selectType(action.key.toInt()) },
                    onSuccess = {
                        viewStateMutable.value = stateConfigurator.defineFragmentState()
                    },
                    onError = ::handleError
                )
            }
            is DraftViewActions.SelectGroup -> {
                launchOnDefault(
                    loader = loadingStateMutable,
                    action = { draftInteractor.selectGroup(DraftFilterGroupType.valueOf(action.key)) },
                    onSuccess = {
                        viewStateMutable.value = stateConfigurator.defineFragmentState()
                    },
                    onError = ::handleError
                )
            }
            is DraftViewActions.SelectHero -> {
                checkHero(action.key.toInt())
            }
            is DraftViewActions.CancelLastChoice -> {
                launchOnDefault(
                    loader = loadingStateMutable,
                    action = { draftInteractor.cancelLastSelection() },
                    onSuccess = {
                        viewStateMutable.value = stateConfigurator.defineFragmentState()
                    },
                    onError = ::handleError
                )
            }
        }
    }

    private fun getAnalysis() {
        launchOnDefault(
            loader = loadingStateMutable,
            action = { draftInteractor.getAnalysis() },
            onSuccess = {
                interactor.setAnalysis(it)
                emit(viewEventMutable, ViewEvent.Navigation(DraftAnalysisScreen()))
            },
            onError = {
                handleError(it)
            }
        )
    }

    private fun checkHero(heroId: Int) {
        if (draftInteractor.isValid(heroId)) {
            selectHero(heroId)
        } else {
            interactor.setReasons(draftInteractor.getHeroReasons(heroId))
            emit(viewEventMutable, ViewEvent.Navigation(DraftHeroInfoScreen(heroId)))
        }
    }

    private fun selectHero(heroId: Int) {
        launchOnDefault(
            loader = loadingStateMutable,
            action = { draftInteractor.selectHero(heroId) },
            onSuccess = {
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            },
            onError = {
                if (it is AlreadySelectedException) {
                    val text = stringProvider.getString(R.string.hero_has_been_selected)
                    emit(
                        viewEventMutable,
                        ViewEvent.AlmagestSnackbar(text, NotificationStatus.ERROR)
                    )
                } else {
                    handleError(it)
                }
            }
        )
    }

    private fun handleError(e: Exception) {
        val text = e.message
        emit(viewEventMutable, ViewEvent.AlmagestSnackbar(text, NotificationStatus.ERROR))
    }

    private fun sendBackClickEvent() {
        emit(viewEventMutable, ViewEvent.PopBackStack.Empty)
    }

    private inner class StateConfigurator {
        var items: List<DotaItemList> = listOf()
        var failedStatusShown = false

        fun defineFragmentState(): DraftViewState {
            if (failedStatusShown == false) {
                if (draftInteractor.isPubMatchesLoadingFailed()) {
                    failedStatusShown = true
                    val text = stringProvider.getString(R.string.ranked_matches_failed)
                    emit(viewEventMutable, ViewEvent.AlmagestSnackbar(text, NotificationStatus.WARNING))
                }
            }
            return when {
                draftInteractor.getDraftModel().gameMode.isLiveGame() -> {
                    items = draftInteractor.getResultItems()
                    DraftViewState.LiveGameState(
                        heroes = getResultHeroes(),
                        selectedResultHero = draftInteractor.getSelectedResultHero(),
                        types = getComplexTypes(draftInteractor.getTypes()),
                        typeId = draftInteractor.getTypeId(),
                        gameMode = draftInteractor.getDraftModel().gameMode,
                        items = getComplexItems(items),
                        radiantName = draftInteractor.getDraftModel().radiantTeam.teamInfo.name.ifEmpty {
                            stringProvider.getString(R.string.filter_radiant)
                        } + getTeamRating(draftInteractor.getDraftModel().radiantTeam),
                        direName = draftInteractor.getDraftModel().direTeam.teamInfo.name.ifEmpty {
                            stringProvider.getString(R.string.filter_dire)
                        } + getTeamRating(draftInteractor.getDraftModel().direTeam)
                    )
                }
                draftInteractor.hasDraftDone() -> {
                    items = draftInteractor.getResultItems()
                    DraftViewState.ResultState(
                        heroes = getResultHeroes(),
                        selectedResultHero = draftInteractor.getSelectedResultHero(),
                        items = getComplexItems(items),
                        types = getComplexTypes(draftInteractor.getTypes()),
                        typeId = draftInteractor.getTypeId(),
                        hasAnalysis = draftInteractor.hasAnalysis(),
                        gameMode = draftInteractor.getDraftModel().gameMode,
                        isAllPick = draftInteractor.getDraftModel().isAllPick,
                        isCancelChoiceAvailable = draftInteractor.isCancelChoiceAvailable(),
                        radiantName = draftInteractor.getDraftModel().radiantTeam.teamInfo.name.ifEmpty {
                            stringProvider.getString(R.string.filter_radiant)
                        } + getTeamRating(draftInteractor.getDraftModel().radiantTeam),
                        direName = draftInteractor.getDraftModel().direTeam.teamInfo.name.ifEmpty {
                            stringProvider.getString(R.string.filter_dire)
                        } + getTeamRating(draftInteractor.getDraftModel().direTeam)
                    )
                }
                draftInteractor.getDraftModel().gameMode.isLiveDraft() -> {
                    DraftViewState.LiveDraftState(
                        isCancelChoiceAvailable = draftInteractor.isCancelChoiceAvailable(),
                        isRevertSequenceAvailable = draftInteractor.isRevertSequenceAvailable(),
                        groups = getComplexGroups(draftInteractor.getGroups()),
                        filters = getComplexFilters(draftInteractor.getFilters()),
                        types = getComplexTypes(draftInteractor.getTypes()),
                        filterDescription = draftInteractor.getFilterDescription(),
                        sequence = getComplexSequence(draftInteractor.getDraftModel().sequence),
                        radiantHeroes = getComplexPicks(draftInteractor.getTeamHeroes(true)),
                        radiantBans = getComplexBans(draftInteractor.getDraftModel().radiantTeam.bans),
                        direHeroes = getComplexPicks(draftInteractor.getTeamHeroes(false)),
                        direBans = getComplexBans(draftInteractor.getDraftModel().direTeam.bans),
                        heroesToSelect = getComplexHeroesToSelect(draftInteractor.getHeroesToSelect()),
                        gameMode = draftInteractor.getDraftModel().gameMode,
                        filterId = draftInteractor.getFilterId(),
                        groupId = draftInteractor.getGroupId(),
                        typeId = draftInteractor.getTypeId(),
                        draftStage = draftInteractor.getDraftStage(),
                        isAllPick = draftInteractor.getDraftModel().isAllPick,
                        radiantName = draftInteractor.getDraftModel().radiantTeam.teamInfo.name.ifEmpty {
                            stringProvider.getString(R.string.filter_radiant)
                        } + getTeamRating(draftInteractor.getDraftModel().radiantTeam),
                        direName = draftInteractor.getDraftModel().direTeam.teamInfo.name.ifEmpty {
                            stringProvider.getString(R.string.filter_dire)
                        } + getTeamRating(draftInteractor.getDraftModel().direTeam)
                    )
                }
                else -> {
                    DraftViewState.DefaultState(
                        isCancelChoiceAvailable = draftInteractor.isCancelChoiceAvailable(),
                        groups = getComplexGroups(draftInteractor.getGroups()),
                        filters = getComplexFilters(draftInteractor.getFilters()),
                        types = getComplexTypes(draftInteractor.getTypes()),
                        filterDescription = draftInteractor.getFilterDescription(),
                        sequence = getComplexSequence(draftInteractor.getDraftModel().sequence),
                        radiantHeroes = getComplexPicks(draftInteractor.getTeamHeroes(true)),
                        radiantBans = getComplexBans(draftInteractor.getDraftModel().radiantTeam.bans),
                        direHeroes = getComplexPicks(draftInteractor.getTeamHeroes(false)),
                        direBans = getComplexBans(draftInteractor.getDraftModel().direTeam.bans),
                        heroesToSelect = getComplexHeroesToSelect(draftInteractor.getHeroesToSelect()),
                        gameMode = draftInteractor.getDraftModel().gameMode,
                        isAllPick = draftInteractor.getDraftModel().isAllPick,
                        filterId = draftInteractor.getFilterId(),
                        groupId = draftInteractor.getGroupId(),
                        typeId = draftInteractor.getTypeId(),
                        draftStage = draftInteractor.getDraftStage(),
                        radiantName = draftInteractor.getDraftModel().radiantTeam.teamInfo.name.ifEmpty {
                            stringProvider.getString(R.string.filter_radiant)
                        } + getTeamRating(draftInteractor.getDraftModel().radiantTeam),
                        direName = draftInteractor.getDraftModel().direTeam.teamInfo.name.ifEmpty {
                            stringProvider.getString(R.string.filter_dire)
                        } + getTeamRating(draftInteractor.getDraftModel().direTeam)
                    )
                }
            }
        }

        private fun getTeamRating(team: TeamDraft): String {
            val gameMode = draftInteractor.getDraftModel().gameMode
            return if (draftInteractor.getDraftModel().gameMode.hasTeams()) {
                if (gameMode.isBattleCup())
                    return ""
                val avg = team.teamInfo.members.fold(0) { acc, next ->
                    acc + (GlobalVars.players[next]?.rating ?: 1400)
                } / 5
                return " ($avg)"
            } else
                ""
        }

        private fun getComplexItems(itemsLists: List<DotaItemList>): List<ComplexItem> {
            return complexList {
                for (index in itemsLists.indices) {
                    val itemsModel = itemsLists[index]
                    itemsListItem {
                        title = if (index == itemsLists.lastIndex) {
                            stringProvider.getString(
                                itemsModel.title,
                                draftInteractor.getSelectedResultHero().name
                            )
                        } else {
                            stringProvider.getString(itemsModel.title)
                        }
                        items = complexList {
                            for (itemIndex in itemsModel.items.indices) {
                                val item = itemsModel.items[itemIndex]
                                itemDefault(key = "$index $itemIndex") {
                                    url = item.imageLink
                                    title = item.name
                                    hasBlackout = item.countersHeroes.isEmpty()
                                    hasPlaceholder = false
                                }
                            }
                        }
                    }
                }
            }
        }

        private fun getResultHeroes(): List<ComplexItem> {
            return complexList {
                val radiant = draftInteractor.getTeamHeroes(true)
                for (hero in radiant)
                    getComplexItem(hero, this)
                val dire = draftInteractor.getTeamHeroes(false)
                for (hero in dire)
                    getComplexItem(hero, this)
            }
        }

        private fun getComplexSequence(sequence: String): List<ComplexItem> {
            return complexList {
                for (index in sequence.indices) {
                    sequenceItem {
                        isCurrent = draftInteractor.isCurrentStage(index)
                        isPick = draftInteractor.isPickStage(index)
                        isRadiant = draftInteractor.isRadiantStage(index)
                    }
                }
            }
        }

        private fun getComplexFilters(filters: List<DraftFilter>): List<ComplexItem> {
            val filterId = draftInteractor.getFilterId()
            return complexList {
                for (filter in filters) {
                    filterItem(key = filter.id.toString()) {
                        title = filter.name
                        isSelected = filters[filterId].id == filter.id
                    }
                }
            }
        }

        private fun getComplexTypes(types: List<HeroDraftType>): List<ComplexItem> {
            val typeId = draftInteractor.getTypeId()
            return complexList {
                for (type in types) {
                    filterItem(key = type.id.toString()) {
                        title = stringProvider.getString(type.name)
                        isSelected = types[typeId].id == type.id
                    }
                }
            }
        }

        private fun getComplexGroups(groups: List<DraftFilterGroup>): List<ComplexItem> {
            val groupId = draftInteractor.getGroupId()
            return complexList {
                for (group in groups) {
                    filterItem(key = group.type.name) {
                        title = group.name
                        isSelected = groups[groupId].type == group.type
                    }
                }
            }
        }

        private fun getComplexBans(bans: List<Int>): List<ComplexItem> {
            return complexList {
                for (heroId in bans) {
                    val hero = heroId.getHeroById()
                    heroBanItem(key = hero.id.toString()) {
                        url = hero.imageLink
                    }
                }
                for (index in (bans.size + 1)..GlobalVars.mainSettings.getBanSizeCM()) {
                    heroBanItem {
                        url = ""
                    }
                }
            }
        }

        private fun getComplexPicks(heroes: List<HeroCoreData>): List<ComplexItem> {
            return complexList {
                for (hero in heroes) {
                    getComplexItem(hero, this)
                }
                for (index in (heroes.size + 1)..GlobalVars.mainSettings.getPickSizeCM()) {
                    itemDefault {
                        url = null
                        title = ""
                        hasBlackout = false
                        hasPlaceholder = true
                    }
                }
            }
        }

        private fun getComplexHeroesToSelect(heroesToSelect: List<HeroCoreData>): List<ComplexItem> {
            return complexList {
                if (draftInteractor.getGroupId() == 2 && draftInteractor.getDraftModel().gameMode.hasTeams()) {
                    getStrategyComplexItem(heroesToSelect, this)
                } else {
                    for (hero in heroesToSelect) {
                        getComplexItem(hero, this)
                    }
                }
            }
        }

        private fun getStrategyComplexItem(heroesToSelect: List<HeroCoreData>, complexDsl: ComplexDsl) {
            var index1 = 0
            var index2 = index1 + GlobalVars.mainSettings.getPickSizeCM()
            while (index2 < heroesToSelect.size) {
                val heroes = heroesToSelect.subList(index1, index2)
                index1 = index2 + GlobalVars.mainSettings.getBanSizeCM()
                val bans = heroesToSelect.subList(index2, index1)
                index2 = index1 + GlobalVars.mainSettings.getPickSizeCM()
                complexDsl.draftStrategyItem {
                    this.picks = complexList { heroes.map { getComplexItem(it, this) } }
                    this.bans = getComplexBans(bans.map { it.id })
                }
            }
        }

        private fun getComplexItem(hero: HeroCoreData, complexDsl: ComplexDsl) {
            val isBlackout = draftInteractor.getHeroesDraftReasons().find { it.id == hero.id }?.reasons?.isNotEmpty() ?: false
            when (hero) {
                is HeroDraftData -> complexDsl.heroDraftItem(key = hero.id.toString()) {
                    url = hero.imageLink
                    title = hero.name
                    hasBlackout = isBlackout
                    counter = hero.counterValue
                    synergy = hero.synergyValue
                    steal = hero.stealValue
                }
                is HeroResultData -> complexDsl.heroResultItem(key = hero.id.toString()) {
                    url = hero.imageLink
                    title = hero.name
                    hasBlackout = isBlackout
                    counter = hero.counterValue
                    synergy = hero.synergyValue
                }
                is HeroPlayerData -> complexDsl.heroPlayersItem(key = hero.id.toString()) {
                    url = hero.imageLink
                    name = hero.name
                    wonCount = hero.wonCount
                    lostCount = hero.lostCount
                    hasBlackout = isBlackout
                    wonCountPub = hero.wonCountPub
                    lostCountPub = hero.lostCountPub
                    players = hero.heroPlayers.orEmpty().toList()
                }
                is HeroSimplePlayerData -> complexDsl.heroSimplePlayerItem(key = hero.id.toString()) {
                    url = hero.imageLink
                    hasBlackout = isBlackout
                    name = hero.heroPlayer.second.ifEmpty { hero.heroPlayer.first.toString() }
                }
                is HeroRoleData -> complexDsl.heroRolesItem(key = hero.id.toString()) {
                    val roles = hero.heroRoles.orEmpty()
                    for (role in roles) {
                        when (role) {
                            RoleType.ROLE_1 -> hasRole1 = true
                            RoleType.ROLE_2 -> hasRole2 = true
                            RoleType.ROLE_3 -> hasRole3 = true
                            RoleType.ROLE_4 -> hasRole4 = true
                            RoleType.ROLE_5 -> hasRole5 = true
                            else -> Unit
                        }
                    }
                    url = hero.imageLink
                    title = hero.name
                    hasBlackout = isBlackout
                    hasNoRoles = hero.heroRoles.isNullOrEmpty()
                }
                is HeroCoreData -> complexDsl.itemDefault(key = hero.id.toString()) {
                    url = hero.imageLink
                    title = hero.name
                    hasBlackout = isBlackout
                }
            }
        }
    }

    class Factory @Inject constructor(
        private val draftInteractor: IDraftInteractor,
        private val stringProvider: StringProvider,
        private val interactor: Interactor
    ) : ViewModelProvider.Factory {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>): T =
            DraftViewModel(draftInteractor, stringProvider, interactor) as T
    }
}