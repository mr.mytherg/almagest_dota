package com.erg.almagestdota.drafting.presentation.players_matches

internal sealed class PlayerMatchesViewActions {

    object OnBackClick : PlayerMatchesViewActions()
    class OpenDraft(val key: String) : PlayerMatchesViewActions()
    class SelectPlayer(val key: String) : PlayerMatchesViewActions()
}