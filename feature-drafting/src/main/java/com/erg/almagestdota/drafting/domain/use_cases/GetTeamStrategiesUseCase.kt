package com.erg.almagestdota.drafting.domain.use_cases

import android.util.Log
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.drafting.domain.models.DraftStrategy
import com.erg.almagestdota.local_storage.external.models.draft.HeroValues
import com.erg.almagestdota.local_storage.external.models.draft.main.DraftModel
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerInfo
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamDraft
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import com.erg.almagestdota.predictor.domain.use_cases.FilterHeroesByRolesUseCase
import kotlinx.coroutines.*
import javax.inject.Inject
import kotlin.coroutines.coroutineContext

internal class GetTeamStrategiesUseCase @Inject constructor(
    private val filterPlayersByHeroesUseCase: FilterPlayersByHeroesUseCase,
    private val stringProvider: StringProvider
) {
    suspend operator fun invoke(
        draftModel: DraftModel,
        isRadiant: Boolean,
        isRadiantFirstPick: Boolean
    ): List<DraftStrategy> {

        val allie = if (isRadiant) draftModel.radiantTeam else draftModel.direTeam
        val enemy = if (!isRadiant) draftModel.radiantTeam else draftModel.direTeam

        var strategies = mutableSetOf<Set<PlayerInfo>>()

        //один герой. один плеер
        if (allie.picks.size >= 2 && rolesAndPlayersOk(allie)) {
            mixHeroes(strategies, allie, enemy, 1, isRadiant, isRadiantFirstPick)
            if (strategies.isEmpty())
                mixHeroes(strategies, allie, enemy, 2, isRadiant, isRadiantFirstPick)
        }
        strategies = strategies.sortedByDescending {
            getTeamValue(it.map { it.heroId }, enemy.picks.map { it.heroId })
        }.take(20).toMutableSet()
        if (strategies.isEmpty())
            return listOf()
        val draftStrategies = mutableListOf<DraftStrategy>()
        val enemyHeroPool = getEnemyHeroPool(enemy, allie)
        for (strategy in strategies) {
            val draftStrategy = DraftStrategy(
                picks = strategy.toList(),
                bans = getBansAgainstStrategy(strategy, enemyHeroPool, allie.bans, enemy, isRadiant, isRadiantFirstPick)
            )
            draftStrategies.add(draftStrategy)
        }

        return draftStrategies
    }

    private fun rolesAndPlayersOk(allie: TeamDraft): Boolean {
        return filterPlayersByHeroesUseCase.getMatchUpList(allie).isNotEmpty()
    }

    private fun getEnemyHeroPool(enemy: TeamDraft, allie: TeamDraft): List<Int> {
        return enemy.heroPool.filter { map ->
            map.value.isNotEmpty() &&
                allie.picks.find { it.heroId == map.key } == null &&
                allie.bans.find { it == map.key } == null &&
                enemy.bans.find { it == map.key } == null &&
                enemy.picks.find { it.heroId == map.key } == null
        }.map { it.key }.ifEmpty {
            GlobalVars.mainSettings.heroes.filter { hero ->
                allie.picks.find { it.heroId == hero.id } == null &&
                    allie.bans.find { it == hero.id } == null &&
                    enemy.bans.find { it == hero.id } == null &&
                    enemy.picks.find { it.heroId == hero.id } == null
            }.map { it.id }
        }
    }

    private fun getBansAgainstStrategy(
        strategy: Set<PlayerInfo>,
        _enemyHeroPool: List<Int>,
        bans: MutableList<Int>,
        enemyTeam: TeamDraft,
        isRadiant: Boolean,
        isRadiantFirstPick: Boolean
    ): List<Int> {
        val enemyHeroPool = _enemyHeroPool.filter { heroId ->
            strategy.find { it.heroId == heroId } == null
        }
        val map = mutableMapOf<Int, Double>()
        for (hero in enemyHeroPool) {
            map[hero] = hero.getCounter(strategy.map { it.heroId })
        }
        val additionalBans = map.toList().sortedByDescending { it.second }.map { it.first }.filter {
            val playersMatchUps = filterPlayersByHeroesUseCase.invoke(listOf(GlobalVars.heroesAsMap[it]!!), enemyTeam)
            playersMatchUps.isNotEmpty()
        }.take(GlobalVars.mainSettings.getBanSizeCM() - bans.size)

        return bans.plus(getBansInTheRightOrder(additionalBans, bans, isRadiant, isRadiantFirstPick))
    }

    private fun getBansInTheRightOrder(
        additionalBans: List<Int>,
        bans: List<Int>,
        isHeroesForRadiant: Boolean,
        hasRadiantFirstPick: Boolean
    ): List<Int> {
        if (additionalBans.isEmpty()) return listOf()
        if (additionalBans.size == 1) return additionalBans

        val stagesGroup = GlobalVars.groups.find { it.type == DraftFilterGroupType.DRAFT_STAGES }!!
        val firstStage = mutableListOf<Int>()
        val secondStage = mutableListOf<Int>()
        val thirdStage = mutableListOf<Int>()
        val fourStage = mutableListOf<Int>()

        if (!(hasRadiantFirstPick && isHeroesForRadiant || !hasRadiantFirstPick && !isHeroesForRadiant)) {
            for (index in 0..6) {
                if (bans.size > index) continue
                val heroId = additionalBans[index - bans.size]
                when {
                    index == 0 && stagesGroup.filters[0].heroes.findHeroByBinaryMethod(heroId) != -1 -> firstStage.add(heroId)
                    index == 1 && stagesGroup.filters[2].heroes.findHeroByBinaryMethod(heroId) != -1 -> secondStage.add(heroId)
                    index in 2..3 && stagesGroup.filters[4].heroes.findHeroByBinaryMethod(heroId) != -1 -> thirdStage.add(heroId)
                    else ->  fourStage.add(heroId)
                }
            }
        } else {
            for (index in 0..6) {
                if (bans.size > index) continue
                val heroId = additionalBans[index - bans.size]
                when {
                    index in 0..1 && stagesGroup.filters[1].heroes.findHeroByBinaryMethod(heroId) != -1 -> firstStage.add(heroId)
                    index == 2 && stagesGroup.filters[3].heroes.findHeroByBinaryMethod(heroId) != -1 -> secondStage.add(heroId)
                    index == 3 && stagesGroup.filters[5].heroes.findHeroByBinaryMethod(heroId) != -1 -> thirdStage.add(heroId)
                    else ->  fourStage.add(heroId)
                }
            }
        }

        return firstStage.plus(secondStage).plus(thirdStage).plus(fourStage)
    }

    private fun getTeamValue(
        allies: List<Int>,
        enemies: List<Int>,
        heroes: Map<Int, HeroValues> = GlobalVars.importantSettings.heroesProValues
    ): Double {
        return allies.fold(0.0) { acc, next ->
            acc + next.getCounter(enemies, heroes) + next.getSynergy(
                allies,
                heroes
            )
        }
    }

    private suspend fun mixHeroes(
        strategies: MutableSet<Set<PlayerInfo>>,
        team: TeamDraft,
        enemyTeam: TeamDraft,
        state: Int,
        isRadiant: Boolean,
        isRadiantFirstPick: Boolean
    ) {
        val heroPool = team.heroPool.filter { map ->
            val playersMatchUps = filterPlayersByHeroesUseCase.invoke(listOf(GlobalVars.heroesAsMap[map.key]!!), enemyTeam)

            map.value.isNotEmpty() &&
                team.picks.find { it.heroId == map.key } == null &&
                team.bans.find { it == map.key } == null &&
                enemyTeam.bans.find { it == map.key } == null &&
                enemyTeam.picks.find { it.heroId == map.key } == null
                && playersMatchUps.isNotEmpty()

        }
        val listDeferred1 = mutableListOf<Deferred<Unit>>()

        val picks2Stage = team.picks.map { it.heroId }
        val stage3Pool = heroPool.filter { pool -> picks2Stage.find { it == pool.key } == null }
        for (hero3 in stage3Pool) {
            val deferred = CoroutineScope(coroutineContext).async(Dispatchers.Default) {
                val picks3Stage: MutableList<Int> = picks2Stage.toMutableList()
//                if (needToContinue(state, hero3.key, enemyTeam))
//                    return@async
                picks3Stage.add(hero3.key)
                if (picks3Stage.size == 5) {
                    addNewStrategies(picks3Stage.toList(), strategies, team, isRadiant, isRadiantFirstPick)
                    return@async
                }
                val stage4Pool = stage3Pool.filter { pool -> picks3Stage.find { it == pool.key } == null }
                for (hero4 in stage4Pool) {
                    val picks4Stage: MutableList<Int> = picks3Stage.toMutableList()
//                    if (needToContinue(state, hero4.key, enemyTeam))
//                        continue
                    picks4Stage.add(hero4.key)
                    if (picks4Stage.size == 5) {
                        addNewStrategies(picks4Stage.toList(), strategies, team, isRadiant, isRadiantFirstPick)
                        continue
                    }
                    val stage5Pool = stage4Pool.filter { pool -> picks4Stage.find { it == pool.key } == null }
                    for (hero5 in stage5Pool) {
                        val picks5Stage: MutableList<Int> = picks4Stage.toMutableList()
//                        if (needToContinue(state, hero5.key, enemyTeam))
//                            continue
                        picks5Stage.add(hero5.key)
                        if (picks5Stage.size == 5) {
                            addNewStrategies(picks5Stage.toList(), strategies, team, isRadiant, isRadiantFirstPick)
                            continue
                        }
                    }
                }
            }
            listDeferred1.add(deferred)
        }
        listDeferred1.awaitAll()
    }

    private fun needToContinue(state: Int, heroId: Int, enemyTeam: TeamDraft): Boolean {
        val isAllPositive = heroId.isAllCounterPositive(enemyTeam.picks.map { it.heroId }, GlobalVars.importantSettings.heroesProValues)

        return if (state == 1)
            !isAllPositive
        else {
            if (isAllPositive)
                false
            else
                heroId.getCounter(enemyTeam.picks.map { it.heroId }, GlobalVars.importantSettings.heroesProValues) < 0
        }
    }

    private fun addNewStrategies(
        picks: List<Int>,
        strategies: MutableSet<Set<PlayerInfo>>,
        team: TeamDraft,
        isRadiant: Boolean,
        isRadiantFirstPick: Boolean
    ) {
        val teamDraft = TeamDraft(picks.map { PlayerInfo(it, 0L) }.toMutableList(), teamInfo = team.teamInfo).apply {
            heroPool = team.heroPool
        }
        val matchUpsPlayers = filterPlayersByHeroesUseCase.getMatchUpList(teamDraft)
//        val matchUpsRoles = filterHeroesByRolesUseCase.getMatchUpList(picks.map { it.getHeroById() })
//        val playersHeroIds = matchUpsPlayers.map { it.map { it.id } }
//
//        val commonMatchUps = matchUpsRoles.map { it.map { it.id } }.filter {
//
//        }
//
        if (matchUpsPlayers.isNotEmpty()) {
            for (matchUp in matchUpsPlayers) {
                if (matchUp.all { it.heroPlayers.orEmpty().isNotEmpty() }) {
                    var heroes = ""
                    val list = matchUp.map {
                        heroes += "${it.name}|"
                        PlayerInfo(it.id, it.heroPlayers.orEmpty().first().first)
                    }
                    val alreadySelectedSize = team.picks.size
                    val listOfHeroes = list.sortedWith(
                        compareByDescending<PlayerInfo> { it.heroId == team.picks[0].heroId }
                            .thenByDescending { it.heroId == team.picks[1].heroId }
                            .thenByDescending { it.heroId == team.picks.getOrNull(2)?.heroId.orDefault() }
                            .thenByDescending { it.heroId == team.picks.getOrNull(3)?.heroId.orDefault() }
                            .thenByDescending { it.heroId == team.picks.getOrNull(4)?.heroId.orDefault() }
                    )
                    val previousList = listOfHeroes.take(alreadySelectedSize)
                    strategies.add(
                        previousList.plus(
                            filterHeroesByStages(
                                team.picks,
                                listOfHeroes.takeLast(GlobalVars.mainSettings.getPickSizeCM() - alreadySelectedSize),
                                isRadiantFirstPick,
                                isRadiant
                            )
                        ).toSet()
                    )
                    Log.e("STRATEGYALMA", "added $heroes ${strategies.size}")
                } else
                    Log.e("STRATEGYALMA", "fail ${strategies.size}")
            }
        }
    }

    private fun filterHeroesByStages(
        teamHeroes: List<PlayerInfo>,
        additionalPicks: List<PlayerInfo>,
        hasRadiantFirstPick: Boolean,
        isHeroesForRadiant: Boolean
    ): List<PlayerInfo> {
        if (additionalPicks.isEmpty()) return listOf()
        if (additionalPicks.size == 1) return additionalPicks

        val stagesGroup = GlobalVars.groups.find { it.type == DraftFilterGroupType.DRAFT_STAGES }!!
        val firstStage = mutableListOf<PlayerInfo>()
        val secondStage = mutableListOf<PlayerInfo>()
        val thirdStage = mutableListOf<PlayerInfo>()
        val fourStage = mutableListOf<PlayerInfo>()

        if (hasRadiantFirstPick && isHeroesForRadiant || !hasRadiantFirstPick && !isHeroesForRadiant) {
            for (index in 0..4) {
                if (teamHeroes.size > index) continue
                val player = additionalPicks[index - teamHeroes.size]
                when {
                    index == 0 && stagesGroup.filters[0].heroes.findHeroByBinaryMethod(player.heroId) != -1 -> firstStage.add(player)
                    index == 1 && stagesGroup.filters[2].heroes.findHeroByBinaryMethod(player.heroId) != -1 -> secondStage.add(player)
                    index in 2..3 && stagesGroup.filters[4].heroes.findHeroByBinaryMethod(player.heroId) != -1 -> thirdStage.add(player)
                    else ->  fourStage.add(player)
                }
            }
        } else {
            for (index in 0..4) {
                if (teamHeroes.size > index) continue
                val player = additionalPicks[index - teamHeroes.size]
                when{
                    index in 0..1 && stagesGroup.filters[1].heroes.findHeroByBinaryMethod(player.heroId) != -1 -> firstStage.add(player)
                    index == 2 && stagesGroup.filters[3].heroes.findHeroByBinaryMethod(player.heroId) != -1 -> secondStage.add(player)
                    index == 3 && stagesGroup.filters[5].heroes.findHeroByBinaryMethod(player.heroId) != -1 -> thirdStage.add(player)
                    else ->  fourStage.add(player)
                }
            }
        }

        return firstStage.plus(secondStage).plus(thirdStage).plus(fourStage)
    }
}