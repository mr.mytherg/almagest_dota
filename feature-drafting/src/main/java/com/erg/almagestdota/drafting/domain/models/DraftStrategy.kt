package com.erg.almagestdota.drafting.domain.models

import androidx.annotation.StringRes
import com.erg.almagestdota.drafting.R
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerInfo

data class DraftStrategy(
    val picks: List<PlayerInfo>,
    val bans: List<Int>
)