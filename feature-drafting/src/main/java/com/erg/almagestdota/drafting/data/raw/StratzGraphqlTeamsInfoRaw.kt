package com.erg.almagestdota.drafting.data.raw

class TeamsInfoResponseRaw(
    val data: TeamsInfoDataRaw? = null
)

class TeamsInfoDataRaw(
    val teams: List<TeamInfoRaw>? = null
)

class TeamInfoRaw(
    val id: Long? = 0L,
    val name: String? = "",
    val logo: String? = "",
    val members: List<MemberInfoRaw?>? = listOf()
)

class MemberInfoRaw(
    val steamAccountId: Long? = 0L,
    val steamAccount: SteamAccountRaw? = null
)

class SteamAccountRaw(
    val proSteamAccount: ProSteamAccountRaw? = null
)

class ProSteamAccountRaw(
    val name: String? = "",
    val position: String? = ""
)