package com.erg.almagestdota.drafting.domain

import com.erg.almagestdota.drafting.domain.use_cases.*
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.DraftAnalysis
import com.erg.almagestdota.local_storage.external.models.draft.*
import com.erg.almagestdota.local_storage.external.models.draft.main.*
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.local_storage.internal.data.models.draft.GameStagesTeamData
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
internal class Interactor @Inject constructor(
    private val localStorage: ILocalStorageContract,
) {
    fun setTeam(team: TeamInfo) {
        localStorage.selectedTeam = team
    }

    fun setDraftModel(draftModel: DraftModel) {
        localStorage.draftModel = draftModel
    }

    fun getDraftModel(): DraftModel {
        return localStorage.draftModel!!
    }

    fun setGameStagesTeamData(gameStagesData: GameStagesTeamData) {
        localStorage.gameStagesData = gameStagesData
    }

    fun getGameStagesTeamData(): GameStagesTeamData {
        return localStorage.gameStagesData
    }

    fun setAnalysis(analysis: List<DraftAnalysis>) {
        localStorage.analysis = analysis
    }

    fun getAnalysis(): List<DraftAnalysis> {
        return localStorage.analysis
    }

    fun setReasons(reasons: List<DraftReasonType>) {
        localStorage.reasons = reasons
    }

    fun getReasons(): List<DraftReasonType> {
        return localStorage.reasons
    }

    fun getTeam(): TeamInfo {
        return localStorage.selectedTeam
    }

    fun isPickStage(sequence: String, stage: Int): Boolean {
        return sequence[stage] == GlobalVars.RADIANT_PICK || sequence[stage] == GlobalVars.DIRE_PICK
    }

    fun isBanStage(sequence: String, stage: Int): Boolean =
        sequence[stage] == GlobalVars.RADIANT_BAN || sequence[stage] == GlobalVars.DIRE_BAN

    fun isRadiantStage(sequence: String, stage: Int): Boolean {
        return sequence[stage] == GlobalVars.RADIANT_PICK || sequence[stage] == GlobalVars.RADIANT_BAN
    }

}