package com.erg.almagestdota.drafting.presentation.sheets.info

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem

internal sealed class DraftInfoViewState : ViewState() {

    class DefaultState(
        val itemUrl: String,
        val heroes: List<ComplexItem>,
    ) : DraftInfoViewState()
}