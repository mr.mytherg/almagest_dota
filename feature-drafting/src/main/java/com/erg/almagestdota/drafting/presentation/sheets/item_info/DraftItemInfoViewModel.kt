package com.erg.almagestdota.drafting.presentation.sheets.item_info

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.complexadapter.complexList
import com.erg.almagestdota.complexadapter.itemDefault
import com.erg.almagestdota.drafting.domain.models.DotaItemResult
import com.erg.almagestdota.drafting.presentation.sheets.info.DraftInfoViewState
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

internal class DraftItemInfoViewModel @AssistedInject constructor(
    @Assisted private val item: DotaItemResult,
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data -> }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    private inner class StateConfigurator {

        fun defineFragmentState(): DraftInfoViewState {
            return DraftInfoViewState.DefaultState(
                itemUrl = item.imageLink,
                heroes = complexList {
                    for (heroId in item.countersHeroes) {
                        val hero = heroId.getHeroById()
                        itemDefault {
                            title = hero.name
                            url = hero.imageLink
                            hasPlaceholder = false
                        }
                    }
                }
            )
        }
    }

    companion object {
        fun provideFactory(
            item: DotaItemResult,
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create(
                        item = item,
                    ) as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(
            item: DotaItemResult,
        ): DraftItemInfoViewModel
    }
}