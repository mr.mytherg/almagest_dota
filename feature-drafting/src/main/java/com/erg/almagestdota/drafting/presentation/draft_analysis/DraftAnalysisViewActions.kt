package com.erg.almagestdota.drafting.presentation.draft_analysis

internal sealed class DraftAnalysisViewActions {

    object OnBackClick : DraftAnalysisViewActions()
    object NextStage : DraftAnalysisViewActions()
    class SelectStage(val key: String) : DraftAnalysisViewActions()
    object PrevStage : DraftAnalysisViewActions()
}