package com.erg.almagestdota.drafting.presentation.roles_selection

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.ComplexAdapter
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.drafting.R
import com.erg.almagestdota.drafting.databinding.RolesSelectionFragmentBinding
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
internal class RolesSelectionFragment : Fragment(R.layout.roles_selection_fragment), BackPressListener {

    companion object {
        const val TAG = "RolesSelectionFragment"
    }

    private val binding by viewBinding(RolesSelectionFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    @Inject
    lateinit var factory: RolesSelectionViewModel.Factory
    private val viewModel by viewModels<RolesSelectionViewModel> { factory }

    override fun onBackPressed() {
        viewModel.obtainAction(RolesSelectionViewActions.OnBackClick)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
        viewModel.viewEvent.setResultListener(this)
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).filterNotNull().onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private inner class ViewsConfigurator {
        private val radiantHeroesAdapter = RolesSelectionAdapter(
            complexListener = object : ComplexListener {
                override fun onClick(item: ComplexItem) {
                    viewModel.obtainAction(RolesSelectionViewActions.SelectHeroRoleRadiant(item.key))
                }
            }
        )
        private val direHeroesAdapter = RolesSelectionAdapter(
            complexListener = object : ComplexListener {
                override fun onClick(item: ComplexItem) {
                    viewModel.obtainAction(RolesSelectionViewActions.SelectHeroRoleDire(item.key))
                }
            }
        )

        fun initStartState() = with(binding) {
            ivBack.clicksWithDebounce {
                viewModel.obtainAction(RolesSelectionViewActions.OnBackClick)
            }
            tvShowData.clicksWithDebounce {
                viewModel.obtainAction(RolesSelectionViewActions.ShowData)
            }

        }

        fun renderState(state: RolesSelectionViewState) {
            when (state) {
                is RolesSelectionViewState.DefaultState -> renderDefaultState(state)
            }
        }

        private fun renderDefaultState(state: RolesSelectionViewState.DefaultState) = with(binding) {

            if (rvRadiant.adapter == null) {
                rvRadiant.adapter = radiantHeroesAdapter
                rvRadiant.setHasFixedSize(true)
            }
            radiantHeroesAdapter.submitList(state.radiantHeroes as List<ComplexItem.RoleSelectionItem>)

            if (rvDire.adapter == null) {
                rvDire.adapter = direHeroesAdapter
                rvDire.setHasFixedSize(true)
            }
            direHeroesAdapter.submitList(state.direHeroes as List<ComplexItem.RoleSelectionItem>)

            tvShowData.alpha = if (state.hasAllHeroesSettled) 1.0f else 0.4f
            tvShowData.isEnabled = state.hasAllHeroesSettled
        }
    }

    private inner class EventsConfigurator {

        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
                is ViewEvent.PopBackStack<*> -> navController.popBackStack()
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            viewModel.viewEvent.setResultListener(this@RolesSelectionFragment)
            navController.navigateViaScreenRoute(screen)
        }
    }
}