package com.erg.almagestdota.drafting.presentation.draft_analysis

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.*
import com.erg.almagestdota.drafting.R
import com.erg.almagestdota.drafting.domain.Interactor
import com.erg.almagestdota.local_storage.external.models.DraftAnalysis
import com.erg.almagestdota.local_storage.external.models.draft.*
import com.erg.almagestdota.local_storage.external.models.draft.main.*
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

internal class DraftAnalysisViewModel @Inject constructor(
    private val interactor: Interactor,
    private val stringProvider: StringProvider,
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data -> }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    fun obtainAction(action: DraftAnalysisViewActions) {
        when (action) {
            is DraftAnalysisViewActions.SelectStage -> {
                val index = action.key.toInt()
                if (stateConfigurator.stage == index) return
                stateConfigurator.stage = index
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is DraftAnalysisViewActions.OnBackClick -> sendBackClickEvent()
            is DraftAnalysisViewActions.NextStage -> {
                if (stateConfigurator.draftAnalysis.lastIndex == stateConfigurator.stage) return
                stateConfigurator.stage++
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is DraftAnalysisViewActions.PrevStage -> {
                if (stateConfigurator.stage == 0) return
                stateConfigurator.stage--
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
        }
    }

    private fun sendBackClickEvent() {
        emit(viewEventMutable, ViewEvent.PopBackStack.Empty)
    }

    private inner class StateConfigurator {
        val draftAnalysis = interactor.getAnalysis()
        var stage = 0
        var selectedStage = interactor.getAnalysis()[stage]
        var isRecommendations = true
        var reasons = listOf<DraftReasonType>(
            DraftReasonType.LOW_COEF,
            DraftReasonType.NOT_IN_POOL,
            DraftReasonType.NON_META,
            DraftReasonType.INVALID_STAGE,
            DraftReasonType.PLAYER_ALREADY_HAS_HERO,
        )

        fun defineFragmentState(): DraftAnalysisViewState {
            stateConfigurator.selectedStage = stateConfigurator.draftAnalysis[stateConfigurator.stage]

            val heroes = if (isRecommendations)
                selectedStage.heroesReasons.filter { it.reasons.count { it.isBad } == 0 }.ifEmpty {
                    selectedStage.heroesReasons.filter {
                        it.reasons.find {
                            it == DraftReasonType.HAS_HERO_FOR_THIS_ROLE
                                || it == DraftReasonType.PLAYER_ALREADY_HAS_HERO
                        } == null
                    }.sortedBy { it.reasons.count { it.isBad } }.take(5)
                }
            else
                selectedStage.heroesReasons

            val radiantName = selectedStage.draftModel.radiantTeam.teamInfo.name.ifEmpty { stringProvider.getString(R.string.radiant) }
            val direName = selectedStage.draftModel.direTeam.teamInfo.name.ifEmpty { stringProvider.getString(R.string.dire) }


            val draftAnalysisComplex = complexList {
                draftReasonItem {
                    name = "$radiantName vs $direName"
                }
                for (reason in reasons) {
                    draftAnalysisReasonItem {
                        name = stringProvider.getString(DraftReasonType.getStringId(reason))
                        radiantCount = draftAnalysis.count { draftAnalysis ->
                            val hero = draftAnalysis.heroesReasons.find { it.id == draftAnalysis.selectedHeroId }!!
                            isRadiantStage(draftAnalysis) && hero.reasons.find { it == reason } != null
                        }
                        direCount = draftAnalysis.count { draftAnalysis ->
                            val hero = draftAnalysis.heroesReasons.find { it.id == draftAnalysis.selectedHeroId }!!
                            isDireStage(draftAnalysis) && hero.reasons.find { it == reason } != null
                        }
                        val hero = selectedStage.heroesReasons.find { it.id == selectedStage.selectedHeroId }!!
                        hasBlackout = hero.reasons.find { it == reason } == null
                    }
                }
            }

            return DraftAnalysisViewState.DefaultState(
                radiantBans = getComplexBans(selectedStage.draftModel.radiantTeam.bans),
                radiantPicks = getComplexPicks(selectedStage.draftModel.radiantTeam.picks.map { it.heroId.getHeroById() }),
                direBans = getComplexBans(selectedStage.draftModel.direTeam.bans),
                direPicks = getComplexPicks(selectedStage.draftModel.direTeam.picks.map { it.heroId.getHeroById() }),
                heroesToSelect = getComplexList(heroes),
                sequence = getComplexSequence(selectedStage.draftModel.sequence),
                isPrevAvailable = stage > 0,
                isNextAvailable = stage < draftAnalysis.lastIndex,
                stage = stage,
                radiantName = radiantName,
                direName = direName,
                draftAnalysisReasons = draftAnalysisComplex
            )
        }

        private fun isRadiantStage(stageAnalysis: DraftAnalysis): Boolean {
            val ch = stageAnalysis.draftModel.sequence[stageAnalysis.stage]
            return ch == GlobalVars.RADIANT_BAN || ch == GlobalVars.RADIANT_PICK
        }

        private fun isDireStage(stageAnalysis: DraftAnalysis): Boolean {
            val ch = stageAnalysis.draftModel.sequence[stageAnalysis.stage]
            return ch == GlobalVars.DIRE_BAN || ch == GlobalVars.DIRE_PICK
        }

        private fun getComplexList(heroes: List<HeroCoreData>): List<ComplexItem> {
            return complexList {
                for (hero in heroes) {
                    getComplexItem(hero, this)
                }
            }
        }


        private fun getComplexSequence(sequence: String): List<ComplexItem> {
            return complexList {
                for (index in sequence.indices) {
                    sequenceItem(key = index.toString()) {
                        isCurrent = index == stage
                        isPick = interactor.isPickStage(selectedStage.draftModel.sequence, index)
                        isRadiant = interactor.isRadiantStage(selectedStage.draftModel.sequence, index)
                    }
                }
            }
        }

        private fun getComplexItem(hero: HeroCoreData, complexDsl: ComplexDsl) {
            when (hero) {
                is HeroDraftData -> complexDsl.heroDraftItem(key = hero.id.toString()) {
                    hasBlackout = selectedStage.selectedHeroId != hero.id
                    url = hero.imageLink
                    title = hero.name
                    counter = hero.counterValue
                    synergy = hero.synergyValue
                    steal = hero.stealValue
                }
                is HeroResultData -> complexDsl.heroResultItem(key = hero.id.toString()) {
                    hasBlackout = selectedStage.selectedHeroId != hero.id
                    url = hero.imageLink
                    title = hero.name
                    counter = hero.counterValue
                    synergy = hero.synergyValue
                }
                is HeroPlayerData -> complexDsl.heroPlayersItem(key = hero.id.toString()) {
                    hasBlackout = selectedStage.selectedHeroId != hero.id
                    url = hero.imageLink
                    name = hero.name
                    wonCount = hero.wonCount
                    lostCount = hero.lostCount
                    wonCountPub = hero.wonCountPub
                    lostCountPub = hero.lostCountPub
                    players = hero.heroPlayers.orEmpty().toList()
                }
                is HeroCoreData -> complexDsl.itemDefault(key = hero.id.toString()) {
                    hasBlackout = selectedStage.selectedHeroId != hero.id
                    url = hero.imageLink
                    title = hero.name
                }
            }
        }

        private fun getComplexPicks(heroes: List<HeroCoreData>): List<ComplexItem> {
            return complexList {
                for (hero in heroes) {
                    getComplexItem(hero, this)
                }
                for (index in (heroes.size + 1)..GlobalVars.mainSettings.getPickSizeCM()) {
                    itemDefault {
                        url = null
                        title = ""
                        hasBlackout = false
                        hasPlaceholder = true
                    }
                }
            }
        }


        private fun getComplexBans(bans: List<Int>): List<ComplexItem> {
            return complexList {
                for (heroId in bans) {
                    val hero = heroId.getHeroById()
                    heroBanItem(key = hero.id.toString()) {
                        url = hero.imageLink
                        hasBlackout = selectedStage.selectedHeroId != hero.id
                    }
                }
                for (index in (bans.size + 1)..GlobalVars.mainSettings.getBanSizeCM()) {
                    heroBanItem {
                        url = ""
                    }
                }
            }
        }
    }

    class Factory @Inject constructor(
        private val interactor: Interactor,
        private val stringProvider: StringProvider,
    ) : ViewModelProvider.Factory {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>): T =
            DraftAnalysisViewModel(interactor, stringProvider) as T
    }
}