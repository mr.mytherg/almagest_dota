package com.erg.almagestdota.drafting.presentation.sheets.hero_info

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.drafting.domain.models.HeroInfoModel
import com.erg.almagestdota.drafting.presentation.DraftFragmentDirections
import com.google.gson.Gson

class DraftHeroInfoScreen(
    heroId: Int,
) : Screen<NavDirections>(
    route = DraftFragmentDirections.toDraftHeroInfoSheet(heroId),
    requestKey = DraftHeroInfoSheet.TAG
) {
    companion object {
        private val HERO_ID_KEY = "HERO_ID_KEY"

        fun getHeroId(data: Bundle): Int = data.getInt(HERO_ID_KEY, 0)

        fun createBundleForResult(heroId: Int): Bundle {
            return bundleOf(HERO_ID_KEY to heroId)
        }
    }
}