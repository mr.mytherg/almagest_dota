package com.erg.almagestdota.drafting.presentation

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.local_storage.external.models.draft.*
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData

internal sealed class DraftViewState : ViewState() {

    class DefaultState(
        val isCancelChoiceAvailable: Boolean,
        val groups: List<ComplexItem>,
        val filters: List<ComplexItem>,
        val types: List<ComplexItem>,
        val filterDescription: String,
        val radiantHeroes: List<ComplexItem>,
        val radiantBans: List<ComplexItem>,
        val direHeroes: List<ComplexItem>,
        val direBans: List<ComplexItem>,
        val heroesToSelect: List<ComplexItem>,
        val sequence: List<ComplexItem>,
        val draftStage: Int,
        val radiantName: String,
        val direName: String,
        val gameMode: GameModeType,
        val isAllPick: Boolean,
        val groupId: Int,
        val filterId: Int,
        val typeId: Int,
    ) : DraftViewState()

    class LiveDraftState(
        val isCancelChoiceAvailable: Boolean,
        val isRevertSequenceAvailable: Boolean,
        val sequence: List<ComplexItem>,
        val draftStage: Int,
        val groups: List<ComplexItem>,
        val filters: List<ComplexItem>,
        val types: List<ComplexItem>,
        val filterDescription: String,
        val radiantHeroes: List<ComplexItem>,
        val radiantBans: List<ComplexItem>,
        val direHeroes: List<ComplexItem>,
        val direBans: List<ComplexItem>,
        val heroesToSelect: List<ComplexItem>,
        val gameMode: GameModeType,
        val groupId: Int,
        val radiantName: String,
        val direName: String,
        val filterId: Int,
        val typeId: Int,
        val isAllPick: Boolean,
        ) : DraftViewState()

    class LiveGameState(
        val types: List<ComplexItem>,
        val heroes: List<ComplexItem>,
        val gameMode: GameModeType,
        val typeId: Int,
        val selectedResultHero: HeroCoreData,
        val items: List<ComplexItem>,
        val radiantName: String,
        val direName: String,
    ) : DraftViewState()

    class ResultState(
        val hasAnalysis: Boolean,
        val isCancelChoiceAvailable: Boolean,
        val gameMode: GameModeType,
        val isAllPick: Boolean,
        val typeId: Int,
        val types: List<ComplexItem>,
        val heroes: List<ComplexItem>,
        val selectedResultHero: HeroCoreData,
        val items: List<ComplexItem>,
        val radiantName: String,
        val direName: String,
    ) : DraftViewState()
}