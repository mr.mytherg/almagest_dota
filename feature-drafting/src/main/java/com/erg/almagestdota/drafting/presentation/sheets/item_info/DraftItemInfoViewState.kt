package com.erg.almagestdota.drafting.presentation.sheets.item_info

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.drafting.domain.models.DotaItemResult

internal sealed class DraftItemInfoViewState : ViewState() {

    class DefaultState(
        val item: DotaItemResult,
    ) : DraftItemInfoViewState()
}