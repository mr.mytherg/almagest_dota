package com.erg.almagestdota.drafting.domain.use_cases

import android.content.Context
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.drafting.R
import com.erg.almagestdota.drafting.domain.use_cases.GetRecommendationsUseCase.Companion.COUNTER_MIN_COEF
import com.erg.almagestdota.drafting.domain.use_cases.GetRecommendationsUseCase.Companion.MIN_COEF
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.GameStageData
import com.erg.almagestdota.local_storage.external.models.draft.GameStageType
import com.erg.almagestdota.local_storage.external.models.draft.HeroValues
import com.erg.almagestdota.local_storage.external.models.draft.RoleType
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import com.erg.almagestdota.local_storage.internal.data.models.draft.GameStageTeamData
import com.erg.almagestdota.local_storage.internal.data.models.draft.GameStagesTeamData
import com.erg.almagestdota.predictor.domain.use_cases.FilterHeroesByRolesUseCase
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class GameDataUseCase @Inject constructor(
    private val localStorage: ILocalStorageContract

) {
    operator fun invoke(
        radiant: List<HeroRoleData>,
        dire: List<HeroRoleData>
    ): GameStagesTeamData {
        val stages = listOf<GameStageType>(
            GameStageType.LANING,
            GameStageType.EARLY_GAME,
            GameStageType.MID_GAME,
            GameStageType.LATE_GAME,
        )
        val gameStagesData = GameStagesTeamData(
            radiantTeam = getDataForTeam(stages, radiant),
            direTeam = getDataForTeam(stages, dire)
        )
        localStorage.gameStagesData = gameStagesData
        return gameStagesData
    }

    private fun getDataForTeam(stages: List<GameStageType>, heroes: List<HeroRoleData>): List<GameStageTeamData> {
        val stagesData = mutableListOf<GameStageTeamData>()
        for (stage in stages) {
            val sideData = GameStageData.createEmpty()
            for (hero in heroes) {
                sideData.neutrals += getSummarizeRolesInfo(hero, stage) { gameStageData: GameStageData? -> gameStageData?.neutrals.orDefault() }
                sideData.dn += getSummarizeRolesInfo(hero, stage) { gameStageData: GameStageData? -> gameStageData?.dn.orDefault() }
                sideData.cs += getSummarizeRolesInfo(hero, stage) { gameStageData: GameStageData? -> gameStageData?.cs.orDefault() }
                sideData.heroDamage += getSummarizeRolesInfo(hero, stage) { gameStageData: GameStageData? -> gameStageData?.heroDamage.orDefault() }
                sideData.magicalDamage += getSummarizeRolesInfo(hero, stage) { gameStageData: GameStageData? -> gameStageData?.magicalDamage.orDefault() }
                sideData.towerDamage += getSummarizeRolesInfo(hero, stage) { gameStageData: GameStageData? -> gameStageData?.towerDamage.orDefault() }
                sideData.damageReceived += getSummarizeRolesInfo(hero, stage) { gameStageData: GameStageData? -> gameStageData?.damageReceived.orDefault() }
                sideData.healingAllies += getSummarizeRolesInfo(hero, stage) { gameStageData: GameStageData? -> gameStageData?.healingAllies.orDefault() }
                sideData.disableCount += getSummarizeRolesInfo(hero, stage) { gameStageData: GameStageData? -> gameStageData?.disableCount.orDefault() }
                sideData.disableDuration += getSummarizeRolesInfo(hero, stage) { gameStageData: GameStageData? -> gameStageData?.disableDuration.orDefault() }
                sideData.stunCount += getSummarizeRolesInfo(hero, stage) { gameStageData: GameStageData? -> gameStageData?.stunCount.orDefault() }
                sideData.stunDuration += getSummarizeRolesInfo(hero, stage) { gameStageData: GameStageData? -> gameStageData?.stunDuration.orDefault() }
                sideData.kills += getSummarizeRolesInfo(hero, stage) { gameStageData: GameStageData? -> gameStageData?.kills.orDefault() }
                sideData.assists += getSummarizeRolesInfo(hero, stage) { gameStageData: GameStageData? -> gameStageData?.assists.orDefault() }
            }
            stagesData.add(GameStageTeamData(stage, sideData))
        }
        return stagesData
    }

    private fun getSummarizeRolesInfo(hero: HeroRoleData, stage: GameStageType, function: (GameStageData?) -> Double): Double {
        var count = 0
        var value = 0.0
        hero.heroRoles.orEmpty().forEach { role ->
            count++
            value += function.invoke(getCurrentStageRole(role, stage, hero))
        }
        return getAverage(value, count)
    }

    private fun getCurrentStageRole(role: RoleType, stage: GameStageType, hero: HeroRoleData): GameStageData? {
        return when (stage) {
            GameStageType.LANING -> GlobalVars.heroesAsMap[hero.id]!!.laning.find { it.position == role }
            GameStageType.EARLY_GAME -> GlobalVars.heroesAsMap[hero.id]!!.early.find { it.position == role }
            GameStageType.MID_GAME -> GlobalVars.heroesAsMap[hero.id]!!.mid.find { it.position == role }
            GameStageType.LATE_GAME -> GlobalVars.heroesAsMap[hero.id]!!.late.find { it.position == role }
        }
    }

    private fun getAverage(param1: Double, param2: Int) =
        if (param1 == 0.0) 0.0 else (param1 / param2).round2()


}