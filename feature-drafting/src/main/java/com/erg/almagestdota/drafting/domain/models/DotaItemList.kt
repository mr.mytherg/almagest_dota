package com.erg.almagestdota.drafting.domain.models

import android.os.Parcelable
import androidx.annotation.StringRes
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DotaItemList(
    @StringRes val title: Int,
    var items: List<DotaItemResult>,
) : Parcelable