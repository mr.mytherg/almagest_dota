package com.erg.almagestdota.drafting.data

class OpenDotaPlayerResponse(
    val profile: OpenDotaPlayerProfile? = null
)
class OpenDotaPlayerProfile(
    val name: String? = null
)