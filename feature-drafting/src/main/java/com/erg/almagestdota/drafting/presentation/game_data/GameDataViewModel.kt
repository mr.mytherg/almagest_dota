package com.erg.almagestdota.drafting.presentation.game_data

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.extensions.round1
import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.*
import com.erg.almagestdota.drafting.R
import com.erg.almagestdota.drafting.domain.Interactor
import com.erg.almagestdota.local_storage.external.models.DraftAnalysis
import com.erg.almagestdota.local_storage.external.models.draft.*
import com.erg.almagestdota.local_storage.external.models.draft.main.*
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

internal class GameDataViewModel @Inject constructor(
    private val interactor: Interactor,
    private val stringProvider: StringProvider,
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data -> }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    fun obtainAction(action: GameDataViewActions) {
        when (action) {
            is GameDataViewActions.OnBackClick -> sendBackClickEvent()
            is GameDataViewActions.SelectGameStage -> {
                if (stateConfigurator.selectedGameStage == GameStageType.valueOf(action.key)) return
                stateConfigurator.selectedGameStage = GameStageType.valueOf(action.key)
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
        }
    }

    private fun sendBackClickEvent() {
        emit(viewEventMutable, ViewEvent.PopBackStack.Empty)
    }

    private inner class StateConfigurator {
        val draftModel = interactor.getDraftModel()
        val gameData = interactor.getGameStagesTeamData()
        var stages = listOf<GameStageType>(
            GameStageType.LANING,
            GameStageType.EARLY_GAME,
            GameStageType.MID_GAME,
            GameStageType.LATE_GAME,
        )
        var selectedGameStage: GameStageType = GameStageType.LANING
        var selectedStageIndex: Int = 0

        fun defineFragmentState(): GameDataViewState {
            val gameStagesComplex = complexList {
                for (index in stages.indices) {
                    val stage = stages[index]
                    filterItem(key = stage.name) {
                        title = stringProvider.getString(GameStageType.getId(stage))
                        isSelected = stage == selectedGameStage
                        if (isSelected)
                            selectedStageIndex = index
                    }
                }
            }

            val gameStagesDataComplex = complexList {
                draftReasonItem {
                    name = "${draftModel.radiantTeam.teamInfo.name.ifEmpty { "Radiant" }} vs ${draftModel.direTeam.teamInfo.name.ifEmpty { "Dire" }}"
                }
                val stage = stages[selectedStageIndex]
                val radiantStageData = gameData.radiantTeam.find { it.stageType == stage }!!.stageData
                val direStageData = gameData.direTeam.find { it.stageType == stage }!!.stageData
                gameDataItem {
                    name = "kills"
                    radiantCount = radiantStageData.kills.round2()
                    direCount = direStageData.kills.round2()
                }
                gameDataItem {
                    name = "assists"
                    radiantCount = radiantStageData.assists.round2()
                    direCount = direStageData.assists.round2()
                }
                gameDataItem {
                    name = "tower damage"
                    radiantCount = radiantStageData.towerDamage.round1()
                    direCount = direStageData.towerDamage.round1()
                }
                gameDataItem {
                    name = "hero damage"
                    radiantCount = radiantStageData.heroDamage.round1()
                    direCount = direStageData.heroDamage.round1()
                }
                gameDataItem {
                    name = "magical damage"
                    radiantCount = radiantStageData.magicalDamage.round1()
                    direCount = direStageData.magicalDamage.round1()
                }
                gameDataItem{
                    name = "durability"
                    radiantCount = radiantStageData.damageReceived.round1()
                    direCount = direStageData.damageReceived.round1()
                }
                gameDataItem{
                    name = "healing"
                    radiantCount = radiantStageData.healingAllies.round1()
                    direCount = direStageData.healingAllies.round1()
                }
                gameDataItem {
                    name = "disableDuration"
                    radiantCount = radiantStageData.disableDuration.round1()
                    direCount = direStageData.disableDuration.round1()
                }
                gameDataItem{
                    name = "stunDuration"
                    radiantCount = radiantStageData.stunDuration.round1()
                    direCount = direStageData.stunDuration.round1()
                }
                gameDataItem{
                    name = "last hits"
                    radiantCount = radiantStageData.cs.round2()
                    direCount = direStageData.cs.round2()
                }
                gameDataItem {
                    name = "denies"
                    radiantCount = radiantStageData.dn.round2()
                    direCount = direStageData.dn.round2()
                }
                gameDataItem {
                    name = "neutrals"
                    radiantCount = radiantStageData.neutrals.round2()
                    direCount = direStageData.neutrals.round2()
                }
            }

            return GameDataViewState.DefaultState(
                gameStagesComplex,
                gameStagesDataComplex
            )
        }
    }

    class Factory @Inject constructor(
        private val interactor: Interactor,
        private val stringProvider: StringProvider,
    ) : ViewModelProvider.Factory {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>): T =
            GameDataViewModel(interactor, stringProvider) as T
    }
}