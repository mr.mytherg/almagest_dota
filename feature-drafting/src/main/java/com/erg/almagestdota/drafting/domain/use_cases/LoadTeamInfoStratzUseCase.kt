package com.erg.almagestdota.drafting.domain.use_cases

import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.drafting.data.IStratzGraphqlApi
import com.erg.almagestdota.local_storage.external.models.draft.main.MemberInfo
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamInfo
import javax.inject.Inject

class LoadTeamInfoStratzUseCase @Inject constructor(
    private val stratz: IStratzGraphqlApi,
) {
    suspend operator fun invoke(teamId: Long): TeamInfo {
        val team = stratz.getTeamsInfo(
            "{teams(teamIds:[$teamId]){id,name,logo,members(take:5){steamAccountId,steamAccount{proSteamAccount{name,position}}}}}"
        ).data?.teams?.firstOrNull()

        return TeamInfo(
            id = teamId,
            name = team?.name.orDefault(),
//            imageLink = team?.logo.orDefault(),
            members = team?.members.orEmpty().map {
                it?.steamAccountId.orDefault()
            }
        )
    }
}