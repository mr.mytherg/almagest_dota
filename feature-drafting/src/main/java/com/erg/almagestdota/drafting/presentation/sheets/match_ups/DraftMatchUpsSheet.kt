package com.erg.almagestdota.drafting.presentation.sheets.match_ups

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.drafting.R
import com.erg.almagestdota.drafting.databinding.DraftMatchUpsSheetBinding
import com.erg.almagestdpta.core_navigation.external.helpers.BottomSheetFragment
import com.erg.almagestdpta.core_navigation.external.helpers.navHostFragmentManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
internal class DraftMatchUpsSheet : BottomSheetFragment(R.layout.draft_match_ups_sheet), BackPressListener {

    private val args by navArgs<DraftMatchUpsSheetArgs>()

    companion object {
        const val TAG = "DraftMatchUpsSheet"
    }

    private val binding by viewBinding { DraftMatchUpsSheetBinding.bind(requireContentView()) }
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()

    @Inject
    lateinit var viewModelFactory: DraftMatchUpsViewModel.Factory
    private val viewModel by viewModels<DraftMatchUpsViewModel> {
        DraftMatchUpsViewModel.provideFactory(
            matchUps = args.matchUps,
            assistedFactory = viewModelFactory
        )
    }

    override fun onBackPressed() {
        onClose()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
    }

    override fun onBottomSheetStateChanged(bottomSheet: View, newState: Int) {
        super.onBottomSheetStateChanged(bottomSheet, newState)
        if (newState == BottomSheetBehavior.STATE_HIDDEN) {
            onClose()
        }
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun onClose() {
        val fm = navHostFragmentManager()
        navController.popBackStack()
        fm.setFragmentResult(TAG, bundleOf())
    }

    private inner class ViewsConfigurator {

        fun initStartState() = with(binding) {
            rvMatchUps.setHasFixedSize(true)
        }

        fun renderState(state: DraftMatchUpsViewState) {
            when (state) {
                is DraftMatchUpsViewState.DefaultState -> renderDefaultState(state)
            }
        }

        private fun renderDefaultState(state: DraftMatchUpsViewState.DefaultState) = with(binding) {
            rvMatchUps.adapter = DraftMatchUpsAdapter().apply {
                submitList(state.matchUps.toList())
            }
        }
    }
}