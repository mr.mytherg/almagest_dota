package com.erg.almagestdpta.core_navigation.external.helpers

import android.content.Context
import android.util.AttributeSet
import androidx.navigation.NavDestination
import androidx.navigation.Navigator
import com.erg.almagestdpta.core_navigation.R

@NavDestination.ClassType(BottomSheetFragment::class)
class BottomSheetDestination constructor(
    bottomSheetNavigator: Navigator<out BottomSheetDestination>,
) : NavDestination(bottomSheetNavigator) {

    private var _className: String? = null
    val className: String
        get() {
            checkNotNull(_className) { "Fragment class was not set" }
            return _className as String
        }

    override fun onInflate(context: Context, attrs: AttributeSet) {
        super.onInflate(context, attrs)
        val array = context.resources.obtainAttributes(attrs, R.styleable.BottomSheetNavigator)
        val className = array.getString(R.styleable.BottomSheetNavigator_android_name)
        if (className != null) {
            _className = className
        }
        array.recycle()
    }

    override fun equals(other: Any?): Boolean {
        if (other == null || other !is BottomSheetDestination) return false
        return super.equals(other) && className == other.className
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + className.hashCode()
        return result
    }
}