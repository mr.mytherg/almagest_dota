package com.erg.almagestdpta.core_navigation.external.helpers

import android.content.Context
import android.net.Uri
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.NavOptions
import androidx.navigation.Navigator
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdpta.core_navigation.R
import java.lang.IllegalArgumentException
import java.net.URLDecoder
import java.net.URLEncoder

fun AppCompatActivity.obtainNavController(containerId: Int): NavController {
    return obtainInternalNavController(
        context = this,
        fragmentManager = supportFragmentManager,
        containerId = containerId
    )
}

fun NavController.navigateViaScreenRoute(
    screen: Screen<*>,
    extras: Navigator.Extras? = null,
) {
    try {
        val options = NavOptions.Builder()
            .setEnterAnim(R.anim.slide_in_right)
            .setExitAnim(R.anim.slide_out_left)
            .setPopEnterAnim(R.anim.slide_in_left)
            .setPopExitAnim(R.anim.slide_out_right)
            .build()
        when (screen.route) {
            is String -> navigate(Uri.parse(screen.route as String), options, extras)
            is NavDirections -> navigate(screen.route as NavDirections, options)
            else -> throw IllegalArgumentException("Unsupported route type (${screen.route})")
        }
    } catch (e: Exception) {
        Log.e("ERROR", e.message.orDefault())
    }
}

private fun obtainInternalNavController(
    context: Context,
    fragmentManager: FragmentManager,
    containerId: Int
): NavController {
    // support for <bottomsheet/> destinations
    val bottomSheetNavigator = BottomSheetNavigator(
        context = context,
        fragmentManager = fragmentManager,
        containerId = containerId
    )
    return fragmentManager.findFragmentById(containerId)!!
        .findNavController().apply {
            navigatorProvider.addNavigator(bottomSheetNavigator)
        }
}

fun Fragment.navHostFragmentManager(): FragmentManager {
    if (this is BottomSheetFragment) {
        return parentFragmentManager
    }
    return findNavHostFragment().parentFragmentManager
}

fun String.encodeForDeeplinkArg(): String = URLEncoder.encode(this, "UTF-8")
fun String.decodeForDeeplinkArg(): String = URLDecoder.decode(this, "UTF-8")

fun Fragment.findNavHostFragment(): NavHostFragment {
    if (this is BottomSheetFragment) {
        return parentFragment?.childFragmentManager?.fragments.orEmpty()
            .find { it is NavHostFragment } as NavHostFragment
    }
    return (parentFragment as? NavHostFragment) ?: parentFragment!!.findNavHostFragment()
}