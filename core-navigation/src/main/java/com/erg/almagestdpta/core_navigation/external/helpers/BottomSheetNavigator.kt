package com.erg.almagestdpta.core_navigation.external.helpers

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit
import androidx.fragment.app.commitNow
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavOptions
import androidx.navigation.Navigator

@Navigator.Name("bottomsheet")
class BottomSheetNavigator(
    private val context: Context,
    private val fragmentManager: FragmentManager,
    private val containerId: Int
) : Navigator<BottomSheetDestination>() {

    override fun navigate(
        entries: List<NavBackStackEntry>,
        navOptions: NavOptions?,
        navigatorExtras: Extras?
    ) {
        if (fragmentManager.isStateSaved) {
            Log.i(TAG, "Ignoring navigate() call: FragmentManager has already saved its state")
            return
        }
        for (entry in entries) {
            navigate(entry)
        }
    }

    private fun navigate(entry: NavBackStackEntry) {
        var className = (entry.destination as BottomSheetDestination).className
        if (className[0] == '.') {
            className = context.packageName + className
        }

        val fragment = fragmentManager.fragmentFactory
            .instantiate(context.classLoader, className)
        fragment.arguments = entry.arguments

        fragmentManager.commit {
            val visibleFragment = fragmentManager.fragments.last()
            if (visibleFragment is BottomSheetFragment) {
                hide(visibleFragment)
            }
            add(containerId, fragment)
            setPrimaryNavigationFragment(fragment)
            setReorderingAllowed(true)
        }
        state.push(entry)
    }

    override fun popBackStack(popUpTo: NavBackStackEntry, savedState: Boolean) {
        if (fragmentManager.isStateSaved) {
            Log.i(TAG, "Ignoring popBackStack() call: FragmentManager has already saved its state")
            return
        }

        val fragments = fragmentManager.fragments

        // commitNow обязателен, исправляет баг с множественным вызовом popBackStack
        fragmentManager.commitNow {
            val visibleFragment = fragments[fragments.lastIndex]
            if (visibleFragment is BottomSheetFragment) {
                remove(visibleFragment)
            }
            val invisibleFragment = fragments[fragments.lastIndex - 1]
            if (invisibleFragment is BottomSheetFragment) {
                show(invisibleFragment)
                // раскрываем предыдущий фрагмент т.к он скорее всего скрыт
                // и при его появлении не будет вызван метод onViewCreated
                invisibleFragment.expand()
            }
        }
        state.pop(popUpTo, savedState)
    }

    override fun createDestination(): BottomSheetDestination {
        return BottomSheetDestination(this)
    }

    override fun onSaveState(): Bundle? = null
    override fun onRestoreState(savedState: Bundle) = Unit

    companion object {
        private const val TAG = "BottomSheetNavigator"
    }
}