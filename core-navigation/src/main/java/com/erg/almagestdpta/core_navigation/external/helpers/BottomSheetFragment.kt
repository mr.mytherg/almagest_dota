package com.erg.almagestdpta.core_navigation.external.helpers

import android.graphics.Point
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import androidx.annotation.LayoutRes
import androidx.core.content.getSystemService
import androidx.core.view.updateLayoutParams
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import com.erg.almagestdota.base.R
import com.erg.almagestdota.base.external.extensions.dpToPx
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlin.math.roundToInt

abstract class BottomSheetFragment(@LayoutRes contentLayoutId: Int) : Fragment(contentLayoutId) {

    private val bottomSheetSlideCallback = object : BottomSheetBehavior.BottomSheetCallback() {
        override fun onStateChanged(bottomSheet: View, newState: Int) = Unit
        override fun onSlide(bottomSheet: View, slideOffset: Float) {
            onBottomSheetSlide(bottomSheet, slideOffset)
        }
    }
    private val bottomSheetStateCallback = object : BottomSheetBehavior.BottomSheetCallback() {
        override fun onSlide(bottomSheet: View, slideOffset: Float) = Unit
        override fun onStateChanged(bottomSheet: View, newState: Int) {
            onBottomSheetStateChanged(bottomSheet, newState)
        }
    }

    private var containerLayout: View? = null
    private var contentLayout: View? = null
    private var bottomSheet: ViewGroup? = null
    private var scrimView: ImageView? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        containerLayout = inflater.inflate(R.layout.layout_bottom_sheet, container, false)
        contentLayout = super.onCreateView(inflater, container, savedInstanceState)
        bottomSheet = containerLayout?.findViewById(R.id.bottomSheet)
        scrimView = containerLayout?.findViewById(R.id.scrimView)

        containerLayout?.setOnClickListener {
            collapseInternalImpl()
        }
        contentLayout?.updatePadding(top = requireContext().dpToPx(16))
        contentLayout?.updateLayoutParams {
            height = (calculateScreenHeight() * calculateScreenOffset()).roundToInt()
        }
        bottomSheet?.updateLayoutParams {
            height = (calculateScreenHeight() * calculateScreenOffset()).roundToInt()
        }
        return containerLayout?.also {
            bottomSheet?.addView(contentLayout)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bottomSheet?.let { bottomSheet ->
            val behavior = createBottomSheetBehavior(bottomSheet)
            behavior.addBottomSheetCallback(bottomSheetSlideCallback)
            behavior.addBottomSheetCallback(bottomSheetStateCallback)
            behavior.skipCollapsed = true
            behavior.isHideable = true
        }
        view.post(::expandInternalImpl)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        bottomSheet = null
        scrimView = null
        containerLayout = null
        contentLayout = null
    }

    /**
     * Всегда возвращает актуальное смещение bottom sheet, был-ли это вызов expand/collapse или же
     * пользователь сам смахнул его пальцем.
     */
    open fun onBottomSheetSlide(bottomSheet: View, slideOffset: Float) {
        scrimView?.alpha = slideOffset // анимация затемнения
    }

    /**
     * Метод будет вызван ТОЛЬКО при закрытии bottom sheet жестом (пальцем).
     * Методы expand/collapse не затронут данный коллбек.
     */
    open fun onBottomSheetStateChanged(bottomSheet: View, newState: Int) = Unit

    /**
     * Если по какой-то причине понадобится поведение отличное от стандартного,
     * переопределите этот метод и добавьте нужные параметры в BottomSheetBehavior.
     */
    open fun createBottomSheetBehavior(bottomSheet: View) = BottomSheetBehavior.from(bottomSheet)

    /**
     * Вычисление доступной высоты экрана.
     */
    open fun calculateScreenHeight(): Int {
        val point = Point()
        requireContext().getSystemService<WindowManager>()!!
            .defaultDisplay.getSize(point)
        return point.y
    }

    /**
     * Максимальная раскрываемая высота bottom sheet.
     */
    open fun calculateScreenOffset(): Float = 0.85f

    fun expand(block: () -> Unit = {}) {
        bottomSheet?.let { bottomSheet ->
            val behavior = createBottomSheetBehavior(bottomSheet)
            val callback = object : BottomSheetBehavior.BottomSheetCallback() {
                override fun onSlide(bottomSheet: View, slideOffset: Float) = Unit
                override fun onStateChanged(bottomSheet: View, newState: Int) {
                    if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                        behavior.addBottomSheetCallback(bottomSheetStateCallback)
                        behavior.removeBottomSheetCallback(this)
                        block.invoke()
                    }
                }
            }
            behavior.removeBottomSheetCallback(bottomSheetStateCallback)
            behavior.addBottomSheetCallback(callback)
            val pendingState = BottomSheetBehavior.STATE_EXPANDED
            if (behavior.state == pendingState) {
                callback.onStateChanged(bottomSheet, pendingState)
            } else {
                behavior.state = pendingState
            }
        }
    }

    fun collapse(block: () -> Unit = {}) {
        bottomSheet?.let { bottomSheet ->
            val behavior = createBottomSheetBehavior(bottomSheet)
            val callback = object : BottomSheetBehavior.BottomSheetCallback() {
                override fun onSlide(bottomSheet: View, slideOffset: Float) = Unit
                override fun onStateChanged(bottomSheet: View, newState: Int) {
                    if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                        behavior.addBottomSheetCallback(bottomSheetStateCallback)
                        behavior.removeBottomSheetCallback(this)
                        block.invoke()
                    }
                }
            }
            behavior.removeBottomSheetCallback(bottomSheetStateCallback)
            behavior.addBottomSheetCallback(callback)
            val pendingState = BottomSheetBehavior.STATE_HIDDEN
            if (behavior.state == pendingState) {
                callback.onStateChanged(bottomSheet, pendingState)
            } else {
                behavior.state = pendingState
            }
        }
    }

    fun requireContentView(): View {
        return contentLayout!!
    }

    /**
     * Вызов аналогичен #expand(), однако отличается тем, что слушатели
     * продолжат работать во время операции.
     */
    private fun expandInternalImpl() {
        bottomSheet?.let(::createBottomSheetBehavior)?.run {
            val pendingState = BottomSheetBehavior.STATE_EXPANDED
            if (state == pendingState) {
                onBottomSheetStateChanged(bottomSheet!!, state)
            } else {
                state = pendingState
            }
        }
    }

    /**
     * Вызов аналогичен #collapse(), однако отличается тем, что слушатели
     * продолжат работать во время операции.
     */
    private fun collapseInternalImpl() {
        bottomSheet?.let(::createBottomSheetBehavior)?.run {
            val pendingState = BottomSheetBehavior.STATE_HIDDEN
            if (state == pendingState) {
                onBottomSheetStateChanged(bottomSheet!!, state)
            } else {
                state = pendingState
            }
        }
    }
}