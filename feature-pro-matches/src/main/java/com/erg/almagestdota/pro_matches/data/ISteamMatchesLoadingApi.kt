package com.erg.almagestdota.pro_matches.data

import com.erg.almagestdota.pro_matches.data.response.get_live_league_games.ContentRaw
import retrofit2.http.GET
import retrofit2.http.Query

interface ISteamMatchesLoadingApi {

    companion object {
        private const val STEAM_API = "/IDOTA2Match_570"
        private const val STEAM_API_KEY = "47A353711F576A647ED1AB6C8B25B1D7" // todo в gradle
    }

     @GET("$STEAM_API/GetLiveLeagueGames/v1/")
     suspend fun getLiveLeagueGames(
         @Query("key") key: String = STEAM_API_KEY,
     ): ContentRaw
}