package com.erg.almagestdota.pro_matches.domain.model.pro_matches_history

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
internal enum class SteamDotaMatchLobbyType(val lobbyType: Int) : Parcelable {
    UNKNOWN(0),
    PRACTISE(1),
    TOURNAMENT(2);

    companion object {
        fun byLobbyType(value: Int?): SteamDotaMatchLobbyType {
            return values().firstOrNull { it.lobbyType == value } ?: UNKNOWN
        }
    }
}