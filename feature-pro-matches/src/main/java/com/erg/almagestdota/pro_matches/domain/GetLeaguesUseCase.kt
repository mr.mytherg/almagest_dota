package com.erg.almagestdota.pro_matches.domain

import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.leagues.League
import com.erg.almagestdota.pro_matches.data.IOpenDotaApi
import com.erg.almagestdota.pro_matches.data.response.get_league_listing.OpenDotaLeagueRaw
import javax.inject.Inject

class GetLeaguesUseCase @Inject constructor(
    private val api: IOpenDotaApi,
    private val mapperToLeague: OpenDotaLeagueRaw.MapperToLeague,
    private val localStorageContract: ILocalStorageContract,
) {
    suspend operator fun invoke(): List<League> {
        val leagues = api.getLeaguesListing().essentialMap(mapperToLeague).sortedBy { it.id }

        localStorageContract.setLeagues(leagues)
        return leagues
    }
}