package com.erg.almagestdota.pro_matches.domain

import android.os.CountDownTimer
import android.util.Log
import com.erg.almagestdota.base.external.extensions.launchOnIO
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.local_storage.external.models.leagues.LeagueTierType
import com.erg.almagestdota.local_storage.external.models.live.LeagueGame
import com.erg.almagestdota.local_storage.external.models.live.LeagueSeriesType
import com.erg.almagestdota.local_storage.external.models.live.PlayerAccountInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.predictor.domain.use_cases.LoadHawkDraftUseCase
import com.erg.almagestdota.predictor.domain.use_cases.PredictionUseCase.Companion.NORMAL_MEMBERS_MINIMUM
import com.erg.almagestdota.predictor.domain.use_cases.models.HawkSeries
import com.erg.almagestdota.pro_matches.data.ISteamMatchesLoadingApi
import com.erg.almagestdota.pro_matches.data.response.get_live_league_games.ContentRaw
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Singleton

@OptIn(DelicateCoroutinesApi::class)
@Singleton
class GetLiveLeagueGamesUseCase @Inject constructor(
    private val steamApi: ISteamMatchesLoadingApi,
    private val mapperToLiveContent: ContentRaw.MapperToLiveContent,
    private val loadHawkDraftUseCase: LoadHawkDraftUseCase,
    private val getLeaguesUseCase: GetLeaguesUseCase,
) {
    companion object {
        private const val TIMER_INTERVAL = 45 * 1000L // 70 * 1000L
        private const val COUNT_DOWN_INTERVAL = 1000L
    }

    private val timerStateMutable = MutableStateFlow(TIMER_INTERVAL)
    private val mutableStateFlow: MutableStateFlow<List<LeagueGame>?> = MutableStateFlow(null)
    private var timer: CountDownTimer? = null
    private val loadingStateMutable = MutableStateFlow<Loading?>(Loading.Enabled)

    init {
        loadLeagues()
    }

    suspend fun getMatches(): StateFlow<List<LeagueGame>?> {
        if (mutableStateFlow.value.orEmpty().isEmpty())
            invoke()
        return mutableStateFlow.asStateFlow()
    }
    fun getTimer() = timerStateMutable.asStateFlow()
    fun getLoader() = loadingStateMutable.asStateFlow()

    private fun initTimer() {
        GlobalScope.launch(Dispatchers.Main) {
            timer?.cancel()

            timer = object : CountDownTimer(
                TIMER_INTERVAL,
                COUNT_DOWN_INTERVAL
            ) {
                override fun onTick(millisUntilFinished: Long) {
                    timerStateMutable.value = millisUntilFinished
                }

                override fun onFinish() {
                    updateMatches()
                    stopTimer()
                    initTimer()
                }
            }
            timer?.start()
        }
    }

    fun loadLeagues() {
        GlobalScope.launchOnIO(
            loader = null,
            action = {
                loadingStateMutable.value = Loading.Enabled
                getLeaguesUseCase.invoke()
            },
            onSuccess = {
                invoke()
                initTimer()
                loadingStateMutable.value = Loading.Disabled
            },
            onError = {
                loadingStateMutable.value = null
            }
        )
    }

    private fun updateMatches() {
        GlobalScope.launchOnIO(
            loader = null,
            action = {
                loadingStateMutable.value = Loading.Enabled
                invoke()
            },
            onSuccess = {
                Log.e("BACKGROUNDALMA", "matches loaded")
                loadingStateMutable.value = Loading.Disabled
            },
            onError = {
                loadingStateMutable.value = Loading.Disabled
            }
        )
    }

    private fun stopTimer() {
        timer?.cancel()
        timer = null
    }

    suspend operator fun invoke() {
        val matches = steamApi.getLiveLeagueGames().essentialMap(mapperToLiveContent)
            .filter {
                it.leagueTierType != LeagueTierType.AMATEUR &&
                    it.radiantTeam?.teamId.orDefault() > 0L && it.direTeam?.teamId.orDefault() > 0L &&
                    isPlayersNormal(it.players) && isPlayersCalibrated(it.players)
            }
            .sortedWith(
                compareByDescending<LeagueGame> { it.leagueTierType == LeagueTierType.PREMIUM }
                    .thenByDescending { it.scoreboard.duration.orDefault() }
                    .thenByDescending {
                        it.scoreboard.radiant.picks.count { it.id > 0 } +
                            it.scoreboard.dire.picks.count { it.id > 0 } +
                            it.scoreboard.radiant.bans.count { it.id > 0 } +
                            it.scoreboard.dire.bans.count { it.id > 0 }
                    }
            )
        if (matches.isNotEmpty()) {
            val hawkMatches = loadHawkDraftUseCase.invoke()
            if (hawkMatches.isNotEmpty())
                for (match in matches) {
                    try {
                        val hawkMatch = hawkMatches.find {
                            it.team1.name.equals(match.radiantTeam?.teamName.orDefault(),true) ||
                            it.team2.name.equals(match.direTeam?.teamName.orDefault(),true) ||
                            it.team1.name.equals(match.direTeam?.teamName.orDefault(),true) ||
                            it.team2.name.equals(match.radiantTeam?.teamName.orDefault(),true)
                        } ?: continue
                        val isTeam1Radiant: Boolean = isTeam1Radiant(match, hawkMatch)

                        match.seriesType = LeagueSeriesType.byHawkSeriesType(hawkMatch.seriesType)
                        match.radiantSeriesWins = if (isTeam1Radiant) hawkMatch.team1Wins ?: 0 else hawkMatch.team2Wins ?: 0
                        match.direSeriesWins = if (!isTeam1Radiant) hawkMatch.team1Wins ?: 0 else hawkMatch.team2Wins ?: 0
                        val currentMatch = hawkMatch.matches.firstOrNull() ?: continue
                        if (currentMatch.heroes.isEmpty() ||
                            match.scoreboard.radiant.picks.count { it.id > 0L } == GlobalVars.mainSettings.getPickSizeCM() &&
                            match.scoreboard.dire.picks.count { it.id > 0L } == GlobalVars.mainSettings.getPickSizeCM()
                        )
                            continue
                        val radiantHeroes = currentMatch.heroes.filter { it.isRadiant }
                        val direHeroes = currentMatch.heroes.filter { !it.isRadiant }
                        match.scoreboard.radiant.picks = radiantHeroes.map { hawkHero ->
                            GlobalVars.mainSettings.heroes.find { it.name == hawkHero.name }!!
                        }
                        match.scoreboard.dire.picks = direHeroes.map { hawkHero ->
                            GlobalVars.mainSettings.heroes.find { it.name == hawkHero.name }!!
                        }
                    } catch (e: Exception) {
                        Log.e("LIVEMATCH", e.message.orDefault())
                    }
                }
        }

        mutableStateFlow.value = matches
    }

    private fun isTeam1Radiant(match: LeagueGame, hawkMatch: HawkSeries): Boolean {
        return hawkMatch.team1.name.equals(match.radiantTeam?.teamName.orDefault(),true) ||
            hawkMatch.team2.name.equals(match.direTeam?.teamName.orDefault(),true)
    }

    private fun isPlayersCalibrated(players: List<PlayerAccountInfo>): Boolean {
        return players.filter { it.team <= 1 }.count {
            GlobalVars.players[it.accountId]?.matchCount.orDefault() >= GlobalVars.CALIBRATION_MATCH_COUNT - 3
        } >= NORMAL_MEMBERS_MINIMUM
    }

    private fun isPlayersNormal(players: List<PlayerAccountInfo>): Boolean {
        return players.filter { it.team <= 1 }.count {
            GlobalVars.players[it.accountId]?.name.orDefault().isNotEmpty()
        } >= NORMAL_MEMBERS_MINIMUM
    }
}