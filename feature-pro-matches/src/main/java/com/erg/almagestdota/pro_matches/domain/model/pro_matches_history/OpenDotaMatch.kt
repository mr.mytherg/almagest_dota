package com.erg.almagestdota.pro_matches.domain.model.pro_matches_history

import android.os.Parcelable
import com.erg.almagestdota.local_storage.external.models.leagues.League
import kotlinx.android.parcel.Parcelize

@Parcelize
internal data class OpenDotaMatch(
    val matchId: Long,
    val radiantTeamName: String,
    val direTeamName: String,
    val league: League,
) : Parcelable