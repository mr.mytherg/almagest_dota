package com.erg.almagestdota.pro_matches.presentation.history

internal sealed class RecentMatchesViewActions {

    object OnBackClick : RecentMatchesViewActions()
    class OpenDraft(val key: String) : RecentMatchesViewActions()
    object UploadNewMatches : RecentMatchesViewActions()
    object LoadMore : RecentMatchesViewActions()
    object Retry : RecentMatchesViewActions()
    object ChangeMatchesCheckbox : RecentMatchesViewActions()
    object ChangeLeaguesCheckbox : RecentMatchesViewActions()
}