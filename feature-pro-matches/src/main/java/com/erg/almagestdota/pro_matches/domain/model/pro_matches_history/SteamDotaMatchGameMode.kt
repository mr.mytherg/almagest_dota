package com.erg.almagestdota.pro_matches.domain.model.pro_matches_history

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
internal enum class SteamDotaMatchGameMode(val gameMode: Int) : Parcelable {
    CAPTAINS_MODE(2),
    UNKNOWN(0);

    companion object {
        fun byGameMode(value: Int?): SteamDotaMatchGameMode {
            return values().firstOrNull { it.gameMode == value } ?: UNKNOWN
        }
    }
}