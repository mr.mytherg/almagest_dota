package com.erg.almagestdota.pro_matches.data.response.get_live_league_games

import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.NotRequired
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.base.external.ui.secondsToMatchTimerFormat
import com.erg.almagestdota.local_storage.external.models.live.FactionInfo
import com.erg.almagestdota.local_storage.external.models.live.GameScoreboard
import com.google.gson.annotations.SerializedName
import javax.inject.Inject

 data class GameScoreboardRaw(
     @NotRequired
     @SerializedName("duration")
     val duration: Double? = null,
     @NotRequired
     @SerializedName("roshan_respawn_timer")
     val roshanRespawnTimer: Int? = null,
     @NotRequired
     @SerializedName("radiant")
     val radiant: FactionInfoRaw? = null,
     @NotRequired
     @SerializedName("dire")
     val dire: FactionInfoRaw? = null,
 ) {
    class MapperToLiveGameScoreboard @Inject constructor(
        private val mapperToFactionInfo: FactionInfoRaw.MapperToLiveFactionInfo
    ) : EssentialMapper<GameScoreboardRaw, GameScoreboard>() {
        override fun transform(raw: GameScoreboardRaw): GameScoreboard {
            return GameScoreboard(
                duration = raw.duration.orDefault(),
                durationReadable = raw.duration.orDefault().toLong().secondsToMatchTimerFormat(),
                roshanRespawnTimer = raw.roshanRespawnTimer.orDefault(),
                radiant = raw.radiant?.essentialMap(mapperToFactionInfo) ?: FactionInfo(),
                dire = raw.dire?.essentialMap(mapperToFactionInfo) ?: FactionInfo(),
            )
        }
    }
}