package com.erg.almagestdota.pro_matches.data.response.get_live_league_games

import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.NotRequired
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData
import com.google.gson.annotations.SerializedName
import javax.inject.Inject

 data class SelectedHeroRaw(
     @NotRequired
     @SerializedName("hero_id")
     val heroId: Int? = null,
 ) {
    class MapperToLiveSelectedHero @Inject constructor() : EssentialMapper<SelectedHeroRaw, HeroCoreData>() {
        override fun transform(raw: SelectedHeroRaw): HeroCoreData {
            val currentHero = GlobalVars.heroesAsMap[raw.heroId.orDefault()] ?: HeroCoreData.createNullHero()
            return HeroCoreData(
                id = raw.heroId.orDefault(),
                imageLink = currentHero.imageLink.orDefault(),
                name = currentHero.name.orDefault(),
            )
        }
    }
}