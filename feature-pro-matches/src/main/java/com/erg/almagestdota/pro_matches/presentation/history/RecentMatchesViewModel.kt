package com.erg.almagestdota.pro_matches.presentation.history

import androidx.lifecycle.ViewModel
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.extensions.launchOnIO
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.ui.millisToTextAgo
import com.erg.almagestdota.base.external.ui.secondsToMatchTimerFormat
import com.erg.almagestdota.base.external.viewEvent.NotificationStatus
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.complexadapter.*
import com.erg.almagestdota.drafting.presentation.DraftScreen
import com.erg.almagestdota.local_storage.external.models.draft.GameModeType
import com.erg.almagestdota.local_storage.external.models.draft.main.*
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import com.erg.almagestdota.pro_matches.R
import com.erg.almagestdota.pro_matches.domain.Interactor
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import java.lang.Exception
import java.lang.IllegalStateException
import javax.inject.Inject

@HiltViewModel
internal class RecentMatchesViewModel @Inject constructor(
    private val stringProvider: StringProvider,
    private val interactor: Interactor,
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data ->
        when (resultKey) {
        }
    }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow<RecentMatchesViewState?>(null)
    val viewState = viewStateMutable.asStateFlow()

    private val loadingStateMutable = MutableStateFlow<Loading>(Loading.Enabled)
    val loadingState = loadingStateMutable.asStateFlow()

    init {
        loadInitial()
    }

    private fun loadInitial() {
        launchOnIO(
            loader = loadingStateMutable,
            action = {
                loadMoreMatches(interactor.getDocumentId(), true)
            },
            onSuccess = {},
            onError = ::handleError
        )
    }

    fun obtainAction(action: RecentMatchesViewActions) {
        when (action) {
            is RecentMatchesViewActions.ChangeMatchesCheckbox -> {
                stateConfigurator.isPredictionsOnly = !stateConfigurator.isPredictionsOnly
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is RecentMatchesViewActions.Retry -> {
                loadInitial()
            }
            is RecentMatchesViewActions.ChangeLeaguesCheckbox -> {
                stateConfigurator.needToShowLeagues = !stateConfigurator.needToShowLeagues
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is RecentMatchesViewActions.OpenDraft -> {
                val dotaMatch =
                    stateConfigurator.dotaMatches.find { it.matchId == action.key.toLong() }
                        ?: return

                interactor.setDraftModel(convertToDraftModel(dotaMatch))
                val screen = DraftScreen(RecentMatchesFragmentDirections.toDraft())
                emit(viewEventMutable, ViewEvent.Navigation(screen))
            }
            is RecentMatchesViewActions.OnBackClick -> sendBackClickEvent()
            is RecentMatchesViewActions.UploadNewMatches -> {
                launchOnIO(
                    loader = loadingStateMutable,
                    action = { interactor.loadNewMatches() },
                    onSuccess = {
                        loadMoreMatches(interactor.getDocumentId(), true)
                    },
                    onError = ::handleError
                )
            }
            is RecentMatchesViewActions.LoadMore -> {
                if (stateConfigurator.wasAllMatchesLoaded) {
                    showAllMatchesWasLoadedSnack()
                } else {
                    loadMoreMatches(stateConfigurator.documentId, false)
                }
            }
        }
    }

    private fun convertToDraftModel(dotaMatch: DotaMatch): DraftModel {
        return DraftModel(
            needToLoadPubMatches = true,
            isAllPick = false,
            gameMode = GameModeType.LIVE_FINISHED,
            sequence = if (dotaMatch.hasRadiantFirstPick) GlobalVars.mainSettings.sequenceCM else reverseSequence(GlobalVars.mainSettings.sequenceCM),
            radiantTeam = TeamDraft(
                picks = dotaMatch.radiantHeroes.toMutableList(),
                bans = dotaMatch.radiantBans.toMutableList(),
                teamInfo = (GlobalVars.teams[dotaMatch.radiantTeamId] ?: TeamInfo(dotaMatch.radiantTeamId)).apply {
                    members = getTeamMembers(dotaMatch.radiantHeroes)
                }
            ),
            direTeam = TeamDraft(
                picks = dotaMatch.direHeroes.toMutableList(),
                bans = dotaMatch.direBans.toMutableList(),
                teamInfo = (GlobalVars.teams[dotaMatch.direTeamId] ?: TeamInfo(dotaMatch.direTeamId)).apply {
                    members = getTeamMembers(dotaMatch.direHeroes)
                }
            ),
        )
    }

    private fun reverseSequence(_sequence: String): String {
        var sequence = ""
        for (ch in _sequence) {
            sequence += when (ch) {
                '0' -> '1'
                '1' -> '0'
                '2' -> '3'
                else -> '2'
            }
        }
        return sequence
    }

    private fun getTeamMembers(players: List<PlayerInfo>): List<Long> {
        return players.map { it.playerId }
    }

    private fun loadMoreMatches(documentId: Int, isFirst: Boolean) {
        launchOnIO(
            loader = loadingStateMutable,
            action = {
                interactor.loadStoredMatches(documentId)
            },
            onSuccess = {
                if (it.isEmpty()) {
                    stateConfigurator.wasAllMatchesLoaded = true
                    showAllMatchesWasLoadedSnack()
                } else {
                    stateConfigurator.documentId = documentId - 1
                    if (isFirst)
                        stateConfigurator.dotaMatches = it
                    else
                        stateConfigurator.dotaMatches = it.plus(stateConfigurator.dotaMatches)
                    viewStateMutable.value = stateConfigurator.defineFragmentState()
                }
            },
            onError = ::handleError
        )
    }

    private fun showAllMatchesWasLoadedSnack() {
        val text = stringProvider.getString(R.string.all_loaded)
        emit(viewEventMutable, ViewEvent.AlmagestSnackbar(text, NotificationStatus.SUCCESS))
    }

    private fun sendBackClickEvent() {
        emit(viewEventMutable, ViewEvent.PopBackStack.Empty)
    }

    private fun handleError(e: Exception) {
        if (e is IllegalStateException) {
            val text = stringProvider.getString(R.string.up_to_date)
            emit(viewEventMutable, ViewEvent.AlmagestSnackbar(text, NotificationStatus.SUCCESS))
        } else {
            val text = e.message
            emit(viewEventMutable, ViewEvent.AlmagestSnackbar(text, NotificationStatus.ERROR))
        }
    }

    private inner class StateConfigurator {
        var dotaMatches = listOf<DotaMatch>()
        var documentId: Int = 0
        var wasAllMatchesLoaded: Boolean = false
        var isPredictionsOnly = true
        var needToShowLeagues = false

        fun defineFragmentState(): RecentMatchesViewState {

            return if (dotaMatches.isEmpty()) {
                RecentMatchesViewState.RetryState
            } else if (needToShowLeagues) {
                val groups = dotaMatches.groupBy { it.leagueId }
                val filteredLeagues = if (isPredictionsOnly) {
                    groups.filter {
                        it.value.find { !it.isInvalid && it.willRadiantWin != null } != null
                    }
                } else {
                    groups
                }
                getLeaguesState(filteredLeagues)
            } else {
                val filteredMatches = if (isPredictionsOnly) {
                    dotaMatches.filter {
                        !it.isInvalid && it.willRadiantWin != null
                    }
                } else {
                    dotaMatches
                }
                getMatchesState(filteredMatches)
            }
        }

        private fun getLeaguesState(groups: Map<Long, List<DotaMatch>>): RecentMatchesViewState {
            return RecentMatchesViewState.LeaguesState(
                complexList {
                    for ((leagueId, matches) in groups) {
                        val tournament = GlobalVars.leagues.find { it.id == leagueId }
                        leagueHistoryItem(key = leagueId.toString()) {
                            name = tournament?.name.orDefault().ifEmpty { leagueId.toString() }
                            successCount = getSuccessCount(matches)
                            failCount = getFailCount(matches)
                            unpredictableCount = getUnpredictableCount(matches)
                        }
                    }
                    leagueHistoryItem(key = "0") {
                        name = stringProvider.getString(R.string.overall)
                        successCount = getSuccessCount(groups.values.flatMap { it })
                        failCount = getFailCount(groups.values.flatMap { it })
                        unpredictableCount = getUnpredictableCount(groups.values.flatMap { it })
                    }
                }.reversed(),
                isPredictionsOnly,
                needToShowLeagues
            )
        }

        private fun getUnpredictableCount(matches: List<DotaMatch>): Int {
            return matches.count { it.isInvalid || it.willRadiantWin == null }
        }

        private fun getFailCount(matches: List<DotaMatch>): Int {
            return matches.count { !it.isInvalid && it.willRadiantWin != null && wasPredictionRight(it) == false }
        }

        private fun getSuccessCount(matches: List<DotaMatch>): Int {
            return matches.count { !it.isInvalid && it.willRadiantWin != null && wasPredictionRight(it) == true }
        }

        private fun getMatchesState(filteredMatches: List<DotaMatch>): RecentMatchesViewState {
            return RecentMatchesViewState.MatchesState(
                complexList {
                    for (match in filteredMatches) {
                        val tournament = GlobalVars.leagues.find { it.id == match.leagueId }
                        playedMatchItem(key = match.matchId.toString()) {
                            val pair = (match.startTime + match.duration).millisToTextAgo()
                            time =
                                stringProvider.getQuantityString(
                                    pair.first,
                                    pair.second.toInt(),
                                    pair.second.toInt()
                                )
                            title =
                                tournament?.name.orDefault().ifEmpty { match.leagueId.toString() }
                            radiantScore = match.radiantScore
                            direScore = match.direScore
                            durationReadable = match.duration.secondsToMatchTimerFormat()
                            hasRadiantWon = match.hasRadiantWon
                            wasPredictionRight = wasPredictionRight(match)
                            radiantName = GlobalVars.teams[match.radiantTeamId]?.name.orDefault()
                                .ifEmpty {
                                    match.radiantTeamId.toString()
                                }
                            direName = GlobalVars.teams[match.direTeamId]?.name.orDefault()
                                .ifEmpty {
                                    match.direTeamId.toString()
                                }
                            radiantHeroes = getComplexPicks(match.radiantHeroes)
                            direHeroes = getComplexPicks(match.direHeroes)
                        }
                    }
                }.reversed(),
                isPredictionsOnly,
                needToShowLeagues
            )
        }

        private fun wasPredictionRight(match: DotaMatch): Boolean? {
            return if (match.willRadiantWin != null) {
                match.willRadiantWin!! && match.hasRadiantWon ||
                    !match.willRadiantWin!! && !match.hasRadiantWon
            } else {
                null
            }
        }

        private fun getComplexPicks(players: List<PlayerInfo>): List<ComplexItem> {
            return complexList {
                for (player in players) {
                    val hero = player.heroId.getHeroById()
                    itemDefault {
                        url = hero.imageLink
                        title = hero.name
                        hasBlackout = false
                        hasPlaceholder = true
                    }
                }
            }
        }
    }
}