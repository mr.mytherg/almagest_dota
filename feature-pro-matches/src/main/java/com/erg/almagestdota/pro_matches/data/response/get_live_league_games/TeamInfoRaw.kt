package com.erg.almagestdota.pro_matches.data.response.get_live_league_games

import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.NotRequired
import com.erg.almagestdota.local_storage.external.models.live.LiveTeamInfo
import com.google.gson.annotations.SerializedName
import javax.inject.Inject

 data class TeamInfoRaw(
     @NotRequired
     @SerializedName("team_name")
     val teamName: String? = null,
     @NotRequired
     @SerializedName("team_id")
     val teamId: Long? = null,
     @NotRequired
     @SerializedName("complete")
     val complete: Boolean? = null,
 ) {
    class MapperToLiveTeamInfo @Inject constructor() : EssentialMapper<TeamInfoRaw, LiveTeamInfo>() {
        override fun transform(raw: TeamInfoRaw) = LiveTeamInfo(
            teamId = raw.teamId.orDefault(),
            teamName = raw.teamName.orDefault().trim(),
            isTeamComplete = raw.complete.orDefault(),
        )
    }
}