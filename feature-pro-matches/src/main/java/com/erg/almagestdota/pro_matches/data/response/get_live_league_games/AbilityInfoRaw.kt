package com.erg.almagestdota.pro_matches.data.response.get_live_league_games

import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.NotRequired
import com.erg.almagestdota.local_storage.external.models.live.AbilityInfo
import com.google.gson.annotations.SerializedName
import javax.inject.Inject

 data class AbilityInfoRaw(
     @NotRequired
     @SerializedName("ability_level")
     val abilityLevel: Int? = null,
     @NotRequired
     @SerializedName("ability_id")
     val abilityId: Int? = null,
 ) {
    class MapperToLiveAbilityInfo @Inject constructor() : EssentialMapper<AbilityInfoRaw, AbilityInfo>() {
        override fun transform(raw: AbilityInfoRaw): AbilityInfo {
            return AbilityInfo(
                abilityLevel = raw.abilityLevel.orDefault(),
                abilityId = raw.abilityId.orDefault(),
            )
        }
    }
}