package com.erg.almagestdota.pro_matches.presentation.live

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.complexadapter.viewHolders.matches.LiveMatchViewHolder

class LiveMatchesAdapter(
    private val complexListener: ComplexListener? = null,
) : ListAdapter<ComplexItem.LiveMatchItem, LiveMatchViewHolder>(diffCallback) {

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<ComplexItem.LiveMatchItem>() {
            override fun areItemsTheSame(
                oldItem: ComplexItem.LiveMatchItem,
                newItem: ComplexItem.LiveMatchItem
            ): Boolean {
                return oldItem.key == newItem.key
            }

            override fun areContentsTheSame(
                oldItem: ComplexItem.LiveMatchItem,
                newItem: ComplexItem.LiveMatchItem
            ): Boolean {
                return oldItem == newItem
            }
        }
    }

    private val pool = RecyclerView.RecycledViewPool()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LiveMatchViewHolder {
        return LiveMatchViewHolder.create(parent, pool, complexListener)
    }

    override fun onBindViewHolder(holder: LiveMatchViewHolder, position: Int) {
        holder.bind(currentList[position])
    }
}