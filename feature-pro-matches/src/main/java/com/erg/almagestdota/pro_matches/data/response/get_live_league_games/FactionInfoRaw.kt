package com.erg.almagestdota.pro_matches.data.response.get_live_league_games

import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.NotRequired
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.local_storage.external.models.live.FactionInfo
import com.google.gson.annotations.SerializedName
import javax.inject.Inject

 data class FactionInfoRaw(
     @NotRequired
     @SerializedName("score")
     val score: Int? = null,
     @NotRequired
     @SerializedName("tower_state")
     val towerState: Long? = null,
     @NotRequired
     @SerializedName("barracks_state")
     val barracksState: Long? = null,

     @NotRequired
     @SerializedName("picks")
     val picks: List<SelectedHeroRaw>? = null,
     @NotRequired
     @SerializedName("bans")
     val bans: List<SelectedHeroRaw>? = null,
     @NotRequired
     @SerializedName("players")
     val players: List<PlayerInfoRaw>? = null,
 ) {
    class MapperToLiveFactionInfo @Inject constructor(
        private val mapperToLiveSelectedHero: SelectedHeroRaw.MapperToLiveSelectedHero,
        private val mapperToLivePlayerInfo: PlayerInfoRaw.MapperToLivePlayerInfo,
        private val mapperToLiveAbilityInfo: AbilityInfoRaw.MapperToLiveAbilityInfo,
    ) : EssentialMapper<FactionInfoRaw, FactionInfo>() {
        override fun transform(raw: FactionInfoRaw): FactionInfo {
            return FactionInfo(
                score = raw.score.orDefault(),
                towerState = raw.towerState.orDefault(),
                barracksState = raw.barracksState.orDefault(),
                picks = raw.picks.orEmpty().map { it.essentialMap(mapperToLiveSelectedHero) },
                bans = raw.bans.orEmpty().map { it.essentialMap(mapperToLiveSelectedHero) },
                players = raw.players.orEmpty().map { it.essentialMap(mapperToLivePlayerInfo) },
            )
        }
    }
}