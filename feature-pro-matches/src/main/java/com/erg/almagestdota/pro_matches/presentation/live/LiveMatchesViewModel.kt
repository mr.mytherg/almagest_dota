package com.erg.almagestdota.pro_matches.presentation.live

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.complexadapter.*
import com.erg.almagestdota.drafting.presentation.DraftScreen
import com.erg.almagestdota.local_storage.external.models.draft.GameModeType
import com.erg.almagestdota.local_storage.external.models.draft.main.*
import com.erg.almagestdota.local_storage.external.models.live.FactionInfo
import com.erg.almagestdota.local_storage.external.models.live.LeagueGame
import com.erg.almagestdota.local_storage.external.models.live.SteamPlayerInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.pro_matches.domain.GetLiveLeagueGamesUseCase
import com.erg.almagestdota.pro_matches.domain.Interactor
import com.erg.almagestdota.pro_matches.presentation.history.RecentMatchesScreen
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import com.google.gson.Gson
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
internal class LiveMatchesViewModel @Inject constructor(
    private val stringProvider: StringProvider,
    private val getLiveLeagueGamesUseCase: GetLiveLeagueGamesUseCase,
    private val interactor: Interactor,
    private val gson: Gson,
) : ViewModel() {


    private val stateConfigurator = StateConfigurator()
    private val viewEventMutable = MutableResultFlow { resultKey, data -> }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow<LiveMatchesViewState?>(null)
    val viewState = viewStateMutable.asStateFlow()
    val timerState = getLiveLeagueGamesUseCase.getTimer()
    val loadingState: MutableStateFlow<Loading> = MutableStateFlow(Loading.Enabled)

    init {
        viewModelScope.launch {
            getLiveLeagueGamesUseCase.getMatches().filterNotNull().onEach {
                stateConfigurator.games = it
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }.launchIn(viewModelScope)
        }
        getLiveLeagueGamesUseCase.getLoader().onEach {
            if (it == null) {
                loadingState.value = Loading.Disabled
                stateConfigurator.isLoadingSuccess = false
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            } else {
                stateConfigurator.isLoadingSuccess = true
                loadingState.value = it
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
        }.launchIn(viewModelScope)
    }

    fun obtainAction(action: LiveMatchesViewActions) {
        when (action) {
            is LiveMatchesViewActions.LoadLeagues -> getLiveLeagueGamesUseCase.loadLeagues()
            is LiveMatchesViewActions.OpenPredictor -> {
                val match =
                    stateConfigurator.games.find { action.key.toLong() == it.matchId } ?: return
                val hasMatchStarted = match.hasMatchStarted()
                val draftModel = DraftModel(
                    needToLoadPubMatches = true,
                    isAllPick = false,
                    gameMode = if (hasMatchStarted) GameModeType.LIVE_IN_GAME else GameModeType.LIVE_IN_DRAFT,
                    sequence = GlobalVars.mainSettings.sequenceCM,
                    radiantTeam = getTeamDraft(match, true, hasMatchStarted),
                    direTeam = getTeamDraft(match, false, hasMatchStarted),
                )
                interactor.setDraftModel(draftModel)
                val screen = DraftScreen(LiveMatchesFragmentDirections.toDraft())
                emit(viewEventMutable, ViewEvent.Navigation(screen))
            }
            is LiveMatchesViewActions.OpenHistory -> {
                val screen =
                    RecentMatchesScreen(LiveMatchesFragmentDirections.toRecentMatchesFragment())
                emit(viewEventMutable, ViewEvent.Navigation(screen))
            }
        }
    }

    private fun getTeamDraft(
        match: LeagueGame,
        isRadiant: Boolean,
        hasMatchStarted: Boolean
    ): TeamDraft {
        val team = if (isRadiant) {
            (GlobalVars.teams[match.radiantTeam?.teamId] ?: TeamInfo(
                id = match.radiantTeam?.teamId.orDefault(),
            )).apply {
                name = match.radiantTeam?.teamName.orDefault()
            }
        } else {
            (GlobalVars.teams[match.direTeam?.teamId] ?: TeamInfo(
                id = match.direTeam?.teamId.orDefault(),
            )).apply {
                name = match.direTeam?.teamName.orDefault()
            }
        }
        val members = match.players.filter { it.team == if (isRadiant) 0 else 1 }
        team.members = members.map {
            it.accountId
        }

        val bans = if (isRadiant) {
            match.scoreboard.radiant.bans.map { it.id }.toMutableList()
        } else {
            match.scoreboard.dire.bans.map { it.id }.toMutableList()
        }

        val picks = if (hasMatchStarted) {
            if (isRadiant) {
                match.scoreboard.radiant.players.map { PlayerInfo(it.id, it.accountId) }
                    .toMutableList()
            } else {
                match.scoreboard.dire.players.map { PlayerInfo(it.id, it.accountId) }
                    .toMutableList()
            }
        } else {
            if (isRadiant) {
                match.scoreboard.radiant.picks.map { PlayerInfo(it.id, 0L) }.toMutableList()
            } else {
                match.scoreboard.dire.picks.map { PlayerInfo(it.id, 0L) }.toMutableList()
            }
        }

        return TeamDraft(
            picks = picks,
            bans = bans,
            teamInfo = team
        )
    }

    private inner class StateConfigurator {
        var games = listOf<LeagueGame>()
        var isLoadingSuccess = false

        fun defineFragmentState(): LiveMatchesViewState {
            return when {
                !isLoadingSuccess -> LiveMatchesViewState.Retry
                games.isEmpty() -> LiveMatchesViewState.EmptyList
                else -> {
                    val complexList = complexList {
                        for (game in games) {
                            liveMatchItem(key = game.matchId.toString()) {
                                title = game.leagueName
                                radiantSeriesScore = game.radiantSeriesWins
                                direSeriesScore = game.direSeriesWins
                                radiantScore = game.scoreboard.radiant.score
                                direScore = game.scoreboard.dire.score
                                durationReadable = game.scoreboard.durationReadable
                                seriesType = game.seriesType.name
                                radiantName = game.radiantTeam?.teamName.orDefault()
                                direName = game.direTeam?.teamName.orDefault()
                                radiantHeroes = getHeroesComplex(game.scoreboard.radiant, game.scoreboard.duration)
                                direHeroes = getHeroesComplex(game.scoreboard.dire, game.scoreboard.duration)
                            }
                        }
                    }
                    LiveMatchesViewState.FilledList(complexList)
                }
            }
        }

        private fun getHeroesComplex(faction: FactionInfo, duration: Double): List<ComplexItem> {
            val heroes = if (duration > 0) faction.players else faction.picks

            return complexList {
                for (hero in heroes) {
                    if (hero is SteamPlayerInfo) {
                        heroScoreItem(key = hero.id.toString()) {
                            url = hero.imageLink
                            title = hero.name
                            kills = hero.kills
                            deaths = hero.death
                            assists = hero.assists
                        }
                    } else {
                        itemDefault(key = hero.id.toString()) {
                            url = hero.imageLink
                            title = hero.name
                        }
                    }
                }
            }
        }
    }
}