package com.erg.almagestdota.pro_matches.data.response.get_live_league_games

import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.NotRequired
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.local_storage.external.models.live.LeagueGame
import com.google.gson.annotations.SerializedName
import javax.inject.Inject

 data class ResultRaw(
     @NotRequired
     @SerializedName("games")
     val games: List<LeagueGameRaw>? = null,
 ) {
    class MapperToLiveResult @Inject constructor(
        private val mapperToLiveLeagueGame: LeagueGameRaw.MapperToLiveLeagueGame
    ) : EssentialMapper<ResultRaw, List<LeagueGame>>() {
        override fun transform(raw: ResultRaw): List<LeagueGame> {
            return raw.games.orEmpty().map {
                it.essentialMap(mapperToLiveLeagueGame)
            }
        }
    }
}