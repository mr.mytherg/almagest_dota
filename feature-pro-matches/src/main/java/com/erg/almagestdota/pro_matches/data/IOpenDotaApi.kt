package com.erg.almagestdota.pro_matches.data

import com.erg.almagestdota.pro_matches.data.response.get_league_listing.OpenDotaLeagueRaw
import retrofit2.http.GET

 interface IOpenDotaApi {

    @GET("leagues")
    suspend fun getLeaguesListing(): List<OpenDotaLeagueRaw>
}