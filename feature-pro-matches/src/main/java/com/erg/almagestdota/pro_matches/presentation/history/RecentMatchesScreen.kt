package com.erg.almagestdota.pro_matches.presentation.history

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen

class RecentMatchesScreen(
    route: NavDirections
) : Screen<NavDirections>(
    route = route,
    requestKey = RecentMatchesFragment.TAG
)