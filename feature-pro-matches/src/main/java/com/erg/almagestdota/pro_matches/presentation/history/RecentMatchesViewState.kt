package com.erg.almagestdota.pro_matches.presentation.history

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem

internal sealed class RecentMatchesViewState : ViewState() {

    class MatchesState(
        val list: List<ComplexItem>,
        val isPredictionsOnly: Boolean,
        val needToShowLeagues: Boolean
    ) : RecentMatchesViewState()

    object RetryState : RecentMatchesViewState()

    class LeaguesState(
        val list: List<ComplexItem>,
        val isPredictionsOnly: Boolean,
        val needToShowLeagues: Boolean
    ) : RecentMatchesViewState()
}