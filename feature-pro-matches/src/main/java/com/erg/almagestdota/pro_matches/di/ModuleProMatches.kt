package com.erg.almagestdota.pro_matches.di

import com.erg.almagestdota.network.external.annotations.RetrofitClientOpendota
import com.erg.almagestdota.pro_matches.data.*
import com.erg.almagestdota.pro_matches.data.IOpenDotaApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
internal object ModuleProMatches {

    @Provides
    @Singleton
    fun provideOpenDotaApi(@RetrofitClientOpendota retrofit: Retrofit): IOpenDotaApi {
        return retrofit.create(IOpenDotaApi::class.java)
    }
}