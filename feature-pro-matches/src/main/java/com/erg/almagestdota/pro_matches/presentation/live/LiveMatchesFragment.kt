package com.erg.almagestdota.pro_matches.presentation.live

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.millisToMinutesTimerFormat
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdota.pro_matches.R
import com.erg.almagestdota.pro_matches.databinding.LiveMatchesFragmentBinding
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
internal class LiveMatchesFragment : Fragment(R.layout.live_matches_fragment) {

    companion object {
        const val TAG = "LiveMatchesFragment"
    }

    private val binding by viewBinding(LiveMatchesFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewModel by hiltNavGraphViewModels<LiveMatchesViewModel>(R.id.graphProMatches)
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle, Lifecycle.State.CREATED).filterNotNull().onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.loadingState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach {
            binding.loadingView.isVisible = it is Loading.Enabled
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.timerState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { millisUntilFinished ->
            binding.tvTimer.text = requireContext().millisToMinutesTimerFormat(millisUntilFinished)
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private inner class ViewsConfigurator {
        private val liveMatchAdapter = LiveMatchesAdapter(
            complexListener = object : ComplexListener {
                override fun onClick(item: ComplexItem) {
                    viewModel.obtainAction(LiveMatchesViewActions.OpenPredictor(item.key))
                }
            }
        )

        fun initStartState() = with(binding) {
            tvOpenHistory.clicksWithDebounce {
                viewModel.obtainAction(LiveMatchesViewActions.OpenHistory)
            }
            tvRetry.clicksWithDebounce {
                viewModel.obtainAction(LiveMatchesViewActions.LoadLeagues)
            }
            tvTitle.text = getString(R.string.live_toolbar_title)
            if (rvLive.adapter == null) {
                rvLive.adapter = liveMatchAdapter
            }
        }

        fun renderState(state: LiveMatchesViewState) {
            when (state) {
                is LiveMatchesViewState.FilledList -> renderFilledListState(state)
                is LiveMatchesViewState.EmptyList -> renderEmptyListState()
                is LiveMatchesViewState.Retry -> renderRetryState()
            }
        }

        private fun renderRetryState() = with(binding) {
            rvLive.isVisible = false
            tvLiveEmpty.isVisible = false
            tvSubtitle.isVisible = false
            tvTip.isVisible = false
            tvTimer.isVisible = false
            tvOpenHistory.isVisible = false
            tvRetry.isVisible = true
        }
        private fun renderEmptyListState() = with(binding) {
            tvRetry.isVisible = false
            tvOpenHistory.isVisible = true
            rvLive.isVisible = false
            tvLiveEmpty.isVisible = true
            tvLiveEmpty.text = getString(R.string.live_empty)
            tvSubtitle.isVisible = false
            tvTip.isVisible = false
            tvTimer.isVisible = true
        }

        private fun renderFilledListState(state: LiveMatchesViewState.FilledList) = with(binding) {
            tvRetry.isVisible = false
            tvOpenHistory.isVisible = true
            tvTimer.isVisible = true
            tvTip.isVisible = true
            rvLive.isVisible = true
            tvLiveEmpty.isVisible = false
            tvSubtitle.isVisible = true
            tvSubtitle.text = getString(R.string.live_toolbar_subtitle, state.matches.size)
            liveMatchAdapter.submitList(state.matches as List<ComplexItem.LiveMatchItem>)
        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            navController.navigateViaScreenRoute(screen)
        }
    }
}