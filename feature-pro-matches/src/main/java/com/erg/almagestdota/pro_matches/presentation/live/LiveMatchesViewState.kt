package com.erg.almagestdota.pro_matches.presentation.live

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem

internal sealed class LiveMatchesViewState : ViewState() {

    object EmptyList : LiveMatchesViewState()
    object Retry : LiveMatchesViewState()
    class FilledList(val matches: List<ComplexItem>) : LiveMatchesViewState()
}