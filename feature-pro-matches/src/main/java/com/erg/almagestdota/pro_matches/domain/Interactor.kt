package com.erg.almagestdota.pro_matches.domain

import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.main.DraftModel
import com.erg.almagestdota.local_storage.external.models.leagues.League
import com.erg.almagestdota.local_storage.external.models.live.LeagueGame
import com.erg.almagestdota.network.external.firebase.FirebaseRepositoryContract
import com.erg.almagestdota.settings.domain.use_cases.LoadDocumentMatchesUseCase
import com.erg.almagestdota.settings.domain.use_cases.LoadNewMatchesUseCase
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject

internal class Interactor @Inject constructor(
    private val localStorageContract: ILocalStorageContract,
    private val loadNewMatchesUseCase: LoadNewMatchesUseCase,
    private val loadDocumentMatchesUseCase: LoadDocumentMatchesUseCase,
    private val firebaseRepository: FirebaseRepositoryContract
) {

    suspend fun getDocumentId(): Int {
        return firebaseRepository.getImportantSettings().lastDocument
    }

    suspend fun loadStoredMatches(documentId: Int) = loadDocumentMatchesUseCase.invoke(documentId)
    suspend fun loadNewMatches() = loadNewMatchesUseCase.invoke(firebaseRepository.getImportantSettings())

    fun setDraftModel(draftModel: DraftModel) {
        localStorageContract.draftModel = draftModel
    }
}