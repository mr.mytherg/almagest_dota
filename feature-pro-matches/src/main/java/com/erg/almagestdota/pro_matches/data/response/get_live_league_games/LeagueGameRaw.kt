package com.erg.almagestdota.pro_matches.data.response.get_live_league_games

import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.NotRequired
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.local_storage.external.models.leagues.findLeagueByBinaryMethod
import com.erg.almagestdota.local_storage.external.models.live.GameScoreboard
import com.erg.almagestdota.local_storage.external.models.live.LeagueGame
import com.erg.almagestdota.local_storage.external.models.live.LeagueSeriesType
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.google.gson.annotations.SerializedName
import javax.inject.Inject

 data class LeagueGameRaw(
     @NotRequired
     @SerializedName("players")
     val players: List<PlayerAccountInfoRaw>? = null,
     @NotRequired
     @SerializedName("radiant_team")
     val radiantTeam: TeamInfoRaw? = null,
     @NotRequired
     @SerializedName("dire_team")
     val direTeam: TeamInfoRaw? = null,
     @NotRequired
     @SerializedName("lobby_id")
     val lobbyId: Long? = null,
     @NotRequired
     @SerializedName("match_id")
     val matchId: Long? = null,
     @NotRequired
     @SerializedName("spectators")
     val spectators: Int? = null,
     @NotRequired
     @SerializedName("league_id")
     val leagueId: Long? = null,
     @NotRequired
     @SerializedName("league_node_id")
     val leagueNodeId: Long? = null,
     @NotRequired
     @SerializedName("stream_delay_s")
     val streamDelayS: Int? = null,
     @NotRequired
     @SerializedName("radiant_series_wins")
     val radiantSeriesWins: Int? = null,
     @NotRequired
     @SerializedName("dire_series_wins")
     val direSeriesWins: Int? = null,
     @NotRequired
     @SerializedName("series_type")
     val seriesType: Int? = null,
     @NotRequired
     @SerializedName("scoreboard")
     val scoreboard: GameScoreboardRaw? = null,
 ) {
    class MapperToLiveLeagueGame @Inject constructor(
        private val mapperToLiveTeamInfo: TeamInfoRaw.MapperToLiveTeamInfo,
        private val mapperToLiveGameScoreboard: GameScoreboardRaw.MapperToLiveGameScoreboard,
        private val mapperToLivePlayerAccountInfo: PlayerAccountInfoRaw.MapperToLivePlayerAccountInfo,
    ) : EssentialMapper<LeagueGameRaw, LeagueGame>() {
        override fun transform(raw: LeagueGameRaw): LeagueGame {
            val currentLeague = GlobalVars.leagues.findLeagueByBinaryMethod(raw.leagueId.orDefault())
            return LeagueGame(
                leagueName = currentLeague.name,
                leagueTierType = currentLeague.tier,
                players = raw.players.orEmpty().map { it.essentialMap(mapperToLivePlayerAccountInfo) },
                matchId = raw.matchId.orDefault(),
                lobbyId = raw.lobbyId.orDefault(),
                radiantSeriesWins = raw.radiantSeriesWins.orDefault(),
                direSeriesWins = raw.direSeriesWins.orDefault(),
                leagueId = raw.leagueId.orDefault(),
                leagueNodeId = raw.leagueNodeId.orDefault(),
                scoreboard = raw.scoreboard?.essentialMap(mapperToLiveGameScoreboard) ?: GameScoreboard(),
                streamDelayS = raw.streamDelayS.orDefault(),
                spectators = raw.spectators.orDefault(),
                seriesType = LeagueSeriesType.bySteamSeriesType(raw.seriesType),
                radiantTeam = raw.radiantTeam?.essentialMap(mapperToLiveTeamInfo),
                direTeam = raw.direTeam?.essentialMap(mapperToLiveTeamInfo),
            )
        }
    }
}