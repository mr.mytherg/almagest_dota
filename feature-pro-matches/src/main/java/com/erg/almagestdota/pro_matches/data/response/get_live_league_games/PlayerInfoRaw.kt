package com.erg.almagestdota.pro_matches.data.response.get_live_league_games

import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.NotRequired
import com.erg.almagestdota.local_storage.external.models.live.SteamPlayerInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData
import com.google.gson.annotations.SerializedName
import javax.inject.Inject

 data class PlayerInfoRaw(
     @NotRequired
     @SerializedName("player_slot")
     val playerSlot: Int? = null,
     @NotRequired
     @SerializedName("account_id")
     val accountId: Long? = null,
     @NotRequired
     @SerializedName("hero_id")
     val heroId: Int? = null,
     @NotRequired
     @SerializedName("kills")
     val kills: Int? = null,
     @NotRequired
     @SerializedName("death")
     val death: Int? = null,
     @NotRequired
     @SerializedName("assists")
     val assists: Int? = null,
     @NotRequired
     @SerializedName("last_hits")
     val lastHits: Int? = null,
     @NotRequired
     @SerializedName("denies")
     val denies: Int? = null,
     @NotRequired
     @SerializedName("gold")
     val gold: Int? = null,
     @NotRequired
     @SerializedName("level")
     val level: Int? = null,
     @NotRequired
     @SerializedName("gold_per_min")
     val goldPerMin: Int? = null,
     @NotRequired
     @SerializedName("xp_per_min")
     val xpPerMin: Int? = null,
     @NotRequired
     @SerializedName("ultimate_state")
     val ultimateState: Int? = null,
     @NotRequired
     @SerializedName("ultimate_cooldown")
     val ultimateCooldown: Int? = null,
     @NotRequired
     @SerializedName("item0")
     val item0: Int? = null,
     @NotRequired
     @SerializedName("item1")
     val item1: Int? = null,
     @NotRequired
     @SerializedName("item2")
     val item2: Int? = null,
     @NotRequired
     @SerializedName("item3")
     val item3: Int? = null,
     @NotRequired
     @SerializedName("item4")
     val item4: Int? = null,
     @NotRequired
     @SerializedName("item5")
     val item5: Int? = null,
     @NotRequired
     @SerializedName("respawn_timer")
     val respawnTimer: Int? = null,
     @NotRequired
     @SerializedName("position_x")
     val positionX: Double? = null,
     @NotRequired
     @SerializedName("position_y")
     val positionY: Double? = null,
     @NotRequired
     @SerializedName("net_worth")
     val netWorth: Int? = null,
 ) {
    class MapperToLivePlayerInfo @Inject constructor() : EssentialMapper<PlayerInfoRaw, SteamPlayerInfo>() {
        override fun transform(raw: PlayerInfoRaw): SteamPlayerInfo {
            val currentHero = GlobalVars.heroesAsMap[raw.heroId.orDefault()] ?: HeroCoreData.createNullHero()
            return SteamPlayerInfo(
                imageLink = currentHero.imageLink.orDefault(),
                name = currentHero.name.orDefault(),
                playerSlot = raw.playerSlot.orDefault(),
                accountId = raw.accountId.orDefault(),
                kills = raw.kills.orDefault(),
                death = raw.death.orDefault(),
                assists = raw.assists.orDefault(),
                lastHits = raw.lastHits.orDefault(),
                denies = raw.denies.orDefault(),
                gold = raw.gold.orDefault(),
                level = raw.level.orDefault(),
                goldPerMin = raw.goldPerMin.orDefault(),
                xpPerMin = raw.xpPerMin.orDefault(),
                ultimateState = raw.ultimateState.orDefault(),
                ultimateCooldown = raw.ultimateCooldown.orDefault(),
                item0 = raw.item0.orDefault(),
                item1 = raw.item1.orDefault(),
                item2 = raw.item2.orDefault(),
                item3 = raw.item3.orDefault(),
                item4 = raw.item4.orDefault(),
                item5 = raw.item5.orDefault(),
                id = raw.heroId.orDefault(),
                respawnTimer = raw.respawnTimer.orDefault(),
                positionX = raw.positionX.orDefault(),
                positionY = raw.positionY.orDefault(),
                netWorth = raw.netWorth.orDefault(),
            )
        }
    }
}