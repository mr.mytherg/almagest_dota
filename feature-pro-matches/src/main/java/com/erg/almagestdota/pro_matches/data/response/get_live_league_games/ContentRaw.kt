package com.erg.almagestdota.pro_matches.data.response.get_live_league_games

import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.NotRequired
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.local_storage.external.models.live.LeagueGame
import com.google.gson.annotations.SerializedName
import javax.inject.Inject

 data class ContentRaw(
     @NotRequired
     @SerializedName("result")
     val result: ResultRaw? = null,
 ) {
    class MapperToLiveContent @Inject constructor(
        private val mapperToLiveResult: ResultRaw.MapperToLiveResult
    ) : EssentialMapper<ContentRaw, List<LeagueGame>>() {
        override fun transform(raw: ContentRaw): List<LeagueGame> {
            return raw.result?.essentialMap(mapperToLiveResult).orEmpty()
        }
    }
}