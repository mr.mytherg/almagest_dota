package com.erg.almagestdota.pro_matches.presentation.history

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.complexadapter.ComplexAdapter
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdota.pro_matches.R
import com.erg.almagestdota.pro_matches.databinding.RecentMatchesFragmentBinding
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.live_matches_fragment.*
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
internal class RecentMatchesFragment : Fragment(R.layout.recent_matches_fragment),
    BackPressListener {

    companion object {
        const val TAG = "RecentMatchesFragment"
    }

    private val binding by viewBinding(RecentMatchesFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    private val viewModel by hiltNavGraphViewModels<RecentMatchesViewModel>(
        R.id.graphProMatches
    )

    override fun onBackPressed() {
        viewModel.obtainAction(RecentMatchesViewActions.OnBackClick)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
        viewModel.viewEvent.setResultListener(this)
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).filterNotNull()
            .onEach { state ->
                viewsConfigurator.renderState(state)
            }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.loadingState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { state ->
            binding.loadingView.isVisible = state is Loading.Enabled
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private inner class ViewsConfigurator {

        private val matchesAdapter by lazy {
            RecentMatchesAdapter(
                complexListener = object : ComplexListener {
                    override fun onClick(item: ComplexItem) {
                        viewModel.obtainAction(RecentMatchesViewActions.OpenDraft(item.key))
                    }
                }
            )
        }

        private val leaguesAdapter by lazy { ComplexAdapter() }

        fun initStartState() = with(binding) {
            ivBack.clicksWithDebounce {
                viewModel.obtainAction(RecentMatchesViewActions.OnBackClick)
            }
            ivRefresh.clicksWithDebounce {
                viewModel.obtainAction(RecentMatchesViewActions.UploadNewMatches)
            }
            tvLoadMore.clicksWithDebounce {
                viewModel.obtainAction(RecentMatchesViewActions.LoadMore)
            }
            cbOnlyWithPredictions.clicksWithDebounce {
                viewModel.obtainAction(RecentMatchesViewActions.ChangeMatchesCheckbox)
            }
            cbLeagues.clicksWithDebounce {
                viewModel.obtainAction(RecentMatchesViewActions.ChangeLeaguesCheckbox)
            }
            tvRetry.clicksWithDebounce {
                viewModel.obtainAction(RecentMatchesViewActions.Retry)
            }
        }

        fun renderState(state: RecentMatchesViewState) {
            when (state) {
                is RecentMatchesViewState.MatchesState -> renderMatchesState(state)
                is RecentMatchesViewState.LeaguesState -> renderLeaguesState(state)
                is RecentMatchesViewState.RetryState -> renderRetryState()
            }
        }

        private fun renderRetryState() = with(binding) {
            tvRetry.isVisible = true
            clInfo.isVisible = false
        }

        private fun renderMatchesState(state: RecentMatchesViewState.MatchesState) = with(binding) {
            tvRetry.isVisible = false
            clInfo.isVisible = true
            cbOnlyWithPredictions.isChecked = state.isPredictionsOnly
            cbLeagues.isChecked = state.needToShowLeagues
            rvLeagues.isVisible = false
            rvMatches.isVisible = true
            if (rvMatches.adapter == null)
                rvMatches.adapter = matchesAdapter
            matchesAdapter.submitList(state.list as List<ComplexItem.PlayedMatchItem>)
        }

        private fun renderLeaguesState(state: RecentMatchesViewState.LeaguesState) = with(binding) {
            tvRetry.isVisible = false
            clInfo.isVisible = true
            cbOnlyWithPredictions.isChecked = state.isPredictionsOnly
            cbLeagues.isChecked = state.needToShowLeagues
            rvLeagues.isVisible = true
            rvMatches.isVisible = false
            if (rvLeagues.adapter == null)
                rvLeagues.adapter = leaguesAdapter
            leaguesAdapter.submitList(state.list as List<ComplexItem.LeagueHistoryItem>)
        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
                else -> handleCustomEvents(event)
            }
        }

        private fun handleCustomEvents(event: ViewEvent) {
            when (event) {
                is ViewEvent.PopBackStack<*> -> {
                    navController.popBackStack()
                }
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            viewModel.viewEvent.setResultListener(this@RecentMatchesFragment)
            navController.navigateViaScreenRoute(screen)
        }
    }
}