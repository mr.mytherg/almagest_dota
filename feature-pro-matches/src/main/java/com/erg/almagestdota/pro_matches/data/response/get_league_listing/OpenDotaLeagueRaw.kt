package com.erg.almagestdota.pro_matches.data.response.get_league_listing

import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.NotRequired
import com.erg.almagestdota.local_storage.external.models.leagues.League
import com.erg.almagestdota.local_storage.external.models.leagues.LeagueTierType
import com.google.gson.annotations.SerializedName
import javax.inject.Inject

/**
 * @param tier - premium, excluded, professional
 */

 data class OpenDotaLeagueRaw(
    @NotRequired
    @SerializedName("name")
    val name: String? = null,
    @NotRequired
    @SerializedName("leagueid")
    val leagueId: Long? = null,
    @NotRequired
    @SerializedName("tier")
    val tier: String? = null,
) {
    class MapperToLeague @Inject constructor() : EssentialMapper<OpenDotaLeagueRaw, League>() {
        override fun transform(raw: OpenDotaLeagueRaw): League {
            return League(
                name = raw.name.orDefault(),
                id = raw.leagueId.orDefault(),
                tier = LeagueTierType.byName(raw.tier.orDefault()),
            )
        }
    }
}