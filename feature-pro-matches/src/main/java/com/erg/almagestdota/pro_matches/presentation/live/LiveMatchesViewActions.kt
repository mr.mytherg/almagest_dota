package com.erg.almagestdota.pro_matches.presentation.live

internal sealed class LiveMatchesViewActions {

    class OpenPredictor(val key: String) : LiveMatchesViewActions()
    object OpenHistory : LiveMatchesViewActions()
    object LoadLeagues : LiveMatchesViewActions()

}