package com.erg.almagestdota.pro_matches.data.response.get_live_league_games

import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.NotRequired
import com.erg.almagestdota.local_storage.external.models.live.PlayerAccountInfo
import com.google.gson.annotations.SerializedName
import javax.inject.Inject

 data class PlayerAccountInfoRaw(
     @NotRequired
     @SerializedName("account_id")
     val accountId: Long? = null,
     @NotRequired
     @SerializedName("hero_id")
     val heroId: Int? = null,
     @NotRequired
     @SerializedName("team")
     val team: Int? = null,
     @NotRequired
     @SerializedName("name")
     val nickname: String? = null,
 ) {
    class MapperToLivePlayerAccountInfo @Inject constructor() : EssentialMapper<PlayerAccountInfoRaw, PlayerAccountInfo>() {
        override fun transform(raw: PlayerAccountInfoRaw): PlayerAccountInfo {
            return PlayerAccountInfo(
                accountId = raw.accountId.orDefault(),
                heroId = raw.heroId.orDefault(),
                team = raw.team.orDefault(),
                nickname = raw.nickname.orDefault(),
            )
        }
    }
}