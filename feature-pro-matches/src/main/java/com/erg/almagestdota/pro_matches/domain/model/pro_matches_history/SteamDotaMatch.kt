package com.erg.almagestdota.pro_matches.domain.model.pro_matches_history

import android.os.Parcelable
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData
import kotlinx.android.parcel.Parcelize

@Parcelize
internal data class SteamDotaMatch(
    val hasRadiantWon: Boolean,
    val hasRadiantFirstPick: Boolean,
    val duration: Long,
    val matchId: Long,
    val radiantHeroes: List<HeroCoreData>,
    val direHeroes: List<HeroCoreData>,
    val radiantBans: List<HeroCoreData>,
    val direBans: List<HeroCoreData>,
    val lobbyType: SteamDotaMatchLobbyType,
    val gameMode: SteamDotaMatchGameMode,
) : Parcelable