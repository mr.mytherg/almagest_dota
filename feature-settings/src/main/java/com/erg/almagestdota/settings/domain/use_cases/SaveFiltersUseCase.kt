package com.erg.almagestdota.settings.domain.use_cases

import android.content.Context
import android.util.Log
import com.erg.almagestdota.base.external.extensions.asMap
import com.erg.almagestdota.drafting.domain.use_cases.LoadPlayerInfoUseCase
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.GameModeType
import com.erg.almagestdota.local_storage.external.models.draft.main.*
import com.erg.almagestdota.local_storage.external.models.leagues.LeagueTierType
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.local_storage.external.raw_models.DotaMatchRaw
import com.erg.almagestdota.local_storage.external.raw_models.ImportantSettingsRaw
import com.erg.almagestdota.network.external.firebase.FirebaseRepositoryContract
import com.erg.almagestdota.predictor.domain.use_cases.HeroValuesCalibratorUseCase
import com.erg.almagestdota.predictor.domain.use_cases.PredictionUseCase
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.*
import kotlinx.coroutines.tasks.await
import java.io.BufferedWriter
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStreamWriter
import java.lang.reflect.Type
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.coroutines.coroutineContext
import kotlin.math.pow

@Singleton
class SaveFiltersUseCase @Inject constructor(
    @ApplicationContext private val context: Context,
    private val gson: Gson
) {
    private val fileName = "${context.filesDir}filters.json"

    fun putToFile(groups: List<DraftFilterGroup>) {
        val matchesFile = File(fileName)

        FileOutputStream(matchesFile).use { fos ->
            OutputStreamWriter(fos, Charsets.UTF_8).use { osw ->
                BufferedWriter(osw).use { bf ->
                    var initial = 0
                    while (true) {
                        if (initial + 100 > groups.size) {
                            val text = gson.toJson(groups.subList(initial, groups.size))
                            if (initial == 0)
                                bf.write(text)
                            else
                                bf.write(text.substring(1))
                            break
                        } else {
                            val text = gson.toJson(groups.subList(initial, initial + 1000))
                            initial += 100
                            if (initial == 100) {
                                bf.write(text.substring(0, text.lastIndex) + ",")
                            } else {
                                bf.write(text.substring(1, text.lastIndex) + ",")
                            }
                        }
                    }
                }
            }
        }
    }

    fun readFromFile(): List<DraftFilterGroup> {
        return try {
            val json = File(fileName).inputStream().readBytes().toString(Charsets.UTF_8)
            val listType: Type = object : TypeToken<List<DraftFilterGroup>>() {}.type
            gson.fromJson(json, listType)
        } catch (e: Exception) {
            Log.e("READFROMFILEEXCEPTION", e.stackTraceToString())
            listOf()
        }
    }
}

