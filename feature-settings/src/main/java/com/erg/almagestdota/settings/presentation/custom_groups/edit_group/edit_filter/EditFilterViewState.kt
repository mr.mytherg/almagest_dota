package com.erg.almagestdota.settings.presentation.custom_groups.edit_group.edit_filter

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem

internal sealed class EditFilterViewState : ViewState() {
    class DefaultState(
        val selectedHeroes: List<ComplexItem>,
        val allHeroes: List<ComplexItem>
    ) : EditFilterViewState()
}