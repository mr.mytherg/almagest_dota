package com.erg.almagestdota.settings.presentation.custom_groups.edit_group

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.local_storage.external.models.main_settings.DraftFilter
import com.erg.almagestdota.local_storage.external.models.main_settings.DraftFilterGroup
import com.erg.almagestdota.settings.presentation.SettingsFragmentDirections
import com.erg.almagestdota.settings.presentation.custom_groups.GroupsFragmentDirections

class EditGroupScreen(group: DraftFilterGroup) : Screen<NavDirections>(
    route = GroupsFragmentDirections.toEditGroupFragment(group),
    requestKey = EditGroupFragment.TAG
) {
    companion object {
        private const val RESULT_KEY = "RESULT_KEY"
        private const val DATA_KEY = "DATA_KEY"

        fun getResult(data: Bundle): Result = Result.enumValueOf(data.getString(RESULT_KEY))
        fun getGroup(data: Bundle): DraftFilterGroup = data.getParcelable(DATA_KEY)!!
    }

    enum class Result {
        REMOVED,
        EDITED,
        CANCELED;

        companion object {
            fun enumValueOf(value: String?): Result {
                return values().firstOrNull { it.name == value } ?: CANCELED
            }

            fun createResult(result: Result, filter: DraftFilterGroup? = null): Bundle {
                return when (result) {
                    REMOVED ->  bundleOf(RESULT_KEY to result.name, DATA_KEY to filter)
                    EDITED -> bundleOf(RESULT_KEY to result.name, DATA_KEY to filter)
                    CANCELED -> bundleOf(RESULT_KEY to result.name)
                }
            }
        }
    }
}

