package com.erg.almagestdota.settings.domain.use_cases

import android.util.Log
import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.HeroStatsData
import com.erg.almagestdota.local_storage.external.models.draft.HeroValues
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.network.external.firebase.FirebaseRepositoryContract
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.delay
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.lang.Exception
import javax.inject.Inject

class LoadCoefficientsCounterUseCase @Inject constructor(
    private val iLocalStorageContract: ILocalStorageContract,
    private val firebaseRepository: FirebaseRepositoryContract
) {
    suspend operator fun invoke(viewModelScope: CoroutineScope) {
        val listDeferred = mutableListOf<Deferred<Unit>>()
        for (hero in GlobalVars.mainSettings.heroes) {
            val deferred = viewModelScope.async(Dispatchers.IO) {
                if (GlobalVars.importantSettings.heroesValues[hero.id] == null) {
                    GlobalVars.importantSettings.heroesValues[hero.id] =
                        HeroValues(mutableMapOf(), mutableMapOf())
                }
                addAgainstHeroItems(hero)
            }
            listDeferred.add(deferred)
            delay(1000)
        }
        listDeferred.awaitAll()
        firebaseRepository.setImportantSettings(GlobalVars.importantSettings)
    }

    private suspend fun addAgainstHeroItems(hero: HeroStatsData) {
        var heroName = hero.name
        if (heroName.contains(" ")) {
            heroName = heroName.replace(' ', '-')
        }
        if (heroName.contains("'")) {
            heroName = heroName.replace("'", "")
        }
        heroName = heroName.lowercase()

        val url = "https://www.dotabuff.com/heroes/$heroName/counters?date=patch_${GlobalVars.mainSettings.patch}"

        var doc: Document
        while (true) {
            try {
                doc = Jsoup.connect(url)
                    .header("Cookie",
                        "_tz=Asia%2FSaigon; _ga=GA1.2.1122684581.1669626336; __qca=P0-773539065-1669626336277; _hjSessionUser_2490228=eyJpZCI6Ijc0MGQ3Y2U2LWQwZjItNTgwZi1iNzkwLTMxZjA4OTJjNjhjNSIsImNyZWF0ZWQiOjE2Njk2MjYzMzYzMjgsImV4aXN0aW5nIjp0cnVlfQ==; _gid=GA1.2.2120575359.1671292247; _hjSession_2490228=eyJpZCI6IjRjMDczYWUwLWE2YjMtNDYyZC1iNjU2LTU3ZWY2ZWMxZWZmOCIsImNyZWF0ZWQiOjE2NzEzMzg0MTg3MjQsImluU2FtcGxlIjpmYWxzZX0=; _hjAbsoluteSessionInProgress=0; _hjIncludedInSessionSample=0; _hi=1671343654459")
                    .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.174 YaBrowser/22.1.3.848 Yowser/2.5 Safari/537.36")
                    .header("Connection", "keep-alive")
                    .header("sec-ch-ua", "\"Not?A_Brand\";v=\"8\", \"Chromium\";v=\"108\", \"Microsoft Edge\";v=\"108\"")
                    .timeout(0)
                    .get()
                break
            } catch (e: Exception) {
                delay(1000)
            }
        }

        val elements = doc.getElementsByAttributeValue("class", "sortable")
        val counters = mutableMapOf<Int, Double>()
        for (count in 1..GlobalVars.mainSettings.heroes.size) {
            val heroRow = elements.select("tr")?.getOrNull(count)
            if (heroRow == null) {
                Log.e("ERROR", "heroRow |$heroRow|")
                break
            }
            val heroNameObj = heroRow.getElementsByTag("td")?.getOrNull(1)?.getElementsByTag("a")?.getOrNull(0)
            if (heroNameObj == null) {
                Log.e("ERROR", "heroNameObj |$heroNameObj|")
                continue
            }
            val heroId = GlobalVars.mainSettings.heroes.find { it.name == heroNameObj.text() }?.id
            if (heroId == null) {
                Log.e("ERROR", "heroId |$heroId|")
                continue
            }
            val valueObj = heroRow.getElementsByTag("td")?.getOrNull(2)
            if (valueObj == null) {
                Log.e("ERROR", "valueObj |$valueObj|")
                continue
            }
            val value = try {
                val str = valueObj.text()
                str.substring(0, str.length - 1).toDouble()
            } catch (e: Exception) {
                0.0
            }
            counters[heroId] = (value * -1).round2()
        }

        if (counters.size < GlobalVars.mainSettings.heroes.size - 1) {
            throw IllegalStateException()
        }
        GlobalVars.importantSettings.heroesValues[hero.id]?.counters = counters
        Log.i("INFO", "counter coefficients loaded from |$url|")
    }
}