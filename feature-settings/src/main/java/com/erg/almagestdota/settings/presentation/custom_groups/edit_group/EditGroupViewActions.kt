package com.erg.almagestdota.settings.presentation.custom_groups.edit_group

import com.erg.almagestdota.settings.presentation.custom_groups.edit_group.edit_filter.EditFilterViewActions

internal sealed class EditGroupViewActions {

    object AddFilter : EditGroupViewActions()
    class EditFilter(val key: String) : EditGroupViewActions()
    object RemoveGroup : EditGroupViewActions()
    class Submit(val name: String) : EditGroupViewActions()
}