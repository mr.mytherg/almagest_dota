package com.erg.almagestdota.settings.data.response.get_matches

import com.google.gson.annotations.SerializedName

/**
 * @param hasRadiantWon - Dictates the winner of the match, true for radiant; false for dire.
 * @param lobbyType - 1: Practise; 2: Tournament
 * @param gameMode - 2: Captain's Mode
 * @param leagueId - The league that this match was a part of. A list of league IDs can be found via the GetLeagueListing method.
 * @param picksBans - A list of the picks and bans in the match, if the game mode is Captains Mode, or bans if not Captains Mode.
 * @param duration - The length of the match, in seconds since the match began.
 */
data class SteamMatchDetailsResultRaw(
    @SerializedName("radiant_win") val hasRadiantWon: Boolean? = null,
    @SerializedName("duration") var duration: Long? = null,
    @SerializedName("match_id") var matchId: Long? = null,
    @SerializedName("leagueid") var leagueId: Long? = null,
    @SerializedName("lobby_type") var lobbyType: Int? = null,
    @SerializedName("game_mode") var gameMode: Int? = null,
    @SerializedName("picks_bans") var picksBans: List<SteamMatchDetailsPicksBansRaw>? = listOf(),
    @SerializedName("players") var players: List<SteamMatchDetailsPlayersRaw>? = listOf()
)