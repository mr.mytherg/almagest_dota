package com.erg.almagestdota.settings.domain.use_cases

import com.erg.almagestdota.drafting.domain.use_cases.GameDataUseCase
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.GameStageData
import com.erg.almagestdota.local_storage.external.models.draft.GameStageType
import com.erg.almagestdota.local_storage.external.models.draft.RoleType
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import javax.inject.Inject
import kotlin.math.absoluteValue

class MatchesPredictionUseCase3 @Inject constructor(
    private val localStorage: ILocalStorageContract,
    private val gameDataUseCase: GameDataUseCase,

    ) {

    private val map = mutableMapOf<Int, Statistics>()
    private val stages = listOf<GameStageType>(
        GameStageType.LANING,
        GameStageType.EARLY_GAME,
        GameStageType.MID_GAME,
        GameStageType.LATE_GAME,
    )

    init {
        for (i in 1..100) {
            map[i] = Statistics(0,0)
        }
    }

    suspend operator fun invoke(): String {
        val dpcMatches = localStorage.metaMatches

        var skippedCount = 0
        for (match in dpcMatches) {
            val heroesRadiant = getHeroes(match.radiantHeroes)
            val heroesDire = getHeroes(match.direHeroes)
            if (!isValidRoles(heroesRadiant) || !isValidRoles(heroesDire)) {
                skippedCount++
                continue
            }
            val gameStages = gameDataUseCase.invoke(radiant = heroesRadiant, dire = heroesDire)
            val wonTeam = if (match.hasRadiantWon) gameStages.radiantTeam else gameStages.direTeam
            val lostTeam = if (!match.hasRadiantWon) gameStages.radiantTeam else gameStages.direTeam

            var count = 0
            var radWin = 0
//            count = getCount(wonTeam[0].stageData, lostTeam[0].stageData)
//            if (count > 0) radWin++ else if (count < 0) radWin--
//            count = getCount(wonTeam[1].stageData, lostTeam[1].stageData)
//            if (count > 0) radWin++ else if (count < 0) radWin--
////            if (match.duration / 60 > 20)
//                count = getCount(wonTeam[2].stageData, lostTeam[2].stageData)
//            if (count > 0) radWin++ else if (count < 0) radWin--
//
////            if (match.duration / 60 > 36)
//                count = getCount(wonTeam[3].stageData, lostTeam[3].stageData)
//            if (count > 0) radWin++ else if (count < 0) radWin--

            count = getBkbCount(match.radiantHeroes, match.direHeroes, match.hasRadiantWon)
            if (count > 0) radWin++ else if (count < 0) radWin--

            map[radWin.absoluteValue]?.apply {
                if (radWin > 0)
                    winCount++
                else if (radWin < 0)
                    loseCount++
            }
        }
        var totalWin = 0
        var totalLose = 0
        var string = "matches - ${dpcMatches.size} skipped - ${skippedCount}\n"
        for (i in 1..100) {
            val data = map[i]!!

            if (data.winCount == 0 && data.loseCount == 0)
                continue
            totalWin += data.winCount
            totalLose += data.loseCount
            string += "impactCount - ${i} wins - ${data.winCount} loses - ${data.loseCount}\n"
        }
        string += "totalWin - ${totalWin} totalLose - ${totalLose}\n"

        return string
    }

    private fun getBkbCount(radiantHeroes: List<PlayerInfo>, direHeroes: List<PlayerInfo>, hasRadiantWon: Boolean): Int {
        val wonTeam = if (hasRadiantWon) radiantHeroes else direHeroes
        val lostTeam = if (!hasRadiantWon) radiantHeroes else direHeroes
        var count = 0
        for (hero in wonTeam) {
            if (GlobalVars.heroesAsMap[hero.heroId]?.hasBkb == true)
                count++
        }
        for (hero in lostTeam) {
            if (GlobalVars.heroesAsMap[hero.heroId]?.hasBkb == true)
                count--
        }
        return count
    }

    private fun getCount(winner: GameStageData, loser: GameStageData): Int {
        var count = 0

        if (winner.kills > loser.kills) count++ else if (winner.kills < loser.kills) count--
        if (winner.deaths < loser.deaths) count++ else if (winner.deaths > loser.deaths) count--
        if (winner.assists > loser.assists) count++ else if (winner.assists < loser.assists) count--
        if (winner.heroDamage > loser.heroDamage) count++ else if (winner.heroDamage < loser.heroDamage) count--
        if (winner.towerDamage > loser.towerDamage) count++ else if (winner.towerDamage < loser.towerDamage) count--
        if (winner.damageReceived + winner.healingAllies > loser.damageReceived + loser.healingAllies) count++ else count--
        if (winner.disableDuration > loser.disableDuration) count++ else if (winner.disableDuration < loser.disableDuration) count--
        if (winner.stunDuration > loser.stunDuration) count++ else if (winner.stunDuration < loser.stunDuration) count--
        return count
    }


    class Statistics(
        var winCount: Int = 0,
        var loseCount: Int = 0,
    )

    private fun isValidRoles(heroes: List<HeroRoleData>): Boolean {
        return heroes.find { it.heroRoles.orEmpty().firstOrNull() == RoleType.UNKNOWN } == null
    }

    private fun getHeroes(radiantHeroes: List<PlayerInfo>): List<HeroRoleData> {
        val heroes = mutableMapOf<Int, RoleType>()
        for (heroPlayer in radiantHeroes) {
            val playerInfo = GlobalVars.players[heroPlayer.playerId]
            if (playerInfo != null) {
                setRoles(heroes, heroPlayer.heroId, playerInfo.role)
            } else {
                heroes[heroPlayer.heroId] = RoleType.UNKNOWN
            }
        }
        return heroes.map {
            val hero = it.key.getHeroById()
            HeroRoleData(
                hero.id,
                hero.name,
                hero.imageLink,
                setOf(it.value)
            )
        }
    }

    private fun setRoles(heroesRoles: MutableMap<Int, RoleType>, heroId: Int, role: RoleType) {
        heroesRoles.forEach { (heroId, roleType) ->
            if (roleType == role)
                heroesRoles[heroId] = RoleType.UNKNOWN
        }
        heroesRoles[heroId] = role
    }


}