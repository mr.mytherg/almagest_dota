package com.erg.almagestdota.settings.data

import com.erg.almagestdota.settings.domain.model.get_matches.SteamDotaMatch

 interface ISteamContract {
    suspend fun getMatchDetails(matchId: Long): SteamDotaMatch
}