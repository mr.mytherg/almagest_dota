package com.erg.almagestdota.settings.domain.use_cases

import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.local_storage.external.models.draft.HeroRole
import com.erg.almagestdota.local_storage.external.models.draft.PlayerLaneOutcome
import com.erg.almagestdota.local_storage.external.models.draft.RoleType
import com.erg.almagestdota.local_storage.external.models.draft.main.LaneStatusType
import com.erg.almagestdota.local_storage.external.models.draft.main.MemberInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import javax.inject.Inject

class GetPlayersLanesUseCase @Inject constructor() {
    operator fun invoke(matches: List<DotaMatch>): Map<Long, PlayerLaneOutcome> {
        val mutableMap = mutableMapOf<Long, PlayerLaneOutcome>()
        val players = mutableSetOf<Long>()
        for (match in matches) {
            for (player in match.radiantHeroes) {
                players.add(player.playerId)
            }
            for (player in match.direHeroes) {
                players.add(player.playerId)
            }
        }

        for (playerId in players) {
            mutableMap[playerId] = PlayerLaneOutcome(playerId, GlobalVars.players[playerId]?.role ?: RoleType.UNKNOWN)
        }

        for (match in matches) {
            for (player in match.radiantHeroes) {
                val playerInfo = GlobalVars.players[player.playerId] ?: MemberInfo(player.playerId)
                if (playerInfo.role == RoleType.UNKNOWN) continue
                mutableMap[player.playerId]?.apply {
                    when (player.laneStatus) {
                        LaneStatusType.RADIANT_STOMP -> winPlus++
                        LaneStatusType.RADIANT_WIN -> win++
                        LaneStatusType.TIE -> tie++
                        LaneStatusType.DIRE_WIN -> lose++
                        LaneStatusType.DIRE_STOMP -> loseMinus++
                        else -> undefined++
                    }
                    teamId = match.radiantTeamId
                    matchCount++
                }
            }
            for (player in match.direHeroes) {
                val playerInfo = GlobalVars.players[player.playerId] ?: MemberInfo(player.playerId)
                if (playerInfo.role == RoleType.UNKNOWN) continue
                mutableMap[player.playerId]?.apply {
                    when (player.laneStatus) {
                        LaneStatusType.RADIANT_STOMP -> loseMinus++
                        LaneStatusType.RADIANT_WIN -> lose++
                        LaneStatusType.TIE -> tie++
                        LaneStatusType.DIRE_WIN -> win++
                        LaneStatusType.DIRE_STOMP -> winPlus++
                        else -> undefined++
                    }
                    teamId = match.direTeamId
                    matchCount++
                }
            }
        }
        return mutableMap
    }
}