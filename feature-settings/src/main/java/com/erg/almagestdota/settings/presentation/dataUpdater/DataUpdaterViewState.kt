package com.erg.almagestdota.settings.presentation.dataUpdater

import androidx.constraintlayout.motion.utils.ViewState

internal sealed class DataUpdaterViewState : ViewState() {
    class SomeState(val msg: String) : DataUpdaterViewState()
}