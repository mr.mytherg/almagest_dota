package com.erg.almagestdota.settings.data.response.get_matches

import com.google.gson.annotations.SerializedName

data class SteamMatchDetailsPlayersRaw(
    @SerializedName("account_id") var accountId: Long? = null,
    @SerializedName("hero_id") var heroId: Int? = null,
)