package com.erg.almagestdota.settings.domain.use_cases

import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.local_storage.external.raw_models.DotaMatchRaw
import com.erg.almagestdota.local_storage.external.raw_models.DotaMatchesRaw
import com.erg.almagestdota.network.external.firebase.FirebaseRepository
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.*
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class LoadDocumentMatchesUseCase @Inject constructor(
    private val firebaseFirestore: FirebaseFirestore,
) {

    suspend operator fun invoke(documentId: Int): List<DotaMatch> {
        val matches = firebaseFirestore.collection(FirebaseRepository.MATCHES_DOCUMENT_NAME).document(documentId.toString())
        val document = matches.get().await()
        val dotaMatches = try {
            document.toObject(DotaMatchesRaw::class.java)?.matches.orEmpty().map { DotaMatchRaw.toModel(it) }
        } catch (e: Exception) {
            listOf()
        }

       return dotaMatches
    }
}