package com.erg.almagestdota.settings.data

import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.settings.data.response.get_matches.OpenDotaMatchRaw
import com.erg.almagestdota.settings.domain.model.get_matches.OpenDotaMatch
import kotlinx.coroutines.delay
import java.lang.Exception
import java.lang.IllegalStateException
import javax.inject.Inject

class OpenDotaRepository @Inject constructor(
    private val api: IOpenDotaApi,
    private val mapperToOpenDotaMatch: OpenDotaMatchRaw.MapperToOpenDotaMatch,
) : IOpenDotaContract {
    override suspend fun loadMatches(endId: Long?, startId: Long?): List<OpenDotaMatch> {
        val matches = mutableListOf<OpenDotaMatch>()
        var lessThan: Long? = startId
        if (startId != null && endId == null) {
            throw IllegalStateException()
        }
        while (true) {
            try {
                val subList = api.loadMatches(lessThan).essentialMap(mapperToOpenDotaMatch).sortedByDescending { it.matchId }
                if (endId == null) {
                    matches.addAll(subList)
                    break
                } else if (subList.last().matchId > endId) {
                    matches.addAll(subList)
                    lessThan = subList.last().matchId
                } else {
                    for (match in subList) {
                        matches.addAll(subList)
                    }
                    break
                }
            } catch (e: Exception) {
                // HTTP 429, 403
                delay(100)
            }
        }

        return matches
    }
}