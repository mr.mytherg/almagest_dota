package com.erg.almagestdota.settings.data.response.get_matches

import com.google.gson.annotations.SerializedName

/**
 * @param isPick - Whether this entry is a pick (true) or a ban (false).
 * @param heroId - The hero's unique ID. A list of hero IDs can be found via the GetHeroes method.
 * @param team - The team who chose the pick or ban; 0 for Radiant, 1 for Dire.
 * @param order - The order of which the picks and bans were selected; 0 - sequence.length.
 */
data class SteamMatchDetailsPicksBansRaw(
    @SerializedName("is_pick") var isPick: Boolean? = null,
    @SerializedName("hero_id") var heroId: Int? = null,
    @SerializedName("team") var team: Int? = null,
    @SerializedName("order") var order: Int? = null
)