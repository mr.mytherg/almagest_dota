package com.erg.almagestdota.settings.domain.use_cases

import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.local_storage.external.models.draft.HeroRole
import com.erg.almagestdota.local_storage.external.models.draft.RoleType
import com.erg.almagestdota.local_storage.external.models.draft.main.MemberInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import javax.inject.Inject

class UpdateHeroRolesUseCase @Inject constructor() {
    operator fun invoke(matches: List<DotaMatch>, teamId: Long, enemy: Boolean = false): Map<Int, List<HeroRole>> {
        val mutableMap = mutableMapOf<Int, MutableMap<RoleType, HeroRole>>()
        for (hero in GlobalVars.mainSettings.heroes) {
            mutableMap[hero.id] = mutableMapOf(
                RoleType.ROLE_1 to HeroRole(roleType = RoleType.ROLE_1, winRatePercentage = 0.0, pickRate = 0.0, matchCount = 0),
                RoleType.ROLE_2 to HeroRole(roleType = RoleType.ROLE_2, winRatePercentage = 0.0, pickRate = 0.0, matchCount = 0),
                RoleType.ROLE_3 to HeroRole(roleType = RoleType.ROLE_3, winRatePercentage = 0.0, pickRate = 0.0, matchCount = 0),
                RoleType.ROLE_4 to HeroRole(roleType = RoleType.ROLE_4, winRatePercentage = 0.0, pickRate = 0.0, matchCount = 0),
                RoleType.ROLE_5 to HeroRole(roleType = RoleType.ROLE_5, winRatePercentage = 0.0, pickRate = 0.0, matchCount = 0),
            )
        }
        for (match in matches) {
            if (teamId == 0L || (match.radiantTeamId == teamId && !enemy || match.direTeamId == teamId && enemy))
                for (player in match.radiantHeroes) {
                    val playerInfo = GlobalVars.players[player.playerId] ?: MemberInfo(player.playerId)
                    if (playerInfo.role == RoleType.UNKNOWN) continue
                    mutableMap[player.heroId]!![playerInfo.role] = mutableMap[player.heroId]!![playerInfo.role]!!.apply {
                        this.matchCount++
                        if (match.hasRadiantWon)
                            this.winCount++
                    }
                }
            if (teamId == 0L || (match.direTeamId == teamId && !enemy || match.radiantTeamId == teamId && enemy))
                for (player in match.direHeroes) {
                    val playerInfo = GlobalVars.players[player.playerId] ?: MemberInfo(player.playerId)
                    if (playerInfo.role == RoleType.UNKNOWN) continue
                    mutableMap[player.heroId]!![playerInfo.role] = mutableMap[player.heroId]!![playerInfo.role]!!.apply {
                        this.matchCount++
                        if (!match.hasRadiantWon)
                            this.winCount++
                    }
                }
        }
        val resultMap = mutableMapOf<Int, List<HeroRole>>()
        for ((k, v) in mutableMap) {
            val totalMatchCount = v.toList().fold(0) { acc, next -> acc + next.second.matchCount }
            val role1 = getRole(RoleType.ROLE_1, v[RoleType.ROLE_1]!!, totalMatchCount)
            val role2 = getRole(RoleType.ROLE_2, v[RoleType.ROLE_2]!!, totalMatchCount)
            val role3 = getRole(RoleType.ROLE_3, v[RoleType.ROLE_3]!!, totalMatchCount)
            val role4 = getRole(RoleType.ROLE_4, v[RoleType.ROLE_4]!!, totalMatchCount)
            val role5 = getRole(RoleType.ROLE_5, v[RoleType.ROLE_5]!!, totalMatchCount)
            resultMap[k] = listOf(
                role1,
                role2,
                role3,
                role4,
                role5
            )
        }
        return resultMap
    }

    private fun getRole(role: RoleType, heroRole: HeroRole, totalMatchCount: Int): HeroRole {
        return if (heroRole.matchCount == 0) {
            HeroRole(
                roleType = role,
                winRatePercentage = 0.0,
                pickRate = 0.0,
                matchCount = 0
            )
        } else
            HeroRole(
                roleType = role,
                winRatePercentage = (heroRole.winCount * 1.0 / heroRole.matchCount * 100).round2(),
                pickRate = (heroRole.matchCount * 1.0 / totalMatchCount * 100).round2(),
                matchCount = heroRole.matchCount
            )
    }
}