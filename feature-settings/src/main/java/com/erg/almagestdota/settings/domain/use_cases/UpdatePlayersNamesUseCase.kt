package com.erg.almagestdota.settings.domain.use_cases

import android.content.Context
import android.content.pm.PackageInfo
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.drafting.data.IStratzGraphqlApi
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.HeroStage
import com.erg.almagestdota.local_storage.external.models.draft.StageType
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.main_settings.MainSettings
import com.erg.almagestdota.local_storage.external.models.main_settings.PlayersSettings
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.network.external.firebase.FirebaseRepositoryContract
import com.erg.almagestdota.settings.domain.model.get_matches.HeroStagesModel
import com.google.firebase.firestore.FirebaseFirestore
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class UpdatePlayersNamesUseCase @Inject constructor(
    private val firebaseRepository: FirebaseRepositoryContract,
    private val stratzApi: IStratzGraphqlApi,
    ) {
    suspend operator fun invoke(): Int {
        val matches = firebaseRepository.getMatches().takeLast(1500)
        var noNamePlayers = 0
        for ((playerId, info) in GlobalVars.players) {
            if (info.name.isEmpty()) {
                noNamePlayers++
            }
        }

        val playersSet = mutableSetOf<Long>()
        for (match in matches) {
            for (player in match.radiantHeroes)
                if (GlobalVars.players[player.playerId]?.name.orDefault().isEmpty())
                    playersSet.add(player.playerId)
            for (player in match.direHeroes)
                if (GlobalVars.players[player.playerId]?.name.orDefault().isEmpty())
                    playersSet.add(player.playerId)
        }
        var playersList = playersSet.toList()
        while (true) {
            if (playersList.size > 5) {
                val currentPlayers = playersList.take(5)
                playersList = playersList.subList(5, playersList.size)
                savePlayers(currentPlayers)
            } else {
                savePlayers(playersList)
                break
            }
        }

        var noNamePlayersNew = 0
        for ((playerId, info) in GlobalVars.players) {
            if (info.name.isEmpty()) {
                noNamePlayersNew++
            }
        }

        firebaseRepository.setPlayersSettings(PlayersSettings(GlobalVars.players.map { it.value }))
        return noNamePlayersNew - noNamePlayers
    }

    private suspend fun savePlayers(currentPlayers: List<Long>) {
        val playersIds = getPlayersIds(currentPlayers)
        val query = "{players(steamAccountIds:[$playersIds]){steamAccount{proSteamAccount{name}}}}"
        val playersInfo = stratzApi.getPlayers(query).data?.players.orEmpty()
        for (playerInfo in playersInfo) {
            val proSteamAccount = playerInfo.steamAccount?.proSteamAccount ?: continue
            if (!proSteamAccount.name.isNullOrEmpty()) {
                val savedPlayer = GlobalVars.players[proSteamAccount.id.orDefault()] ?: continue
                GlobalVars.players[proSteamAccount.id.orDefault()] = savedPlayer.apply {
                    name = proSteamAccount.name.orDefault()
                }
            }
        }
    }

    private fun getPlayersIds(currentPlayers: List<Long>): String {
        var string = ""
        for (player in currentPlayers) {
            if (string.isNotEmpty())
                string += ","
            string += player
        }
        return string
    }

    private suspend fun doTillSuccess(action: suspend () -> Unit) {
        while (true) {
            try {
                action.invoke()
                break
            } catch (e: Exception) {
                delay(200)
            }
        }
    }
}