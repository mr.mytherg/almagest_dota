package com.erg.almagestdota.settings.domain.use_cases

import android.content.Context
import android.content.pm.PackageInfo
import android.util.Log
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.DotaItem
import com.erg.almagestdota.local_storage.external.models.draft.HeroStatsData
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.main_settings.MainSettings
import com.erg.almagestdota.network.external.firebase.FirebaseRepositoryContract
import com.google.firebase.firestore.FirebaseFirestore
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.delay
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import javax.inject.Inject

class LoadDataCounterItemsUseCase @Inject constructor(
    private val iLocalStorageContract: ILocalStorageContract,
    private val firebaseRepository: FirebaseRepositoryContract
) {
    suspend operator fun invoke(viewModelScope: CoroutineScope) {
        val listDeferred = mutableListOf<Deferred<Unit>>()
        for (hero in GlobalVars.mainSettings.heroes) {
            val deferred = viewModelScope.async(Dispatchers.IO) {
                val heroItems = addHeroItems(hero)
                GlobalVars.mainSettings.heroesItems[hero.id]!!.againstHero = heroItems
            }
            listDeferred.add(deferred)
            delay(100)
        }
        listDeferred.awaitAll()
        iLocalStorageContract.setMainSettings(GlobalVars.mainSettings)
        firebaseRepository.setMainSettings(GlobalVars.mainSettings)
    }

    private fun addHeroItems(hero: HeroStatsData): List<DotaItem> {
        var heroName = hero.name
        if (heroName.contains(" ")) {
            heroName = heroName.replace(' ', '+')
        }
        if (heroName.contains("'")) {
            heroName = heroName.replace("'", "%27")
        }

        val url = "https://counterpick.herokuapp.com/item_counters?enemies%5B%5D=$heroName"
        val doc: Document = Jsoup.connect(url).timeout(0).get()
        val itemSize = doc.getElementsByTag("h3").size
        var count = 0
        val itemNames = mutableListOf<String>()
        while (count < itemSize) {
            if (doc.getElementsByTag("h3")[count] == null) break
            val text = doc.getElementsByTag("h3")[count].text()
            itemNames.add(text)
            count++
        }

        Log.i("INFO", "counter items loaded from |$url|")
        return createItemList(hero, itemNames)
    }

    private fun createItemList(hero: HeroStatsData, itemNames: MutableList<String>): List<DotaItem> {
        val againstHeroItems = mutableListOf<DotaItem>()
        itemNames.forEach { name ->
            val dotaItem = GlobalVars.mainSettings.items.find { it.name == name }
            if (dotaItem == null) {
                Log.e("ERROR", "no item in dotaItems with name |$name|")
            } else if (name == "Aghanim's Scepter" || name == "Aghanims's Shard") {
                Log.i("INFO", "this item can't be a counter item |$name|")
            } else {
                againstHeroItems.add(dotaItem)
            }
        }

        return againstHeroItems
    }
}