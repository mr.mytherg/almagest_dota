package com.erg.almagestdota.settings.di

import android.content.Context
import com.erg.almagestdota.drafting.domain.use_cases.LoadPlayerInfoUseCase
import com.erg.almagestdota.drafting.domain.use_cases.LoadTeamInfoStratzUseCase
import com.erg.almagestdota.drafting.domain.use_cases.LoadTeamInfoUseCase
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.network.external.annotations.RetrofitClientGraphqlStratz
import com.erg.almagestdota.network.external.annotations.RetrofitClientOpendota
import com.erg.almagestdota.network.external.annotations.RetrofitClientSteam
import com.erg.almagestdota.network.external.firebase.FirebaseRepositoryContract
import com.erg.almagestdota.predictor.domain.use_cases.HeroValuesCalibratorUseCase
import com.erg.almagestdota.predictor.domain.use_cases.PredictionUseCase
import com.erg.almagestdota.drafting.data.IStratzGraphqlApi
import com.erg.almagestdota.drafting.domain.use_cases.GameDataUseCase
import com.erg.almagestdota.settings.data.*
import com.erg.almagestdota.settings.data.response.get_matches.OpenDotaMatchRaw
import com.erg.almagestdota.settings.data.response.get_matches.SteamMatchDetailsRaw
import com.erg.almagestdota.settings.domain.use_cases.*
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ModuleSettings {

    @Provides
    @Singleton
    fun provideLoadDataCounterItemsUseCase(
        iLocalStorageContract: ILocalStorageContract,
        firebaseRepository: FirebaseRepositoryContract
    ): LoadDataCounterItemsUseCase {
        return LoadDataCounterItemsUseCase(
            iLocalStorageContract,
            firebaseRepository
        )
    }

    @Provides
    @Singleton
    fun provideFiltersRepository(
        saveFiltersUseCase: SaveFiltersUseCase,
        getFilterGroupsUseCase: GetFilterGroupsUseCase
    ): FiltersRepository {
        return FiltersRepository(
            saveFiltersUseCase,
            getFilterGroupsUseCase
        )
    }

    @Provides
    @Singleton
    fun provideGetPlayersLanesUseCase(
    ): GetPlayersLanesUseCase {
        return GetPlayersLanesUseCase(
        )
    }

    @Provides
    @Singleton
    fun provideGetSinglePlayerLanesUseCase(
        getPlayersLanesUseCase: GetPlayersLanesUseCase
    ): GetSinglePlayerLanesUseCase {
        return GetSinglePlayerLanesUseCase(
            getPlayersLanesUseCase
        )
    }

    @Singleton
    fun provideMatchesPredictionUseCase4(
        saveMatchesUseCase: SaveMatchesUseCase,
    ): MatchesPredictionUseCase4 {
        return MatchesPredictionUseCase4(
            saveMatchesUseCase,
        )
    }

    @Provides
    @Singleton
    fun provideLoadItemsUseCase(
        iLocalStorageContract: ILocalStorageContract,
        firebaseRepository: FirebaseRepositoryContract
    ): LoadItemsUseCase {
        return LoadItemsUseCase(
            iLocalStorageContract,
            firebaseRepository
        )
    }

    @Provides
    @Singleton
    fun provideMatchesPredictionUseCase(
        @ApplicationContext context: Context,
        heroValuesCalibratorUseCase: HeroValuesCalibratorUseCase,
        gson: Gson
    ): MatchesPredictionUseCase {
        return MatchesPredictionUseCase(
            context,
            heroValuesCalibratorUseCase,
            gson
        )
    }

    @Provides
    @Singleton
    fun provideDefinePlayersRolesUseCase(
        firebaseRepository: FirebaseRepositoryContract
    ): DefinePlayersRolesUseCase {
        return DefinePlayersRolesUseCase(
            firebaseRepository
        )
    }

    @Provides
    @Singleton
    fun provideUpdateTeamNamesUseCase(
        firebaseRepository: FirebaseRepositoryContract,
        localStorage: ILocalStorageContract,
        loadTeamInfoStratzUseCase: LoadTeamInfoStratzUseCase,

        ): UpdateTeamNamesUseCase {
        return UpdateTeamNamesUseCase(
            firebaseRepository,
            localStorage,
            loadTeamInfoStratzUseCase
        )
    }

    @Provides
    @Singleton
    fun provideLoadRolesUseCase(
        stratzGraphqlApi: IStratzGraphqlApi,
        iLocalStorageContract: ILocalStorageContract,
        firebaseRepository: FirebaseRepositoryContract
    ): LoadRolesUseCase {
        return LoadRolesUseCase(
            stratzGraphqlApi,
            iLocalStorageContract,
            firebaseRepository
        )
    }

    @Provides
    @Singleton
    fun provideMatchesPredictionUseCase5(
        iLocalStorageContract: ILocalStorageContract,
    ): MatchesPredictionUseCase5 {
        return MatchesPredictionUseCase5(
            iLocalStorageContract,
        )
    }

    @Provides
    @Singleton
    fun provideLoadCoefficientsSynergyUseCase(
        stratzGraphqlApi: IStratzGraphqlApi,
        iLocalStorageContract: ILocalStorageContract,
        firebaseRepository: FirebaseRepositoryContract
    ): LoadCoefficientsSynergyUseCase {
        return LoadCoefficientsSynergyUseCase(
            stratzGraphqlApi,
            iLocalStorageContract,
            firebaseRepository
        )
    }

    @Provides
    @Singleton
    fun provideMatchesPredictionUseCase3(
        localStorage: ILocalStorageContract,
        gameDataUseCase: GameDataUseCase,
    ): MatchesPredictionUseCase3 {
        return MatchesPredictionUseCase3(
            localStorage,
            gameDataUseCase,
        )
    }


    @Provides
    @Singleton
    fun provideSaveFiltersUseCase(
        @ApplicationContext context: Context,
        gson: Gson
    ): SaveFiltersUseCase {
        return SaveFiltersUseCase(
            context,
            gson,
        )
    }

    @Provides
    @Singleton
    fun provideLoadCoefficientsCountersUseCase(
        iLocalStorageContract: ILocalStorageContract,
        firebaseRepository: FirebaseRepositoryContract
    ): LoadCoefficientsCounterUseCase {
        return LoadCoefficientsCounterUseCase(
            iLocalStorageContract,
            firebaseRepository
        )
    }

    @Provides
    @Singleton
    fun provideLoadCoreDataUseCase(
        stratzGraphqlApi: IStratzGraphqlApi,
        iLocalStorageContract: ILocalStorageContract,
        firebaseRepository: FirebaseRepositoryContract
    ): LoadCoreDataUseCase {
        return LoadCoreDataUseCase(
            stratzGraphqlApi,
            iLocalStorageContract,
            firebaseRepository
        )
    }

    @Provides
    @Singleton
    fun provideGraphqlStratzApi(@RetrofitClientGraphqlStratz retrofit: Retrofit): IStratzGraphqlApi {
        return retrofit.create(IStratzGraphqlApi::class.java)
    }

    @Provides
    @Singleton
    fun provideUpdateHeroStagesUseCase(
        iLocalStorageContract: ILocalStorageContract,
        firebaseRepository: FirebaseRepositoryContract
    ): UpdateHeroStagesUseCase {
        return UpdateHeroStagesUseCase(
            iLocalStorageContract,
            firebaseRepository
        )
    }

    @Provides
    @Singleton
    fun provideUpdateHeroRolesUseCase(
    ): UpdateHeroRolesUseCase {
        return UpdateHeroRolesUseCase(
        )
    }

    @Provides
    @Singleton
    fun provideSteamRepository(
        api: ISteamApi,
        mapperToSteamDotaMatch: SteamMatchDetailsRaw.MapperToSteamDotaMatch,
    ): ISteamContract {
        return SteamRepository(
            api = api,
            mapperToSteamDotaMatch = mapperToSteamDotaMatch
        )
    }

    @Provides
    @Singleton
    fun provideLoadMatchLaneStatusUseCase(
        api: IStratzGraphqlApi,
    ): LoadMatchLaneStatusUseCase {
        return LoadMatchLaneStatusUseCase(
            api
        )
    }

    @Provides
    @Singleton
    fun provideOpenDotaRepository(
        api: IOpenDotaApi,
        mapperToOpenDotaMatch: OpenDotaMatchRaw.MapperToOpenDotaMatch,
    ): IOpenDotaContract {
        return OpenDotaRepository(
            api = api,
            mapperToOpenDotaMatch = mapperToOpenDotaMatch
        )
    }

    @Provides
    @Singleton
    fun provideSteamApi(@RetrofitClientSteam retrofit: Retrofit): ISteamApi {
        return retrofit.create(ISteamApi::class.java)
    }

    @Provides
    @Singleton
    fun provideOpenDotaApi(@RetrofitClientOpendota retrofit: Retrofit): IOpenDotaApi {
        return retrofit.create(IOpenDotaApi::class.java)
    }

    @Provides
    @Singleton
    fun provideNewMatchesLoadingUseCase(
        steam: ISteamContract,
        firebaseRepository: FirebaseRepositoryContract,
        openDota: IOpenDotaContract,
        loadPlayerInfoUseCase: LoadPlayerInfoUseCase,
        loadTeamInfoUseCase: LoadTeamInfoUseCase,
        loadDocumentMatchesUseCase: LoadDocumentMatchesUseCase,
        loadTeamInfoStratzUseCase: LoadTeamInfoStratzUseCase,
        calibrateMatchesUseCase: CalibrateMatchesUseCase,
        iLocalStorageContract: ILocalStorageContract,
        loadMatchLaneStatusUseCase: LoadMatchLaneStatusUseCase,
    ): LoadNewMatchesUseCase {
        return LoadNewMatchesUseCase(
            steam,
            firebaseRepository,
            openDota,
            loadPlayerInfoUseCase,
            loadTeamInfoUseCase,
            loadDocumentMatchesUseCase,
            loadTeamInfoStratzUseCase,
            calibrateMatchesUseCase,
            iLocalStorageContract,
            loadMatchLaneStatusUseCase,
        )
    }

    @Provides
    @Singleton
    fun provideCalibrateMatchesUseCase(
        firebaseRepository: FirebaseRepositoryContract,
        heroValuesCalibratorUseCase: HeroValuesCalibratorUseCase,
        predictionUseCase: PredictionUseCase,
        loadPlayerInfoUseCase: LoadPlayerInfoUseCase,
    ): CalibrateMatchesUseCase {
        return CalibrateMatchesUseCase(
            firebaseRepository,
            heroValuesCalibratorUseCase,
            predictionUseCase,
            loadPlayerInfoUseCase,
        )
    }

    @Provides
    @Singleton
    fun provideOldMatchesLoadingUseCase(
        steam: ISteamContract,
        firebaseFirestore: FirebaseFirestore,
        openDota: IOpenDotaContract,
        gson: Gson,
        loadDocumentMatchesUseCase: LoadDocumentMatchesUseCase,
    ): LoadOldMatchesUseCase {
        return LoadOldMatchesUseCase(
            steam,
            firebaseFirestore,
            openDota,
            gson,
            loadDocumentMatchesUseCase,
        )
    }

    @Provides
    @Singleton
    fun provideLoadTeamInfoStratzUseCase(
        stratz: IStratzGraphqlApi,
    ): LoadTeamInfoStratzUseCase {
        return LoadTeamInfoStratzUseCase(
            stratz,
        )
    }

    @Provides
    @Singleton
    fun provideUpdatePlayersNamesUseCase(
        firebaseRepository: FirebaseRepositoryContract,
        stratzApi: IStratzGraphqlApi,
    ): UpdatePlayersNamesUseCase {
        return UpdatePlayersNamesUseCase(
            firebaseRepository,
            stratzApi
        )
    }
}