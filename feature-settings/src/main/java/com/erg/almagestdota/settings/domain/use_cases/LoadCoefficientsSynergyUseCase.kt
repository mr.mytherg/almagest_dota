package com.erg.almagestdota.settings.domain.use_cases

import android.util.Log
import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.HeroStatsData
import com.erg.almagestdota.local_storage.external.models.draft.HeroValues
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.network.external.firebase.FirebaseRepositoryContract
import com.erg.almagestdota.drafting.data.IStratzGraphqlApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.delay
import javax.inject.Inject

class LoadCoefficientsSynergyUseCase @Inject constructor(
    private val stratzGraphqlApi: IStratzGraphqlApi,
    private val iLocalStorageContract: ILocalStorageContract,
    private val firebaseRepository: FirebaseRepositoryContract
) {
    private fun getQueryWithId(id: Int): String {
        return "{heroStats{matchUp(heroId:$id,take:${GlobalVars.mainSettings.heroes.size},matchLimit:1){with{heroId2, synergy}}}}"
    }

    suspend operator fun invoke(viewModelScope: CoroutineScope) {
        val listDeferred = mutableListOf<Deferred<Unit>>()
        for (hero in GlobalVars.mainSettings.heroes) {
            val deferred = viewModelScope.async(Dispatchers.IO) {
                loadCoefficientsSynergy(hero)
            }
            listDeferred.add(deferred)
            delay(100)
        }
        listDeferred.awaitAll()
        firebaseRepository.setImportantSettings(GlobalVars.importantSettings)
    }

    private suspend fun loadCoefficientsSynergy(hero: HeroStatsData) {
        val result = stratzGraphqlApi.loadCoefficientsSynergy(getQueryWithId(hero.id))
        val synergies = mutableMapOf<Int, Double>()
        for (matchUp in result.data.heroStats.matchUp.first().with) {
            synergies[matchUp.heroId2] = (matchUp.synergy / 3.0).round2()
        }
        if (synergies.size < GlobalVars.mainSettings.heroes.size - 1) {
            throw IllegalStateException()
        }
        if (GlobalVars.importantSettings.heroesValues[hero.id] == null) {
            GlobalVars.importantSettings.heroesValues[hero.id] = HeroValues(synergies = synergies, mutableMapOf())
        } else {
            GlobalVars.importantSettings.heroesValues[hero.id]?.synergies = synergies
        }
        Log.i("INFO", "synergy coefficients loaded for ${hero.name}")
    }
}