package com.erg.almagestdota.settings.domain.use_cases

import android.util.Log
import com.erg.almagestdota.base.external.extensions.asMap
import com.erg.almagestdota.drafting.domain.use_cases.LoadPlayerInfoUseCase
import com.erg.almagestdota.drafting.domain.use_cases.LoadTeamInfoStratzUseCase
import com.erg.almagestdota.drafting.domain.use_cases.LoadTeamInfoUseCase
import com.erg.almagestdota.local_storage.external.models.draft.main.MemberInfo
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerInfo
import com.erg.almagestdota.local_storage.external.models.leagues.LeagueTierType
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.main_settings.ImportantSettings
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.local_storage.external.raw_models.DotaMatchesRaw
import com.erg.almagestdota.network.external.firebase.FirebaseRepository
import com.erg.almagestdota.settings.data.IOpenDotaContract
import com.erg.almagestdota.settings.data.ISteamContract
import com.erg.almagestdota.settings.domain.model.get_matches.OpenDotaMatch
import com.erg.almagestdota.settings.domain.model.get_matches.SteamDotaMatch
import com.erg.almagestdota.settings.domain.model.get_matches.SteamDotaMatchGameMode
import com.erg.almagestdota.settings.domain.model.get_matches.SteamDotaMatchLobbyType
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import kotlinx.coroutines.*
import javax.inject.Inject
import kotlin.coroutines.coroutineContext

class LoadOldMatchesUseCase @Inject constructor(
    private val steam: ISteamContract,
    private val firebaseFirestore: FirebaseFirestore,
    private val openDota: IOpenDotaContract,
    private val gson: Gson,
    private val loadDocumentMatchesUseCase: LoadDocumentMatchesUseCase,
) {

    suspend operator fun invoke(settings: ImportantSettings) {
        val dotaMatches = loadDocumentMatchesUseCase.invoke(settings.firstDocument)

        val openDotaMatches =
            openDota.loadMatches(dotaMatches.first().matchId - 10_000_000, dotaMatches.first().matchId)
        val opendotaMatchesFiltered = openDotaMatches.filter {
            it.tier != LeagueTierType.UNKNOWN && it.tier != LeagueTierType.AMATEUR &&
                it.radiantTeamId != 0L && it.direTeamId != 0L && (it.direScore + it.radiantScore) > 0
        }
        if (opendotaMatchesFiltered.isEmpty()) {
            throw IllegalStateException("0 матчей опендота")
        }
        val newMatches = getMatchesDetails(opendotaMatchesFiltered).sortedBy { it.matchId }
        if (newMatches.isEmpty()) {
            throw IllegalStateException("0 матчей стим")
        }
        val totalMatches = newMatches.plus(dotaMatches)

        updateRemoteMatches(
            totalMatches,
            settings.firstDocument,
            settings
        )
        Log.e("ALMATIP", "ALL DONE")
    }

    private suspend fun updateRemoteMatches(
        dotaMatches: List<DotaMatch>,
        documentsToIgnore: Int,
        settings: ImportantSettings
    ) {
        var matchList = dotaMatches.map { DotaMatch.toModel(it) }.sortedBy { it.match_id }

        var index = 0
        while (true) {
            if (matchList.size > MATCHES_PER_DOCUMENT) {
                val subList = matchList.takeLast(MATCHES_PER_DOCUMENT)
                matchList = matchList.subList(0, matchList.size - MATCHES_PER_DOCUMENT)
                val docRef = firebaseFirestore.collection(FirebaseRepository.MATCHES_DOCUMENT_NAME).document((documentsToIgnore - index).toString())
                docRef.set(DotaMatchesRaw(subList).asMap(gson))
            } else {
                if (matchList.isNotEmpty()) {
                    val docRef =
                        firebaseFirestore.collection(FirebaseRepository.MATCHES_DOCUMENT_NAME).document((documentsToIgnore - index).toString())
                    docRef.set(DotaMatchesRaw(matchList).asMap(gson))
                }
                setDocumentNumber(documentsToIgnore - index, settings)
                break
            }
            index++
        }
    }

    private fun setDocumentNumber(documentCount: Int, settings: ImportantSettings) {
        settings.firstDocument = documentCount
        val docRef = firebaseFirestore.collection(FirebaseRepository.MATCHES_DOCUMENT_NAME).document("important")
        docRef.set(ImportantSettings.toModel(settings))
    }

    private suspend fun doForSure(action: suspend () -> Unit) {
        while (true) {
            try {
                action.invoke()
                break
            } catch (e: Exception) {
                // HTTP 429, 403
                delay(100)
            }
        }
    }

    private suspend fun getMatchesDetails(
        openDotaMatches: List<OpenDotaMatch>
    ): List<DotaMatch> {
        val deferredList = mutableListOf<Deferred<DotaMatch?>>()

        for (match in openDotaMatches) {
            val deferred = CoroutineScope(coroutineContext).async {
                var steamDotaMatchNull: SteamDotaMatch? = null
                doForSure { steamDotaMatchNull = steam.getMatchDetails(match.matchId) }
                val steamDotaMatch = steamDotaMatchNull!!
                if (steamDotaMatch.lobbyType == SteamDotaMatchLobbyType.UNKNOWN ||
                    steamDotaMatch.gameMode == SteamDotaMatchGameMode.UNKNOWN ||
                    steamDotaMatch.radiantHeroes.size != GlobalVars.mainSettings.getPickSizeCM() ||
                    steamDotaMatch.direHeroes.size != GlobalVars.mainSettings.getPickSizeCM()
                ) {
                    return@async null
                }
                val dotaMatch = DotaMatch(
                    radiantScore = match.radiantScore,
                    direScore = match.direScore,
                    radiantTeamId = match.radiantTeamId,
                    direTeamId = match.direTeamId,
                    leagueId = match.leagueId,
                    radiantHeroes = steamDotaMatch.radiantHeroes,
                    direHeroes = steamDotaMatch.direHeroes,
                    startTime = match.startTime,
                    hasRadiantFirstPick = steamDotaMatch.hasRadiantFirstPick,
                    radiantBans = steamDotaMatch.radiantBans,
                    direBans = steamDotaMatch.direBans,
                    duration = steamDotaMatch.duration,
                    matchId = match.matchId,
                    seriesId = match.seriesId,
                    hasRadiantWon = steamDotaMatch.hasRadiantWon,
                    leagueTierType = match.tier,
                )
                dotaMatch
            }
            deferredList.add(deferred)
        }

        return deferredList.awaitAll().filterNotNull()
    }

    companion object {
         const val MATCHES_PER_DOCUMENT = 300
    }
}