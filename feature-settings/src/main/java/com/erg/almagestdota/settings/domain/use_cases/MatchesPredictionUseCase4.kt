package com.erg.almagestdota.settings.domain.use_cases

import com.erg.almagestdota.base.external.extensions.round1
import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.local_storage.external.models.draft.RoleType
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import javax.inject.Inject
import kotlin.math.absoluteValue

class MatchesPredictionUseCase4 @Inject constructor(
    private val saveMatchesUseCase: SaveMatchesUseCase,
    ) {

    private val winList = mutableListOf<Double>()
    private val loseList = mutableListOf<Double>()

    suspend operator fun invoke(): String {
        val dpcMatches = saveMatchesUseCase.readFromFile()

        var skippedCount = 0
        for (match in dpcMatches) {
            val radiantCarry = match.radiantHeroes.find { GlobalVars.players[it.playerId]?.role == RoleType.ROLE_1 }
            val direCarry = match.direHeroes.find { GlobalVars.players[it.playerId]?.role == RoleType.ROLE_1 }
            if (radiantCarry == null || direCarry == null)
            {
                skippedCount++
                continue
            }
            val wonHero = if (match.hasRadiantWon) radiantCarry else direCarry
            val lostHero = if (!match.hasRadiantWon) radiantCarry else direCarry
            val wonTeam = if (match.hasRadiantWon) match.radiantHeroes else match.direHeroes
            val lostTeam = if (!match.hasRadiantWon) match.radiantHeroes else match.direHeroes
            val valueWon = wonHero.heroId.getCounter(lostTeam.map { it.heroId }, GlobalVars.importantSettings.heroesProValues)
            val valueLost = lostHero.heroId.getCounter(wonTeam.map { it.heroId }, GlobalVars.importantSettings.heroesProValues)
            winList.add(valueWon)
            loseList.add(valueLost)
        }
        var string = "matches - ${dpcMatches.size} skipped - ${skippedCount}\n"


        return string + compareLists(winList, loseList, 1.0)
    }

    private fun compareLists(
        winList: MutableList<Double>,
        loseList: MutableList<Double>,
        increment: Double
    ): String {
        val mutableMap = mutableMapOf<Int, Statistics>()
        for (i in 0..100) {
            mutableMap[i] = Statistics()
        }
        for (index in winList.indices) {
            val winElem = winList[index]
            val loseElem = loseList[index]
            val valueDiff = (winElem - loseElem).round1()
            val mapValue = (valueDiff / increment).toInt()
            if (mapValue in 0..100 && valueDiff > 0) {
                mutableMap[mapValue.absoluteValue]?.apply {
                    winCount++
                }
            } else if (mapValue in -100..0 && valueDiff < 0) {
                mutableMap[mapValue.absoluteValue]?.apply {
                    loseCount++
                }
            }
        }
        var string = ""
        for (i in 0..100) {
            string += "interval:${i * increment}..${(i+1)*increment} %: ${getPercentage(mutableMap[i]!!.winCount, mutableMap[i]!!.loseCount)} w: ${mutableMap[i]!!.winCount} l: ${mutableMap[i]!!.loseCount}\n"
        }
        return string
    }

    class Statistics(
        var winCount: Int = 0,
        var loseCount: Int = 0,
    )


    private fun getPercentage(param1: Int, param2: Int) =
        if (param1 == 0 && param2 == 0) 0.0 else (param1 * 1.0 / (param1 + param2) * 100).round2()

}