package com.erg.almagestdota.settings.presentation.dataUpdater

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.settings.presentation.SettingsFragmentDirections

object DataUpdaterScreen : Screen<NavDirections>(
    route = SettingsFragmentDirections.toDataUpdaterFragment(),
    requestKey = DataUpdaterFragment.TAG
)