package com.erg.almagestdota.settings.domain.model.get_matches

import android.os.Parcelable
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerInfo
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SteamDotaMatch(
    val hasRadiantWon: Boolean,
    val hasRadiantFirstPick: Boolean,
    val duration: Long,
    val matchId: Long,
    val radiantHeroes: List<PlayerInfo>,
    val direHeroes: List<PlayerInfo>,
    val radiantBans: List<Int>,
    val direBans: List<Int>,
    val lobbyType: SteamDotaMatchLobbyType,
    val gameMode: SteamDotaMatchGameMode,
) : Parcelable