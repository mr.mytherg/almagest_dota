package com.erg.almagestdota.settings.presentation.custom_groups

import android.provider.Telephony.Mms.Draft
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.complexadapter.commonButtonItem
import com.erg.almagestdota.complexadapter.complexList
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.main_settings.DraftFilter
import com.erg.almagestdota.local_storage.external.models.main_settings.DraftFilterGroup
import com.erg.almagestdota.local_storage.external.models.main_settings.DraftFilterGroupType
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.settings.data.FiltersRepository
import com.erg.almagestdota.settings.presentation.custom_groups.edit_group.EditGroupFragment
import com.erg.almagestdota.settings.presentation.custom_groups.edit_group.EditGroupScreen
import com.erg.almagestdota.settings.presentation.custom_groups.edit_group.EditGroupViewActions
import com.erg.almagestdota.settings.presentation.custom_groups.edit_group.edit_filter.EditFilterFragment
import com.erg.almagestdota.settings.presentation.custom_groups.edit_group.edit_filter.EditFilterScreen
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

internal class GroupsViewModel @AssistedInject constructor(
    private val stringProvider: StringProvider,
    private val filtersRepository: FiltersRepository,
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data ->
        when (resultKey) {
            EditGroupFragment.TAG -> {
                when (EditGroupScreen.getResult(data)) {
                    EditGroupScreen.Result.EDITED -> {
                        val group = EditGroupScreen.getGroup(data)
                        stateConfigurator.groups[group.name] = group
                        saveGroups(stateConfigurator.groups)
                        viewStateMutable.value = stateConfigurator.defineFragmentState()
                    }
                    EditGroupScreen.Result.REMOVED -> {
                        val group = EditGroupScreen.getGroup(data)
                        stateConfigurator.groups[group.name] = null
                        saveGroups(stateConfigurator.groups)
                        viewStateMutable.value = stateConfigurator.defineFragmentState()
                    }
                    EditGroupScreen.Result.CANCELED -> Unit
                }
            }
        }
    }

    private fun saveGroups(groupsMap: MutableMap<String, DraftFilterGroup?>) {
        val groups = groupsMap.map { it.value }.filterNotNull()
        filtersRepository.updateGroups(groups)
    }

    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow<GroupsViewState?>(null)
    val viewState = viewStateMutable.asStateFlow()

    private val loadingStateMutable = MutableStateFlow<Loading>(Loading.Disabled)
    val loadingState = loadingStateMutable.asStateFlow()

    init {
        viewStateMutable.value = stateConfigurator.defineFragmentState()

        filtersRepository.getGroupsStateFlow().onEach {
            stateConfigurator.groups = it.associateBy { it.name }.toMutableMap()
            viewStateMutable.value = stateConfigurator.defineFragmentState()
        }.launchIn(viewModelScope)
    }

    fun obtainAction(action: GroupsViewActions) {
        when (action) {
            is GroupsViewActions.CreateGroup -> {
                val group = DraftFilterGroup(
                    name = "",
                    type = DraftFilterGroupType.MY_POOL,
                    filters = listOf()
                )
                val screen = EditGroupScreen(group)
                emit(viewEventMutable, ViewEvent.Navigation(screen))
            }
            is GroupsViewActions.EditGroup -> {
                val group = stateConfigurator.groups[action.key]!!
                val screen = EditGroupScreen(group)
                emit(viewEventMutable, ViewEvent.Navigation(screen))
            }
        }
    }

    private inner class StateConfigurator {
        var groups: MutableMap<String, DraftFilterGroup?> = mutableMapOf()

        fun defineFragmentState(): GroupsViewState {
            val complexGroups = complexList {
                for ((name, group) in groups) {
                    if (group != null) {
                        commonButtonItem(key = name) {
                            title = name
                        }
                    }
                }
            }
            return GroupsViewState.DefaultState(complexGroups)
        }
    }

    companion object {
        fun provideFactory(
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create(
                    ) as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(
        ): GroupsViewModel
    }
}