package com.erg.almagestdota.settings.domain.use_cases

import android.content.Context
import android.content.pm.PackageInfo
import android.util.Log
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.DotaItem
import com.erg.almagestdota.local_storage.external.models.draft.HeroItems
import com.erg.almagestdota.local_storage.external.models.draft.HeroStatsData
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.main_settings.MainSettings
import com.erg.almagestdota.network.external.firebase.FirebaseRepositoryContract
import com.google.firebase.firestore.FirebaseFirestore
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.lang.Exception
import javax.inject.Inject

class LoadItemsUseCase @Inject constructor(
    private val iLocalStorageContract: ILocalStorageContract,
    private val firebaseRepository: FirebaseRepositoryContract

) {
    suspend operator fun invoke(viewModelScope: CoroutineScope) {
        for (hero in GlobalVars.mainSettings.heroes) {
                if (GlobalVars.mainSettings.heroesItems[hero.id] == null) {
                    GlobalVars.mainSettings.heroesItems[hero.id] =
                        HeroItems(listOf(), listOf(), listOf(), listOf(), listOf(), listOf(), listOf(), listOf(), listOf(), listOf())
                }
                addAgainstHeroItems(hero)
            }

        iLocalStorageContract.setMainSettings(GlobalVars.mainSettings)
        firebaseRepository.setMainSettings(GlobalVars.mainSettings)
    }

    private suspend fun addAgainstHeroItems(hero: HeroStatsData) {
        var heroName = hero.name
        if (heroName.contains(" ")) {
            heroName = heroName.replace(' ', '-')
        }
        if (heroName.contains("'")) {
            heroName = heroName.replace("'", "")
        }
        heroName = heroName.lowercase()

        val url = "https://www.dotabuff.com/heroes/$heroName/items?date=patch_${GlobalVars.mainSettings.patch}"
        var doc: Document
        while (true) {
            try {
                doc = Jsoup.connect(url)
                    .header("Cookie",
                        "_tz=Asia%2FSaigon; _ga=GA1.2.1122684581.1669626336; __qca=P0-773539065-1669626336277; _hjSessionUser_2490228=eyJpZCI6Ijc0MGQ3Y2U2LWQwZjItNTgwZi1iNzkwLTMxZjA4OTJjNjhjNSIsImNyZWF0ZWQiOjE2Njk2MjYzMzYzMjgsImV4aXN0aW5nIjp0cnVlfQ==; _gid=GA1.2.2120575359.1671292247; _hjSession_2490228=eyJpZCI6IjRjMDczYWUwLWE2YjMtNDYyZC1iNjU2LTU3ZWY2ZWMxZWZmOCIsImNyZWF0ZWQiOjE2NzEzMzg0MTg3MjQsImluU2FtcGxlIjpmYWxzZX0=; _hjAbsoluteSessionInProgress=0; _hjIncludedInSessionSample=0; _hi=1671343654459")
                    .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.174 YaBrowser/22.1.3.848 Yowser/2.5 Safari/537.36")
                    .header("Connection", "keep-alive")
                    .header("sec-ch-ua", "\"Not?A_Brand\";v=\"8\", \"Chromium\";v=\"108\", \"Microsoft Edge\";v=\"108\"")
                    .timeout(0)
                    .get()
                break
            } catch (e: Exception) {
                Log.e("ERROR", "url |$url|")
                delay(300)
            }
        }

        val elements = doc.getElementsByAttributeValue("class", "sortable")
        val itemNames = mutableListOf<String>()
        for (count in 1..350) {
            val itemRow = elements.select("tr")?.getOrNull(count) ?: break
            val itemNameObj = itemRow.getElementsByTag("td")?.getOrNull(1)?.getElementsByTag("a")?.getOrNull(0)
            if (itemNameObj == null) {
                Log.e("ERROR", "itemNameObj |$itemNameObj|")
                continue
            }
            itemNames.add(itemNameObj.text())
        }

        val allHeroItems = mutableListOf<DotaItem>()
        itemNames.forEach { name ->
            var dotaItem = GlobalVars.mainSettings.items.find { it.name == name }
            if (dotaItem == null) {
                if (name.contains("Dagon")) {
                    dotaItem = GlobalVars.mainSettings.items.find { it.name == "Dagon" }!!
                    allHeroItems.add(dotaItem)
                } else {
                    Log.e("ERROR", "no item in dotaItems with name |$name|")
                }
            } else {
                allHeroItems.add(dotaItem)
            }
        }
        val mainItems = mutableListOf<DotaItem>()
        val situationalItems = mutableListOf<DotaItem>()
        val earlyItems = mutableListOf<DotaItem>()
        val neutral1 = mutableListOf<DotaItem>()
        val neutral2 = mutableListOf<DotaItem>()
        val neutral3 = mutableListOf<DotaItem>()
        val neutral4 = mutableListOf<DotaItem>()
        val neutral5 = mutableListOf<DotaItem>()
        allHeroItems.forEach {
            if (it.early && earlyItems.size < 4) {
                earlyItems.add(it)
            } else if (it.neutralTier == 1 && neutral1.size < 8) {
                neutral1.add(it)
            } else if (it.neutralTier == 2 && neutral2.size < 8) {
                neutral2.add(it)
            } else if (it.neutralTier == 3 && neutral3.size < 8) {
                neutral3.add(it)
            } else if (it.neutralTier == 4 && neutral4.size < 8) {
                neutral4.add(it)
            } else if (it.neutralTier == 5 && neutral5.size < 8) {
                neutral5.add(it)
            } else if (!it.early && it.neutralTier == 0 && !isRoshanItem(it)) {
                if (mainItems.size < 4) {
                    mainItems.add(it)
                } else if (situationalItems.size < 16) {
                    situationalItems.add(it)
                }
            }
        }

        GlobalVars.mainSettings.heroesItems[hero.id]!!.main = mainItems
        GlobalVars.mainSettings.heroesItems[hero.id]!!.situational = situationalItems
        GlobalVars.mainSettings.heroesItems[hero.id]!!.early = earlyItems
        GlobalVars.mainSettings.heroesItems[hero.id]!!.neutralTier1 = neutral1
        GlobalVars.mainSettings.heroesItems[hero.id]!!.neutralTier2 = neutral2
        GlobalVars.mainSettings.heroesItems[hero.id]!!.neutralTier3 = neutral3
        GlobalVars.mainSettings.heroesItems[hero.id]!!.neutralTier4 = neutral4
        GlobalVars.mainSettings.heroesItems[hero.id]!!.neutralTier5 = neutral5
        Log.i("INFO", "items loaded from |$url|")
    }

    private fun isRoshanItem(it: DotaItem): Boolean {
        return it.name == "Aegis of the Immortal" || it.name == "Cheese" || it.name == "Refresher Shard"
    }
}