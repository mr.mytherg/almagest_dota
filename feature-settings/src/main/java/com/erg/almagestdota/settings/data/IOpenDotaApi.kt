package com.erg.almagestdota.settings.data

import com.erg.almagestdota.drafting.data.OpenDotaPlayerResponse
import com.erg.almagestdota.settings.data.response.get_matches.OpenDotaMatchRaw
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface IOpenDotaApi {

    @GET("proMatches")
    suspend fun loadMatches(
        @Query("less_than_match_id") lessThanMatchId: Long?
    ): List<OpenDotaMatchRaw>

    @GET("players/{player_id}")
    suspend fun loadPlayer(
        @Path("player_id") playerId: Long,
    ): OpenDotaPlayerResponse
}