package com.erg.almagestdota.settings.presentation.custom_groups.edit_group

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.complexadapter.ComplexAdapter
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdota.settings.R
import com.erg.almagestdota.settings.databinding.CustomGroupsFragmentBinding
import com.erg.almagestdota.settings.databinding.EditGroupFragmentBinding
import com.erg.almagestdota.settings.presentation.custom_groups.edit_group.edit_filter.EditFilterFragment
import com.erg.almagestdota.settings.presentation.custom_groups.edit_group.edit_filter.EditFilterViewActions
import com.erg.almagestdpta.core_navigation.external.helpers.navHostFragmentManager
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
internal class EditGroupFragment : Fragment(R.layout.edit_group_fragment), BackPressListener {

    private val args by navArgs<EditGroupFragmentArgs>()

    companion object {
        const val TAG = "EditGroupFragment"
    }

    private val binding by viewBinding(EditGroupFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    @Inject
    lateinit var viewModelFactory: EditGroupViewModel.Factory
    private val viewModel by viewModels<EditGroupViewModel> {
        EditGroupViewModel.provideFactory(
            args.group,
            assistedFactory = viewModelFactory
        )
    }

    override fun onBackPressed() {
        navController.popBackStack()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
        viewModel.viewEvent.setResultListener(this)
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).filterNotNull().onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.loadingState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach {
            binding.loadingView.isVisible = it is Loading.Enabled
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private inner class ViewsConfigurator {

        fun initStartState() = with(binding) {

            ivBack.clicksWithDebounce {
                navController.popBackStack()
            }
            tvAddFilter.clicksWithDebounce {
                viewModel.obtainAction(EditGroupViewActions.AddFilter)
            }
            ivDelete.clicksWithDebounce {
                viewModel.obtainAction(EditGroupViewActions.RemoveGroup)
            }
            ivSubmit.clicksWithDebounce {
                viewModel.obtainAction(EditGroupViewActions.Submit(etName.text.toString().trim()))
            }

        }

        fun renderState(state: EditGroupViewState) = with(binding) {
            when (state) {
                is EditGroupViewState.DefaultState -> renderDefaultState(state)
            }
        }

        private fun renderDefaultState(state: EditGroupViewState.DefaultState) = with(binding) {
            etName.setText(args.group.name)
            if (state.filters.isEmpty()) {
                rvFilters.isVisible = false
                tvEmpty.isVisible = true
            } else {
                rvFilters.isVisible = true
                tvEmpty.isVisible = false
                rvFilters.adapter = ComplexAdapter(
                    complexListener = object : ComplexListener {
                        override fun onClick(item: ComplexItem) {
                            viewModel.obtainAction(EditGroupViewActions.EditFilter(item.key))
                        }
                    }
                ).apply {
                    submitList(state.filters)
                }
            }
        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
                is ViewEvent.PopBackStack<*> -> {
                    val fm = navHostFragmentManager()
                    navController.popBackStack()
                    fm.setFragmentResult(TAG, event.data as Bundle)
                }
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            viewModel.viewEvent.setResultListener(this@EditGroupFragment)
            navController.navigateViaScreenRoute(screen)
        }
    }
}