package com.erg.almagestdota.settings.domain.use_cases

import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.HeroRole
import com.erg.almagestdota.local_storage.external.models.draft.RoleType
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.network.external.firebase.FirebaseRepositoryContract
import com.erg.almagestdota.drafting.data.IStratzGraphqlApi
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject

class LoadRolesUseCase @Inject constructor(
    private val stratzGraphqlApi: IStratzGraphqlApi,
    private val iLocalStorageContract: ILocalStorageContract,
    private val firebaseRepository: FirebaseRepositoryContract
) {
    private fun getQueryWithId(pos: Int): String {
        val posText = when (pos) {
            1 -> "POSITION_1"
            2 -> "POSITION_2"
            3 -> "POSITION_3"
            4 -> "POSITION_4"
            else -> "POSITION_5"
        }
        return "{heroStats{winMonth(heroIds:[${GlobalVars.getAllIds()}],bracketIds:[IMMORTAL],positionIds:[$posText]){heroId,winCount,matchCount}}}"
    }

    suspend operator fun invoke(viewModelScope: CoroutineScope) {
        loadRole()
        iLocalStorageContract.setMainSettings(GlobalVars.mainSettings)
        firebaseRepository.setMainSettings(GlobalVars.mainSettings)
    }

    private suspend fun loadRole() {
        val mutableMap = mutableMapOf<Int, MutableList<HeroRole>>()
        for (hero in GlobalVars.mainSettings.heroes) {
            mutableMap[hero.id] = mutableListOf(
                HeroRole(roleType = RoleType.ROLE_1, winRatePercentage = 0.0, pickRate = 0.0, matchCount = 0),
                HeroRole(roleType = RoleType.ROLE_2, winRatePercentage = 0.0, pickRate = 0.0, matchCount = 0),
                HeroRole(roleType = RoleType.ROLE_3, winRatePercentage = 0.0, pickRate = 0.0, matchCount = 0),
                HeroRole(roleType = RoleType.ROLE_4, winRatePercentage = 0.0, pickRate = 0.0, matchCount = 0),
                HeroRole(roleType = RoleType.ROLE_5, winRatePercentage = 0.0, pickRate = 0.0, matchCount = 0),
            )
        }
        for (pos in 1..5) {
            val result = stratzGraphqlApi.getHeroRoles(getQueryWithId(pos))
            val grouped = result.data.heroStats.winMonth.groupBy { it.heroId }
            for (item in grouped) {
                val reduced = item.value.reduce { acc, roleRaw ->
                    acc.matchCount += roleRaw.matchCount
                    acc.winCount += roleRaw.winCount
                    acc
                }
                val hero = mutableMap[item.key]!!
                val role = hero.get(pos - 1)
                hero.removeAt(pos - 1)
                role.matchCount = reduced.matchCount
                role.winCount = reduced.winCount
                hero.add(pos - 1, role)
                mutableMap[item.key] = hero
            }
        }
        for ((k, v) in mutableMap) {
            val totalMatchCount = v.fold(0) { acc, next -> acc + next.matchCount }
            val role1 = getRole(RoleType.ROLE_1, v[0], totalMatchCount)
            val role2 = getRole(RoleType.ROLE_2, v[1], totalMatchCount)
            val role3 = getRole(RoleType.ROLE_3, v[2], totalMatchCount)
            val role4 = getRole(RoleType.ROLE_4, v[3], totalMatchCount)
            val role5 = getRole(RoleType.ROLE_5, v[4], totalMatchCount)
            GlobalVars.mainSettings.heroes.find { it.id == k }?.roles = listOf(
                role1,
                role2,
                role3,
                role4,
                role5
            )
        }
    }

    private fun getRole(role: RoleType, heroRole: HeroRole, totalMatchCount: Int): HeroRole {
        return HeroRole(
            roleType = role,
            winRatePercentage = (heroRole.winCount * 1.0 / heroRole.matchCount * 100).round2(),
            pickRate = (heroRole.matchCount * 1.0 / totalMatchCount * 100).round2(),
            matchCount = 0
        )
    }
}