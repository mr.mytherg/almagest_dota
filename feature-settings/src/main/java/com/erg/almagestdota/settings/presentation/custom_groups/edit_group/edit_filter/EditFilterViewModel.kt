package com.erg.almagestdota.settings.presentation.custom_groups.edit_group.edit_filter

import android.provider.Telephony.Mms.Draft
import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.NotificationStatus
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.complexadapter.complexList
import com.erg.almagestdota.complexadapter.itemDefault
import com.erg.almagestdota.heroselector.presentation.heroSelector.HeroSelectorScreen
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.main_settings.DraftFilter
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import com.erg.almagestdota.settings.R
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.selects.select

internal class EditFilterViewModel @AssistedInject constructor(
    @Assisted private val filter: DraftFilter,
    private val stringProvider: StringProvider,
    private val localStorage: ILocalStorageContract,
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data ->
        when (resultKey) {
            HeroSelectorScreen.TAG -> {
                if (HeroSelectorScreen.getResult(data) == HeroSelectorScreen.Result.SELECTED) {
                    val heroId = HeroSelectorScreen.getSelectedHero(data)
                    if (stateConfigurator.mutableMapHeroes[heroId] == false) {
                        selectHero(heroId)
                    } else {
                        showAlreadySelectedError(heroId)
                    }
                }
            }
        }
    }

    private fun showAlreadySelectedError(heroId: Int) {
        val text = stringProvider.getString(R.string.hero_has_selected)
        emit(viewEventMutable, ViewEvent.AlmagestSnackbar(text, NotificationStatus.ERROR))
    }

    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow<EditFilterViewState?>(null)
    val viewState = viewStateMutable.asStateFlow()

    private val loadingStateMutable = MutableStateFlow<Loading>(Loading.Disabled)
    val loadingState = loadingStateMutable.asStateFlow()

    init {
        viewStateMutable.value = stateConfigurator.defineFragmentState()
    }

    fun obtainAction(action: EditFilterViewActions) {
        when (action) {
            is EditFilterViewActions.RemoveFilter -> {
                val result = EditFilterScreen.Result.createResult(EditFilterScreen.Result.REMOVED, filter)
                emit(viewEventMutable, ViewEvent.PopBackStack.Data(result))
            }
            is EditFilterViewActions.SelectHero -> {
                val heroId = action.key.toInt()
                selectHero(heroId)
            }
            is EditFilterViewActions.Submit -> {
                if (action.name.isEmpty()) {
                    val text = stringProvider.getString(R.string.empty_name)
                    emit(viewEventMutable, ViewEvent.AlmagestSnackbar(text, NotificationStatus.ERROR))
                    return
                }
                filter.name = action.name
                filter.heroes = stateConfigurator.mutableMapHeroes.filter { it.value }.map {
                    it.key
                }.sorted()
                if (filter.heroes.isEmpty()) {
                    val text = stringProvider.getString(R.string.empty_heroes_list)
                    emit(viewEventMutable, ViewEvent.AlmagestSnackbar(text, NotificationStatus.ERROR))
                    return
                }
                val result = EditFilterScreen.Result.createResult(EditFilterScreen.Result.EDITED, filter)
                emit(viewEventMutable, ViewEvent.PopBackStack.Data(result))
            }
            is EditFilterViewActions.OpenSearch -> {
                val screen = HeroSelectorScreen()
                emit(viewEventMutable, ViewEvent.Navigation(screen))
            }
            is EditFilterViewActions.UnselectHero -> {
                val heroId = action.key.toInt()
                stateConfigurator.mutableMapHeroes[heroId] = false
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
        }
    }

    private fun selectHero(heroId: Int) {
        stateConfigurator.mutableMapHeroes[heroId] = !stateConfigurator.mutableMapHeroes[heroId]!!
        viewStateMutable.value = stateConfigurator.defineFragmentState()
    }

    private inner class StateConfigurator {
        var mutableMapHeroes = mutableMapOf<Int, Boolean>()
        private val heroes = GlobalVars.mainSettings.heroes.sortedBy { it.name }

        init {
            for (hero in GlobalVars.mainSettings.heroes) {
                mutableMapHeroes[hero.id] = filter.heroes.find { it == hero.id } != null
            }
        }

        fun defineFragmentState(): EditFilterViewState {
            val complexSelectedHeroes = complexList {
                for (hero in heroes) {
                    val isSelected = mutableMapHeroes[hero.id]!!
                    if (isSelected)
                        itemDefault(key = hero.id.toString()) {
                            hasBlackout = false
                            title = hero.name
                            url = hero.imageLink
                        }
                }
            }
            val complexAllHeroes = complexList {
                for (hero in heroes) {
                    val isSelected = mutableMapHeroes[hero.id]!!
                    itemDefault(key = hero.id.toString()) {
                        hasBlackout = isSelected
                        title =hero.name
                        url = hero.imageLink
                    }
                }
            }
            return EditFilterViewState.DefaultState(
                complexSelectedHeroes,
                complexAllHeroes
            )
        }
    }

    companion object {
        fun provideFactory(
            filter: DraftFilter,
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create(
                        filter
                    ) as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(
            filter: DraftFilter,
        ): EditFilterViewModel
    }
}