package com.erg.almagestdota.settings.domain.model.get_matches

import android.os.Parcelable
import com.erg.almagestdota.local_storage.external.models.leagues.LeagueTierType
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OpenDotaMatch(
    val matchId: Long,
    val radiantTeamId: Long,
    val direTeamId: Long,
    val leagueId: Long,
    val seriesId: Long,
    val radiantScore: Int,
    val direScore: Int,
    val startTime: Long,
    val tier: LeagueTierType,
) : Parcelable