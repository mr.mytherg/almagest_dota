package com.erg.almagestdota.settings.presentation.custom_groups.edit_group.edit_filter

internal sealed class EditFilterViewActions {

    object OpenSearch : EditFilterViewActions()
    class SelectHero(val key: String) : EditFilterViewActions()
    class UnselectHero(val key: String) : EditFilterViewActions()
    object RemoveFilter : EditFilterViewActions()
    class Submit(val name: String) : EditFilterViewActions()
}