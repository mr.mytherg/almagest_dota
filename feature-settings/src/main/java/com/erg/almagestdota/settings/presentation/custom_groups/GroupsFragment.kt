package com.erg.almagestdota.settings.presentation.custom_groups

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.complexadapter.ComplexAdapter
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdota.settings.R
import com.erg.almagestdota.settings.databinding.CustomGroupsFragmentBinding
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.edit_group_fragment.*
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
internal class GroupsFragment : Fragment(R.layout.custom_groups_fragment), BackPressListener {

    companion object {
        const val TAG = "GroupsFragment"
    }

    private val binding by viewBinding(CustomGroupsFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    @Inject
    lateinit var viewModelFactory: GroupsViewModel.Factory
    private val viewModel by viewModels<GroupsViewModel> {
        GroupsViewModel.provideFactory(
            assistedFactory = viewModelFactory
        )
    }

    override fun onBackPressed() {
        navController.popBackStack()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
        viewModel.viewEvent.setResultListener(this)
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).filterNotNull().onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.loadingState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach {
            binding.loadingView.isVisible = it is Loading.Enabled
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private inner class ViewsConfigurator {

        fun initStartState() = with(binding) {

            ivBack.clicksWithDebounce {
                navController.popBackStack()
            }
            tvCreateGroup.clicksWithDebounce {
                viewModel.obtainAction(GroupsViewActions.CreateGroup)
            }

        }

        fun renderState(state: GroupsViewState) = with(binding) {
            when (state) {
                is GroupsViewState.DefaultState -> renderDefaultState(state)
            }
        }

        private fun renderDefaultState(state: GroupsViewState.DefaultState) = with(binding) {
            if (state.groups.isEmpty()) {
                tvEmpty.isVisible = true
                tvTip.isVisible = false
                rvGroups.isVisible = false
            } else {
                tvEmpty.isVisible = false
                tvTip.isVisible = true
                rvGroups.isVisible = true
                rvGroups.adapter = ComplexAdapter(
                    complexListener = object : ComplexListener {
                        override fun onClick(item: ComplexItem) {
                            viewModel.obtainAction(GroupsViewActions.EditGroup(item.key))
                        }
                    }
                ).apply {
                    submitList(state.groups)
                }
            }
        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            viewModel.viewEvent.setResultListener(this@GroupsFragment)
            navController.navigateViaScreenRoute(screen)
        }
    }
}