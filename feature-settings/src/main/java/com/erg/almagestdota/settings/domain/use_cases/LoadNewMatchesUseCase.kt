package com.erg.almagestdota.settings.domain.use_cases

import com.erg.almagestdota.drafting.domain.use_cases.LoadPlayerInfoUseCase
import com.erg.almagestdota.drafting.domain.use_cases.LoadTeamInfoStratzUseCase
import com.erg.almagestdota.drafting.domain.use_cases.LoadTeamInfoUseCase
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.main.MemberInfo
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerInfo
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamInfo
import com.erg.almagestdota.local_storage.external.models.leagues.LeagueTierType
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.main_settings.ImportantSettings
import com.erg.almagestdota.local_storage.external.models.main_settings.TeamsSettings
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.network.external.firebase.FirebaseRepositoryContract
import com.erg.almagestdota.settings.data.IOpenDotaContract
import com.erg.almagestdota.settings.data.ISteamContract
import com.erg.almagestdota.settings.domain.model.get_matches.OpenDotaMatch
import com.erg.almagestdota.settings.domain.model.get_matches.SteamDotaMatch
import com.erg.almagestdota.settings.domain.model.get_matches.SteamDotaMatchGameMode
import com.erg.almagestdota.settings.domain.model.get_matches.SteamDotaMatchLobbyType
import kotlinx.coroutines.*
import javax.inject.Inject
import kotlin.coroutines.coroutineContext

class LoadNewMatchesUseCase @Inject constructor(
    private val steam: ISteamContract,
    private val firebaseRepository: FirebaseRepositoryContract,
    private val openDota: IOpenDotaContract,
    private val loadPlayerInfoUseCase: LoadPlayerInfoUseCase,
    private val loadTeamInfoUseCase: LoadTeamInfoUseCase,
    private val loadDocumentMatchesUseCase: LoadDocumentMatchesUseCase,
    private val loadTeamInfoStratzUseCase: LoadTeamInfoStratzUseCase,
    private val calibrateMatchesUseCase: CalibrateMatchesUseCase,
    private val iLocalStorageContract: ILocalStorageContract,
    private val loadMatchLaneStatusUseCase: LoadMatchLaneStatusUseCase,
) {

    suspend operator fun invoke(settings: ImportantSettings): List<DotaMatch> {
        var dotaMatches = loadDocumentMatchesUseCase.invoke(settings.lastDocument - 1).plus(
            loadDocumentMatchesUseCase.invoke(settings.lastDocument)
        )
        dotaMatches = loadMatchLaneStatusUseCase.invoke(dotaMatches)

        val openDotaMatches = if (dotaMatches.isEmpty()) {
            openDota.loadMatches()
        } else {
            openDota.loadMatches(dotaMatches.last().matchId)//что делать если матч начался позже но закончился раньше?
        }
        val opendotaMatchesFiltered = openDotaMatches.filter {
            it.tier != LeagueTierType.UNKNOWN && it.tier != LeagueTierType.AMATEUR &&
                it.radiantTeamId != 0L && it.direTeamId != 0L && (it.direScore + it.radiantScore) > 0
        }
        if (opendotaMatchesFiltered.isEmpty()) {
            updateRemoteMatches(
                dotaMatches,
                settings.lastDocument - 1,
                settings
            )
            throw IllegalStateException("0 матчей опендота")
        }
        val uniqueNewMatches = getUniqueMatches(opendotaMatchesFiltered, dotaMatches)
        val newMatches = getMatchesDetails(uniqueNewMatches).sortedBy { it.matchId }
        if (newMatches.isEmpty()) {
            updateRemoteMatches(
                dotaMatches,
                settings.lastDocument - 1,
                settings
            )
            throw IllegalStateException("0 матчей стим")
        }
        val totalMatches = dotaMatches.plus(newMatches)

        val players = getPlayers(newMatches)
        calibrateMatchesUseCase.invoke(newMatches, players)
        updateTeams(newMatches)

        updateRemoteMatches(
            totalMatches,
            settings.lastDocument - 1,
            settings
        )
        iLocalStorageContract.setMainPredictor(GlobalVars.mainPredictor)
        firebaseRepository.setMainPredictor(GlobalVars.mainPredictor)
        firebaseRepository.setTeamsSettings(TeamsSettings(GlobalVars.teams.map { it.value }))

        return newMatches
    }

    private fun getUniqueMatches(loadedMatches: List<OpenDotaMatch>, oldMatches: List<DotaMatch>): List<OpenDotaMatch> {
        val uniqueMatches = mutableMapOf<Long, OpenDotaMatch>()
        for (match in loadedMatches) {
            if (oldMatches.first().matchId < match.matchId && oldMatches.find { it.matchId == match.matchId } == null)
                uniqueMatches[match.matchId] = match
        }
        return uniqueMatches.toList().map { it.second }.sortedBy { it.matchId }
    }

    private suspend fun getPlayers(newMatches: List<DotaMatch>): List<MemberInfo> {
        val playerIds = mutableSetOf<Long>()
        for (match in newMatches) {
            for (player in match.radiantHeroes)
                playerIds.add(player.playerId)
            for (player in match.direHeroes)
                playerIds.add(player.playerId)
        }
        val players = mutableSetOf<MemberInfo>()
        for (playerId in playerIds) {
            doForSure {
                players.add(GlobalVars.players[playerId]  ?: loadPlayerInfoUseCase.invoke(playerId))
            }
        }

        return players.toList()
    }

    private suspend fun updateTeams(newMatches: List<DotaMatch>) {
        val teams = mutableSetOf<Long>()
        for (match in newMatches) {
            teams.add(match.radiantTeamId)
            teams.add(match.direTeamId)
        }

        val deferredList = mutableListOf<Deferred<Unit>>()
        for (teamId in teams) {
            val deferred = CoroutineScope(coroutineContext).async(Dispatchers.IO) {
                var team = loadTeamInfoUseCase.invoke(teamId)
                val lastMatch = newMatches.last()
                val players = if (teamId == lastMatch.direTeamId) {
                    lastMatch.direHeroes
                } else {
                    lastMatch.radiantHeroes
                }
                if (!allTeamMembers(players, team.members)) {
                    team = loadTeamInfoStratzUseCase.invoke(team.id)
                }
                GlobalVars.teams[teamId]?.apply {
                    rating = getTeamRating(players)
                    matchCount = team.matchCount
                }
                Unit
            }
            deferredList.add(deferred)
        }
        deferredList.awaitAll()
        firebaseRepository.setTeamsSettings(TeamsSettings(GlobalVars.teams.map { it.value }))
    }

    private fun getTeamRating(players: List<PlayerInfo>): Int {
        return players.fold(0) { acc, next -> acc + (GlobalVars.players[next.playerId]?.rating ?: 1400) } / 5
    }

    private fun allTeamMembers(players: List<PlayerInfo>, members: List<Long>): Boolean {
        for (player in players) {
            if (members.find { player.playerId == it } == null) {
                return false
            }
        }
        return true
    }

    private suspend fun updateRemoteMatches(
        dotaMatches: List<DotaMatch>,
        documentsToIgnore: Int,
        settings: ImportantSettings
    ) {
        var matchList = dotaMatches.sortedBy { it.matchId }

        var index = 0
        while (true) {
            if (matchList.size > MATCHES_PER_DOCUMENT) {
                val subList = matchList.take(MATCHES_PER_DOCUMENT)
                matchList = matchList.subList(MATCHES_PER_DOCUMENT, matchList.size)
                firebaseRepository.setMatchesPage(documentsToIgnore + index, subList)
            } else {
                if (matchList.isNotEmpty())
                    firebaseRepository.setMatchesPage(documentsToIgnore + index, matchList)
                setDocumentNumber(documentsToIgnore + index, settings)
                break
            }
            index++
        }
    }

    private suspend fun setDocumentNumber(documentCount: Int, settings: ImportantSettings) {
        settings.lastDocument = documentCount
        firebaseRepository.setImportantSettings(settings)
    }

    private suspend fun doForSure(action: suspend () -> Unit) {
        while (true) {
            try {
                action.invoke()
                break
            } catch (e: Exception) {
                // HTTP 429, 403
                delay(100)
            }
        }
    }

    private suspend fun getMatchesDetails(
        openDotaMatches: List<OpenDotaMatch>
    ): List<DotaMatch> {
        val deferredList = mutableListOf<Deferred<DotaMatch?>>()

        for (match in openDotaMatches) {
            val deferred = CoroutineScope(coroutineContext).async(Dispatchers.IO) {
                var steamDotaMatchNull: SteamDotaMatch? = null
                doForSure { steamDotaMatchNull = steam.getMatchDetails(match.matchId) }
                val steamDotaMatch = steamDotaMatchNull!!
                if (steamDotaMatch.lobbyType == SteamDotaMatchLobbyType.UNKNOWN ||
                    steamDotaMatch.gameMode == SteamDotaMatchGameMode.UNKNOWN ||
                    steamDotaMatch.radiantHeroes.size != GlobalVars.mainSettings.getPickSizeCM() ||
                    steamDotaMatch.direHeroes.size != GlobalVars.mainSettings.getPickSizeCM()
                ) {
                    return@async null
                }
                val dotaMatch = DotaMatch(
                    radiantScore = match.radiantScore,
                    direScore = match.direScore,
                    radiantTeamId = match.radiantTeamId,
                    leagueId = match.leagueId,
                    direTeamId = match.direTeamId,
                    radiantHeroes = steamDotaMatch.radiantHeroes,
                    direHeroes = steamDotaMatch.direHeroes,
                    startTime = match.startTime,
                    hasRadiantFirstPick = steamDotaMatch.hasRadiantFirstPick,
                    radiantBans = steamDotaMatch.radiantBans,
                    direBans = steamDotaMatch.direBans,
                    duration = steamDotaMatch.duration,
                    matchId = match.matchId,
                    seriesId = match.seriesId,
                    hasRadiantWon = steamDotaMatch.hasRadiantWon,
                    leagueTierType = match.tier,
                )
                dotaMatch
            }
            deferredList.add(deferred)
        }

        return deferredList.awaitAll().filterNotNull().sortedByDescending { it.matchId }
    }

    companion object {
        private const val MATCHES_PER_DOCUMENT = 300
    }
}