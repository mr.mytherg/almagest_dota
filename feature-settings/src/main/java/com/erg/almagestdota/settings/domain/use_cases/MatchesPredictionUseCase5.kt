package com.erg.almagestdota.settings.domain.use_cases

import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.extensions.round1
import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.RoleType
import com.erg.almagestdota.local_storage.external.models.leagues.LeagueTierType
import com.erg.almagestdota.local_storage.external.models.live.LeagueGame
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import javax.inject.Inject
import kotlin.math.absoluteValue
import kotlin.math.atan

class MatchesPredictionUseCase5 @Inject constructor(
    private val localStorage: ILocalStorageContract,
) {


    suspend operator fun invoke(): String {
        val dpcMatches = localStorage.metaMatches
        val mutableMap = mutableMapOf<Long, TeamStatus>()
        val teams = mutableSetOf<Long>()
        for (match in dpcMatches) {
            teams.add(match.radiantTeamId)
            teams.add(match.direTeamId)
        }
        for (team in teams) {
            mutableMap[team] = TeamStatus()
        }

        for (match in dpcMatches) {
            mutableMap[match.radiantTeamId]?.apply {
                list.add(Pair(match.direTeamId, if (match.hasRadiantWon) 1 else 0))
            }
            mutableMap[match.direTeamId]?.apply {
                list.add(Pair(match.radiantTeamId, if (!match.hasRadiantWon) 1 else 0))
            }
        }

        for (team in teams) {
            mutableMap[team]?.apply {
                val last = list.takeLast(14)
                wins = last.count { it.second == 1 }
            }
        }

        for (team in teams) {
            mutableMap[team]?.apply {
                val last = list.takeLast(14)
                for (match in last)
                    bugholtz += mutableMap[match.first]!!.wins
            }
        }

        val sortedByBughholtz = mutableMap.toList()
            .sortedWith(
                compareByDescending<Pair<Long , TeamStatus>> { it.second.wins }
                    .thenByDescending { it.second.bugholtz }
            )
            .sortedByDescending { it.second.wins }
            .filter {
                it.second.list.size >= 14
            }

        var string = ""
        for (team in sortedByBughholtz) {
            val teamName = GlobalVars.teams[team.first]?.name.orDefault().ifEmpty {
                team.first
            }
            string += "$teamName wins: ${team.second.wins} coef: ${team.second.bugholtz}\n"
        }

        return string
    }
}

class TeamStatus(
    var bugholtz: Int = 0,
    var wins: Int = 0,
    var list: MutableList<Pair<Long, Int>> = mutableListOf(),
)