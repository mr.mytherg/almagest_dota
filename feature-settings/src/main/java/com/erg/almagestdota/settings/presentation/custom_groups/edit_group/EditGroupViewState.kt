package com.erg.almagestdota.settings.presentation.custom_groups.edit_group

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem

internal sealed class EditGroupViewState : ViewState() {
    class DefaultState(
        val filters: List<ComplexItem>
    ) : EditGroupViewState()
}