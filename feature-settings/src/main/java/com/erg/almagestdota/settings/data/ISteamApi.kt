package com.erg.almagestdota.settings.data

import com.erg.almagestdota.settings.data.response.get_matches.SteamMatchDetailsRaw
import retrofit2.http.GET
import retrofit2.http.Query

interface ISteamApi {

    companion object {
        private const val STEAM_API = "/IDOTA2Match_570"
        private const val STEAM_API_KEY = "47A353711F576A647ED1AB6C8B25B1D7" // todo в gradle
    }

    @GET("$STEAM_API/GetMatchDetails/V001/")
    suspend fun getMatchDetails(
        @Query("key") key: String = STEAM_API_KEY,
        @Query("match_id") matchId: String,
    ): SteamMatchDetailsRaw
}