package com.erg.almagestdota.settings.domain.use_cases

import com.erg.almagestdota.drafting.data.IStratzGraphqlApi
import com.erg.almagestdota.drafting.data.raw.MatchInfoRaw
import com.erg.almagestdota.drafting.domain.use_cases.LoadPlayerInfoUseCase
import com.erg.almagestdota.drafting.domain.use_cases.LoadTeamInfoStratzUseCase
import com.erg.almagestdota.drafting.domain.use_cases.LoadTeamInfoUseCase
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.RoleType
import com.erg.almagestdota.local_storage.external.models.draft.main.LaneStatusType
import com.erg.almagestdota.local_storage.external.models.draft.main.MemberInfo
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerInfo
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamInfo
import com.erg.almagestdota.local_storage.external.models.leagues.LeagueTierType
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.main_settings.ImportantSettings
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.network.external.firebase.FirebaseRepository
import com.erg.almagestdota.network.external.firebase.FirebaseRepositoryContract
import com.erg.almagestdota.predictor.domain.use_cases.Role
import com.erg.almagestdota.settings.data.IOpenDotaContract
import com.erg.almagestdota.settings.data.ISteamContract
import com.erg.almagestdota.settings.domain.model.get_matches.OpenDotaMatch
import com.erg.almagestdota.settings.domain.model.get_matches.SteamDotaMatch
import com.erg.almagestdota.settings.domain.model.get_matches.SteamDotaMatchGameMode
import com.erg.almagestdota.settings.domain.model.get_matches.SteamDotaMatchLobbyType
import kotlinx.coroutines.*
import javax.inject.Inject
import kotlin.coroutines.coroutineContext

class LoadMatchLaneStatusUseCase @Inject constructor(
    private val stratzApi: IStratzGraphqlApi
) {
    suspend operator fun invoke(dotaMatches: List<DotaMatch>): List<DotaMatch> {
        var ids = ""
        var count = 0
        val deferredList = mutableListOf<List<MatchInfoRaw>>()
        for (dotaMatch in dotaMatches) {
            if (count < 10) {
                if (dotaMatch.radiantHeroes[0].laneStatus == LaneStatusType.UNDEFINED) {
                    if (ids.isNotEmpty())
                        ids += ","
                    ids += dotaMatch.matchId
                    count++
                }
                if (dotaMatch.matchId == dotaMatches.last().matchId) {
                    deferredList.add(loadData(ids))
                }
            } else {
                deferredList.add(loadData(ids))
                delay(500)
                ids = ""
                count = 0
            }
        }
        val loadedMatches = deferredList.flatMap { it }
        dotaMatches.map { match ->
            val laneOutcome = loadedMatches.find { it.id == match.matchId }
            if (laneOutcome != null) {
                match.radiantHeroes.map {
                    when (GlobalVars.players[it.playerId]?.role) {
                        RoleType.ROLE_1 -> it.laneStatus = LaneStatusType.byLaneStatusTypeStratz(laneOutcome.bottomLaneOutcome)
                        RoleType.ROLE_2 -> it.laneStatus = LaneStatusType.byLaneStatusTypeStratz(laneOutcome.midLaneOutcome)
                        RoleType.ROLE_3 -> it.laneStatus = LaneStatusType.byLaneStatusTypeStratz(laneOutcome.topLaneOutcome)
                        RoleType.ROLE_4 -> it.laneStatus = LaneStatusType.byLaneStatusTypeStratz(laneOutcome.topLaneOutcome)
                        RoleType.ROLE_5 -> it.laneStatus = LaneStatusType.byLaneStatusTypeStratz(laneOutcome.bottomLaneOutcome)
                        else -> Unit
                    }
                }
                match.direHeroes.map {
                    when (GlobalVars.players[it.playerId]?.role) {
                        RoleType.ROLE_1 -> it.laneStatus = LaneStatusType.byLaneStatusTypeStratz(laneOutcome.topLaneOutcome)
                        RoleType.ROLE_2 -> it.laneStatus = LaneStatusType.byLaneStatusTypeStratz(laneOutcome.midLaneOutcome)
                        RoleType.ROLE_3 -> it.laneStatus = LaneStatusType.byLaneStatusTypeStratz(laneOutcome.bottomLaneOutcome)
                        RoleType.ROLE_4 -> it.laneStatus = LaneStatusType.byLaneStatusTypeStratz(laneOutcome.bottomLaneOutcome)
                        RoleType.ROLE_5 -> it.laneStatus = LaneStatusType.byLaneStatusTypeStratz(laneOutcome.topLaneOutcome)
                        else -> Unit
                    }
                }
            }
        }
        return dotaMatches
    }

    private suspend fun loadData(ids: String): List<MatchInfoRaw> {
        val query = "{matches(ids:[$ids]){topLaneOutcome,id,bottomLaneOutcome,midLaneOutcome}}"
        val result = stratzApi.getMatchesLanes(query)
        return result.data?.matches.orEmpty()
    }
}