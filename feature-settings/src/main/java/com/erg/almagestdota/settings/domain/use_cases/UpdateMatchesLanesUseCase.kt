package com.erg.almagestdota.settings.domain.use_cases

import com.erg.almagestdota.drafting.domain.use_cases.LoadPlayerInfoUseCase
import com.erg.almagestdota.drafting.domain.use_cases.LoadTeamInfoStratzUseCase
import com.erg.almagestdota.drafting.domain.use_cases.LoadTeamInfoUseCase
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.main.MemberInfo
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerInfo
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamInfo
import com.erg.almagestdota.local_storage.external.models.leagues.LeagueTierType
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.main_settings.ImportantSettings
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.network.external.firebase.FirebaseRepositoryContract
import com.erg.almagestdota.settings.data.IOpenDotaContract
import com.erg.almagestdota.settings.data.ISteamContract
import com.erg.almagestdota.settings.domain.model.get_matches.OpenDotaMatch
import com.erg.almagestdota.settings.domain.model.get_matches.SteamDotaMatch
import com.erg.almagestdota.settings.domain.model.get_matches.SteamDotaMatchGameMode
import com.erg.almagestdota.settings.domain.model.get_matches.SteamDotaMatchLobbyType
import com.erg.almagestdota.settings.domain.use_cases.LoadOldMatchesUseCase.Companion.MATCHES_PER_DOCUMENT
import kotlinx.coroutines.*
import javax.inject.Inject
import kotlin.coroutines.coroutineContext

class UpdateMatchesLanesUseCase @Inject constructor(
    private val firebaseRepository: FirebaseRepositoryContract,
    private val loadDocumentMatchesUseCase: LoadDocumentMatchesUseCase,
    private val loadMatchLaneStatusUseCase: LoadMatchLaneStatusUseCase,
) {

    suspend operator fun invoke(settings: ImportantSettings) {
        val docStart = 983
        for (i in 0 until 10) {
            val dotaMatches = loadDocumentMatchesUseCase.invoke(docStart - i)
            updateRemoteMatches(
                loadMatchLaneStatusUseCase.invoke(dotaMatches),
                docStart - i,
                settings
            )
        }
    }

    private suspend fun updateRemoteMatches(
        dotaMatches: List<DotaMatch>,
        documentsToIgnore: Int,
        settings: ImportantSettings
    ) {
        var matchList = dotaMatches.sortedBy { it.matchId }

        var index = 0
        while (true) {
            if (matchList.size > MATCHES_PER_DOCUMENT) {
                val subList = matchList.take(MATCHES_PER_DOCUMENT)
                matchList = matchList.subList(MATCHES_PER_DOCUMENT, matchList.size)
                firebaseRepository.setMatchesPage(documentsToIgnore + index, subList)
            } else {
                if (matchList.isNotEmpty())
                    firebaseRepository.setMatchesPage(documentsToIgnore + index, matchList)
                break
            }
            index++
        }
    }

}