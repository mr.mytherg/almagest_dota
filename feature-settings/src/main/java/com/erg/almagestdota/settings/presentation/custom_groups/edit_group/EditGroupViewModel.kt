package com.erg.almagestdota.settings.presentation.custom_groups.edit_group

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.NotificationStatus
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.complexadapter.commonButtonItem
import com.erg.almagestdota.complexadapter.complexList
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.main_settings.DraftFilter
import com.erg.almagestdota.local_storage.external.models.main_settings.DraftFilterGroup
import com.erg.almagestdota.settings.R
import com.erg.almagestdota.settings.presentation.custom_groups.edit_group.edit_filter.EditFilterFragment
import com.erg.almagestdota.settings.presentation.custom_groups.edit_group.edit_filter.EditFilterScreen
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

internal class EditGroupViewModel @AssistedInject constructor(
    @Assisted private val group: DraftFilterGroup,
    private val stringProvider: StringProvider,
    private val localStorage: ILocalStorageContract,
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data ->
        when (resultKey) {
            EditFilterFragment.TAG -> {
                when (EditFilterScreen.getResult(data)) {
                    EditFilterScreen.Result.EDITED -> {
                        val filter = EditFilterScreen.getFilter(data)
                        stateConfigurator.filters[filter.name] = filter
                        viewStateMutable.value = stateConfigurator.defineFragmentState()
                    }
                    EditFilterScreen.Result.REMOVED -> {
                        val filter = EditFilterScreen.getFilter(data)
                        stateConfigurator.filters[filter.name] = null
                        viewStateMutable.value = stateConfigurator.defineFragmentState()
                    }
                    EditFilterScreen.Result.CANCELED -> Unit
                }
            }
        }
    }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow<EditGroupViewState?>(null)
    val viewState = viewStateMutable.asStateFlow()

    private val loadingStateMutable = MutableStateFlow<Loading>(Loading.Disabled)
    val loadingState = loadingStateMutable.asStateFlow()

    init {
        viewStateMutable.value = stateConfigurator.defineFragmentState()
    }

    fun obtainAction(action: EditGroupViewActions) {
        when (action) {
            is EditGroupViewActions.RemoveGroup -> {
                val result = EditGroupScreen.Result.createResult(EditGroupScreen.Result.REMOVED, group)
                emit(viewEventMutable, ViewEvent.PopBackStack.Data(result))
            }
            is EditGroupViewActions.AddFilter -> {
                val filter = DraftFilter(
                    id = 0,
                    name = "",
                    description = "",
                    heroes = listOf()
                )
                val screen = EditFilterScreen(filter)
                emit(viewEventMutable, ViewEvent.Navigation(screen))
            }
            is EditGroupViewActions.EditFilter -> {
                val filter = stateConfigurator.filters[action.key]!!
                val screen = EditFilterScreen(filter)
                emit(viewEventMutable, ViewEvent.Navigation(screen))
            }
            is EditGroupViewActions.Submit -> {
                if (action.name.isEmpty()) {
                    val text = stringProvider.getString(R.string.empty_name)
                    emit(viewEventMutable, ViewEvent.AlmagestSnackbar(text, NotificationStatus.ERROR))
                    return
                }
                group.name = action.name
                group.filters = stateConfigurator.filters.map { it.value }.filterNotNull()

                if (group.filters.isEmpty()) {
                    val text = stringProvider.getString(R.string.empty_filters_list)
                    emit(viewEventMutable, ViewEvent.AlmagestSnackbar(text, NotificationStatus.ERROR))
                    return
                }
                val result = EditGroupScreen.Result.createResult(EditGroupScreen.Result.EDITED, group)
                emit(viewEventMutable, ViewEvent.PopBackStack.Data(result))
            }
        }
    }

    private inner class StateConfigurator {
        val filters: MutableMap<String, DraftFilter?> = group.filters.associateBy { it.name }.toMutableMap()

        fun defineFragmentState(): EditGroupViewState {
            val complexFilters = complexList {
                for ((name, filter) in filters) {
                    if (filter != null) {
                        commonButtonItem(key = name) {
                            title = name
                        }
                    }
                }
            }
            return EditGroupViewState.DefaultState(complexFilters)
        }
    }

    companion object {
        fun provideFactory(
            group: DraftFilterGroup,
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create(
                        group
                    ) as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(
            group: DraftFilterGroup
        ): EditGroupViewModel
    }
}