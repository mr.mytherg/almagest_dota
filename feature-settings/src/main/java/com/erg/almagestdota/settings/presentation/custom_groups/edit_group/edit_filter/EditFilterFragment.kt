package com.erg.almagestdota.settings.presentation.custom_groups.edit_group.edit_filter

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.complexadapter.ComplexAdapter
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdota.heroselector.presentation.heroSelector.HeroSelectorScreen
import com.erg.almagestdota.settings.R
import com.erg.almagestdota.settings.databinding.EditFilterFragmentBinding
import com.erg.almagestdota.settings.databinding.EditGroupFragmentBinding
import com.erg.almagestdpta.core_navigation.external.helpers.navHostFragmentManager
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.edit_group_fragment.*
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
internal class EditFilterFragment : Fragment(R.layout.edit_filter_fragment), BackPressListener {

    private val args by navArgs<EditFilterFragmentArgs>()

    companion object {
        const val TAG = "EditFilterFragment"
    }

    private val binding by viewBinding(EditFilterFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    @Inject
    lateinit var viewModelFactory: EditFilterViewModel.Factory
    private val viewModel by viewModels<EditFilterViewModel> {
        EditFilterViewModel.provideFactory(
            args.filter,
            assistedFactory = viewModelFactory
        )
    }

    override fun onBackPressed() {
        navController.popBackStack()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
        viewModel.viewEvent.setResultListener(this)
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).filterNotNull().onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.loadingState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach {
            binding.loadingView.isVisible = it is Loading.Enabled
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private inner class ViewsConfigurator {

        fun initStartState() = with(binding) {

            ivBack.clicksWithDebounce {
                navController.popBackStack()
            }
            tvOpenSearch.clicksWithDebounce {
                viewModel.obtainAction(EditFilterViewActions.OpenSearch)
            }
            ivDelete.clicksWithDebounce {
                viewModel.obtainAction(EditFilterViewActions.RemoveFilter)
            }
            ivSubmit.clicksWithDebounce {
                viewModel.obtainAction(EditFilterViewActions.Submit(etName.text.toString().trim()))
            }

        }

        fun renderState(state: EditFilterViewState) = with(binding) {
            when (state) {
                is EditFilterViewState.DefaultState -> renderDefaultState(state)
            }
        }

        private val allHeroesAdapter = ComplexAdapter(
            complexListener = object : ComplexListener {
                override fun onClick(item: ComplexItem) {
                    viewModel.obtainAction(EditFilterViewActions.SelectHero(item.key))
                }
            }
        )
        private val selectedHeroesAdapter = ComplexAdapter(
            complexListener = object : ComplexListener {
                override fun onClick(item: ComplexItem) {
                    viewModel.obtainAction(EditFilterViewActions.UnselectHero(item.key))
                }
            }
        )

        private fun renderDefaultState(state: EditFilterViewState.DefaultState) = with(binding) {
            etName.setText(args.filter.name)
            tvSelectedHeroes.isVisible = state.selectedHeroes.isNotEmpty()
            if (rvAllHeroes.adapter == null) {
                rvAllHeroes.adapter = allHeroesAdapter
            }
            allHeroesAdapter.submitList(state.allHeroes)
            if (rvSelectedHeroes.adapter == null) {
                rvSelectedHeroes.adapter = selectedHeroesAdapter
            }
            selectedHeroesAdapter.submitList(state.selectedHeroes)

        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
                is ViewEvent.PopBackStack<*> -> {
                    val fm = navHostFragmentManager()
                    navController.popBackStack()
                    fm.setFragmentResult(TAG, event.data as Bundle)
                }
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            viewModel.viewEvent.setResultListener(this@EditFilterFragment)
            navController.navigateViaScreenRoute(screen)
        }
    }
}