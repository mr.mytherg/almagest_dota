package com.erg.almagestdota.settings.domain.use_cases

import com.erg.almagestdota.base.external.extensions.round1
import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.local_storage.external.models.draft.HeroRole
import com.erg.almagestdota.local_storage.external.models.draft.PlayerLaneOutcome
import com.erg.almagestdota.local_storage.external.models.draft.PlayerLaneOutcomeSingle
import com.erg.almagestdota.local_storage.external.models.draft.RoleType
import com.erg.almagestdota.local_storage.external.models.draft.main.LaneOutcomeType
import com.erg.almagestdota.local_storage.external.models.draft.main.LaneStatusType
import com.erg.almagestdota.local_storage.external.models.draft.main.MemberInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import javax.inject.Inject

class GetSinglePlayerLanesUseCase @Inject constructor(
    private val getPlayersLanesUseCase: GetPlayersLanesUseCase
) {
    operator fun invoke(matches: List<DotaMatch>, playerId: Long): Pair<Long, List<PlayerLaneOutcomeSingle>> {
        val player = getPlayersLanesUseCase.invoke(matches)[playerId]!!

        return Pair(playerId, getListOfOutcomes(player))
    }

    private fun getListOfOutcomes(player: PlayerLaneOutcome): List<PlayerLaneOutcomeSingle> {
        val total = player.lose + player.winPlus + player.win + player.loseMinus + player.tie + player.undefined
        return listOf<PlayerLaneOutcomeSingle>(
            PlayerLaneOutcomeSingle(
                player.id,
                statusType = LaneOutcomeType.WIN_PLUS,
                count = player.winPlus,
                matchPercentage = getPercentage(player.winPlus, total)
            ),
            PlayerLaneOutcomeSingle(
                player.id,
                statusType = LaneOutcomeType.WIN,
                count = player.win,
                matchPercentage = getPercentage(player.win, total)
            ),
            PlayerLaneOutcomeSingle(
                player.id,
                statusType = LaneOutcomeType.TIE,
                count = player.tie,
                matchPercentage = getPercentage(player.tie, total)
            ),
            PlayerLaneOutcomeSingle(
                player.id,
                statusType = LaneOutcomeType.LOSE,
                count = player.lose,
                matchPercentage = getPercentage(player.lose, total)
            ),
            PlayerLaneOutcomeSingle(
                player.id,
                statusType = LaneOutcomeType.LOSE_MINUS,
                count = player.loseMinus,
                matchPercentage = getPercentage(player.loseMinus, total)
            ),
            PlayerLaneOutcomeSingle(
                player.id,
                statusType = LaneOutcomeType.UNDEFINED,
                count = player.undefined,
                matchPercentage = getPercentage(player.undefined, total)
            ),
        )
    }

    private fun getPercentage(param1: Int, param2: Int): Double {
        return if (param1 == 0 || param2 == 0) 0.0 else (param1 * 1.0 / param2 * 100).round1()

    }
}