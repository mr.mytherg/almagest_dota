package com.erg.almagestdota.settings.domain.use_cases

import android.content.Context
import android.content.pm.PackageInfo
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.drafting.data.IStratzGraphqlApi
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.HeroStage
import com.erg.almagestdota.local_storage.external.models.draft.RoleType
import com.erg.almagestdota.local_storage.external.models.draft.StageType
import com.erg.almagestdota.local_storage.external.models.draft.isHeroFitsForRole
import com.erg.almagestdota.local_storage.external.models.draft.main.MemberInfo
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerStats
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.main_settings.MainSettings
import com.erg.almagestdota.local_storage.external.models.main_settings.PlayersSettings
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.network.external.firebase.FirebaseRepositoryContract
import com.erg.almagestdota.settings.domain.model.get_matches.HeroStagesModel
import com.google.firebase.firestore.FirebaseFirestore
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class DefinePlayersRolesUseCase @Inject constructor(
    private val firebaseRepository: FirebaseRepositoryContract,
) {
    suspend operator fun invoke(): Int {
        val matches = firebaseRepository.getMatches()

        val playerHeroes = mutableMapOf<Long, MutableSet<Int>>()
        for (match in matches) {
            for (player in match.radiantHeroes) {
                val heroPool = playerHeroes[player.playerId] ?: mutableSetOf()
                heroPool.add(player.heroId)
                playerHeroes[player.playerId] = heroPool
            }
            for (player in match.direHeroes) {
                val heroPool = playerHeroes[player.playerId] ?: mutableSetOf()
                heroPool.add(player.heroId)
                playerHeroes[player.playerId] = heroPool
            }
        }
        val roles = listOf<RoleType>(
            RoleType.ROLE_1,
            RoleType.ROLE_2,
            RoleType.ROLE_3,
            RoleType.ROLE_4,
            RoleType.ROLE_5,
        )
        val players = mutableListOf<MemberInfo>()

        for ((playerId, heroes) in playerHeroes) {
            val playerHeroesRoles = mutableListOf<Pair<RoleType, Int>>()
            for (role in roles) {
                val count = heroes.count { GlobalVars.heroesAsMap[it]!!.roles.isHeroFitsForRole(role) }
                playerHeroesRoles.add(Pair(role, count))
            }
            val playerRole = playerHeroesRoles.sortedBy { it.second }.last().first
            val playerInfo = (GlobalVars.players[playerId] ?: MemberInfo(playerId)).apply {
                this.role = playerRole
            }
            players.add(playerInfo)
        }
        firebaseRepository.setPlayersSettings(PlayersSettings(players))
        return 0
    }

}