package com.erg.almagestdota.settings.domain.use_cases

import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.HeroStage
import com.erg.almagestdota.local_storage.external.models.draft.StageType
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.network.external.firebase.FirebaseRepositoryContract
import com.erg.almagestdota.settings.domain.model.get_matches.HeroStagesModel
import javax.inject.Inject

class UpdateHeroStagesUseCase @Inject constructor(
    private val iLocalStorageContract: ILocalStorageContract,
    private val firebaseRepository: FirebaseRepositoryContract,
) {
    operator fun invoke(matches: List<DotaMatch>, teamId: Long): Map<Int, List<HeroStage>> {

        val map = mutableMapOf<Int, Map<StageType, HeroStagesModel>>()
        for (hero in GlobalVars.mainSettings.heroes) {
            val mutableMap = mutableMapOf<StageType, HeroStagesModel>()
            mutableMap[StageType.FIRST_PICK] = HeroStagesModel(StageType.FIRST_PICK)
            mutableMap[StageType.DOUBLE_PICK] = HeroStagesModel(StageType.DOUBLE_PICK)
            mutableMap[StageType.FIRST_STAGE_ENDING] = HeroStagesModel(StageType.FIRST_STAGE_ENDING)
            mutableMap[StageType.SECOND_STAGE_OPENING] = HeroStagesModel(StageType.SECOND_STAGE_OPENING)
            mutableMap[StageType.SECOND_STAGE_DOUBLE_PICK] = HeroStagesModel(StageType.SECOND_STAGE_DOUBLE_PICK)
            mutableMap[StageType.SECOND_STAGE_ENDING] = HeroStagesModel(StageType.SECOND_STAGE_ENDING)
            mutableMap[StageType.THIRD_STAGE_OPENING] = HeroStagesModel(StageType.THIRD_STAGE_OPENING)
            mutableMap[StageType.LAST_PICK] = HeroStagesModel(StageType.LAST_PICK)
            map[hero.id] = mutableMap
        }

        for (match in matches) {
            val firstPickTeam: Pair<Long, List<PlayerInfo>>
            val lastPickTeam: Pair<Long, List<PlayerInfo>>
            if (match.hasRadiantFirstPick) {
                firstPickTeam = match.radiantTeamId to match.radiantHeroes
                lastPickTeam = match.direTeamId to match.direHeroes
            } else {
                firstPickTeam = match.direTeamId to match.direHeroes
                lastPickTeam = match.radiantTeamId to match.radiantHeroes
            }
            if (teamId == 0L || firstPickTeam.first == teamId)
                firstPickTeam.second.forEachIndexed { index, player ->
                    val heroId = player.heroId
                    when (index + 1) {
                        1 -> {
                            map[heroId]!![StageType.FIRST_PICK]!!.pickCount++
                            if (hasTeamLost(
                                    match,
                                    player
                                )
                            ) map[heroId]!![StageType.FIRST_PICK]!!.lostCount++ else map[heroId]!![StageType.FIRST_PICK]!!.winCount++
                        }
                        2 -> {
                            map[heroId]!![StageType.FIRST_STAGE_ENDING]!!.pickCount++
                            if (hasTeamLost(
                                    match,
                                    player
                                )
                            ) map[heroId]!![StageType.FIRST_STAGE_ENDING]!!.lostCount++ else map[heroId]!![StageType.FIRST_STAGE_ENDING]!!.winCount++
                        }
                        3, 4 -> {
                            map[heroId]!![StageType.SECOND_STAGE_DOUBLE_PICK]!!.pickCount++
                            if (hasTeamLost(
                                    match,
                                    player
                                )
                            ) map[heroId]!![StageType.SECOND_STAGE_DOUBLE_PICK]!!.lostCount++ else map[heroId]!![StageType.SECOND_STAGE_DOUBLE_PICK]!!.winCount++
                        }
                        5 -> {
                            map[heroId]!![StageType.THIRD_STAGE_OPENING]!!.pickCount++
                            if (hasTeamLost(
                                    match,
                                    player
                                )
                            ) map[heroId]!![StageType.THIRD_STAGE_OPENING]!!.lostCount++ else map[heroId]!![StageType.THIRD_STAGE_OPENING]!!.winCount++
                        }
                        else -> Unit
                    }
                }

            if (teamId == 0L || lastPickTeam.first == teamId)
                lastPickTeam.second.forEachIndexed { index, player ->
                    val heroId = player.heroId
                    when (index + 1) {
                        1, 2 -> {
                            map[heroId]!![StageType.DOUBLE_PICK]!!.pickCount++
                            if (hasTeamLost(
                                    match,
                                    player
                                )
                            ) map[heroId]!![StageType.DOUBLE_PICK]!!.lostCount++ else map[heroId]!![StageType.DOUBLE_PICK]!!.winCount++
                        }
                        3 -> {
                            map[heroId]!![StageType.SECOND_STAGE_OPENING]!!.pickCount++
                            if (hasTeamLost(
                                    match,
                                    player
                                )
                            ) map[heroId]!![StageType.SECOND_STAGE_OPENING]!!.lostCount++ else map[heroId]!![StageType.SECOND_STAGE_OPENING]!!.winCount++
                        }
                        4 -> {
                            map[heroId]!![StageType.SECOND_STAGE_ENDING]!!.pickCount++
                            if (hasTeamLost(
                                    match,
                                    player
                                )
                            ) map[heroId]!![StageType.SECOND_STAGE_ENDING]!!.lostCount++ else map[heroId]!![StageType.SECOND_STAGE_ENDING]!!.winCount++
                        }
                        5 -> {
                            map[heroId]!![StageType.LAST_PICK]!!.pickCount++
                            if (hasTeamLost(
                                    match,
                                    player
                                )
                            ) map[heroId]!![StageType.LAST_PICK]!!.lostCount++ else map[heroId]!![StageType.LAST_PICK]!!.winCount++
                        }
                        else -> Unit
                    }
                }
        }

        val resultMap = mutableMapOf<Int, List<HeroStage>>()
        for ((heroId, stages) in map) {
            val firstPick = stages[StageType.FIRST_PICK]!!
            val doublePick = stages[StageType.DOUBLE_PICK]!!
            val firstStageEnding = stages[StageType.FIRST_STAGE_ENDING]!!
            val secondStageOpening = stages[StageType.SECOND_STAGE_OPENING]!!
            val secondStageDoublePick = stages[StageType.SECOND_STAGE_DOUBLE_PICK]!!
            val secondStageEnding = stages[StageType.SECOND_STAGE_ENDING]!!
            val thirdStageOpening = stages[StageType.THIRD_STAGE_OPENING]!!
            val lastPick = stages[StageType.LAST_PICK]!!

            val totalPicks = firstPick.pickCount + doublePick.pickCount +
                firstStageEnding.pickCount + secondStageOpening.pickCount +
                secondStageDoublePick.pickCount + secondStageEnding.pickCount +
                thirdStageOpening.pickCount + lastPick.pickCount

            val list = listOf(
                HeroStage(
                    stage = StageType.FIRST_PICK,
                    pickRate = firstPick.getPercentage(firstPick.pickCount, totalPicks),
                    winRate = firstPick.getWinRate(),
                    matchCount = firstPick.pickCount
                ),
                HeroStage(
                    stage = StageType.DOUBLE_PICK,
                    pickRate = doublePick.getPercentage(doublePick.pickCount, totalPicks),
                    winRate = doublePick.getWinRate(),
                    matchCount = doublePick.pickCount
                ),
                HeroStage(
                    stage = StageType.FIRST_STAGE_ENDING,
                    pickRate = firstStageEnding.getPercentage(firstStageEnding.pickCount, totalPicks),
                    winRate = firstStageEnding.getWinRate(),
                    matchCount = firstStageEnding.pickCount
                ),
                HeroStage(
                    stage = StageType.SECOND_STAGE_OPENING,
                    pickRate = secondStageOpening.getPercentage(secondStageOpening.pickCount, totalPicks),
                    winRate = secondStageOpening.getWinRate(),
                    matchCount = secondStageOpening.pickCount
                ),
                HeroStage(
                    stage = StageType.SECOND_STAGE_DOUBLE_PICK,
                    pickRate = secondStageDoublePick.getPercentage(secondStageDoublePick.pickCount, totalPicks),
                    winRate = secondStageDoublePick.getWinRate(),
                    matchCount = secondStageDoublePick.pickCount
                ),
                HeroStage(
                    stage = StageType.SECOND_STAGE_ENDING,
                    pickRate = secondStageEnding.getPercentage(secondStageEnding.pickCount, totalPicks),
                    winRate = secondStageEnding.getWinRate(),
                    matchCount = secondStageEnding.pickCount
                ),
                HeroStage(
                    stage = StageType.THIRD_STAGE_OPENING,
                    pickRate = thirdStageOpening.getPercentage(thirdStageOpening.pickCount, totalPicks),
                    winRate = thirdStageOpening.getWinRate(),
                    matchCount = thirdStageOpening.pickCount
                ),
                HeroStage(
                    stage = StageType.LAST_PICK,
                    pickRate = lastPick.getPercentage(lastPick.pickCount, totalPicks),
                    winRate = lastPick.getWinRate(),
                    matchCount = lastPick.pickCount
                )
            )
            resultMap[heroId] = list
        }
        return resultMap
    }

    private fun hasTeamLost(match: DotaMatch, player: PlayerInfo): Boolean {
        val loserTeam = if (match.hasRadiantWon) match.direHeroes else match.radiantHeroes
        return loserTeam.find { it.playerId == player.playerId } != null
    }
}