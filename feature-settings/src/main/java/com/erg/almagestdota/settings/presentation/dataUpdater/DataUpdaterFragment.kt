package com.erg.almagestdota.settings.presentation.dataUpdater

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdota.settings.R
import com.erg.almagestdota.settings.databinding.DataUpdaterFragmentBinding
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
internal class DataUpdaterFragment : Fragment(R.layout.data_updater_fragment), BackPressListener {

    companion object {
        const val TAG = "DataUpdaterFragment"
    }

    private val binding by viewBinding(DataUpdaterFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewModel by hiltNavGraphViewModels<DataUpdaterViewModel>(R.id.graphSettings)
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
    }

    override fun onBackPressed() {
        navController.popBackStack()
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.loadingState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach {
            binding.loadingView.isVisible = it is Loading.Enabled
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private inner class ViewsConfigurator {

        fun initStartState() = with(binding) {
            tvLoadCounterItems.clicksWithDebounce {
                viewModel.obtainAction(DataUpdaterViewActions.LoadDotaCounterItems)
            }
            tvStages.clicksWithDebounce {
                viewModel.obtainAction(DataUpdaterViewActions.UpdateHeroStages)
            }
            tvLoadItems.clicksWithDebounce {
                viewModel.obtainAction(DataUpdaterViewActions.LoadDotaItems)
            }
            tvPredictMatches.clicksWithDebounce {
                viewModel.obtainAction(DataUpdaterViewActions.PredictMatches)
            }
            tvPredictMatches2.clicksWithDebounce {
                viewModel.obtainAction(DataUpdaterViewActions.PredictMatches2)
            }
            tvPredictTeams.clicksWithDebounce {
                viewModel.obtainAction(DataUpdaterViewActions.PredictTeams)
            }
            tvPredictPlayers.clicksWithDebounce {
                viewModel.obtainAction(DataUpdaterViewActions.PredictPlayers)
            }
            tvSaveTeams.clicksWithDebounce {
                viewModel.obtainAction(DataUpdaterViewActions.SaveTeams)
            }
            tvLoadHeroesCoefficientsSynergies.clicksWithDebounce {
                viewModel.obtainAction(DataUpdaterViewActions.LoadHeroesCoefficientsSynergiesData)
            }
            tvLoadHeroesCoefficientsCounters.clicksWithDebounce {
                viewModel.obtainAction(DataUpdaterViewActions.LoadHeroesCoefficientsCountersData)
            }
            tvLoadHeroesCoreData.clicksWithDebounce {
                viewModel.obtainAction(DataUpdaterViewActions.LoadHeroesCoreData)
            }
            tvLoadHeroesRoles.clicksWithDebounce {
                viewModel.obtainAction(DataUpdaterViewActions.LoadHeroesRolesData)
            }
            tvNewMatches.clicksWithDebounce {
                viewModel.obtainAction(DataUpdaterViewActions.LoadNewMatches)
            }
            tvOldMatches.clicksWithDebounce {
                viewModel.obtainAction(DataUpdaterViewActions.LoadOldMatches)
            }
        }

        fun renderState(state: DataUpdaterViewState) = with(binding) {
            when (state) {
                is DataUpdaterViewState.SomeState -> {
                    tvLog.text = state.msg
                }
            }
        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            navController.navigateViaScreenRoute(screen)
        }
    }
}