package com.erg.almagestdota.settings.domain

import android.provider.Settings.Global
import com.erg.almagestdota.drafting.data.OpenDotaApi
import com.erg.almagestdota.drafting.domain.use_cases.LoadPlayerInfoUseCase
import com.erg.almagestdota.local_storage.external.models.draft.main.MemberInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.network.external.firebase.FirebaseRepositoryContract
import com.erg.almagestdota.drafting.data.IStratzGraphqlApi
import com.erg.almagestdota.local_storage.external.models.main_settings.PlayersSettings
import com.erg.almagestdota.local_storage.external.models.main_settings.TeamsSettings
import com.erg.almagestdota.predictor.domain.use_cases.LoadHawkDraftUseCase
import com.erg.almagestdota.predictor.domain.use_cases.LoadPlayerHeroPoolPublicUseCase
import com.erg.almagestdota.settings.domain.use_cases.*
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject
import kotlin.coroutines.coroutineContext

class Interactor @Inject constructor(
    private val loadDataCounterItemsUseCase: LoadDataCounterItemsUseCase,
    private val loadItemsUseCase: LoadItemsUseCase,
    private val loadCoreDataUseCase: LoadCoreDataUseCase,
    private val loadRolesUseCase: LoadRolesUseCase,
    private val loadCoefficientsSynergyUseCase: LoadCoefficientsSynergyUseCase,
    private val loadCoefficientsCounterUseCase: LoadCoefficientsCounterUseCase,
    private val loadNewMatchesUseCase: LoadNewMatchesUseCase,
    private val loadOldMatchesUseCase: LoadOldMatchesUseCase,
    private val matchesPredictionUseCase: MatchesPredictionUseCase,
    private val matchesPredictionUseCase2: MatchesPredictionUseCase2,
    private val matchesPredictionUseCase3: MatchesPredictionUseCase3,
    private val matchesPredictionUseCase4: MatchesPredictionUseCase4,
    private val matchesPredictionUseCase5: MatchesPredictionUseCase5,
    private val teamPredictionUseCase: TeamPredictionUseCase,
    private val playerPredictionUseCase: PlayerPredictionUseCase,
    private val updateHeroStagesUseCase: UpdateHeroStagesUseCase,
    private val loadPlayerInfoUseCase: LoadPlayerInfoUseCase,
    private val firebaseRepository: FirebaseRepositoryContract,
    private val updatePlayersNamesUseCase: UpdatePlayersNamesUseCase,
    private val definePlayersRolesUseCase: DefinePlayersRolesUseCase,
    private val updateTeamNamesUseCase: UpdateTeamNamesUseCase,
    private val updateMatchesLanesUseCase: UpdateMatchesLanesUseCase,
) {

    suspend fun predictMatches(viewModelScope: CoroutineScope): String {
        val matches =
            firebaseRepository.getMatches().filter { it.matchId > 6492259768 } // -5 от текущего

        val string = matchesPredictionUseCase.invoke(viewModelScope, matches)

        firebaseRepository.setMainPredictor(GlobalVars.mainPredictor)
        firebaseRepository.setTeamsSettings(TeamsSettings(GlobalVars.teams.map { it.value }))
        firebaseRepository.setPlayersSettings(PlayersSettings(GlobalVars.players.map { it.value }))
        firebaseRepository.setImportantSettings(GlobalVars.importantSettings)
        return string
    }

    suspend fun predictMatches2(viewModelScope: CoroutineScope): String {
        return matchesPredictionUseCase5.invoke()
    }

    private suspend fun getPlayers(matches: List<DotaMatch>): List<MemberInfo> {
        val playersSettings = firebaseRepository.getPlayersSettings()
        val players = mutableSetOf<Long>()
        for (match in matches) {
            for (player in match.radiantHeroes)
                players.add(player.playerId)
            for (player in match.direHeroes)
                players.add(player.playerId)
        }
        val unloadedPlayers = mutableListOf<MemberInfo>()
        for (player in players) {
            if (GlobalVars.players[player] == null) {
                unloadedPlayers.add(loadPlayerInfoUseCase.invoke(player))
            }
        }

        return playersSettings.players.map {
            it.matchCount = 0
            it.rating = 1400
            it
        }.plus(unloadedPlayers)
    }

    suspend fun predictTeams(viewModelScope: CoroutineScope): String {
        val matches = firebaseRepository.getMatches()
        return teamPredictionUseCase.invoke(viewModelScope, matches, firebaseRepository.getTeamsSettings().teams)
    }

    suspend fun predictPlayers(viewModelScope: CoroutineScope): String {
        val matches = firebaseRepository.getMatches()

        return playerPredictionUseCase.invoke(viewModelScope, matches, getPlayers(matches))
    }

    suspend fun saveTeams(): String {
        updateMatchesLanesUseCase.invoke(firebaseRepository.getImportantSettings())
        //  definePlayersRolesUseCase.invoke()
        //updateTeamNamesUseCase.invoke(CoroutineScope(coroutineContext))
        return "done"
//        teamsSaverUseCase.invoke(firebaseRepository.getTeams())
    }


    suspend fun loadDotaItems(viewModelScope: CoroutineScope) =
        loadItemsUseCase.invoke(viewModelScope)

    suspend fun loadDotaCounterItems(viewModelScope: CoroutineScope) =
        loadDataCounterItemsUseCase.invoke(viewModelScope)

    suspend fun loadHeroesRolesData(viewModelScope: CoroutineScope) =
        loadRolesUseCase.invoke(viewModelScope)

    suspend fun loadHeroesCoreData() = loadCoreDataUseCase.invoke()

    suspend fun loadCoefficientsSynergy(viewModelScope: CoroutineScope) =
        loadCoefficientsSynergyUseCase.invoke(viewModelScope)

    suspend fun loadCoefficientsCounters(viewModelScope: CoroutineScope) =
        loadCoefficientsCounterUseCase.invoke(viewModelScope)



    suspend fun loadNewMatches() =
        loadNewMatchesUseCase.invoke(firebaseRepository.getImportantSettings())

    suspend fun loadOldMatches() =
        loadOldMatchesUseCase.invoke(firebaseRepository.getImportantSettings())
}