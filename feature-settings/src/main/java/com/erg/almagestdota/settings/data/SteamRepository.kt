package com.erg.almagestdota.settings.data

import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.settings.data.response.get_matches.SteamMatchDetailsRaw
import com.erg.almagestdota.settings.domain.model.get_matches.SteamDotaMatch
import javax.inject.Inject

 class SteamRepository @Inject constructor(
     private val api: ISteamApi,
     private val mapperToSteamDotaMatch: SteamMatchDetailsRaw.MapperToSteamDotaMatch,
 ) : ISteamContract {
    override suspend fun getMatchDetails(matchId: Long): SteamDotaMatch {
        return api.getMatchDetails(matchId = matchId.toString()).essentialMap(mapperToSteamDotaMatch)
    }
}