package com.erg.almagestdota.settings.domain.use_cases

import android.util.Log
import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.drafting.domain.use_cases.LoadPlayerInfoUseCase
import com.erg.almagestdota.local_storage.external.models.draft.GameModeType
import com.erg.almagestdota.local_storage.external.models.draft.HeroValues
import com.erg.almagestdota.local_storage.external.models.draft.main.*
import com.erg.almagestdota.local_storage.external.models.leagues.LeagueTierType
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.network.external.firebase.FirebaseRepositoryContract
import com.erg.almagestdota.predictor.domain.use_cases.HeroValuesCalibratorUseCase
import com.erg.almagestdota.predictor.domain.use_cases.HeroValuesReverseCalibratorUseCase
import com.erg.almagestdota.predictor.domain.use_cases.PredictionUseCase
import com.erg.almagestdota.predictor.domain.use_cases.models.FailReasons
import com.erg.almagestdota.predictor.domain.use_cases.models.PredictType
import com.google.gson.Gson
import kotlinx.coroutines.*
import javax.inject.Inject
import kotlin.math.absoluteValue
import kotlin.math.pow

class PlayerPredictionUseCase @Inject constructor(
    private val firebaseRepository: FirebaseRepositoryContract,
    private val heroValuesCalibratorUseCase: HeroValuesCalibratorUseCase,
    private val heroValuesReverseCalibratorUseCase: HeroValuesReverseCalibratorUseCase,
    private val predictionUseCase: PredictionUseCase,
    private val loadPlayerInfoUseCase: LoadPlayerInfoUseCase,
    private val gson: Gson
) {

    private val predictorTeam = PredictorTeam()
    private var calibratorTeam = CalibratorTeam(listOf())

    suspend operator fun invoke(
        viewModelScope: CoroutineScope,
        dotaMatches: List<DotaMatch>,
        players: List<MemberInfo>
    ): String {
        val recentMatches = dotaMatches.filter { it.matchId >= 6795566593 }.sortedByDescending { it.matchId }
        val currentValues = HeroValues.copy(GlobalVars.importantSettings.heroesProValues)
        heroValuesReverseCalibratorUseCase.calibrateProMatches(recentMatches, viewModelScope, GlobalVars.importantSettings.heroesValues)

        calibratorTeam = CalibratorTeam(players)
        val list = predictorTeam.predict(dotaMatches, viewModelScope, calibratorTeam).filter { it.isSuccess != null }

        var successDraft = 0
        var successTeam = 0
        var success = 0
        var fail = 0
        var failDraft = 0
        var failTeam = 0
        var controversialDraftPub = 0
        var controversialDraft = 0
        var controversialTeam = 0
        var controversial = 0
        var invalidPlayers = 0
        var notCalibrated = 0
        var lowCoef = 0
        var rowLoses = mutableListOf<Int>()
        var rowWins = mutableListOf<Int>()
        var wins = 0
        var loses = 0
        for (item in list) {
            when (item.reason) {
                null -> {
                    if (item.isSuccess == true) {
                        wins++
                        if (loses != 0) {
                            rowLoses.add(loses)
                            loses = 0
                        }

                        success++
                        if (item.predictType == PredictType.TeamPredict) {
                            successTeam++
                        }
                        if (item.predictType == PredictType.DraftPredict) {
                            successDraft++
                        }
                    }
                    if (item.isSuccess == false) {
                        loses++
                        if (wins != 0) {
                            rowWins.add(wins)
                            wins = 0
                        }

                        fail++
                        if (item.predictType == PredictType.TeamPredict) {
                            failTeam++
                        }
                        if (item.predictType == PredictType.DraftPredict) {
                            failDraft++
                        }
                    }
                }
                FailReasons.PLAYERS_INVALID -> invalidPlayers++
                FailReasons.CONTROVERSIAL_INFO -> {
                    controversial++
                    if (item.predictType == PredictType.TeamPredict) {
                        controversialTeam++
                    }
                    if (item.predictType == PredictType.DraftPredict) {
                        controversialDraft++
                    }
                }
                FailReasons.CONTROVERSIAL_INFO_DRAFTS -> {
                    controversialDraftPub++
                }
                FailReasons.PLAYERS_NOT_CALIBRATED -> notCalibrated++
                FailReasons.TOO_LOW_COEFFICIENTS -> lowCoef++
            }
        }
        val team0 = list.filter { it.teamDifference.absoluteValue in 0..50 }
        val team50 = list.filter { it.teamDifference.absoluteValue in 50..100 }
        val team100 = list.filter { it.teamDifference.absoluteValue in 100..150 }
        val team150 = list.filter { it.teamDifference.absoluteValue in 150..200 }
        val team200 = list.filter { it.teamDifference.absoluteValue in 200..250 }
        val team250 = list.filter { it.teamDifference.absoluteValue in 250..300 }
        val team300 = list.filter { it.teamDifference.absoluteValue > 300 }
        val draft0 = list.filter { it.draftValue.absoluteValue in 0.0..10.0 }
        val draft10 = list.filter { it.draftValue.absoluteValue in 10.0..20.0 }
        val draft20 = list.filter { it.draftValue.absoluteValue in 20.0..30.0 }
        val draft30 = list.filter { it.draftValue.absoluteValue in 30.0..40.0 }
        val draft40 = list.filter { it.draftValue.absoluteValue in 40.0..50.0 }
        val draft50 = list.filter { it.draftValue.absoluteValue in 50.0..60.0 }
        val draft60 = list.filter { it.draftValue.absoluteValue > 60.0 }
        var string = """
            successDraft - $successDraft
            successTeam - $successTeam
            success - $success

            fail - $fail
            failTeam - $failTeam
            failDraft - $failDraft

            controversial - $controversial
            controversialTeam - $controversialTeam
            controversialDraft - $controversialDraft
            controversialDraftPub - $controversialDraftPub

            invalidPlayers - $invalidPlayers
            notCalibrated - $notCalibrated
            lowCoef - $lowCoef
            avgWinRow - ${rowWins.average()}
            maxWinRow - ${rowWins.maxOf { it }}
            avgLoseRow - ${rowLoses.average()}
            maxLoseRow - ${rowLoses.maxOf { it }}

            team 0 draft 30 = ${getTeamChances(team0, draft30)}
            team 0 draft 40 = ${getTeamChances(team0, draft40)}
            team 0 draft 50 = ${getTeamChances(team0, draft50)}
            team 0 draft 60 = ${getTeamChances(team0, draft60)}

            team 50 draft 30 = ${getTeamChances(team50, draft30)}
            team 50 draft 40 = ${getTeamChances(team50, draft40)}
            team 50 draft 50 = ${getTeamChances(team50, draft50)}
            team 50 draft 60 = ${getTeamChances(team50, draft60)}

            team 100 draft 0 = ${getTeamChances(team100, draft0)}
            team 100 draft 10 = ${getTeamChances(team100, draft10)}
            team 100 draft 20 = ${getTeamChances(team100, draft20)}
            team 100 draft 30 = ${getTeamChances(team100, draft30)}
            team 100 draft 40 = ${getTeamChances(team100, draft40)}
            team 100 draft 50 = ${getTeamChances(team100, draft50)}
            team 100 draft 60 = ${getTeamChances(team100, draft60)}

            team 150 draft 0 = ${getTeamChances(team150, draft0)}
            team 150 draft 10 = ${getTeamChances(team150, draft10)}
            team 150 draft 20 = ${getTeamChances(team150, draft20)}
            team 150 draft 30 = ${getTeamChances(team150, draft30)}
            team 150 draft 40 = ${getTeamChances(team150, draft40)}
            team 150 draft 50 = ${getTeamChances(team150, draft50)}
            team 150 draft 60 = ${getTeamChances(team150, draft60)}

            team 200 draft 0 = ${getTeamChances(team200, draft0)}
            team 200 draft 10 = ${getTeamChances(team200, draft10)}
            team 200 draft 20 = ${getTeamChances(team200, draft20)}
            team 200 draft 30 = ${getTeamChances(team200, draft30)}
            team 200 draft 40 = ${getTeamChances(team200, draft40)}
            team 200 draft 50 = ${getTeamChances(team200, draft50)}
            team 200 draft 60 = ${getTeamChances(team200, draft60)}

            team 250 draft 0 = ${getTeamChances(team250, draft0)}
            team 250 draft 10 = ${getTeamChances(team250, draft10)}
            team 250 draft 20 = ${getTeamChances(team250, draft20)}
            team 250 draft 30 = ${getTeamChances(team250, draft30)}
            team 250 draft 40 = ${getTeamChances(team250, draft40)}
            team 250 draft 50 = ${getTeamChances(team250, draft50)}
            team 250 draft 60 = ${getTeamChances(team250, draft60)}

            team 300 draft 0 = ${getTeamChances(team300, draft0)}
            team 300 draft 10 = ${getTeamChances(team300, draft10)}
            team 300 draft 20 = ${getTeamChances(team300, draft20)}
            team 300 draft 30 = ${getTeamChances(team300, draft30)}
            team 300 draft 40 = ${getTeamChances(team300, draft40)}
            team 300 draft 50 = ${getTeamChances(team300, draft50)}
            team 300 draft 60 = ${getTeamChances(team300, draft60)}
  """.trimIndent()

        for ((id1, heroValues) in GlobalVars.importantSettings.heroesProValues) {
            for ((id2, value) in heroValues.counters) {
                if (currentValues[id1]!!.counters[id2]!! != value)
                    string += "failed ${currentValues[id1]!!.counters[id2]!!} $value\n"
            }
            for ((id2, value) in heroValues.synergies) {
                if (currentValues[id1]!!.synergies[id2]!! != value)
                    string += "failed ${currentValues[id1]!!.synergies[id2]!!} $value\n"
            }
        }

//        updateRemoteMatches(dotaMatches, 1007) // todo
//        val settings = firebaseRepository.getImportantSettings()
//        val playerMap = mutableMapOf<Long, MemberInfo>()
//        for (player in settings.players) {
//            playerMap[player.playerId] = player
//        }
//        for (player in players) {
//            val playerId = player.playerId
//            playerMap[playerId] =
//                (playerMap[playerId] ?: loadPlayerInfoUseCase.invoke(playerId)).apply {
//                    val playerInfo =
//                        calibratorTeam.playerStatsMap[playerId] ?: throw IllegalStateException("2")
//                    rating = playerInfo.rating
//                    matchCount = playerInfo.matchCount
//                }
//        }
//        settings.players = playerMap.map { it.value }
//        firebaseRepository.setImportantSettings(settings)
        return string
    }

    private fun getTeamChances(team: List<PredictionPlayer>, draft: List<PredictionPlayer>): String {
        val size =
            team.filter { draft.contains(it) && ((it.draftValue > -10 && it.teamDifference > -50) || (it.draftValue < 10 && it.teamDifference < 50)) }.size
        val success =
            team.filter { draft.contains(it) && ((it.hasRadiantWon && it.draftValue > -10 && it.teamDifference > -50) || (!it.hasRadiantWon && it.draftValue < 10 && it.teamDifference < 50)) }.size
        val fail = size - success
        return "${getPercentage(success, fail)}% success $success fail $fail"
    }

    private suspend fun updateRemoteMatches(
        dotaMatches: List<DotaMatch>,
        documentsToIgnore: Int,
    ) {
        var matchList = dotaMatches.sortedBy { it.matchId }

        var index = 0
        while (true) {
            if (matchList.size > LoadOldMatchesUseCase.MATCHES_PER_DOCUMENT) {
                val subList = matchList.takeLast(LoadOldMatchesUseCase.MATCHES_PER_DOCUMENT)
                matchList = matchList.subList(
                    0,
                    matchList.size - LoadOldMatchesUseCase.MATCHES_PER_DOCUMENT
                )
                firebaseRepository.setMatchesPage(documentsToIgnore - index, subList)
            } else {
                if (matchList.isNotEmpty()) {
                    firebaseRepository.setMatchesPage(documentsToIgnore - index, matchList)
                }
                break
            }
            index++
        }
    }

    private fun isEarlyGame(duration: Long) = duration / 60 < TIME_1
    private fun isMidGame(duration: Long) = duration / 60 in TIME_1..TIME_2
    private fun isLateGame(duration: Long) = duration / 60 > TIME_2

    private fun getPercentage(param1: Int, param2: Int): Double {
        return if (param1 + param2 == 0) 0.0 else (param1 * 1.0 / (param1 + param2) * 100).round2()
    }

    private inner class PredictorTeam {
        suspend fun predict(
            list: List<DotaMatch>,
            viewModelScope: CoroutineScope,
            calibratorTeam: CalibratorTeam
        ): List<PredictionPlayer> {
            val predictions = mutableListOf<PredictionPlayer>()

            var index = 0
            for (match in list) {
                val prediction = predictionUseCase.invoke(convertToDraftModel(match), false)

                match.isInvalid = prediction.reason != null
                match.willRadiantWin = prediction.willRadiantWin
                val predictionPlayer = PredictionPlayer(
                    hasRadiantWon = match.hasRadiantWon,
                    isSuccess = if (prediction.willRadiantWin != null) {
                        match.hasRadiantWon && prediction.willRadiantWin!! || !match.hasRadiantWon && !prediction.willRadiantWin!!
                    } else {
                        null
                    },
                    reason = prediction.reason,
                    predictType = prediction.predictType,
                    draftValue = prediction.draftValue,
                    teamDifference = prediction.teamDifference
                )
                if (match.matchId >= 6795566593) {
                    heroValuesCalibratorUseCase.calibrateProMatches(listOf(match), viewModelScope, GlobalVars.importantSettings.heroesProValues)
                    predictions.add(predictionPlayer)
                }
                calibratorTeam.calibrateMatch(match)
                Log.e("PREDICTOR", index.toString())
                index++
            }
            return predictions
        }
    }

    private fun convertToDraftModel(dotaMatch: DotaMatch): DraftModel {
        return DraftModel(
            isAllPick = false,
            gameMode = GameModeType.LIVE_FINISHED,
            sequence = GlobalVars.mainSettings.sequenceCM,
            radiantTeam = TeamDraft(
                picks = dotaMatch.radiantHeroes.toMutableList(),
                bans = dotaMatch.radiantBans.toMutableList(),
                teamInfo = TeamInfo(
                    dotaMatch.radiantTeamId,
                    members = getTeamMembers(dotaMatch.radiantHeroes)
                )
            ),
            direTeam = TeamDraft(
                picks = dotaMatch.direHeroes.toMutableList(),
                bans = dotaMatch.direBans.toMutableList(),
                teamInfo = TeamInfo(
                    dotaMatch.direTeamId,
                    members = getTeamMembers(dotaMatch.direHeroes)
                )
            ),
        )
    }

    private fun getTeamMembers(players: List<PlayerInfo>): List<Long> {
        val mutableList = mutableListOf<Long>()
        for (player in players) {
            GlobalVars.players[player.playerId] = calibratorTeam.playerStatsMap[player.playerId] ?: throw IllegalStateException("1")
            mutableList.add(
                player.playerId
            )
        }
        return mutableList
    }

    private inner class CalibratorTeam(
        private val players: List<MemberInfo>
    ) {
        val playerMap: MutableMap<Long, EloRatingPlayer> = mutableMapOf()
        val playerStatsMap: MutableMap<Long, MemberInfo> = mutableMapOf()

        init {
            for (player in players) {
                playerMap[player.playerId] = EloRatingPlayer()
                playerStatsMap[player.playerId] = player
            }
        }

        fun calibrateMatch(match: DotaMatch) {
            val radiantPlayers: List<Int> = match.radiantHeroes.map {
                val player = playerMap[it.playerId] ?: EloRatingPlayer()
                player.value
            }
            val direPlayers: List<Int> = match.direHeroes.map {
                val player = playerMap[it.playerId] ?: EloRatingPlayer()
                player.value
            }
            for (player in match.radiantHeroes) {
                val playerItem = (playerMap[player.playerId] ?: EloRatingPlayer()).apply {
                    value = getNewValue(
                        match.leagueTierType,
                        value,
                        matchCount,
                        match.hasRadiantWon,
                        radiantPlayers,
                        direPlayers
                    )
                }
                playerMap[player.playerId] = playerItem
                addOtherValues(player.playerId, match.hasRadiantWon, match.duration)
                val playerStatsItem =
                    (playerStatsMap[player.playerId] ?: MemberInfo(player.playerId)).apply {
                        matchCount++
                        rating = playerItem.value
                    }
                playerStatsMap[player.playerId] = playerStatsItem
            }
            for (player in match.direHeroes) {
                val playerItem = (playerMap[player.playerId] ?: EloRatingPlayer()).apply {
                    value = getNewValue(
                        match.leagueTierType,
                        value,
                        matchCount,
                        !match.hasRadiantWon,
                        direPlayers,
                        radiantPlayers
                    )
                }
                playerMap[player.playerId] = playerItem
                addOtherValues(player.playerId, !match.hasRadiantWon, match.duration)
                val playerStatsItem =
                    (playerStatsMap[player.playerId] ?: MemberInfo(player.playerId)).apply {
                        matchCount++
                        rating = playerItem.value
                    }
                playerStatsMap[player.playerId] = playerStatsItem
            }
        }

        private fun addOtherValues(playerId: Long, won: Boolean, duration: Long) {
            addGameStagesValues(playerId, won, duration)
            playerMap[playerId]!!.matchCount++
        }

        private fun addGameStagesValues(playerId: Long, won: Boolean, duration: Long) {
            when {
                isEarlyGame(duration) -> if (won) playerMap[playerId]!!.earlyWinCount++ else playerMap[playerId]!!.earlyLoseCount++
                isMidGame(duration) -> if (won) playerMap[playerId]!!.midWinCount++ else playerMap[playerId]!!.midLoseCount++
                else -> if (won) playerMap[playerId]!!.lateWinCount++ else playerMap[playerId]!!.lateLoseCount++
            }
        }

        private fun getNewValue(
            tier: LeagueTierType,
            rating: Int,
            matchCount: Int,
            hasWon: Boolean,
            allies: List<Int>,
            enemies: List<Int>
        ): Int {
            val maxDiff = 400.0
            var maxChange: Double = when {
                matchCount < GlobalVars.CALIBRATION_MATCH_COUNT -> 40.0
                else -> 20.0
            }
            val teamB = enemies.fold(0) { acc, next -> acc + next } / 5
            maxChange *= if (tier == LeagueTierType.PREMIUM) 3.0 else 1.0

            val result = if (hasWon) 1 else 0
            val ea = 1 / (1 + 10.0.pow((teamB - rating) * 1.0 / maxDiff))
            return (rating + maxChange * (result - ea)).toInt()
        }
    }

    companion object {
        private const val TIME_1 = 25
        private const val TIME_2 = 40
    }
}

data class PredictionPlayer(
    val hasRadiantWon: Boolean,
    val isSuccess: Boolean? = null,
    val reason: FailReasons? = null,
    val predictType: PredictType? = null,
    val draftValue: Double,
    val teamDifference: Int
)

data class EloRatingPlayer(
    var matchCount: Int = 0,
    var earlyWinCount: Int = 0, //
    var earlyLoseCount: Int = 0, //
    var midWinCount: Int = 0, //
    var midLoseCount: Int = 0, //
    var lateWinCount: Int = 0, //
    var lateLoseCount: Int = 0, //
    var value: Int = 1400, //
)