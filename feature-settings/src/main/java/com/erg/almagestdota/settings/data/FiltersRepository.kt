package com.erg.almagestdota.settings.data

import com.erg.almagestdota.local_storage.external.models.main_settings.DraftFilterGroup
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.settings.domain.use_cases.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import javax.inject.Inject

class FiltersRepository @Inject constructor(
    private val saveFiltersUseCase: SaveFiltersUseCase,
    private val getFilterGroupsUseCase: GetFilterGroupsUseCase,
) {
    private val mutableStateFlow = MutableStateFlow(saveFiltersUseCase.readFromFile())

    fun getGroupsStateFlow() = mutableStateFlow.asStateFlow()

    fun updateGroups(groups: List<DraftFilterGroup>) {
        mutableStateFlow.update {
            groups
        }
        saveFiltersUseCase.putToFile(groups)
        GlobalVars.groups = getFilterGroupsUseCase.invoke(groups)

    }
}