package com.erg.almagestdota.settings.presentation

import androidx.lifecycle.ViewModel
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.settings.domain.Interactor
import com.erg.almagestdota.settings.presentation.custom_groups.GroupsScreen
import com.erg.almagestdota.settings.presentation.dataUpdater.DataUpdaterScreen
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

@HiltViewModel
internal class SettingsViewModel @Inject constructor(
    private val stringProvider: StringProvider,
    private val interactor: Interactor,
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data -> }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    fun obtainAction(action: SettingsViewActions) {
        when (action) {
            is SettingsViewActions.OpenCustomGroups -> emit(viewEventMutable, ViewEvent.Navigation(GroupsScreen))
            is SettingsViewActions.OpenDataUpdaterFragment -> emit(viewEventMutable, ViewEvent.Navigation(DataUpdaterScreen))
        }
    }

    private inner class StateConfigurator {

        fun defineFragmentState(): SettingsViewState {
            return SettingsViewState.DefaultState
        }
    }
}