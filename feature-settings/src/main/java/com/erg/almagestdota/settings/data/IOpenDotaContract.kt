package com.erg.almagestdota.settings.data

import com.erg.almagestdota.settings.domain.model.get_matches.OpenDotaMatch

 interface IOpenDotaContract {
    suspend fun loadMatches(endId: Long? = null, startId: Long? = null): List<OpenDotaMatch>
}