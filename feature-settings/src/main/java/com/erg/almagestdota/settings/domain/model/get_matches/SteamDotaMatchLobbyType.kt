package com.erg.almagestdota.settings.domain.model.get_matches

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
 enum class SteamDotaMatchLobbyType(val lobbyType: Int) : Parcelable {
    UNKNOWN(0),
    PRACTISE(1),
    TOURNAMENT(2);

    companion object {
        fun byLobbyType(value: Int?): SteamDotaMatchLobbyType {
            return values().firstOrNull { it.lobbyType == value } ?: UNKNOWN
        }
    }
}