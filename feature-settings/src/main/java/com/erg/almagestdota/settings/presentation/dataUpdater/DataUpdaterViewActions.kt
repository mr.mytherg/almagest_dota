package com.erg.almagestdota.settings.presentation.dataUpdater

internal sealed class DataUpdaterViewActions {

    object LoadHeroesCoreData : DataUpdaterViewActions()
    object LoadHeroesCoefficientsCountersData : DataUpdaterViewActions()
    object LoadHeroesCoefficientsSynergiesData : DataUpdaterViewActions()
    object LoadHeroesRolesData : DataUpdaterViewActions()
    object LoadNewMatches : DataUpdaterViewActions()
    object LoadOldMatches : DataUpdaterViewActions()
    object LoadDotaItems : DataUpdaterViewActions()
    object PredictMatches : DataUpdaterViewActions()
    object PredictMatches2 : DataUpdaterViewActions()
    object PredictTeams : DataUpdaterViewActions()
    object PredictPlayers : DataUpdaterViewActions()
    object SaveTeams : DataUpdaterViewActions()
    object LoadDotaCounterItems : DataUpdaterViewActions()
    object UpdateHeroStages : DataUpdaterViewActions()
}