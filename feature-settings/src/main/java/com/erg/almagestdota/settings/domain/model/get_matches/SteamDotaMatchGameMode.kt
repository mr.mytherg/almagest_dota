package com.erg.almagestdota.settings.domain.model.get_matches

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
 enum class SteamDotaMatchGameMode(val gameMode: Int) : Parcelable {
    CAPTAINS_MODE(2),
    UNKNOWN(0);

    companion object {
        fun byGameMode(value: Int?): SteamDotaMatchGameMode {
            return values().firstOrNull { it.gameMode == value } ?: UNKNOWN
        }
    }
}