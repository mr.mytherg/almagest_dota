package com.erg.almagestdota.settings.domain.use_cases

import android.content.Context
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.HeroValues
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.local_storage.external.raw_models.DotaMatchRaw
import com.erg.almagestdota.predictor.domain.use_cases.HeroValuesCalibratorUseCase
import com.erg.almagestdota.predictor.domain.use_cases.PredictionUseCase
import com.google.gson.Gson
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.*
import java.io.BufferedWriter
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStreamWriter
import javax.inject.Inject
import kotlin.math.absoluteValue

private const val MATCHES_TO_PREDICT = 0.6

class MatchesPredictionUseCase @Inject constructor(
    @ApplicationContext private val context: Context,
    private val heroValuesCalibratorUseCase: HeroValuesCalibratorUseCase,
    private val gson: Gson

) {
    private val predictor = Predictor()

    private var heroes: MutableMap<Int, HeroValues> = HeroValues.copy(GlobalVars.importantSettings.heroesValues)

    private var matchesCalibrateCount = 0
    private var matchesPredictCount = 0

    private val mult = 10

    private fun isValueFits(listValue: Double, currentValue: Double): Boolean {
        return listValue in currentValue..(currentValue + mult) // listValue >= currentValue//
    }

    var draftProbabilities = mutableMapOf<String, Double>()

    suspend operator fun invoke(
        viewModelScope: CoroutineScope,
        dotaMatches: List<DotaMatch>,
    ): String {
        draftProbabilities = mutableMapOf()

        matchesPredictCount = (dotaMatches.size * MATCHES_TO_PREDICT).toInt()
        matchesCalibrateCount = dotaMatches.size - matchesPredictCount

        heroes = heroValuesCalibratorUseCase.calibrateProMatches(dotaMatches, viewModelScope)
        GlobalVars.importantSettings.heroesProValues = heroes
        secondCalibration()
        val matchesToCalibrate = dotaMatches.take(matchesCalibrateCount)
        heroes = heroValuesCalibratorUseCase.calibrateProMatches(matchesToCalibrate, viewModelScope)
        val matchesToPredict = dotaMatches.takeLast(matchesPredictCount)
        val list = predictor.predict(matchesToPredict, viewModelScope)

        val predictionMap = mutableMapOf<Long, List<PredictionState>>()
//        val groups = list.groupBy { it.leagueId }
//        for (group in groups) {
//            addPredictions(group.value, predictionMap)
//        }
        addPredictions(list, predictionMap, true)

       GlobalVars.mainPredictor.draftProbabilities = draftProbabilities

        val avgDur = list.fold(0) { acc, next -> acc + next.duration.toInt() / 60 } / list.size
        var string = ""
        string += "average duration - $avgDur"
        string += "predicted count - $matchesPredictCount\n"
        string += "calibrated count - $matchesCalibrateCount\n"
        val badTournamentsList = mutableListOf<Triple<Long, Int, Int>>()
        for ((leagueId, predictionList) in predictionMap) {
            string += "${GlobalVars.leagues.find { it.id == leagueId }?.name ?: "OVERALL STATS"} \n"
            string += "$leagueId \n"
            var wins = 0
            var loses = 0
            for (prediction in predictionList) {
                string += "outdraft value - ${prediction.startValue}\n"
                string += "success - ${prediction.success} fail - ${prediction.fail} % - ${prediction.winRate}\n"
                string += "% of games - ${((prediction.success + prediction.fail) * 1.0 / list.size * 100).round2()}\n\n"
                wins += prediction.success
                loses += prediction.fail
            }
            if (loses > wins) {
                badTournamentsList.add(Triple(leagueId, wins, loses))
            }
        }
        string += "$######################################\n"
        string += "$######################################\n"
        string += "$######################################\n"
        string += "$######################################\n"
        for ((leagueId, wins, loses) in badTournamentsList) {
            string += "${GlobalVars.leagues.find { it.id == leagueId }?.name.orDefault()} \n"
            string += "$leagueId wins - $wins loses - $loses wr - ${getPercentage(wins, loses)}%\n"
        }

        return string
    }

    private fun addPredictions(
        list: List<Prediction>,
        predictionMap: MutableMap<Long, List<PredictionState>>,
        isOverall: Boolean = false
    ) {
        val mutableList = mutableListOf<PredictionState>()
        for (value in 0..mult * 6 step mult) {
            val successEarly = if (value == mult * 6) {
                list.filter { it.isSuccess && it.value >= mult * 6 }
            } else {
                list.filter { it.isSuccess && isValueFits(it.value, value.toDouble()) }
            }
            val failEarly = if (value == mult * 6) {
                list.filter { it.isSuccess.not() && it.value >= mult * 6 }
            } else {
                list.filter { it.isSuccess.not() && isValueFits(it.value, value.toDouble()) }
            }
            if (successEarly.size + failEarly.size == 0) {
                continue
            }

            val winrate = getPercentage(successEarly.size, failEarly.size)

            when {
                value == mult -> draftProbabilities[PredictionType.P5_10.name] = winrate
                value == mult * 2 -> draftProbabilities[PredictionType.P10_15.name] = winrate
                value == mult * 3 -> draftProbabilities[PredictionType.P15_20.name] = winrate
                value == mult * 4 -> draftProbabilities[PredictionType.P20_25.name] = winrate
                value == mult * 5 -> draftProbabilities[PredictionType.P25_30.name] = winrate
                value == mult * 6 -> draftProbabilities[PredictionType.P30.name] = winrate
                else -> Unit
            }

            val prediction = PredictionState(
                startValue = value.toDouble(),
                success = successEarly.size,
                fail = failEarly.size,
                winRate = winrate,
            )
            mutableList.add(prediction)
        }
        predictionMap[if (isOverall) 0 else list.first().leagueId] = mutableList
    }

    private fun secondCalibration() {
        for ((id, hero) in heroes) {
            for ((id2, value) in hero.synergies) {
                if (value.absoluteValue < 1.0) {
                    hero.synergies[id2] = GlobalVars.importantSettings.heroesValues[id]!!.synergies[id2]!!
                }
            }
            for ((id2, value) in hero.counters) {
                if (value.absoluteValue < 1.0) {
                    hero.counters[id2] = GlobalVars.importantSettings.heroesValues[id]!!.counters[id2]!!
                }
            }
        }
    }


    private fun getPercentage(param1: Int, param2: Int): Double {
        return if (param1 + param2 == 0) 0.0 else (param1 * 1.0 / (param1 + param2) * 100).round2()
    }

    private inner class Predictor {

        suspend fun predict(
            dotaMatches: List<DotaMatch>,
            viewModelScope: CoroutineScope,
        ): List<Prediction> {
            val predictionList = mutableListOf<Prediction>()

//            for (match in dotaMatches) {
//                val value = predictionUseCase.invoke(
//                    radiantHeroes = match.radiantHeroes,
//                    direHeroes = match.direHeroes,
//                    heroesValues = heroes
//                )
//
//                heroes = heroValuesCalibratorUseCase.calibrateProMatches(
//                    listOf(match),
//                    viewModelScope,
//                    heroes
//                )
//
//                predictionList.add(
//                    Prediction(
//                        isSuccess = match.hasRadiantWon && value > 0 || !match.hasRadiantWon && value < 0,
//                        value = value.absoluteValue,
//                        leagueId = match.leagueId,
//                        duration = match.duration,
//                        hasRadiantWon = match.hasRadiantWon,
//                        radiantTeamName = GlobalVars.mainPredictor.teamRatings[match.radiantTeamId]?.name.orDefault(),
//                        direTeamName = GlobalVars.mainPredictor.teamRatings[match.direTeamId]?.name.orDefault(),
//                    )
//                )
//            }
            return predictionList
        }
    }
}

data class Prediction(
    val isSuccess: Boolean,
    val hasRadiantWon: Boolean,
    val radiantTeamName: String,
    val direTeamName: String,
    val value: Double,
    val leagueId: Long,
    val duration: Long,
)

data class PredictionState(
    val startValue: Double,
    val success: Int,
    val fail: Int,
    val winRate: Double,

)