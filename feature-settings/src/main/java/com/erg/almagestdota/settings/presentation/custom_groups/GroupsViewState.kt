package com.erg.almagestdota.settings.presentation.custom_groups

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem

internal sealed class GroupsViewState : ViewState() {
    class DefaultState(
        val groups: List<ComplexItem>
    ) : GroupsViewState()
}