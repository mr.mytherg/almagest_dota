package com.erg.almagestdota.settings.domain.use_cases

import android.content.Context
import androidx.annotation.StringRes
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.*
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.settings.R
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class GetFilterGroupsUseCase @Inject constructor(
    private val localStorage: ILocalStorageContract,
    private val filtersUseCase: SaveFiltersUseCase,
    @ApplicationContext private val context: Context,
) {
    companion object {
        private const val DMG_MIN_PERCENTAGE = 30.0
        private const val TOWER_DMG_MIN_VALUE = 80.0
        private const val HEALING_MIN_VALUE = 45.0
        const val STAGE_MINIMUM_PERCENTAGE = 10
    }

    operator fun invoke(customGroups: List<DraftFilterGroup>? = null): List<DraftFilterGroup> {
        val filterGroups: MutableList<DraftFilterGroup> = mutableListOf()

        val heroes = GlobalVars.mainSettings.heroes.sortedBy { it.name }
        addSuggestions(filterGroups, heroes)
        filterGroups.addAll(customGroups ?: filtersUseCase.readFromFile())
        addRoles(filterGroups, heroes)
        addInGameRoles(filterGroups, heroes)
        addStages(filterGroups, heroes)
        addDmgTypes(filterGroups, heroes)
        addGameStages(filterGroups, heroes)
        addAttackTypes(filterGroups, heroes)
        addAttributes(filterGroups, heroes)
        addComplexity(filterGroups, heroes)
        addFactions(filterGroups, heroes)

        return filterGroups.toList()
    }

    private fun addGameStages(filterGroups: MutableList<DraftFilterGroup>, heroes: List<HeroStatsData>) {
        val list: MutableList<DraftFilter> = mutableListOf()
        createFilter(
            list,
            context.getString(R.string.filter_name_game_stage_laning),
            context.getString(R.string.description_filter_game_stage),
            heroes.sortedByDescending { it.laning.fold(0.0) { acc, next -> acc + next.kills + next.assists } }.take(80)
        )
        createFilter(
            list,
            context.getString(R.string.filter_name_game_stage_early),
            context.getString(R.string.description_filter_game_stage),
            heroes.sortedByDescending { it.early.fold(0.0) { acc, next -> acc + next.kills + next.assists } }.take(80)
        )
        createFilter(
            list,
            context.getString(R.string.filter_name_game_stage_mid),
            context.getString(R.string.description_filter_game_stage),
            heroes.sortedByDescending { it.mid.fold(0.0) { acc, next -> acc + next.kills + next.assists } }.take(80)
        )
        createFilter(
            list,
            context.getString(R.string.filter_name_game_stage_late),
            context.getString(R.string.description_filter_game_stage),
            heroes.sortedByDescending { it.late.fold(0.0) { acc, next -> acc + next.kills + next.assists } }.take(80)
        )
        val group = DraftFilterGroup(
            filters = getRemainedHeroes(heroes, list), name = context.getString(R.string.filter_group_name_game_stages), type = DraftFilterGroupType.GAME_STAGES
        )
        filterGroups.add(group)
    }

    private fun getRemainedHeroes(heroes: List<HeroStatsData>, list: MutableList<DraftFilter>): List<DraftFilter> {
        val mutableList = mutableListOf<DraftFilter>()
        var lateList = list[3].heroes
        var midList = list[2].heroes
        var earlyList = list[1].heroes
        var laningList = list[0].heroes
        for (hero in heroes) {
            // laning
            if (list[0].heroes.find { it == hero.id } != null) {
                continue
            }
            // early
            if (list[1].heroes.find { it == hero.id } != null) {
                continue
            }
            // mid
            if (list[2].heroes.find { it == hero.id } != null) {
                continue
            }
            // late
            if (list[3].heroes.find { it == hero.id } != null) {
                continue
            }
            val late = hero.late.fold(0.0) { acc, next -> acc + next.assists + next.kills }
            val mid = hero.mid.fold(0.0) { acc, next -> acc + next.assists + next.kills }
            val early = hero.early.fold(0.0) { acc, next -> acc + next.assists + next.kills }
            val laning = hero.laning.fold(0.0) { acc, next -> acc + next.assists + next.kills }
            if (late >= mid && late >= early && late >= laning) {
                lateList = lateList.plus(hero.id)
            } else if (mid >= late && mid >= early && mid >= laning) {
                midList = midList.plus(hero.id)
            } else if (early >= late && early >= laning && early >= mid) {
                earlyList = earlyList.plus(hero.id)
            } else {
                laningList = laningList.plus(hero.id)
            }
        }
        mutableList.add(DraftFilter(list[0].id, list[0].name, list[0].description, laningList.sortedBy { it }))
        mutableList.add(DraftFilter(list[1].id, list[1].name, list[1].description, earlyList.sortedBy { it }))
        mutableList.add(DraftFilter(list[2].id, list[2].name, list[2].description, midList.sortedBy { it }))
        mutableList.add(DraftFilter(list[3].id, list[3].name, list[3].description, lateList.sortedBy { it }))
        return mutableList
    }

    private fun addDmgTypes(filterGroups: MutableList<DraftFilterGroup>, heroes: List<HeroStatsData>) {
        val list: MutableList<DraftFilter> = mutableListOf()
        createFilter(list, context.getString(R.string.filter_stats_magical_dmg), "", heroes.filter { it.magicalDamagePercentage >= DMG_MIN_PERCENTAGE })
        createFilter(list, context.getString(R.string.filter_stats_physical_dmg), "", heroes.filter { it.physicalDamagePercentage >= DMG_MIN_PERCENTAGE })
        createFilter(list, context.getString(R.string.filter_stats_pure_dmg), "", heroes.filter { it.pureDamagePercentage >= DMG_MIN_PERCENTAGE })
        createFilter(list, context.getString(R.string.filter_stats_tower_dmg), "", heroes.filter { it.towerDamage >= TOWER_DMG_MIN_VALUE })
        createFilter(list, context.getString(R.string.filter_stats_healing), "", heroes.filter { it.healing >= HEALING_MIN_VALUE })
        val group = DraftFilterGroup(
            filters = list, type = DraftFilterGroupType.DAMAGE_AND_HEAL, name = context.getString(R.string.filter_group_name_stats)
        )
        filterGroups.add(group)
    }

    private fun addStages(filterGroups: MutableList<DraftFilterGroup>, heroes: List<HeroStatsData>) {
        val list: MutableList<DraftFilter> = mutableListOf()
        addFilter(StageType.FIRST_PICK, heroes, list, R.string.filter_stage_first_pick)
        addFilter(StageType.DOUBLE_PICK, heroes, list, R.string.filter_stage_double_pick)
        addFilter(StageType.FIRST_STAGE_ENDING, heroes, list, R.string.filter_stage_1_stage_ending)
        addFilter(StageType.SECOND_STAGE_OPENING, heroes, list, R.string.filter_stage_2_stage_opening)
        addFilter(StageType.SECOND_STAGE_DOUBLE_PICK, heroes, list, R.string.filter_stage_2_stage_double_pick)
        addFilter(StageType.SECOND_STAGE_ENDING, heroes, list, R.string.filter_stage_2_stage_ending)
        addFilter(StageType.THIRD_STAGE_OPENING, heroes, list, R.string.filter_stage_3_stage_opening)
        addFilter(StageType.LAST_PICK, heroes, list, R.string.filter_stage_last_pick)


        val group = DraftFilterGroup(
            filters = list, type = DraftFilterGroupType.DRAFT_STAGES, name = context.getString(R.string.filter_stages)
        )
        filterGroups.add(group)
    }

    private fun addFilter(stageType: StageType, heroes: List<HeroStatsData>, list: MutableList<DraftFilter>, @StringRes filterStageFirstPick: Int) {
        val stage = heroes.filter { it.stages.find { it.stage == stageType }!!.pickRate >= STAGE_MINIMUM_PERCENTAGE }
        createFilter(list, context.getString(filterStageFirstPick), context.getString(R.string.description_filter_stage), stage.sortedBy { it.name })
    }

    private fun addRoles(filterGroups: MutableList<DraftFilterGroup>, heroes: List<HeroStatsData>) {
        var list: MutableList<DraftFilter> = mutableListOf()
        createFilter(list, context.getString(R.string.role_1), "", heroes.filter { it.roles.isHeroFitsForRole(RoleType.ROLE_1) })
        createFilter(list, context.getString(R.string.role_2), "", heroes.filter { it.roles.isHeroFitsForRole(RoleType.ROLE_2) })
        createFilter(list, context.getString(R.string.role_3), "", heroes.filter { it.roles.isHeroFitsForRole(RoleType.ROLE_3) })
        createFilter(list, context.getString(R.string.role_4), "", heroes.filter { it.roles.isHeroFitsForRole(RoleType.ROLE_4) })
        createFilter(list, context.getString(R.string.role_5), "", heroes.filter { it.roles.isHeroFitsForRole(RoleType.ROLE_5) })
        var group = DraftFilterGroup(
            filters = list, type = DraftFilterGroupType.ROLES, name = context.getString(R.string.filter_roles)
        )
        filterGroups.add(group)

        list = mutableListOf()
        createFilter(list,
            context.getString(R.string.filter_universal_1),
            context.getString(R.string.description_filter_universal),
            heroes.filter { it.roles.countNumberOfPossibleRoles() == 1 })
        createFilter(list,
            context.getString(R.string.filter_universal_2),
            context.getString(R.string.description_filter_universal),
            heroes.filter { it.roles.countNumberOfPossibleRoles() == 2 })
        createFilter(list,
            context.getString(R.string.filter_universal_3),
            context.getString(R.string.description_filter_universal),
            heroes.filter { it.roles.countNumberOfPossibleRoles() == 3 })
        createFilter(list,
            context.getString(R.string.filter_universal_4),
            context.getString(R.string.description_filter_universal),
            heroes.filter { it.roles.countNumberOfPossibleRoles() == 4 })
        createFilter(list,
            context.getString(R.string.filter_universal_5),
            context.getString(R.string.description_filter_universal),
            heroes.filter { it.roles.countNumberOfPossibleRoles() == 5 })
        group = DraftFilterGroup(
            filters = list, type = DraftFilterGroupType.FLEXIBILITY, name = context.getString(R.string.filter_universal)
        )
        filterGroups.add(group)
    }

    private fun addSuggestions(filterGroups: MutableList<DraftFilterGroup>, heroes: List<HeroStatsData>) {
        val list: MutableList<DraftFilter> = mutableListOf()
        createFilter(list, context.getString(R.string.filter_the_best), context.getString(R.string.description_filter_the_best), heroes)
        createFilter(list, context.getString(R.string.filter_can_be_stolen), context.getString(R.string.description_filter_can_be_stolen), heroes)
        createFilter(list, context.getString(R.string.filter_counter), context.getString(R.string.description_filter_counter), heroes)
        createFilter(list, context.getString(R.string.filter_synergy), context.getString(R.string.description_filter_synergy), heroes)
        createFilter(list, context.getString(R.string.filter_all_heroes), "", heroes)
        val group = DraftFilterGroup(
            filters = list, type = DraftFilterGroupType.RECOMMENDATIONS, name = context.getString(R.string.filter_group_name_suggestions)
        )
        filterGroups.add(group)
    }

    private fun addComplexity(filterGroups: MutableList<DraftFilterGroup>, heroes: List<HeroStatsData>) {
        val list: MutableList<DraftFilter> = mutableListOf()
        createFilter(list, context.getString(R.string.filter_complexity_1), "", heroes.filter { it.complexity == ComplexityType.SIMPLE })
        createFilter(list, context.getString(R.string.filter_complexity_2), "", heroes.filter { it.complexity == ComplexityType.MEDIUM })
        createFilter(list, context.getString(R.string.filter_complexity_3), "", heroes.filter { it.complexity == ComplexityType.HARD })
        val group = DraftFilterGroup(
            filters = list, type = DraftFilterGroupType.COMPLEXITY, name = context.getString(R.string.filter_group_name_complexity)
        )
        filterGroups.add(group)
    }

    private fun addAttackTypes(filterGroups: MutableList<DraftFilterGroup>, heroes: List<HeroStatsData>) {
        val list: MutableList<DraftFilter> = mutableListOf()
        createFilter(list, context.getString(R.string.filter_melee), "", heroes.filter { it.attackType == AttackType.MELEE })
        createFilter(list, context.getString(R.string.filter_ranged), "", heroes.filter { it.attackType == AttackType.RANGED })
        val group = DraftFilterGroup(
            filters = list, type = DraftFilterGroupType.ATTACK_TYPE, name = context.getString(R.string.filter_group_name_attack_type)
        )
        filterGroups.add(group)
    }

    private fun addAttributes(filterGroups: MutableList<DraftFilterGroup>, heroes: List<HeroStatsData>) {
        val list: MutableList<DraftFilter> = mutableListOf()
        createFilter(list, context.getString(R.string.filter_strength), "", heroes.filter { it.primaryAttribute == AttributeType.STRENGTH })
        createFilter(list, context.getString(R.string.filter_agility), "", heroes.filter { it.primaryAttribute == AttributeType.AGILITY })
        createFilter(list, context.getString(R.string.filter_intelligence), "", heroes.filter { it.primaryAttribute == AttributeType.INTELLIGENCE })
        val group = DraftFilterGroup(
            filters = list, type = DraftFilterGroupType.ATTRIBUTES, name = context.getString(R.string.filter_group_name_attributes)
        )
        filterGroups.add(group)
    }

    private fun addFactions(filterGroups: MutableList<DraftFilterGroup>, heroes: List<HeroStatsData>) {
        val list: MutableList<DraftFilter> = mutableListOf()
        createFilter(list, context.getString(R.string.filter_radiant), "", heroes.filter { it.team })
        createFilter(list, context.getString(R.string.filter_dire), "", heroes.filter { !it.team })
        val group = DraftFilterGroup(
            filters = list, type = DraftFilterGroupType.FACTION, name = context.getString(R.string.filter_group_name_faction)
        )
        filterGroups.add(group)
    }

    private fun addInGameRoles(filterGroups: MutableList<DraftFilterGroup>, heroes: List<HeroStatsData>) {
        val list: MutableList<DraftFilter> = mutableListOf()
        //team needs stacker if there is a carry who farms lots of neutrals
        //ganker damage + long disable or stun + kills
        createFilter(list,
            context.getString(R.string.filter_initiator),
            "",
            heroes.filter { hasHeroBlinkDagger(it) }
        )
//        createFilter(list,
//            context.getString(R.string.filter_escaper),
//            "",
//            heroes.filter {  GlobalVars.heroesAsMap[it.id]!!.hasBlink }
//        )
        val roles = filterGroups.find { it.type == DraftFilterGroupType.ROLES }!!
        createFilter(
            list, context.getString(R.string.filter_ganker), "",
            heroes
                .filter { hero ->
                    roles.filters[1].heroes.find { it == hero.id } != null || hasBootsOfTravel(hero)
                }
                .filter {
                    with(it.early.find { it.position.number == 2 }) {
                        this?.stunCount.orDefault() >= 0.3 || this?.disableDuration.orDefault() >= 100
                    } || with(it.mid.find { it.position.number == 2 }) {
                        this?.stunCount.orDefault() >= 0.3 || this?.disableDuration.orDefault() >= 100
                    } || hasBootsOfTravel(it)
                }
                .filter {
                    with(it.early.find { it.position.number == 2 }) { this?.kills.orDefault() >= 0.1 }
                        || with(it.mid.find { it.position.number == 2 }) { this?.kills.orDefault() >= 0.1 }
                        || hasBootsOfTravel(it)
                }
        )
        createFilter(list,
            context.getString(R.string.filter_roamer),
            "",
            heroes.filter { hero -> roles.filters[3].heroes.find { it == hero.id } != null }
                .filter {
                    with(it.laning.find { it.position.number == 4 }) {
                        this?.stunCount.orDefault() >= 0.3 || this?.disableDuration.orDefault() >= 100
                    }
                })
        val group = DraftFilterGroup(
            filters = list, type = DraftFilterGroupType.IN_GAME_ROLES, name = context.getString(R.string.filter_group_name_in_game_roles)
        )
        filterGroups.add(group)
    }

    private fun hasHeroBlinkDagger(hero: HeroStatsData): Boolean {
        return GlobalVars.heroesAsMap[hero.id]!!.hasBlink ||
            GlobalVars.mainSettings.heroesItems[hero.id]!!.main.find { it.id == 4 } != null
    }

    private fun hasBootsOfTravel(hero: HeroStatsData): Boolean {
        return GlobalVars.mainSettings.heroesItems[hero.id]!!.main.find { it.id == 12 } != null
    }

    private fun <T : HeroCoreData> createFilter(
        filters: MutableList<DraftFilter>, name: String, description: String, heroes: List<T>
    ) {
        val draftFilter = DraftFilter(id = filters.size, name = name, description = description, heroes = heroes.map { it.id }.sortedBy { it })
        filters.add(draftFilter)
    }
}