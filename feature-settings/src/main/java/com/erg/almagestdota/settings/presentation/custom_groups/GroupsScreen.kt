package com.erg.almagestdota.settings.presentation.custom_groups

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.settings.presentation.SettingsFragmentDirections

object GroupsScreen : Screen<NavDirections>(
    route = SettingsFragmentDirections.toGroupsFragment(),
    requestKey = GroupsFragment.TAG
)

