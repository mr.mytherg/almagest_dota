package com.erg.almagestdota.settings.domain.use_cases

import android.content.Context
import com.erg.almagestdota.base.external.extensions.asMap
import com.erg.almagestdota.drafting.domain.use_cases.LoadPlayerInfoUseCase
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.GameModeType
import com.erg.almagestdota.local_storage.external.models.draft.main.*
import com.erg.almagestdota.local_storage.external.models.leagues.LeagueTierType
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.local_storage.external.raw_models.ImportantSettingsRaw
import com.erg.almagestdota.network.external.firebase.FirebaseRepositoryContract
import com.erg.almagestdota.predictor.domain.use_cases.HeroValuesCalibratorUseCase
import com.erg.almagestdota.predictor.domain.use_cases.PredictionUseCase
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.*
import kotlinx.coroutines.tasks.await
import javax.inject.Inject
import kotlin.coroutines.coroutineContext
import kotlin.math.pow

class CalibrateMatchesUseCase @Inject constructor(
    private val firebaseRepository: FirebaseRepositoryContract,
    private val heroValuesCalibratorUseCase: HeroValuesCalibratorUseCase,
    private val predictionUseCase: PredictionUseCase,
    private val loadPlayerInfoUseCase: LoadPlayerInfoUseCase,
) {

    private val predictorTeam = PredictorTeam()
    private var calibratorTeam = CalibratorTeam(listOf())

    suspend operator fun invoke(
        dotaMatches: List<DotaMatch>,
        players: List<MemberInfo>
    ) {
        calibratorTeam = CalibratorTeam(players)
        predictorTeam.predict(dotaMatches, CoroutineScope(coroutineContext), calibratorTeam)

        val playerMap = mutableMapOf<Long, MemberInfo>()
        for ((playerId, player) in GlobalVars.players) {
            playerMap[player.playerId] = player
        }

        firebaseRepository.setPlayersSettings(PlayersSettings(GlobalVars.players.map { it.value }))
        firebaseRepository.setImportantSettings(GlobalVars.importantSettings)
    }

    private fun getUniqueMatches(matches: List<DotaMatch>): List<DotaMatch> {
        val uniqueMatches = mutableMapOf<Long, DotaMatch>()
        for (match in matches) {
            uniqueMatches[match.matchId] = match
        }
        return uniqueMatches.toList().map { it.second }.sortedBy { it.matchId }.takeLast(50)
    }

    private inner class PredictorTeam {
        suspend fun predict(
            list: List<DotaMatch>,
            viewModelScope: CoroutineScope,
            calibratorTeam: CalibratorTeam,
        ) {
            for (match in list) {
                val prediction = predictionUseCase.invoke(convertToDraftModel(match), false)
                match.isInvalid = prediction.reason != null
                match.willRadiantWin = prediction.willRadiantWin
            }
            for (match in list) {
                calibratorTeam.calibrateMatch(match)
                GlobalVars.importantSettings.heroesProValues =
                    heroValuesCalibratorUseCase.calibrateProMatches(
                        listOf(match),
                        viewModelScope,
                        GlobalVars.importantSettings.heroesProValues
                    )
            }
        }
    }

    private fun convertToDraftModel(dotaMatch: DotaMatch): DraftModel {
        return DraftModel(
            isAllPick = false,
            gameMode = GameModeType.LIVE_FINISHED,
            sequence = GlobalVars.mainSettings.sequenceCM,
            radiantTeam = TeamDraft(
                picks = dotaMatch.radiantHeroes.toMutableList(),
                bans = dotaMatch.radiantBans.toMutableList(),
                teamInfo = TeamInfo(
                    dotaMatch.radiantTeamId,
                    members = getTeamMembers(dotaMatch.radiantHeroes)
                )
            ),
            direTeam = TeamDraft(
                picks = dotaMatch.direHeroes.toMutableList(),
                bans = dotaMatch.direBans.toMutableList(),
                teamInfo = TeamInfo(
                    dotaMatch.direTeamId,
                    members = getTeamMembers(dotaMatch.direHeroes)
                )
            ),
        )
    }

    private fun getTeamMembers(players: List<PlayerInfo>): List<Long> {
        val mutableList = mutableListOf<Long>()

        for (player in players) {
            GlobalVars.players[player.playerId] = calibratorTeam.playerStatsMap[player.playerId]!!
            mutableList.add(
                player.playerId
            )
        }
        return mutableList
    }

    private inner class CalibratorTeam(
        private val players: List<MemberInfo>
    ) {
        val playerMap: MutableMap<Long, EloRatingPlayerCalibration> = mutableMapOf()
        val playerStatsMap: MutableMap<Long, MemberInfo> = mutableMapOf()

        init {
            for (player in players) {
                playerMap[player.playerId] =
                    EloRatingPlayerCalibration(player.matchCount, player.rating)
                playerStatsMap[player.playerId] = player
            }
        }

        fun calibrateMatch(match: DotaMatch) {
            val radiantPlayers: List<Int> = match.radiantHeroes.map {
                val player = playerMap[it.playerId]!!
                player.value
            }
            val direPlayers: List<Int> = match.direHeroes.map {
                val player = playerMap[it.playerId]!!
                player.value
            }
            for (player in match.radiantHeroes) {
                val playerItem = (playerMap[player.playerId]!!).apply {
                    value = getNewValue(
                        match.leagueTierType,
                        value,
                        matchCount,
                        match.hasRadiantWon,
                        radiantPlayers,
                        direPlayers
                    )
                }
                playerMap[player.playerId] = playerItem
                playerMap[player.playerId]!!.matchCount++
                val playerStatsItem =
                    (playerStatsMap[player.playerId]!!).apply {
                        matchCount++
                        rating = playerItem.value
                    }
                playerStatsMap[player.playerId] = playerStatsItem
            }
            for (player in match.direHeroes) {
                val playerItem = (playerMap[player.playerId]!!).apply {
                    value = getNewValue(
                        match.leagueTierType,
                        value,
                        matchCount,
                        !match.hasRadiantWon,
                        direPlayers,
                        radiantPlayers
                    )
                }
                playerMap[player.playerId] = playerItem
                playerMap[player.playerId]!!.matchCount++
                val playerStatsItem =
                    (playerStatsMap[player.playerId]!!).apply {
                        matchCount++
                        rating = playerItem.value
                    }
                playerStatsMap[player.playerId] = playerStatsItem
            }
        }

        private fun getNewValue(
            tier: LeagueTierType,
            rating: Int,
            matchCount: Int,
            hasWon: Boolean,
            allies: List<Int>,
            enemies: List<Int>
        ): Int {
            val maxDiff = 400.0
            var maxChange: Double = when {
                matchCount < GlobalVars.CALIBRATION_MATCH_COUNT -> 40.0
                else -> 20.0
            }
            val teamB = enemies.fold(0) { acc, next -> acc + next } / 5
            maxChange *= if (tier == LeagueTierType.PREMIUM) 3.0 else 1.0

            val result = if (hasWon) 1 else 0
            val ea = 1 / (1 + 10.0.pow((teamB - rating) * 1.0 / maxDiff))
            return (rating + maxChange * (result - ea)).toInt()
        }
    }
}

data class EloRatingPlayerCalibration(
    var matchCount: Int = 0,
    var value: Int = 1400, //
)