package com.erg.almagestdota.settings.domain.use_cases

import android.content.Context
import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.predictor.domain.use_cases.HeroValuesCalibratorUseCase
import com.erg.almagestdota.predictor.domain.use_cases.PredictionUseCase
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.*
import javax.inject.Inject

class MatchesPredictionUseCase2 @Inject constructor(
    private val firebaseFirestore: FirebaseFirestore,
    @ApplicationContext private val context: Context,
    private val iLocalStorageContract: ILocalStorageContract,
    private val predictionUseCase: PredictionUseCase,
    private val heroValuesCalibratorUseCase: HeroValuesCalibratorUseCase,
    private val gson: Gson

) {
    private val counterStatsMap: MutableMap<Int, MutableMap<Int, Stats>> = mutableMapOf()
    private val synergyStatsMap: MutableMap<Int, MutableMap<Int, Stats>> = mutableMapOf()

    private val mainValue = 2

    init {
        val map1 = mutableMapOf<Int, Stats>()
        val map2 = mutableMapOf<Int, Stats>()
        val map3 = mutableMapOf<Int, Stats>()
        val map4 = mutableMapOf<Int, Stats>()
        for (hero in GlobalVars.mainSettings.heroes) {
            map1[hero.id] = Stats(heroName = hero.name, heroId = hero.id)
            map2[hero.id] = Stats(heroName = hero.name, heroId = hero.id)
            counterStatsMap[-mainValue] = map1
            counterStatsMap[mainValue] = map2
        }
        for (hero in GlobalVars.mainSettings.heroes) {
            map3[hero.id] = Stats(heroName = hero.name, heroId = hero.id)
            map4[hero.id] = Stats(heroName = hero.name, heroId = hero.id)
            synergyStatsMap[-mainValue] = map3
            synergyStatsMap[mainValue] = map4
        }
    }

    class Stats(
        var winRate: Double = 0.0,
        var matchCount: Int = 0,
        var winCount: Int = 0,
        val heroName: String,
        val heroId: Int
    )

    private fun getRelevantValue(value: Double): Int {
        return when {
            value <= -mainValue -> -mainValue
            value >= mainValue -> mainValue
            else -> 0
        }
    }

    private fun calibrateProMatches(matchesToCalibrate: List<DotaMatch>) {
        for (match in matchesToCalibrate) {
            val wonTeam = if (match.hasRadiantWon) match.radiantHeroes else match.direHeroes
            val lostTeam = if (!match.hasRadiantWon) match.radiantHeroes else match.direHeroes

            for (wonHero in wonTeam) {
                val counterValue = wonHero.heroId.getCounter(
                    lostTeam.map { it.heroId },
                    GlobalVars.importantSettings.heroesProValues
                )
                var relevantValue = getRelevantValue(counterValue)
                if (relevantValue != 0) {
                    counterStatsMap[relevantValue]!![wonHero.heroId]!!.apply {
                        winCount++
                        matchCount++
                    }
                }
                val synergyValue = wonHero.heroId.getSynergy(
                    wonTeam.map { it.heroId },
                    GlobalVars.importantSettings.heroesProValues
                )
                relevantValue = getRelevantValue(synergyValue)
                if (relevantValue != 0) {
                    synergyStatsMap[relevantValue]!![wonHero.heroId]!!.apply {
                        winCount++
                        matchCount++
                    }
                }
            }

            for (lostHero in lostTeam) {
                val counterValue = lostHero.heroId.getCounter(
                    wonTeam.map { it.heroId },
                    GlobalVars.importantSettings.heroesProValues
                )
                var relevantValue = getRelevantValue(counterValue)
                if (relevantValue != 0) {
                    counterStatsMap[relevantValue]!![lostHero.heroId]!!.apply {
                        matchCount++
                    }
                }
                val synergyValue = lostHero.heroId.getSynergy(
                    lostTeam.map { it.heroId },
                    GlobalVars.importantSettings.heroesProValues
                )
                relevantValue = getRelevantValue(synergyValue)
                if (relevantValue != 0) {
                    synergyStatsMap[relevantValue]!![lostHero.heroId]!!.apply {
                        matchCount++
                    }
                }
            }
        }
    }

    suspend operator fun invoke(
        dotaMatches: List<DotaMatch>,
    ): String {
        calibrateProMatches(dotaMatches.takeLast(2000))

        for (hero in GlobalVars.mainSettings.heroes) {
            val negativeStats = counterStatsMap[-mainValue]!![hero.id]!!
            counterStatsMap[-mainValue]!![hero.id]!!.apply {
                winRate = getPercentage(negativeStats.winCount, negativeStats.matchCount)
            }
            val positiveStats = counterStatsMap[mainValue]!![hero.id]!!
            counterStatsMap[mainValue]!![hero.id]!!.apply {
                winRate = getPercentage(positiveStats.winCount, positiveStats.matchCount)
            }
        }

        for (hero in GlobalVars.mainSettings.heroes) {
            val negativeStats = synergyStatsMap[-mainValue]!![hero.id]!!
            synergyStatsMap[-mainValue]!![hero.id]!!.apply {
                winRate = getPercentage(negativeStats.winCount, negativeStats.matchCount)
            }
            val positiveStats = synergyStatsMap[mainValue]!![hero.id]!!
            synergyStatsMap[mainValue]!![hero.id]!!.apply {
                winRate = getPercentage(positiveStats.winCount, positiveStats.matchCount)
            }
        }

            var string = ""
        string += "COUNTERS\n"
        for (hero in GlobalVars.mainSettings.heroes) {
            val negativeStats = counterStatsMap[-mainValue]!![hero.id]!!
            val positiveStats = counterStatsMap[mainValue]!![hero.id]!!
            string += "${hero.name} ${negativeStats.winRate} -> ${positiveStats.winRate}\n"
        }
        string += "###################################\n"
        string += "###################################\n"
        string += "###################################\n"
        string += "SYNERGIES\n"
        for (hero in GlobalVars.mainSettings.heroes) {
            val negativeStats = synergyStatsMap[-mainValue]!![hero.id]!!
            val positiveStats = synergyStatsMap[mainValue]!![hero.id]!!
            string += "${hero.name} ${negativeStats.winRate} -> ${positiveStats.winRate} \n"
        }

        return string
    }

    private fun getPercentage(param1: Int, total: Int): Double {
        return if (total == 0) 0.0 else (param1 * 1.0 / total * 100).round2()
    }
}