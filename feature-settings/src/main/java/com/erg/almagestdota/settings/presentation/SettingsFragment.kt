package com.erg.almagestdota.settings.presentation

import android.content.Intent
import android.content.pm.PackageInfo
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.NotificationStatus
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdota.settings.BuildConfig
import com.erg.almagestdota.settings.R
import com.erg.almagestdota.settings.databinding.SettingsFragmentBinding
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
internal class SettingsFragment : Fragment(R.layout.settings_fragment) {

    companion object {
        const val TAG = "SettingsFragment"
    }

    private val binding by viewBinding(SettingsFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewModel by hiltNavGraphViewModels<SettingsViewModel>(R.id.graphSettings)
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private inner class ViewsConfigurator {

        fun initStartState() = with(binding) {
            ivClear.clicksWithDebounce {
                viewModel.obtainAction(SettingsViewActions.OpenDataUpdaterFragment)
            }
            tvGroups.clicksWithDebounce {
                viewModel.obtainAction(SettingsViewActions.OpenCustomGroups)
            }
            tvEmail.clicksWithDebounce {
                val mailIntent = Intent(Intent.ACTION_SENDTO)
                mailIntent.data = Uri.parse("mailto:")
                if (mailIntent.resolveActivity(requireActivity().packageManager) == null) {
                    val event = ViewEvent.AlmagestSnackbar(getString(R.string.no_email_snack_error), NotificationStatus.ERROR)
                    eventsConfigurator.handleViewEvent(event)
                } else {
                    mailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf("mr.mytherg@mail.ru"))
                    mailIntent.putExtra(Intent.EXTRA_SUBJECT, "App feedback")
                    startActivity(mailIntent)
                }
            }
        }

        fun renderState(state: SettingsViewState) = with(binding) {
            when (state) {
                is SettingsViewState.DefaultState -> renderDefaultState()
            }
        }

        private fun renderDefaultState() = with(binding) {
            ivClear.isVisible = BuildConfig.DEBUG
            val pInfo: PackageInfo = requireContext().packageManager.getPackageInfo(requireContext().packageName, 0)
            tvVersion.text = getString(R.string.app_version, pInfo.versionName)
            tvEmail.text = getString(R.string.write_to_developer)
            tvGroups.text = getString(R.string.custom_groups)
        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            navController.navigateViaScreenRoute(screen)
        }
    }
}