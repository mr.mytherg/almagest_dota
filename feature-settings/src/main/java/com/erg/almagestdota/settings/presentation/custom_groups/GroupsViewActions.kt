package com.erg.almagestdota.settings.presentation.custom_groups

internal sealed class GroupsViewActions {

    object CreateGroup : GroupsViewActions()
    class EditGroup(val key: String) : GroupsViewActions()
}