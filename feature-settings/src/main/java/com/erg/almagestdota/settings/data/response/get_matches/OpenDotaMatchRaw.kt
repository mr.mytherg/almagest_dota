package com.erg.almagestdota.settings.data.response.get_matches

import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.base.external.mapper.NotRequired
import com.erg.almagestdota.local_storage.external.models.leagues.findLeagueByBinaryMethod
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.settings.domain.model.get_matches.OpenDotaMatch
import com.google.gson.annotations.SerializedName
import javax.inject.Inject

data class OpenDotaMatchRaw(
    @NotRequired
    @SerializedName("match_id")
    val matchId: Long? = null,
    @NotRequired
    @SerializedName("radiant_team_id")
    val radiantTeamId: Long? = null,
    @NotRequired
    @SerializedName("dire_team_id")
    val direTeamId: Long? = null,
    @NotRequired
    @SerializedName("dire_score")
    val direScore: Int? = null,
    @NotRequired
    @SerializedName("series_id")
    val seriesId: Long? = null,
    @NotRequired
    @SerializedName("radiant_score")
    val radiantScore: Int? = null,
    @NotRequired
    @SerializedName("start_time")
    val startTime: Long? = null,
    @NotRequired
    @SerializedName("leagueid")
    val leagueId: Long? = null,
) {
    class MapperToOpenDotaMatch @Inject constructor() : EssentialMapper<OpenDotaMatchRaw, OpenDotaMatch>() {
        override fun transform(raw: OpenDotaMatchRaw): OpenDotaMatch {
            val currentLeagueTier = GlobalVars.leagues.findLeagueByBinaryMethod(raw.leagueId.orDefault()).tier
            return OpenDotaMatch(
                matchId = raw.matchId.orDefault(),
                direScore = raw.direScore.orDefault(),
                radiantScore = raw.radiantScore.orDefault(),
                radiantTeamId = raw.radiantTeamId.orDefault(),
                direTeamId = raw.direTeamId.orDefault(),
                seriesId = raw.seriesId.orDefault(),
                startTime = raw.startTime.orDefault(),
                tier = currentLeagueTier,
                leagueId = raw.leagueId.orDefault(),
            )
        }
    }
}