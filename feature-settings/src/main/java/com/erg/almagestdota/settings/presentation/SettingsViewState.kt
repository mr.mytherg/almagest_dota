package com.erg.almagestdota.settings.presentation

import androidx.constraintlayout.motion.utils.ViewState

internal sealed class SettingsViewState : ViewState() {
    object DefaultState : SettingsViewState()
}