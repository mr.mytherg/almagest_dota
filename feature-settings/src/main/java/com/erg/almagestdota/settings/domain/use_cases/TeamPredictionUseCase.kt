package com.erg.almagestdota.settings.domain.use_cases

import android.content.Context
import android.content.pm.PackageInfo
import android.util.Log
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamInfo
import com.erg.almagestdota.local_storage.external.models.leagues.LeagueTierType
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.network.external.firebase.FirebaseRepositoryContract
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.*
import kotlinx.coroutines.tasks.await
import javax.inject.Inject
import kotlin.math.absoluteValue
import kotlin.math.pow

class TeamPredictionUseCase @Inject constructor(
    private val iLocalStorageContract: ILocalStorageContract,
    private val firebaseRepository: FirebaseRepositoryContract
) {

    private val MATCHES_TO_PREDICT = 0.5
    private var calibratedCount = 0
    private var predictedCount = 0
    private val predictorTeam = PredictorTeam()

    private fun isValueFits(listValue: Int, currentValue: Int): Boolean {
        return if (listValue >= 300) {
            isValueFitsTillEnd(listValue, currentValue)
        } else {
            listValue in currentValue..(currentValue + 49)
        }
    }

    private fun isValueFitsTillEnd(listValue: Int, currentValue: Int): Boolean {
        return listValue >= currentValue
    }

    suspend operator fun invoke(
        viewModelScope: CoroutineScope,
        dotaMatches: List<DotaMatch>,
        teams: List<TeamInfo>
    ): String {
        var string = "matchCount ${dotaMatches.size}\n"
        Log.i("PREDICTION", "calibration started")
        val calibratorTeam = CalibratorTeam(dotaMatches, teams)

        val list = predictorTeam.predict(dotaMatches, viewModelScope, calibratorTeam)
        Log.i("PREDICTION", "prediction done")

        val predictionMap = mutableMapOf<Long, List<PredictionStateTeam>>()
//        for (team in teams) {
//            addPredictions(list.filter { it.radiantTeamId == team.id || it.direTeamId == team.id }, predictionMap, false, team.id)
//        }
        addPredictions(list, predictionMap, true, 0L)

        string += printTeamProbabilities(predictionMap)
        string += getTeamsRating(calibratorTeam.teamMap)
//        GlobalVars.mainPredictor.teamProbabilities = predictorTeam.teamProbabilities
        GlobalVars.teams = calibratorTeam.teamStatsMap.toMutableMap()
        iLocalStorageContract.setMainPredictor(GlobalVars.mainPredictor)
        firebaseRepository.setMainPredictor(GlobalVars.mainPredictor)
        firebaseRepository.setTeamsSettings(TeamsSettings(GlobalVars.teams.map { it.value }))

        return string
    }

    private fun addPredictions(
        list: List<PredictionTeam>,
        predictionMap: MutableMap<Long, List<PredictionStateTeam>>,
        isOverall: Boolean = false,
        teamId: Long
    ) {
        val mutableList = mutableListOf<PredictionStateTeam>()
        for (value in 0..300 step 50) {
            val successEarly =
                list.filter { it.isSuccess && isValueFits(it.value, value) && isEarlyGame(it.duration) }
            val failEarly = list.filter {
                it.isSuccess.not() && isValueFits(it.value, value) && isEarlyGame(it.duration)
            }
            val successMid =
                list.filter { it.isSuccess && isValueFits(it.value, value) && isMidGame(it.duration) }
            val failMid =
                list.filter { it.isSuccess.not() && isValueFits(it.value, value) && isMidGame(it.duration) }
            val successLate =
                list.filter { it.isSuccess && isValueFits(it.value, value) && isLateGame(it.duration) }
            val failLate =
                list.filter { it.isSuccess.not() && isValueFits(it.value, value) && isLateGame(it.duration) }

            if (successEarly.size + failEarly.size + successMid.size + failMid.size + successLate.size + failLate.size == 0) {
                continue
            }

            val prediction = PredictionStateTeam(
                startValue = value.toDouble(),
                successEarly = successEarly.map { it.matchId },
                failEarly = failEarly.map { it.matchId },
                successPercentageEarly = getPercentage(successEarly.size, failEarly.size),
                successMid = successMid.map { it.matchId },
                failMid = failMid.map { it.matchId },
                successPercentageMid = getPercentage(successMid.size, failMid.size),
                successLate = successLate.map { it.matchId },
                failLate = failLate.map { it.matchId },
                successPercentageLate = getPercentage(successLate.size, failLate.size),
            )
            mutableList.add(prediction)
        }
        predictionMap[if (isOverall) 0 else teamId] = mutableList
    }

    private fun printTeamProbabilities(predictionMap: MutableMap<Long, List<PredictionStateTeam>>): String {
        var string = "calibrated by - $calibratedCount matches\n"
        string += "predicted count - $predictedCount\n"
        var totalpercentage = 0.0
        val badTeamsList = mutableListOf<Triple<Long, Int, Int>>()
        for ((id, predictions) in predictionMap) {
            if (predictions.isEmpty()) continue
            string += "${GlobalVars.teams[id]?.name ?: "OVERALL STATS"} \n"
            string += "$id \n"
            var wins = 0
            var loses = 0
            for (prediction in predictions) {
                string += "rating difference - ${prediction.startValue}\n"
                string += "Early success - ${prediction.successEarly.size} fail - ${prediction.failEarly.size} % - ${prediction.successPercentageEarly}\n"
                string += "Mid success - ${prediction.successMid.size} fail - ${prediction.failMid.size} % - ${prediction.successPercentageMid}\n"
                string += "Late success - ${prediction.successLate.size} fail - ${prediction.failLate.size} % - ${prediction.successPercentageLate}\n"
                string += "winrate % avg - ${getSuccess(prediction)} "
                val successSum = prediction.successLate.size + prediction.successMid.size + prediction.successEarly.size
                val failSum = prediction.failLate.size + prediction.failMid.size + prediction.failEarly.size
                val percentage = ((successSum + failSum) * 1.0 / predictedCount * 100).round2()
                string += "% of games - ${percentage}\n"
                string += "\n"
                totalpercentage += percentage
                when {
                    prediction.startValue == 50.0 -> predictorTeam.teamProbabilities[TeamPredictionType.P50_100.name] =
                        getSuccess(prediction).toDouble()
                    prediction.startValue == 100.0 -> predictorTeam.teamProbabilities[TeamPredictionType.P100_150.name] =
                        getSuccess(prediction).toDouble()
                    prediction.startValue == 150.0 -> predictorTeam.teamProbabilities[TeamPredictionType.P150_200.name] =
                        getSuccess(prediction).toDouble()
                    prediction.startValue == 200.0 -> predictorTeam.teamProbabilities[TeamPredictionType.P200_250.name] =
                        getSuccess(prediction).toDouble()
                    prediction.startValue == 250.0 -> predictorTeam.teamProbabilities[TeamPredictionType.P250_300.name] =
                        getSuccess(prediction).toDouble()
                    prediction.startValue == 300.0 -> predictorTeam.teamProbabilities[TeamPredictionType.P300.name] =
                        getSuccess(prediction).toDouble()
                    else -> Unit
                }
            }
            if (loses > wins) {
                badTeamsList.add(Triple(id, wins, loses))
            }
        }
        string += "$######################################\n"
        string += "$######################################\n"
        string += "$######################################\n"
        string += "$######################################\n"
        for ((teamId, wins, loses) in badTeamsList) {
            string += "${GlobalVars.leagues.find { it.id == teamId }?.name.orDefault()} \n"
            string += "$teamId wins - $wins loses - $loses wr - ${getPercentage(wins, loses)}%\n"
        }
        string += "unpredictable % ${100 - totalpercentage}\n"
        return string
    }

    private fun getTeamsRating(teamMap: MutableMap<Long, EloRating>): String {
        var string = "${teamMap.entries.fold(0) { acc, next -> acc + next.value.matchCount }}\n"
        val sorted = teamMap.entries.sortedByDescending { it.value.value }
        for ((teamName, elo) in sorted) {
            if (elo.matchCount >= GlobalVars.CALIBRATION_MATCH_COUNT) {
                string += "value |${elo.value}| matches |${elo.matchCount}| name |$teamName|\n"
                string += "earlyMatches% |${
                    getPercentage(
                        (elo.earlyWinCount + elo.earlyLoseCount),
                        (elo.midLoseCount + elo.midWinCount + elo.lateLoseCount + elo.lateWinCount)
                    )
                }| win |${getPercentage(elo.earlyWinCount, elo.earlyLoseCount)}| countWin |${elo.earlyWinCount}|\n"
                string += "midMatches% |${
                    getPercentage(
                        (elo.midWinCount + elo.midLoseCount),
                        (elo.earlyWinCount + elo.earlyLoseCount + elo.lateLoseCount + elo.lateWinCount)
                    )
                }| win |${getPercentage(elo.midWinCount, elo.midLoseCount)}| countWin |${elo.midWinCount}|\n"
                string += "lateMatches% |${
                    getPercentage(
                        (elo.lateWinCount + elo.lateLoseCount),
                        (elo.midLoseCount + elo.midWinCount + elo.earlyWinCount + elo.earlyLoseCount)
                    )
                }| win |${getPercentage(elo.lateWinCount, elo.lateLoseCount)}| countWin |${elo.lateWinCount}|\n\n"
            }
        }
        return string
    }

    private fun getSuccess(prediction: PredictionStateTeam): String {
        val successSum = prediction.successLate.size + prediction.successMid.size + prediction.successEarly.size
        val failSum = prediction.failLate.size + prediction.failMid.size + prediction.failEarly.size
        return getPercentage(successSum, failSum).toString()
    }

    private fun isEarlyGame(duration: Long) = duration / 60 <= TIME_1
    private fun isMidGame(duration: Long) = duration / 60 in TIME_1 + 1..TIME_2
    private fun isLateGame(duration: Long) = duration / 60 > TIME_2

    private fun getPercentage(param1: Int, param2: Int): Double {
        return if (param1 + param2 == 0) 0.0 else (param1 * 1.0 / (param1 + param2) * 100).round2()
    }

    private inner class PredictorTeam {
        val teamProbabilities: MutableMap<String, Double> = mutableMapOf()

        suspend fun predict(
            list: List<DotaMatch>,
            viewModelScope: CoroutineScope,
            calibratorTeam: CalibratorTeam
        ): List<PredictionTeam> {
            val predictions = mutableListOf<PredictionTeam>()

            for (match in list) {
                val radiantStats = calibratorTeam.teamStatsMap[match.radiantTeamId]!!
                val direStats = calibratorTeam.teamStatsMap[match.direTeamId]!!
                val value = radiantStats.rating - direStats.rating
                if (radiantStats.matchCount.orDefault() < GlobalVars.CALIBRATION_MATCH_COUNT) {
                    calibratorTeam.calibrateMatch(match)
                    calibratedCount++
                    continue
                }
                if (direStats.matchCount.orDefault() < GlobalVars.CALIBRATION_MATCH_COUNT) {
                    calibratorTeam.calibrateMatch(match)
                    calibratedCount++
                    continue
                }
                predictedCount++
                predictions.add(
                    PredictionTeam(
                        isSuccess = match.hasRadiantWon && value >= 0 || !match.hasRadiantWon && value < 0,
                        hasRadiantWon = match.hasRadiantWon,
                        radiantTeamName = GlobalVars.teams[match.radiantTeamId]?.name.orDefault(),
                        direTeamName = GlobalVars.teams[match.direTeamId]?.name.orDefault(),
                        value = value.absoluteValue,
                        matchId = match.matchId,
                        radiantTeamId = match.radiantTeamId,
                        direTeamId = match.direTeamId,
                        duration = match.duration,
                    )
                )
                calibratorTeam.calibrateMatch(match)
            }
            return predictions
        }
    }

    private inner class CalibratorTeam(private val dotaMatches: List<DotaMatch>, private val teams: List<TeamInfo>) {
        val teamMap: MutableMap<Long, EloRating> = mutableMapOf()
        val teamStatsMap: MutableMap<Long, TeamInfo> = mutableMapOf()

        init {
            val radiant: Set<Long> = dotaMatches.map { it.radiantTeamId }.toSet()
            val dire: Set<Long> = dotaMatches.map { it.direTeamId }.toSet()
            val total = radiant.plus(dire).toList()

            for (teamId in total) {
                teamMap[teamId] = EloRating()
                teamStatsMap[teamId] = teams.find { it.id == teamId }!!
            }
        }

        fun calibrateMatch(match: DotaMatch) {
            val radiant = teamMap[match.radiantTeamId]!!
            val dire = teamMap[match.direTeamId]!!
            val radiantPair: Pair<Int, Int> = Pair(radiant.matchCount, radiant.value)
            val direPair: Pair<Int, Int> = Pair(dire.matchCount, dire.value)
            (teamMap[match.radiantTeamId]!!).apply {
                value = getNewValue(
                    match.leagueTierType,
                    radiantPair,
                    direPair,
                    match.hasRadiantWon
                )
            }
            addOtherValues(match.radiantTeamId, match.hasRadiantWon, match.duration)
            teamStatsMap[match.radiantTeamId] = getTeamStats(true, match, teamMap[match.radiantTeamId]!!)
            (teamMap[match.direTeamId]!!).apply {
                value = getNewValue(
                    match.leagueTierType,
                    direPair,
                    radiantPair,
                    !match.hasRadiantWon
                )
            }
            addOtherValues(match.direTeamId, !match.hasRadiantWon, match.duration)
            teamStatsMap[match.direTeamId] = getTeamStats(false, match, teamMap[match.direTeamId]!!)
        }

        private fun getTeamStats(isRadiant: Boolean, match: DotaMatch, eloRating: EloRating): TeamInfo {
            val teamStats = if (isRadiant) {
                teamStatsMap[match.radiantTeamId] ?: teams.find { it.id == match.radiantTeamId }!!
            } else {
                teamStatsMap[match.direTeamId] ?: teams.find { it.id == match.direTeamId }!!
            }
            teamStats.rating = eloRating.value
            teamStats.matchCount = eloRating.matchCount
            return teamStats
        }

        private fun addOtherValues(teamId: Long, won: Boolean, duration: Long) {
            addGameStagesValues(teamId, won, duration)
            teamMap[teamId]!!.matchCount++
        }

        private fun addGameStagesValues(teamId: Long, won: Boolean, duration: Long) {
            when {
                isEarlyGame(duration) -> if (won) teamMap[teamId]!!.earlyWinCount++ else teamMap[teamId]!!.earlyLoseCount++
                isMidGame(duration) -> if (won) teamMap[teamId]!!.midWinCount++ else teamMap[teamId]!!.midLoseCount++
                else -> if (won) teamMap[teamId]!!.lateWinCount++ else teamMap[teamId]!!.lateLoseCount++
            }
        }

        private fun getNewValue(
            tier: LeagueTierType,
            teamA: Pair<Int, Int>,
            teamB: Pair<Int, Int>,
            hasWon: Boolean
        ): Int {
            val maxDiff = 400.0
            var maxChange: Double = when {
                teamA.first < GlobalVars.CALIBRATION_MATCH_COUNT -> 40.0
                else -> 20.0
            }
            maxChange *= if (tier == LeagueTierType.PREMIUM) 3.0 else 1.0

            val result = if (hasWon) 1 else 0
            val ea = 1 / (1 + 10.0.pow((teamB.second - teamA.second) * 1.0 / maxDiff))
            return (teamA.second + maxChange * (result - ea)).toInt()
        }
    }

    companion object {
        private const val TIME_1 = 25
        private const val TIME_2 = 40
    }
}

data class PredictionTeam(
    val isSuccess: Boolean,
    val hasRadiantWon: Boolean,
    val radiantTeamName: String,
    val direTeamName: String,
    val value: Int,
    val matchId: Long,
    val radiantTeamId: Long,
    val direTeamId: Long,
    val duration: Long,
)

data class PredictionStateTeam(
    val startValue: Double,
    val successEarly: List<Long>,
    val failEarly: List<Long>,
    val successPercentageEarly: Double,
    val successMid: List<Long>,
    val failMid: List<Long>,
    val successPercentageMid: Double,
    val successLate: List<Long>,
    val failLate: List<Long>,
    val successPercentageLate: Double,
)

data class EloRating(
    var matchCount: Int = 0,
    var earlyWinCount: Int = 0, //
    var earlyLoseCount: Int = 0, //
    var midWinCount: Int = 0, //
    var midLoseCount: Int = 0, //
    var lateWinCount: Int = 0, //
    var lateLoseCount: Int = 0, //
    var value: Int = 1400, //
)