package com.erg.almagestdota.settings.domain.use_cases

import android.content.Context
import android.content.pm.PackageInfo
import android.util.Log
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.drafting.domain.use_cases.LoadTeamInfoStratzUseCase
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.DotaItem
import com.erg.almagestdota.local_storage.external.models.draft.HeroItems
import com.erg.almagestdota.local_storage.external.models.draft.HeroStatsData
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.main_settings.MainSettings
import com.erg.almagestdota.local_storage.external.models.main_settings.TeamsSettings
import com.erg.almagestdota.network.external.firebase.FirebaseRepositoryContract
import com.google.firebase.firestore.FirebaseFirestore
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.*
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.lang.Exception
import javax.inject.Inject

class UpdateTeamNamesUseCase @Inject constructor(
    private val firebaseRepository: FirebaseRepositoryContract,
    private val localStorage: ILocalStorageContract,
    private val loadTeamInfoStratzUseCase: LoadTeamInfoStratzUseCase,

    ) {
    suspend operator fun invoke(viewModelScope: CoroutineScope) {
        val matches = firebaseRepository.getMatches().takeLast(2000)
        val mutableTeamIds = mutableSetOf<Long>()
        for (match in matches) {
            mutableTeamIds.add(match.radiantTeamId)
            mutableTeamIds.add(match.direTeamId)
        }
        val deferredList = mutableListOf<Deferred<TeamInfo>>()
        for (teamId in mutableTeamIds) {
            val deferred = viewModelScope.async {
                loadTeamInfoStratzUseCase.invoke(teamId)
            }
            deferredList.add(deferred)
        }
        val teamNames = deferredList.awaitAll()
        for (team in teamNames) {
            GlobalVars.teams[team.id]?.apply {
                name = team.name
            }
        }
        firebaseRepository.setTeamsSettings(TeamsSettings(GlobalVars.teams.map { it.value }))
    }

    private suspend fun getTeamName(teamId: Long): String {
        val url = "https://www.dotabuff.com/esports/teams/$teamId"
        var doc: Document
        while (true) {
            try {
                doc = Jsoup.connect(url)
                    .header("Cookie",
                        "_tz=Asia%2FSaigon; _ga=GA1.2.1122684581.1669626336; __qca=P0-773539065-1669626336277; _hjSessionUser_2490228=eyJpZCI6Ijc0MGQ3Y2U2LWQwZjItNTgwZi1iNzkwLTMxZjA4OTJjNjhjNSIsImNyZWF0ZWQiOjE2Njk2MjYzMzYzMjgsImV4aXN0aW5nIjp0cnVlfQ==; _gid=GA1.2.2120575359.1671292247; _hjSession_2490228=eyJpZCI6IjRjMDczYWUwLWE2YjMtNDYyZC1iNjU2LTU3ZWY2ZWMxZWZmOCIsImNyZWF0ZWQiOjE2NzEzMzg0MTg3MjQsImluU2FtcGxlIjpmYWxzZX0=; _hjAbsoluteSessionInProgress=0; _hjIncludedInSessionSample=0; _hi=1671343654459")
                    .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.174 YaBrowser/22.1.3.848 Yowser/2.5 Safari/537.36")
                    .header("Connection", "keep-alive")
                    .header("sec-ch-ua", "\"Not?A_Brand\";v=\"8\", \"Chromium\";v=\"108\", \"Microsoft Edge\";v=\"108\"")
                    .timeout(0)
                    .get()
                break
            } catch (e: Exception) {
                Log.e("ERROR", "url |$url|")
                delay(300)
            }
        }

        val element = doc.getElementsByAttributeValue("class", "header-content-title").select("h1")[0]
        return element.text().substring(0, element.text().indexOf("Summary"))
    }
}