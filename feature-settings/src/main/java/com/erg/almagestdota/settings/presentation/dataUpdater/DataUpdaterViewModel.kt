package com.erg.almagestdota.settings.presentation.dataUpdater

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.extensions.launchOnIO
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.NotificationStatus
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.settings.domain.Interactor
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
internal class DataUpdaterViewModel @Inject constructor(
    private val stringProvider: StringProvider,
    private val interactor: Interactor,
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data -> }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    private val loadingStateMutable = MutableStateFlow<Loading>(Loading.Disabled)
    val loadingState = loadingStateMutable.asStateFlow()

    fun obtainAction(action: DataUpdaterViewActions) {
        when (action) {
            DataUpdaterViewActions.LoadHeroesCoreData -> loadHeroesCore()
            DataUpdaterViewActions.LoadHeroesCoefficientsSynergiesData -> loadCoefficientsSynergy()
            DataUpdaterViewActions.LoadNewMatches -> loadNewMatches()
            DataUpdaterViewActions.LoadOldMatches -> loadOldMatches()
            DataUpdaterViewActions.LoadHeroesRolesData -> loadRoles()
            DataUpdaterViewActions.LoadHeroesCoefficientsCountersData -> loadCoefficientsCounters()
            DataUpdaterViewActions.LoadDotaItems -> loadItems()
            DataUpdaterViewActions.PredictMatches -> predictMatches()
            DataUpdaterViewActions.PredictMatches2 -> predictMatches2()
            DataUpdaterViewActions.PredictTeams -> predictTeams()
            DataUpdaterViewActions.PredictPlayers -> predictPlayers()
            DataUpdaterViewActions.SaveTeams -> saveTeams()
            DataUpdaterViewActions.LoadDotaCounterItems -> loadCounterItems()
            DataUpdaterViewActions.UpdateHeroStages -> updateHeroStages()
        }
    }

    private fun handleError(e: Exception) {
        stateConfigurator.msg = e.message.orDefault()
        viewStateMutable.value = stateConfigurator.defineFragmentState()
    }

    private fun loadOldMatches() {
        launchOnIO(
            loader = loadingStateMutable,
            action = { interactor.loadOldMatches() },
            onSuccess = {
                val text = "success"
                emit(viewEventMutable, ViewEvent.AlmagestSnackbar(message = text, status = NotificationStatus.SUCCESS))
            },
            onError = ::handleError
        )
    }

    private fun loadNewMatches() {
        launchOnIO(
            loader = loadingStateMutable,
            action = { interactor.loadNewMatches() },
            onSuccess = {
                val text = "success"
                emit(viewEventMutable, ViewEvent.AlmagestSnackbar(message = text, status = NotificationStatus.SUCCESS))
            },
            onError = ::handleError
        )
    }

    private fun loadRoles() {
        launchOnIO(
            loader = loadingStateMutable,
            action = { interactor.loadHeroesRolesData(viewModelScope) },
            onSuccess = {
                val text = "success"
                emit(viewEventMutable, ViewEvent.AlmagestSnackbar(message = text, status = NotificationStatus.SUCCESS))
            },
            onError = ::handleError
        )
    }

    private fun loadCoefficientsSynergy() {
        launchOnIO(
            loader = loadingStateMutable,
            action = { interactor.loadCoefficientsSynergy(viewModelScope) },
            onSuccess = {
                val text = "success"
                emit(viewEventMutable, ViewEvent.AlmagestSnackbar(message = text, status = NotificationStatus.SUCCESS))
            },
            onError = ::handleError
        )
    }

    private fun loadCoefficientsCounters() {
        launchOnIO(
            loader = loadingStateMutable,
            action = { interactor.loadCoefficientsCounters(viewModelScope) },
            onSuccess = {
                val text = "success"
                emit(viewEventMutable, ViewEvent.AlmagestSnackbar(message = text, status = NotificationStatus.SUCCESS))
            },
            onError = ::handleError
        )
    }

    private fun updateHeroStages() {
        launchOnIO(
            loader = loadingStateMutable,
            action = {  },
            onSuccess = {
                val text = "nothing"
                emit(viewEventMutable, ViewEvent.AlmagestSnackbar(message = text, status = NotificationStatus.SUCCESS))
            },
            onError = ::handleError
        )
    }

    private fun loadHeroesCore() {
        launchOnIO(
            loader = loadingStateMutable,
            action = { interactor.loadHeroesCoreData() },
            onSuccess = {
                val text = "success"
                emit(viewEventMutable, ViewEvent.AlmagestSnackbar(message = text, status = NotificationStatus.SUCCESS))
            },
            onError = ::handleError
        )
    }

    private fun loadItems() {
        launchOnIO(
            loader = loadingStateMutable,
            action = { interactor.loadDotaItems(viewModelScope) },
            onSuccess = {
                val text = "success"
                emit(viewEventMutable, ViewEvent.AlmagestSnackbar(message = text, status = NotificationStatus.SUCCESS))
            },
            onError = ::handleError
        )
    }

    private fun predictMatches2() {
        launchOnIO(
            loader = loadingStateMutable,
            action = { interactor.predictMatches2(viewModelScope) },
            onSuccess = {
                stateConfigurator.msg = it
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            },
            onError = ::handleError
        )
    }

    private fun predictMatches() {
        launchOnIO(
            loader = loadingStateMutable,
            action = { interactor.predictMatches(viewModelScope) },
            onSuccess = {
                stateConfigurator.msg = it
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            },
            onError = ::handleError
        )
    }

    private fun predictTeams() {
        launchOnIO(
            loader = loadingStateMutable,
            action = { interactor.predictTeams(viewModelScope) },
            onSuccess = {
                stateConfigurator.msg = it
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            },
            onError = ::handleError
        )
    }

    private fun predictPlayers() {
        launchOnIO(
            loader = loadingStateMutable,
            action = { interactor.predictPlayers(viewModelScope) },
            onSuccess = {
                stateConfigurator.msg = it
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            },
            onError = ::handleError
        )
    }

    private fun saveTeams() {
        launchOnIO(
            loader = loadingStateMutable,
            action = { interactor.saveTeams() },
            onSuccess = {
                stateConfigurator.msg = it.toString()
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            },
            onError = ::handleError
        )
    }

    private fun loadCounterItems() {
        launchOnIO(
            loader = loadingStateMutable,
            action = { interactor.loadDotaCounterItems(viewModelScope) },
            onSuccess = {
                val text = "success"
                emit(viewEventMutable, ViewEvent.AlmagestSnackbar(message = text, status = NotificationStatus.SUCCESS))
            },
            onError = ::handleError
        )
    }

    private inner class StateConfigurator {
        var msg = ""

        fun defineFragmentState(): DataUpdaterViewState {
            return DataUpdaterViewState.SomeState(msg)
        }
    }
}