package com.erg.almagestdota.settings.domain.use_cases

import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.network.external.firebase.FirebaseRepositoryContract
import com.erg.almagestdota.drafting.data.IStratzGraphqlApi
import com.erg.almagestdota.drafting.data.raw.KillsEventRaw
import com.erg.almagestdota.local_storage.external.models.draft.GameStageData
import com.erg.almagestdota.local_storage.external.models.draft.HeroStatsData
import com.erg.almagestdota.local_storage.external.models.draft.RoleType
import javax.inject.Inject

class LoadCoreDataUseCase @Inject constructor(
    private val stratzGraphqlApi: IStratzGraphqlApi,
    private val iLocalStorageContract: ILocalStorageContract,
    private val firebaseRepository: FirebaseRepositoryContract
) {
    suspend operator fun invoke() {
        val result = stratzGraphqlApi.getHeroDamage()
        val stats = result.data.heroStats.stats
        for (hero in stats) {
            GlobalVars.mainSettings.heroes.find { it.id == hero.heroId }?.apply {
                val totalDmg = hero.magicalDamage + hero.physicalDamage + hero.pureDamage
                magicalDamagePercentage = (hero.magicalDamage / totalDmg * 100).round2()
                physicalDamagePercentage = (hero.physicalDamage / totalDmg * 100).round2()
                pureDamagePercentage = (hero.pureDamage / totalDmg * 100).round2()
                towerDamage = hero.towerDamage
                healing = hero.healingAllies + hero.healingItemAllies
            } ?: throw IllegalStateException()
        }
        getHeroPositionStats()
        iLocalStorageContract.setMainSettings(GlobalVars.mainSettings)
        firebaseRepository.setMainSettings(GlobalVars.mainSettings)
    }

    private suspend fun getHeroPositionStats() {
        var result = stratzGraphqlApi.getHeroKillsLate()
        val lateStage = result.data.heroStats.late.groupBy { it.heroId }
        addStage(lateStage) { heroId: Int, roles: List<GameStageData> ->
            GlobalVars.mainSettings.heroes.find { it.id == heroId }?.apply { this.late = roles } ?: throw IllegalStateException()
        }

        result = stratzGraphqlApi.getHeroKillsMid()
        val midStage = result.data.heroStats.mid.groupBy { it.heroId }
        addStage(midStage) { heroId: Int, roles: List<GameStageData> ->
            GlobalVars.mainSettings.heroes.find { it.id == heroId }?.apply { this.mid = roles } ?: throw IllegalStateException()
        }

        result = stratzGraphqlApi.getHeroKillsEarly()
        val earlyStage = result.data.heroStats.early.groupBy { it.heroId }
        addStage(earlyStage) { heroId: Int, roles: List<GameStageData> ->
            GlobalVars.mainSettings.heroes.find { it.id == heroId }?.apply { this.early = roles } ?: throw IllegalStateException()
        }

        result = stratzGraphqlApi.getHeroKillsLaning()
        val laningStage = result.data.heroStats.laning.groupBy { it.heroId }
        addStage(laningStage) { heroId: Int, roles: List<GameStageData> ->
            GlobalVars.mainSettings.heroes.find { it.id == heroId }?.apply { this.laning = roles } ?: throw IllegalStateException()
        }

//        result = stratzGraphqlApi.getHeroKillsSuperLate()
//        val superLateStage = result.data.heroStats.superLate.groupBy { it.heroId }
//        addStage(superLateStage) { heroId: Int, roles: List<GameStageData> ->
//            GlobalVars.mainSettings.heroes.find { it.id == heroId }?.apply { this.superLate = roles } ?: throw IllegalStateException()
//        }
    }

    private fun addStage(stage: Map<Int, List<KillsEventRaw>>, function: (Int, List<GameStageData>) -> HeroStatsData) {
        for ((heroId, roles) in stage) {
            val heroGameStage = mutableListOf<GameStageData>()
            for (role in roles) {
                val gameStageData = GameStageData(
                    kills = role.kills,
                    deaths = role.deaths,
                    assists = role.assists,
                    magicalDamage = role.magicalDamage,
                    position = RoleType.byPosition(role.position),
                    cs = role.cs,
                    dn = role.dn,
                    neutrals = role.neutrals,
                    heroDamage = role.heroDamage,
                    towerDamage = role.towerDamage,
                    healingAllies = role.healingAllies,
                    damageReceived = role.damageReceived,
                    disableCount = role.disableCount,
                    disableDuration = role.disableDuration,
                    stunCount = role.stunCount,
                    stunDuration = role.stunDuration,
                )
                heroGameStage.add(gameStageData)
            }

            function.invoke(heroId, heroGameStage)
        }
    }
}