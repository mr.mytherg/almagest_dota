package com.erg.almagestdota.settings.presentation

internal sealed class SettingsViewActions {

    object OpenDataUpdaterFragment : SettingsViewActions()
    object OpenCustomGroups : SettingsViewActions()
}