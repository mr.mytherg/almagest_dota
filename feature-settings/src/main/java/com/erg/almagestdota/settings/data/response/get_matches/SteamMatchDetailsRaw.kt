package com.erg.almagestdota.settings.data.response.get_matches

import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.mapper.EssentialMapper
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerInfo
import com.erg.almagestdota.settings.domain.model.get_matches.SteamDotaMatch
import com.erg.almagestdota.settings.domain.model.get_matches.SteamDotaMatchGameMode
import com.erg.almagestdota.settings.domain.model.get_matches.SteamDotaMatchLobbyType
import com.google.gson.annotations.SerializedName
import javax.inject.Inject

data class SteamMatchDetailsRaw(
    @SerializedName("result") val result: SteamMatchDetailsResultRaw? = null,
) {
    class MapperToSteamDotaMatch @Inject constructor() : EssentialMapper<SteamMatchDetailsRaw, SteamDotaMatch>() {
        override fun transform(raw: SteamMatchDetailsRaw): SteamDotaMatch {
            val result = raw.result

            val radiantHeroes = mutableListOf<PlayerInfo>()
            val direHeroes = mutableListOf<PlayerInfo>()
            val radiantBans = mutableListOf<Int>()
            val direBans = mutableListOf<Int>()
            for (item in result?.picksBans.orEmpty()) {
                val heroId = item.heroId.orDefault()
                when {
                    item.team.orDefault() == 0 -> {
                        if (item.isPick.orDefault()) {
                            radiantHeroes.add(
                                PlayerInfo(heroId = heroId, playerId = result?.players?.find { it.heroId == heroId }?.accountId.orDefault())
                            )
                        } else {
                            radiantBans.add(item.heroId.orDefault())
                        }
                    }
                    else -> {
                        if (item.isPick.orDefault()) {
                            direHeroes.add(
                                PlayerInfo(heroId = heroId, playerId = result?.players?.find { it.heroId == heroId }?.accountId.orDefault())
                            )
                        } else {
                            direBans.add(item.heroId.orDefault())
                        }
                    }
                }
            }

            return SteamDotaMatch(
                hasRadiantWon = result?.hasRadiantWon.orDefault(),
                hasRadiantFirstPick = result?.picksBans?.firstOrNull()?.team == 0,
                duration = result?.duration.orDefault(),
                matchId = result?.matchId.orDefault(),
                radiantHeroes = radiantHeroes,
                direHeroes = direHeroes,
                radiantBans = radiantBans,
                direBans = direBans,
                gameMode = SteamDotaMatchGameMode.byGameMode(result?.gameMode),
                lobbyType = SteamDotaMatchLobbyType.byLobbyType(result?.lobbyType),
            )
        }
    }
}