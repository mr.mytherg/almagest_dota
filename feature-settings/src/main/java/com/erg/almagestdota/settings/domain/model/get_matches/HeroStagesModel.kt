package com.erg.almagestdota.settings.domain.model.get_matches

import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.local_storage.external.models.draft.StageType

class HeroStagesModel(
    val stageType: StageType,
    var pickCount: Int = 0,
    var winCount: Int = 0,
    var lostCount: Int = 0,
) {
    fun getWinRate() =
        if (winCount == 0) 0.0 else (winCount * 1.0 / (winCount + lostCount) * 100).round2()

    fun getPercentage(param1: Int, param2: Int) =
        if (param1 == 0) 0.0 else (param1 * 1.0 / (param2) * 100).round2()
}