package com.erg.almagestdota.stats.presentation.hero_impacts

import androidx.constraintlayout.motion.utils.ViewState

internal sealed class HeroImpactsViewState : ViewState() {
    object DefaultState : HeroImpactsViewState()
}