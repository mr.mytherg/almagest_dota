package com.erg.almagestdota.stats.presentation.changeable_stats.hero_roles.single_hero_roles

internal sealed class SingleHeroRolesActions {

    object SortByRole : SingleHeroRolesActions()
    object SortByMatchCount : SingleHeroRolesActions()
    object SortByWinRate : SingleHeroRolesActions()
    object SortByPickRate : SingleHeroRolesActions()
    class ShowMatches(val key: String) : SingleHeroRolesActions()
}