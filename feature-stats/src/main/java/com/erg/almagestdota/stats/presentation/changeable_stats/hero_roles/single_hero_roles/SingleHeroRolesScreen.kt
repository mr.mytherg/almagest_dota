package com.erg.almagestdota.stats.presentation.changeable_stats.hero_roles.single_hero_roles

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen

class SingleHeroRolesScreen(route: NavDirections) : Screen<NavDirections>(
    route = route,
    requestKey = SingleHeroRolesFragment.TAG
)