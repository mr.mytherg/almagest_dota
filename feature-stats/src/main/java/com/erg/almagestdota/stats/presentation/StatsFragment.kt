package com.erg.almagestdota.stats.presentation

import android.content.Intent
import android.content.pm.PackageInfo
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.NotificationStatus
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdota.stats.R
import com.erg.almagestdota.stats.databinding.StatsFragmentBinding
import com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.DraftStagesViewActions
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.hero_impacts_fragment.*
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
internal class StatsFragment : Fragment(R.layout.stats_fragment) {

    companion object {
        const val TAG = "StatsFragment"
    }

    private val binding by viewBinding(StatsFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewModel by hiltNavGraphViewModels<StatsViewModel>(R.id.graphStats)
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
        viewModel.viewEvent.setResultListener(this)
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).filterNotNull().onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.loadingState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach {
            binding.loadingView.isVisible = it is Loading.Enabled
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private inner class ViewsConfigurator {

        fun initStartState() = with(binding) {
            tvAllTeams.clicksWithDebounce {
                viewModel.obtainAction(StatsViewActions.OpenAllTeams)
            }
            tvSelectTeam.clicksWithDebounce {
                viewModel.obtainAction(StatsViewActions.OpenSelectTeam)
            }
            tvCoefs.clicksWithDebounce {
                viewModel.obtainAction(StatsViewActions.OpenCoefs)
            }
            tvOpenSelection.clicksWithDebounce {
                viewModel.obtainAction(StatsViewActions.OpenTournaments)
            }
            tvHeroImpacts.clicksWithDebounce {
                viewModel.obtainAction(StatsViewActions.OpenHeroImpacts)
            }
            tvPlayersLanes.clicksWithDebounce {
                viewModel.obtainAction(StatsViewActions.OpenPlayersLanes)
            }
        }

        fun renderState(state: StatsViewState) = with(binding) {
            when (state) {
                is StatsViewState.DefaultState -> renderDefaultState()
            }
        }

        private fun renderDefaultState() = with(binding) {

        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            viewModel.viewEvent.setResultListener(this@StatsFragment)
            navController.navigateViaScreenRoute(screen)
        }
    }
}