package com.erg.almagestdota.stats.domain

import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.network.external.firebase.FirebaseRepositoryContract
import com.erg.almagestdota.settings.domain.use_cases.GetFilterGroupsUseCase
import com.erg.almagestdota.settings.domain.use_cases.LoadDocumentMatchesUseCase
import com.erg.almagestdota.settings.domain.use_cases.UpdateHeroRolesUseCase
import com.erg.almagestdota.settings.domain.use_cases.UpdateHeroStagesUseCase
import kotlinx.coroutines.*
import javax.inject.Inject
import kotlin.coroutines.coroutineContext

internal class Interactor @Inject constructor(
    private val firebaseRepository: FirebaseRepositoryContract,
    private val localStorage: ILocalStorageContract,
    private val updateHeroStagesUseCase: UpdateHeroStagesUseCase,
    private val updateHeroRolesUseCase: UpdateHeroRolesUseCase,
    private val getFilterGroupsUseCase: GetFilterGroupsUseCase,
    private val updateFactionsDataUseCase: UpdateFactionsDataUseCase,
    private val loadDocumentMatchesUseCase: LoadDocumentMatchesUseCase,
) {

    suspend fun updateHeroStages(matches: List<DotaMatch>) {
        val factions = updateFactionsDataUseCase.invoke(matches, 0L)
        val roles = updateHeroRolesUseCase.invoke(matches, 0L)
        val stages =  updateHeroStagesUseCase.invoke(matches, 0L)
        for (hero in GlobalVars.mainSettings.heroes) {
            hero.factions = factions[hero.id]!!
            hero.roles = roles[hero.id]!!
            hero.stages = stages[hero.id]!!
        }
        localStorage.setMainSettings(GlobalVars.mainSettings)
       // firebaseRepository.setMainSettings(GlobalVars.mainSettings)
        GlobalVars.groups = getFilterGroupsUseCase.invoke()
    }

    suspend fun getDocumentId(): Int {
        return firebaseRepository.getImportantSettings().lastDocument
    }

    suspend fun loadStoredMatches(documentId: Int) = loadDocumentMatchesUseCase.invoke(documentId)
    suspend fun loadStoredMatches(documentId: Int, count: Int): List<DotaMatch> {
        val scope = CoroutineScope(coroutineContext)
        val deferredList = mutableListOf<Deferred<List<DotaMatch>>>()
        for (i in 0 until count) {
            val deferred = scope.async(Dispatchers.IO) {
                loadDocumentMatchesUseCase.invoke(documentId - i)
            }
            deferredList.add(deferred)
        }
        return deferredList.awaitAll().flatten()
    }
}