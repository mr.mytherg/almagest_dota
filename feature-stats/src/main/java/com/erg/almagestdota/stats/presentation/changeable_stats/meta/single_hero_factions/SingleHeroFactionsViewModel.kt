package com.erg.almagestdota.stats.presentation.changeable_stats.meta.single_hero_factions

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.extensions.round1
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.NotificationStatus
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.complexList
import com.erg.almagestdota.complexadapter.draftStageItem
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import com.erg.almagestdota.stats.R
import com.erg.almagestdota.stats.domain.Interactor
import com.erg.almagestdota.local_storage.external.models.draft.FactionType
import com.erg.almagestdota.stats.domain.UpdateFactionsDataUseCase
import com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.DraftStagesSortType
import com.erg.almagestdota.stats.presentation.changeable_stats.selected_matches.SelectedMatchesScreen
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

internal class SingleHeroFactionsViewModel @AssistedInject constructor(
    @Assisted private val heroId: Int,
    @Assisted private val teamId: Long,
    private val stringProvider: StringProvider,
    private val interactor: Interactor,
    private val localStorage: ILocalStorageContract,
    private val updateFactionsDataUseCase: UpdateFactionsDataUseCase
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data -> }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    fun obtainAction(action: SingleHeroFactionsActions) {
        when (action) {
            is SingleHeroFactionsActions.SortByPickRate -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.PICK_RATE) return
                stateConfigurator.selectedSortType = DraftStagesSortType.PICK_RATE
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is SingleHeroFactionsActions.SortByFaction -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.NAME) return
                stateConfigurator.selectedSortType = DraftStagesSortType.NAME
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is SingleHeroFactionsActions.ShowMatches -> {
                val factionType = FactionType.byNumber(action.key.toInt())
                localStorage.matchesToShow = localStorage.selectedMatches.filter {
                    when (factionType) {
                        FactionType.OVERALL -> it.direHeroes.find { heroId == it.heroId } != null && (teamId == 0L || it.direTeamId == teamId)
                            || it.radiantHeroes.find { heroId == it.heroId } != null && (teamId == 0L || it.radiantTeamId == teamId)
                        FactionType.RADIANT -> it.radiantHeroes.find { heroId == it.heroId } != null && (teamId == 0L || it.radiantTeamId == teamId)
                        FactionType.DIRE -> it.direHeroes.find { heroId == it.heroId } != null && (teamId == 0L || it.direTeamId == teamId)
                    }
                }

                if (localStorage.matchesToShow.isNotEmpty()) {
                    val screen = SelectedMatchesScreen(
                        SingleHeroFactionsFragmentDirections.toSelectedMatchesFragment(
                            heroId = heroId,
                            title = stringProvider.getString(FactionType.getId(factionType))
                        )
                    )
                    emit(viewEventMutable, ViewEvent.Navigation(screen))
                } else {
                    val text = stringProvider.getString(R.string.no_matches)
                    emit(viewEventMutable, ViewEvent.AlmagestSnackbar(text, NotificationStatus.ERROR))
                }
            }
            is SingleHeroFactionsActions.SortByMatchCount -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.MATCHES) return
                stateConfigurator.selectedSortType = DraftStagesSortType.MATCHES
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is SingleHeroFactionsActions.SortByWinRate -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.WIN_RATE) return
                stateConfigurator.selectedSortType = DraftStagesSortType.WIN_RATE
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
        }
    }

    private inner class StateConfigurator {
        var selectedSortType: DraftStagesSortType = DraftStagesSortType.MATCHES

        val factions = updateFactionsDataUseCase.invoke(localStorage.selectedMatches, teamId)[heroId] ?: listOf()

        fun defineFragmentState(): SingleHeroFactionsViewState {
            val factionsSorted = when (selectedSortType) {
                DraftStagesSortType.NAME -> {
                    factions.sortedBy { it.factionType.number }
                }
                DraftStagesSortType.WIN_RATE -> {
                    factions.sortedByDescending { it.winRate }
                }
                DraftStagesSortType.PICK_RATE -> {
                    factions.sortedByDescending { it.pickRate }
                }
                DraftStagesSortType.MATCHES -> {
                    factions.sortedByDescending { it.matchCount }
                }
            }
            val hero = heroId.getHeroById()
            val factionsComplex = complexList {
                for (faction in factionsSorted) {
                    draftStageItem(key = faction.factionType.number.toString()) {
                        heroName = stringProvider.getString(FactionType.getId(faction.factionType))
                        url = hero.imageLink
                        hideImage = true
                        pickRate = faction.pickRate.round1()
                        matchCount = faction.matchCount
                        winRate = faction.winRate.round1()
                        hasBlackout = false
                    }
                }
            }

            return SingleHeroFactionsViewState.DefaultState(
                hero.name,
                factionsComplex,
                selectedSortType
            )
        }
    }

    companion object {
        fun provideFactory(
            heroId: Int,
            teamId: Long,
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create(
                        heroId, teamId
                    ) as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(
            heroId: Int,
            teamId: Long
        ): SingleHeroFactionsViewModel
    }
}