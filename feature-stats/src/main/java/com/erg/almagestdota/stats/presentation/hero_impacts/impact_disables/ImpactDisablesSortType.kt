package com.erg.almagestdota.stats.presentation.hero_impacts.impact_disables

enum class ImpactDisablesSortType {
    NAME,
    DURATION;
}