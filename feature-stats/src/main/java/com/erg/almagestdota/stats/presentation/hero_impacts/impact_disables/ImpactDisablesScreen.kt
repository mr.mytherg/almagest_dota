package com.erg.almagestdota.stats.presentation.hero_impacts.impact_disables

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.stats.presentation.StatsFragmentDirections
import com.erg.almagestdota.stats.presentation.hero_impacts.HeroImpactsFragmentDirections

object ImpactDisablesScreen : Screen<NavDirections>(
    route = HeroImpactsFragmentDirections.toImpactDisablesFragment(),
    requestKey = ImpactDisablesFragment.TAG
)