package com.erg.almagestdota.stats.presentation.hero_impacts.impact_damage

enum class ImpactDamageSortType {
    NAME,
    TOWER,
    HERO;
}