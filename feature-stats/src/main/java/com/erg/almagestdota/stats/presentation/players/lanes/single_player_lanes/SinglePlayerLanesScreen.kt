package com.erg.almagestdota.stats.presentation.players.lanes.single_player_lanes

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.stats.presentation.players.lanes.PlayersLanesFragmentDirections

class SinglePlayerLanesScreen(playerId: Long) : Screen<NavDirections>(
    route = PlayersLanesFragmentDirections.toSinglePlayerLanesFragment(playerId),
    requestKey = SinglePlayerLanesFragment.TAG
)