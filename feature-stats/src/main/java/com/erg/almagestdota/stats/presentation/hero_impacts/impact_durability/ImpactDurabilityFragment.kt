package com.erg.almagestdota.stats.presentation.hero_impacts.impact_durability

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.ComplexAdapter
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdota.stats.R
import com.erg.almagestdota.stats.databinding.ImpactDurabilityFragmentBinding
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import com.google.android.material.textview.MaterialTextView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
internal class ImpactDurabilityFragment : Fragment(R.layout.impact_durability_fragment), BackPressListener {

    companion object {
        const val TAG = "ImpactDurabilityFragment"
        const val MILLISECONDS_PER_INCH = 500f
    }

    private val binding by viewBinding(ImpactDurabilityFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    @Inject
    lateinit var viewModelFactory: ImpactDurabilityViewModel.Factory
    private val viewModel by viewModels<ImpactDurabilityViewModel> {
        ImpactDurabilityViewModel.provideFactory(
            assistedFactory = viewModelFactory
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
        viewModel.viewEvent.setResultListener(this)
    }

    override fun onBackPressed() {
        navController.popBackStack()
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private inner class ViewsConfigurator {
        private val gameStages = ComplexAdapter(
            complexListener = object : ComplexListener {
                override fun onClick(item: ComplexItem) {
                    viewModel.obtainAction(ImpactDurabilityViewActions.SelectGameStage(item.key))
                }
            }
        )
        private val roles = ComplexAdapter(
            complexListener = object : ComplexListener {
                override fun onClick(item: ComplexItem) {
                    viewModel.obtainAction(ImpactDurabilityViewActions.SelectRole(item.key))
                }
            }
        )

        fun initStartState() = with(binding) {
            tvOverall.clicksWithDebounce {
                viewModel.obtainAction(ImpactDurabilityViewActions.SortByOverall)
            }
            ivBack.clicksWithDebounce {
                navController.popBackStack()
            }
            tvName.clicksWithDebounce {
                viewModel.obtainAction(ImpactDurabilityViewActions.SortByHeroName)
            }
        }

        fun renderState(state: ImpactDurabilityViewState) = with(binding) {
            when (state) {
                is ImpactDurabilityViewState.DefaultState -> renderDefaultState(state)
            }
        }

        private fun renderDefaultState(state: ImpactDurabilityViewState.DefaultState) = with(binding) {
            if (rvRoles.adapter == null) {
                rvRoles.adapter = roles
                rvRoles.setHasFixedSize(true)
            }
            roles.submitList(state.roles)
            if (rvGameStages.adapter == null) {
                rvGameStages.adapter = gameStages
                rvGameStages.setHasFixedSize(true)
            }
            gameStages.submitList(state.gameStages)
            rvHeroes.adapter = ComplexAdapter(
                complexListener = object : ComplexListener {
                    override fun onClick(item: ComplexItem) {
                    }
                }
            ).apply {
                submitList(state.heroes)
            }
            setSortUnselected(tvOverall)
            setSortUnselected(tvName)
            when (state.sortType) {
                ImpactDurabilitySortType.OVERALL -> setSortSelected(tvOverall)
                ImpactDurabilitySortType.NAME -> setSortSelected(tvName)

            }
        }

        private fun setSortUnselected(textView: MaterialTextView) {
            textView.setTextColor(ContextCompat.getColor(textView.context, R.color.textColor))
        }

        private fun setSortSelected(textView: MaterialTextView) {
            textView.setTextColor(ContextCompat.getColor(textView.context, R.color.colorPrimary))
        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            viewModel.viewEvent.setResultListener(this@ImpactDurabilityFragment)
            navController.navigateViaScreenRoute(screen)
        }
    }
}