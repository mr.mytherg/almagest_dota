package com.erg.almagestdota.stats.domain.models

import android.os.Parcelable
import androidx.annotation.StringRes
import com.erg.almagestdota.local_storage.R
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class BandType(val number: Int) : Parcelable {
    BAND2(0),
    BAND3(1),
    BAND4(2);

    companion object {
        @StringRes
        fun getId(role: BandType): Int {
            return when (role) {
                BAND2 -> R.string.two
                BAND3 -> R.string.three
                else -> R.string.four
            }
        }

        fun byName(value: String): BandType {
            return BandType.values().find { it.name == value }!!
        }

        fun byNumber(number: Int): BandType {
            return BandType.values().firstOrNull() { it.number == number }!!
        }
    }
}