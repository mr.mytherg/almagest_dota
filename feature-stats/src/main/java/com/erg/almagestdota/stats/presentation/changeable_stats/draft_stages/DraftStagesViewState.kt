package com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem

internal sealed class DraftStagesViewState : ViewState() {
    class DefaultState(
        val draftStages: List<ComplexItem>,
        val heroes: List<ComplexItem>,
        val stageIndex: Int,
        val sortType: DraftStagesSortType
    ) : DraftStagesViewState()
}