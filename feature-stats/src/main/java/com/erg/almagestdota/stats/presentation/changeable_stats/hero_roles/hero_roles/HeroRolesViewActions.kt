package com.erg.almagestdota.stats.presentation.changeable_stats.hero_roles.hero_roles

internal sealed class HeroRolesViewActions {

    class SelectRole(val key: String) : HeroRolesViewActions()
    class SelectHero(val key: String) : HeroRolesViewActions()
    object SortByHeroName : HeroRolesViewActions()
    object SortByMatchCount : HeroRolesViewActions()
    object SortByWinRate : HeroRolesViewActions()
    object SortByPickRate : HeroRolesViewActions()
}