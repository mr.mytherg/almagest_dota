package com.erg.almagestdota.stats.presentation.team_selector

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.erg.almagestdota.base.external.ui.ActivityManager
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.heroselector.R
import com.erg.almagestdota.heroselector.databinding.HeroSelectorSheetBinding
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdpta.core_navigation.external.helpers.BottomSheetFragment
import com.erg.almagestdpta.core_navigation.external.helpers.navHostFragmentManager
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import com.google.android.material.bottomsheet.BottomSheetBehavior
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
internal class TeamSelectionSheet :
    BottomSheetFragment(R.layout.hero_selector_sheet), BackPressListener {

    private val binding by viewBinding { HeroSelectorSheetBinding.bind(requireContentView()) }
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()
    private val isRadiant = TeamSelectionScreen.isRadiant

    @Inject
    lateinit var viewModelFactory: TeamSelectionViewModel.Factory
    private val viewModel by viewModels<TeamSelectionViewModel> {
        TeamSelectionViewModel.provideFactory(
            assistedFactory = viewModelFactory
        )
    }

    override fun onBackPressed() {
        viewModel.obtainAction(TeamSelectionViewActions.OnBackClick)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
        viewModel.viewEvent.setResultListener(this)
    }

    override fun onBottomSheetStateChanged(bottomSheet: View, newState: Int) {
        super.onBottomSheetStateChanged(bottomSheet, newState)
        if (newState == BottomSheetBehavior.STATE_HIDDEN) {
            onClose()
        }
    }
    override fun onResume() {
        super.onResume()
        binding.etSearch.requestFocus()
        (requireActivity() as ActivityManager).showKeyboard(binding.etSearch)
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun onClose(result: Bundle = TeamSelectionScreen.Result.createResult(isRadiant = isRadiant)) {
        val fm = navHostFragmentManager()
        navController.popBackStack()
        fm.setFragmentResult(TeamSelectionScreen.TAG, result)
    }

    private inner class ViewsConfigurator {
        private val teamsToSelectAdapter by lazy {
            com.erg.almagestdota.complexadapter.ComplexAdapter(
                complexListener = object : ComplexListener {
                    override fun onClick(item: ComplexItem) {
                        viewModel.obtainAction(TeamSelectionViewActions.SelectTeam(item.key))
                    }
                },
            )
        }

        private val searchWatcher = object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {
                val query = p0.toString()
                viewModel.obtainAction(TeamSelectionViewActions.FilterByName(query.trim()))
            }
        }

        fun initStartState() = with(binding) {
            if (rvHeroes.adapter == null) {
                rvHeroes.adapter = teamsToSelectAdapter
            }
            etSearch.addTextChangedListener(searchWatcher)
        }

        fun renderState(state: TeamSelectionViewState) {
            when (state) {
                is TeamSelectionViewState.DefaultState -> renderDefaultState(state)
            }
        }

        private fun renderDefaultState(state: TeamSelectionViewState.DefaultState) = with(binding) {
            teamsToSelectAdapter.submitList(state.teams)
        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
                else -> handleCustomEvents(event)
            }
        }

        private fun handleCustomEvents(event: ViewEvent) {
            when (event) {
                is TeamSelectionViewEvents.BackClickEvent -> {
                    onClose()
                }
                is TeamSelectionViewEvents.ResultEvent -> {
                    onClose(TeamSelectionScreen.Result.createResult(event.teamId, isRadiant))
                }
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            viewModel.viewEvent.setResultListener(this@TeamSelectionSheet)
            navController.navigateViaScreenRoute(screen)
        }
    }
}