package com.erg.almagestdota.stats.presentation.tournament_selection

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.extensions.launchOnIO
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.extensions.round1
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.NotificationStatus
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.complexadapter.complexList
import com.erg.almagestdota.complexadapter.draftStageItem
import com.erg.almagestdota.complexadapter.statsTournamentsItem
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.StageType
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import com.erg.almagestdota.local_storage.external.raw_models.DotaMatchRaw
import com.erg.almagestdota.pro_matches.domain.GetLeaguesUseCase
import com.erg.almagestdota.settings.domain.use_cases.SaveMatchesUseCase
import com.erg.almagestdota.stats.R
import com.erg.almagestdota.stats.domain.Interactor
import com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.DraftStagesSortType
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

internal class TournamentSelectionViewModel @AssistedInject constructor(
    private val stringProvider: StringProvider,
    private val getLeaguesUseCase: GetLeaguesUseCase,
    private val interactor: Interactor,
    private val saveMatchesUseCase: SaveMatchesUseCase,
    private val localStorage: ILocalStorageContract,

    ) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data ->

    }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow<TournamentSelectionViewState?>(null)
    val viewState = viewStateMutable.asStateFlow()

    private val loadingStateMutable = MutableStateFlow<Loading>(Loading.Enabled)
    val loadingState = loadingStateMutable.asStateFlow()

    init {
        loadForFirstTime()
    }

    private fun loadForFirstTime() {
        launchOnIO(
            loader = loadingStateMutable,
            action = {
                getLeaguesUseCase.invoke()
            },
            onSuccess = {
                loadMoreMatches(interactor.getDocumentId())
            },
            onError = {
                val text = it.message
                emit(viewEventMutable, ViewEvent.AlmagestSnackbar(text, NotificationStatus.ERROR))
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
        )
    }

    fun obtainAction(action: TournamentSelectionHeroActions) {
        when (action) {
            is TournamentSelectionHeroActions.SelectTournament -> {
                val tour = stateConfigurator.tournaments.first { it.id == action.key.toLong() }
                val index = stateConfigurator.tournaments.indexOf(tour)
                stateConfigurator.tournaments.removeAt(index)
                tour.isSelected = !tour.isSelected
                stateConfigurator.tournaments.add(index, tour)
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is TournamentSelectionHeroActions.Submit -> {
                val matches = stateConfigurator.tournaments.filter { it.isSelected && stateConfigurator.checkShady(it) }.flatMap {
                    it.matches
                }
                saveMatchesUseCase.putToFile(matches.map { DotaMatch.toModel(it) })
                localStorage.metaMatches = matches
                localStorage.selectedMatches = matches
                updateHeroStages(matches)
            }
            is TournamentSelectionHeroActions.Retry -> loadForFirstTime()
            is TournamentSelectionHeroActions.CheckShady -> {
                stateConfigurator.hasShady = !stateConfigurator.hasShady
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is TournamentSelectionHeroActions.LoadMore -> {
                if (stateConfigurator.wasAllMatchesLoaded) {
                    showAllMatchesWasLoadedSnack()
                } else {
                    loadMoreMatches(stateConfigurator.documentId)
                }
            }
        }
    }

    private fun showAllMatchesWasLoadedSnack() {
        val text = stringProvider.getString(R.string.all_loaded)
        emit(viewEventMutable, ViewEvent.AlmagestSnackbar(text, NotificationStatus.SUCCESS))
    }


    private fun loadMoreMatches(documentId: Int) {
        launchOnIO(
            loader = loadingStateMutable,
            action = {
                interactor.loadStoredMatches(documentId, TIMES)
            },
            onSuccess = {
                if (it.isEmpty()) {
                    stateConfigurator.wasAllMatchesLoaded = true
                    showAllMatchesWasLoadedSnack()
                } else {
                    stateConfigurator.documentId = documentId - TIMES
                    stateConfigurator.dotaMatches = it.plus(stateConfigurator.dotaMatches)
                    stateConfigurator.tournaments = stateConfigurator.dotaMatches.groupBy { it.leagueId }.toList().map { pair ->
                        StatsTournamentModel(
                            pair.first,
                            pair.second,
                            stateConfigurator.tournaments.find { it.id == pair.first }?.isSelected ?: false
                        )
                    }.toMutableList()
                    viewStateMutable.value = stateConfigurator.defineFragmentState()
                }
            },
            onError = {
                val text = it.message
                emit(viewEventMutable, ViewEvent.AlmagestSnackbar(text, NotificationStatus.ERROR))
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
        )
    }

    private fun updateHeroStages(matches: List<DotaMatch>) {
        launchOnIO(
            loader = loadingStateMutable,
            action = { interactor.updateHeroStages(matches) },
            onSuccess = {
                viewEventMutable.emit(ViewEvent.PopBackStack.Empty)
            },
            onError = {
                val text = it.message
                emit(viewEventMutable, ViewEvent.AlmagestSnackbar(text, NotificationStatus.ERROR))
            }
        )
    }

    private inner class StateConfigurator {
        var tournaments = mutableListOf<StatsTournamentModel>()
        var dotaMatches = listOf<DotaMatch>()
        var wasAllMatchesLoaded = false
        var documentId = 0
        var hasShady = false

        private var firstTimeSelection = true

        fun defineFragmentState(): TournamentSelectionViewState {
            if (firstTimeSelection) {
                val previousSelection = localStorage.metaMatches
                val leagues = mutableMapOf<Long, Boolean>()
                for (match in previousSelection) {
                    leagues[match.leagueId] = true
                }
                tournaments.map { it.isSelected = leagues[it.id] != null }
                firstTimeSelection = false
            }

            val tournamentsSorted = tournaments.sortedWith(
                compareByDescending<StatsTournamentModel> { it.isSelected }
                    .thenByDescending { it.matches.last().startTime }
            ).filter {
                checkShady(it)
            }
            val tournamentsComplex = complexList {
                for (tour in tournamentsSorted) {
                    statsTournamentsItem(key = tour.id.toString()) {
                        name = GlobalVars.leagues.find { it.id == tour.id }?.name.orDefault()
                        matchCount = tour.matches.size
                        isSelected = tour.isSelected
                    }
                }
            }

            return if (tournaments.isEmpty()) {
                TournamentSelectionViewState.Retry
            } else
                TournamentSelectionViewState.DefaultState(tournamentsComplex)
        }

        fun checkShady(tour: StatsTournamentModel): Boolean {
            return if (hasShady) {
                true
            } else {
                val countNormal = tour.matches.count {
                    it.radiantHeroes.plus(it.direHeroes).count {
                        GlobalVars.players[it.playerId]?.name.orDefault().isNotEmpty()
                    } >= 6
                }
                countNormal >= tour.matches.size * 0.7
            }
        }
    }

    companion object {
        private const val TIMES = 10
        fun provideFactory(
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create() as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(): TournamentSelectionViewModel
    }
}