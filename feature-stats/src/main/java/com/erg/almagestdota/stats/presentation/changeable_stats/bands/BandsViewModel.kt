package com.erg.almagestdota.stats.presentation.changeable_stats.bands

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.extensions.round1
import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.*
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerInfo
import com.erg.almagestdota.stats.domain.Interactor
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import com.erg.almagestdota.stats.domain.models.BandType
import com.erg.almagestdota.stats.domain.models.SimpleStats
import com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.DraftStagesSortType
import com.erg.almagestdota.stats.presentation.changeable_stats.selected_matches.SelectedMatchesScreen
import com.erg.almagestdota.stats.presentation.tournament_selection.TournamentSelectionFragment
import com.erg.almagestdota.stats.presentation.tournament_selection.TournamentSelectionScreen
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

internal class BandsViewModel @AssistedInject constructor(
    @Assisted private val teamId: Long,
    private val stringProvider: StringProvider,
    private val interactor: Interactor,
    private val localStorage: ILocalStorageContract,
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data ->
        when (resultKey) {
            TournamentSelectionFragment.TAG -> {
                if (TournamentSelectionScreen.getResult(data) == TournamentSelectionScreen.Result.SUBMITTED) {
                    viewStateMutable.value = stateConfigurator.defineFragmentState()
                }
            }
        }
    }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    fun obtainAction(action: BandsViewActions) {
        when (action) {
            is BandsViewActions.SortByPickRate -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.PICK_RATE) return
                stateConfigurator.selectedSortType = DraftStagesSortType.PICK_RATE
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is BandsViewActions.SelectBandType -> {
                val band = BandType.valueOf(action.key)
                if (stateConfigurator.selectedBand == band) return
                stateConfigurator.selectedBand = band
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is BandsViewActions.ShowBands -> {
                val mutableList = mutableListOf<Int>()
                var str = action.key
                while (str.isNotEmpty()) {
                    val index = str.indexOf(",")
                    mutableList.add(str.substring(0, index).toInt())
                    str = str.substring(index + 1)
                }
                localStorage.matchesToShow = localStorage.selectedMatches.filter {
                    isPickContainsAll(it.radiantHeroes, mutableList) && (it.radiantTeamId == teamId || teamId == 0L) ||
                        isPickContainsAll(it.direHeroes, mutableList) && (it.direTeamId == teamId || teamId == 0L)
                }
                val screen = SelectedMatchesScreen(
                    BandsFragmentDirections.toSelectedMatchesFragment(
                        heroId = mutableList.getOrNull(0).orDefault(),
                        heroId2 = mutableList.getOrNull(1).orDefault(),
                        heroId3 = mutableList.getOrNull(2).orDefault(),
                        heroId4 = mutableList.getOrNull(3).orDefault(),
                        title = stringProvider.getString(R.string.recent_matches_title)
                    )
                )
                emit(viewEventMutable, ViewEvent.Navigation(screen))
            }
            is BandsViewActions.SortByMatches -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.MATCHES) return
                stateConfigurator.selectedSortType = DraftStagesSortType.MATCHES
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is BandsViewActions.SortByHeroName -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.NAME) return
                stateConfigurator.selectedSortType = DraftStagesSortType.NAME
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is BandsViewActions.SortByWinRate -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.WIN_RATE) return
                stateConfigurator.selectedSortType = DraftStagesSortType.WIN_RATE
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
        }
    }

    private fun isPickContainsAll(players: List<PlayerInfo>, heroes: MutableList<Int>): Boolean {
        for (hero in heroes) {
            if (players.find { it.heroId == hero } == null)
                return false
        }
        return true
    }

    private inner class StateConfigurator {
        var selectedSortType: DraftStagesSortType = DraftStagesSortType.MATCHES
        var selectedBand: BandType = BandType.BAND2
        private val bandTypes = listOf(
            BandType.BAND2,
            BandType.BAND3,
            BandType.BAND4,
        )
        private val bands2 = getPopularBands(localStorage.selectedMatches, BandType.BAND2, teamId)
        private val bands3 = getPopularBands(localStorage.selectedMatches, BandType.BAND3, teamId)
        private val bands4 = getPopularBands(localStorage.selectedMatches, BandType.BAND4, teamId)

        fun defineFragmentState(): BandsViewState {
            val bandsComplex = complexList {
                for (index in bandTypes.indices) {
                    val faction = bandTypes[index]
                    filterItem(key = faction.name) {
                        title = stringProvider.getString(BandType.getId(faction))
                        isSelected = faction == selectedBand
                    }
                }
            }


            return BandsViewState.DefaultState(
                bandsComplex,
                getBands(selectedBand),
                selectedSortType
            )
        }

        private fun getBands(selectedBand: BandType): List<ComplexItem> {
            val bands = when (selectedBand) {
                BandType.BAND2 -> bands2
                BandType.BAND3 -> bands3
                BandType.BAND4 -> bands4
            }
            val bandsToShow = when (selectedSortType) {
                DraftStagesSortType.NAME -> {
                    bands.sortedBy {
                        it.first.first().getHeroById().name
                    }
                }
                DraftStagesSortType.WIN_RATE -> {
                    bands.sortedWith(
                        compareByDescending<Pair<Set<Int>, SimpleStats>> { it.second.winRate }
                            .thenByDescending { it.second.matchCount }
                    )
                }
                DraftStagesSortType.PICK_RATE -> {
                    bands.sortedWith(
                        compareByDescending<Pair<Set<Int>, SimpleStats>> { it.second.pickRate }
                            .thenByDescending { it.second.winRate }
                    )
                }
                DraftStagesSortType.MATCHES -> {
                    bands.sortedWith(
                        compareByDescending<Pair<Set<Int>, SimpleStats>> { it.second.matchCount }
                            .thenByDescending { it.second.winRate }
                    )
                }
            }
            return complexList {
                for (band in bandsToShow) {
                    var key = ""
                    band.first.forEach {
                        key += "$it,"
                    }
                    val list = band.first.toList()
                    when (selectedBand) {
                        BandType.BAND2 -> {
                            val hero1: HeroCoreData = list[0].getHeroById()
                            val hero2: HeroCoreData = list[1].getHeroById()
                            band2Item(key = key) {
                                heroName1 = hero1.name
                                url1 = hero1.imageLink
                                heroName2 = hero2.name
                                url2 = hero2.imageLink
                                matchCount = band.second.matchCount
                                winRate = band.second.winRate.round1()
                                pickRate = band.second.pickRate.round1()
                                hasBlackout = false
                            }
                        }
                        BandType.BAND3 -> {
                            val hero1: HeroCoreData = list[0].getHeroById()
                            val hero2: HeroCoreData = list[1].getHeroById()
                            val hero3: HeroCoreData = list[2].getHeroById()
                            band3Item(key = key) {
                                heroName1 = hero1.name
                                url1 = hero1.imageLink
                                heroName2 = hero2.name
                                url2 = hero2.imageLink
                                heroName3 = hero3.name
                                url3 = hero3.imageLink
                                matchCount = band.second.matchCount
                                winRate = band.second.winRate.round1()
                                pickRate = band.second.pickRate.round1()
                                hasBlackout = false
                            }
                        }
                        BandType.BAND4 -> {
                            val hero1: HeroCoreData = list[0].getHeroById()
                            val hero2: HeroCoreData = list[1].getHeroById()
                            val hero3: HeroCoreData = list[2].getHeroById()
                            val hero4: HeroCoreData = list[3].getHeroById()
                            band4Item(key = key) {
                                heroName1 = hero1.name
                                url1 = hero1.imageLink
                                heroName2 = hero2.name
                                url2 = hero2.imageLink
                                heroName3 = hero3.name
                                url3 = hero3.imageLink
                                heroName4 = hero4.name
                                url4 = hero4.imageLink
                                matchCount = band.second.matchCount
                                winRate = band.second.winRate.round1()
                                pickRate = band.second.pickRate.round1()
                                hasBlackout = false
                            }
                        }
                    }

                }
            }
        }

        private fun getPopularBands(metaMatches: List<DotaMatch>, band: BandType, teamId: Long): List<Pair<Set<Int>, SimpleStats>> {
            val mutableMap = mutableMapOf<Set<Int>, SimpleStats>()
            for (match in metaMatches) {
                if (teamId == 0L || match.radiantTeamId == teamId)
                    for (index1 in match.radiantHeroes.indices) {
                        val heroId1 = match.radiantHeroes[index1].heroId
                        for (index2 in (index1 + 1) until match.radiantHeroes.size) {
                            val heroId2 = match.radiantHeroes[index2].heroId
                            if (band == BandType.BAND2)
                                addSimpleStats(mutableMap, setOf(heroId1, heroId2), match, true)
                            else {
                                for (index3 in (index2 + 1) until match.radiantHeroes.size) {
                                    val heroId3 = match.radiantHeroes[index3].heroId
                                    if (band == BandType.BAND3)
                                        addSimpleStats(mutableMap, setOf(heroId1, heroId2, heroId3), match, true)
                                    else
                                        for (index4 in (index3 + 1) until match.radiantHeroes.size) {
                                            val heroId4 = match.radiantHeroes[index4].heroId
                                            addSimpleStats(mutableMap, setOf(heroId1, heroId2, heroId3, heroId4), match, true)
                                        }
                                }
                            }
                        }
                    }
                if (teamId == 0L || match.direTeamId == teamId)
                    for (index1 in match.direHeroes.indices) {
                        val heroId1 = match.direHeroes[index1].heroId
                        for (index2 in (index1 + 1) until match.direHeroes.size) {
                            val heroId2 = match.direHeroes[index2].heroId
                            if (band == BandType.BAND2)
                                addSimpleStats(mutableMap, setOf(heroId1, heroId2), match, false)
                            else {
                                for (index3 in (index2 + 1) until match.direHeroes.size) {
                                    val heroId3 = match.direHeroes[index3].heroId
                                    if (band == BandType.BAND3)
                                        addSimpleStats(mutableMap, setOf(heroId1, heroId2, heroId3), match, false)
                                    else
                                        for (index4 in (index3 + 1) until match.direHeroes.size) {
                                            val heroId4 = match.direHeroes[index4].heroId
                                            addSimpleStats(mutableMap, setOf(heroId1, heroId2, heroId3, heroId4), match, false)
                                        }
                                }
                            }
                        }
                    }
            }
            return mutableMap.toList().filter {
                it.second.matchCount = it.second.loseCount + it.second.winCount
                it.second.winRate = getPercentage(it.second.winCount, it.second.matchCount)
                it.second.pickRate = getPercentage(it.second.matchCount, metaMatches.size)
                if (teamId == 0L)
                    when (band) {
                        BandType.BAND2 -> it.second.pickRate >= 1.5 && it.second.matchCount > 3
                        BandType.BAND3 -> it.second.pickRate >= 0.5 && it.second.matchCount > 2
                        BandType.BAND4 -> it.second.pickRate >= 0.15 && it.second.matchCount > 1
                    }
                else
                    it.second.matchCount > 1
            }
        }

        private fun addSimpleStats(mutableMap: MutableMap<Set<Int>, SimpleStats>, key: Set<Int>, match: DotaMatch, isRadiant: Boolean) {
            val simpleStats = mutableMap[key] ?: SimpleStats()
            mutableMap[key] = simpleStats.apply {
                if (match.hasRadiantWon && isRadiant || !match.hasRadiantWon && !isRadiant) {
                    winCount++
                } else {
                    loseCount++
                }
                matches.add(match.matchId)
            }
        }

        private fun getPercentage(param1: Int, param2: Int) =
            if (param1 == 0 || param2 == 0) 0.0 else (param1 * 1.0 / (param2) * 100).round2()
    }

    companion object {
        fun provideFactory(
            teamId: Long,
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create(
                        teamId
                    ) as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(
            teamId: Long
        ): BandsViewModel
    }
}