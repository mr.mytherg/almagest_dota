package com.erg.almagestdota.stats.presentation.changeable_stats

internal sealed class ChangeableStatsViewActions {

    object OpenDraftStages : ChangeableStatsViewActions()
    object OpenMeta : ChangeableStatsViewActions()
    object OpenHeroesRoles : ChangeableStatsViewActions()
    object OpenBands : ChangeableStatsViewActions()
    object OpenHeroesRolesEnemies : ChangeableStatsViewActions()
}