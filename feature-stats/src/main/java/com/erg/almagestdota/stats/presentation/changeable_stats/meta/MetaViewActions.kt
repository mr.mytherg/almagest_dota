package com.erg.almagestdota.stats.presentation.changeable_stats.meta

internal sealed class MetaViewActions {

    class SelectFactionType(val key: String) : MetaViewActions()
    object SortByHeroName : MetaViewActions()
    object SortByWinRate : MetaViewActions()
    object SortByMatches : MetaViewActions()
    object SortByPickRate : MetaViewActions()
    class ShowFactions(val key: String) : MetaViewActions()
}