package com.erg.almagestdota.stats.presentation.changeable_stats.open_match

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.stats.presentation.changeable_stats.selected_matches.SelectedMatchesFragmentDirections

class OpenMatchScreen(
     route: NavDirections
) : Screen<NavDirections>(
    route = route,
    requestKey = OpenMatchFragment.TAG
)