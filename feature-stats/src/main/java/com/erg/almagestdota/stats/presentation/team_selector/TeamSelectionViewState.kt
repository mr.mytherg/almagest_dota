package com.erg.almagestdota.stats.presentation.team_selector

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem

internal sealed class TeamSelectionViewState : ViewState() {

    class DefaultState(
        val teams: List<ComplexItem>,
    ) : TeamSelectionViewState()
}