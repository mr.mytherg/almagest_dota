package com.erg.almagestdota.stats.presentation.players.selected_player_matches

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem

internal sealed class SelectedPlayerMatchesViewState : ViewState() {

    class MatchesState(
        val list: List<ComplexItem>,
        val playerName:String,
    ) : SelectedPlayerMatchesViewState()
}