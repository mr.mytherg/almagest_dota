package com.erg.almagestdota.stats.presentation.changeable_stats.hero_roles.single_hero_roles

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.extensions.round1
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.NotificationStatus
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.complexList
import com.erg.almagestdota.complexadapter.draftStageItem
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.RoleType
import com.erg.almagestdota.local_storage.external.models.draft.isHeroFitsForRole
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import com.erg.almagestdota.settings.domain.use_cases.UpdateHeroRolesUseCase
import com.erg.almagestdota.stats.R
import com.erg.almagestdota.stats.domain.Interactor
import com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.DraftStagesSortType
import com.erg.almagestdota.stats.presentation.changeable_stats.selected_matches.SelectedMatchesScreen
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

internal class SingleHeroRolesViewModel @AssistedInject constructor(
    @Assisted private val heroId: Int,
    @Assisted private val teamId: Long,
    private val stringProvider: StringProvider,
    private val interactor: Interactor,
    private val localStorage: ILocalStorageContract,
    private val updateHeroRolesUseCase: UpdateHeroRolesUseCase,

    ) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data -> }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    fun obtainAction(action: SingleHeroRolesActions) {
        when (action) {
            is SingleHeroRolesActions.SortByPickRate -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.PICK_RATE) return
                stateConfigurator.selectedSortType = DraftStagesSortType.PICK_RATE
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is SingleHeroRolesActions.SortByRole -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.NAME) return
                stateConfigurator.selectedSortType = DraftStagesSortType.NAME
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is SingleHeroRolesActions.ShowMatches -> {
                val roleType = RoleType.byNumber(action.key.toInt())
                localStorage.matchesToShow = localStorage.selectedMatches.filter {
                    isPlayerRelevant(it.radiantHeroes, it.radiantTeamId, roleType) ||
                        isPlayerRelevant(it.direHeroes, it.direTeamId, roleType)

                }

                if (localStorage.matchesToShow.isNotEmpty()) {
                    val screen = SelectedMatchesScreen(
                        SingleHeroRolesFragmentDirections.toSelectedMatchesFragment(
                            heroId = heroId,
                            title = stringProvider.getString(RoleType.getId(roleType))
                        )
                    )
                    emit(viewEventMutable, ViewEvent.Navigation(screen))
                } else {
                    val text = stringProvider.getString(R.string.no_matches)
                    emit(viewEventMutable, ViewEvent.AlmagestSnackbar(text, NotificationStatus.ERROR))
                }
            }
            is SingleHeroRolesActions.SortByMatchCount -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.MATCHES) return
                stateConfigurator.selectedSortType = DraftStagesSortType.MATCHES
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is SingleHeroRolesActions.SortByWinRate -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.WIN_RATE) return
                stateConfigurator.selectedSortType = DraftStagesSortType.WIN_RATE
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
        }
    }

    private fun isPlayerRelevant(heroes: List<PlayerInfo>, teamId: Long, roleType: RoleType): Boolean {
        val playerId = heroes.find { heroId == it.heroId }?.playerId ?: 0L
        return if (playerId != 0L) {
            GlobalVars.players[playerId]?.role == roleType && roleType != RoleType.UNKNOWN && isTeamRelevant(teamId)
        } else {
            false
        }
    }

    private fun isTeamRelevant(teamId: Long): Boolean {
        return this.teamId == 0L || teamId == this.teamId
    }

    private inner class StateConfigurator {
        var selectedSortType: DraftStagesSortType = DraftStagesSortType.MATCHES

        val heroRoles = updateHeroRolesUseCase.invoke(localStorage.selectedMatches, teamId)[heroId] ?: listOf()


        fun defineFragmentState(): SingleHeroRolesViewState {
            val rolesSorted = when (selectedSortType) {
                DraftStagesSortType.NAME -> {
                    heroRoles.sortedBy { it.roleType.number }
                }
                DraftStagesSortType.WIN_RATE -> {
                    heroRoles.sortedByDescending { it.winRatePercentage }
                }
                DraftStagesSortType.PICK_RATE -> {
                    heroRoles.sortedByDescending { it.pickRate }
                }
                DraftStagesSortType.MATCHES -> {
                    heroRoles.sortedByDescending { it.matchCount }
                }
            }
            val hero = heroId.getHeroById()
            val stagesComplex = complexList {
                for (heroRole in rolesSorted) {
                    draftStageItem(key = heroRole.roleType.number.toString()) {
                        heroName = stringProvider.getString(RoleType.getId(heroRole.roleType))
                        url = hero.imageLink
                        hideImage = true
                        pickRate = heroRole.pickRate.round1()
                        matchCount = heroRole.matchCount
                        winRate = heroRole.winRatePercentage.round1()
                        hasBlackout = !rolesSorted.isHeroFitsForRole(heroRole.roleType)
                    }
                }
            }

            return SingleHeroRolesViewState.DefaultState(
                hero.name,
                stagesComplex,
                selectedSortType
            )
        }
    }

    companion object {
        fun provideFactory(
            heroId: Int,
            teamId: Long,
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create(
                        heroId, teamId
                    ) as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(
            heroId: Int,
            teamId: Long,
        ): SingleHeroRolesViewModel
    }
}