package com.erg.almagestdota.stats.presentation.changeable_stats.meta.single_hero_factions

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.ComplexAdapter
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.stats.R
import com.erg.almagestdota.stats.databinding.SingleHeroRolesFragmentBinding
import com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.DraftStagesSortType
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import com.google.android.material.textview.MaterialTextView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
internal class SingleHeroFactionsFragment : Fragment(R.layout.single_hero_factions_fragment), BackPressListener {

    private val args by navArgs<SingleHeroFactionsFragmentArgs>()

    companion object {
        const val TAG = "SingleHeroFactionsFragment"
    }

    private val binding by viewBinding(SingleHeroRolesFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    @Inject
    lateinit var viewModelFactory: SingleHeroFactionsViewModel.Factory
    private val viewModel by viewModels<SingleHeroFactionsViewModel> {
        SingleHeroFactionsViewModel.provideFactory(
            heroId = args.heroId,
            teamId= args.teamId,
            assistedFactory = viewModelFactory
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
    }

    override fun onBackPressed() {
        navController.popBackStack()
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private inner class ViewsConfigurator {
        fun initStartState() = with(binding) {
            tvWinRate.clicksWithDebounce {
                viewModel.obtainAction(SingleHeroFactionsActions.SortByWinRate)
            }
            ivBack.clicksWithDebounce {
                navController.popBackStack()
            }
            tvName.clicksWithDebounce {
                viewModel.obtainAction(SingleHeroFactionsActions.SortByFaction)
            }
            tvMatchCount.clicksWithDebounce {
                viewModel.obtainAction(SingleHeroFactionsActions.SortByMatchCount)
            }
            tvPickRate.clicksWithDebounce {
                viewModel.obtainAction(SingleHeroFactionsActions.SortByPickRate)
            }
        }

        fun renderState(state: SingleHeroFactionsViewState) = with(binding) {
            when (state) {
                is SingleHeroFactionsViewState.DefaultState -> renderDefaultState(state)
            }
        }

        private fun renderDefaultState(state: SingleHeroFactionsViewState.DefaultState) = with(binding) {
            tvName.text = getString(R.string.filter_group_name_faction)
            if (args.teamId != 0L)
                tvSubtitle.text = GlobalVars.teams[args.teamId]?.name.orDefault().ifEmpty { args.teamId.toString() }
            tvTitle.text = state.heroName
            rvHeroes.adapter = ComplexAdapter(object : ComplexListener {
                override fun onClick(item: ComplexItem) {
                    viewModel.obtainAction(SingleHeroFactionsActions.ShowMatches(item.key))
                }
            }).apply {
                submitList(state.heroes)
            }
            setSortUnselected(tvWinRate)
            setSortUnselected(tvPickRate)
            setSortUnselected(tvName)
            setSortUnselected(tvMatchCount)
            when (state.sortType) {
                DraftStagesSortType.PICK_RATE -> setSortSelected(tvPickRate)
                DraftStagesSortType.WIN_RATE -> setSortSelected(tvWinRate)
                DraftStagesSortType.NAME -> setSortSelected(tvName)
                DraftStagesSortType.MATCHES -> setSortSelected(tvMatchCount)
            }
        }

        private fun setSortUnselected(textView: MaterialTextView) {
            textView.setTextColor(ContextCompat.getColor(textView.context, R.color.textColor))
        }

        private fun setSortSelected(textView: MaterialTextView) {
            textView.setTextColor(ContextCompat.getColor(textView.context, R.color.colorPrimary))
        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            navController.navigateViaScreenRoute(screen)
        }
    }
}