package com.erg.almagestdota.stats.presentation.players.lanes

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.stats.presentation.StatsFragmentDirections
import com.erg.almagestdota.stats.presentation.changeable_stats.ChangeableStatsFragmentDirections

class PlayersLanesScreen() : Screen<NavDirections>(
    route = StatsFragmentDirections.toPlayersLanesFragment(),
    requestKey = PlayersLanesFragment.TAG
)