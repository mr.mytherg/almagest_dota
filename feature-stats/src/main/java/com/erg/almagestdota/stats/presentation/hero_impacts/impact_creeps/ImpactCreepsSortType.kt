package com.erg.almagestdota.stats.presentation.hero_impacts.impact_creeps

enum class ImpactCreepsSortType {
    NAME,
    LAST_HITS,
    DENIES,
    NEUTRALS;
}