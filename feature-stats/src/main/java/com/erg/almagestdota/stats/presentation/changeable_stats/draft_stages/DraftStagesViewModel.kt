package com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.extensions.round1
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.complexList
import com.erg.almagestdota.complexadapter.draftStageItem
import com.erg.almagestdota.complexadapter.filterItem
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.HeroStage
import com.erg.almagestdota.local_storage.external.models.draft.StageType
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import com.erg.almagestdota.settings.domain.use_cases.UpdateHeroStagesUseCase
import com.erg.almagestdota.stats.domain.Interactor
import com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.draft_stage_hero.SingleHeroDraftStagesScreen
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

internal class DraftStagesViewModel @AssistedInject constructor(
    @Assisted private val teamId: Long,
    private val stringProvider: StringProvider,
    private val interactor: Interactor,
    private val localStorage: ILocalStorageContract,
    private val updateHeroStagesUseCase: UpdateHeroStagesUseCase
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data ->

    }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    fun obtainAction(action: DraftStagesViewActions) {
        when (action) {
            is DraftStagesViewActions.SelectDraftStage -> {
                if (stateConfigurator.selectedDraftStage == StageType.byName(action.key)) return
                stateConfigurator.selectedDraftStage = StageType.byName(action.key)
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is DraftStagesViewActions.SelectHero -> {
                val heroId = action.key.toInt()
                emit(viewEventMutable, ViewEvent.Navigation(SingleHeroDraftStagesScreen(heroId,teamId)))
            }

            is DraftStagesViewActions.SortByPickRate -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.PICK_RATE) return
                stateConfigurator.selectedSortType = DraftStagesSortType.PICK_RATE
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is DraftStagesViewActions.SortByMatches -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.MATCHES) return
                stateConfigurator.selectedSortType = DraftStagesSortType.MATCHES
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is DraftStagesViewActions.SortByHeroName -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.NAME) return
                stateConfigurator.selectedSortType = DraftStagesSortType.NAME
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is DraftStagesViewActions.SortByWinRate -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.WIN_RATE) return
                stateConfigurator.selectedSortType = DraftStagesSortType.WIN_RATE
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
        }
    }

    private inner class StateConfigurator {
        var selectedDraftStage: StageType = StageType.FIRST_PICK
        var selectedSortType: DraftStagesSortType = DraftStagesSortType.MATCHES
        var selectedStageIndex: Int = 0
        private val draftStageTypes = listOf(
            StageType.FIRST_PICK,
            StageType.DOUBLE_PICK,
            StageType.FIRST_STAGE_ENDING,
            StageType.SECOND_STAGE_OPENING,
            StageType.SECOND_STAGE_DOUBLE_PICK,
            StageType.SECOND_STAGE_ENDING,
            StageType.THIRD_STAGE_OPENING,
            StageType.LAST_PICK
        )
        val draftStages = updateHeroStagesUseCase.invoke(localStorage.selectedMatches, teamId).toList().map {
            it.first.getHeroById() to it.second
        }

        fun defineFragmentState(): DraftStagesViewState {
            val draftStagesComplex = complexList {
                for (index in draftStageTypes.indices) {
                    val stage = draftStageTypes[index]
                    filterItem(key = stage.name) {
                        title = stringProvider.getString(StageType.getId(stage))
                        isSelected = stage == selectedDraftStage
                        if (isSelected)
                            selectedStageIndex = index
                    }
                }
            }
            val heroesToShow = when (selectedSortType) {
                DraftStagesSortType.NAME -> {
                    draftStages.sortedBy {
                        it.first.name
                    }
                }
                DraftStagesSortType.MATCHES -> {
                    draftStages.sortedWith(
                        compareByDescending<Pair<HeroCoreData, List<HeroStage>>> {
                            val stage = it.second.first { it.stage == selectedDraftStage }
                            stage.matchCount
                        }.thenByDescending {
                            val stage = it.second.first { it.stage == selectedDraftStage }
                            stage.winRate
                        }
                    )
                }
                DraftStagesSortType.WIN_RATE -> {
                    draftStages.sortedWith(
                        compareByDescending<Pair<HeroCoreData, List<HeroStage>>> {
                            val stage = it.second.first { it.stage == selectedDraftStage }
                            stage.winRate
                        }.thenByDescending {
                            val stage = it.second.first { it.stage == selectedDraftStage }
                            stage.matchCount
                        }
                    )
                }
                DraftStagesSortType.PICK_RATE -> {
                    draftStages.sortedWith(
                        compareByDescending<Pair<HeroCoreData, List<HeroStage>>> {
                            val stage = it.second.first { it.stage == selectedDraftStage }
                            stage.pickRate
                        }.thenByDescending {
                            val stage = it.second.first { it.stage == selectedDraftStage }
                            stage.winRate
                        }
                    )
                }
            }
            val heroes = complexList {
                for (hero in heroesToShow) {
                    val stage = hero.second.first { it.stage.number == selectedDraftStage.number }
                    draftStageItem(key = hero.first.id.toString()) {
                        heroName = hero.first.name
                        url = hero.first.imageLink
                        pickRate = stage.pickRate.round1()
                        winRate = stage.winRate.round1()
                        matchCount = stage.matchCount
                        hasBlackout = stage.pickRate.round1() < 10
                    }
                }
            }

            return DraftStagesViewState.DefaultState(
                draftStagesComplex,
                heroes,
                selectedStageIndex,
                selectedSortType
            )
        }
    }

    companion object {
        fun provideFactory(
            teamId: Long,
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create(
                        teamId
                    ) as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(
            teamId: Long,
        ): DraftStagesViewModel
    }
}