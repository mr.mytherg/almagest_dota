package com.erg.almagestdota.stats.presentation.coefs

internal sealed class CoefsActions {

    class SelectCoefType(val key: String) : CoefsActions()
    object SortByHeroName : CoefsActions()
    object SortByValue : CoefsActions()
}