package com.erg.almagestdota.stats.presentation.hero_impacts.impact_damage

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.stats.presentation.hero_impacts.HeroImpactsFragmentDirections

object ImpactDamageScreen : Screen<NavDirections>(
    route = HeroImpactsFragmentDirections.toImpactDamageFragment(),
    requestKey = ImpactDamageFragment.TAG
)