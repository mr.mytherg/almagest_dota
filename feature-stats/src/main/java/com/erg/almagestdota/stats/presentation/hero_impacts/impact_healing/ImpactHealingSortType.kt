package com.erg.almagestdota.stats.presentation.hero_impacts.impact_healing

enum class ImpactHealingSortType {
    NAME,
    OVERALL;
}