package com.erg.almagestdota.stats.presentation.players.lanes

import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.ComplexAdapter
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdota.stats.R
import com.erg.almagestdota.stats.databinding.PlayersLanesFragmentBinding
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import com.google.android.material.textview.MaterialTextView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
internal class PlayersLanesFragment : Fragment(R.layout.players_lanes_fragment), BackPressListener {

    companion object {
        const val TAG = "PlayersLanesFragment"
        const val MILLISECONDS_PER_INCH = 500f
    }

    private val binding by viewBinding(PlayersLanesFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    @Inject
    lateinit var viewModelFactory: PlayersLanesViewModel.Factory
    private val viewModel by viewModels<PlayersLanesViewModel> {
        PlayersLanesViewModel.provideFactory(
            assistedFactory = viewModelFactory
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
        viewModel.viewEvent.setResultListener(this)
    }

    override fun onBackPressed() {
        navController.popBackStack()
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private inner class ViewsConfigurator {
        private val roles = ComplexAdapter(
            complexListener = object : ComplexListener {
                override fun onClick(item: ComplexItem) {
                    viewModel.obtainAction(PlayersLanesViewActions.SelectRole(item.key))
                }
            }
        )

        private val smoothScrollerStages: RecyclerView.SmoothScroller by lazy { createSmoothScrollListener() }
        private fun createSmoothScrollListener(): RecyclerView.SmoothScroller {
            return object : LinearSmoothScroller(requireContext()) {
                override fun getHorizontalSnapPreference(): Int = SNAP_TO_START
                override fun calculateSpeedPerPixel(displayMetrics: DisplayMetrics): Float =
                    MILLISECONDS_PER_INCH / displayMetrics.densityDpi
            }
        }

        fun initStartState() = with(binding) {
            ivBack.clicksWithDebounce {
                navController.popBackStack()
            }
            tvName.clicksWithDebounce {
                viewModel.obtainAction(PlayersLanesViewActions.SortByPlayerName)
            }
            tvMatchCount.clicksWithDebounce {
                viewModel.obtainAction(PlayersLanesViewActions.SortByMatchCount)
            }
            tvOverall.clicksWithDebounce {
                viewModel.obtainAction(PlayersLanesViewActions.SortByOverall)
            }

        }

        fun renderState(state: PlayersLanesViewState) = with(binding) {
            when (state) {
                is PlayersLanesViewState.DefaultState -> renderDefaultState(state)
            }
        }

        private fun renderDefaultState(state: PlayersLanesViewState.DefaultState) = with(binding) {
            if (rvRoles.adapter == null) {
                rvRoles.adapter = roles
            }
            roles.submitList(state.roles)
            rvPlayers.adapter = ComplexAdapter(
                complexListener = object : ComplexListener {
                    override fun onClick(item: ComplexItem) {
                        viewModel.obtainAction(PlayersLanesViewActions.OpenPlayer(item.key))
                    }
                }
            ).apply {
                submitList(state.players)
            }
            setSortUnselected(tvName)
            setSortUnselected(tvMatchCount)
            setSortUnselected(tvOverall)
            when (state.sortType) {
                PlayersLanesSortType.NAME -> setSortSelected(tvName)
                PlayersLanesSortType.MATCH_COUNT -> setSortSelected(tvMatchCount)
                PlayersLanesSortType.OVERALL -> setSortSelected(tvOverall)
            }
        }

        private fun setSortUnselected(textView: MaterialTextView) {
            textView.setTextColor(ContextCompat.getColor(textView.context, R.color.textColor))
        }

        private fun setSortSelected(textView: MaterialTextView) {
            textView.setTextColor(ContextCompat.getColor(textView.context, R.color.colorPrimary))
        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            viewModel.viewEvent.setResultListener(this@PlayersLanesFragment)
            navController.navigateViaScreenRoute(screen)
        }
    }
}