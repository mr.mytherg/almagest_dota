package com.erg.almagestdota.stats.domain.models

class SimpleStats(
    var winCount: Int = 0,
    var loseCount: Int = 0,
    var winRate: Double = 0.0,
    var pickRate: Double = 0.0,
    var matchCount: Int = 0,
    var matches: MutableList<Long> = mutableListOf(),
)