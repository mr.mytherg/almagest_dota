package com.erg.almagestdota.stats.presentation.hero_impacts.impact_durability

enum class ImpactDurabilitySortType {
    NAME,
    OVERALL;
}