package com.erg.almagestdota.stats.presentation.hero_impacts.impact_disables

internal sealed class ImpactDisablesViewActions {

    class SelectRole(val key: String) : ImpactDisablesViewActions()
    class SelectGameStage(val key: String) : ImpactDisablesViewActions()
    object SortByHeroName : ImpactDisablesViewActions()
    object SortByValue1 : ImpactDisablesViewActions()
}