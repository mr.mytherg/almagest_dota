package com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.stats.presentation.StatsFragmentDirections
import com.erg.almagestdota.stats.presentation.changeable_stats.ChangeableStatsFragmentDirections

class DraftStagesScreen(teamId: Long)  : Screen<NavDirections>(
    route = ChangeableStatsFragmentDirections.toDraftStagesFragment(teamId),
    requestKey = DraftStagesFragment.TAG
)