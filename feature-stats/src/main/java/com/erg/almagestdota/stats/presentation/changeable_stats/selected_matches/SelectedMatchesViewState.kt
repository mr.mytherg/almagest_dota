package com.erg.almagestdota.stats.presentation.changeable_stats.selected_matches

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem

internal sealed class SelectedMatchesViewState : ViewState() {

    class MatchesState(
        val list: List<ComplexItem>,
    ) : SelectedMatchesViewState()
}