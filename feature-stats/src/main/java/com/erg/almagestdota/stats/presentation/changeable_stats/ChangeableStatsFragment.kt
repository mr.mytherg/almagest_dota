package com.erg.almagestdota.stats.presentation.changeable_stats

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.stats.R
import com.erg.almagestdota.stats.databinding.ChangeableStatsFragmentBinding
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
internal class ChangeableStatsFragment : Fragment(R.layout.changeable_stats_fragment), BackPressListener {

    private val args by navArgs<ChangeableStatsFragmentArgs>()

    companion object {
        const val TAG = "ChangeableStatsFragment"
    }

    private val binding by viewBinding(ChangeableStatsFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    @Inject
    lateinit var viewModelFactory: ChangeableStatsViewModel.Factory
    private val viewModel by viewModels<ChangeableStatsViewModel> {
        ChangeableStatsViewModel.provideFactory(
            args.teamId,
            assistedFactory = viewModelFactory
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
        viewModel.viewEvent.setResultListener(this)
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).filterNotNull().onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.loadingState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach {
            binding.loadingView.isVisible = it is Loading.Enabled
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private inner class ViewsConfigurator {

        fun initStartState() = with(binding) {
            tvStages.clicksWithDebounce {
                viewModel.obtainAction(ChangeableStatsViewActions.OpenDraftStages)
            }
            ivBack.clicksWithDebounce {
                navController.popBackStack()
            }
            tvPopularBand.clicksWithDebounce {
                viewModel.obtainAction(ChangeableStatsViewActions.OpenBands)
            }
            tvMeta.clicksWithDebounce {
                viewModel.obtainAction(ChangeableStatsViewActions.OpenMeta)
            }
            tvHeroesRoles.clicksWithDebounce {
                viewModel.obtainAction(ChangeableStatsViewActions.OpenHeroesRoles)
            }
            tvHeroesRolesEnemies.clicksWithDebounce {
                viewModel.obtainAction(ChangeableStatsViewActions.OpenHeroesRolesEnemies)
            }
        }

        fun renderState(state: ChangeableStatsViewState) = with(binding) {
            when (state) {
                is ChangeableStatsViewState.DefaultState -> renderDefaultState(state)
            }
        }

        private fun renderDefaultState(state: ChangeableStatsViewState.DefaultState) = with(binding) {
            if (args.teamId != 0L)
                tvTitle.text = GlobalVars.teams[args.teamId]?.name.orDefault().ifEmpty { args.teamId.toString() }

            tvHeroesRolesEnemies.isVisible = state.isTeam
        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            viewModel.viewEvent.setResultListener(this@ChangeableStatsFragment)
            navController.navigateViaScreenRoute(screen)
        }
    }

    override fun onBackPressed() {
        navController.popBackStack()
    }
}