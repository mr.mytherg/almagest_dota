package com.erg.almagestdota.stats.presentation.changeable_stats.hero_roles.single_hero_roles

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.DraftStagesSortType

internal sealed class SingleHeroRolesViewState : ViewState() {
    class DefaultState(
        val heroName: String,
        val heroes: List<ComplexItem>,
        val sortType: DraftStagesSortType
    ) : SingleHeroRolesViewState()
}