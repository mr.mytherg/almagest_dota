package com.erg.almagestdota.stats.presentation.hero_impacts.impact_healing

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.stats.presentation.hero_impacts.HeroImpactsFragmentDirections

object ImpactHealingScreen : Screen<NavDirections>(
    route = HeroImpactsFragmentDirections.toImpactHealingFragment(),
    requestKey = ImpactHealingFragment.TAG
)