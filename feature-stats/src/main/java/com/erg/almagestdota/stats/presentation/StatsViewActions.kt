package com.erg.almagestdota.stats.presentation

internal sealed class StatsViewActions {

    object OpenCoefs : StatsViewActions()
    object OpenHeroImpacts : StatsViewActions()
    object OpenSelectTeam : StatsViewActions()
    object OpenAllTeams : StatsViewActions()
    object OpenPlayersLanes : StatsViewActions()
    object OpenTournaments : StatsViewActions()
}