package com.erg.almagestdota.stats.presentation.changeable_stats.hero_roles.hero_roles_enemies

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.stats.presentation.changeable_stats.ChangeableStatsFragmentDirections

class HeroRolesEnemiesScreen(teamId: Long) : Screen<NavDirections>(
    route = ChangeableStatsFragmentDirections.toHeroRolesEnemiesFragment(teamId),
    requestKey = HeroRolesEnemiesFragment.TAG
)