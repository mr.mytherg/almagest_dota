package com.erg.almagestdota.stats.presentation.changeable_stats.selected_matches

internal sealed class SelectedMatchesViewActions {

    object OnBackClick : SelectedMatchesViewActions()
    class OpenDraft(val key: String) : SelectedMatchesViewActions()
}