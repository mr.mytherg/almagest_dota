package com.erg.almagestdota.stats.presentation.changeable_stats.meta.single_hero_factions

internal sealed class SingleHeroFactionsActions {

    object SortByFaction : SingleHeroFactionsActions()
    object SortByMatchCount : SingleHeroFactionsActions()
    object SortByWinRate : SingleHeroFactionsActions()
    object SortByPickRate : SingleHeroFactionsActions()
    class ShowMatches(val key: String) : SingleHeroFactionsActions()
}