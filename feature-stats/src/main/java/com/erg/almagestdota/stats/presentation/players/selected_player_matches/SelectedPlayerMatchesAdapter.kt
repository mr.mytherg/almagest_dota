package com.erg.almagestdota.stats.presentation.players.selected_player_matches

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.complexadapter.viewHolders.matches.PlayedMatchViewHolder

internal class SelectedPlayerMatchesAdapter(
    private val complexListener: ComplexListener? = null,
) : ListAdapter<ComplexItem.PlayedMatchItem, PlayedMatchViewHolder>(diffCallback) {

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<ComplexItem.PlayedMatchItem>() {
            override fun areItemsTheSame(
                oldItem: ComplexItem.PlayedMatchItem,
                newItem: ComplexItem.PlayedMatchItem
            ): Boolean {
                return oldItem.key == newItem.key
            }

            override fun areContentsTheSame(
                oldItem: ComplexItem.PlayedMatchItem,
                newItem: ComplexItem.PlayedMatchItem
            ): Boolean {
                return oldItem == newItem
            }
        }
    }

    private val pool = RecyclerView.RecycledViewPool()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayedMatchViewHolder {
        return PlayedMatchViewHolder.create(parent, pool, complexListener)
    }

    override fun onBindViewHolder(holder: PlayedMatchViewHolder, position: Int) {
        holder.bind(currentList[position])
    }
}