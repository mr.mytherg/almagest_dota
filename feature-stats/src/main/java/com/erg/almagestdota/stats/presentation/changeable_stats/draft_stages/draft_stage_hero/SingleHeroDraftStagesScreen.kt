package com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.draft_stage_hero

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.DraftStagesFragmentDirections

class SingleHeroDraftStagesScreen(heroId: Int, teamId: Long) : Screen<NavDirections>(
    route = DraftStagesFragmentDirections.toSingleHeroDraftStagesFragment(heroId,teamId),
    requestKey = SingleHeroDraftStagesFragment.TAG
)