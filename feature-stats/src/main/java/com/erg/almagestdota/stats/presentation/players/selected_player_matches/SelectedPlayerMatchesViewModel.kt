package com.erg.almagestdota.stats.presentation.players.selected_player_matches

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.ui.millisToTextAgo
import com.erg.almagestdota.base.external.ui.secondsToMatchTimerFormat
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.*
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.main.*
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import com.erg.almagestdota.stats.presentation.changeable_stats.open_match.OpenMatchScreen
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

internal class SelectedPlayerMatchesViewModel @AssistedInject constructor(
    @Assisted private val playerId: Long,
    private val stringProvider: StringProvider,
    private val localStorage: ILocalStorageContract
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data -> }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    fun obtainAction(action: SelectedPlayerMatchesViewActions) {
        when (action) {
            is SelectedPlayerMatchesViewActions.OpenDraft -> {
                val matchId = action.key.toLong()
                val screen = OpenMatchScreen(SelectedPlayerMatchesFragmentDirections.toOpenMatchFragment(matchId))
                emit(viewEventMutable, ViewEvent.Navigation(screen))
            }
            is SelectedPlayerMatchesViewActions.OnBackClick -> sendBackClickEvent()
        }
    }

    private fun sendBackClickEvent() {
        emit(viewEventMutable, ViewEvent.PopBackStack.Empty)
    }

    private inner class StateConfigurator {
        var dotaMatches = localStorage.matchesToShow.sortedBy { it.matchId }

        fun defineFragmentState(): SelectedPlayerMatchesViewState {
            return SelectedPlayerMatchesViewState.MatchesState(
                complexList {
                    for (match in dotaMatches) {
                        val tournament = GlobalVars.leagues.find { it.id == match.leagueId }
                        playedMatchItem(key = match.matchId.toString()) {
                            val pair = (match.startTime + match.duration).millisToTextAgo()
                            time =
                                stringProvider.getQuantityString(
                                    pair.first,
                                    pair.second.toInt(),
                                    pair.second.toInt()
                                )
                            title =
                                tournament?.name.orDefault().ifEmpty { match.leagueId.toString() }
                            radiantScore = match.radiantScore
                            direScore = match.direScore
                            durationReadable = match.duration.secondsToMatchTimerFormat()
                            hasRadiantWon = match.hasRadiantWon
                            wasPredictionRight = null
                            radiantName =
                                GlobalVars.teams[match.radiantTeamId]?.name.orDefault()
                                    .ifEmpty {
                                        match.radiantTeamId.toString()
                                    }
                            direName =
                                GlobalVars.teams[match.direTeamId]?.name.orDefault()
                                    .ifEmpty {
                                        match.direTeamId.toString()
                                    }
                            radiantHeroes = getComplexPicks(match.radiantHeroes)
                            direHeroes = getComplexPicks(match.direHeroes)
                        }
                    }
                }.reversed(),
                GlobalVars.players[playerId]?.name.orDefault().ifEmpty { playerId.toString() }
            )
        }

        private fun getComplexPicks(players: List<PlayerInfo>): List<ComplexItem> {
            return complexList {
                for (player in players) {
                    val hero = player.heroId.getHeroById()
                    itemDefault {
                        url = hero.imageLink
                        title = hero.name
                        hasBlackout = player.playerId != playerId
                        hasPlaceholder = true
                    }
                }
            }
        }
    }

    companion object {
        fun provideFactory(
            playerId: Long,
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create(
                        playerId,
                    ) as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(
            playerId: Long,
        ): SelectedPlayerMatchesViewModel
    }
}