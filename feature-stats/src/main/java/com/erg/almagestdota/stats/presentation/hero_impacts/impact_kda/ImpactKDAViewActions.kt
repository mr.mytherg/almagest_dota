package com.erg.almagestdota.stats.presentation.hero_impacts.impact_kda

internal sealed class ImpactKDAViewActions {

    class SelectRole(val key: String) : ImpactKDAViewActions()
    class SelectGameStage(val key: String) : ImpactKDAViewActions()
    object SortByHeroName : ImpactKDAViewActions()
    object SortByValue1 : ImpactKDAViewActions()
    object SortByValue2 : ImpactKDAViewActions()
    object SortByValue3 : ImpactKDAViewActions()
}