package com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.draft_stage_hero

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.extensions.round1
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.NotificationStatus
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.complexList
import com.erg.almagestdota.complexadapter.draftStageItem
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.StageType
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import com.erg.almagestdota.settings.domain.use_cases.UpdateHeroStagesUseCase
import com.erg.almagestdota.stats.R
import com.erg.almagestdota.stats.domain.Interactor
import com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.DraftStagesSortType
import com.erg.almagestdota.stats.presentation.changeable_stats.selected_matches.SelectedMatchesScreen
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

internal class SingleHeroDraftStagesViewModel @AssistedInject constructor(
    @Assisted private val heroId: Int,
    @Assisted private val teamId: Long,
    private val stringProvider: StringProvider,
    private val interactor: Interactor,
    private val localStorage: ILocalStorageContract,
    private val updateHeroStagesUseCase: UpdateHeroStagesUseCase,
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data -> }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    fun obtainAction(action: SingleHeroDraftStagesViewActions) {
        when (action) {
            is SingleHeroDraftStagesViewActions.SortByPickRate -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.PICK_RATE) return
                stateConfigurator.selectedSortType = DraftStagesSortType.PICK_RATE
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is SingleHeroDraftStagesViewActions.SortByMatches -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.MATCHES) return
                stateConfigurator.selectedSortType = DraftStagesSortType.MATCHES
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is SingleHeroDraftStagesViewActions.ShowMatches -> {
                val stageType = StageType.byNumber(action.key.toInt())
                localStorage.matchesToShow = localStorage.selectedMatches.filter {
                    val firstPickTeam = if (it.hasRadiantFirstPick) it.radiantTeamId to it.radiantHeroes else it.direTeamId to it.direHeroes
                    val lastPickTeam = if (!it.hasRadiantFirstPick) it.radiantTeamId to it.radiantHeroes else it.direTeamId to it.direHeroes
                    when (stageType) {
                        StageType.FIRST_PICK -> firstPickTeam.second[0].heroId == heroId && isTeamRelevant(firstPickTeam.first)
                        StageType.DOUBLE_PICK -> (lastPickTeam.second[0].heroId == heroId || lastPickTeam.second[1].heroId == heroId) && isTeamRelevant(lastPickTeam.first)
                        StageType.FIRST_STAGE_ENDING -> firstPickTeam.second[1].heroId == heroId && isTeamRelevant(firstPickTeam.first)
                        StageType.SECOND_STAGE_OPENING -> lastPickTeam.second[2].heroId == heroId && isTeamRelevant(lastPickTeam.first)
                        StageType.SECOND_STAGE_DOUBLE_PICK -> (firstPickTeam.second[2].heroId == heroId || firstPickTeam.second[3].heroId == heroId) && isTeamRelevant(firstPickTeam.first)
                        StageType.SECOND_STAGE_ENDING -> lastPickTeam.second[3].heroId == heroId && isTeamRelevant(lastPickTeam.first)
                        StageType.THIRD_STAGE_OPENING -> firstPickTeam.second[4].heroId == heroId && isTeamRelevant(firstPickTeam.first)
                        StageType.LAST_PICK -> lastPickTeam.second[4].heroId == heroId && isTeamRelevant(lastPickTeam.first)
                    }
                }
                if (localStorage.matchesToShow.isNotEmpty()) {
                    val screen = SelectedMatchesScreen(
                        SingleHeroDraftStagesFragmentDirections.toSelectedMatchesFragment(
                            heroId = heroId,
                            title = stringProvider.getString(StageType.getId(stageType))
                        )
                    )
                    emit(viewEventMutable, ViewEvent.Navigation(screen))
                } else {
                    val text = stringProvider.getString(R.string.no_matches)
                    emit(viewEventMutable, ViewEvent.AlmagestSnackbar(text, NotificationStatus.ERROR))
                }
            }
            is SingleHeroDraftStagesViewActions.SortByStage -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.NAME) return
                stateConfigurator.selectedSortType = DraftStagesSortType.NAME
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is SingleHeroDraftStagesViewActions.SortByWinRate -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.WIN_RATE) return
                stateConfigurator.selectedSortType = DraftStagesSortType.WIN_RATE
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
        }
    }

    private fun isTeamRelevant(teamId: Long): Boolean {
        return this.teamId == 0L || teamId == this.teamId
    }

    private inner class StateConfigurator {
        var selectedSortType: DraftStagesSortType = DraftStagesSortType.MATCHES


        val heroStages = updateHeroStagesUseCase.invoke(localStorage.selectedMatches, teamId)[heroId] ?: listOf()

        fun defineFragmentState(): SingleHeroDraftStagesViewState {
            val stagesSorted = when (selectedSortType) {
                DraftStagesSortType.NAME -> {
                    heroStages.sortedBy { it.stage.number }
                }
                DraftStagesSortType.MATCHES -> {
                    heroStages.sortedByDescending { it.matchCount }
                }
                DraftStagesSortType.WIN_RATE -> {
                    heroStages.sortedByDescending { it.winRate }
                }
                DraftStagesSortType.PICK_RATE -> {
                    heroStages.sortedByDescending { it.pickRate }
                }
            }
            val hero = heroId.getHeroById()
            val stagesComplex = complexList {
                for (heroStage in stagesSorted) {
                    draftStageItem(key = heroStage.stage.number.toString()) {
                        heroName = stringProvider.getString(StageType.getId(heroStage.stage))
                        url = hero.imageLink
                        hideImage = true
                        pickRate = heroStage.pickRate.round1()
                        winRate = heroStage.winRate.round1()
                        matchCount = heroStage.matchCount
                        hasBlackout = heroStage.pickRate.round1() < 10
                    }
                }
            }

            return SingleHeroDraftStagesViewState.DefaultState(
                hero.name,
                stagesComplex,
                selectedSortType
            )
        }
    }

    companion object {
        fun provideFactory(
            heroId: Int,
            teamId: Long,
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create(
                        heroId,
                        teamId
                    ) as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(
            heroId: Int,
            teamId: Long
        ): SingleHeroDraftStagesViewModel
    }
}