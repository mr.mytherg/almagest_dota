package com.erg.almagestdota.stats.presentation.players.open_player

import androidx.constraintlayout.motion.utils.ViewState

internal sealed class OpenPlayerViewState : ViewState() {
    object DefaultState : OpenPlayerViewState()
}