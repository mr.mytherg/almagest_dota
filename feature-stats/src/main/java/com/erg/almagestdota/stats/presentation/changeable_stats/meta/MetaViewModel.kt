package com.erg.almagestdota.stats.presentation.changeable_stats.meta

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.extensions.round1
import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.complexList
import com.erg.almagestdota.complexadapter.draftStageItem
import com.erg.almagestdota.complexadapter.filterItem
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.FactionData
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.stats.domain.Interactor
import com.erg.almagestdota.local_storage.external.models.draft.FactionType
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import com.erg.almagestdota.stats.R
import com.erg.almagestdota.stats.domain.UpdateFactionsDataUseCase
import com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.DraftStagesSortType
import com.erg.almagestdota.stats.presentation.changeable_stats.meta.single_hero_factions.SingleHeroFactionsScreen
import com.erg.almagestdota.stats.presentation.tournament_selection.TournamentSelectionFragment
import com.erg.almagestdota.stats.presentation.tournament_selection.TournamentSelectionScreen
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

internal class MetaViewModel @AssistedInject constructor(
    @Assisted private val teamId: Long,
    private val stringProvider: StringProvider,
    private val interactor: Interactor,
    private val localStorage: ILocalStorageContract,
    private val updateFactionsDataUseCase: UpdateFactionsDataUseCase
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data ->
        when (resultKey) {
            TournamentSelectionFragment.TAG -> {
                if (TournamentSelectionScreen.getResult(data) == TournamentSelectionScreen.Result.SUBMITTED) {
                    viewStateMutable.value = stateConfigurator.defineFragmentState()
                }
            }
        }
    }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    fun obtainAction(action: MetaViewActions) {
        when (action) {
            is MetaViewActions.SortByPickRate -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.PICK_RATE) return
                stateConfigurator.selectedSortType = DraftStagesSortType.PICK_RATE
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is MetaViewActions.SelectFactionType -> {
                val faction = FactionType.valueOf(action.key)
                if (stateConfigurator.selectedFaction == faction) return
                stateConfigurator.selectedFaction = faction
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is MetaViewActions.ShowFactions -> {
                val heroId = action.key.toInt()
                if (heroId == 0) return
                val screen = SingleHeroFactionsScreen(heroId, teamId)
                emit(viewEventMutable, ViewEvent.Navigation(screen))
            }
            is MetaViewActions.SortByMatches -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.MATCHES) return
                stateConfigurator.selectedSortType = DraftStagesSortType.MATCHES
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is MetaViewActions.SortByHeroName -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.NAME) return
                stateConfigurator.selectedSortType = DraftStagesSortType.NAME
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is MetaViewActions.SortByWinRate -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.WIN_RATE) return
                stateConfigurator.selectedSortType = DraftStagesSortType.WIN_RATE
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
        }
    }

    private inner class StateConfigurator {
        var selectedSortType: DraftStagesSortType = DraftStagesSortType.MATCHES
        var selectedFaction: FactionType = FactionType.OVERALL
        private val factionTypes = listOf(
            FactionType.OVERALL,
            FactionType.RADIANT,
            FactionType.DIRE,
        )
        val factions = updateFactionsDataUseCase.invoke(localStorage.selectedMatches, teamId).toList().map {
            it.first.getHeroById() to it.second
        }

        fun defineFragmentState(): MetaViewState {
            val factionsComplex = complexList {
                for (index in factionTypes.indices) {
                    val faction = factionTypes[index]
                    filterItem(key = faction.name) {
                        title = stringProvider.getString(FactionType.getId(faction))
                        isSelected = faction == selectedFaction
                    }
                }
            }
            val heroesToShow = when (selectedSortType) {
                DraftStagesSortType.NAME -> {
                    factions.sortedBy {
                        it.first.name
                    }
                }
                DraftStagesSortType.WIN_RATE -> {
                    factions.sortedWith(
                        compareByDescending<Pair<HeroCoreData, List<FactionData>>> {
                            val faction = it.second.first { it.factionType == selectedFaction }
                            faction.winRate
                        }.thenByDescending {
                            val faction = it.second.first { it.factionType == selectedFaction }
                            faction.matchCount
                        }
                    )
                }
                DraftStagesSortType.PICK_RATE -> {
                    factions.sortedWith(
                        compareByDescending<Pair<HeroCoreData, List<FactionData>>> {
                            val faction = it.second.first { it.factionType == selectedFaction }
                            faction.pickRate
                        }.thenByDescending {
                            val faction = it.second.first { it.factionType == selectedFaction }
                            faction.winRate
                        }
                    )
                }
                DraftStagesSortType.MATCHES -> {
                    factions.sortedWith(
                        compareByDescending<Pair<HeroCoreData, List<FactionData>>> {
                            val faction = it.second.first { it.factionType == selectedFaction }
                            faction.matchCount
                        }.thenByDescending {
                            val faction = it.second.first { it.factionType == selectedFaction }
                            faction.winRate
                        }
                    )
                }
            }
            val heroes = complexList {
//                val matches = localStorage.selectedMatches.filter {
//                    when (selectedFaction) {
//                        FactionType.OVERALL -> true
//                        FactionType.DIRE -> !it.hasRadiantWon
//                        else -> it.hasRadiantWon
//                    }
//                }
//                draftStageItem(key = "0") {
//                    heroName = stringProvider.getString(R.string.overall)
//                    url = ""
//                    pickRate = if (selectedFaction == FactionType.OVERALL) 100.0 else 50.0
//                    winRate = if (selectedFaction == FactionType.OVERALL) 50.0 else getPercentage(matches.size, localStorage.selectedMatches.size)
//                    matchCount = localStorage.selectedMatches.size
//                }
                for (hero in heroesToShow) {
                    val faction = hero.second.first { it.factionType == selectedFaction }
                    draftStageItem(key = hero.first.id.toString()) {
                        heroName = hero.first.name
                        url = hero.first.imageLink
                        pickRate = faction.pickRate.round1()
                        winRate = faction.winRate.round1()
                        matchCount = faction.matchCount
                        hasBlackout = GlobalVars.heroesAsMap[hero.first.id]!!.factions[0].pickRate < 2
                    }
                }
            }

            return MetaViewState.DefaultState(
                factionsComplex,
                heroes,
                selectedSortType
            )
        }

        private fun getPercentage(param1: Int, total: Int): Double {
            return if (total == 0) 0.0 else (param1 * 1.0 / total * 100).round1()
        }
    }

    companion object {
        fun provideFactory(
            teamId: Long,
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create(teamId
                    ) as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(
            teamId: Long
        ): MetaViewModel
    }
}