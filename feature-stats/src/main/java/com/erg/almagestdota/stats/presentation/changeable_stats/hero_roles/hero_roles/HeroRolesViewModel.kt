package com.erg.almagestdota.stats.presentation.changeable_stats.hero_roles.hero_roles

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.extensions.round1
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.complexList
import com.erg.almagestdota.complexadapter.draftStageItem
import com.erg.almagestdota.complexadapter.filterItem
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.*
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData
import com.erg.almagestdota.local_storage.external.models.matches.getHeroById
import com.erg.almagestdota.settings.domain.use_cases.UpdateHeroRolesUseCase
import com.erg.almagestdota.stats.domain.Interactor
import com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.DraftStagesSortType
import com.erg.almagestdota.stats.presentation.changeable_stats.hero_roles.single_hero_roles.SingleHeroRolesScreen
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

internal class HeroRolesViewModel @AssistedInject constructor(
    @Assisted private val teamId: Long,
    private val stringProvider: StringProvider,
    private val interactor: Interactor,
    private val updateHeroRolesUseCase: UpdateHeroRolesUseCase,
    private val localStorage: ILocalStorageContract,
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data ->

    }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    fun obtainAction(action: HeroRolesViewActions) {
        when (action) {
            is HeroRolesViewActions.SelectRole -> {
                if (stateConfigurator.selectedRole == RoleType.byName(action.key)) return
                stateConfigurator.selectedRole = RoleType.byName(action.key)
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is HeroRolesViewActions.SelectHero -> {
                val heroId = action.key.toInt()
                val screen = SingleHeroRolesScreen(HeroRolesFragmentDirections.toSingleHeroRolesFragment(heroId, false, teamId))
                emit(viewEventMutable, ViewEvent.Navigation(screen))
            }
            is HeroRolesViewActions.SortByPickRate -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.PICK_RATE) return
                stateConfigurator.selectedSortType = DraftStagesSortType.PICK_RATE
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is HeroRolesViewActions.SortByMatchCount -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.MATCHES) return
                stateConfigurator.selectedSortType = DraftStagesSortType.MATCHES
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is HeroRolesViewActions.SortByHeroName -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.NAME) return
                stateConfigurator.selectedSortType = DraftStagesSortType.NAME
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is HeroRolesViewActions.SortByWinRate -> {
                if (stateConfigurator.selectedSortType == DraftStagesSortType.WIN_RATE) return
                stateConfigurator.selectedSortType = DraftStagesSortType.WIN_RATE
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
        }
    }

    private inner class StateConfigurator {
        var selectedRole: RoleType = RoleType.ROLE_1
        var selectedSortType: DraftStagesSortType = DraftStagesSortType.MATCHES
        var selectedRoleIndex: Int = 0
        private val roleTypes = listOf(
            RoleType.ROLE_1,
            RoleType.ROLE_2,
            RoleType.ROLE_3,
            RoleType.ROLE_4,
            RoleType.ROLE_5,
        )
        val roles = updateHeroRolesUseCase.invoke(localStorage.selectedMatches, teamId, false).toList().map {
            it.first.getHeroById() to it.second
        }

        fun defineFragmentState(): HeroRolesViewState {
            val rolesComplex = complexList {
                for (index in roleTypes.indices) {
                    val role = roleTypes[index]
                    filterItem(key = role.name) {
                        title = stringProvider.getString(RoleType.getId(role))
                        isSelected = role == selectedRole
                        if (isSelected)
                            selectedRoleIndex = index
                    }
                }
            }
            val heroesToShow = when (selectedSortType) {
                DraftStagesSortType.NAME -> {
                    roles.sortedBy {
                        it.first.name
                    }
                }
                DraftStagesSortType.WIN_RATE -> {
                    roles.sortedWith(
                        compareByDescending<Pair<HeroCoreData, List<HeroRole>>> {
                            val role = it.second.first { it.roleType == selectedRole }
                            role.winRatePercentage
                        }.thenByDescending {
                            val role = it.second.first { it.roleType == selectedRole }
                            role.matchCount
                        }
                    )
                }
                DraftStagesSortType.PICK_RATE -> {
                    roles.sortedWith(
                        compareByDescending<Pair<HeroCoreData, List<HeroRole>>> {
                            val role = it.second.first { it.roleType == selectedRole }
                            role.pickRate
                        }.thenByDescending {
                            val role = it.second.first { it.roleType == selectedRole }
                            role.winRatePercentage
                        }
                    )
                }
                DraftStagesSortType.MATCHES -> {
                    roles.sortedWith(
                        compareByDescending<Pair<HeroCoreData, List<HeroRole>>> {
                            val role = it.second.first { it.roleType == selectedRole }
                            role.matchCount
                        }.thenByDescending {
                            val role = it.second.first { it.roleType == selectedRole }
                            role.winRatePercentage
                        }
                    )
                }
            }
            val heroes = complexList {
                for (hero in heroesToShow) {
                    val role = hero.second.first { it.roleType == selectedRole }
                    draftStageItem(key = hero.first.id.toString()) {
                        heroName = hero.first.name
                        url = hero.first.imageLink
                        pickRate = role.pickRate.round1()
                        winRate = role.winRatePercentage.round1()
                        matchCount = role.matchCount
                        hasBlackout = !hero.second.isHeroFitsForRole(role.roleType)
                    }
                }
            }

            return HeroRolesViewState.DefaultState(
                rolesComplex,
                heroes,
                selectedRoleIndex,
                selectedSortType
            )
        }
    }

    companion object {
        fun provideFactory(
            teamId: Long,
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create(
                        teamId
                    ) as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(
            teamId: Long
        ): HeroRolesViewModel
    }
}