package com.erg.almagestdota.stats.presentation.players.lanes

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.extensions.round1
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.complexList
import com.erg.almagestdota.complexadapter.filterItem
import com.erg.almagestdota.complexadapter.playerLanesItem
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.*
import com.erg.almagestdota.local_storage.external.models.draft.main.MemberInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.settings.domain.use_cases.GetPlayersLanesUseCase
import com.erg.almagestdota.stats.domain.Interactor
import com.erg.almagestdota.stats.presentation.players.lanes.single_player_lanes.SinglePlayerLanesScreen
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

internal class PlayersLanesViewModel @AssistedInject constructor(
    private val stringProvider: StringProvider,
    private val interactor: Interactor,
    private val getPlayersLanesUseCase: GetPlayersLanesUseCase,
    private val localStorage: ILocalStorageContract,
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data ->

    }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    fun obtainAction(action: PlayersLanesViewActions) {
        when (action) {
            is PlayersLanesViewActions.SelectRole -> {
                if (stateConfigurator.selectedRole == RoleType.byName(action.key)) return
                stateConfigurator.selectedRole = RoleType.byName(action.key)
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is PlayersLanesViewActions.OpenPlayer -> {
                val playerId = action.key.toLong()
                val screen = SinglePlayerLanesScreen(playerId)
                emit(viewEventMutable, ViewEvent.Navigation(screen))
            }
            is PlayersLanesViewActions.SortByPlayerName -> {
                if (stateConfigurator.selectedSortType == PlayersLanesSortType.NAME) return
                stateConfigurator.selectedSortType = PlayersLanesSortType.NAME
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is PlayersLanesViewActions.SortByMatchCount -> {
                if (stateConfigurator.selectedSortType == PlayersLanesSortType.MATCH_COUNT) return
                stateConfigurator.selectedSortType = PlayersLanesSortType.MATCH_COUNT
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is PlayersLanesViewActions.SortByOverall -> {
                if (stateConfigurator.selectedSortType == PlayersLanesSortType.OVERALL) return
                stateConfigurator.selectedSortType = PlayersLanesSortType.OVERALL
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
        }
    }

    private inner class StateConfigurator {
        var selectedRole: RoleType = RoleType.ROLE_1
        var selectedSortType: PlayersLanesSortType = PlayersLanesSortType.OVERALL
        var selectedRoleIndex: Int = 0

        private val roleTypes = listOf(
            RoleType.ROLE_1,
            RoleType.ROLE_2,
            RoleType.ROLE_3,
            RoleType.ROLE_4,
            RoleType.ROLE_5,
        )
        val players = getPlayersLanesUseCase.invoke(localStorage.selectedMatches).toList().map {
            (GlobalVars.players[it.first] ?: MemberInfo(it.first)) to it.second
        }

        fun defineFragmentState(): PlayersLanesViewState {
            val rolesComplex = complexList {
                for (index in roleTypes.indices) {
                    val role = roleTypes[index]
                    filterItem(key = role.name) {
                        title = stringProvider.getString(RoleType.getId(role))
                        isSelected = role == selectedRole
                        if (isSelected)
                            selectedRoleIndex = index
                    }
                }
            }
            val playersFiltered = players.filter { it.first.role == selectedRole }
            val playersToShow = when (selectedSortType) {
                PlayersLanesSortType.NAME -> {
                    playersFiltered.sortedBy {
                        it.first.name.ifEmpty { it.first.playerId.toString() }
                    }
                }
                PlayersLanesSortType.OVERALL -> {
                    playersFiltered.sortedWith(
                        compareByDescending<Pair<MemberInfo, PlayerLaneOutcome>> {
                            getOverall(it.second)
                        }.thenByDescending {
                            it.second.matchCount
                        }
                    )
                }
                PlayersLanesSortType.MATCH_COUNT -> {
                    playersFiltered.sortedWith(
                        compareByDescending<Pair<MemberInfo, PlayerLaneOutcome>> {
                            it.second.matchCount
                        }.thenByDescending {
                            getOverall(it.second)
                        }
                    )
                }
            }
            val players = complexList {
                for (player in playersToShow) {
                    val teamName = GlobalVars.teams[player.second.teamId]?.name ?: player.second.teamId.toString()
                    playerLanesItem(key = player.first.playerId.toString()) {
                        playerName = player.first.name.ifEmpty { player.first.playerId.toString() } + " (${teamName})"
                        overall = getOverall(player.second)
                        matchCount = player.second.matchCount
                    }
                }
            }

            return PlayersLanesViewState.DefaultState(
                rolesComplex,
                players,
                selectedSortType
            )
        }

        private fun getOverall(outcome: PlayerLaneOutcome): Double {
            var res = 0.0
            res += outcome.winPlus * 8
            res += outcome.win * 6
            res += outcome.tie * 4
            res += outcome.lose * 2
            val sum = outcome.win + outcome.winPlus + outcome.tie + outcome.lose + outcome.loseMinus

            return if (res == 0.0 || sum == 0) 0.0 else (res / sum).round1()
        }
    }

    companion object {
        fun provideFactory(
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create(
                    ) as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(
        ): PlayersLanesViewModel
    }
}