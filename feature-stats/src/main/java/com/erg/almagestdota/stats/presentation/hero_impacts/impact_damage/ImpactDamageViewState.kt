package com.erg.almagestdota.stats.presentation.hero_impacts.impact_damage

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem

internal sealed class ImpactDamageViewState : ViewState() {
    class DefaultState(
        val roles: List<ComplexItem>,
        val gameStages: List<ComplexItem>,
        val heroes: List<ComplexItem>,
        val sortType: ImpactDamageSortType
    ) : ImpactDamageViewState()
}