package com.erg.almagestdota.stats.presentation.players.selected_player_matches

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.stats.R
import com.erg.almagestdota.stats.databinding.SelectedMatchesFragmentBinding
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject


@AndroidEntryPoint
internal class SelectedPlayerMatchesFragment : Fragment(R.layout.selected_matches_fragment),
    BackPressListener {

    private val args by navArgs<SelectedPlayerMatchesFragmentArgs>()

    companion object {
        const val TAG = "SelectedPlayerMatchesFragment"
    }

    private val binding by viewBinding(SelectedMatchesFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    @Inject
    lateinit var viewModelFactory: SelectedPlayerMatchesViewModel.Factory
    private val viewModel by viewModels<SelectedPlayerMatchesViewModel> {
        SelectedPlayerMatchesViewModel.provideFactory(
            args.playerId,
            assistedFactory = viewModelFactory
        )
    }

    override fun onBackPressed() {
        viewModel.obtainAction(SelectedPlayerMatchesViewActions.OnBackClick)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
        viewModel.viewEvent.setResultListener(this)
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).filterNotNull().onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private inner class ViewsConfigurator {

        private val matchesAdapter by lazy {
            SelectedPlayerMatchesAdapter(
                complexListener = object : ComplexListener {
                    override fun onClick(item: ComplexItem) {
                        viewModel.obtainAction(SelectedPlayerMatchesViewActions.OpenDraft(item.key))
                    }
                }
            )
        }

        fun initStartState() = with(binding) {
            ivBack.clicksWithDebounce {
                viewModel.obtainAction(SelectedPlayerMatchesViewActions.OnBackClick)
            }
        }

        fun renderState(state: SelectedPlayerMatchesViewState) {
            when (state) {
                is SelectedPlayerMatchesViewState.MatchesState -> renderMatchesState(state)
            }
        }

        private fun renderMatchesState(state: SelectedPlayerMatchesViewState.MatchesState) = with(binding) {
            if (rvMatches.adapter == null)
                rvMatches.adapter = matchesAdapter
            matchesAdapter.submitList(state.list as List<ComplexItem.PlayedMatchItem>)
            tvTitle.text = state.playerName
        }


    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
                is ViewEvent.PopBackStack<*> -> navController.popBackStack()
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            viewModel.viewEvent.setResultListener(this@SelectedPlayerMatchesFragment)
            navController.navigateViaScreenRoute(screen)
        }
    }
}