package com.erg.almagestdota.stats.presentation.changeable_stats.hero_roles.hero_roles

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.stats.presentation.changeable_stats.ChangeableStatsFragmentDirections

class HeroRolesScreen(teamId: Long) : Screen<NavDirections>(
    route = ChangeableStatsFragmentDirections.toHeroRolesFragment(teamId),
    requestKey = HeroRolesFragment.TAG
)