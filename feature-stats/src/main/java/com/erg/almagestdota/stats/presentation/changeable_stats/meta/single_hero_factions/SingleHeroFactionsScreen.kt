package com.erg.almagestdota.stats.presentation.changeable_stats.meta.single_hero_factions

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.stats.presentation.changeable_stats.meta.MetaFragmentDirections

class SingleHeroFactionsScreen(heroId: Int, teamId: Long) : Screen<NavDirections>(
    route = MetaFragmentDirections.toSingleHeroFactionsFragment(heroId, teamId),
    requestKey = SingleHeroFactionsFragment.TAG
)