package com.erg.almagestdota.stats.presentation.changeable_stats.hero_roles.hero_roles_enemies

internal sealed class HeroRolesEnemiesViewActions {

    class SelectRole(val key: String) : HeroRolesEnemiesViewActions()
    class SelectHero(val key: String) : HeroRolesEnemiesViewActions()
    object SortByHeroName : HeroRolesEnemiesViewActions()
    object SortByMatchCount : HeroRolesEnemiesViewActions()
    object SortByWinRate : HeroRolesEnemiesViewActions()
    object SortByPickRate : HeroRolesEnemiesViewActions()
}