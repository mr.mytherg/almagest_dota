package com.erg.almagestdota.stats.presentation.hero_impacts.impact_healing

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem

internal sealed class ImpactHealingViewState : ViewState() {
    class DefaultState(
        val roles: List<ComplexItem>,
        val gameStages: List<ComplexItem>,
        val heroes: List<ComplexItem>,
        val sortType: ImpactHealingSortType
    ) : ImpactHealingViewState()
}