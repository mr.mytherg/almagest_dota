package com.erg.almagestdota.stats.presentation.hero_impacts.impact_kda

enum class ImpactKDASortType {
    NAME,
    KILLS,
    DEATHS,
    ASSISTS;
}