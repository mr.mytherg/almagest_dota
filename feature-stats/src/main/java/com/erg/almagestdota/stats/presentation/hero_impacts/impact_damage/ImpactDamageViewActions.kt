package com.erg.almagestdota.stats.presentation.hero_impacts.impact_damage

internal sealed class ImpactDamageViewActions {

    class SelectRole(val key: String) : ImpactDamageViewActions()
    class SelectGameStage(val key: String) : ImpactDamageViewActions()
    object SortByHeroName : ImpactDamageViewActions()
    object SortByValue1 : ImpactDamageViewActions()
    object SortByValue2 : ImpactDamageViewActions()
}