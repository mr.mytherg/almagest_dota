package com.erg.almagestdota.stats.presentation.hero_impacts.impact_disables

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem

internal sealed class ImpactDisablesViewState : ViewState() {
    class DefaultState(
        val roles: List<ComplexItem>,
        val gameStages: List<ComplexItem>,
        val heroes: List<ComplexItem>,
        val sortType: ImpactDisablesSortType
    ) : ImpactDisablesViewState()
}