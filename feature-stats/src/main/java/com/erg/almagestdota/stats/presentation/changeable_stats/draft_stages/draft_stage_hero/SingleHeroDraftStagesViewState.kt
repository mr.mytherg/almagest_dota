package com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.draft_stage_hero

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.DraftStagesSortType

internal sealed class SingleHeroDraftStagesViewState : ViewState() {
    class DefaultState(
        val heroName: String,
        val heroes: List<ComplexItem>,
        val sortType: DraftStagesSortType
    ) : SingleHeroDraftStagesViewState()
}