package com.erg.almagestdota.stats.presentation.coefs

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.stats.presentation.StatsFragmentDirections

class CoefsScreen(heroId: Int) : Screen<NavDirections>(
    route = StatsFragmentDirections.toCoefsFragment(heroId),
    requestKey = CoefsFragment.TAG
)