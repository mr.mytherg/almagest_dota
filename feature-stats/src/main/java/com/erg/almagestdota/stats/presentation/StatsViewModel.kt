package com.erg.almagestdota.stats.presentation

import androidx.lifecycle.ViewModel
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.NotificationStatus
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.heroselector.presentation.heroSelector.HeroSelectorScreen
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.stats.R
import com.erg.almagestdota.stats.presentation.changeable_stats.ChangeableStatsScreen
import com.erg.almagestdota.stats.presentation.coefs.CoefsScreen
import com.erg.almagestdota.stats.presentation.hero_impacts.HeroImpactsScreen
import com.erg.almagestdota.stats.presentation.players.lanes.PlayersLanesScreen
import com.erg.almagestdota.stats.presentation.team_selector.TeamSelectionScreen
import com.erg.almagestdota.stats.presentation.team_selector.TeamSelectionSheet
import com.erg.almagestdota.stats.presentation.tournament_selection.TournamentSelectionFragment
import com.erg.almagestdota.stats.presentation.tournament_selection.TournamentSelectionScreen
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

@HiltViewModel
internal class StatsViewModel @Inject constructor(
    private val stringProvider: StringProvider,
    private val localStorage: ILocalStorageContract,
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data ->
        when (resultKey) {
            TeamSelectionScreen.TAG -> {
                if (TeamSelectionScreen.getResult(data) == TeamSelectionScreen.Result.SELECTED) {
                    val teamId = TeamSelectionScreen.getSelectedTeam(data)
                    openTeam(teamId)
                }
            }
            HeroSelectorScreen.TAG -> {
                if (HeroSelectorScreen.getResult(data) == HeroSelectorScreen.Result.SELECTED) {
                    val heroId = HeroSelectorScreen.getSelectedHero(data)
                    openCoefsScreen(heroId)
                }
            }
            TournamentSelectionFragment.TAG -> {
                if (TournamentSelectionScreen.getResult(data) == TournamentSelectionScreen.Result.SUBMITTED) {
                    stateConfigurator.hasMatchesLoaded = true
                }
            }
        }
    }

    private fun openCoefsScreen(heroId: Int) {
        emit(viewEventMutable, ViewEvent.Navigation(CoefsScreen(heroId)))
    }

    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow<StatsViewState?>(null)
    val viewState = viewStateMutable.asStateFlow()

    private val loadingStateMutable = MutableStateFlow<Loading>(Loading.Disabled)
    val loadingState = loadingStateMutable.asStateFlow()

    init {
        if (localStorage.metaMatches.isNotEmpty())
            stateConfigurator.hasMatchesLoaded = true
        viewStateMutable.value = stateConfigurator.defineFragmentState()
    }

    private fun openTeam(teamId: Long) {
        val screen = ChangeableStatsScreen.ChangeableStatsScreenDeeplink(teamId)
        emit(viewEventMutable, ViewEvent.Navigation(screen))
    }

    fun obtainAction(action: StatsViewActions) {
        when (action) {
            is StatsViewActions.OpenSelectTeam -> {
                if (stateConfigurator.hasMatchesLoaded)
                    emit(viewEventMutable, ViewEvent.Navigation(TeamSelectionScreen(true)))
                else
                    showSelectTournamentsError()
            }
            is StatsViewActions.OpenAllTeams -> {
                if (stateConfigurator.hasMatchesLoaded) {
                    localStorage.selectedMatches = localStorage.metaMatches
                    val screen = ChangeableStatsScreen.ChangeableStatsScreenDeeplink(0L)
                    emit(viewEventMutable, ViewEvent.Navigation(screen))
                } else
                    showSelectTournamentsError()
            }
            is StatsViewActions.OpenPlayersLanes -> {
                if (stateConfigurator.hasMatchesLoaded) {
                    localStorage.selectedMatches = localStorage.metaMatches
                    emit(viewEventMutable, ViewEvent.Navigation(PlayersLanesScreen()))
                } else
                    showSelectTournamentsError()
            }
            is StatsViewActions.OpenCoefs -> emit(viewEventMutable, ViewEvent.Navigation(HeroSelectorScreen()))
            is StatsViewActions.OpenHeroImpacts -> emit(viewEventMutable, ViewEvent.Navigation(HeroImpactsScreen))
            is StatsViewActions.OpenTournaments -> emit(viewEventMutable, ViewEvent.Navigation(TournamentSelectionScreen()))
        }
    }

    private fun showSelectTournamentsError() {
        val text = stringProvider.getString(R.string.stats_snack_error_select_tournaments)
        emit(viewEventMutable, ViewEvent.AlmagestSnackbar(text, NotificationStatus.ERROR))
    }

    private inner class StateConfigurator {
        var hasMatchesLoaded: Boolean = false

        fun defineFragmentState(): StatsViewState {
            return StatsViewState.DefaultState
        }
    }
}