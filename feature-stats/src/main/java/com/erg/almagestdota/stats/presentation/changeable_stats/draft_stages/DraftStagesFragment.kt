package com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages

import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.ComplexAdapter
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.stats.R
import com.erg.almagestdota.stats.databinding.DraftStagesFragmentBinding
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import com.google.android.material.textview.MaterialTextView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
internal class DraftStagesFragment : Fragment(R.layout.draft_stages_fragment), BackPressListener {

    private val args by navArgs<DraftStagesFragmentArgs>()
    companion object {
        const val TAG = "DraftStagesFragment"
        const val MILLISECONDS_PER_INCH = 500f
    }

    private val binding by viewBinding(DraftStagesFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    @Inject
    lateinit var viewModelFactory: DraftStagesViewModel.Factory
    private val viewModel by viewModels<DraftStagesViewModel> {
        DraftStagesViewModel.provideFactory(
            args.teamId,
            assistedFactory = viewModelFactory
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
        viewModel.viewEvent.setResultListener(this)
    }

    override fun onBackPressed() {
        navController.popBackStack()
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private inner class ViewsConfigurator {
        private val draftStages = ComplexAdapter(
            complexListener = object : ComplexListener {
                override fun onClick(item: ComplexItem) {
                    viewModel.obtainAction(DraftStagesViewActions.SelectDraftStage(item.key))
                }
            }
        )

        private val smoothScrollerStages: RecyclerView.SmoothScroller by lazy { createSmoothScrollListener() }
        private fun createSmoothScrollListener(): RecyclerView.SmoothScroller {
            return object : LinearSmoothScroller(requireContext()) {
                override fun getHorizontalSnapPreference(): Int = SNAP_TO_START
                override fun calculateSpeedPerPixel(displayMetrics: DisplayMetrics): Float =
                    MILLISECONDS_PER_INCH / displayMetrics.densityDpi
            }
        }

        fun initStartState() = with(binding) {
            tvWinRate.clicksWithDebounce {
                viewModel.obtainAction(DraftStagesViewActions.SortByWinRate)
            }
            ivBack.clicksWithDebounce {
                navController.popBackStack()
            }
            tvName.clicksWithDebounce {
                viewModel.obtainAction(DraftStagesViewActions.SortByHeroName)
            }
            tvPickRate.clicksWithDebounce {
                viewModel.obtainAction(DraftStagesViewActions.SortByPickRate)
            }
            tvMatchCount.clicksWithDebounce {
                viewModel.obtainAction(DraftStagesViewActions.SortByMatches)
            }
        }

        fun renderState(state: DraftStagesViewState) = with(binding) {
            when (state) {
                is DraftStagesViewState.DefaultState -> renderDefaultState(state)
            }
        }

        private fun renderDefaultState(state: DraftStagesViewState.DefaultState) = with(binding) {
            if (rvStages.adapter == null) {
                rvStages.adapter = draftStages
                rvStages.setHasFixedSize(true)
            }
            if (args.teamId != 0L)
                tvSubtitle.text = GlobalVars.teams[args.teamId]?.name.orDefault().ifEmpty { args.teamId.toString() }
            draftStages.submitList(state.draftStages)
            rvHeroes.adapter = ComplexAdapter(
                complexListener = object : ComplexListener {
                    override fun onClick(item: ComplexItem) {
                        viewModel.obtainAction(DraftStagesViewActions.SelectHero(item.key))
                    }
                }
            ).apply {
                submitList(state.heroes)
            }
            scrollRvToPosition(state.stageIndex, smoothScrollerStages, rvStages.layoutManager!!)
            setSortUnselected(tvWinRate)
            setSortUnselected(tvPickRate)
            setSortUnselected(tvMatchCount)
            setSortUnselected(tvName)
            when (state.sortType) {
                DraftStagesSortType.PICK_RATE -> setSortSelected(tvPickRate)
                DraftStagesSortType.WIN_RATE -> setSortSelected(tvWinRate)
                DraftStagesSortType.NAME -> setSortSelected(tvName)
                DraftStagesSortType.MATCHES -> setSortSelected(tvMatchCount)

            }
        }

        private fun setSortUnselected(textView: MaterialTextView) {
            textView.setTextColor(ContextCompat.getColor(textView.context, R.color.textColor))
        }

        private fun setSortSelected(textView: MaterialTextView) {
            textView.setTextColor(ContextCompat.getColor(textView.context, R.color.colorPrimary))
        }

        private fun scrollRvToPosition(
            position: Int,
            smoothScroller: RecyclerView.SmoothScroller,
            layoutManager: RecyclerView.LayoutManager
        ) {
//            smoothScroller.targetPosition = position
//            layoutManager.startSmoothScroll(smoothScroller)
        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            viewModel.viewEvent.setResultListener(this@DraftStagesFragment)
            navController.navigateViaScreenRoute(screen)
        }
    }
}