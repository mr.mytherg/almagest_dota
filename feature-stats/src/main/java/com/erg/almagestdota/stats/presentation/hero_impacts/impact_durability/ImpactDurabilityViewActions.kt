package com.erg.almagestdota.stats.presentation.hero_impacts.impact_durability

internal sealed class ImpactDurabilityViewActions {

    class SelectRole(val key: String) : ImpactDurabilityViewActions()
    class SelectGameStage(val key: String) : ImpactDurabilityViewActions()
    object SortByHeroName : ImpactDurabilityViewActions()
    object SortByOverall : ImpactDurabilityViewActions()
}