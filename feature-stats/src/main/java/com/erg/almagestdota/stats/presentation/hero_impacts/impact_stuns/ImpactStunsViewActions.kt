package com.erg.almagestdota.stats.presentation.hero_impacts.impact_stuns

internal sealed class ImpactStunsViewActions {

    class SelectRole(val key: String) : ImpactStunsViewActions()
    class SelectGameStage(val key: String) : ImpactStunsViewActions()
    object SortByHeroName : ImpactStunsViewActions()
    object SortByValue1 : ImpactStunsViewActions()
}