package com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages

enum class DraftStagesSortType {
    NAME,
    WIN_RATE,
    MATCHES,
    PICK_RATE;
}