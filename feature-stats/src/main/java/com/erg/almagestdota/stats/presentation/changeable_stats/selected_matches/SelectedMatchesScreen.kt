package com.erg.almagestdota.stats.presentation.changeable_stats.selected_matches

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen

class SelectedMatchesScreen(
    route: NavDirections
) : Screen<NavDirections>(
    route = route,
    requestKey = SelectedMatchesFragment.TAG
)