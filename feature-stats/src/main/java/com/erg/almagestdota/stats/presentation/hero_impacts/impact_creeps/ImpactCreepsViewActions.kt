package com.erg.almagestdota.stats.presentation.hero_impacts.impact_creeps

internal sealed class ImpactCreepsViewActions {

    class SelectRole(val key: String) : ImpactCreepsViewActions()
    class SelectGameStage(val key: String) : ImpactCreepsViewActions()
    object SortByHeroName : ImpactCreepsViewActions()
    object SortByValue1 : ImpactCreepsViewActions()
    object SortByValue2 : ImpactCreepsViewActions()
    object SortByValue3 : ImpactCreepsViewActions()
}