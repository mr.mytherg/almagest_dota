package com.erg.almagestdota.stats.presentation.coefs

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData

internal sealed class CoefsViewState : ViewState() {
    class DefaultState(
        val hero: HeroCoreData,
        val coefTypes: List<ComplexItem>,
        val heroes: List<ComplexItem>,
        val sortType: CoefsSortType
    ) : CoefsViewState()
}