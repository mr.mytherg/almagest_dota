package com.erg.almagestdota.stats.presentation.tournament_selection

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.stats.presentation.StatsFragmentDirections

class TournamentSelectionScreen : Screen<NavDirections>(
    route = StatsFragmentDirections.toTournamentSelectionFragment(),
    requestKey = TournamentSelectionFragment.TAG
) {
    companion object {
        private const val RESULT_KEY = "RESULT_KEY"

        fun getResult(data: Bundle): Result = Result.enumValueOf(data.getString(RESULT_KEY))
    }

    enum class Result {
        SUBMITTED,
        CANCELED;

        companion object {
            fun enumValueOf(value: String?): Result {
                return values().firstOrNull { it.name == value } ?: CANCELED
            }

            internal fun createResultCanceled(): Bundle {
                return bundleOf(RESULT_KEY to CANCELED.name)
            }

            internal fun createResultSubmitted(): Bundle {
                return bundleOf(RESULT_KEY to SUBMITTED.name)
            }
        }
    }
}