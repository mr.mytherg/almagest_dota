package com.erg.almagestdota.stats.presentation.changeable_stats.bands

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.stats.presentation.StatsFragmentDirections
import com.erg.almagestdota.stats.presentation.changeable_stats.ChangeableStatsFragmentDirections

class BandsScreen(teamId: Long) : Screen<NavDirections>(
    route = ChangeableStatsFragmentDirections.toBandsFragment(teamId),
    requestKey = BandsFragment.TAG
)