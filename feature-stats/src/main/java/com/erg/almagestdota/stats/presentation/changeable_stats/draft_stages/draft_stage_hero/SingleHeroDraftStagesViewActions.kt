package com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.draft_stage_hero

internal sealed class SingleHeroDraftStagesViewActions {

    object SortByStage : SingleHeroDraftStagesViewActions()
    object SortByWinRate : SingleHeroDraftStagesViewActions()
    object SortByMatches : SingleHeroDraftStagesViewActions()
    object SortByPickRate : SingleHeroDraftStagesViewActions()
    class ShowMatches(val key: String) : SingleHeroDraftStagesViewActions()
}