package com.erg.almagestdota.stats.presentation.players.lanes.single_player_lanes

internal sealed class SinglePlayerLanesActions {

    object SortByMatchCount : SinglePlayerLanesActions()
    object SortByPercentage : SinglePlayerLanesActions()
    object SortByName : SinglePlayerLanesActions()
    object OpenPlayer : SinglePlayerLanesActions()
    class ShowMatches(val key: String) : SinglePlayerLanesActions()
}