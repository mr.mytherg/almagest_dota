package com.erg.almagestdota.stats.presentation.coefs

import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.ComplexAdapter
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdota.stats.R
import com.erg.almagestdota.stats.databinding.CoefsFragmentBinding
import com.erg.almagestdota.stats.databinding.DraftStagesFragmentBinding
import com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.DraftStagesSortType
import com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.DraftStagesViewModel
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import com.google.android.material.textview.MaterialTextView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.stats_fragment.*
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
internal class CoefsFragment : Fragment(R.layout.coefs_fragment), BackPressListener {

    private val args by navArgs<CoefsFragmentArgs>()

    companion object {
        const val TAG = "CoefsFragment"
    }

    private val binding by viewBinding(CoefsFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    @Inject
    lateinit var viewModelFactory: CoefsViewModel.Factory
    private val viewModel by viewModels<CoefsViewModel> {
        CoefsViewModel.provideFactory(
            heroId = args.heroId,
            assistedFactory = viewModelFactory
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
    }

    override fun onBackPressed() {
        navController.popBackStack()
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private inner class ViewsConfigurator {
        private val coefTypes = ComplexAdapter(
            complexListener = object : ComplexListener {
                override fun onClick(item: ComplexItem) {
                    viewModel.obtainAction(CoefsActions.SelectCoefType(item.key))
                }
            }
        )

        fun initStartState() = with(binding) {
            tvValue.clicksWithDebounce {
                viewModel.obtainAction(CoefsActions.SortByValue)
            }
            ivBack.clicksWithDebounce {
                navController.popBackStack()
            }
            tvName.clicksWithDebounce {
                viewModel.obtainAction(CoefsActions.SortByHeroName)
            }
        }

        fun renderState(state: CoefsViewState) = with(binding) {
            when (state) {
                is CoefsViewState.DefaultState -> renderDefaultState(state)
            }
        }

        private fun renderDefaultState(state: CoefsViewState.DefaultState) = with(binding) {
            if (rvCoefs.adapter == null) {
                rvCoefs.adapter = coefTypes
                rvCoefs.setHasFixedSize(true)
            }
            coefTypes.submitList(state.coefTypes)
            rvHeroes.adapter = ComplexAdapter().apply {
                submitList(state.heroes)
            }
            tvTitle.text = state.hero.name
            setSortUnselected(tvValue)
            setSortUnselected(tvName)
            when (state.sortType) {
                CoefsSortType.VALUE -> setSortSelected(tvValue)
                CoefsSortType.NAME -> setSortSelected(tvName)
            }
        }

        private fun setSortUnselected(textView: MaterialTextView) {
            textView.setTextColor(ContextCompat.getColor(textView.context, R.color.textColor))
        }

        private fun setSortSelected(textView: MaterialTextView) {
            textView.setTextColor(ContextCompat.getColor(textView.context, R.color.colorPrimary))
        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            navController.navigateViaScreenRoute(screen)
        }
    }
}