package com.erg.almagestdota.stats.presentation.changeable_stats.bands

internal sealed class BandsViewActions {

    class SelectBandType(val key: String) : BandsViewActions()
    object SortByHeroName : BandsViewActions()
    object SortByWinRate : BandsViewActions()
    object SortByMatches : BandsViewActions()
    object SortByPickRate : BandsViewActions()
    class ShowBands(val key: String) : BandsViewActions()
}