package com.erg.almagestdota.stats.presentation.hero_impacts.impact_stuns

enum class ImpactStunsSortType {
    NAME,
    DURATION;
}