package com.erg.almagestdota.stats.presentation.hero_impacts.impact_healing

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.extensions.round1
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.complexList
import com.erg.almagestdota.complexadapter.filterItem
import com.erg.almagestdota.complexadapter.stats1Item
import com.erg.almagestdota.local_storage.external.models.draft.*
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.stats.domain.Interactor
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

internal class ImpactHealingViewModel @AssistedInject constructor(
    private val stringProvider: StringProvider,
    private val interactor: Interactor,
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data ->

    }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    fun obtainAction(action: ImpactHealingViewActions) {
        when (action) {
            is ImpactHealingViewActions.SelectGameStage -> {
                if (stateConfigurator.selectedGameStage == GameStageType.valueOf(action.key)) return
                stateConfigurator.selectedGameStage = GameStageType.valueOf(action.key)
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is ImpactHealingViewActions.SelectRole -> {
                if (stateConfigurator.selectedRole == RoleType.valueOf(action.key)) return
                stateConfigurator.selectedRole = RoleType.valueOf(action.key)
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is ImpactHealingViewActions.SortByOverall -> {
                if (stateConfigurator.selectedSortType == ImpactHealingSortType.OVERALL) return
                stateConfigurator.selectedSortType = ImpactHealingSortType.OVERALL
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is ImpactHealingViewActions.SortByHeroName -> {
                if (stateConfigurator.selectedSortType == ImpactHealingSortType.NAME) return
                stateConfigurator.selectedSortType = ImpactHealingSortType.NAME
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }

        }
    }

    private inner class StateConfigurator {
        var selectedGameStage: GameStageType = GameStageType.LANING
        var selectedRole: RoleType = RoleType.ROLE_1
        var selectedSortType: ImpactHealingSortType = ImpactHealingSortType.OVERALL
        var selectedStageIndex: Int = 0
        var selectedRoleIndex: Int = 0
        private val gameStages = listOf(
            GameStageType.LANING,
            GameStageType.EARLY_GAME,
            GameStageType.MID_GAME,
            GameStageType.LATE_GAME,
        )
        private val roles = listOf(
            RoleType.ROLE_1,
            RoleType.ROLE_2,
            RoleType.ROLE_3,
            RoleType.ROLE_4,
            RoleType.ROLE_5,
        )

        fun defineFragmentState(): ImpactHealingViewState {
            val gameStagesComplex = complexList {
                for (index in gameStages.indices) {
                    val stage = gameStages[index]
                    filterItem(key = stage.name) {
                        title = stringProvider.getString(GameStageType.getId(stage))
                        isSelected = stage == selectedGameStage
                        if (isSelected)
                            selectedStageIndex = index
                    }
                }
            }
            val rolesComplex = complexList {
                for (index in roles.indices) {
                    val role = roles[index]
                    filterItem(key = role.name) {
                        title = stringProvider.getString(RoleType.getId(role))
                        isSelected = role == selectedRole
                        if (isSelected)
                            selectedRoleIndex = index
                    }
                }
            }
            val filteredByRole = GlobalVars.mainSettings.heroes.filter {
                it.roles.isHeroFitsForRole(selectedRole)
            }
            val heroesToShow = when (selectedSortType) {
                ImpactHealingSortType.NAME -> {
                    filteredByRole.sortedBy {
                        it.name
                    }
                }
                ImpactHealingSortType.OVERALL -> {
                    filteredByRole.sortedByDescending {
                        val stage: GameStageData = getStage(it, selectedGameStage, selectedRole)
                        stage.healingAllies
                    }
                }
            }
            val heroes = complexList {
                for (hero in heroesToShow) {
                    val stage: GameStageData = getStage(hero, selectedGameStage, selectedRole)
                    stats1Item(key = hero.id.toString()) {
                        heroName = hero.name
                        url = hero.imageLink
                        value1 = stage.healingAllies.round1().toString()
                        hasBlackout = false
                    }
                }
            }

            return ImpactHealingViewState.DefaultState(
                rolesComplex,
                gameStagesComplex,
                heroes,
                selectedSortType
            )
        }

        private fun getStage(hero: HeroStatsData, selectedGameStage: GameStageType, selectedRole: RoleType): GameStageData {
            val stage = when (selectedGameStage) {
                GameStageType.LANING -> hero.laning
                GameStageType.EARLY_GAME -> hero.early
                GameStageType.MID_GAME -> hero.mid
                GameStageType.LATE_GAME -> hero.late
            }
            return stage.find { it.position == selectedRole } ?: GameStageData.createEmpty().apply {
                position = selectedRole
            }
        }
    }

    companion object {
        fun provideFactory(
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create(
                    ) as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(
        ): ImpactHealingViewModel
    }
}