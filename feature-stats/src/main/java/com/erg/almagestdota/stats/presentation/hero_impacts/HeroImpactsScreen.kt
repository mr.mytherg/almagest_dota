package com.erg.almagestdota.stats.presentation.hero_impacts

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.stats.presentation.StatsFragmentDirections

object HeroImpactsScreen : Screen<NavDirections>(
    route = StatsFragmentDirections.toHeroImpactsFragment(),
    requestKey = HeroImpactsFragment.TAG
)