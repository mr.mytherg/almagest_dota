package com.erg.almagestdota.stats.presentation.hero_impacts

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdota.stats.R
import com.erg.almagestdota.stats.databinding.HeroImpactsFragmentBinding
import com.erg.almagestdota.stats.databinding.StatsFragmentBinding
import com.erg.almagestdota.stats.presentation.changeable_stats.meta.MetaViewModel
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.hero_impacts_fragment.*
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
internal class HeroImpactsFragment : Fragment(R.layout.hero_impacts_fragment), BackPressListener {

    companion object {
        const val TAG = "HeroImpactsFragment"
    }

    private val binding by viewBinding(HeroImpactsFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    @Inject
    lateinit var viewModelFactory: HeroImpactsViewModel.Factory
    private val viewModel by viewModels<HeroImpactsViewModel> {
        HeroImpactsViewModel.provideFactory(
            assistedFactory = viewModelFactory
        )
    }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
        viewModel.viewEvent.setResultListener(this)
    }

    override fun onBackPressed() {
        navController.popBackStack()
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private inner class ViewsConfigurator {

        fun initStartState() = with(binding) {
            ivBack.clicksWithDebounce {
                navController.popBackStack()
            }
            tvDamage.clicksWithDebounce {
                viewModel.obtainAction(HeroImpactsViewActions.OpenDamage)
            }
            tvDisables.clicksWithDebounce {
                viewModel.obtainAction(HeroImpactsViewActions.OpenDisables)
            }
            tvStuns.clicksWithDebounce {
                viewModel.obtainAction(HeroImpactsViewActions.OpenStuns)
            }
            tvCreepsStats.clicksWithDebounce {
                viewModel.obtainAction(HeroImpactsViewActions.OpenCreepStats)
            }
            tvDurability.clicksWithDebounce {
                viewModel.obtainAction(HeroImpactsViewActions.OpenDurability)
            }
            tvAllieHeal.clicksWithDebounce {
                viewModel.obtainAction(HeroImpactsViewActions.OpenAllieHeal)
            }
            tvKda.clicksWithDebounce {
                viewModel.obtainAction(HeroImpactsViewActions.OpenKDA)
            }
        }

        fun renderState(state: HeroImpactsViewState) = with(binding) {
            when (state) {
                is HeroImpactsViewState.DefaultState -> renderDefaultState()
            }
        }

        private fun renderDefaultState() = with(binding) {

        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            viewModel.viewEvent.setResultListener(this@HeroImpactsFragment)
            navController.navigateViaScreenRoute(screen)
        }
    }
}