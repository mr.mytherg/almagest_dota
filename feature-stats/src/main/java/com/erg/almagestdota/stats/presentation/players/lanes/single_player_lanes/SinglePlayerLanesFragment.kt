package com.erg.almagestdota.stats.presentation.players.lanes.single_player_lanes

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.ComplexAdapter
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdota.stats.R
import com.erg.almagestdota.stats.databinding.SinglePlayerLanesFragmentBinding
import com.erg.almagestdota.stats.presentation.players.lanes.PlayersLanesSortType
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import com.google.android.material.textview.MaterialTextView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
internal class SinglePlayerLanesFragment : Fragment(R.layout.single_player_lanes_fragment), BackPressListener {

    private val args by navArgs<SinglePlayerLanesFragmentArgs>()

    companion object {
        const val TAG = "SinglePlayerLanesFragment"
    }

    private val binding by viewBinding(SinglePlayerLanesFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    @Inject
    lateinit var viewModelFactory: SinglePlayerLanesViewModel.Factory
    private val viewModel by viewModels<SinglePlayerLanesViewModel> {
        SinglePlayerLanesViewModel.provideFactory(
           args.playerId,
            assistedFactory = viewModelFactory
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
    }

    override fun onBackPressed() {
        navController.popBackStack()
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private inner class ViewsConfigurator {
        fun initStartState() = with(binding) {
            tvMatchCount.clicksWithDebounce {
                viewModel.obtainAction(SinglePlayerLanesActions.SortByMatchCount)
            }
            ivBack.clicksWithDebounce {
                navController.popBackStack()
            }
            tvName.clicksWithDebounce {
                viewModel.obtainAction(SinglePlayerLanesActions.SortByName)
            }
            tvMatchPercentage.clicksWithDebounce {
                viewModel.obtainAction(SinglePlayerLanesActions.SortByPercentage)
            }
            tvOpenPlayer.clicksWithDebounce {
                viewModel.obtainAction(SinglePlayerLanesActions.OpenPlayer)
            }
        }

        fun renderState(state: SinglePlayerLanesViewState) = with(binding) {
            when (state) {
                is SinglePlayerLanesViewState.DefaultState -> renderDefaultState(state)
            }
        }

        private fun renderDefaultState(state: SinglePlayerLanesViewState.DefaultState) = with(binding) {
            tvTitle.text = state.playerName
            rvHeroes.adapter = ComplexAdapter(object : ComplexListener {
                override fun onClick(item: ComplexItem) {
                    viewModel.obtainAction(SinglePlayerLanesActions.ShowMatches(item.key))
                }
            }).apply {
                submitList(state.lanes)
            }
            setSortUnselected(tvMatchPercentage)
            setSortUnselected(tvName)
            setSortUnselected(tvMatchCount)
            when (state.sortType) {
                PlayersLanesSortType.OVERALL -> setSortSelected(tvMatchPercentage)
                PlayersLanesSortType.NAME -> setSortSelected(tvName)
                PlayersLanesSortType.MATCH_COUNT -> setSortSelected(tvMatchCount)
            }
        }

        private fun setSortUnselected(textView: MaterialTextView) {
            textView.setTextColor(ContextCompat.getColor(textView.context, R.color.textColor))
        }

        private fun setSortSelected(textView: MaterialTextView) {
            textView.setTextColor(ContextCompat.getColor(textView.context, R.color.colorPrimary))
        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            navController.navigateViaScreenRoute(screen)
        }
    }
}