package com.erg.almagestdota.stats.presentation.players.lanes

enum class PlayersLanesSortType {
    NAME,
    OVERALL,
    MATCH_COUNT;
}