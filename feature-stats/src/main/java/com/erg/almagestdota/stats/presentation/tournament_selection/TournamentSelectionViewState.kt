package com.erg.almagestdota.stats.presentation.tournament_selection

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.DraftStagesSortType

internal sealed class TournamentSelectionViewState : ViewState() {
    class DefaultState(
        val tournaments: List<ComplexItem>,
    ) : TournamentSelectionViewState()
    object Retry: TournamentSelectionViewState()
}