package com.erg.almagestdota.stats.presentation.changeable_stats.bands

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.DraftStagesSortType

internal sealed class BandsViewState : ViewState() {
    class DefaultState(
        val bands: List<ComplexItem>,
        val heroes: List<ComplexItem>,
        val sortType: DraftStagesSortType
    ) : BandsViewState()
}