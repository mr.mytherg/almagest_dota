package com.erg.almagestdota.stats.presentation.hero_impacts

internal sealed class HeroImpactsViewActions {

    object OpenCreepStats : HeroImpactsViewActions()
    object OpenDamage : HeroImpactsViewActions()
    object OpenDurability : HeroImpactsViewActions()
    object OpenAllieHeal : HeroImpactsViewActions()
    object OpenKDA : HeroImpactsViewActions()
    object OpenDisables : HeroImpactsViewActions()
    object OpenStuns : HeroImpactsViewActions()
}