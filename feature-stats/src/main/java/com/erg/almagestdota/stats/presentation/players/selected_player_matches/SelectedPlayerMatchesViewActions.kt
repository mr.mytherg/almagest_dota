package com.erg.almagestdota.stats.presentation.players.selected_player_matches

internal sealed class SelectedPlayerMatchesViewActions {

    object OnBackClick : SelectedPlayerMatchesViewActions()
    class OpenDraft(val key: String) : SelectedPlayerMatchesViewActions()
}