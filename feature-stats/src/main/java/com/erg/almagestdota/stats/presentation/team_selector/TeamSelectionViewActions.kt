package com.erg.almagestdota.stats.presentation.team_selector

internal sealed class TeamSelectionViewActions {

    object OnBackClick : TeamSelectionViewActions()
    object ClearSearch : TeamSelectionViewActions()
    class FilterByName(val query: String) : TeamSelectionViewActions()
    class SelectTeam(val key: String) : TeamSelectionViewActions()
}