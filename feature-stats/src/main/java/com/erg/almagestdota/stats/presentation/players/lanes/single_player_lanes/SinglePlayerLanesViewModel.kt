package com.erg.almagestdota.stats.presentation.players.lanes.single_player_lanes

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.NotificationStatus
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.complexadapter.*
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.main.LaneOutcomeType
import com.erg.almagestdota.local_storage.external.models.draft.main.LaneStatusType
import com.erg.almagestdota.local_storage.external.models.draft.main.MemberInfo
import com.erg.almagestdota.local_storage.external.models.draft.main.PlayerInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.settings.domain.use_cases.GetSinglePlayerLanesUseCase
import com.erg.almagestdota.stats.R
import com.erg.almagestdota.stats.domain.Interactor
import com.erg.almagestdota.stats.presentation.players.lanes.PlayersLanesSortType
import com.erg.almagestdota.stats.presentation.players.open_player.OpenPlayerScreen
import com.erg.almagestdota.stats.presentation.players.selected_player_matches.SelectedPlayerMatchesScreen
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

internal class SinglePlayerLanesViewModel @AssistedInject constructor(
    @Assisted private val playerId: Long,
    private val stringProvider: StringProvider,
    private val interactor: Interactor,
    private val localStorage: ILocalStorageContract,
    private val getSinglePlayerLanesUseCase: GetSinglePlayerLanesUseCase,

    ) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data -> }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    fun obtainAction(action: SinglePlayerLanesActions) {
        when (action) {
            is SinglePlayerLanesActions.SortByMatchCount -> {
                if (stateConfigurator.selectedSortType == PlayersLanesSortType.MATCH_COUNT) return
                stateConfigurator.selectedSortType = PlayersLanesSortType.MATCH_COUNT
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is SinglePlayerLanesActions.OpenPlayer -> {
                val screen = OpenPlayerScreen(playerId)
                emit(viewEventMutable, ViewEvent.Navigation(screen))
            }
            is SinglePlayerLanesActions.ShowMatches -> {
                val laneOutcomeType = LaneOutcomeType.valueOf(action.key)

                localStorage.matchesToShow = localStorage.selectedMatches.filter {
                    val isRadiant = it.radiantHeroes.find { it.playerId == playerId } != null
                    val isDire = it.direHeroes.find { it.playerId == playerId } != null
                    if (!isRadiant  && !isDire)
                        return@filter false
                    val laneStatusType = when (laneOutcomeType) {
                        LaneOutcomeType.WIN_PLUS -> if (isRadiant) LaneStatusType.RADIANT_STOMP else LaneStatusType.DIRE_STOMP
                        LaneOutcomeType.WIN -> if (isRadiant) LaneStatusType.RADIANT_WIN else LaneStatusType.DIRE_WIN
                        LaneOutcomeType.TIE -> LaneStatusType.TIE
                        LaneOutcomeType.LOSE -> if (isRadiant) LaneStatusType.DIRE_WIN else LaneStatusType.RADIANT_WIN
                        LaneOutcomeType.LOSE_MINUS -> if (isRadiant) LaneStatusType.DIRE_STOMP else LaneStatusType.RADIANT_STOMP
                        LaneOutcomeType.UNDEFINED -> LaneStatusType.UNDEFINED
                    }
                    isMatchRelevant(it.radiantHeroes, playerId, laneStatusType) ||
                        isMatchRelevant(it.direHeroes, playerId, laneStatusType)

                }
                if (localStorage.matchesToShow.isNotEmpty()) {
                    val screen = SelectedPlayerMatchesScreen(
                        SinglePlayerLanesFragmentDirections.toSelectedPlayerMatchesFragment(playerId)
                    )
                    emit(viewEventMutable, ViewEvent.Navigation(screen))
                } else {
                    val text = stringProvider.getString(R.string.no_matches)
                    emit(viewEventMutable, ViewEvent.AlmagestSnackbar(text, NotificationStatus.ERROR))
                }
            }
            is SinglePlayerLanesActions.SortByName -> {
                if (stateConfigurator.selectedSortType == PlayersLanesSortType.NAME) return
                stateConfigurator.selectedSortType = PlayersLanesSortType.NAME
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is SinglePlayerLanesActions.SortByPercentage -> {
                if (stateConfigurator.selectedSortType == PlayersLanesSortType.OVERALL) return
                stateConfigurator.selectedSortType = PlayersLanesSortType.OVERALL
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
        }
    }

    private fun isMatchRelevant(heroes: List<PlayerInfo>, playerId: Long, laneStatusType: LaneStatusType): Boolean {
        val player = heroes.find { playerId == it.playerId }
        return if (player != null) {
            player.laneStatus == laneStatusType
        } else {
            false
        }
    }

    private inner class StateConfigurator {
        var selectedSortType: PlayersLanesSortType = PlayersLanesSortType.MATCH_COUNT

        val heroLanesStatus = getSinglePlayerLanesUseCase.invoke(localStorage.selectedMatches, playerId)


        fun defineFragmentState(): SinglePlayerLanesViewState {
            val statusesSorted = when (selectedSortType) {
                PlayersLanesSortType.NAME -> {
                    heroLanesStatus.second.sortedBy { it.statusType.number }
                }
                PlayersLanesSortType.MATCH_COUNT -> {
                    heroLanesStatus.second.sortedByDescending { it.count }
                }
                PlayersLanesSortType.OVERALL -> {
                    heroLanesStatus.second.sortedByDescending { it.matchPercentage }
                }
            }
            val player = GlobalVars.players[playerId] ?: MemberInfo(playerId)
            val stagesComplex = complexList {
                for (status in statusesSorted) {
                    stats2Item(key = status.statusType.name) {
                        heroName = stringProvider.getString(LaneOutcomeType.getId(status.statusType))
                        url = ""
                        hasImage = false
                        value1 = status.count.toString()
                        value2 = stringProvider.getString(R.string.percentage,status.matchPercentage.toString())
                    }
                }
            }

            return SinglePlayerLanesViewState.DefaultState(
                player.name.ifEmpty { playerId.toString() },
                stagesComplex,
                selectedSortType
            )
        }
    }

    companion object {
        fun provideFactory(
            playerId: Long,
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create(
                        playerId
                    ) as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(
            playerId: Long,
        ): SinglePlayerLanesViewModel
    }
}