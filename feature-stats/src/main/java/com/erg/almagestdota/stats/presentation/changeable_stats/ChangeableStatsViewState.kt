package com.erg.almagestdota.stats.presentation.changeable_stats

import androidx.constraintlayout.motion.utils.ViewState

internal sealed class ChangeableStatsViewState : ViewState() {
    class DefaultState(
        val isTeam: Boolean,
    ) : ChangeableStatsViewState()
}