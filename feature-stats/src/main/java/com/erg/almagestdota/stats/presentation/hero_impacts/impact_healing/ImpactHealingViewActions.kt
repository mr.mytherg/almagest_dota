package com.erg.almagestdota.stats.presentation.hero_impacts.impact_healing

internal sealed class ImpactHealingViewActions {

    class SelectRole(val key: String) : ImpactHealingViewActions()
    class SelectGameStage(val key: String) : ImpactHealingViewActions()
    object SortByHeroName : ImpactHealingViewActions()
    object SortByOverall : ImpactHealingViewActions()
}