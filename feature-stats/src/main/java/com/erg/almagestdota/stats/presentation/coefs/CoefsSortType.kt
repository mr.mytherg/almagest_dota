package com.erg.almagestdota.stats.presentation.coefs

enum class CoefsSortType {
    NAME,
    VALUE;
}