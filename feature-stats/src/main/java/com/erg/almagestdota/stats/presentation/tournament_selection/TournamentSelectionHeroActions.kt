package com.erg.almagestdota.stats.presentation.tournament_selection

internal sealed class TournamentSelectionHeroActions {

    object CheckShady : TournamentSelectionHeroActions()
    object Submit : TournamentSelectionHeroActions()
    object Retry : TournamentSelectionHeroActions()
    object LoadMore : TournamentSelectionHeroActions()
    class SelectTournament(val key: String) : TournamentSelectionHeroActions()
}