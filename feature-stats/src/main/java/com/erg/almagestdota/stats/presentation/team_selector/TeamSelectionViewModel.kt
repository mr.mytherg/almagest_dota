package com.erg.almagestdota.stats.presentation.team_selector

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.complexadapter.complexList
import com.erg.almagestdota.complexadapter.itemDefault
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

internal class TeamSelectionViewModel @AssistedInject constructor(
    private val stringProvider: StringProvider,
    private val localStorage: ILocalStorageContract
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data -> }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    fun obtainAction(action: TeamSelectionViewActions) {
        when (action) {
            is TeamSelectionViewActions.FilterByName -> {
                stateConfigurator.query = action.query
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is TeamSelectionViewActions.ClearSearch -> {
                stateConfigurator.query = ""
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is TeamSelectionViewActions.OnBackClick -> {
                emit(viewEventMutable, TeamSelectionViewEvents.BackClickEvent)
            }
            is TeamSelectionViewActions.SelectTeam -> {
                emit(viewEventMutable, TeamSelectionViewEvents.ResultEvent(action.key.toLong()))
            }
        }
    }

    private inner class StateConfigurator {
        var query = ""

        private var teams: List<Pair<Long, TeamInfo?>>

        init {
            val set = mutableSetOf<Long>()
            for (match in localStorage.metaMatches) {
                set.add(match.radiantTeamId)
                set.add(match.direTeamId)
            }
            teams = set.map {
                if (GlobalVars.teams[it] == null)
                    Log.e("TRASHCODE", it.toString())
                it to GlobalVars.teams[it]
            }.sortedByDescending { it.second?.rating }
        }

        fun defineFragmentState(): TeamSelectionViewState {
            val teams = teams.filter { it.second?.name?.contains(query, true) ?: false }
            return TeamSelectionViewState.DefaultState(
                teams = complexList {
                    for (team in teams) {
                        itemDefault(key = team.first.toString()) {
                            title = team.second?.name.orDefault()
                            url = null
                            hasPlaceholder = true
                            hasBlackout = false
                        }
                    }
                },
            )
        }
    }

    companion object {
        fun provideFactory(
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create(
                    ) as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(
        ): TeamSelectionViewModel
    }
}