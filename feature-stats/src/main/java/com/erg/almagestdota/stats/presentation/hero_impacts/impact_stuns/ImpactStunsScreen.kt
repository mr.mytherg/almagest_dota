package com.erg.almagestdota.stats.presentation.hero_impacts.impact_stuns

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.stats.presentation.hero_impacts.HeroImpactsFragmentDirections

object ImpactStunsScreen : Screen<NavDirections>(
    route = HeroImpactsFragmentDirections.toImpactStunsFragment(),
    requestKey = ImpactStunsFragment.TAG
)