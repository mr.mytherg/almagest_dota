package com.erg.almagestdota.stats.presentation.changeable_stats.open_match

import androidx.constraintlayout.motion.utils.ViewState

internal sealed class OpenMatchViewState : ViewState() {
    object DefaultState : OpenMatchViewState()
}