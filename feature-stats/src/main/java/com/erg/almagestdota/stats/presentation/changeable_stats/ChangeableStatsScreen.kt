package com.erg.almagestdota.stats.presentation.changeable_stats

import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen


sealed class ChangeableStatsScreen(
    route: Any,
    requestKey: String
) : Screen<Any>(route, requestKey) {
    class ChangeableStatsScreenDirection(
        route: NavDirections
    ) : ChangeableStatsScreen(
        route = route,
        requestKey = ChangeableStatsFragment.TAG
    )
    class ChangeableStatsScreenDeeplink(
        teamId: Long,
    ) : ChangeableStatsScreen(
        route = "com.erg.almagestdota://stats/changeableStats?teamId=$teamId",
        requestKey = ChangeableStatsFragment.TAG
    )
}

