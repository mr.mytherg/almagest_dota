package com.erg.almagestdota.stats.domain

import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.local_storage.external.models.draft.FactionData
import com.erg.almagestdota.local_storage.external.models.draft.FactionType
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import javax.inject.Inject

class UpdateFactionsDataUseCase @Inject constructor(
    private val localStorage: ILocalStorageContract,
) {

    operator fun invoke(matches: List<DotaMatch>, teamId: Long): Map<Int, List<FactionData>> {
        val mutableMap = mutableMapOf<Int, List<FactionData>>()
        for (hero in GlobalVars.mainSettings.heroes) {
            mutableMap[hero.id] = listOf<FactionData>(
                FactionData(factionType = FactionType.OVERALL, 0.0, 0.0, 0),
                FactionData(factionType = FactionType.RADIANT, 0.0, 0.0, 0),
                FactionData(factionType = FactionType.DIRE, 0.0, 0.0, 0),
            )
        }
        for (match in matches) {
            if (teamId == 0L || match.radiantTeamId == teamId)
                for (heroPlayer in match.radiantHeroes) {
                    mutableMap[heroPlayer.heroId]?.apply {
                        get(1).apply {
                            pickCount++
                            if (match.hasRadiantWon)
                                winCount++
                        }
                    }
                }
            if (teamId == 0L || match.direTeamId == teamId)
                for (heroPlayer in match.direHeroes) {
                    mutableMap[heroPlayer.heroId]?.apply {
                        get(2).apply {
                            pickCount++
                            if (!match.hasRadiantWon)
                                winCount++
                        }
                    }
                }
        }
        val resultMap = mutableMapOf<Int, List<FactionData>>()
        for ((heroId, factions) in mutableMap) {
            val pickCount = factions[1].pickCount + factions[2].pickCount
            val winCount = factions[1].winCount + factions[2].winCount
            factions[0].apply {
                matchCount = pickCount
                winRate = getPercentage(winCount, pickCount)
                pickRate = getPercentage(pickCount, matches.size)
            }
            factions[1].apply {
                matchCount = this.pickCount
                winRate = getPercentage(this.winCount, this.pickCount)
                pickRate = getPercentage(this.pickCount, pickCount)
            }
            factions[2].apply {
                matchCount = this.pickCount
                winRate = getPercentage(this.winCount, this.pickCount)
                pickRate = getPercentage(this.pickCount, pickCount)
            }
            resultMap[heroId] = factions

        }
        return resultMap
    }

    private fun getPercentage(param1: Int, param2: Int) =
        if (param2 == 0) 0.0 else (param1 * 1.0 / (param2) * 100).round2()
}