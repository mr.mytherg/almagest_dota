package com.erg.almagestdota.stats.presentation.team_selector

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.stats.presentation.StatsFragmentDirections

class TeamSelectionScreen(
     isRadiant: Boolean
) : Screen<String>(
    route = "com.erg.almagestdota://team_selector",
    requestKey = TAG
) {
    init {
        Companion.isRadiant = isRadiant
    }

    companion object {
        var isRadiant: Boolean = false
        const val TAG = "TeamSelectionScreen"
        private const val RESULT_KEY = "RESULT_KEY"
        private const val DATA_KEY = "DATA_KEY"
        private const val SIDE_KEY = "SIDE_KEY"

        fun getResult(data: Bundle): Result = Result.enumValueOf(data.getString(RESULT_KEY))
        fun getSelectedTeam(data: Bundle): Long = data.getLong(DATA_KEY)
        fun isRadiant(data: Bundle): Boolean = data.getBoolean(SIDE_KEY)
    }

    enum class Result {
        SELECTED,
        CANCELED;

        companion object {
            fun enumValueOf(value: String?): Result {
                return values().firstOrNull { it.name == value } ?: CANCELED
            }

            internal fun createResult(teamId: Long? = null, isRadiant: Boolean): Bundle {
                return if (teamId == null) {
                    bundleOf(RESULT_KEY to CANCELED.name)
                } else {
                    bundleOf(RESULT_KEY to SELECTED.name, DATA_KEY to teamId, SIDE_KEY to isRadiant)
                }
            }
        }
    }
}