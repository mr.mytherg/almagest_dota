package com.erg.almagestdota.stats.presentation.hero_impacts.impact_stuns

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem

internal sealed class ImpactStunsViewState : ViewState() {
    class DefaultState(
        val roles: List<ComplexItem>,
        val gameStages: List<ComplexItem>,
        val heroes: List<ComplexItem>,
        val sortType: ImpactStunsSortType
    ) : ImpactStunsViewState()
}