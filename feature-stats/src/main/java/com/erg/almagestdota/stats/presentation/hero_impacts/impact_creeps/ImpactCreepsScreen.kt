package com.erg.almagestdota.stats.presentation.hero_impacts.impact_creeps

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.stats.presentation.hero_impacts.HeroImpactsFragmentDirections

object ImpactCreepsScreen : Screen<NavDirections>(
    route = HeroImpactsFragmentDirections.toImpactCreepsFragment(),
    requestKey = ImpactCreepsFragment.TAG
)