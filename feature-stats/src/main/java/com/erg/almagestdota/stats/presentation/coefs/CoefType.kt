package com.erg.almagestdota.stats.presentation.coefs

import androidx.annotation.StringRes
import com.erg.almagestdota.local_storage.R
import com.erg.almagestdota.local_storage.external.models.draft.StageType

enum class CoefType {
    SYNERGY,
    COUNTER,
    STEAL;

    companion object {

        @StringRes
        fun getId(stage: CoefType): Int {
            return when (stage) {
                CoefType.SYNERGY -> R.string.filter_synergy
                CoefType.COUNTER -> R.string.filter_counter
                CoefType.STEAL -> R.string.filter_can_be_stolen
            }
        }
    }
}