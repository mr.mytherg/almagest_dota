package com.erg.almagestdota.stats.presentation.players.lanes

internal sealed class PlayersLanesViewActions {

    class SelectRole(val key: String) : PlayersLanesViewActions()
    object SortByPlayerName : PlayersLanesViewActions()
    object SortByOverall : PlayersLanesViewActions()
    object SortByMatchCount : PlayersLanesViewActions()
    class OpenPlayer(val key: String) : PlayersLanesViewActions()
}