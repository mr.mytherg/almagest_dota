package com.erg.almagestdota.stats.presentation

import androidx.constraintlayout.motion.utils.ViewState

internal sealed class StatsViewState : ViewState() {
    object DefaultState : StatsViewState()
}