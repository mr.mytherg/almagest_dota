package com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages

internal sealed class DraftStagesViewActions {

    class SelectDraftStage(val key: String) : DraftStagesViewActions()
    class SelectHero(val key: String) : DraftStagesViewActions()
    object SortByHeroName : DraftStagesViewActions()
    object SortByWinRate : DraftStagesViewActions()
    object SortByMatches : DraftStagesViewActions()
    object SortByPickRate : DraftStagesViewActions()
}