package com.erg.almagestdota.stats.presentation.changeable_stats.hero_roles.hero_roles

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.DraftStagesSortType

internal sealed class HeroRolesViewState : ViewState() {
    class DefaultState(
        val roles: List<ComplexItem>,
        val heroes: List<ComplexItem>,
        val stageIndex: Int,
        val sortType: DraftStagesSortType
    ) : HeroRolesViewState()
}