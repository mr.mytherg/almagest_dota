package com.erg.almagestdota.stats.presentation.players.lanes.single_player_lanes

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.DraftStagesSortType
import com.erg.almagestdota.stats.presentation.players.lanes.PlayersLanesSortType

internal sealed class SinglePlayerLanesViewState : ViewState() {
    class DefaultState(
        val playerName: String,
        val lanes: List<ComplexItem>,
        val sortType: PlayersLanesSortType
    ) : SinglePlayerLanesViewState()
}