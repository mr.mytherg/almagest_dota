package com.erg.almagestdota.stats.presentation.changeable_stats.meta

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.DraftStagesSortType

internal sealed class MetaViewState : ViewState() {
    class DefaultState(
        val factions: List<ComplexItem>,
        val heroes: List<ComplexItem>,
        val sortType: DraftStagesSortType
    ) : MetaViewState()
}