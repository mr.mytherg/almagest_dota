package com.erg.almagestdota.stats.presentation.hero_impacts.impact_durability

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.stats.presentation.hero_impacts.HeroImpactsFragmentDirections

object ImpactDurabilityScreen : Screen<NavDirections>(
    route = HeroImpactsFragmentDirections.toImpactDurabilityFragment(),
    requestKey = ImpactDurabilityFragment.TAG
)