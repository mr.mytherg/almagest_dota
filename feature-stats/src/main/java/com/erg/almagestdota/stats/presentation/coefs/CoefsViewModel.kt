package com.erg.almagestdota.stats.presentation.coefs

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.round1
import com.erg.almagestdota.base.external.extensions.round2
import com.erg.almagestdota.base.external.extensions.save2DigitsAfterDot
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.complexadapter.complexList
import com.erg.almagestdota.complexadapter.draftStageItem
import com.erg.almagestdota.complexadapter.filterItem
import com.erg.almagestdota.complexadapter.statsCoefItem
import com.erg.almagestdota.local_storage.external.models.draft.StageType
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.stats.domain.Interactor
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow


internal class CoefsViewModel @AssistedInject constructor(
    @Assisted private val heroId: Int,
    private val stringProvider: StringProvider,
    private val interactor: Interactor,
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data -> }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    fun obtainAction(action: CoefsActions) {
        when (action) {
            is CoefsActions.SelectCoefType -> {
                if (stateConfigurator.selectedCoefType == CoefType.valueOf(action.key)) return
                stateConfigurator.selectedCoefType = CoefType.valueOf(action.key)
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is CoefsActions.SortByValue -> {
                if (stateConfigurator.selectedSortType == CoefsSortType.VALUE) return
                stateConfigurator.selectedSortType = CoefsSortType.VALUE
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is CoefsActions.SortByHeroName -> {
                if (stateConfigurator.selectedSortType == CoefsSortType.NAME) return
                stateConfigurator.selectedSortType = CoefsSortType.NAME
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
        }
    }

    private inner class StateConfigurator {
        var selectedCoefType: CoefType = CoefType.COUNTER
        var selectedSortType: CoefsSortType = CoefsSortType.VALUE
        var selectedStageIndex: Int = 0
        private val coefTypes = listOf(
            CoefType.COUNTER,
            CoefType.SYNERGY,
            CoefType.STEAL,
        )

        fun defineFragmentState(): CoefsViewState {
            val draftStagesComplex = complexList {
                for (index in coefTypes.indices) {
                    val stage = coefTypes[index]
                    filterItem(key = stage.name) {
                        title = stringProvider.getString(CoefType.getId(stage))
                        isSelected = stage == selectedCoefType
                        if (isSelected)
                            selectedStageIndex = index
                    }
                }
            }
            val heroesToShow: MutableList<HeroValueModel> = mutableListOf()
            for (hero in GlobalVars.mainSettings.heroes) {
                if (hero.id == heroId) continue
                heroesToShow.add(
                    HeroValueModel(
                        hero,
                        when (selectedCoefType) {
                            CoefType.COUNTER -> {
                                ((GlobalVars.importantSettings.heroesProValues[heroId]!!.counters[hero.id]!! -
                                    GlobalVars.importantSettings.heroesProValues[hero.id]!!.counters[heroId]!!) / 2).round2()
                            }
                            CoefType.STEAL -> {
                                ((GlobalVars.importantSettings.heroesProValues[hero.id]!!.counters[heroId]!! - GlobalVars.importantSettings.heroesProValues[heroId]!!.counters[hero.id]!!) / 2 +
                                    GlobalVars.importantSettings.heroesProValues[heroId]!!.synergies[hero.id]!!).round2()
                            }
                            CoefType.SYNERGY -> {
                                GlobalVars.importantSettings.heroesProValues[heroId]!!.synergies[hero.id]!!
                            }
                        }
                    )
                )
            }
            val heroesSorted = when (selectedSortType) {
                CoefsSortType.NAME -> {
                    heroesToShow.sortedBy { it.hero.name }
                }
                CoefsSortType.VALUE -> {
                    heroesToShow.sortedByDescending { it.value }
                }
            }
            val heroes = complexList {
                for (model in heroesSorted) {
                    statsCoefItem(key = model.hero.id.toString()) {
                        heroName = model.hero.name
                        url = model.hero.imageLink
                        value = model.value.toString().save2DigitsAfterDot()
                    }
                }
            }

            return CoefsViewState.DefaultState(
                GlobalVars.heroesAsMap[heroId]!!,
                draftStagesComplex,
                heroes,
                selectedSortType
            )
        }
    }

    companion object {
        fun provideFactory(
            heroId: Int,
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create(
                        heroId
                    ) as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(
            heroId: Int
        ): CoefsViewModel
    }
}