package com.erg.almagestdota.stats.presentation.changeable_stats

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.stats.presentation.changeable_stats.bands.BandsScreen
import com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.DraftStagesScreen
import com.erg.almagestdota.stats.presentation.changeable_stats.hero_roles.hero_roles.HeroRolesScreen
import com.erg.almagestdota.stats.presentation.changeable_stats.hero_roles.hero_roles_enemies.HeroRolesEnemiesScreen
import com.erg.almagestdota.stats.presentation.changeable_stats.meta.MetaScreen
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

internal class ChangeableStatsViewModel @AssistedInject constructor(
    @Assisted private val teamId: Long,
    private val stringProvider: StringProvider,
    private val localStorage: ILocalStorageContract,
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data ->
    }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow<ChangeableStatsViewState?>(null)
    val viewState = viewStateMutable.asStateFlow()

    private val loadingStateMutable = MutableStateFlow<Loading>(Loading.Disabled)
    val loadingState = loadingStateMutable.asStateFlow()

    init {
        viewStateMutable.value = stateConfigurator.defineFragmentState()
    }

    fun obtainAction(action: ChangeableStatsViewActions) {
        when (action) {
            is ChangeableStatsViewActions.OpenDraftStages -> {
                localStorage.selectedMatches = stateConfigurator.matches
                emit(viewEventMutable, ViewEvent.Navigation(DraftStagesScreen(teamId)))
            }
            is ChangeableStatsViewActions.OpenHeroesRoles -> {
                localStorage.selectedMatches = stateConfigurator.matches
                emit(viewEventMutable, ViewEvent.Navigation(HeroRolesScreen(teamId)))
            }
            is ChangeableStatsViewActions.OpenMeta -> {
                localStorage.selectedMatches = stateConfigurator.matches
                emit(viewEventMutable, ViewEvent.Navigation(MetaScreen(teamId)))
            }
            is ChangeableStatsViewActions.OpenBands -> {
                localStorage.selectedMatches = stateConfigurator.matches
                emit(viewEventMutable, ViewEvent.Navigation(BandsScreen(teamId)))
            }
            is ChangeableStatsViewActions.OpenHeroesRolesEnemies -> {
                localStorage.selectedMatches = stateConfigurator.matches
                emit(viewEventMutable, ViewEvent.Navigation(HeroRolesEnemiesScreen(teamId)))
            }
        }
    }

    private inner class StateConfigurator {
        val matches: List<DotaMatch> = if (teamId == 0L) {
            localStorage.metaMatches
        } else {
            localStorage.metaMatches.filter {
                it.radiantTeamId == teamId ||
                it.direTeamId == teamId
            }
        }

        fun defineFragmentState(): ChangeableStatsViewState {
            return ChangeableStatsViewState.DefaultState(
                teamId != 0L
            )
        }
    }

    companion object {
        fun provideFactory(
            teamId: Long,
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create(
                        teamId
                    ) as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(
            teamId: Long
        ): ChangeableStatsViewModel
    }
}