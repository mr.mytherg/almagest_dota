package com.erg.almagestdota.stats.presentation.coefs

import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData

data class HeroValueModel(
    val hero: HeroCoreData,
    val value: Double
)
