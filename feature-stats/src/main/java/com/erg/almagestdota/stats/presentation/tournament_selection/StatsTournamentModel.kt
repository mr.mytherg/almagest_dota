package com.erg.almagestdota.stats.presentation.tournament_selection

import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch

data class StatsTournamentModel(
    val id: Long,
    val matches: List<DotaMatch>,
    var isSelected: Boolean
)
