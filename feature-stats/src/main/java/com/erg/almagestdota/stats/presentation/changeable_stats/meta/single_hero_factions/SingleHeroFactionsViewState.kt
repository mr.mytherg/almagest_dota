package com.erg.almagestdota.stats.presentation.changeable_stats.meta.single_hero_factions

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.stats.presentation.changeable_stats.draft_stages.DraftStagesSortType

internal sealed class SingleHeroFactionsViewState : ViewState() {
    class DefaultState(
        val heroName: String,
        val heroes: List<ComplexItem>,
        val sortType: DraftStagesSortType
    ) : SingleHeroFactionsViewState()
}