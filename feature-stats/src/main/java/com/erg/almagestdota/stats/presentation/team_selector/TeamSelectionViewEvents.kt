package com.erg.almagestdota.stats.presentation.team_selector

import com.erg.almagestdota.base.external.viewEvent.ViewEvent

internal sealed class TeamSelectionViewEvents : ViewEvent() {
    object BackClickEvent : TeamSelectionViewEvents()
    class ResultEvent(val teamId: Long) : TeamSelectionViewEvents()
}