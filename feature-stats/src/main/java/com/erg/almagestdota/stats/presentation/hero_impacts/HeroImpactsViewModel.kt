package com.erg.almagestdota.stats.presentation.hero_impacts

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.heroselector.presentation.heroSelector.HeroSelectorScreen
import com.erg.almagestdota.stats.domain.Interactor
import com.erg.almagestdota.stats.presentation.coefs.CoefsScreen
import com.erg.almagestdota.stats.presentation.hero_impacts.impact_creeps.ImpactCreepsScreen
import com.erg.almagestdota.stats.presentation.hero_impacts.impact_damage.ImpactDamageScreen
import com.erg.almagestdota.stats.presentation.hero_impacts.impact_disables.ImpactDisablesScreen
import com.erg.almagestdota.stats.presentation.hero_impacts.impact_durability.ImpactDurabilityScreen
import com.erg.almagestdota.stats.presentation.hero_impacts.impact_healing.ImpactHealingScreen
import com.erg.almagestdota.stats.presentation.hero_impacts.impact_kda.ImpactKDAScreen
import com.erg.almagestdota.stats.presentation.hero_impacts.impact_stuns.ImpactStunsScreen
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

internal class HeroImpactsViewModel @AssistedInject constructor(
    private val stringProvider: StringProvider,
    private val interactor: Interactor,
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data ->
        when (resultKey) {
            HeroSelectorScreen.TAG -> {
                if (HeroSelectorScreen.getResult(data) == HeroSelectorScreen.Result.SELECTED) {
                    val heroId = HeroSelectorScreen.getSelectedHero(data)
                    openStatsCoef(heroId)
                }
            }
        }
    }

    private fun openStatsCoef(heroId: Int) {
        emit(viewEventMutable, ViewEvent.Navigation(CoefsScreen(heroId)))
    }

    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    fun obtainAction(action: HeroImpactsViewActions) {
        when (action) {
            is HeroImpactsViewActions.OpenCreepStats -> {
                emit(viewEventMutable, ViewEvent.Navigation(ImpactCreepsScreen))
            }
            is HeroImpactsViewActions.OpenKDA -> {
                emit(viewEventMutable, ViewEvent.Navigation(ImpactKDAScreen))
            }
            is HeroImpactsViewActions.OpenAllieHeal -> {
                emit(viewEventMutable, ViewEvent.Navigation(ImpactHealingScreen))
            }
            is HeroImpactsViewActions.OpenDamage -> {
                emit(viewEventMutable, ViewEvent.Navigation(ImpactDamageScreen))
            }
            is HeroImpactsViewActions.OpenDurability -> {
                emit(viewEventMutable, ViewEvent.Navigation(ImpactDurabilityScreen))
            }
            is HeroImpactsViewActions.OpenStuns -> {
                emit(viewEventMutable, ViewEvent.Navigation(ImpactStunsScreen))
            }
            is HeroImpactsViewActions.OpenDisables -> {
                emit(viewEventMutable, ViewEvent.Navigation(ImpactDisablesScreen))
            }
        }
    }

    private inner class StateConfigurator {

        fun defineFragmentState(): HeroImpactsViewState {
            return HeroImpactsViewState.DefaultState
        }
    }

    companion object {
        fun provideFactory(
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create(
                    ) as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(
        ): HeroImpactsViewModel
    }
}