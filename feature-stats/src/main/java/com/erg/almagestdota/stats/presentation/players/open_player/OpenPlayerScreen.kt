package com.erg.almagestdota.stats.presentation.players.open_player

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.stats.presentation.players.lanes.PlayersLanesFragmentDirections
import com.erg.almagestdota.stats.presentation.players.lanes.single_player_lanes.SinglePlayerLanesFragmentDirections

class OpenPlayerScreen(
     playerId: Long
) : Screen<NavDirections>(
    route = SinglePlayerLanesFragmentDirections.toOpenPlayerFragment(playerId),
    requestKey = OpenPlayerFragment.TAG
)