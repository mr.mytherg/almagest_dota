package com.erg.almagestdota.stats.presentation.tournament_selection

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.complexadapter.ComplexAdapter
import com.erg.almagestdota.complexadapter.ComplexItem
import com.erg.almagestdota.complexadapter.ComplexListener
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdota.stats.R
import com.erg.almagestdota.stats.databinding.TournamentSelectionFragmentBinding
import com.erg.almagestdpta.core_navigation.external.helpers.navHostFragmentManager
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
internal class TournamentSelectionFragment : Fragment(R.layout.tournament_selection_fragment), BackPressListener {

    companion object {
        const val TAG = "TournamentSelectionFragment"
    }

    private val binding by viewBinding(TournamentSelectionFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    @Inject
    lateinit var viewModelFactory: TournamentSelectionViewModel.Factory
    private val viewModel by viewModels<TournamentSelectionViewModel> {
        TournamentSelectionViewModel.provideFactory(
            assistedFactory = viewModelFactory
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
    }

    override fun onBackPressed() {
        onClose()
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).filterNotNull().onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.loadingState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { state ->
            binding.loadingView.isVisible = state is Loading.Enabled
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun onClose(bundle: Bundle = TournamentSelectionScreen.Result.createResultCanceled()) {
        val fm = navHostFragmentManager()
        navController.popBackStack()
        fm.setFragmentResult(TAG, bundle)
    }

    private inner class ViewsConfigurator {
        private val adapter = ComplexAdapter(
            complexListener = object : ComplexListener {
                override fun onClick(item: ComplexItem) {
                    viewModel.obtainAction(TournamentSelectionHeroActions.SelectTournament(item.key))
                }
            }
        )

        fun initStartState() = with(binding) {
            ivBack.clicksWithDebounce {
                navController.popBackStack()
            }
            tvLoadMore.clicksWithDebounce {
                viewModel.obtainAction(TournamentSelectionHeroActions.LoadMore)
            }
            cbShady.clicksWithDebounce {
                viewModel.obtainAction(TournamentSelectionHeroActions.CheckShady)
            }
            ivSubmit.clicksWithDebounce {
                viewModel.obtainAction(TournamentSelectionHeroActions.Submit)
            }
            tvRetry.clicksWithDebounce {
                viewModel.obtainAction(TournamentSelectionHeroActions.Retry)
            }
        }

        fun renderState(state: TournamentSelectionViewState) = with(binding) {
            when (state) {
                is TournamentSelectionViewState.DefaultState -> renderDefaultState(state)
                is TournamentSelectionViewState.Retry -> renderRetryState()
            }
        }

        private fun renderRetryState() = with(binding) {
            tvRetry.isVisible = true
            llInfo.isVisible = false
            ivSubmit.isVisible = false
            tvLoadMore.isVisible = false
        }

        private fun renderDefaultState(state: TournamentSelectionViewState.DefaultState) = with(binding) {
            tvRetry.isVisible = false
            llInfo.isVisible = true
            tvLoadMore.isVisible = true
            ivSubmit.isVisible = true
            if (rvTournaments.adapter == null) {
                rvTournaments.adapter = adapter
            }
            adapter.submitList(state.tournaments)
        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
                is ViewEvent.PopBackStack.Empty -> {
                    val bundle = TournamentSelectionScreen.Result.createResultSubmitted()
                    onClose(bundle)
                }
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            navController.navigateViaScreenRoute(screen)
        }
    }
}