package com.erg.almagestdota.stats.presentation.players.selected_player_matches

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen

class SelectedPlayerMatchesScreen(
    route: NavDirections
) : Screen<NavDirections>(
    route = route,
    requestKey = SelectedPlayerMatchesFragment.TAG
)