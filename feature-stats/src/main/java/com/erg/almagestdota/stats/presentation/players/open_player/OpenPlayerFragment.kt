package com.erg.almagestdota.stats.presentation.players.open_player

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.NotificationStatus
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdota.stats.R
import com.erg.almagestdota.stats.databinding.OpenMatchFragmentBinding
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
internal class OpenPlayerFragment : Fragment(R.layout.open_match_fragment),
    BackPressListener {

    private val args by navArgs<OpenPlayerFragmentArgs>()

    companion object {
        const val TAG = "OpenMatchFragment"
    }

    private val binding by viewBinding(OpenMatchFragmentBinding::bind)
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    @Inject
    lateinit var viewModelFactory: OpenPlayerViewModel.Factory
    private val viewModel by viewModels<OpenPlayerViewModel> {
        OpenPlayerViewModel.provideFactory(
            assistedFactory = viewModelFactory
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private inner class ViewsConfigurator {

        fun initStartState() = with(binding) {
            ivBack.clicksWithDebounce {
                navController.popBackStack()
            }
            tvStratz.clicksWithDebounce {
                openUrl("https://stratz.com/players/${args.playerId}")
            }
            tvDotabuff.clicksWithDebounce {
                openUrl("https://www.dotabuff.com/esports/players/${args.playerId}")
            }
            tvOpenDota.clicksWithDebounce {
                openUrl("https://www.opendota.com/players/${args.playerId}")
            }
        }

        private fun openUrl(url: String) {
            val webpage: Uri = Uri.parse(url)
            val intent = Intent(Intent.ACTION_VIEW, webpage)
            if (intent.resolveActivity(requireContext().packageManager) != null) {
                startActivity(intent)
            } else {
                val snack = ViewEvent.AlmagestSnackbar(getString(R.string.web_browser_error), NotificationStatus.ERROR)
                eventsConfigurator.handleViewEvent(snack)
            }
        }

        fun renderState(state: OpenPlayerViewState) = with(binding) {
            when (state) {
                is OpenPlayerViewState.DefaultState -> renderDefaultState()
            }
        }

        private fun renderDefaultState() = with(binding) {

        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            navController.navigateViaScreenRoute(screen)
        }
    }

    override fun onBackPressed() {
        navController.popBackStack()
    }
}