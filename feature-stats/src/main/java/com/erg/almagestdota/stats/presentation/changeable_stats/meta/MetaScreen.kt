package com.erg.almagestdota.stats.presentation.changeable_stats.meta

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.stats.presentation.StatsFragmentDirections
import com.erg.almagestdota.stats.presentation.changeable_stats.ChangeableStatsFragmentDirections

class MetaScreen(teamId: Long) : Screen<NavDirections>(
    route = ChangeableStatsFragmentDirections.toMetaFragment(teamId),
    requestKey = MetaFragment.TAG
)