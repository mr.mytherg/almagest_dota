package com.erg.almagestdota.stats.presentation.hero_impacts.impact_kda

import androidx.navigation.NavDirections
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.stats.presentation.hero_impacts.HeroImpactsFragmentDirections

object ImpactKDAScreen : Screen<NavDirections>(
    route = HeroImpactsFragmentDirections.toImpactKDAFragment(),
    requestKey = ImpactKDAFragment.TAG
)