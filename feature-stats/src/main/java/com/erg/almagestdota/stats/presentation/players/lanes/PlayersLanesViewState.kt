package com.erg.almagestdota.stats.presentation.players.lanes

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.complexadapter.ComplexItem

internal sealed class PlayersLanesViewState : ViewState() {
    class DefaultState(
        val roles: List<ComplexItem>,
        val players: List<ComplexItem>,
        val sortType: PlayersLanesSortType
    ) : PlayersLanesViewState()
}