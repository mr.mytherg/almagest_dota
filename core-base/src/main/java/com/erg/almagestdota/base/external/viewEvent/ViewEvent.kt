package com.erg.almagestdota.base.external.viewEvent

import android.view.Gravity
import androidx.annotation.StringRes

abstract class ViewEvent {
    class AlmagestSnackbar(
        val message: String? = null,
        val status: NotificationStatus,
        @StringRes val messageId: Int? = null,
        val delayMillis: Long = 0L,
        val gravity: Int = Gravity.TOP,
        val onDismiss: (() -> Unit)? = null
    ) : ViewEvent()

    class Navigation(val screen: Screen<*>) : ViewEvent()
    sealed class PopBackStack<T>(val data: T) : ViewEvent() {
        object Empty : PopBackStack<Unit>(Unit)
        class Data<T>(data: T) : PopBackStack<T>(data)
    }
}