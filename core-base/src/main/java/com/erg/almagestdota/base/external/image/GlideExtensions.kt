package com.erg.almagestdota.base.external.image

import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.bumptech.glide.Glide

fun ImageView.loadImageByUrl(url: String?, @DrawableRes placeholder: Int? = null) {
    if (placeholder == null) {
        Glide.with(context.applicationContext)
            .load(url)
            .into(this)
    } else {
        Glide.with(context.applicationContext)
            .load(url)
            .placeholder(placeholder)
            .into(this)
    }
}