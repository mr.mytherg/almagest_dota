package com.erg.almagestdota.base.external.ui

interface BackPressListener {
    fun onBackPressed()
}