package com.erg.almagestdota.base.external.mapper

import com.erg.almagestdota.base.external.extensions.deepAssert
import com.erg.almagestdota.base.external.extensions.mapping

fun <T : Any, R : Any> T.essentialMap(mapper: EssentialMapper<T, R>): R = let(mapper::invoke)
fun <T : Any, R : Any> List<T?>?.essentialMap(mapper: EssentialMapper<T, R>): List<R> {
    return deepAssert().mapping(mapper)
}