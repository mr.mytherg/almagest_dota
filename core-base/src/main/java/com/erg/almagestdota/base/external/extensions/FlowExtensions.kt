package com.erg.almagestdota.base.external.extensions

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.erg.almagestdota.base.external.viewState.Loading
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import java.lang.Exception

fun <T> ViewModel.emit(sharedFlow: MutableSharedFlow<T>, event: T) {
    viewModelScope.launch {
        sharedFlow.emit(event)
    }
}

private fun <T> CoroutineScope.emit(sharedFlow: MutableSharedFlow<T>, event: T) {
    this.launch {
        sharedFlow.emit(event)
    }
}

fun <T> ViewModel.launchOnIO(
    loader: MutableStateFlow<Loading>? = null,
    action: suspend () -> T,
    onSuccess: suspend (T) -> Unit,
    onError: suspend (Exception) -> Unit
) {
    if (loader != null) {
        loader.value = Loading.Enabled
    }
    viewModelScope.launch(Dispatchers.IO) {
        try {
            val data = action.invoke()
            onSuccess.invoke(data)
        } catch (e: Exception) {
            Log.e("ALMAGESTERROR", "${e.message}")
            onError.invoke(e)
        }
        if (loader != null) {
            loader.value = Loading.Disabled
        }
    }
}

fun <T> CoroutineScope.launchOnIO(
    loader: MutableStateFlow<Loading>? = null,
    action: suspend () -> T,
    onSuccess: suspend (T) -> Unit,
    onError: suspend (Exception) -> Unit
) {
    if (loader != null) {
        loader.value = Loading.Enabled
    }
    this.launch(Dispatchers.IO) {
        try {
            val data = action.invoke()
            onSuccess.invoke(data)
        } catch (e: Exception) {
            Log.e("ALMAGESTERROR", "${e.message}")
            onError.invoke(e)
        }
        if (loader != null) {
            loader.value = Loading.Disabled
        }
    }
}

fun <T> ViewModel.launchOnDefault(
    loader: MutableStateFlow<Loading>? = null,
    action: suspend () -> T,
    onSuccess: suspend (T) -> Unit,
    onError: suspend (Exception) -> Unit
) {
    if (loader != null) {
        loader.value = Loading.Enabled
    }
    viewModelScope.launch(Dispatchers.Default) {
        try {
            val data = action.invoke()
            onSuccess.invoke(data)
        } catch (e: Exception) {
            Log.e("ALMAGESTERROR", "${e.message}")
            onError.invoke(e)
        }
        if (loader != null) {
            loader.value = Loading.Disabled
        }
    }
}