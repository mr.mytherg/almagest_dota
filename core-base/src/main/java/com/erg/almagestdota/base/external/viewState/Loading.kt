package com.erg.almagestdota.base.external.viewState

sealed class Loading {
    object Enabled : Loading()
    object Disabled : Loading()
}