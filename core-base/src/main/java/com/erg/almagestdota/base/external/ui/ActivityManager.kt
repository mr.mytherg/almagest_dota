package com.erg.almagestdota.base.external.ui

import android.widget.EditText

interface ActivityManager {
	fun hideKeyboard()
	fun showAppReview()
	fun showKeyboard(editText: EditText)
}