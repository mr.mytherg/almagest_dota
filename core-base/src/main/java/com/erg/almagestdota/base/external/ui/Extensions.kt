package com.erg.almagestdota.base.external.ui

import android.content.Context
import com.erg.almagestdota.base.R
import java.util.*

fun Context.millisToMinutesTimerFormat(timestamp: Long): String {
    val calendar: Calendar = Calendar.getInstance()
    calendar.timeInMillis = timestamp
    return if (calendar.get(Calendar.SECOND) < 10) {
        getString(R.string.timer_less_than_10_seconds, calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND))
    } else {
        getString(R.string.timer_default, calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND))
    }
}

fun Long.secondsToMatchTimerFormat(): String {
    val hours = this / 3600
    val minutes = this / 60 % 60
    val seconds = this % 60

    var time = ""
    if (hours != 0L) {
        time += "$hours:"
    }

    if (minutes < 10) {
        time += "0$minutes:"
    } else {
        time += "$minutes:"
    }

    if (seconds < 10) {
        time += "0$seconds"
    } else {
        time += "$seconds"
    }
    return time
}

fun Long.millisToTextAgo(): Pair<Int, Long> {
    val ago = System.currentTimeMillis() / 1000 - this
    val pair = when {
        ago / 84600 > 0 -> R.plurals.days_ago to ago / 84600
        ago / 3600 > 0 -> R.plurals.hours_ago to ago / 3600
        else -> R.plurals.minutes_ago to ago / 60 % 60
    }
    return pair
}