package com.erg.almagestdota.base.external.mapper

import java.lang.Exception

interface Function<T, R> {
    /**
     * Apply some calculation to the input value and return some other value.
     * @param t the input value
     * @return the output value
     * @throws Exception on error
     */
    @Throws(Exception::class)
    fun apply(t: T): R
}