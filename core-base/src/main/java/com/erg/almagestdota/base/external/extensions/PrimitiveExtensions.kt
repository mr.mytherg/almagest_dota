package com.erg.almagestdota.base.external.extensions

import java.text.DecimalFormat

fun Double.round2(): Double {
    var formattedDouble = DecimalFormat("#0.00").format(this)
    formattedDouble = formattedDouble.replace(",", ".")
    return formattedDouble.toDouble()
}

fun String.round2(): Double {
    val double = this.toDouble()
    var formattedDouble = DecimalFormat("#0.00").format(double)
    formattedDouble = formattedDouble.replace(",", ".")
    return formattedDouble.toDouble()
}

fun Double.round1(): Double {
    var formattedDouble = DecimalFormat("#0.0").format(this)
    formattedDouble = formattedDouble.replace(",", ".")
    return formattedDouble.toDouble()
}

fun String.round1(): Double {
    val double = this.toDouble()
    var formattedDouble = DecimalFormat("#0.0").format(double)
    formattedDouble = formattedDouble.replace(",", ".")
    return formattedDouble.toDouble()
}

fun String.save2DigitsAfterDot(): String {
    var count = 0
    var hasDotPassed = false
    for (ch in this) {
        if (ch == '.')
            hasDotPassed = true
        if (hasDotPassed && ch.isDigit())
            count++
    }
    return if (count == 1)
        this + "0"
    else
        this
}