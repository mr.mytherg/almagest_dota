package com.erg.almagestdota.base.external.viewEvent

abstract class Screen<T>(
    open val route: T,
    val requestKey: String? = null
)