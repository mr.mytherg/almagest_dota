package com.erg.almagestdota.heroselector.presentation.heroSelector

import android.os.Bundle
import androidx.core.os.bundleOf
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData

class HeroSelectorScreen() : Screen<String>(
    route = "com.erg.almagestdota://hero_selector",
    requestKey = TAG
) {
    companion object {
        const val TAG = "HeroSelectorSheet"
        private const val RESULT_KEY = "RESULT_KEY"
        private const val DATA_KEY = "DATA_KEY"

        fun getResult(data: Bundle): Result = Result.enumValueOf(data.getString(RESULT_KEY))
        fun getSelectedHero(data: Bundle): Int = data.getInt(DATA_KEY)
    }

    enum class Result {
        SELECTED,
        CANCELED;

        companion object {
            fun enumValueOf(value: String?): Result {
                return values().firstOrNull { it.name == value } ?: CANCELED
            }

            internal fun createResult(hero: HeroCoreData? = null): Bundle {
                return if (hero == null) {
                    bundleOf(RESULT_KEY to CANCELED.name)
                } else {
                    bundleOf(RESULT_KEY to SELECTED.name, DATA_KEY to hero.id)
                }
            }
        }
    }
}