package com.erg.almagestdota.heroselector.domain

import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData

class HeroSelectorHeroes(
    val heroes: List<HeroCoreData>
)