package com.erg.almagestdota.heroselector.presentation.heroSelector

import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData

internal sealed class HeroSelectorViewEvents : ViewEvent() {
    object BackClickEvent : HeroSelectorViewEvents()
    class ResultEvent(val hero: HeroCoreData) : HeroSelectorViewEvents()
}