package com.erg.almagestdota.heroselector.presentation.heroSelector

import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData

internal sealed class HeroSelectorViewActions {

    object OnBackClick : HeroSelectorViewActions()
    object ClearSearch : HeroSelectorViewActions()
    class FilterByName(val query: String) : HeroSelectorViewActions()
    class SelectHero(val hero: HeroCoreData) : HeroSelectorViewActions()
}