package com.erg.almagestdota.heroselector.presentation.heroSelector

import androidx.constraintlayout.motion.utils.ViewState
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData

internal sealed class HeroSelectorViewState : ViewState() {

    class DefaultState(
        val heroes: List<HeroCoreData>,
    ) : HeroSelectorViewState()
}