package com.erg.almagestdota.heroselector.presentation.heroSelector

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

internal class HeroSelectorViewModel @AssistedInject constructor(
    private val stringProvider: StringProvider
) : ViewModel() {

    private val stateConfigurator = StateConfigurator()

    private val viewEventMutable = MutableResultFlow { resultKey, data -> }
    val viewEvent: ResultFlow = viewEventMutable

    private val viewStateMutable = MutableStateFlow(stateConfigurator.defineFragmentState())
    val viewState = viewStateMutable.asStateFlow()

    fun obtainAction(action: HeroSelectorViewActions) {
        when (action) {
            is HeroSelectorViewActions.FilterByName -> {
                stateConfigurator.query = action.query
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is HeroSelectorViewActions.ClearSearch -> {
                stateConfigurator.query = ""
                viewStateMutable.value = stateConfigurator.defineFragmentState()
            }
            is HeroSelectorViewActions.OnBackClick -> {
                emit(viewEventMutable, HeroSelectorViewEvents.BackClickEvent)
            }
            is HeroSelectorViewActions.SelectHero -> {
                emit(viewEventMutable, HeroSelectorViewEvents.ResultEvent(action.hero))
            }
        }
    }

    private inner class StateConfigurator {
        var query = ""
        val heroes = GlobalVars.mainSettings.heroes.sortedBy { it.name }

        fun defineFragmentState(): HeroSelectorViewState {
            return HeroSelectorViewState.DefaultState(
                heroes = heroes.filter { it.name.contains(query, true) },
            )
        }
    }

    companion object {
        fun provideFactory(
            assistedFactory: Factory
        ): ViewModelProvider.Factory {
            return object : ViewModelProvider.Factory {

                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return assistedFactory.create() as T
                }
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(): HeroSelectorViewModel
    }
}