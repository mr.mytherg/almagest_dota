package com.erg.almagestdota.heroselector.domain

import android.os.Parcelable
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData
import kotlinx.android.parcel.Parcelize

@Parcelize
internal class HeroSelectorHeroCoreData(
    override val name: String,
    override val id: Int,
    override val imageLink: String,
    val isSelected: Boolean
) : Parcelable, HeroCoreData(
    name = name,
    id = id,
    imageLink = imageLink
) {
    companion object {
        fun create(hero: HeroCoreData, selectedHeroes: List<HeroCoreData>): HeroSelectorHeroCoreData {
            return HeroSelectorHeroCoreData(
                name = hero.name,
                id = hero.id,
                imageLink = hero.imageLink,
                isSelected = selectedHeroes.find { it.id == hero.id } != null
            )
        }
    }
}