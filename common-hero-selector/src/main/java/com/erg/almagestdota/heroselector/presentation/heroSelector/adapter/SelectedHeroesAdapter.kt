package com.erg.almagestdota.heroselector.presentation.heroSelector.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.erg.almagestdota.base.external.image.loadImageByUrl
import com.erg.almagestdota.heroselector.R
import com.erg.almagestdota.heroselector.databinding.DraftHeroRolesBinding
import com.erg.almagestdota.local_storage.external.models.draft.RoleType
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroRoleData
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroTeamData

class SelectedHeroesAdapter(val isBans: Boolean = false) :
    ListAdapter<HeroCoreData, SelectedHeroesAdapter.HeroInDraftListViewHolder>(diffCallback) {

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<HeroCoreData>() {
            override fun areItemsTheSame(oldItem: HeroCoreData, newItem: HeroCoreData): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: HeroCoreData, newItem: HeroCoreData): Boolean {
                return when {
                    oldItem is HeroTeamData && newItem is HeroTeamData -> {
                        oldItem.lostCount == newItem.lostCount &&
                            oldItem.wonCount == newItem.wonCount &&
                            oldItem.heroRoles == newItem.heroRoles &&
                            oldItem == newItem
                    }
                    oldItem is HeroRoleData && newItem is HeroRoleData -> {
                        oldItem.heroRoles == newItem.heroRoles &&
                            oldItem == newItem
                    }
                    else -> oldItem == newItem
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeroInDraftListViewHolder {
        val binding = DraftHeroRolesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return HeroInDraftListViewHolder(binding)
    }

    override fun onBindViewHolder(holder: HeroInDraftListViewHolder, position: Int) {
        holder.bind(getItem(position), isBans)
    }

    class HeroInDraftListViewHolder(
        private val binding: DraftHeroRolesBinding,
    ) : RecyclerView.ViewHolder(binding.root) {
        private val context = binding.root.context

        fun bind(hero: HeroCoreData, isBans: Boolean) = with(binding) {
            ivHero.alpha = if (isBans) 0.4f else 1.0f
            tvRole1.isVisible = false
            tvWon.isVisible = false
            tvLost.isVisible = false
            tvRole2.isVisible = false
            tvRole3.isVisible = false
            tvRole4.isVisible = false
            tvRole5.isVisible = false
            when {
                hero.isNullHero() -> {
                    tvName.isVisible = false
                    ivHero.loadImageByUrl(null, R.drawable.ic_dota_2)
                }
                hero.isSkippedHero() -> {
                    tvName.isVisible = false
                    ivHero.loadImageByUrl(null, R.drawable.ic_dota_2)
                }
                else -> {
                    tvName.isVisible = true
                    tvName.text = hero.name
                    ivHero.loadImageByUrl(hero.imageLink)

                    if (isBans) return@with
                    if (hero is HeroRoleData) {
                        if (hero.heroRoles.isNullOrEmpty()) {
                            tvRole1.isVisible = true
                            tvRole1.text = context.getString(R.string.hero_role_unknown)
                        } else {
                            fillRoles(hero)
                        }
                        if (hero is HeroTeamData) {
                            tvWon.isVisible = true
                            tvLost.isVisible = true
                            tvWon.text = hero.wonCount.toString()
                            tvLost.text = hero.lostCount.toString()
                        }
                    }
                }
            }
        }

        private fun fillRoles(hero: HeroRoleData) = with(binding) {
            for (role in hero.heroRoles!!) {
                when (role) {
                    RoleType.ROLE_1 -> {
                        tvRole1.isVisible = true
                        tvRole1.text = context.getString(R.string.role_1)
                    }
                    RoleType.ROLE_2 -> {
                        tvRole2.isVisible = true
                        tvRole2.text = context.getString(R.string.role_2)
                    }
                    RoleType.ROLE_3 -> {
                        tvRole3.isVisible = true
                        tvRole3.text = context.getString(R.string.role_3)
                    }
                    RoleType.ROLE_4 -> {
                        tvRole4.isVisible = true
                        tvRole4.text = context.getString(R.string.role_4)
                    }
                    RoleType.ROLE_5 -> {
                        tvRole5.isVisible = true
                        tvRole5.text = context.getString(R.string.role_5)
                    }
                }
            }
        }
    }
}