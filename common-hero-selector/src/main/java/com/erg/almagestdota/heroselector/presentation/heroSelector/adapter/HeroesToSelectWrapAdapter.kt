package com.erg.almagestdota.heroselector.presentation.heroSelector.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.erg.almagestdota.base.external.image.loadImageByUrl
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.heroselector.databinding.DraftHeroItemWrapBinding
import com.erg.almagestdota.local_storage.external.models.main_settings.HeroCoreData

class HeroesToSelectWrapAdapter(
    private val onClick: (HeroCoreData) -> Unit,
    private val onLongClick: (HeroCoreData) -> Unit,
) : ListAdapter<HeroCoreData, HeroesToSelectWrapAdapter.HeroToSelectViewHolder>(diffCallback) {

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<HeroCoreData>() {
            override fun areItemsTheSame(oldItem: HeroCoreData, newItem: HeroCoreData): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: HeroCoreData, newItem: HeroCoreData): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeroToSelectViewHolder {
        val binding = DraftHeroItemWrapBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return HeroToSelectViewHolder(binding, onClick, onLongClick)
    }

    override fun onBindViewHolder(holder: HeroToSelectViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    class HeroToSelectViewHolder(
        private val binding: DraftHeroItemWrapBinding,
        private val onClick: (HeroCoreData) -> Unit,
        private val onLongClick: (HeroCoreData) -> Unit,
    ) : RecyclerView.ViewHolder(binding.root) {
        private val context = binding.root.context

        fun bind(hero: HeroCoreData) = with(binding) {
            root.clicksWithDebounce { onClick.invoke(hero) }
            root.setOnLongClickListener {
                onLongClick.invoke(hero)
                return@setOnLongClickListener true
            }
            tvName.text = hero.name
            ivHero.loadImageByUrl(hero.imageLink)
        }
    }
}