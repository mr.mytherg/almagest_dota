package com.erg.almagestdota.heroselector.presentation.heroSelector

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import com.erg.almagestdota.base.external.ui.ActivityManager
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewBinding.viewBinding
import com.erg.almagestdota.base.external.viewEvent.Screen
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.heroselector.R
import com.erg.almagestdota.heroselector.databinding.HeroSelectorSheetBinding
import com.erg.almagestdota.heroselector.presentation.heroSelector.adapter.HeroesToSelectAdapter
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdpta.core_navigation.external.helpers.BottomSheetFragment
import com.erg.almagestdpta.core_navigation.external.helpers.navHostFragmentManager
import com.erg.almagestdpta.core_navigation.external.helpers.navigateViaScreenRoute
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
internal class HeroSelectorSheet : BottomSheetFragment(R.layout.hero_selector_sheet), BackPressListener {

    private val binding by viewBinding { HeroSelectorSheetBinding.bind(requireContentView()) }
    private val navController by lazy { findNavController() }
    private val viewsConfigurator = ViewsConfigurator()
    private val eventsConfigurator = EventsConfigurator()

    @Inject
    lateinit var gson: Gson

    @Inject
    lateinit var viewModelFactory: HeroSelectorViewModel.Factory
    private val viewModel by viewModels<HeroSelectorViewModel> {
        HeroSelectorViewModel.provideFactory(
            assistedFactory = viewModelFactory
        )
    }

    override fun onBackPressed() {
        viewModel.obtainAction(HeroSelectorViewActions.OnBackClick)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewsConfigurator.initStartState()
        viewModel.viewEvent.setResultListener(this)
    }

    override fun onBottomSheetStateChanged(bottomSheet: View, newState: Int) {
        super.onBottomSheetStateChanged(bottomSheet, newState)
        if (newState == BottomSheetBehavior.STATE_HIDDEN) {
            onClose()
        }
    }
    override fun onResume() {
        super.onResume()
        binding.etSearch.requestFocus()
        (requireActivity() as ActivityManager).showKeyboard(binding.etSearch)
    }

    private fun observeViewModel() {
        viewModel.viewState.flowWithLifecycle(viewLifecycleOwner.lifecycle, Lifecycle.State.RESUMED).onEach { state ->
            viewsConfigurator.renderState(state)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.viewEvent.flowWithLifecycle(viewLifecycleOwner.lifecycle, Lifecycle.State.RESUMED).onEach { event ->
            eventsConfigurator.handleViewEvent(event)
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun onClose() {
        val fm = navHostFragmentManager()
        navController.popBackStack()
        fm.setFragmentResult(HeroSelectorScreen.TAG, HeroSelectorScreen.Result.createResult())
    }

    private inner class ViewsConfigurator {
        private val heroSelectorAdapter by lazy {
            HeroesToSelectAdapter(
                onClick = { hero ->
                    viewModel.obtainAction(HeroSelectorViewActions.SelectHero(hero))
                },
                onLongClick = { hero ->
                    viewModel.obtainAction(HeroSelectorViewActions.SelectHero(hero))
                }
            )
        }

        private val searchWatcher = object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {
                val query = p0.toString()
                viewModel.obtainAction(HeroSelectorViewActions.FilterByName(query.trim()))
            }
        }

        fun initStartState() = with(binding) {
            if (rvHeroes.adapter == null) {
                val itemDecorator = DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
                itemDecorator.setDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.vertical_line_grey)!!)
                rvHeroes.addItemDecoration(itemDecorator)
                rvHeroes.adapter = heroSelectorAdapter
            }
            etSearch.addTextChangedListener(searchWatcher)
        }

        fun renderState(state: HeroSelectorViewState) {
            when (state) {
                is HeroSelectorViewState.DefaultState -> renderDefaultState(state)
            }
        }

        private fun renderDefaultState(state: HeroSelectorViewState.DefaultState) = with(binding) {
            heroSelectorAdapter.submitList(state.heroes)
        }
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.Navigation -> handleScreen(event.screen)
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
                else -> handleCustomEvents(event)
            }
        }

        private fun handleCustomEvents(event: ViewEvent) {
            when (event) {
                is HeroSelectorViewEvents.BackClickEvent -> {
                    onClose()
                }
                is HeroSelectorViewEvents.ResultEvent -> {
                    val fm = navHostFragmentManager()
                    navController.popBackStack()
                    fm.setFragmentResult(
                        HeroSelectorScreen.TAG,
                        HeroSelectorScreen.Result.createResult(event.hero)
                    )
                }
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(requireView())
        }

        private fun handleScreen(screen: Screen<*>) {
            viewModel.viewEvent.setResultListener(this@HeroSelectorSheet)
            navController.navigateViaScreenRoute(screen)
        }
    }
}