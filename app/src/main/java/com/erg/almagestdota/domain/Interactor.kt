package com.erg.almagestdota.domain

import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.ISharedPreferencesContract
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.main_settings.MainSettings
import com.erg.almagestdota.settings.domain.use_cases.SaveMatchesUseCase
import com.erg.almagestdota.settings.domain.use_cases.UpdateHeroRolesUseCase
import com.erg.almagestdota.settings.domain.use_cases.UpdateHeroStagesUseCase
import com.erg.almagestdota.stats.domain.UpdateFactionsDataUseCase
import com.erg.almagestdota.settings.domain.use_cases.GetFilterGroupsUseCase
import javax.inject.Inject

class Interactor @Inject constructor(
    private val repository: IContract,
    private val localStorage: ILocalStorageContract,
    private val sharedPreferences: ISharedPreferencesContract,
    private val getFilterGroupsUseCase: GetFilterGroupsUseCase,
    private val saveMatchesUseCase: SaveMatchesUseCase,
    private val updateHeroStagesUseCase: UpdateHeroStagesUseCase,
    private val updateHeroRolesUseCase: UpdateHeroRolesUseCase,
    private val updateFactionsDataUseCase: UpdateFactionsDataUseCase,
) {

    suspend fun loadData() = repository.loadData()

    fun getDataExpirationTimestamp(): Long = sharedPreferences.getDataExpirationTimestamp()
    fun getLastReviewTimestamp(): Long = sharedPreferences.getLastReviewTimestamp()
    fun setLastReviewTimestamp(timestamp: Long) = sharedPreferences.setLastReviewTimestamp(timestamp)
    suspend fun initGlobalMainSettings() = localStorage.initGlobalMainSettings(::loadFilterGroups)
    private suspend fun loadFilterGroups() {
        val matches = saveMatchesUseCase.readFromFile()
        localStorage.metaMatches = matches
        if (matches.isNotEmpty()) {
            val factions = updateFactionsDataUseCase.invoke(matches, 0L)
            val roles = updateHeroRolesUseCase.invoke(matches, 0L)
            val stages =  updateHeroStagesUseCase.invoke(matches, 0L)
            for (hero in GlobalVars.mainSettings.heroes) {
                if (factions[hero.id] != null)
                    hero.factions = factions[hero.id]!!
                if (roles[hero.id] != null)
                    hero.roles = roles[hero.id]!!
                if (stages[hero.id] != null)
                    hero.stages = stages[hero.id]!!
            }
        }
        GlobalVars.groups = getFilterGroupsUseCase.invoke()
    }

    fun getMainSettings() = localStorage.getMainSettings()
}