package com.erg.almagestdota.domain.di

import android.content.Context
import com.erg.almagestdota.data.Repository
import com.erg.almagestdota.domain.IContract
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.ISharedPreferencesContract
import com.erg.almagestdota.local_storage.external.raw_models.MainPredictorRaw
import com.erg.almagestdota.local_storage.external.raw_models.MainSettingsRaw
import com.erg.almagestdota.network.external.annotations.RetrofitClientSteam
import com.erg.almagestdota.network.external.firebase.FirebaseRepository
import com.erg.almagestdota.network.external.firebase.FirebaseRepositoryContract
import com.erg.almagestdota.presentation.workManagers.IPredictorRepository
import com.erg.almagestdota.presentation.workManagers.PredictorRepository
import com.erg.almagestdota.pro_matches.data.ISteamMatchesLoadingApi
import com.erg.almagestdota.pro_matches.domain.GetLiveLeagueGamesUseCase
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ModuleMain {

    @Provides
    fun provideRepository(
        api: FirebaseFirestore,
        @ApplicationContext context: Context,
        firebaseRepository: FirebaseRepositoryContract,
        localStorage: ILocalStorageContract,
        sharedPreferences: ISharedPreferencesContract,
        gson: Gson,
    ): IContract {
        return Repository(
            api = api,
            context = context,
            firebaseRepository = firebaseRepository,
            localStorage = localStorage,
            sharedPreferences = sharedPreferences,
            gson = gson,
        )
    }

    @Provides
    @Singleton
    fun providePredictorRepository(
        getLiveLeagueGamesUseCase: GetLiveLeagueGamesUseCase
    ): IPredictorRepository {
        return PredictorRepository(
            getLiveLeagueGamesUseCase
        )
    }

    @Provides
    fun provideGson(): Gson {
        return Gson()
            .newBuilder()
            .create()
    }

    @Provides
    @Singleton
    fun provideFirebaseRepository(
         api: FirebaseFirestore,
        @ApplicationContext context: Context,
         gson: Gson,
    ): FirebaseRepositoryContract {
        return FirebaseRepository(
            api,
            context,
            gson
        )
    }

    @Provides
    @Singleton
    fun provideSteamMatchesLoadingApi(@RetrofitClientSteam retrofit: Retrofit): ISteamMatchesLoadingApi {
        return retrofit.create(ISteamMatchesLoadingApi::class.java)
    }
}