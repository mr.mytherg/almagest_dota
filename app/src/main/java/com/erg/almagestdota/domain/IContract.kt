package com.erg.almagestdota.domain

import com.erg.almagestdota.local_storage.external.models.main_settings.MainSettings

interface IContract {
    suspend fun loadData()
}