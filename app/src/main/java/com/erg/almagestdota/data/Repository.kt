package com.erg.almagestdota.data

import android.content.Context
import android.content.pm.PackageInfo
import com.erg.almagestdota.base.external.extensions.asMap
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.domain.IContract
import com.erg.almagestdota.local_storage.external.ILocalStorageContract
import com.erg.almagestdota.local_storage.external.ISharedPreferencesContract
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.local_storage.external.models.main_settings.MainSettings
import com.erg.almagestdota.local_storage.external.raw_models.MainPredictorRaw
import com.erg.almagestdota.local_storage.external.raw_models.MainSettingsRaw
import com.erg.almagestdota.network.external.firebase.FirebaseRepositoryContract
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.tasks.await
import java.lang.IllegalStateException
import javax.inject.Inject
import kotlin.coroutines.coroutineContext

class Repository @Inject constructor(
    private val api: FirebaseFirestore,
    @ApplicationContext private val context: Context,
    private val firebaseRepository: FirebaseRepositoryContract,
    private val localStorage: ILocalStorageContract,
    private val sharedPreferences: ISharedPreferencesContract,
    private val gson: Gson,
) : IContract {

    companion object {
        private const val DATA_STORAGE_DURATION: Long = 24 * 60 * 60 * 1000 // 24 HOURS
    }

    private val pInfo: PackageInfo = context.packageManager.getPackageInfo(context.packageName, 0)
    private val version = pInfo.versionName

    private val loadList = listOf<suspend () -> Unit>(
        {
            val mainSettings = firebaseRepository.getMainSettingsOrNull()
            if (mainSettings == null) {
                val data = api.collection("0.3.3-release").document("settings").get().await().data
                val settingsRaw: MainSettingsRaw = gson.fromJson(gson.toJson(data?.toMap()), MainSettingsRaw::class.java)
                api.collection(version).document("settings").set(settingsRaw.asMap(gson)).await()
                val dataPredictor = api.collection("0.3.3-release").document("predictor").get().await().data
                val predictorRaw: MainPredictorRaw = gson.fromJson(gson.toJson(dataPredictor?.toMap()), MainPredictorRaw::class.java)
                api.collection(version).document("predictor").set(predictorRaw.asMap(gson)).await()
                throw IllegalStateException("Миграция сделана. Перезайди в приложение")
            }
            GlobalVars.mainSettings = mainSettings
            localStorage.setMainSettings(mainSettings.copy(heroes = mainSettings.heroes.sortedBy { it.id }))
        },
        {
            firebaseRepository.getTeamsSettings()
        },
        {
            firebaseRepository.getPlayersSettings()
        },
        {
            val mainPredictor = firebaseRepository.getMainPredictor()
            localStorage.setMainPredictor(mainPredictor)
        },
        {
            GlobalVars.importantSettings = firebaseRepository.getImportantSettings()
            sharedPreferences.setDataExpirationTimestamp(System.currentTimeMillis() + DATA_STORAGE_DURATION)
        },
    )

    override suspend fun loadData() {
        val deferredList = mutableListOf<Deferred<Unit>>()
        for (action in loadList) {
            val deferred = CoroutineScope(coroutineContext).async {
                action.invoke()
                Unit
            }
            deferredList.add(deferred)
        }
        deferredList.awaitAll()
    }
}