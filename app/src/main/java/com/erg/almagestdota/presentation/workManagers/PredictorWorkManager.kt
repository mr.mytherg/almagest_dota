package com.erg.almagestdota.presentation.workManagers

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.hilt.work.HiltWorker
import androidx.lifecycle.viewModelScope
import androidx.work.*
import com.erg.almagestdota.R
import com.erg.almagestdota.base.external.extensions.orDefault
import com.erg.almagestdota.local_storage.external.models.live.LeagueGame
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@HiltWorker
class PredictorWorkManager @AssistedInject constructor(
    @Assisted appContext: Context,
    @Assisted workerParams: WorkerParameters,
    private val predictorRepository: IPredictorRepository,
) : CoroutineWorker(appContext, workerParams) {

    companion object {
        private const val NOTIFICATION_ID = 123213
        private const val NOTIFICATION_CHANNEL_ID = "foregr"
    }

    override suspend fun getForegroundInfo(): ForegroundInfo {
        return createForegroundInfo()
    }

    override suspend fun doWork(): Result {
        setForegroundAsync(createForegroundInfo())
        return withContext(Dispatchers.IO) {
            try {
                val matches = predictorRepository.getLiveLeagueGames().value.orEmpty()

                if (matches.isEmpty()) {
                    return@withContext Result.success()
                }
                val listDeferred = mutableListOf<Deferred<Unit>>()
                for (match in matches) {
                    val deferred = async(Dispatchers.Default) {
                        if (match.scoreboard.radiant.picks.all { it.id > 0 } && match.scoreboard.radiant.picks.size == 5 &&
                            match.scoreboard.dire.picks.all { it.id > 0 } && match.scoreboard.dire.picks.size == 5
                        ) {
                            notifyResult(match, false)
                        } else {
                            notifyResult(match, true)
                        }
                    }
                    listDeferred.add(deferred)
                }
                listDeferred.awaitAll()
                Result.success()
            } catch (e: Exception) {
                Result.failure()
            }
        }
    }

    private fun notifyResult(match: LeagueGame, isPreGame: Boolean) {
        val radiantName = match.radiantTeam?.teamName?.orDefault()
        val direTeamName = match.direTeam?.teamName?.orDefault()
        val gameNum = match.radiantSeriesWins + match.direSeriesWins + 1
        val text = "Game $gameNum. ${match.leagueName}."

        val title = if (isPreGame) {
            "Drafting. $radiantName vs $direTeamName"
        } else {
            "Draft prediction. $radiantName vs $direTeamName"
        }

        val builder = NotificationCompat.Builder(applicationContext, NOTIFICATION_CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_dota_2)
            .setContentTitle(title)
//            .setStyle(
//                NotificationCompat.BigTextStyle()
//                    .bigText(text)
//            )
            .setContentText(text)

        val notification: Notification = builder.build()

        val notificationManager = applicationContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(radiantName.hashCode(), notification)
    }

    private fun createForegroundInfo(): ForegroundInfo {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel()
        }

        val title = "Almagest dota 2 picker"

        val notification = NotificationCompat.Builder(applicationContext, NOTIFICATION_CHANNEL_ID)
            .setContentTitle(title)
            .setTicker(title)
            .setContentText("loading matches")
            .setSmallIcon(R.drawable.ic_dota_2)
            .setOngoing(true)
            .build()

        return ForegroundInfo(NOTIFICATION_ID, notification)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createChannel() {
        val name = "Almagest dota 2 picker"
        val descriptionText = "loading matches"
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val mChannel = NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance)
        mChannel.description = descriptionText
        val notificationManager = applicationContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(mChannel)
    }
}