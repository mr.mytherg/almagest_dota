package com.erg.almagestdota.presentation

import androidx.lifecycle.ViewModel
import com.erg.almagestdota.R
import com.erg.almagestdota.base.external.extensions.emit
import com.erg.almagestdota.base.external.extensions.launchOnDefault
import com.erg.almagestdota.base.external.extensions.launchOnIO
import com.erg.almagestdota.base.external.providers.StringProvider
import com.erg.almagestdota.base.external.viewEvent.NotificationStatus
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.domain.Interactor
import com.erg.almagestdota.local_storage.external.models.main_settings.GlobalVars
import com.erg.almagestdota.network.external.firebase.FirebaseRepository
import com.erg.almagestdpta.core_navigation.external.helpers.MutableResultFlow
import com.erg.almagestdpta.core_navigation.external.helpers.ResultFlow
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import java.util.*
import javax.inject.Inject
import kotlin.math.abs

@HiltViewModel
class ActivityViewModel @Inject constructor(
    private val interactor: Interactor,
    private val stringProvider: StringProvider,
    private val firebaseRepository: FirebaseRepository,
) : ViewModel() {
    private val viewEventMutable = MutableResultFlow { resultKey, data -> }
    val viewEvent: ResultFlow = viewEventMutable

    private val loadingStateMutable = MutableStateFlow<Loading>(Loading.Disabled)
    val loadingState = loadingStateMutable.asStateFlow()

    private val viewStateMutable = MutableStateFlow<MainActivityViewState?>(null)
    val viewState = viewStateMutable.asStateFlow()

    private val stateConfigurator = StateConfigurator()

    init {
//        if (BuildConfig.DEBUG) {
//            loadData()
//        } else {
        if (needToLoadData()) {
            loadData()
        } else {
            initGlobalMainSettings()
        }
//        }
    }

    fun reload() {
        loadData()
    }

    private fun needToLoadData(): Boolean {
        val dataExpirationTimestamp = interactor.getDataExpirationTimestamp()
        return dataExpirationTimestamp == 0L ||
            dataExpirationTimestamp < System.currentTimeMillis() ||
            interactor.getMainSettings() == null
    }

    private fun loadData() {
        launchOnIO(
            loader = loadingStateMutable,
            action = { interactor.loadData() },
            onSuccess = {
                if (!GlobalVars.mainSettings.isActualVersion) {
                    val text = stringProvider.getString(R.string.has_new_version)
                    emit(
                        viewEventMutable,
                        ViewEvent.AlmagestSnackbar(text, NotificationStatus.WARNING)
                    )
                }
                initGlobalMainSettings()
            },
            onError = {
                if (interactor.getMainSettings() == null) {
                    val text = stringProvider.getString(R.string.loading_error)
                    emit(
                        viewEventMutable,
                        ViewEvent.AlmagestSnackbar(text, NotificationStatus.ERROR)
                    )
                    viewStateMutable.value = stateConfigurator.defineState(false)
                } else {
                    initGlobalMainSettings()
                }
            }
        )
    }

    private fun initGlobalMainSettings() {
        launchOnDefault(
            loader = loadingStateMutable,
            action = {
                firebaseRepository.getPlayersSettings()
                firebaseRepository.getTeamsSettings()
                GlobalVars.importantSettings = firebaseRepository.getImportantSettings()
                interactor.initGlobalMainSettings()
            },
            onSuccess = {
                viewStateMutable.value = stateConfigurator.defineState(true)
            },
            onError = {
                val text = it.message
                emit(viewEventMutable, ViewEvent.AlmagestSnackbar(text, NotificationStatus.ERROR))
                viewStateMutable.value = stateConfigurator.defineState(false)
            }
        )
    }

    fun needToShowAppReview(): Boolean {
        val previousAppReviewTime = interactor.getLastReviewTimestamp()
        return if (previousAppReviewTime == 0L) {
            true
        } else {
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = System.currentTimeMillis()
            val currentMonth =
                MONTH_IN_YEAR * calendar.get(Calendar.YEAR) + calendar.get(Calendar.MONTH)
            calendar.timeInMillis = previousAppReviewTime
            val previousMonth =
                MONTH_IN_YEAR * calendar.get(Calendar.YEAR) + calendar.get(Calendar.MONTH)
            abs(currentMonth - previousMonth) >= DELAY_BETWEEN_REVIEWS_IN_MONTHS
        }
    }

    fun finishReview() {
        interactor.setLastReviewTimestamp(System.currentTimeMillis())
    }

    private inner class StateConfigurator {
        fun defineState(isDataLoaded: Boolean): MainActivityViewState {
            return if (isDataLoaded) {
                MainActivityViewState.Loaded
            } else {
                MainActivityViewState.Retry
            }
        }
    }

    companion object {
        private const val DELAY_BETWEEN_REVIEWS_IN_MONTHS = 2
        private const val MONTH_IN_YEAR = 12
    }
}