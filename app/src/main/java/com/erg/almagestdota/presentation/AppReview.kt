package com.erg.almagestdota.presentation

import android.app.Activity
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.play.core.review.ReviewInfo
import com.google.android.play.core.review.ReviewManagerFactory
import java.util.*

object AppReview {
    private const val SP_APP_REVIEW = "SP_APP_REVIEW"

    private fun requestReview(activity: Activity, finishAction: () -> Unit) {
        if (hasGoogleServices(activity)) {
            /**
             * https://developer.android.com/guide/playcore/in-app-review
             */
            openGoogleServicesAppReview(activity, finishAction)
        }
    }

    private fun openGoogleServicesAppReview(activity: Activity, finishAction: () -> Unit) {
        val reviewManager = ReviewManagerFactory.create(activity)
        val requestReviewFlow = reviewManager.requestReviewFlow()
        requestReviewFlow.addOnCompleteListener { task ->
            try {
                if (task.isSuccessful) {
                    val reviewInfo: ReviewInfo = task.result
                    val flow = reviewManager.launchReviewFlow(activity, reviewInfo)
                    flow.addOnCompleteListener { finishAction.invoke() }
                } else {
                    handleReviewError()
                }
            } catch (ex: Exception) {
                handleReviewError()
            }
        }
    }

    private fun hasGoogleServices(activity: Activity): Boolean {
        val googleApiAvailability: GoogleApiAvailability = GoogleApiAvailability.getInstance()
        val resultCode: Int = googleApiAvailability.isGooglePlayServicesAvailable(activity.applicationContext)
        return resultCode == ConnectionResult.SUCCESS
    }

    private fun handleReviewError() {
    }

    fun configureAppReview(activity: Activity, finishAction: () -> Unit) {
        requestReview(activity, finishAction)
    }
}