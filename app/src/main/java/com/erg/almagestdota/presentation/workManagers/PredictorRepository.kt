package com.erg.almagestdota.presentation.workManagers

import com.erg.almagestdota.local_storage.external.models.live.LeagueGame
import com.erg.almagestdota.pro_matches.domain.GetLiveLeagueGamesUseCase
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

class PredictorRepository @Inject constructor(
    private val getLiveLeagueGamesUseCase: GetLiveLeagueGamesUseCase
) : IPredictorRepository {
    override suspend fun getLiveLeagueGames(): StateFlow<List<LeagueGame>?> {
        return getLiveLeagueGamesUseCase.getMatches()
    }
}