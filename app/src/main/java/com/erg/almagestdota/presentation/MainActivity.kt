package com.erg.almagestdota.presentation

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.ui.setupWithNavController
import androidx.work.*
import com.erg.almagestdota.R
import com.erg.almagestdota.base.external.rx.clicksWithDebounce
import com.erg.almagestdota.base.external.ui.ActivityManager
import com.erg.almagestdota.base.external.ui.BackPressListener
import com.erg.almagestdota.base.external.viewEvent.ViewEvent
import com.erg.almagestdota.base.external.viewState.Loading
import com.erg.almagestdota.databinding.ActivityMainBinding
import com.erg.almagestdota.external.AlmagestSnackbar
import com.erg.almagestdota.presentation.workManagers.PredictorWorkManager
import com.erg.almagestdpta.core_navigation.external.helpers.BottomSheetFragment
import com.erg.almagestdpta.core_navigation.external.helpers.obtainNavController
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import java.util.concurrent.TimeUnit
import kotlin.system.exitProcess

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), ActivityManager {

    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private val navController by lazy { obtainNavController(R.id.navHost) }
    private var backPressed = 0L
    private val viewModel by viewModels<ActivityViewModel>()
    private val eventsConfigurator = EventsConfigurator()
    private val currentVisibleFragment: Fragment?
        get() =
            if (supportFragmentManager.fragments.lastOrNull() is BottomSheetFragment) {
                supportFragmentManager.fragments.last()
            } else {
                supportFragmentManager.fragments.first().childFragmentManager.fragments.lastOrNull { it.isVisible }
            }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        observeViewModel()
        binding.tvRetry.clicksWithDebounce {
            viewModel.reload()
        }
    }

    private fun observeViewModel() {
        viewModel.viewEvent.flowWithLifecycle(lifecycle).onEach { viewEvent ->
            eventsConfigurator.handleViewEvent(viewEvent)
        }.launchIn(lifecycleScope)
        viewModel.loadingState.flowWithLifecycle(lifecycle).onEach {
            binding.loadingView.isVisible = it is Loading.Enabled
        }.launchIn(lifecycleScope)
        viewModel.viewState.flowWithLifecycle(lifecycle, Lifecycle.State.CREATED).filterNotNull().onEach {
            if (it is MainActivityViewState.Retry) {
                binding.btmNavView.isVisible = false
                binding.navHost.isVisible = false
                binding.tvRetry.isVisible = true
            } else {
                navController.setGraph(R.navigation.navigation_root)
                binding.btmNavView.setupWithNavController(navController)
                binding.btmNavView.isVisible = true
                binding.navHost.isVisible = true
                binding.tvRetry.isVisible = false
                for (item in list)
                    startWork(item)
            }
        }.launchIn(lifecycleScope)
    }

    private val list: List<Long> = listOf(
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
        27,
        28,
        29,
        31,
        33,
        35,
        37,
        39,
        41,
        43,
        47,
        49,
        51,
        53,
        57,
        59,
        61,
        67,
        69
    )

    private fun startWork(min: Long) {
        val work = PeriodicWorkRequestBuilder<PredictorWorkManager>(min, TimeUnit.MINUTES)
            .build()
        WorkManager.getInstance(applicationContext)
            .enqueueUniquePeriodicWork(min.toString(), ExistingPeriodicWorkPolicy.KEEP, work)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        navController.handleDeepLink(intent)
    }

    override fun onBackPressed() {
        if (onBackPressedConsumedByFragment()) return
        (this as? ActivityManager)?.hideKeyboard()
        val countFragment = supportFragmentManager.backStackEntryCount
        var countChild = 0
        for (str in supportFragmentManager.fragments) {
            if (str.childFragmentManager.backStackEntryCount > 1) countChild++
        }
        if (countChild > 0) {
            for (fragment in supportFragmentManager.fragments) {
                if (fragment.childFragmentManager.backStackEntryCount > 1) {
                    fragment.childFragmentManager.popBackStack()
                }
            }
        } else if (countFragment > 0) {
            supportFragmentManager.popBackStack()
        } else {
            if (backPressed + 2000 > System.currentTimeMillis()) {
                super.onBackPressed()
                finishAndRemoveTask()
                exitProcess(0)
            } else {
                Toast.makeText(this, getString(R.string.main_back_pressed), Toast.LENGTH_SHORT).show()
                backPressed = System.currentTimeMillis()
            }
        }
    }

    private fun onBackPressedConsumedByFragment(): Boolean {
        val fragment = currentVisibleFragment
        if (fragment is BackPressListener) {
            fragment.onBackPressed()
            return true
        }
        return false
    }

    override fun showAppReview() {
        if (viewModel.needToShowAppReview()) {
            AppReview.configureAppReview(this) { viewModel.finishReview() }
        }
    }

    override fun hideKeyboard() {
        (this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager)
            .hideSoftInputFromWindow(binding.root.windowToken, 0)
    }

    override fun showKeyboard(editText: EditText) {
        (this.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager)
            .showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
    }

    private inner class EventsConfigurator {
        fun handleViewEvent(event: ViewEvent) {
            when (event) {
                is ViewEvent.AlmagestSnackbar -> handleSnackbar(event)
                else -> Unit
            }
        }

        private fun handleSnackbar(event: ViewEvent.AlmagestSnackbar) {
            AlmagestSnackbar.Builder(binding.root.context)
                .setStatus(event.status.index)
                .setMessage(event.messageId)
                .setMessage(event.message)
                .setGravity(event.gravity)
                .setDelay(event.delayMillis)
                .setOnDismissCallback(event.onDismiss)
                .build()
                .show(binding.root)
        }
    }
}

// todo настроить ci/cd
// todo sampleapp для запуска каждой отдельной фичи в своем активити
// todo тесты kaspresso
// todo привести в порядок строки