package com.erg.almagestdota.presentation

import com.erg.almagestdota.base.external.viewState.ViewState

sealed class MainActivityViewState : ViewState() {
    object Retry : MainActivityViewState()
    object Loaded : MainActivityViewState()
}