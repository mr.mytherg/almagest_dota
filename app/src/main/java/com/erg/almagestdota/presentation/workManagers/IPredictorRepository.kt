package com.erg.almagestdota.presentation.workManagers

import com.erg.almagestdota.local_storage.external.models.live.LeagueGame
import kotlinx.coroutines.flow.StateFlow

interface IPredictorRepository {
     suspend fun getLiveLeagueGames(): StateFlow<List<LeagueGame>?>
}