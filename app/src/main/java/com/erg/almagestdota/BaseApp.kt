package com.erg.almagestdota

import android.app.Application
import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import androidx.hilt.work.HiltWorkerFactory
import androidx.work.Configuration
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class BaseApp : Application(), Configuration.Provider {

    @Inject
    lateinit var workerFactory: HiltWorkerFactory

    override fun getWorkManagerConfiguration() =
        Configuration.Builder()
            .setWorkerFactory(workerFactory)
            .build()

	init {
		AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
	}

	override fun attachBaseContext(base: Context?) {
		super.attachBaseContext(base)
	}

	override fun onCreate() {
        super.onCreate()
    }
}