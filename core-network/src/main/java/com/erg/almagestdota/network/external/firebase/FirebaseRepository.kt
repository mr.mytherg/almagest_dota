package com.erg.almagestdota.network.external.firebase

import android.content.Context
import android.content.pm.PackageInfo
import android.provider.Settings.Global
import com.erg.almagestdota.base.external.extensions.asMap
import com.erg.almagestdota.base.external.mapper.essentialMap
import com.erg.almagestdota.local_storage.external.models.draft.main.MemberInfo
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch
import com.erg.almagestdota.local_storage.external.raw_models.*
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.tasks.await
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FirebaseRepository @Inject constructor(
    private val api: FirebaseFirestore,
    @ApplicationContext private val context: Context,
    private val gson: Gson,
) : FirebaseRepositoryContract {
    private val pInfo: PackageInfo = context.packageManager.getPackageInfo(context.packageName, 0)
    private val version = pInfo.versionName

    companion object {
        val MATCHES_DOCUMENT_NAME = "16Matches"
        val TEAMS_DOC_NAME = "1Teams"
        val PLAYER_DOC_NAME = "1Players"
        const val PLAYERS_BY_DOC = 6000
        const val TEAMS_BY_DOC = 2000
    }

    override suspend fun getMainSettingsOrNull(): MainSettings? {
        val docRef = api.collection(version).document("settings")
        val document = docRef.get().await()
        return if (document.exists()) {
            val settingsRaw: MainSettingsRaw =
                gson.fromJson(gson.toJson(document.data?.toMap()), MainSettingsRaw::class.java)
            MainSettingsRaw.toModel(settingsRaw)
        } else
            null
    }

    override suspend fun getMainSettings(): MainSettings {
        val docRef = api.collection(version).document("settings")
        val document = docRef.get().await()
        return MainSettingsRaw.toModel(document.toObject(MainSettingsRaw::class.java)!!)
    }

    override suspend fun setMainSettings(mainSettings: MainSettings) {
        api.collection(version).document("settings").set(MainSettings.toModel(mainSettings)).await()
    }

    override suspend fun getMainPredictor(): MainPredictor {
        val docRef = api.collection(version).document("predictor")
        val document = docRef.get().await()
        val model = MainPredictorRaw.toModel(document.toObject(MainPredictorRaw::class.java)!!)
        return model
    }

    override suspend fun setMainPredictor(mainPredictor: MainPredictor) {
        api.collection(version).document("predictor").set(MainPredictor.toModel(mainPredictor)).await()
    }

    override suspend fun getImportantSettings(): ImportantSettings {
        val docRef = api.collection(MATCHES_DOCUMENT_NAME).document("important")
        val document = docRef.get().await()
        val model = ImportantSettingsRaw.toModel(document.toObject(ImportantSettingsRaw::class.java)!!)
        return model
    }

    override suspend fun setImportantSettings(importantSettings: ImportantSettings) {
        api.collection(MATCHES_DOCUMENT_NAME).document("important")
            .set(ImportantSettings.toModel(importantSettings)).await()
    }

    override suspend fun getPlayersSettings(): PlayersSettings {
        val playersDoc = api.collection(PLAYER_DOC_NAME)
        val queryDocumentSnapshot = playersDoc.get().await()
        val rawSet = mutableSetOf<MemberInfoRaw>()
        for (documentSnapshot in queryDocumentSnapshot) {
            rawSet.addAll(documentSnapshot.toObject(PlayersSettingsRaw::class.java).players)
        }
        val players = rawSet.map { MemberInfoRaw.toModel(it) }
        GlobalVars.initPlayers(players)
        return PlayersSettings(players)
    }

    override suspend fun setPlayersSettings(playersSettings: PlayersSettings) {
        var players = playersSettings.players.toList()
        var index = 0
        while (true) {
            if (players.size > PLAYERS_BY_DOC) {
                val subList = players.take(PLAYERS_BY_DOC)
                players = players.subList(PLAYERS_BY_DOC, players.size)
                setPlayersPage(index, subList)
            } else {
                if (players.isNotEmpty())
                    setPlayersPage(index, players)
                break
            }
            index++
        }
        GlobalVars.initPlayers(playersSettings.players)
    }

    private suspend fun setPlayersPage(page: Int, players: List<MemberInfo>) {
        val docRef = api.collection(PLAYER_DOC_NAME).document(page.toString())
        docRef.set(PlayersSettings.toModel(PlayersSettings(players)))
    }

    private suspend fun setTeamsPage(page: Int, teams: List<TeamInfo>) {
        val docRef = api.collection(TEAMS_DOC_NAME).document(page.toString())
        docRef.set(TeamsSettings.toModel(TeamsSettings(teams)))
    }

    override suspend fun getTeamsSettings(): TeamsSettings {
        val teamsDoc = api.collection(TEAMS_DOC_NAME)
        val queryDocumentSnapshot = teamsDoc.get().await()
        val rawSet = mutableSetOf<TeamInfoRaw>()
        for (documentSnapshot in queryDocumentSnapshot) {
            rawSet.addAll(documentSnapshot.toObject(TeamSettingsRaw::class.java).teams)
        }
        val teams = rawSet.map { TeamInfoRaw.toModel(it) }
        GlobalVars.initTeams(teams)
        return TeamsSettings(teams)
    }

    override suspend fun setTeamsSettings(teamsSettings: TeamsSettings) {
        var teams = teamsSettings.teams.toList()
        var index = 0
        while (true) {
            if (teams.size > TEAMS_BY_DOC) {
                val subList = teams.take(TEAMS_BY_DOC)
                teams = teams.subList(TEAMS_BY_DOC, teams.size)
                setTeamsPage(index, subList)
            } else {
                if (teams.isNotEmpty())
                    setTeamsPage(index, teams)
                break
            }
            index++
        }
        GlobalVars.initTeams(teamsSettings.teams)
    }

    override suspend fun getMatchesPage(page: Int): List<DotaMatch> {
        val docRef = api.collection(MATCHES_DOCUMENT_NAME).document(page.toString())
        val document = docRef.get().await()
        return document.toObject(DotaMatchesRaw::class.java)!!.matches.map { DotaMatchRaw.toModel(it) }
    }

    override suspend fun setMatchesPage(page: Int, matches: List<DotaMatch>) {
        val docRef = api.collection(MATCHES_DOCUMENT_NAME).document(page.toString())
        docRef.set(DotaMatchesRaw(matches.map { DotaMatch.toModel(it) }))
    }

    override suspend fun getMatches(): List<DotaMatch> {
        val matches = api.collection(MATCHES_DOCUMENT_NAME)
        val queryDocumentSnapshot = matches.get().await()
        val allTournamentMatchesRawSet = mutableSetOf<DotaMatchRaw>()
        for (documentSnapshot in queryDocumentSnapshot) {
            allTournamentMatchesRawSet.addAll(documentSnapshot.toObject(DotaMatchesRaw::class.java).matches)
        }
        val dotaMatches =
            allTournamentMatchesRawSet.map { DotaMatchRaw.toModel(it) }.sortedBy { it.matchId }
        return dotaMatches
    }

    override suspend fun copyMatches() {
        val matches = api.collection(MATCHES_DOCUMENT_NAME)
        val queryDocumentSnapshot = matches.get().await()
        for (documentSnapshot in queryDocumentSnapshot) {
            val docMap = documentSnapshot.data as HashMap<String, Any>
            api.collection("17Matches").document(documentSnapshot.id).set(docMap)
        }
    }

    override suspend fun copyPlayers() {
        val matches = api.collection(PLAYER_DOC_NAME)
        val queryDocumentSnapshot = matches.get().await()
        for (documentSnapshot in queryDocumentSnapshot) {
            val docMap = documentSnapshot.data as HashMap<String, Any>
            api.collection("2Players").document(documentSnapshot.id).set(docMap)
        }
    }

    override suspend fun copyTeams() {
        val matches = api.collection(TEAMS_DOC_NAME)
        val queryDocumentSnapshot = matches.get().await()
        for (documentSnapshot in queryDocumentSnapshot) {
            val docMap = documentSnapshot.data as HashMap<String, Any>
            api.collection("10Teams").document(documentSnapshot.id).set(docMap)
        }
    }
}