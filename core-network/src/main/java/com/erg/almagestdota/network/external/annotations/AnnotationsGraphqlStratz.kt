package com.erg.almagestdota.network.external.annotations

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class OkHttpClientGraphqlStratz()

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class RetrofitClientGraphqlStratz()