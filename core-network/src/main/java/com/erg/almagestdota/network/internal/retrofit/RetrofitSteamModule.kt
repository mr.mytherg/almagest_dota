package com.erg.almagestdota.network.internal.retrofit

import com.erg.almagestdota.network.external.annotations.OkHttpClientSteam
import com.erg.almagestdota.network.external.annotations.RetrofitClientSteam
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
internal object RetrofitSteamModule {

    private const val CONNECT_TIMEOUT_SECONDS = 300L
    private const val READ_TIMEOUT_SECONDS = 300L
    private const val WRITE_TIMEOUT_SECONDS = 300L
    private const val BASE_URL = "https://api.steampowered.com/"

    @Provides
    @Singleton
    @OkHttpClientSteam
    fun provideHttpClientSteam(): OkHttpClient {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient().newBuilder()
            .followRedirects(true)
            .followSslRedirects(false)
            .addInterceptor(httpLoggingInterceptor)
            .connectTimeout(CONNECT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .readTimeout(READ_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .writeTimeout(WRITE_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .build()
    }

    @Provides
    @Singleton
    @RetrofitClientSteam
    fun provideRetrofitClientForSteamApi(@OkHttpClientSteam httpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .client(httpClient)
            .build()
    }
}