package com.erg.almagestdota.network.external.annotations

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class OkHttpClientStratz()

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class RetrofitClientStratz()