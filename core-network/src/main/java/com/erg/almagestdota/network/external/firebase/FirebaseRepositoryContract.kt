package com.erg.almagestdota.network.external.firebase

import com.erg.almagestdota.local_storage.external.models.draft.main.MemberInfo
import com.erg.almagestdota.local_storage.external.models.draft.main.TeamInfo
import com.erg.almagestdota.local_storage.external.models.main_settings.*
import com.erg.almagestdota.local_storage.external.models.matches.DotaMatch

interface FirebaseRepositoryContract {
    suspend fun getMainSettingsOrNull(): MainSettings?
    suspend fun getMainSettings(): MainSettings
    suspend fun setMainSettings(mainSettings: MainSettings)

    suspend fun getMainPredictor(): MainPredictor
    suspend fun setMainPredictor(mainPredictor: MainPredictor)

    suspend fun getImportantSettings(): ImportantSettings
    suspend fun setImportantSettings(importantSettings: ImportantSettings)

    suspend fun getPlayersSettings(): PlayersSettings
    suspend fun setPlayersSettings(playersSettings: PlayersSettings)

    suspend fun getTeamsSettings(): TeamsSettings
    suspend fun setTeamsSettings(teamsSettings: TeamsSettings)

    suspend fun getMatchesPage(page: Int): List<DotaMatch>
    suspend fun setMatchesPage(page: Int, matches: List<DotaMatch>)

    suspend fun getMatches(): List<DotaMatch>
    suspend fun copyMatches()
    suspend fun copyPlayers()
    suspend fun copyTeams()


}