package com.erg.almagestdota.network.internal.apollo

import com.apollographql.apollo.ApolloClient
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
internal object ApolloModule {
    private const val STRATZ_QRAPHQL_BASE_URL = "https://api.stratz.com/graphql"

    @Provides
    @Singleton
    fun provideApolloClient(): ApolloClient {
        return ApolloClient.builder()
            .serverUrl(STRATZ_QRAPHQL_BASE_URL)
            .build()
    }
}